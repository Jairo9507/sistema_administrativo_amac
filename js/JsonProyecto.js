$(document).ready(function(){
	$("#codigo").focus();

	$("input#fechaFin").blur(function(){
		var date1=new Date($("input#fechaInicio").val());
		var date2 = new Date($(this).val());
		var diffTime= Math.abs(date2-date1);
		console.log(diffTime);
		var diffDays= Math.ceil(diffTime/ (1000 * 60 * 60 * 24));
		console.log(diffDays);
		$("#totalDias").val(diffDays);
	});

	$("#btnGuardar").click(function(){
		var proyecto=new Object();
		proyecto.id=$("input#id").val();
		proyecto.codigo=$("input#codigo").val();
		proyecto.nombre=$("textarea#nombre").val();
		proyecto.alcance=$("textarea#alcance").val();
		proyecto.justificante=$("textarea#justificante").val();
		proyecto.prioridad=$("select#prioridad").val();
		proyecto.tipo=$("select#tipoProyecto").val();
		proyecto.fechaInicio=$("input#fechaInicio").val();
		proyecto.fechaFin= $("input#fechaFin").val();
		proyecto.costo= $("input#costo").val();
		proyecto.ubicacion= $("textarea#ubicacion").val();
		proyecto.supervisor= $("select#nombreSupervisor").val();
		proyecto.realizador= $("input#realizador").val();
		proyecto.totalDias=$("input#totalDias").val();
		if (proyecto.id===undefined) {
			proyecto.id='';
		}
		console.log(proyecto);
			$("#modal_nuevo").modal('hide');
						
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");		
		var DatosJson=JSON.stringify(proyecto);
		console.log(DatosJson);
		$.post(baseurl+'proyectos/proyecto/save',{
			ProyectoPost: DatosJson
		},function(data,textStatus){
			console.log(data);
			$("#mensaje").html(data.response_msg);
		},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    					console.log('Error: ' + response.responseText);

		});;
	});
});