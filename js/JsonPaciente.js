$(document).ready(function(){
$("#primer_nombre").focus();
	/*$("#telefono_fijo","#formulario").keypress(function(e){
        var key = e.charCode || e.keyCode || 0;

        $nit = $(this);

        // Auto-format- do not expose the mask as the user begins to type
        if (key !== 8 && key !== 9) {

            if ($nit.val().length === 4) {
                $nit.val($nit.val() + '-');
            } else if($nit.val().length> 8) {
                 e.preventDefault();
            }



        }

        // Allow numeric (and tab, backspace, delete) keys only
        return (key === 8 ||
                key === 9 ||
                key === 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
	});
	$("#telefono_movil","#formulario").keypress(function(e){
        var key = e.charCode || e.keyCode || 0;

        $nit = $(this);

        // Auto-format- do not expose the mask as the user begins to type
        if (key !== 8 && key !== 9) {

            if ($nit.val().length === 4) {
                $nit.val($nit.val() + '-');
            }  else if($nit.val().length> 8) {
                 e.preventDefault();
            }

        }

        // Allow numeric (and tab, backspace, delete) keys only
        return (key === 8 ||
                key === 9 ||
                key === 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
	});	*/
	$("#dui","#formulario").keypress(function(e){
        var key = e.charCode || e.keyCode || 0;

        $nit = $(this);

        // Auto-format- do not expose the mask as the user begins to type
        if (key !== 8 && key !== 9) {

            if ($nit.val().length === 8) {
                $nit.val($nit.val() + '-');
            } else if($nit.val().length> 8) {
                 e.preventDefault();
            }



        }

        // Allow numeric (and tab, backspace, delete) keys only
        return (key === 8 ||
                key === 9 ||
                key === 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
	});	
var codigo='';
    $.ajax({
        type: "GET",
        url: baseurl+'clinica/paciente/obtenerCodigo',
        dataType:"json",
        success: function(data,textStatus){
            var d=new Date();
            var n =d.getFullYear(); 
                       
            console.log("data "+data.codigo);
            if (data.codigo!=null) {                
                if (data.codigo.split('-')[1]==n) {
                    console.log(data.codigo.split('-')[0]);
                    codigo="0"+String(parseInt(data.codigo.split('-')[0])+1)+"-"+n;                                

                    console.log(codigo);
                } else {
                    codigo='01-'+n;
                    console.log(codigo);
                }

            } else {
                $("input#codigo_expediente").val('01-'+n);
            }
        }

    });

	$("#btnGuardar").click(function(){
		var paciente=new Object();
		paciente.id=$("input#id").val();
		paciente.primer_nombre=$("input#primer_nombre").val();
		paciente.segundo_nombre=$("input#segundo_nombre").val();
		paciente.tercer_nombre=$("input#tercer_nombre").val();
		paciente.primer_apellido=$("input#primer_apellido").val();
		paciente.segundo_apellido=$("input#segundo_apellido").val();
		paciente.tercer_apellido=$("input#tercer_apellido").val();
		paciente.genero=$("input[name='genero']:checked").val();
        paciente.fecha_nacimiento=$("input#fecha_nacimiento").val();
		paciente.nombre_madre=$("input#nombre_madre").val();
		paciente.nombre_padre=$("input#nombre_padre").val();
		paciente.direccion=$("textarea#direccion").val();
		paciente.dui=$("input#dui").val().replace("-","");
        paciente.responsable=$("input#responsable").val();
        if($("input#codigo_expediente").val()===undefined)
        {
            paciente.codigo_expediente=codigo;
        } else {
            paciente.codigo_expediente=$("input#codigo_expediente").val();
        }
        if(paciente.id===undefined){paciente.id='';} else {

        }
		console.log(paciente);
		$("#modal_nuevo").modal('hide');
		$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");		
		var DatosJson=JSON.stringify(paciente);
		$.post(baseurl+'clinica/paciente/save',{
			PacientePost:DatosJson
		},function(data,textStatus){
			console.log(data);
			$("#mensaje").html(data.response_msg);
		},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    			console.log('Error: ' + response.responseText);

		});;
	});
});