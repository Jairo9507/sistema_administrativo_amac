$(document).ready(function(){

	$("#nombre").focus();
    
	$("#telefono_empresa","#formulario").keypress(function(e){ 

        var key = e.charCode || e.keyCode || 0;
        $nit = $(this);

        // Auto-format- do not expose the mask as the user begins to type
        if (key >=48 && key <= 57) {                     
            if ($nit.val().length === 4) {
                $nit.val($nit.val() + '-');
            } else if($nit.val().length===9)
            {
                e.preventDefault();
            }             

        } else {
            e.preventDefault();
        }

        // Allow numeric (and tab, backspace, delete) keys only
        return (key === 8 ||
                key === 9 ||
                key === 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
	});
    $("#celular_contacto","#formulario").keypress(function(e){ 

        var key = e.charCode || e.keyCode || 0;
        $nit = $(this);

        // Auto-format- do not expose the mask as the user begins to type
        if (key >=48 && key <= 57) {                     
            if ($nit.val().length === 4) {
                $nit.val($nit.val() + '-');
            } else if($nit.val().length===9)
            {
                e.preventDefault();
            }            

        } else {
            e.preventDefault();
        }

        // Allow numeric (and tab, backspace, delete) keys only
        return (key === 8 ||
                key === 9 ||
                key === 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
    });
    $("#telefono_contacto","#formulario").keypress(function(e){ 

        var key = e.charCode || e.keyCode || 0;
        $nit = $(this);

        // Auto-format- do not expose the mask as the user begins to type
        if (key >=48 && key <= 57) {                     
            if ($nit.val().length === 4) {
                $nit.val($nit.val() + '-');
            } else if($nit.val().length===9)
            {
                e.preventDefault();
            }            

        } else {
            e.preventDefault();
        }

        // Allow numeric (and tab, backspace, delete) keys only
        return (key === 8 ||
                key === 9 ||
                key === 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
    });


    $("#fax_empresa","#formulario").keypress(function(e){
        var key = e.charCode || e.keyCode || 0;

        $nit = $(this);

        // Auto-format- do not expose the mask as the user begins to type
        if (key >=48 && key <= 57) {                     
            if ($nit.val().length === 4) {
                $nit.val($nit.val() + '-');
            } else if($nit.val().length===9)
            {
                e.preventDefault();
            }            

        } else {
            e.preventDefault();
        }



        

        // Allow numeric (and tab, backspace, delete) keys only
        return (key === 8 ||
                key === 9 ||
                key === 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));     

    });

    $("#celular_proveedor","#formulario").keypress(function(e){
        var key = e.charCode || e.keyCode || 0;

        $nit = $(this);

        // Auto-format- do not expose the mask as the user begins to type
        if (key >=48 && key <= 57) {                     
            if ($nit.val().length === 7) {
                $nit.val($nit.val() + '-');
            } else if($nit.val().length===9)
            {
                e.preventDefault();
            }            

        } else {
            e.preventDefault();
        }

        // Allow numeric (and tab, backspace, delete) keys only
        return (key === 8 ||
                key === 9 ||
                key === 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
    }); 

    $("#nrc_empresa","#formulario").keypress(function(e){
        var key = e.charCode || e.keyCode || 0;

        $nit = $(this);

        // Auto-format- do not expose the mask as the user begins to type
            if (key >=48 && key <= 57) {                     
                if ($nit.val().length === 7) {
                    $nit.val($nit.val() + '-');
                } else if($nit.val().length===9)
                    {
                        e.preventDefault();
                    }            
            } else {
                e.preventDefault();
            }

        // Allow numeric (and tab, backspace, delete) keys only
        return (key === 8 ||
                key === 9 ||
                key === 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
    }); 
    $("#nit_empresa","#formulario").keypress(function(e){
        var key = e.charCode || e.keyCode || 0;
        //console.log(key);
        $nit = $(this);

        // Auto-format- do not expose the mask as the user begins to type
        if (key >=48 && key <= 57) {
                console.log($nit.val().length);
            if ($nit.val().length === 4) {
                $nit.val($nit.val() + '-');
            } else if($nit.val().length===11){
                $nit.val($nit.val() + '-');
            } else if($nit.val().length===15){
                $nit.val($nit.val() + '-');
            } else if($nit.val().length>=16){
                 e.preventDefault();
            }
        } else {
            e.preventDefault();
        }

        // Allow numeric (and tab, backspace, delete) keys only
        return (key === 8 ||
                key === 9 ||
                key === 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
    });



	$("#btnGuardar").click(function(){
			var Proveedor =new Object();
			Proveedor.id= $("input#id").val();
			Proveedor.nombre =$("input#nombre").val();
			Proveedor.telefono=$("input#telefono_empresa").val().replace("-","");
			Proveedor.fax=$("input#fax_empresa").val().replace("-","");
			Proveedor.direccion=$("textarea#direccion").val();
			Proveedor.web=$("input#pagina_empresa").val();
			Proveedor.correo=$("input#correo_empresa").val();
			Proveedor.nrc=$("input#nrc_empresa").val().replace(new RegExp('-','g'),"");
			Proveedor.nit=$("input#nit_empresa").val().replace(new RegExp('-','g'),"");
			Proveedor.rubro=$("select#rubro_id").val();
			Proveedor.contacto_primer_nombre=$("input#primer_nombre_contacto").val();
			Proveedor.contacto_segundo_nombre=$("input#segundo_nombre_contacto").val();
			Proveedor.contacto_primer_apellido=$("input#primer_apellido_contacto").val();
			Proveedor.contacto_segundo_apellido=$("input#contacto_segundo_apellido").val();
			Proveedor.contacto_telefono=$("input#telefono_contacto").val().replace("-","");
			Proveedor.contacto_celular=$("input#celular_contacto").val().replace("-","");
			if(Proveedor.id==undefined){
				Proveedor.id='';
			}
			console.log(Proveedor);
			$("#modal_nuevo").modal('hide');
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
			var DatosJson= JSON.stringify(Proveedor);
			$.post(baseurl+'catalogos/cat_proveedor/save',{
				proveedorPost:DatosJson
			},function(data,textStatus){
				$("#mensaje").html(data.response_msg)
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    					console.log('Error: ' + response.responseText);

			});;
			return false;
	});


});

