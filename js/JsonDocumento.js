$(document).ready(function(){
var codigo=null;
//var correlativo=null;
	
	$.ajax({
    	type: "GET",
    	url: baseurl+'archivos/documentos/obtenerCodigo',
    	dataType:"json",
    	success:function(data,textStatus){
                console.log("data "+data.codigo);
                if (data.codigo!=null &&  data.codigo!="000") {                	
                    //int=parseInt(data.codigo.substring(data.codigo.length-1));                    
                    //console.log(int);                        
                    codigo=data.codigo;               
                    console.log("codigo: "+codigo);                
                } else {
                    codigo='1';
                    console.log("else "+codigo);
                } 
    	}

	});
/*	$.ajax({
    	type: "GET",
    	url: baseurl+'archivos/documentos/obtenerCorrelativoCentral',
    	dataType:"json",
    	success:function(data,textStatus){
                console.log("data "+data.codigo);
                if (data.codigo!=null &&  data.codigo!="000") {                	
                    console.log(data.codigo.substring(data.codigo.length-1));
                    int=parseInt(data.codigo.substring(data.codigo.length-1));                    
                    console.log(int);                        
                    correlativo=int+1;  
                    if ($("input#caja_correlativo_cental")!=''|| $("input#caja_correlativo_cental")!=undefined) {
                    	$("input#caja_correlativo_central").val(correlativo);
                    } 
                    console.log("correlativo_central: "+correlativo);                
                } else {
                    correlativo=1;
                    $("input#caja_correlativo_central").val(correlativo); 
                    console.log("input "+$("input#caja_correlativo_central").val());
                } 
    	}

	});
*/
	$("#codigo").focus();

		$("select#serie_id").change(function(){
			$("#subserie_id").attr("disabled",true);
			var html='';
			var serie=new Object();
			serie.id=$(this).val();
			console.log(serie);
			var DatosJson=JSON.stringify(serie);
			$.post(baseurl+'archivos/documentos/obtenerSubseries',{
				SeriePost:DatosJson
			},function(data,textStatus){
				 html+='<option value="">Seleccione</option>';
				 console.log(data.valor);
				data.valor.forEach(function(element){
					//console.log(element);
					html+="<option value='"+element.ID_SUB_SERIE_DOCUMENTAL+"'>"+element.NOMBRE+"</option>";
				});
				$("#subserie_id").attr("disabled",false);
				$("#subserie_id").html(html);
			},"json").fail(function(response){
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
	    					console.log('Error: ' + response.responseText);
			});
		});
		$("select#pasillo_id").change(function(){
			$("select#estante_id").attr("disabled",true);
			$("select#anaquel_id").attr("disabled",true);
			var html='';
			var pasillo=new Object();
			pasillo.id=$(this).val();
			console.log(pasillo);
			var DatosJson=JSON.stringify(pasillo);
			$.post(baseurl+'archivos/documentos/obtenerEstantes',{
				PasilloPost:DatosJson
			},function(data,textStatus){
			 html+='<option value="">Seleccione</option>';
			 console.log(data.valor);
			data.valor.forEach(function(element){
				//console.log(element);
				html+="<option value='"+element.ID_ESTANTE+"'>"+element.CODIGO_ESTANTE+" "+element.DESCRIPCION+"</option>";
			});
			$("select#estante_id").attr("disabled",false);
			$("select#estante_id").html(html);
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    					console.log('Error: ' + response.responseText);				
			});
		});
		$("select#estante_id").change(function(){
			$("select#anaquel_id").attr("disabled",true);
			var html='';
			var estante=new Object();
			estante.id=$(this).val();
			console.log(estante);
			var DatosJson=JSON.stringify(estante);
			$.post(baseurl+'archivos/documentos/obtenerAnaqueles',{
				EstantePost:DatosJson
			},function(data,textStatus){
			 html+='<option value="">Seleccione</option>';
			 console.log(data.valor);
			data.valor.forEach(function(element){
				console.log(element);
				html+="<option value='"+element.ID_ANAQUEL+"'>"+element.DESCRIPCION+"</option>";
			});
			$("select#anaquel_id").attr("disabled",false);
			$("select#anaquel_id").html(html);
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    					console.log('Error: ' + response.responseText);				

			});
		});

		$("select#unidad_id").change(function(){
			$("select#departamento_id").attr("disabled",true);
			var html='';
			var unidad=new Object();
			unidad.id=$(this).val();
			console.log(unidad);
			var DatosJson=JSON.stringify(unidad);
			$.post(baseurl+'archivos/documentos/obtenerDepartamentos',{
				DepartamentoPost:DatosJson
			},function(data,textStatus){
				html+='<option value="">Seleccione</option>';
				data.valor.forEach(function(element){
					console.log(element);
					html+="<option value='"+element.ID_UNIDAD+"''>"+element.CODIGO_UNIDAD+" "+element.DESCRIPCION+"</option>";
				});
				$("select#departamento_id").attr("disabled",false);				
				$("select#departamento_id").html(html);
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    					console.log('Error: ' + response.responseText);		
			});
		});

		$("select#tiempo").change(function(){
			var tiempo=$(this).val();
			if (tiempo!='P' || tiempo!='H') {
				var d = new Date($("input#fecha_documento_inicio").val());
				var year = d.getFullYear()+parseInt(tiempo);
				var month = d.getMonth();
				var day = d.getDate();
				console.log(d.toISOString());
				var c = new Date(year , month, day);
				console.log(c.getDate()+'/'+c.getMonth()+'/'+c.getFullYear());
				var date=c.getFullYear()+'-0'+c.getMonth()+'-'+c.getDate();
				console.log(c.toISOString().substr(0, 19).replace('T', ' '));
				$("input#fecha_salida").val(c.toISOString().substr(0, 10).replace('T', ' '));
			}
		});

		/*$("input#vigencia_gestion").blur(function(){			
			var num;
			num=parseInt($(this).val())+parseInt($("input#vigencia_central").val());
			console.log(num);
			$("input#vigencia_documental").val(num);
		});

		$("input#vigencia_central").blur(function(){
			var num;
			num=parseInt($(this).val())+parseInt($("input#vigencia_gestion").val());
			console.log(num);
			$("input#vigencia_documental").val(num);
		});	*/	
		var codigo=null;		

		$("#btnGuardar").click(function(){
			$.ajax({
		    	type: "GET",
		    	url: baseurl+'archivos/documentos/obtenerCodigo',
		    	dataType:"json",
		    	success:function(data,textStatus){
		                console.log("data "+data.codigo);
		                if (data.codigo!=null &&  data.codigo!="000") {                	
		                    //int=parseInt(data.codigo.substring(data.codigo.length-1));                    
		                    //console.log(int);                        
		                    codigo=data.codigo;               
		                    console.log("codigo: "+codigo);                
		                } else {
		                    codigo='1';
		                    console.log("else "+codigo);
		                } 
		    	}

			});			

			var documento=new Object();
			documento.id=$("input#id").val();
			if ($("input#codigo").val()==='' || $("input#codigo").val()===undefined) {
				documento.codigo="DOC-0"+codigo;

			} else {
				documento.codigo=$("input#codigo").val();
			}
			documento.nombre=$("input#nombre").val();
			documento.serie=$("select#serie_id").val();
			documento.subserie=$("select#subserie_id").val();
			documento.fecha_entrada=$("input#fecha_entrada").val();
			documento.fecha_salida=$("input#fecha_salida").val();
			documento.no_cajas=$("input#no_cajas").val();
			documento.no_carpetas=$("input#no_carpetas").val();
			documento.no_tomo=$("input#no_tomo").val();
			documento.no_folios=$("input#no_folios").val();
			documento.asuntos=$("textarea#asuntos").val();
			documento.otros=$("input#otros").val();
			documento.soporte=$("select#soporte").val();
			documento.notas=$("input#notas").val();
			documento.anaquel=$("select#anaquel_id").val();
			documento.unidad=$("select#unidad_id").val();
			documento.departamento=$("select#departamento_id").val();
			documento.tipo=$("select#tiempo").val();
			documento.frecuencia=$("select#frecuencia_consulta").val();
			documento.almacenado=$("select#almacenado").val();
			documento.valor_primario=$("select#valor_primario").val();
			documento.valor_secundario=$("select#valor_secundario").val();
			documento.disposicion_final=$("select#disposicion_final").val();
			documento.unidad_comparte=$("select#unidad_comparte").val();
			documento.vigencia_central=$("input#vigencia_central").val();
			documento.vigencia_gestion=$("input#vigencia_gestion").val();
			documento.vigencia_documental=parseInt($("input#vigencia_gestion").val())+parseInt($("input#vigencia_central").val());
			documento.contenido=$("textarea#contenido").val();
			documento.fecha_inicio=$("input#fecha_documento_inicio").val();
			documento.fecha_final=$("input#fecha_documento_final").val();
			documento.correlativo_gestion=$("input#caja_correlativo_gestion").val();
			documento.correlativo_central=$("input#caja_correlativo_central").val();
			if (documento.tipo=='P' || documento.tip=='H') {
				documento.fecha_salida=null;
			}
			if($("textarea#procedimiento")===undefined){
				documento.procedimiento='';
			} else {
				documento.procedimiento=$("textarea#procedimiento").val();
			}
			$("#modal_nuevo").modal('hide');						
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");		
			console.log(documento);
			var DatosJson=JSON.stringify(documento);
			$.post(baseurl+'archivos/documentos/save',{
				DocumentoPost:DatosJson
			},function(data,textStatus){
				$("#mensaje").html(data.response_msg);
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    					console.log('Error: ' + response.responseText);				
			});
		});	
});