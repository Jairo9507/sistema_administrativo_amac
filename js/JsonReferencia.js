$(document).ready(function(){

var confirmacion = $('input[name="confirmacion"]:checked').val();

		if(confirmacion=="si"){
			$("input#primerNombre").css('display','none');
			$("#labelPrimernombre").css("display",'none');
			$("input#segundoNombre").css('display','none');
			$("#labelSegundonombre").css('display','none');
			$("input#primerApellido").css('display','none');
			$("#labelPrimerapellido").css('display','none');
			$("input#segundoApellido").css('display','none');
			$("#labelSegundoapellido").css('display','none');
			$("input#CorreoDoc").css('display','none');
			$("#divlistEspecialidad").css('display','block');
			$("#labelEspecialidad").css('display' ,'block');
			$("#divlistDoctor").css('display','block');
			$("#divNombreClinica").css('display','none');
			$("#labelClinica").css('display','none');
			$("#labelDoctor").css('display','block');
			$("#labelCorreo").css('display','none');
			$("#divCorreoDoc").css('display','none');
		}

	$("input[name='confirmacion']").click(function(){
		
		var confirmacion = $('input[name="confirmacion"]:checked').val(); 
		if(confirmacion=="si"){
			$("input#primerNombre").css('display','none');
			$("#labelPrimernombre").css("display",'none');
			$("input#segundoNombre").css('display','none');
			$("#labelSegundonombre").css('display','none');
			$("input#primerApellido").css('display','none');
			$("#labelPrimerapellido").css('display','none');
			$("input#segundoApellido").css('display','none');
			$("#labelSegundoapellido").css('display','none');
			$("input#CorreoDoc").css('display','none');
			$("#divlistEspecialidad").css('display','block');
			$("#labelEspecialidad").css('display' ,'block');
			$("#divlistDoctor").css('display','block');
			$("#divNombreClinica").css('display','none');
			$("#labelClinica").css('display','none');
			$("#labelDoctor").css('display','block');
			$("#labelCorreo").css('display','none');
			$("#divCorreoDoc").css('display','none');			
		}
		else{
			$("input#primerNombre").css('display','block');
			$("#labelPrimernombre").css("display",'block');
			$("input#segundoNombre").css('display','block');
			$("#labelSegundonombre").css('display','block');
			$("input#primerApellido").css('display','block');
			$("#labelPrimerapellido").css('display','block');
			$("input#segundoApellido").css('display','block');
			$("#labelSegundoapellido").css('display','block');
			$("input#CorreoDoc").css('display','block');
			$("#divlistEspecialidad").css('display','block');
			$("#labelEspecialidad").css('display' ,'block');
			$("#divlistDoctor").css('display','none');
			$("#divNombreClinica").css('display','block');
			$("#labelClinica").css('display','block');
			$("#labelDoctor").css('display','none');
			$("#labelCorreo").css('display','block');
			$("#divCorreoDoc").css('display','block');			
		} 
	});

	$("#doctor").prop("disabled",true);
	
	$("#btnGuardar").click(function(){
		var msg="";
 		var Referencia =  new Object();
 		Referencia.Id 				= $("#id").val();
 		Referencia.idMedicoRecibe	= $("#listDoctores").val();
 		Referencia.idPaciente 		= $("#listPacientes").val();
 		Referencia.motivo 			= $("#motivoReferencia").val();
 		Referencia.primerNombre		= $("#primerNombre").val();
 		Referencia.segundoNombre	= $("#segundoNombre").val();
 		Referencia.primerApellido	= $("#primerApellido").val();
 		Referencia.segundoApellido	= $("#segundoApellido").val();
 		Referencia.correoDoc		= $("#correoDoc").val();
 		Referencia.confirmacion		= $("input[name='confirmacion']:checked").val();
 		Referencia.idEspecialidad 	= $("#listEspecialidad").val();
 		Referencia.nombreClinica 	= $("#nombreClinica").val();
 		console.log(Referencia);

 		/*if ($("input#idMedicoRecibe").val()!==undefined && $("input#idMedicoRecibe").val()!=='' ) {
 			Referencia.confirmacion="si";
 		} else {
 			Referencia.confirmacion="no";
 		}*/
 		if (Referencia.idMedicoRecibe===undefined  ) {
 			Referencia.idMedicoRecibe='';
 		}
 		if (Referencia.primerNombre ===undefined) {
	 			Referencia.primerNombre =''; 
 				Referencia.segundoNombre='';
 			 	Referencia.primerApellido='' ;
 			 	Referencia.segundoApellido='';
 			 	Referencia.correoDoc='';
 			 	Referencia.nombreClinica='';
 		}
 		$("#modal_nuevo").modal('hide');
		$('#mensaje').append("<div class='modal1'><div class='center1'><center><img src='"+ baseurl +"/img/gif-load.gif'>Guardando Informacion.....</center></div></div>");
		var DatosJson = JSON.stringify(Referencia); 
		$.post(baseurl+'clinica/referencia/save',{
			ReferenciaPost:DatosJson
		},function(data,textStatus){

			$("#mensaje").html(data.response_msg);

		},"json").fail(function(response,data){
			console.log('query'+data.error_msg)
			console.log('Error:'+response.responseText);

		});		
	});
	/*$("#bntGuardar").click(function(){
		alert("voy");
		var msg="";
 		var Referencia =  new Object();
 		Referencia.Id 				= $("#id").val();
 		Referencia.idMedicoRecibe	= $("#doctor").val();
 		Referencia.idPaciente 		= $("#paciente").val();
 		Referencia.motivo 			= $("#motivoReferencia").val();
 		Referencia.primerNombre		= $("#primerNombre").val();
 		Referencia.segundoNombre	= $("#segundoNombre").val();
 		Referencia.primerApellido	= $("#primerApellido").val();
 		Referencia.segundoApellido	= $("#segundoApellido").val();
 		Referencia.correoDoc		= $("#correoDoc").val();
 		Referencia.confirmacion		= $("input[name='confirmacion']:checked").val();
 		Referencia.idEspecialidad 	= $("#especialidad").val();
 		Referencia.nombreClinica 	= $("#nombreClinica").val();
 		
 		$("#modal_nuevo").modal('hide');
		  //$("#modal_nuevo").hide(); 
		$('#mensaje_registro').append("<div class='modal1'><div class='center1'><center><img src='"+ baseurl +"/img/gif-load.gif'>Guardando Informacion.....</center></div></div>");
		var DatosJson = JSON.stringify(Referencia);
		 
		$.post(baseurl+"clreferencia/guardarReferencia",
			{
				DatosReferencia: DatosJson
			},
			function(data,textStatus){
				//alert("voy");
				//alert(JSON.stringify(data));
				if (data.campo!=""){
					$("#modal_nuevo").modal("show");			
					$("#"+data.campo+"").focus();
				}
				$("#mensaje_registro").html(data.response_msg);
					
			},
			"json"
		).fail(function(response,data){
			console.log('query'+data.error_msg)
			console.log('Error:'+response.responseText);
		});;
		return false;
	});*/
	$("#listEspecialidad").change(function(){
		var idEspecialidad =  $(this).val();
		var Especialidad = new Object();
		Especialidad.id = idEspecialidad;
		console.log(Especialidad);

		if(idEspecialidad==""){
			$("#listDoctores").prop("disabled",true);
			$("#listDoctores").html("<option value=''>Seleccione un Medico</option>");
		}
		else{
			//$("#doctor").prop("disabled",false);
			var DatosJson = JSON.stringify(Especialidad);
			var html='';
			$.post(baseurl+"clinica/referencia/listarMedicoEspecialidad",
			{
				EspecialidadPost: DatosJson	
			},
			function(data,textStatus){
        	//$("#doctor").html(data.response_msg);
        	//console.log(data);
          	//console.log(data.valor);
          	 html+='<option value="0">Seleccione</option>';
          	array1=data.valor;
          	console.log(array1);
          	array1.forEach(function(element){
          		html+='<option value="'+element.ID_MEDICO+'">'+element.nombre_completo+'</option>';
          	});
          	$("#listDoctores").attr("disabled",false);
          	$("#listDoctores").html(html);
      		},
      		"json"
			).fail(function(response){
				console.log("Error "+response.responseText);
			});
			return false;
		}
	})
});