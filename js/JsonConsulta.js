$(document).ready(function(){
		$(document).on('click', '.btn-describe', function(event) {

			console.log("abrir");

			var div = $(this).parent();

			console.log(div);

			var desc = $(div).next();

			console.log(desc);

			$(desc).toggle("slow");

	});	

$("#datos_generales").focus();

    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {

        e.preventDefault();

        $(this).siblings('a.active').removeClass("active");

        $(this).addClass("active");

        var index = $(this).index();

        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");

        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");

    });
    
    $('.antechan').change(function(evt){
    	//alert(evt.target.name);
    	saveAntecedentes(evt.target);
    });

	function saveAntecedentes(ev)

	{
		var antecedentes=new Object();
		antecedentes.id=$("input#idAntecedente").val();
		antecedentes.datos_generales=$("textarea#general").val();
		antecedentes.hereditario=$("textarea#hereditario_familiar").val();
		antecedentes.personal_npatologico=$("textarea#personales_no_patologicos").val();
		antecedentes.personal_patologico=$("textarea#personales_patologicos").val();
		console.log(antecedentes);
		var DatosJson=JSON.stringify(antecedentes);
		$.post(baseurl + 'clinica/consulta/saveAntecedentes',
		{
					 AntecedentePost:DatosJson
		 },
		 function(data){	
			//alert(data.id)	
			ev.style="background:#99F894"			
				//alert(data.id)
				//$("#idconsulta").val(data.id_consulta);
				//$("#mensaje").html(data.error_msg);
			$("#mensaje").html(data.response_msg)	
		 },
		 "json"
		 ).fail(function(response){
			ev.style="background:#F49FC6"
			 //alert('Error::'+response.responseText);
			 console.log('Error::'+response.responseText);
		 });	
	}

	$('.menchan').change(function(evt){
		//alert(evt.target.name);
		saveMenstruacion(evt.target);
	});
	function saveMenstruacion(ev)
	{
		var menstruacion=new Object();
		menstruacion.id=$("input#idMenstruacion").val();
		menstruacion.primera_menstruacion=$("input#primera_menstruacion").val();
		menstruacion.ivsa=$("input#ivsa").val();
		menstruacion.menopausia=$("input#menopausia").val();
		menstruacion.caracteristicas=$("select#caracteristicas").val();
		menstruacion.otros=$("textarea#otros_menstruacion").val();
		console.log(menstruacion);
		var DatosJson=JSON.stringify(menstruacion);
		$.post(baseurl+'clinica/consulta/saveMenstruacion',{
			MenstruacionPost:DatosJson
		},function(data,textStatus){
			//alert(data.id)	
			ev.style="background:#99F894"			
				//alert(data.id)
				//$("#idconsulta").val(data.id_consulta);
				//$("#mensaje").html(data.error_msg);
			$("#mensaje").html(data.response_msg)	
		 },
		 "json"
		 ).fail(function(response){
			ev.style="background:#F49FC6"
			 //alert('Error::'+response.responseText);
			 console.log('Error::'+response.responseText);
		 });	

	}

	$('.embchan').change(function(evt){
		//alert(evt.target.name);
		saveEmbarazo(evt.target);
	});	

	function saveEmbarazo(ev)
	{
		var embarazo=new Object();
		embarazo.id=$("input#idEmbarazo").val();
		embarazo.total_embarazo=$("input#total_embarazo").val();
		embarazo.no_partos=$("input#no_partos").val();
		embarazo.no_cesareas=$("input#no_cesareas").val();
		embarazo.no_abortos=$("input#no_abortos").val();
		embarazo.nacidos_vivos=$("input#nacidos_vivos").val();
		embarazo.vivos_actuales=$("input#vivos_actuales").val();
		embarazo.otros=$("textarea#otros_embarazo").val();
		console.log(embarazo);
		var DatosJson=JSON.stringify(embarazo);
		$.post(baseurl+'clinica/consulta/saveEmbarazo',{
			EmbarazoPost:DatosJson
		},function(data,textStatus){
			//alert(data.id)	
			ev.style="background:#99F894"			
				//alert(data.id)
				//$("#idconsulta").val(data.id_consulta);
				//$("#mensaje").html(data.error_msg);
			$("#mensaje").html(data.response_msg)	
		 },
		 "json"
		 ).fail(function(response){
			ev.style="background:#F49FC6"
			 //alert('Error::'+response.responseText);
			 console.log('Error::'+response.responseText);
		 });	

	}
	$('.intechan').change(function(evt){
		//alert(evt.target.name);
		saveInteres(evt.target);
	});
	function saveInteres(ev)
	{
		var interes=new Object();
		interes.id=$("input#idInformacion").val();
		interes.ultima_papanicolau=$("input#fecha_ultima_papanicolau").val();
		interes.ultima_colposcopia=$("input#fecha_ultima_colposcopia").val();
		interes.ultima_mamografia=$("input#fecha_ultima_mamografia").val();
		interes.procedimientos_ginecologicos=$("textarea#procedimientos_ginecologicos").val();
		interes.parejas_sexuales=$("input#parejas_sexuales").val();
		interes.flujos_vaginales=$("textarea#flujos_vaginales").val();
		interes.metodos_anticonceptivos=$("textarea#metodos_anticonceptivos").val();
		interes.habitos=$("textarea#habitos").val();
		interes.cirujias_previas=$("textarea#cirujias_previas").val();
		interes.otros=$("textarea#otros_intereses").val();
		console.log(interes);
		var DatosJson=JSON.stringify(interes);
		$.post(baseurl+'clinica/consulta/saveIntereses',{
			InteresPost:DatosJson
		},function(data,textStatus){
			//alert(data.id)	
			ev.style="background:#99F894"			
				//alert(data.id)
				//$("#idconsulta").val(data.id_consulta);
				//$("#mensaje").html(data.error_msg);
			$("#mensaje").html(data.response_msg)	
		 },
		 "json"
		 ).fail(function(response){
			ev.style="background:#F49FC6"
			 //alert('Error::'+response.responseText);
			 console.log('Error::'+response.responseText);
		 });	
	}

	$('.conschan').change(function(evt){
		//alert(evt.target.name);
		saveConsulta(evt.target);
	});

	function saveConsulta(ev)
	{
		var consulta=new Object();
		consulta.id=$("input#idConsulta").val();
		consulta.sintoma=$("textarea#sintoma").val();
		consulta.observacion=$("textarea#observacion").val();
		consulta.tratamiento=$("textarea#tratamiento_previo").val();
		consulta.diagnostico=$("textarea#diagnostico").val();
		consulta.enfermedad=$("select#diagnosticolistEnf").val();
		consulta.id_cita=$("input#idCita").val();
		consulta.indicaciones=$("textarea#indicaciones_consulta").val();
		console.log(consulta);
		var DatosJson=JSON.stringify(consulta);
		$.post(baseurl+'clinica/consulta/saveDiagnostico',{
			ConsultaPost:DatosJson
		},function(data,textStatus){
			//alert(data.id)	
			ev.style="background:#99F894"			
				//alert(data.id)
				//$("#idconsulta").val(data.id_consulta);
				//$("#mensaje").html(data.error_msg);
			$("#mensaje").html(data.response_msg)	
		 },
		 "json"
		 ).fail(function(response){
			ev.style="background:#F49FC6"
			 //alert('Error::'+response.responseText);
			 console.log('Error::'+response.responseText);
		 });
	}

	$('.explochan').change(function(evt){
		//alert(evt.target.name);
		var imc_c=parseFloat($("input#peso").val()/Math.pow($("input#talla").val(),2));
		console.log(imc_c);
		var imc_t=imc_c.toFixed(2);
		console.log(imc_t);
		$("input#imc").val(imc_t);
		saveExploracion(evt.target);	
	});

	function saveExploracion(ev)
	{
		var exploracion=new Object();
		exploracion.id=$("input#idExploracion").val();
		exploracion.peso=$("input#peso").val();
		exploracion.talla=$("input#talla").val();
		exploracion.cadera=$("input#cadera").val();
		exploracion.cintura=$("input#cintura").val();
		exploracion.imc=$("input#imc").val();
		exploracion.temperatura=$("input#tamperatura").val();
		exploracion.presion_arterial=$("input#presion_arterial").val();
		exploracion.frecuencia_cardiaca=$("input#frecuencia_cardiaca").val();
		exploracion.frecuencia_respiratoria=$("input#frecuencia_respiratoria").val();
		exploracion.cintura_abdominal=$("input#cintura_abdominal").val();
		exploracion.pliegue_abdominal=$("input#pliegue_abdominal").val();
		exploracion.grasa_corporal=$("input#grasa_corporal").val();
		exploracion.fuerza_mano=$("input#fuerza_mano").val();
		exploracion.trigliceridos=$("input#trigliceridos").val();
		exploracion.colesterol=$("input#colesterol").val();
		exploracion.hdl=$("input#hdl").val();
		exploracion.ldl=$("input#ldl").val();
		exploracion.creatinina=$("input#creatinina").val();
		exploracion.acido_urico=$("input#acido_urico").val();
		exploracion.hemoglobina=$("input#hemoglobina").val();
		exploracion.cabeza=$("textarea#cabeza").val();
		exploracion.cuello=$("textarea#cuello").val();
		exploracion.torax=$("textarea#torax").val();
		exploracion.abdomen=$("textarea#abdomen").val();
		exploracion.pulso=$("textarea#pulso").val();
		exploracion.facies=$("textarea#facies").val();
		exploracion.miembros_inferiores=$("textarea#miembros_inferiores").val();
		exploracion.pie=$("textarea#pie").val();
		exploracion.genitales=$("textarea#genitales").val();
		exploracion.otros=$("textarea#otro_exploracion").val();
		exploracion.glucosa_laboratorio=$("input#glucosa_laboratorio").val();
		exploracion.glucosa_capilar=$("input#glucosa_capilar").val();
		exploracion.glicosalida=$("input#glicosalida").val();
		exploracion.transaminasa=$("input#transaminasa").val();
		exploracion.observaciones_glucosa=$("textarea#observaciones_glucosa").val();
		console.log(exploracion);
		var DatosJson=JSON.stringify(exploracion);
		$.post(baseurl+'clinica/consulta/saveExploracion',{
			ExploracionPost:DatosJson
		},function(data,textStatus){
			//alert(data.id)	
			ev.style="background:#99F894"			
				//alert(data.id)
				//$("#idconsulta").val(data.id_consulta);
				//$("#mensaje").html(data.error_msg);
			$("#mensaje").html(data.response_msg)	
		 },
		 "json"
		 ).fail(function(response){
			ev.style="background:#F49FC6"
			 //alert('Error::'+response.responseText);
			 console.log('Error::'+response.responseText);
		 });	

	}

	$("#listEnfermedades").change(function(){
		var enfermedad=new Object();
		enfermedad.id=$(this).val();
		enfermedad.id_paciente=$("input#idPaciente").val();
		console.log(enfermedad);
		var DatosJson=JSON.stringify(enfermedad);
		$.post(baseurl+'clinica/consulta/saveEnfermedadPaciente',{
			EnfermedadPost:DatosJson
		},function(data,textStatus){
			$("#mensaje").html(data.response_msg);
			var html='';
			html+='<option value='+$(this).val()+'>'+$(this).text()+'</option>';
			$("#enfermedades").html(html);
		},"json").fail(function(response){
			$("select#enfermedades").css('background-color','#99F894');
			console.log('Error::'+response.responseText);
		});;
	});
	$("#diagnosticolistEnf").change(function(){
		var consulta=new Object();
		consulta.id=$("input#idConsulta").val();
		consulta.enfermedad=$("select#diagnosticolistEnf").val();
		consulta.sintoma=$("textarea#sintoma").val();
		consulta.observacion=$("textarea#observacion").val();
		consulta.tratamiento=$("textarea#tratamiento_previo").val();
		consulta.diagnostico=$("textarea#diagnostico").val();	
		consulta.id_cita=$("input#idCita").val();	
		console.log(consulta);
		var DatosJson=JSON.stringify(consulta);
		$.post(baseurl+'clinica/consulta/saveDiagnostico',{
			ConsultaPost:DatosJson
		},function(data,textStatus){
			$("#mensaje").html(data.response_msg);
			$("#select_enfermedad_diagnostico").css("background-color","#99F894");
		},"json").fail(function(response){
			$("#select_enfermedad_diagnostico").css("background-color","#F49FC6");
			console.log('Error::'+response.responseText);			
		});
	});

	$("#listMedicamento").change(function(){
		$("input#presentacion").val($(this).find("option:selected").text());
		$("input#dosis").attr("disabled",false);
		$("input#indicaciones").attr("disabled",false);
		$("input#cantidad_receta").attr("disabled",false);
	});
	$("input#radio_alergia").click(function(){
		$(this).is("checked",true);
		$("select#listAlergias").attr("disabled",false);
		$("select#listAlergias").val('').trigger('change');
		$("select#listOtrasAlergias").attr("disabled",true);
		$("select#listOtrasAlergias").val('').trigger('change');
		$("input#otra_alergia").attr("disabled",true);
	});

	$("input#radio_alergia_sustancia").click(function(){
		$(this).is("checked",true);
		$("select#listAlergias").attr("disabled",true);
		$("select#listAlergias").val('').trigger('change');
		$("select#listOtrasAlergias").attr("disabled",false);
		$("input#otra_alergia").attr("disabled",true);
		$("input#otra_alergia").val('');
	});	

	$("input#radio_otras_alergias").click(function(){
		$(this).is("checked",true);
		$("select#listAlergias").attr("disabled",true);
		$("select#listOtrasAlergias").attr("disabled",true);
		$("select#listAlergias").val('').trigger('change');
		$("select#listOtrasAlergias").val('').trigger('change');
		$("input#otra_alergia").attr("disabled",false);
	});	

	$("#btnAlergia").click(function(){
		var alergia=new Object();
		var html='';
		alergia.alergia_id=$("select#listAlergias").val();
		alergia.sustancia_id=$("select#listOtrasAlergias").val();
		alergia.otra_alergia=$("input#otra_alergia").val();
		alergia.id_paciente=$("input#idPaciente").val();
		console.log(alergia);
		var DatosJson=JSON.stringify(alergia);
		$.post(baseurl+'clinica/consulta/saveAlergiaPaciente',{
			AlergiaPost:DatosJson
		},function(data,textStatus){
			if (alergia.alergia_id!='') {
				html+='<option>'+$("select#listAlergias option:selected").text()+'</option>';
			} else if(alergia.sustancia_id!='') {
				html+='<option>'+$("select#listOtrasAlergias option:selected").text()+'</option>';
			} else {
				html+='<option>'+alergia.otra_alergia+'</option>';
			}
			console.log(html);
			$("#mensaje").html(data.response_msg);
			$("#listaAlergiasPaciente").append(html);
		}).fail(function(response){
			$("#listAlergiasPaciente").css("background:","#F49FC6");
			console.log('Error::'+response.responseText);
		});
	});

	$("#btnReceta").click(function(){
		var receta=new Object();
		receta.id_medicamento=$("select#listMedicamento").val();
		receta.dosis=$("input#dosis").val();
		receta.indicaciones=$("input#indicaciones").val();
		receta.id_consulta=$("input#idConsulta").val();
		receta.cantidad=$("input#cantidad_receta").val();
		var html='';
		var medicamento=$("select#listMedicamento option:selected").text().split("-");
		console.log("medicamento "+medicamento);
		console.log(receta);
		var DatosJson=JSON.stringify(receta);
		$.post(baseurl+'clinica/consulta/saveReceta',{
			RecetaPost: DatosJson
		},function(data,textStatus){
			console.log(data.receta_id);
			if (data.receta_id!==null && data.receta_id!== "") {
				html+="<tr>";
				html+="<td>"+medicamento[0]+"</td>";
				html+="<td>"+medicamento[1]+"</td>";
				html+="<td>"+receta.dosis+"</td>";
				html+="<td>"+receta.indicaciones+"</td>";
				html+="<td>"+receta.cantidad+"</td>";
				html+="<td style='display:none;' class='Idreceta'>"+data.receta_id+"</td>";
				html+="<td><button type='button'  title='Eliminar Medicamento' class='btn btn-danger btn-xs EliminarMedicamento'><span class='glyphicon glyphicon-remove'></span></td>";
				html+="</tr>";

			}
			$("input#presentacion").val('');
			$("input#dosis").val('');
			$("input#indicaciones").val('');
			$("input#cantidad_receta").val('');
			$("input#dosis").attr("disabled",true);
			$("input#indicaciones").attr("disabled",true);
			$("input#cantidad_receta").attr("disabled",true);			
			$("#recetaTablabody").append(html);
			$("#mensaje").html(data.response_msg);

		},"json").fail(function(response){
			$("input#dosis").css("background:","#F49FC6");
			$("input#indicaciones").css("background:","#F49FC6");
			$("input#presentacion").css("background:","#F49FC6");
			console.log('Error::'+response.responseText);
		});
	});
	$(document).on('click','.EliminarMedicamento',function(){
		var receta =new Object();
		receta.id=$(this).closest("tr").find(".Idreceta").text();
		console.log(receta);
		var DatosJson=JSON.stringify(receta);
		$.post(baseurl+'clinica/consulta/deleteReceta',{
			RecetaPost:DatosJson
		},function(data,textStatus){
			$("#mensaje").html(data.response_msg);
		},"json").fail(function(response){
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);
		});
        $(this).closest("tr").remove();		
	});

	$(document).on('click','.btnModificar',function(){
		var idEstudio=$(this).closest("tr").find('.Idestudio').text();
		var estudio=new Object();
		console.log(idEstudio);
		estudio.id=$(this).closest("tr").find(".Iddetalle").text();
		estudio.respuesta=$("textarea#estudio_"+idEstudio).val();
		console.log(estudio);
		var DatosJson=JSON.stringify(estudio);
		$.post(baseurl+'clinica/consulta/updateEstudio',{
			EstudioPost:DatosJson
		},function(data,textStatus){
			$("textarea#estudio_"+idEstudio).css("background-color",'#99F894');
			$("#mensaje").html(data.response_msg);
		},"json").fail(function(response){
			$("textarea#estudio_"+idEstudio).css("background:","#F49FC6");
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);

		});
	});
	$(document).on('click','.btnEliminar',function(){

	});
	$("#btnGuardarExamen").click(function(){
		var estudio=new Object();
		var html='';
		estudio.id=$("select#listEstudios").val();
		estudio.cita=$("input#idCita").val();
		console.log(estudio);
		var DatosJson=JSON.stringify(estudio);
		$.post(baseurl+'clinica/consulta/addEstudio',{
			EstudioPost:DatosJson
		},function(data,textStatus){
			html+='<tr>';
			html+='<td>'+$("select#listEstudios option:selected").text()+'</td>';
			html+='<td>SOLICITADO</td>';
			html+='<td>'+estudio.id+'</td>';
			html+='<td><textarea type="text" name="estudio" rows="3" class="form-control estchan" id="estudio_"'+estudio.id+'></textarea></td>';
			html+="<td style='display:none;' class='Iddetalle'>"+data.detalle_id+"</td>"
			html+="<td><button type='button'  title='Eliminar Estudio' class='btn btn-danger btn-xs btnEliminar'><span class='glyphicon glyphicon-remove'></span></button> <buton type='button' title='Guardar Lectura' class='btn btn-primary btn-xs btnModificar'><span class='glyphicon glyphicon-saved'></span></button></td>";
			html+='</tr>';
			$("#mensaje").html(data.response_msg);
			$("#tablebodyEstudios").append(html);
		},"json").fail(function(response){
		    $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);	
		});
	});
});


