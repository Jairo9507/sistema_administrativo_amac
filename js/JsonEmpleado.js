$(document).ready(function(){
	$("#primer_nombre").focus();

	$("#btnGuardar").click(function(){
		var empleado=new Object();
		empleado.id=$("input#id").val();
		empleado.primer_nombre=$("input#primer_nombre").val();
		empleado.segundo_nombre=$("input#segundo_nombre").val();
		empleado.primer_apellido=$("input#primer_apellido").val();
		empleado.segundo_apellido=$("input#segundo_apellido").val();
		empleado.apellido_casada=$("input#apellido_casada").val();
		empleado.genero=$("input[name='genero']:checked").val();
		empleado.estado=$("select#estado").val();
		empleado.municipio=$("select#municipio_id").val();
		empleado.unidad=$("select#unidad_id").val();
		empleado.codigo=$("input#codigo_empleado").val();
		console.log(empleado);
		var DatosJson=JSON.stringify(empleado);
		$("#modal_nuevo").modal('hide');						
		$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");			
		$.post(baseurl+'admin/empleado/save',{
			EmpleadoPost:DatosJson
		},function(data,textStatus){
			console.log(data);
			$("#mensaje").html(data.response_msg);
		},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    					console.log('Error: ' + response.responseText);
		});
	});
});