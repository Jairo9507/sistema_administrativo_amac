$(document).ready(function(){
	$("#codigo_unidad").focus();

	$("#btnGuardar").click(function(){
		var unidad=new Object();
		unidad.id=$("input#id").val();
		unidad.codigo=$("input#codigo_unidad").val();
		unidad.descripcion=$("textarea#descripcion").val();
		unidad.dependencia=$("select#unidad_id").val();
		console.log(unidad);
		$("#modal_nuevo").modal('hide');						
		$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");		
		var DatosJson=JSON.stringify(unidad);
		$.post(baseurl+'admin/unidad_administrativa/save',{
			UnidadPost:DatosJson
		},function(data,textStatus){
			$("#mensaje").html(data.response_msg);
		},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    					console.log('Error: ' + response.responseText);
		});
	});
});