<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="content-title">Lista de Modelos de vehículos</h3>
			</div>
<!-- 			<div class="row">

				<div class="col-sm-2 pull-right">
					<a href="cat_actividad/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>
				</div>
			</div> -->
			<br>
			<div class="row">
				<div class="col-md-8" style="width: 65%;left: 15px;">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Lista de modelos</h3>
						</div>
						<table id="modelo" class="table table-bordered table-striped">
							<thead>
								<th>Codigo</th>
								<th>Modelo de vehículo</th>
								<th>Marca</th>
								<th>Estado</th>
								<th></th>
							</thead>
							<tbody>
								<?php 
								if ($modelo) {
									foreach ($modelo as $mo) {
										$idmodelo= base64_encode($mo->ID_VEHICULO_MODELO);
										echo "<tr>";
										echo "<td>".$mo->ID_VEHICULO_MODELO."</td>";
										echo "<td>".$mo->DESMODELO."</td>";
										echo "<td>".$mo->DESMARCA."</td>";
										if ($mo->ESTADO==1) {
											echo "<td>ACTIVO</td>";
										} 
										echo "<td><a href='cat_modelo_vehiculo/editar/".$idmodelo."' ><button type='button' title='Editar Modelo' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
										?>
										<button type="button"  title="Eliminar modelo de vehiculo" onclick="eliminarModeloVehiculo('<?php echo $idmodelo?>','<?php echo $mo->DESMODELO?>', '<?php echo $mo->DESMARCA?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
										<?php 
										echo "</td";
										echo "</tr>";
									}
								}

								?>
							</tbody>
						</table>
					</div>	
				</div>
				<div class="col-md-4">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Agregar Modelo de Vehículo</h3>
						</div>
						<form role="form">
							<div class="box-body">
								<div class="form-group">
									<br>
									<div id="mensaje"></div>
									<label>Clase de vehículo</label>
									<select class="form-control js-example-basic-single" id="marca" style="width: 100%;">
										<?php if ($marca){
											echo "<option value='0'>Seleccione una opción</option>";
											foreach ($marca as $ma) {
												echo "<option value='".$ma->ID_VEHICULO_MARCA."'>".$ma->DESCRIPCION."</option>";
											}
										}

										?>
									</select>
									<br><br>
									<label>Modelo de vehículo</label>
									<textarea id="descripcion" name="descripcion" class="form-control" rows="3" placeholder="Ingrese nuevo modelo de vehículo..."></textarea>
								</div>
							</div>
						</form>
						<br>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
						</div>

					</div>	
				</div>
			</div>
		</div>
	</section>
</div>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>

<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	$(document).ready(function() {
		$("#marca").select2();
		$("#modelo").DataTable({
			paging      : true,
			pageLength  : 5,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			"order" : [],
			info        : true,
			autoWidth   : false,
			language: {
				search:'Buscar:',
				order: 'Mostrar Entradas',
				paginate: {
					first:"Primero",
					previous:"Anterior",
					next:"Siguiente",
					last:"Ultimo"
				},
				emptyTable: "No hay informacion disponible",
				infoEmpty: "Mostrando 0 de 0 de 0 entradas",
				lengthMenu:"Mostrar _MENU_ Entradas",
				info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
			} 
		});

		$("#btnGuardar").click(function() {
			var Modelo = new Object();
			Modelo.id = '';
			Modelo.descripcion = $("textarea#descripcion").val();
			Modelo.id_vehiculo_marca = $("select#marca").val();
			var des = $("textarea#descripcion").val();
			var marca = $("select#marca").val();
			console.log(Modelo);
			var DatosJson = JSON.stringify(Modelo);
			if (des == '' && marca == 0 ) {
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Descripcion es obligatoria</div>");
			}else{
				
				console.log(DatosJson);
				$.post(baseurl+"catalogos/cat_modelo_vehiculo/guardar",{
					ModeloVehiculoPost : DatosJson
				}, function(data, textStatus) {
					$("#mensaje").html(data.response_msg);
					
					if ($("#descripcion").val("") != '' && $("select#clase").val() !=0) {
				// $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
				location.reload();
			}
		}, "json").fail(function(response) {
			$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
			console.log('Error: ' + response.responseText);
		});;
	}
});
	});

	function eliminarModeloVehiculo(idmodelo,nombre,marca)
	{
		alertify.confirm("Eliminar modelo de vehículo","¿Seguro de eliminar el modelo "+nombre+"?",function(){
			document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
			var Modelo =new Object();
			Modelo.id =idmodelo;
			var DatosJson=JSON.stringify(Modelo);
			$.post(baseurl+'/catalogos/cat_modelo_vehiculo/eliminar',{
				ModeloVehiculoPost: DatosJson
			},function(data,textStatus){
				console.log(data);
				$("#mensaje").html(data.response_msg);
				location.reload();             	
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
				console.log('Error: ' + response.responseText);
			});;
			
		},function(){

		});
	}

</script>