<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 

	$id_presupuesto= array(
		'name' 		=> 'id_presupuesto',
		'id'		=> 'id_presupuesto',
		'value'		=> set_value('id_presupuesto',@$presupuesto[0]->ID_PRESUPUESTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '',
		'disabled'	=> '',
		'style'		 => 'width:55%'	

	);

	$unidad =array(
		'name' 		=> 'unidad',
		'id'		=> 'unidad',
		'value'		=> set_value('unidad',@$presupuesto[0]->ID_UNIDAD),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '',
		'style'		 => 'width:55%'	
	);

	$cuenta = array(
		'name'  	  =>  'cuenta',
		'id'    	  =>  'cuenta',
		'value' 	  =>   set_value('cuenta', @$cuenta[0]->NOMBRE_CUENTA),
		'type'  	  =>  'text',
		'class' 	  =>  'form-control',
		'placeholder' => '',
		'disabled'    =>'',
		'style'		  => 'width:55%'	
	);
    $total = array(
    'name'      =>  'total',
    'id'        =>  'total',
    'value'     =>   set_value('total', @$total[0]->TOTAL),
    'type'      =>  'text',
    'class'     =>  'form-control',
    'placeholder' => '',
    'disabled'    =>'',
    'style'     => 'width:55%'  
  );


 ?>
 <input type="hidden" name="id" id="cod_orden" value="<?php echo @$presupuesto[0]->ID_PRESUPUESTO?>">
  <div class="content-wrapper">
 	<section class="content-header">
 		<?php echo $pagetitle; ?>
 		<?php echo $breadcrumb; ?>
 	</section>
 		<div class="box">
 			<div class="box-header">
 		<h3 class="header-title"></h3>
 			</div>
 			<div class="box-body">
 				<div id="mensaje"></div>
 				<form class="form-horizontal" name="formulario" id="formulario" role="form">
 					<div class="form-group">
 						<label for="id_presupuesto" class="col-sm-2 form-label" style="text-align: center; top: 12px;">Presupuesto</label>
 						<div class="col-sm-4" style="height: 40px;">
 							<?php  echo form_input($id_presupuesto);?>
 						</div>
 						<label for="numFactura" class="col-sm-2 form-label" style="text-align: center;top: 12px;">Unidad administrativa</label>
 						<div id="unidad" class="col-sm-4" style="height: 50px;">
 							<?php  echo form_input($unidad);?>
 						</div> 
              <label for="total" class="col-sm-2 form-label" style="text-align: center;top: 12px;">TOTAL PRESUPUESTO</label>
            <div id="total" class="col-sm-4">
              <?php  echo form_input($total);?>
            </div> 
 						<label for="codigo" class="col-sm-2 form-label" style="text-align: center;top: 29px;">Cuenta</label>
 						<div class="col-sm-4">
 							<h3><?php  echo form_input($cuenta);?></h3>
 						</div>						
 					</div>
 					<div class="form-group">
 						<div class="col-sm-3 pull-right">
 							<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal" id="btnAgregarProducto" style="position: relative;right: 200px; bottom: 8px;">
             				<span class="glyphicon glyphicon-plus"></span>Agregar productos</button>
 						</div>
 					</div>
 				</form>
				<div >
<div id="tablaProductos" class="col-sm-12" ">
<table id="productos_editar" class="table table-bordered table-striped"  style="width: 99%;position: initial;left: 35px;bottom: 77px;">
      <thead>
        <tr>
          <th style="width: 15%;">Codigo</th>
          <th style="width: 50%;">Nombre</th>
          <th style="width: 15%;">Cantidad</th>
          <th style="width: 15%;">Precio</th>
          <th style="width: 15%;">Total</th>
          <th></th>
        </tr>
      </thead>
      <tbody id="tablaProductosbody">
      	<?php 
            if ($detalles) {
                foreach ($detalles as $d) {
                    echo "<tr>";
                    echo "<td>".$d->ID_PRODUCTO."</td>";
                    echo "<td>".$d->NOMBRE_PRODUCTO."</td>";
                    echo "<td class='col-xs-1'> <input type='text' class='form-control' style='text-align:right' id='cantidad_".$d->ID_PRODUCTO."'value='".$d->CANTIDAD."' ></td>";
                    echo "<td class='col-xs-1'> <input type='text' class='form-control' style='text-align:right' id='precio_venta_".$d->ID_PRODUCTO."'value='".$d->PRECIO_UNITARIO."' ></td>";
                    echo "<td>".$d->COSTO_APROXIMADO."</td>";
                    echo "";

         ?>
                    <td><span class='pull-right'><a href='#' onclick='modificar("<?php echo $d->NOMBRE_PRODUCTO?>","<?php echo $d->ID_DETALLE?>","<?php echo $d->ID_PRODUCTO?>")'><i class='glyphicon glyphicon-pencil'></i></a></span></td>
         <?php 

                    echo "</tr>";                                                
                }
            }

          ?>
      </tbody>
</table> 


					 </div>	
				</div> 				
 			</div>
 			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"  onclick="salir()"><span class="glyphicon glyphicon-saved" ></span> Guardar</button> 				
 			</div>
 		</div>

 	          <!-- Modal -->
      <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Buscar productos</h4>
          </div>
          <div class="modal-body">
            <div id="mensaje_productos"></div>
   <!--   <form class="form-horizontal">
            <div class="form-group">
            <div class="col-sm-6">
              <input type="text" class="form-control" id="q" placeholder="Buscar productos" onkeyup="load(1)">
              <input type="hidden" name="prueba" id="prueba">
            </div>
            <button type="button" class="btn btn-default" onclick="load(1)"><span class='glyphicon glyphicon-search'></span> Buscar</button>
            </div>
          </form> -->
          <div id="loader" style="position: absolute; text-align: center; top: 55px;  width: 100%;display:none;"></div><!-- Carga gif animado -->
      <div class="outer_div" >
      <div class="box-body">
    <div id="mensaje"></div>
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <th>Cod. Producto</th>
        <th>Nombre</th>
        <th>Cantidad</th>
        <th>Precio</th>
        <th style="width: 36px;"></th>
        <tbody>
          <?php if ($productos) {
            foreach ($productos as $prod) {
              echo   "<tr>";
              echo "<td>".$prod->ID_PRODUCTO."</td>";
              echo "<td>".$prod->NOMBRE_PRODUCTO."</td>";
              echo "<td class='col-xs-1'>
                    <div class='pull-right'>
                    <input type='text' class='form-control' style='text-align:right' id='cantidad_".$prod->ID_PRODUCTO."'  value='1' >
                    </div></td>";
              echo "<td class='col-xs-2'><div class='pull-right'>
                    <input type='text' class='form-control' style='text-align:right' id='precio_venta_".$prod->ID_PRODUCTO."'  value='100'>
                    </div></td>";

              echo "<td><span class='pull-right'><a href='#' onclick='agregar(\"".$prod->ID_PRODUCTO."\",\"".$prod->NOMBRE_PRODUCTO."\")'><i class='glyphicon glyphicon-plus'></i></a></span></td>";
              echo "</tr>";
            }
          } ?>
        </tbody>
      </table>
    </div>
          </div><!-- Datos ajax Final -->
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload()">Cerrar</button>
          
          </div>
        </div>
        </div>
      </div>
 </div>

<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
var base_url = "<?php echo base_url();?>";

$(document).ready(function() {
	$('#productos_editar').DataTable({
		pageLength:5,
		"order" : [],
		language: {
			search:'Buscar:',
			order: 'Mostrar Entradas',
			paginate: {
				first:"Primero",
				previous:"Anterior",
				next:"Siguiente",
				last:"Ultimo"
			},
			emptyTable: "No hay informacion disponible",
			infoEmpty: "Mostrando 0 de 0 de 0 entradas",
			lengthMenu:"Mostrar _MENU_ Entradas",
			info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
		}            
	});

	$('#example1').DataTable(
	{
		pageLength:5,
      "order" : [],
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }
	});
	 $("#btnAgregarProducto").click(function() {
	 	var ordenCompra = new Object();
	 	ordenCompra.id='';
	 	ordenCompra.codOrden = $("input#codigo_orden").val();
	 	ordenCompra.numFactura = $("input#num_factura").val();      
  });
	
  });

	function modificar(nombre, iddetalle, id_producto) {
		
		var html = '';
        var encabezado = '';
        var cantidad = $("#cantidad_"+id_producto).val();
        var precio = $("#precio_venta_"+id_producto).val();
        console.log(cantidad);
        console.log(precio);

        if (isNaN(cantidad)) {
            alert('Esto no es un numero');
            document.getElementById('cantidad_'+id_producto).focus();
            return false;
        }else if (isNaN(precio)) {
        	alert('Esto no es un numero');
            document.getElementById('cantidad_'+id_producto).focus();
            return false;
        }
        var detalle = new Object();
        detalle.codigoOrden=$("input#cod_orden").val();
        detalle.id_producto=id_producto;
        detalle.cantidad=cantidad;
        detalle.precio=precio;
        detalle.id_detalle=iddetalle;
        var DatosJson=JSON.stringify(detalle);
        console.log(DatosJson);
        $.post(base_url+'compra/ordencompra/agregarPedido',{
            DetallePost:DatosJson
        },function(data,textStatus){
            console.log(data);
            $("#mensaje").append(data.response_msg);
        },"json").fail(function(response){
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);
        });;
	}

	function agregar(id, nombre) {
    var html = '';
    var encabezado = '';
    var cuenta     = $("input#cuenta").val();
    var precio     = $("#precio_venta_"+id).val();
    var cantidad   = $("#cantidad_"+id).val();
    var detalle    = new Object();

    //Validación de cantidad y precio
    if (isNaN(cantidad)) {
      alert('Esto no es un numero');
      document.getElementById('cantidad'+id).focus();
      return false;
    }
    if (isNaN(precio)) {
      alert('Esto no es un numero');
      document.getElementById('precio_venta'+id).focus();
      return false;
    }
    //Fin de validación
    detalle.id_producto=id;
    detalle.cantidad = cantidad;
    detalle.precio=precio;
    detalle.cuenta = cuenta;
    detalle.id_detalle='';
    detalle.id_presupuesto = $("input#id_presupuesto").val();
    console.log(detalle);
    var DatosJson = JSON.stringify(detalle);
    console.log(DatosJson);
    $.post(base_url+"compra/presupuesto/agregarProducto",{
      DetallePost:DatosJson
    }, function(data, textStatus) {
      console.log("data : "+data);
      $("#mensaje_productos").append(data.response_msg);
       $("#respuesta").show();
    $("#save").show();
    html=
    "<table class='table table-bordered table-striped' style='width: 99%;position: relative;left: 35px;bottom: 77px;'>"+
      "<tbody>"+
        "<tr>"+
          "<th style='width:15%'>"+id+"</th>"+
          "<th style='width:50%'>"+nombre+"</th>"+
          "<th style='width:15%'>"+cantidad+"</th>"+
          "<th stye='width:15%'>"+precio+"</th>"+
        "</tr>"+
      "</tbody>"+
    "<table>";
      $("#respuesta").removeAttr('disabled');
      $("#btnGuardar").attr("disabled", false);
      $("#respuesta").append(html);
    }, "json").fail(function(response) {
        $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
        console.log('Error: ' + response.responseText);
    });
  }

    function salir()
    {
      alertify.confirm("¿Esta seguro de guardar la Orden de Compra?","Click en ACEPTAR para guardar",function(){
          window.location.href = base_url+"compra/detalle_ordencompra"

      },function(){

      }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }
       function regresar()
    {
      alertify.confirm("¿Desea Regresar?","Click en ACEPTAR para regresar",function(){
          window.location.href = base_url+"compra/detalle_ordencompra"

      },function(){

      }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }


</script>


