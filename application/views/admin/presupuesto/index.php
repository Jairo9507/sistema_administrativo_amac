<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content-wrapper">
 <section class="content-header">
  <?php echo $pagetitle; ?>
  <?php echo $breadcrumb; ?>
</section>

     <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Presupuesto</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div id="mensaje"></div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <form method="POST" name="formulario" id="formulario"  >
                <label>Cuentas Presupuestarias</label>
                <select class="form-control js-example-basic-single" id="cuenta" style="width: 100%;">
                  <?php if ($cuentas){
                    foreach ($cuentas as $cp) {
                      echo "<option value='".$cp->ID."'>".$cp->NOMBRE_CUENTA."</option>";
                    }
                  }
                    
                   ?>
                </select>
              </div>
            </div>
                        <!-- /.col -->
            <div class="col-md-4">
              <div class="form-group">
                <label>Unidad Administrativa</label>
                    <select class="form-control js-example-basic-single" id="unidad" style="width: 100%;">
                  <?php if ($unidades){
                    foreach ($unidades as $u) {
                      echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD."</option>";
                    }
                  }
                   ?>
                </select>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-4">
              <div class="form-group">
                <label>Fecha registro(*)</label>
                  <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="date" class="form-control" id="fecha">
                </div>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div>
     
        <!-- /.box-body -->
          <div class="box-footer">
            <input type="hidden" name="idproveedor" id="idproveedor">
            <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal" id="btnAgregarProducto">
             <span class="glyphicon glyphicon-plus"></span>Agregar productos</button>
          </div>
        </form>
        <br><br>
          <div id="respuesta"  class="box-body" disabled="" style="display: none;width: 80%"> 
            <table class="table table-bordered table-striped"  style="width: 99%;position: relative;left: 35px;bottom: 77px;">
              <thead>
                <tr>
                  <th style="width: 15%;">Codigo</th>
                  <th style="width: 50%;">Nombre</th>
                  <th style="width: 15%;">Cantidad</th>
                  <th>Precio</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>           
          </div>
        <!--   <div><h3 style="text-align: center; position: relative;bottom: 95px;width: 248px;left: 50%;">TOTAL:</h3></div> -->
          <div class="box-footer">
          <button type="submit" class="btn btn-primary pull-right" id="btnGuardar" disabled="" onclick="salir()"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>        
          </div>
      </div>
      <!-- /.box -->
    </section>


          <!-- Modal -->
      <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Buscar productos</h4>
          </div>
          <div class="modal-body">
            <div id="mensaje_productos"></div>
   <!--   <form class="form-horizontal">
            <div class="form-group">
            <div class="col-sm-6">
              <input type="text" class="form-control" id="q" placeholder="Buscar productos" onkeyup="load(1)">
              <input type="hidden" name="prueba" id="prueba">
            </div>
            <button type="button" class="btn btn-default" onclick="load(1)"><span class='glyphicon glyphicon-search'></span> Buscar</button>
            </div>
          </form> -->
          <div id="loader" style="position: absolute; text-align: center; top: 55px;  width: 100%;display:none;"></div><!-- Carga gif animado -->
      <div class="outer_div" >
      <div class="box-body">
    <div id="mensaje"></div>
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <th>Cod. Producto</th>
        <th>Nombre</th>
        <th>Cantidad</th>
        <th>Precio</th>
        <th style="width: 36px;"></th>
        <tbody>
          <?php if ($productos) {
            foreach ($productos as $prod) {
              echo   "<tr>";
              echo "<td>".$prod->ID_PRODUCTO."</td>";
              echo "<td>".$prod->NOMBRE_PRODUCTO."</td>";
              echo "<td class='col-xs-1'>
                    <div class='pull-right'>
                    <input type='text' class='form-control' style='text-align:right' id='cantidad_".$prod->ID_PRODUCTO."'  value='1' >
                    </div></td>";
              echo "<td class='col-xs-2'><div class='pull-right'>
                    <input type='text' class='form-control' style='text-align:right' id='precio_venta_".$prod->ID_PRODUCTO."'  value='100'>
                    </div></td>";
              echo "<td><span class='pull-right'><a href='#' onclick='agregar(\"".$prod->ID_PRODUCTO."\",\"".$prod->NOMBRE_PRODUCTO."\")'><i class='glyphicon glyphicon-plus'></i></a></span></td>";
              echo "</tr>";
            }
          } ?>
        </tbody>
      </table>
    </div>
          </div><!-- Datos ajax Final -->
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          
          </div>
        </div>
        </div>
      </div>
</div>

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
      $("#cuenta").select2();
      $("#producto").select2();
      $("#unidad").select2();

    $('#example1').DataTable({
      pageLength:5,
      "order" : [],
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }            
      
    })
  });
  var base_url = "<?php echo base_url();?>";
  function agregar(id, nombre) {
    var html = '';
    var encabezado = '';
    var cuenta = $("select#cuenta").val();
    var precio  = $("#precio_venta_"+id).val();
    var cantidad = $("#cantidad_"+id).val();
    var detalle = new Object();

    //Validación de cantidad y precio
    if (isNaN(cantidad)) {
      alert('Esto no es un numero');
      document.getElementById('cantidad'+id).focus();
      return false;
    }
    if (isNaN(precio)) {
      alert('Esto no es un numero');
      document.getElementById('precio_venta'+id).focus();
      return false;
    }
    //Fin de validación
    detalle.id_producto=id;
    detalle.cantidad = cantidad;
    detalle.precio=precio;
    detalle.cuenta = cuenta;
    detalle.id_detalle='';
    detalle.id_presupuesto=$("input#id_presupuesto").val();;
    console.log(detalle);
    var DatosJson = JSON.stringify(detalle);
    console.log(DatosJson);
    $.post(base_url+"compra/presupuesto/agregarProducto",{
      DetallePost:DatosJson
    }, function(data, textStatus) {
      console.log("data : "+data);
      $("#mensaje_productos").append(data.response_msg);
       $("#respuesta").show();
    $("#save").show();
    html=
    "<table class='table table-bordered table-striped' style='width: 99%;position: relative;left: 35px;bottom: 77px;'>"+
      "<tbody>"+
        "<tr>"+
          "<th style='width:15%'>"+id+"</th>"+
          "<th style='width:50%'>"+nombre+"</th>"+
          "<th style='width:15%'>"+cantidad+"</th>"+
          "<th stye='width:15%'>"+precio+"</th>"+
        "</tr>"+
      "</tbody>"+
    "<table>";
      $("#respuesta").removeAttr('disabled');
      $("#btnGuardar").attr("disabled", false);
      $("#respuesta").append(html);
    }, "json").fail(function(response) {
        $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
        console.log('Error: ' + response.responseText);
    });
  }
   function salir()
    {
      alertify.confirm("¿Esta seguro de guardar la Orden de Compra?","Click en ACEPTAR para guardar",function(){
          window.location.href = base_url+"compra/detalle_presupuesto"

      },function(){

      }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }
    $("#btnAgregarProducto").click(function(e) {
        var presupuesto = new Object();
        presupuesto.unidad = $("select#unidad").val();
        presupuesto.fecha = $("input#fecha").val();
        presupuesto.id_presupuesto ='';

        if (presupuesto.fecha == '') {
          e.preventDefault();
          e.stopPropagation();
          $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese codigo de Orden de Compra</div>");
        }
        var DatosJson = JSON.stringify(presupuesto);
           console.log(DatosJson);
        $.post(base_url+"compra/presupuesto/agregarPresupuesto",{
          presupuestoPost: DatosJson
        },function(data,textStatus) {
          $("#mensaje").append(data.response_msg);
        },"json").fail(function(response) {
           $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
        console.log('Error: ' + response.responseText);
        });
    });
  
</script>