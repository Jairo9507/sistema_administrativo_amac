<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Detalle</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="proveedor" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Codigo Presupuesto</th>
							<th>Unidad</th>
							<th>Fecha</th>
							<th>Usuario</th>
							<th></th>
						</tr>
					</thead>
<tbody>
<?php if($detalle){
foreach ($detalle as $det) {
	$idPresupuesto = base64_encode($det->ID_PRESUPUESTO);
	echo "<tr>";
	echo "<td>".$det->ID_PRESUPUESTO."</td>";
	echo "<td>".$det->UNIDAD."</td>";
	echo "<td>".$det->FECHA_REGISTRO."</td>";
	echo "<td>".$det->USUARIO_CREACION."</td>";
	echo "<td><a href='detalle_presupuesto/editar/".$idPresupuesto."'><button type='button' title='Editar Presupuesto' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
}
} ?> 	
</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->	
	</section>
</div>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$('#proveedor').DataTable(
			{
		pageLength:5,
      "order" : [],
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }
	}
			);
	});
</script>