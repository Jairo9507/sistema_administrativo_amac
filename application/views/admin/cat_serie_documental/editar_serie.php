<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('descripcion',@$serie[0]->DESCRIPCION),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion'

	);

	$nombre =array(
		'name' 		=> 'nombre',
		'id'		=> 'nombre',
		'value'		=> set_value('codigo',@$serie[0]->NOMBRE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Serie 1'

	);

	$codigo=array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',@$serie[0]->CODIGO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Codigo'

	);

	$tipologia=array(
		'name' 		=> 'tipologia',
		'id'		=> 'tipologia',
		'value'		=> set_value('tipologia',@$serie[0]->TIPOLOGIA),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Tipologia'

	);

?>
<input type="hidden" name="id" id="id" value="<?php echo @$serie[0]->ID_SERIE_DOCUMENTAL?>">
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>					
	</section>
	<section class="content">
					<div class="box">
						<div id="mensaje"></div>
						<div class="box-header with-border">
							<h3 class="box-title">Agregar nueva serie</h3>
						</div>
						<div class="box-header">
						<form class="form-horizontal" name="formulario" id="formulario" role="form">
							<div class="form-group">
								<input type="hidden" name="id" id="id" value="">
								<label class="form-label col-sm-2">Nombre<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<div class="col-sm-10">
									<?php echo form_input($nombre) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="form-label col-sm-2">C&oacute;digo<!--<span style="color: #F20A06; font-size: 15px;">*</span>-->:</label>
								<div class="col-sm-10">
									<?php echo form_input($codigo); ?>
								</div>								
							</div>
							<div class="form-group">
								<label class="form-label col-sm-2">Descripci&oacute;n:</label>
								<div class="col-sm-10">
									<?php echo form_textarea($descripcion); ?>
								</div>								
							</div>
							<div class="form-group">
								<label class="form-label col-sm-2">Tipologia:</label>
								<div class="col-sm-10">
									<?php echo form_textarea($tipologia); ?>
								</div>
							</div>
						</form>											
						</div>
						<div class="box-footer">
								<button type="button" class="btn btn-default " onclick="regresar()">Cancelar</button>
						<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
						</div>							
					</div>
	</section>
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/JsonSerie.js');?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_serie_documental"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"catalogos/cat_serie_documental"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }


</script>
