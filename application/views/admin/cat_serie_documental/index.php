<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"catalogos/cat_serie_documental"
        } );
    }
	function eliminarSerie(id,nombre){
		alertify.confirm("Eliminar Serie","¿Seguro de eliminar la serie "+nombre+"?",function(){
            document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
			var serie=new Object();
			serie.id=id;
			console.log(serie);
			var DatosJson=JSON.stringify(serie);
			$.post(baseurl+'catalogos/cat_serie_documental/eliminar',{
				SeriePost:DatosJson
			},function(data,textStatus){
				$("#mensaje").html(data.response_msg);
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
	    		console.log('Error: ' + response.responseText);
			});			
		},function(){

		});
	}    
</script>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="content-title">Lista de Series</h3>
			</div>
                	<div class="row">
                		<div class="col-sm-2 pull-right">
                            <a href="cat_serie_documental/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                		</div>
                	</div>				
			<div id="mensaje"></div>
					<div class="box-body" >
						<table id="series" class="table table-bordered table-striped">
							<thead>
								<th>Serie</th>
								<th>Descripci&oacute;n</th>
								<th>Tipologia</th>
								<th>C&oacute;digo</th>
								<th>Estado</th>
								<th></th>
							</thead>
							<tbody>
								<?php 
									if ($series) {
										foreach ($series as $s) {
											$serieId=base64_encode($s->ID_SERIE_DOCUMENTAL);
											echo "<tr>";
											echo "<td>".$s->NOMBRE."</td>";
											echo "<td>".$s->DESCRIPCION."</td>";
											echo "<td>".$s->TIPOLOGIA."</td>";
											echo "<td>".$s->CODIGO."</td>";
											if ($s->ESTADO=1) {
												echo "<td>ACTIVO</td>";
											}
											echo "<td><a href='cat_serie_documental/editar/".$serieId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";

								 ?>

										<button type="button"  title="Eliminar tipo" onclick="eliminarSerie('<?php echo $serieId?>','<?php echo $s->NOMBRE?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
								 <?php
								 			echo "</td>";
								 			echo "</tr>"; 
										}
									}
								  ?>
							</tbody>
						</table>
					</div>			
		</div>
	</section>	
</div>
<script type="text/javascript">

	function limpiar()
	{
		$("input#id").val('');
		$("input#nombre").val('');
		$("textarea#descripcion").val('');		
	}

	$(document).ready(function(){
		$("#nombre").focus();
		$("#series").DataTable({ 
			paging      : true,
			pageLength  : 5,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			"order" : [],
			info        : true,
			autoWidth   : false,
			language: {
				search:'Buscar:',
				order: 'Mostrar Entradas',
				paginate: {
					first:"Primero",
					previous:"Anterior",
					next:"Siguiente",
					last:"Ultimo"
				},
				emptyTable: "No hay informacion disponible",
				infoEmpty: "Mostrando 0 de 0 de 0 entradas",
				lengthMenu:"Mostrar _MENU_ Entradas",
				info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
			}    
		});


	});
</script>
<div class="modal fade text-center" id="modal_nuevo">
  <div class="modal-dialog" >
    <div class="modal-content">
    </div>
  </div>
</div>