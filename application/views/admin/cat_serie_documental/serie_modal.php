<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 
	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion'

	);

	$nombre =array(
		'name' 		=> 'nombre',
		'id'		=> 'nombre',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Serie 1'

	);

	$codigo=array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Codigo'

	);

	$tipologia=array(
		'name' 		=> 'tipologia',
		'id'		=> 'tipologia',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Tipologia'

	);


 ?>
 <input type="hidden" name="id" id="id" value="">
<div class="modal-content">
 		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" >
				<span >x</span>
			</button>
			<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
		</div>
 	<div class="modal-body">
						<form class="form-horizontal" name="formulario" id="formulario" role="form">
							<div class="form-group">
								<input type="hidden" name="id" id="id" value="">
								<label class="form-label col-sm-2">Nombre<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<div class="col-sm-10">
									<?php echo form_input($nombre) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="form-label col-sm-2">C&oacute;digo<!--<span style="color: #F20A06; font-size: 15px;">*</span>-->:</label>
								<div class="col-sm-10">
									<?php echo form_input($codigo); ?>
								</div>								
							</div>
							<div class="form-group">
								<label class="form-label col-sm-2">Descripci&oacute;n:</label>
								<div class="col-sm-10">
									<?php echo form_textarea($descripcion); ?>
								</div>								
							</div>
							<div class="form-group">
								<label class="form-label col-sm-2">Tipologia:</label>
								<div class="col-sm-10">
									<?php echo form_textarea($tipologia); ?>
								</div>
							</div>
 	</div>
 	<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	 		
 	</div>			
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonSerie.js')?>"></script>