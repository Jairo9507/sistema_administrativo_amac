<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 
	$nombre= array(
		'name' 		=> 'nombre',
		'id'		=> 'nombre',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Sub serie 1'

	);

	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion'

	);

	$codigo= array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Sub serie 1'

	);
	$tipologia=array(
		'name' 		=> 'tipologia',
		'id'		=> 'tipologia',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Tipologia'

	);

 ?>
 <input type="hidden" name="id" id="id" value="">
<div class="modal-content">
 		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" >
				<span >x</span>
			</button>
			<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
		</div>
 	<div class="modal-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div class="form-group">
				<label class="form-label col-sm-4" for="codigo">Nombre<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-6">
					<?php echo form_input($nombre); ?>
				</div>		
			</div>
			<div class="form-group">
				<label class="form-label col-sm-4" for="codigo">C&oacute;digo<!--<span style="color: #F20A06; font-size: 15px;">*</span>-->:</label>
				<div class="col-sm-6">
					<?php echo form_input($codigo); ?>
				</div>					
			</div>
			<div class="form-group">
				<label class="form-label col-sm-4" for="descripcion">Descripci&oacute;n:</label>
				<div class="col-sm-6">
					<?php echo form_textarea($descripcion); ?>
				</div>						
			</div>
			<div class="form-group">
				<label class="form-label col-sm-4" for="tipologia">Tipolog&iacute;a:</label>
				<div class="col-sm-6">
					<?php echo form_textarea($tipologia); ?>
				</div>						
			</div>			

			<div class="form-group">
				<label class="form-label col-sm-4" for="numero_estantes">Serie<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-6">
					<select class="form-control" id="serie_id" >
						<option value="">Seleccione</option>
						<?php 
							if ($series) {
								foreach ($series as $s) {
									echo "<option value='".$s->ID_SERIE_DOCUMENTAL."'>".$s->NOMBRE."</option>";
								}
							}
						 ?>
					</select>
				</div>				
			</div>			
		</form> 		
 	</div>
 	<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	 		
 	</div>			
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonSubserie.js')?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#serie_id").select2();
	});
</script>