<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 
	$nombre= array(
		'name' 		=> 'nombre',
		'id'		=> 'nombre',
		'value'		=> set_value('nombre',@$subserie[0]->NOMBRE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Sub serie 1'

	);

	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('descripcion',@$subserie[0]->DESCRIPCION),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion'

	);

	$codigo= array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',@$subserie[0]->CODIGO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Sub serie 1'

	);

	$tipologia=array(
		'name' 		=> 'tipologia',
		'id'		=> 'tipologia',
		'value'		=> set_value('codigo',@$subserie[0]->TIPOLOGIA),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Tipologia'

	);


 ?>
 <input type="hidden" name="id" id="id" value="<?php echo @$subserie[0]->ID_SUB_SERIE_DOCUMENTAL?>">
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="content-title">Datos</h3>	
			</div>
			<div class="box-body">
				<form class="form-horizontal" name="formulario" id="formulario" role="form">
					<div class="form-group">
						<label class="form-label col-sm-4" for="codigo">Nombre<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
						<div class="col-sm-6">
							<?php echo form_input($nombre); ?>
						</div>		
					</div>
					<div class="form-group">
						<label class="form-label col-sm-4" for="codigo">C&oacute;digo<!--<span style="color: #F20A06; font-size: 15px;">*</span>-->:</label>
						<div class="col-sm-6">
							<?php echo form_input($codigo); ?>
						</div>					
					</div>
					<div class="form-group">
						<label class="form-label col-sm-4" for="descripcion">Descripci&oacute;n:</label>
						<div class="col-sm-6">
							<?php echo form_textarea($descripcion); ?>
						</div>						
					</div>
					<div class="form-group">
						<label class="form-label col-sm-4" for="tipologia">Tipolog&iacute;a:</label>
						<div class="col-sm-6">
							<?php echo form_textarea($tipologia); ?>
						</div>						
					</div>					

					<div class="form-group">
						<label class="form-label col-sm-4" for="numero_estantes">Serie<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
						<div class="col-sm-6">
							<select class="form-control" id="serie_id">
								<option value="">Seleccione</option>
								<?php 
									if ($series) {
										foreach ($series as $s) {
											if ($s->ID_SERIE_DOCUMENTAL==@$subserie[0]->ID_SERIE_DOCUMENTAL) {
											echo "<option value='".$s->ID_SERIE_DOCUMENTAL."' selected>".$s->NOMBRE."</option>";
											} else {
											echo "<option value='".$s->ID_SERIE_DOCUMENTAL."'>".$s->NOMBRE."</option>";
											}
											
										}
									}
								 ?>
							</select>
						</div>				
					</div>			
				</form> 				
			</div>
			<div class="box-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	 					
			</div>
		</div>
	</section>	
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_sub_serie_documental"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"catalogos/cat_sub_serie_documental"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
	$(document).ready(function(){
		$("#serie_id").select2();
	});
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsonSubserie.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>
