<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
?>

<div class="content-wrapper">
		<section class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-header">
					<h3 class="title-header">Prestamos con retraso al archivo</h3>
				</div>
					<div class="box-body">
						<table id="prestamos" class="table table-bordered table-striped"> 
						<thead>
							<th>C&oacute;digo documento</th>
							<th>Contenido</th>
							<!--<th>Codigo transferencia</th>-->
							<th>fecha de Registro de Prestamo</th>
							<th>Unidad solicito</th>						
							<th>Responsable archivo de gesti&oacute;n</th>
							<th>Fecha de salida</th>
							<th>Fecha de regreso estimada</th>							
						</thead>
						<tbody>
							<?php 
								if($prestamos){
									foreach ($prestamos as $p) {
										echo "<tr>";
										echo "<td>".$p->CODIGO_DOCUMENTO."</td>";
										echo "<td style='width:30%;'>".$p->DOCUMENTO."</td>";
										//echo "<td>".$p->CODIGO."</td>";
										echo "<td>".$p->FECHA_TRANSFERENCIA."</td>";
										echo "<td>".$p->UNIDAD."</td>";
										echo "<td>".$p->responsable."</td>";
										echo "<td>".date('m-d-Y',strtotime($p->FECHA_SALIDA_ARCHIVO_CENTRAL))."</td>";
										echo "<td>".date('m-d-Y',strtotime($p->FECHA_REGRESO_ARCHIVO_CENTRAL))."</td>";
										echo "</tr>";
									}
								} else {
									echo "<tr><td colspan='8' style='text-align:center;'>No hay informacion</td></tr>";
								}
							?>
						</tbody>							
						</table>
					</div>
				
				
			</div>
		</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
    $('#prestamos').DataTable({
      paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas"
      }        
    });		
	})
</script>