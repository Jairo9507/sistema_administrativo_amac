<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$codigo_unidad=array(
		'name' 		=> 'codigo_unidad',
		'id'		=> 'codigo_unidad',
		'value'		=> set_value('codigo_unidad',@$unidad[0]->CODIGO_UNIDAD),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'C&oacute;digo',
		'disabled'=>true
	);

	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('descripcion',@$unidad[0]->DESCRIPCION),
		'type'		=> 'textarea',
		'class'		=> 'form-control',
		'rows'=>3,
		'placeholder' => 'Descripcion',
		'onkeypress'	=> 'return validarn(event)'	
	);

?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dist/css/alertify.css')?>">
<input type="hidden" name="id" id="id" value="<?php echo @$unidad[0]->ID_UNIDAD?>">
<div class="content-wrapper">
	<div class="content-header">
		<?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>		
	</div>
	<div class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="box-title">Editar perfil</h3>				
			</div>
			<div class="box-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
				<div class="form-group">
					<label for="nombre" class="col-sm-2 form-label">C&oacute;digo de Unidad:</label>
					<div class="col-sm-10">
						<?php echo form_input($codigo_unidad);?>
					</div>
				</div>
				<div class="form-group">
					<label for="nombre" class="col-sm-2 form-label">Descripci&oacute;n:</label>
					<div class="col-sm-10">
						<?php echo form_textarea($descripcion);?>
					</div>
				</div>			
				<div class="form-group">
					<label for="unidad_id" class="col-sm-2 form-label">Dependencia:</label>
					<div class="col-sm-10">
						<select id="unidad_id" style="width:100%!important;">
							<option value="">Seleccione</option>
							<?php 
								if ($unidades) {
									foreach ($unidades as $u) {
										if ($u->ID_UNIDAD==@$unidad[0]->UNIDAD_DEPENDE) {
										echo "<option value='".$u->ID_UNIDAD."' selected>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
										} else {
										echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
										}
										

									}
								}
							 ?>
						</select>
					</div>
				</div>
		</form>
			</div>
			<div class="box-footer">			
					<button type="button" class="btn btn-default " onclick="regresar()">Cancelar</button>
					<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"admin/unidad_administrativa"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"admin/unidad_administrativa"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }	
	$(document).ready(function(){
		$("#unidad_id").select2();
	})
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/JsonUnidadAdministrativa.js"></script>