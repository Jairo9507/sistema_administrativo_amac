<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$codigo_unidad=array(
		'name' 		=> 'codigo_unidad',
		'id'		=> 'codigo_unidad',
		'value'		=> set_value('nombre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'C&oacute;digo'
	);

	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('nombre',''),
		'type'		=> 'textarea',
		'class'		=> 'form-control',
		'rows'=>3,
		'placeholder' => 'Descripcion',
		'onkeypress'	=> 'return validarn(event)'	
	);

?>
<input type="hidden" name="id" value="" id="id">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h4 class="modal-title"><?php echo $pagetitle; ?></h4>
	</div>
	<div class="modal-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
				<div class="form-group">
					<label for="nombre" class="col-sm-2 form-label">C&oacute;digo de Unidad:</label>
					<div class="col-sm-10">
						<?php echo form_input($codigo_unidad);?>
					</div>
				</div>
				<div class="form-group">
					<label for="nombre" class="col-sm-2 form-label">Descripci&oacute;n:</label>
					<div class="col-sm-10">
						<?php echo form_textarea($descripcion);?>
					</div>
				</div>			
				<div class="form-group">
					<label for="unidad_id" class="col-sm-2 form-label">Dependencia:</label>
					<div class="col-sm-10">
						<select id="unidad_id">
							<option value="">Seleccione</option>
							<?php 
								if ($unidades) {
									foreach ($unidades as $u) {
										echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
									}
								}
							 ?>
						</select>
					</div>
				</div>
		</form>
	</div>
	<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>		
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>js/JsonUnidadAdministrativa.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#unidad_id").select2();
	});
</script>