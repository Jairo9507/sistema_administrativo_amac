<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="content-title">Lista de Clases de Vehículo</h3>
			</div>
<!-- 			<div class="row">

				<div class="col-sm-2 pull-right">
					<a href="cat_actividad/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>
				</div>
			</div> -->
			<br>
			<div class="row">
				<div class="col-md-8" style="width: 65%;left: 15px;">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Lista de Clases de Vehículo</h3>
						</div>
						<table id="clase" class="table table-bordered table-striped">
							<thead>
								<th>Codigo</th>
								<th>Clase Vehículo</th>
								<th>Estado</th>
								<th></th>
							</thead>
							<tbody>
								<?php 
								if ($clases) {
									foreach ($clases as $c) {
										$idclase= base64_encode($c->ID_CLASE_VEHICULO);
										echo "<tr>";
										echo "<td>".$c->ID_CLASE_VEHICULO."</td>";
										echo "<td>".$c->DESCRIPCION."</td>";
										if ($c->ESTADO==1) {
											echo "<td>ACTIVO</td>";
										} 
										echo "<td><a href='cat_clase_vehiculo/editar/".$idclase."' ><button type='button' title='Editar actividad' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
										?>
										<button type="button"  title="Eliminar actividad" onclick="eliminarClaseVehiculo('<?php echo $idclase?>','<?php echo $c->DESCRIPCION?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
										<?php 
										echo "</td";
										echo "</tr>";
									}
								}

								?>
							</tbody>
						</table>
					</div>	
				</div>
				<div class="col-md-4">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Agregar Clase de Vehículo</h3>
						</div>
						<form role="form">
							<div class="box-body">
								<div class="form-group">
									<br>
									<label for="exampleInputEmail1">Clase de Vehículo</label>
									<br>
									<div id="mensaje"></div>
									<textarea id="descripcion" name="descripcion" class="form-control" rows="3" placeholder="Ingrese nueva clase de vehículo ..."></textarea>
								</div>
							</div>
						</form>
						<br>
						<div class="modal-footer">
								<button type="button" class="btn btn-default" onclick="limpiar()">Cancelar</button>	&nbsp;							
							<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
						</div>

					</div>	
				</div>
			</div>
		</div>
	</section>
</div>

<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>

<script type="text/javascript">
	function limpiar(){
		$("textarea#descripcion").val('');
	}
	var baseurl = "<?php echo base_url();?>";
	$(document).ready(function() {
		$("#clase").DataTable({
			paging      : true,
		  pageLength  : 5,
	      lengthChange: true,
	      searching   : true,
	      ordering    : true,
	      "order" : [],
	      info        : true,
	      autoWidth   : false,
	      language: {
	        search:'Buscar:',
	        order: 'Mostrar Entradas',
	        paginate: {
	            first:"Primero",
	            previous:"Anterior",
	            next:"Siguiente",
	            last:"Ultimo"
	        },
	        emptyTable: "No hay informacion disponible",
	        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
	        lengthMenu:"Mostrar _MENU_ Entradas",
	        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
	      }    
		});

		$("#btnGuardar").click(function() {
		var Clase = new Object();
		Clase.id = '';
		Clase.descripcion = $("textarea#descripcion").val();
		var des = $("textarea#descripcion").val();
		console.log(Clase);
		var DatosJson = JSON.stringify(Clase);
		if (des == '') {
			$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Descripcion es obligatoria</div>");
		}else{
		
		console.log(DatosJson);
		$.post(baseurl+"catalogos/cat_clase_vehiculo/guardar",{
			ClaseVehiculoPost : DatosJson
		}, function(data, textStatus) {
			$("#mensaje").html(data.response_msg);
			
			if ($("#descripcion").val("") != '') {
				// $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
					location.reload();
			}
		}, "json").fail(function(response) {
			$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    		console.log('Error: ' + response.responseText);
		});;
	}
	});
	});

	function eliminarClaseVehiculo(idclase,nombre)
    {
    	alertify.confirm("Eliminar Actividad","¿Seguro de eliminar la actividad "+nombre+"?",function(){
             document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
             var Clase=new Object();
             Clase.id=idclase;
             var DatosJson=JSON.stringify(Clase);
             $.post(baseurl+'/catalogos/cat_clase_vehiculo/eliminar',{
             	ClaseVehiculoPost: DatosJson
             },function(data,textStatus){
             	console.log(data);
                $("#mensaje").html(data.response_msg);
                location.reload();             	
             },"json").fail(function(response){
                $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
             });;
              		
    	},function(){

    	});
    }
</script>