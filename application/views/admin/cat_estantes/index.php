<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
		var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"catalogos/cat_estantes"
        } );
    }

    function eliminarEstante(id,nombre)
    {
    	alertify.confirm("Eliminar el estante","¿Seguro de eliminar el estante "+nombre+"?",function(){
			 document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
            var estante=new Object();
            estante.id=id;
            var DatosJson=JSON.stringify(estante);
            $.post(baseurl+'catalogos/cat_estantes/eliminar',{
            	EstantePost:DatosJson
            },function(data,textStatus){
            	$("#mensaje").html(data.response_msg)
            },"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);

            })
    	},function(){

    	});
    }

</script>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="content-title">Lista de Estantes</h3>
			</div>
                	<div class="row">
                		<div class="col-sm-2 pull-right">
                            <a href="cat_estantes/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                		</div>
                	</div>
            <div class="box-body">
						<table id="estantes" class="table table-bordered table-striped">
							<thead>
								<th>Pasillo</th>
								<th>Código Estante</th>
								<th>Descripcion</th>
								<th>Estado</th>
								<th></th>
							</thead>
							<tbody>
								<?php 
								if ($estantes) {
									foreach ($estantes as $e) {
										$idestante=base64_encode($e->ID_ESTANTE);
										echo "<tr>";
										echo "<td>".$e->CODIGO." ".$e->DESCRIPCION."</td>";
										echo "<td>".$e->CODIGO_ESTANTE."</td>";
										echo "<td>".$e->DESTANTE."</td>";
										if ($e->ESTADO==1) {
											echo "<td>OCUPADO</td>";
										}elseif ($e->ESTADO==2) {
											echo "<td>DISPONIBLE</td>";
										} else{
											echo "<td>INHABILITADO</td>";
										}
										echo "<td><a href='cat_estantes/editar/".$idestante."' ><button type='button' title='Editar Documento' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
										?>

										<button type="button"  title="Eliminar actividad" onclick="eliminarEstante('<?php echo $idestante?>','<?php echo $e->DESTANTE?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
										<?php 
										echo "</td";
										echo "</tr>";
									}
								}

								?>
							</tbody>
						</table>            	
            </div>					
			<!--<div class="row">
				<div class="col-md-8" style="width: 65%;left: 15px;">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Lista de Estantes</h3>
						</div>

					</div>	
				</div>
				<div class="col-md-4">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Agregar nuevo estante</h3>
						</div>
						<form role="form">
							<div class="box-body">
								<div class="form-group">
									<input type="hidden" name="id" id="id" value="">
									<br>
									<label for="descripcion" class="col-sm-2">Descripcion:</label>
									<div id="mensaje"></div>
									<input type="text" class="form-control col-sm-12" placeholder="ESTANTE 12" name="descripcion" id="descripcion">
									<br>
									<label class="col-sm-2">Código<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
									<br>
									<input type="text" class="form-control col-sm-12" placeholder="AAC-0012" name="codigo" id="codigo" value="AAC-000<?php echo @$correlativo[0]->CORRELATIVO?>" disabled="true"> 
									<label class="col-sm-12">Pasillo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>		
									<div class="col-sm-12">
										<select id="pasillo_id" class="form-control">
											<option value="">Seleccione</option>
											<?php 
												if ($pasillos) {
													foreach ($pasillos as $p) {
														echo "<option value='".$p->ID_CAT_PASILLO."'>".$p->CODIGO." ".$p->DESCRIPCION."</option>";
													}
												}
											 ?>
										</select>
									</div>							
								</div>
							</div>
						</form>
						<br>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" onclick="limpiar()">Cancelar</button>	&nbsp;							
							<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
						</div>

					</div>	
				</div>
			</div>-->

		</div>
	</section>
</div>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#pasillo_id").select2();
		$("#estantes").DataTable({ 
			paging      : true,
			pageLength  : 5,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			"order" : [],
			info        : true,
			autoWidth   : false,
			language: {
				search:'Buscar:',
				order: 'Mostrar Entradas',
				paginate: {
					first:"Primero",
					previous:"Anterior",
					next:"Siguiente",
					last:"Ultimo"
				},
				emptyTable: "No hay informacion disponible",
				infoEmpty: "Mostrando 0 de 0 de 0 entradas",
				lengthMenu:"Mostrar _MENU_ Entradas",
				info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
			}    
		});




	});

</script>

<div class="modal fade text-center" id="modal_nuevo" >
  <div class="modal-dialog" >
    <div class="modal-content" style="width: 900px; right:60px;">
    </div>
  </div>
</div>