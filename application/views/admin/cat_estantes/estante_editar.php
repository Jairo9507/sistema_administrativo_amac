<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<input type="hidden" name="id" id="id" value="<?php echo @$estante[0]->ID_ESTANTE?>">

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
	</section>
	<section class="content"> 
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="content-title">Datos</h3>				
			</div>
			<div class="box-body">
						<form class="form-horizontal" name="formulario" id="formulario" role="form">
							<div class="box-body">
								<div class="form-group">
									<label for='descripcion' class="form-label col-sm-2">Descripcion:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control " placeholder="ESTANTE 12" name="descripcion" id="descripcion" value="<?php echo  @$estante[0]->DESTANTE?>">
									</div>							
								</div>
								<div class="form-group">
									<label class=" form-label col-sm-2">Código<span style="color: #F20A06; font-size: 15px;">*</span>:</label>					
									<div class="col-sm-10">
										<input type="text" class="form-control" placeholder="AAC-0012" name="codigo" id="codigo" value="<?php echo @$estante[0]->CODIGO_ESTANTE?>" disabled="true">					
									</div> 																
								</div>
								<div class="form-group">
									<label class="form-label col-sm-2">Pasillo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>		
									<div class="col-sm-10">
										<select id="pasillo_id" class="form-control">
											<option value="">Seleccione</option>
											<?php 
												if ($pasillos) {
													foreach ($pasillos as $p) {
														if ( @$estante[0]->ID_CAT_PASILLO==$p->ID_CAT_PASILLO) {
														echo "<option value='".$p->ID_CAT_PASILLO."' selected>".$p->CODIGO." ".$p->DESCRIPCION."</option>";

														} else {
														echo "<option value='".$p->ID_CAT_PASILLO."'>".$p->CODIGO." ".$p->DESCRIPCION."</option>";

														}
														
													}
												}
											 ?>
										</select>
									</div>									
								</div>
							</div>
		</form>
				
			</div>
			<div class="box-footer">
				<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
				<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>					
			</div>
		</div>
	</section>
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    	alertify.confirm("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
    		  window.location.href = baseurl+"catalogos/cat_estantes";

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});        
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"catalogos/cat_estantes";

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
	$(document).ready(function(){
		$("pasillo_id").select2();
	});
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsonEstante.js');?>"></script>