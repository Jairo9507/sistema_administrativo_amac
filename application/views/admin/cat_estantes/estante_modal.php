<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<input type="hidden" name="id" id="id" value="">

<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
	</div>
	<div class="modal-body">		
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
							<div class="box-body">
								<div class="form-group">
									<label for='descripcion' class="form-label col-sm-2">Descripcion:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control " placeholder="ESTANTE 12" name="descripcion" id="descripcion">
									</div>							
								</div>
								<!--<div class="form-group">
									<label class=" form-label col-sm-2">Código<span style="color: #F20A06; font-size: 15px;">*</span>:</label>					
									<div class="col-sm-10">
										<input type="text" class="form-control" placeholder="AAC-0012" name="codigo" id="codigo" value="E-<?php echo @$correlativo[0]->CORRELATIVO?>" disabled="true">					
									</div> 																
								</div>-->
								<div class="form-group">
									<label class="form-label col-sm-2">Pasillo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>		
									<div class="col-sm-10">
										<select id="pasillo_id" class="form-control">
											<option value="">Seleccione</option>
											<?php 
												if ($pasillos) {
													foreach ($pasillos as $p) {
														echo "<option value='".$p->ID_CAT_PASILLO."'>".$p->CODIGO." ".$p->DESCRIPCION."</option>";
													}
												}
											 ?>
										</select>
									</div>									
								</div>
							</div>

		</form>
	</div>
	<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonEstante.js');?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("pasillo_id").select2();
	})
</script>
