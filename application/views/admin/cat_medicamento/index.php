<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"catalogos/cat_medicamento"
        } );
    }

    function eliminarProducto(id,nombre) {
    	 
    		alertify.confirm("Eliminar medicamento","¿Seguro de eliminar el Medicamento "+nombre+"?",function(){
    			 document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
    			 var Medicamento = new Object();
    			 Medicamento.id=id;
    			 var DatosJson=JSON.stringify(Medicamento);
    			 console.log(DatosJson);
    			 $.post(baseurl+'/catalogos/cat_medicamento/eliminar',{
    			 	MedicamentoPost: DatosJson
    			 },function(data,textStatus){
	                $("#mensaje").html(data.response_msg);
    			 },"json").fail(function(response){
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                        console.log('Error: ' + response.responseText);
    			 });;
    		},function(){

    		});
    	}	
</script>
<div class="content-wrapper">
		<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>				
		</section>
		<section class="content">
			<div class="box">
				<div class="box-header">
					<h3 class="header-title">Lista de Medicamentos</h3>
				</div>
                	<div class="row">
                        <div class="col-sm-2 col-sm-offset-6">
                            <a href="cat_producto/vistaLevantaminetoPDF" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_levantamineto_pdf" data-toggle="modal"><button type="button" class="btn btn-info" style="position: relative;top:32px;"><span class="glyphicon glyphicon-file"></span>Levantamiento físico</button></a>                                
                        </div>                        
                        <div class="col-sm-2 col-sm-offset-8">
                            <a href="cat_medicamento/vistaPDF" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_pdf" data-toggle="modal"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-file"></span> Imprimir</button></a>                                
                        </div>                        
                		<div class="col-sm-2 pull-right">
                            <a href="cat_medicamento/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                		</div>
                	</div>				
				<div class="box-body">
					<div id="mensaje"></div>
					<table id="productos" class="table table-bordered table-striped">
                    <thead>
                            <th>C&oacute;digo</th>
                            <th >Nombre</th>
                            <th>Unidad de medida</th>
                            <th>Tipo de producto</th>
                            <th>Minimo</th>
                            <th>Cuenta Administrativa</th>
                            <th></th>
                        </thead>
						<tbody>
							<?php 
								if ($medicamentos) {
									foreach ($medicamentos as $p) {
										$productoId=base64_encode($p->ID_PRODUCTO);
										echo "<tr>";
                                        echo "<td>".$p->COD_PRODUCTO."</td>";
										echo "<td>".$p->NOMBRE_PRODUCTO." ".$p->PRESENTACION." ".$p->CONCENTRACION."&nbsp;".$p->FORMA_FARMACEUTICA."</td>";
										echo "<td>".$p->UNIDAD."</td>";
										echo "<td>".$p->TIPO."</td>";
										echo "<td>".$p->MINIMO."</td>";
										echo "<td>".$p->NOMBRE_CUENTA."</td>";
										echo "<td><a href='cat_medicamento/editar/".$productoId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";		
	                						
                						?>

                							 <button type="button"  title="Eliminar Producto" onclick="eliminarProducto('<?php echo $productoId?>','<?php echo $p->NOMBRE_PRODUCTO ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
							<?php
									echo "</td>";
									echo "</tr>";
									}
								} else {

								}								
							?>
						</tbody>
					</table>
				</div>
				
			</div>
			
		</section>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#productos").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
		});
	});
</script>
<div class="modal fade text-center" id="modal_nuevo">
  <div class="modal-dialog" style="width: 80%">
    <div class="modal-content">
    </div>
  </div>
</div>
<div class="modal fade text-center"  id="modal_pdf">
  <div class="modal-dialog" style="width: 90%">
    <div class="modal-content">
    </div>
  </div>
</div>