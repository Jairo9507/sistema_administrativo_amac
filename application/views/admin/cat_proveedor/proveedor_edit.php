<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 
	$nombre=array(
		'name' 		=> 'nombre_proveedor',
		'id'		=> 'nombre',
		'value'		=> set_value('nombre',@$proveedor[0]->NOMBRE_PROVEEDOR),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre S.A de C.V'
	);


	$direccion=array(
		'name' 		=> 'direccion_proveedor',
		'id'		=> 'direccion',
		'value'		=> set_value('direccion',@$proveedor[0]->DIRECCION),
		'type'		=> 'textarea',
		'rows'		=>4,
		'class'		=> 'form-control',
		'placeholder' => 'Direcci&oacute;n',

	);

	$telefono= array(
		'name'		=> 'telefono_proveedor',
		'id'		=> 'telefono_empresa',
		'value'		=> set_value('telefono',@$proveedor[0]->TELEFONO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '2123-4567/7123-4567'		
	);
	$fax= array(
		'name'		=> 'fax_proveedor',
		'id'		=> 'fax_empresa',
		'value'		=> set_value('fax',@$proveedor[0]->FAX),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '2123-4567'	
	);	

	$correo =array(
		'name'		=> 'correo_proveedor',
		'id'		=> 'correo_empresa',
		'value'		=> set_value('correo',@$proveedor[0]->CORREO), 
		'type'		=> 'email',
		'class'		=> 'form-control',
		'placeholder' => 'ejemplo@correo.com',
		'onkeyup' => 'return validarEmail(value)'
	);

	$pagina = array(
		'name'		=>'pagina_proveedor',
		'id'		=>'pagina_empresa',
		'value'		=>set_value('pagina',@$proveedor[0]->PAGINA_WEB),
		'type'		=>'url',
		'class'		=> 'form-control',
		'placeholder' =>'http://www.ejemplo.com',

	);

	$primer_nombre_contacto =array(
		'name'		=>'primer_nombre_contacto',
		'id'		=> 'primer_nombre_contacto',
		'value'		=> set_value('primer_nombre',@$proveedor[0]->PRIMER_NOMBRE_PERSONA_CONTACTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Primer nombre contacto',
		'onkeypress' => 'return validarn(event)'
	);

	$segundo_nombre_contacto =array(
		'name' 		=>'segundo_nombre_contacto',
		'id'		=>'segundo_nombre_contacto',
		'value'		=>set_value('segundo_nombre',@$proveedor[0]->SEGUNDO_NOMBRE_PERSONA_CONTACTO), 
		'type'		=>'text',
		'class'		=>'form-control',
		'placeholder' =>'Segundo nombre contacto',
		'onkeypress' => 'return validarn(event)'
	);

	$primer_apellido_contacto =array(
		'name' 		=>'primar_apellido_contacto',
		'id'		=>'primer_apellido_contacto',
		'value'		=>set_value('primer_apellido',@$proveedor[0]->PRIMER_APELLIDO_PERSONA_CONTACTO), 
		'type'		=>'text',
		'class'		=>'form-control',
		'placeholder' =>'Primer apellido contacto',
		'onkeypress' => 'return validarn(event)'

	);

	$segundo_apellido_contacto =array(
		'name' 		=>'contacto_segundo_apellido',
		'id'		=>'contacto_segundo_apellido',
		'value'		=>set_value('segundo_nombre',@$proveedor[0]->SEGUNDO_APELLIDO_PERSONA_CONTACTO), 
		'type'		=>'text',
		'class'		=>'form-control',
		'placeholder' =>'Segundo nombre contacto',
		'onkeypress' => 'return validarn(event)'
	);

	$telefono_contacto= array(
		'name'		=> 'telefono_contacto',
		'id'		=> 'telefono_contacto',
		'value'		=> set_value('telefono_contacto',@$proveedor[0]->TELEFONO_CONTACTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '2123-4567',
		'onkeypress' => 'return validarTelefono(event)'		
	);

	$celular_contacto= array(
		'name'		=> 'celular_proveedor',
		'id'		=> 'celular_contacto',
		'value'		=> set_value('celular_contacto',@$proveedor[0]->CELULAR_CONTACTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '7123-4567',
		'onkeypress' => 'return validarTelefono(event)'		
	);

	$nrc= array(
		'name'		=> 'nrc_proveedor',
		'id'		=> 'nrc_empresa',
		'value'		=> set_value('nrc_empresa',@$proveedor[0]->NRC),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '100001-1'	
	);

	$nit= array(
		'name'		=> 'nit_proveedor',
		'id'		=> 'nit_empresa',
		'value'		=> set_value('nit_empresa',@$proveedor[0]->NIT),
		'type'		=> 'text', 
		'class'		=> 'form-control',
		'placeholder' => '0123-023456-012-8'	
	);

?>
<input type="hidden" name="id" id="id" value="<?php echo @$proveedor[0]->ID_PROVEEDOR?>">


<div class="content-wrapper">
		<div class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
	</div>
	<section class="content">
			<div id="mensaje"></div>
			<div id="validamail"></div>
			<div id="mensaje_registro"></div>			
			<div class="box">
				<div class="box-header">
						<h3 class="box-title">Editar proveedor</h3>
				</div>
				<form role="form"  name="formulario" id="formulario">
					<div class="box-body">
						<div class="form-group col-sm-12">
							<div class="col-sm-12 text-center">
								<h4>Datos del proveedor</h4>
							</div>
						</div>
						<div class="form-group col-sm-12">
							<label for="nombre_proveedor" class="col-sm-2 form-label">Nombre<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
							<div class="col-sm-4">
								<?php echo form_input($nombre);?>
							</div>
							<label for="direccion" class="col-sm-2 form-label">Telefono/movil<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
							<div class="col-sm-4">
									<?php echo form_input($telefono)?>
							</div>							
						</div>
						<div class="form-group col-sm-12">
							<label for="direccion" class="col-sm-2 form-label">Direcci&oacute;n:</label>
							<div class="col-sm-4">
									<?php echo form_textarea($direccion)?>
							</div>				
							<label for="direccion" class="col-sm-2 form-label">Fax:</label>
							<div class="col-sm-4">
									<?php echo form_input($fax)?>
							</div>		
						</div>			
						<div class="form-group col-sm-12">
							<label for="pagina" class="col-sm-2 form-label">P&aacute;gina web:</label>
							<div class="col-sm-4">
								<?php echo form_input($pagina)?>
							</div>
							<label for="correo" class="col-sm-2 form-label">Correo:</label>
							<div class="col-sm-4">
								<?php echo form_input($correo)?>
							</div>				
						</div>
						<div class="form-group col-sm-12">
							<label for="nrc" class="col-sm-2 form-label">NRC:</label>
							<div class="col-sm-4">
								<?php echo form_input($nrc);?>
							</div>
							<label for="nit" class="col-sm-2 form-label">NIT<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
							<div class="col-sm-4">
								<?php echo form_input($nit);?>
							</div>							
						</div>			
						<div class="form-group col-sm-12">
							<label for="pagina" class="col-sm-2 form-label">Rubro empresarial:</label>
							<div class="col-sm-4">
								<select class="form-control" id="rubro_id">
									<option value="0">Seleccione el rubro</option>					
								<?php if ($rubro) {
									foreach ($rubro as $r) {
										if($r->ID_CAT_RUBRO==@$proveedor[0]->ID_CAT_RUBRO){
											echo "<option value='".$r->ID_CAT_RUBRO."' selected>".$r->DESCRIPCION."</option>";
										} else{
											echo "<option value='".$r->ID_CAT_RUBRO."'>".$r->DESCRIPCION."</option>";
										}
									}
								}?>
								</select>
							</div>				
						</div>
							<div class="col-sm-12 text-center">
								<h4>Datos del Contacto</h4>
							</div>						
						<div class="form-group col-sm-12">
							<label for="persona_primer_nombre" class="col-sm-2 form-label">Contacto Primer Nombre:</label>
							<div class="col-sm-4">
								<?php echo form_input($primer_nombre_contacto)?>
							</div>
							<label for="persona_segundo_nombre" class="col-sm-2 form-label">Contacto Segundo Nombre:</label>
							<div class="col-sm-4">
								<?php echo form_input($segundo_nombre_contacto)?>
							</div>								
						</div>
						<div class="form-group col-sm-12">
							<label for="persona_primer_apellido" class="col-sm-2 form-label">Contacto Primer Apellido:</label>
							<div class="col-sm-4">
								<?php echo form_input($primer_apellido_contacto);?>
							</div>
							<label for="persona_segundo_apellido" class="col-sm-2 form-label">Contacto Segundo Apellido:</label>
							<div class="col-sm-4">
								<?php echo form_input($segundo_apellido_contacto);?>
							</div>
						</div>
						<div class="form-group col-sm-12">
							<label for="celular_contacto" class="col-sm-2 form-label">Contacto celular:</label>
							<div class="col-sm-4">
								<?php echo form_input($celular_contacto);?>
							</div>
							<label for="persona_primer_apellido" class="col-sm-2 form-label">Contacto telefono:</label>
							<div class="col-sm-4">
								<?php echo form_input($telefono_contacto);?>
							</div>				
						</div>							
					</div>					
				</form>
				<div class="box-footer">
								<button type="button" class="btn btn-default " onclick="regresar()">Cancelar</button>
						<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>
				</div>						
			</div>
	</section>
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_proveedor"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"catalogos/cat_proveedor"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
    $(document).ready(function(){
		$("#rubro_id").select2();
    });
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsonProveedor.js');?>"></script>