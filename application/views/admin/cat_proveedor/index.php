<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";

    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"catalogos/cat_proveedor"
        } );
    }
    function eliminarProveedor(idproveedor,nombre){

        alertify.confirm("Eliminar Proveedor","¿Seguro de eliminar al proveedor "+nombre+"?",function(){
             document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
             var Proveedor =new Object();
             Proveedor.id=idproveedor;
             
             var DatosJson=JSON.stringify(Proveedor);
             $.post(baseurl+"/catalogos/cat_proveedor/eliminar",{
                proveedorPost: DatosJson
             },function(data,textStatus){
                $("#mensaje").html(data.response_msg);
             },"json").fail(function(response){
                $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                        console.log('Error: ' + response.responseText);

             });;
        },function(){});
    }
</script>
<div class="content-wrapper">
	            <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>	

                <section class="content">
                	<div class="box">
                		<div class="box-header">
                			<h3 class="header-title">Lista de Proveedores</h3>
                		</div>
                		<div class="row">
                			<div class="col-sm-2 pull-right">
                                <a href="cat_proveedor/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                			</div>
                		</div>
                			<div class="box-body">
                				<div id="mensaje"></div>
                				<table id="proveedores" class="table table-bordered table-striped">
                					<thead>
                            <th>Codigo</th>
                						<th>Nombre</th>
                						<th>Rubro</th>
                						<th>Direcci&oacute;n</th>
                						<th>Telefono/movil</th>
                						<th>Nombre Contacto</th>
                						<th>NIT</th>
                						<th></th>
                					</thead>
                					<tbody>
                						<?php
                							if($proveedores){
	                							foreach ($proveedores as $p) {
	                								$proveedorId=base64_encode($p->ID_PROVEEDOR);
	                								echo "<tr>";
                                  echo "<td>".$p->ID_PROVEEDOR."</td>";
	                								echo "<td>".$p->NOMBRE_PROVEEDOR."</td>";
	                								echo "<td>".$p->RUBRO."</td>";
	                								echo "<td>".$p->DIRECCION."</td>";
	                								echo "<td>".$p->TELEFONO."</td>";
	                								echo "<td>".$p->NOMBRE_CONTACTO."</td>";
	                								echo "<td>".$p->NIT."</td>";
	                								echo "<td><a href='cat_proveedor/editar/".$proveedorId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";		
	                						
                						?>

                							 <button type="button"  title="Eliminar Perfil" onclick="eliminarProveedor('<?php echo $proveedorId?>','<?php echo $p->NOMBRE_PROVEEDOR ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
                						<?php
                								echo "</td";
                								echo "</tr>";
	                						}                								
	                							} else {
	                								echo "<tr><td>No hay informacion disponible</td></tr>";
	                							} 
                							
                						 ?>
                					</tbody>
                				</table>
                			</div>
                	</div>
                </section>
</div>

<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#proveedores").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
		});
	});
</script>
<div class="modal fade text-center" id="modal_nuevo">
  <div class="modal-dialog" style="width: 80%">
    <div class="modal-content">
    </div>
  </div>
</div>