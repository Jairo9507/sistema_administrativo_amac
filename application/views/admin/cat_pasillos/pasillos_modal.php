<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 
	$codigo= array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'P-01'

	);

	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion'

	);

	$numero_estantes= array(
		'name' 		=> 'numero_estantes',
		'id'		=> 'numero_estantes',
		'value'		=> set_value('codigo',''),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0,
		'onkeypress'	=> 'return validarNumeros(event)'
	);


 ?>
 <input type="hidden" name="id" id="id" value="">
 <div class="modal-content">
 		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" >
				<span >x</span>
			</button>
			<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
		</div>		 	
 	<div class="modal-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<!--<div class="form-group">
				<label class="form-label col-sm-4" for="codigo">C&oacute;digo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-6">
					<?php //echo form_input($codigo); ?>
				</div>		
			</div>-->
			<div class="form-group">
				<label class="form-label col-sm-4" for="descripcion">Descripci&oacute;n:</label>
				<div class="col-sm-6">
					<?php echo form_textarea($descripcion); ?>
				</div>						
			</div>
			<div class="form-group">
				<label class="form-label col-sm-4" for="numero_estantes">Numero de estantes<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-6">
					<?php echo form_input($numero_estantes); ?>
				</div>		
						
			</div>	

		</form> 		
 	</div>
 	<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	 		
 	</div> 
	
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#listUnidad").select2();
	});
</script>
  <script type="text/javascript" src="<?php echo base_url('js/JsonPasillo.js');?>"></script>