<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 
	$primer_nombre= array(
		'name' 		=> 'primer_nombre',
		'id'		=> 'primer_nombre',
		'value'		=> set_value('primer_nombre',@$paciente[0]->PRIMER_NOMBRE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Primer nombre',
		'onkeypress'	=> 'return validarn(event)'

	);

	$segundo_nombre= array(
		'name' 		=> 'segundo_nombre',
		'id'		=> 'segundo_nombre',
		'value'		=> set_value('segundo_nombre',@$paciente[0]->SEGUNDO_NOMBRE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Segundo nombre',
		'onkeypress'	=> 'return validarn(event)'

	);

	$tercer_nombre= array(
		'name' 		=> 'tercer_nombre',
		'id'		=> 'tercer_nombre',
		'value'		=> set_value('tercer_nombre',@$paciente[0]->TERCER_NOMBRE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Tercer nombre',
		'onkeypress'	=> 'return validarn(event)'

	);		

	$primer_apellido= array(
		'name' 		=> 'primer_apellido',
		'id'		=> 'primer_apellido',
		'value'		=> set_value('primer_apellido',@$paciente[0]->PRIMER_APELLIDO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Primer apellido',
		'onkeypress'	=> 'return validarn(event)'

	);
	$segundo_apellido= array(
		'name' 		=> 'segundo_apellido',
		'id'		=> 'segundo_apellido',
		'value'		=> set_value('segundo_apellido',@$paciente[0]->SEGUNDO_APELLIDO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Segundo apellido',
		'onkeypress'	=> 'return validarn(event)'

	);
	$tercer_apellido= array(
		'name' 		=> 'tercer_apellido',
		'id'		=> 'tercer_apellido',
		'value'		=> set_value('tercer_apellido',@$paciente[0]->TERCER_APELLIDO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Apellido casada',
		'onkeypress'	=> 'return validarn(event)'

	);			

	$fecha_nac= array(
		'name' 		=> 'fecha_nacimiento',
		'id'		=> 'fecha_nacimiento',
		'value'		=> set_value('fecha_nacimiento',date("Y-m-d", strtotime(@$paciente[0]->FECHA_NACIMIENTO))),
		'type'		=> 'date',
		'class'		=> 'form-control',

	);				

	$nombre_madre= array(
		'name' 		=> 'nombre_madre',
		'id'		=> 'nombre_madre',
		'value'		=> set_value('nombre_madre',@$paciente[0]->NOMBRE_MADRE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre completo',

	);				

	$nombre_padre= array(
		'name' 		=> 'nombre_padre',
		'id'		=> 'nombre_padre',
		'value'		=> set_value('nombre_padre',@$paciente[0]->NOMBRE_PADRE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre completo',
	);					

	$responsable= array(
		'name' 		=> 'responsable',
		'id'		=> 'responsable',
		'value'		=> set_value('responsable',@$paciente[0]->RESPONSABLE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre completo',

	);						

	$direccion= array(
		'name' 		=> 'direccion',
		'id'		=> 'direccion',
		'value'		=> set_value('direccion',@$paciente[0]->DIRECCION),
		'type'		=> 'textarea',
		'rows'		=>4,
		'class'		=> 'form-control',
		'placeholder' => 'Direcci&oacute;n',
	);

	$dui= array(
		'name' 		=> 'dui',
		'id'		=> 'dui',
		'value'		=> set_value('dui',@$paciente[0]->DUI),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '01234567-8',
	);

	$codigo_expediente= array(
		'name' 		=> 'codigo_expediente',
		'id'		=> 'codigo_expediente',
		'value'		=> set_value('codigo',@$paciente[0]->CODIGO_EXPEDIENTE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '01-2019',
		'disabled'		=>true
	);

?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>

<input type="hidden" name="id" id="id" value="<?php echo @$paciente[0]->ID_PACIENTE;?>">
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
 		<?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="header-title">Datos</h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" name="formulario" id="formulario" role="form">
					<div class="form-group">
						<label for='primer_nombre' class="form-label col-sm-2">Primer nombre:</label>
						<div class="col-sm-4">
							<?php echo form_input($primer_nombre);?>
						</div>
						<label for='segundo_nombre' class="form-label col-sm-2">Segundo nombre:</label>
						<div class="col-sm-4">
							<?php echo form_input($segundo_nombre);?>
						</div>				
					</div>
					<div class="form-group">
						<label for='tercer_nombre' class="form-label col-sm-2">Tercer nombre:</label>
						<div class="col-sm-4">
							<?php echo form_input($tercer_nombre);?>
						</div>
						<label for='primer_apellido' class="form-label col-sm-2">Primer apellido:</label>
						<div class="col-sm-4">
							<?php echo form_input($primer_apellido);?>
						</div>				
					</div>
					<div class="form-group">
						<label for='segundo_apellido' class="form-label col-sm-2">Segundo apellido:</label>
						<div class="col-sm-4">
							<?php echo form_input($segundo_apellido);?>
						</div>
						<label for='tercer_apellido' class="form-label col-sm-2">Apellido de Casada:</label>
						<div class="col-sm-4">
							<?php echo form_input($tercer_apellido);?>
						</div>				
					</div>
					<div class="form-group">
						<label for='fecha_nacimiento' class="form-label col-sm-2">Fecha de nacimiento:</label>
						<div class="col-sm-4">
							<?php echo form_input($fecha_nac);?>
						</div>
						<label for='genero' class="form-label col-sm-2">Genero:</label>
						<div class="col-sm-4">                
			                <div class="col-sm-4 radio">
			                    <label>Masculino <?php if (@$paciente[0]->SEXO=='M') {
	                  	        		echo '<input type="radio" value="M" name="genero" id="masculino" checked="checked">';  	
	                    			} else {echo '<input type="radio" name="genero" id="masculino" value="M" >';}  ?></label>
			                </div>

			                <div class="col-sm-4 radio">
			                    <label>Femenino <?php if (@$paciente[0]->SEXO=='F') {
	                  	        		echo '<input type="radio" value="F" name="genero" id="femenino" checked="checked">';  	
	                    			} else {echo '<input type="radio" name="genero" id="femenino" value="M" >';}?></label>
			                </div>
						</div>				
					</div>		
					<div class="form-group">
						<label for='nombre_madre' class="form-label col-sm-2">Nombre Madre :</label>
						<div class="col-sm-4">
							<?php echo form_input($nombre_madre);?>
						</div>
						<label for='nombre_padre' class="form-label col-sm-2">Nombre Padre :</label>
						<div class="col-sm-4">                
							<?php echo form_input($nombre_padre);?>
						</div>				
					</div>
					<div class="form-group">
						<label for='nombre_madre' class="form-label col-sm-2">Nombre Madre :</label>
						<div class="col-sm-4">
							<?php echo form_input($responsable);?>
						</div>
				
					</div>										
					<div class="form-group">
						<label for='direccion' class="form-label col-sm-2">Direcci&oacute;n:</label>
						<div class="col-sm-4">
							<?php echo form_textarea($direccion);?>
						</div>
						<label for='dui' class="form-label col-sm-2">Dui:</label>
						<div class="col-sm-4">                
							<?php echo form_input($dui);?>
						</div>				
					</div>
					<div class="form-group">
						<label for="codigo_expediente" class="form-label col-sm-2">Codigo Expediente:</label>
						<div class="col-sm-4">
							<?php echo form_input($codigo_expediente); ?>
						</div>
					</div>												
				</form>				
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar();">Cerrar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
				
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonPaciente.js');?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"clinica/paciente"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"clinica/paciente"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    } 
</script>