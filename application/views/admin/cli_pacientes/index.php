<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"clinica/paciente"
        } );
    }

function eliminarPaciente(id,nombre){
	alertify.confirm("Eliminar paciente","¿Seguro de eliminar al paciente "+nombre+"?",function(){
    			 document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
    			 var paciente=new Object();
    			 paciente.id=id;
    			 var DatosJson=JSON.stringify(paciente);
    			 console.log(DatosJson);
    			 $.post(baseurl+'clinica/paciente/eliminar',{
    			 	PacientePost:DatosJson
    			 },function(data,textStatus){
	                $("#mensaje").html(data.response_msg);
    			 },"json").fail(function(response){
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                        console.log('Error: ' + response.responseText);
    			 });;
	},function(){

	});
}
</script>
<div class="content-wrapper">
	<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>				
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="header-title">Listado de pacientes</h3>
			</div>
                	<div class="row">
                		<div class="col-sm-2 pull-right">
                            <a href="paciente/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                		</div>
                	</div>			
			<div class="box-body">
				<div id="mensaje"></div>
				<table id="pacientes" class="table table-bordered table-striped">
					<thead>
						<th>Nombres</th>
						<th>Apellidos</th>
						<th>Fecha Nacimiento</th>
						<th>Nombre Madre</th>
						<th>Nombre Padre</th>
						<th>Direcci&oacute;n</th>
						<th>DUI</th>
						<th>Genero</th>
						<th>Codigo Expediente</th>
						<th></th>
					</thead>
					<tbody>
						<?php 
							if ($pacientes) {
								foreach ($pacientes as $p) {
									$pacienteId=base64_encode($p->ID_PACIENTE);
									echo "<tr>";
									echo "<td>".$p->NOMBRES."</td>";
									echo "<td>".$p->APELLIDOS."</td>";
									echo "<td>".date("d/m/Y",strtotime($p->FECHA_NACIMIENTO))."</td>";
									echo "<td>".$p->NOMBRE_MADRE."</td>";
									echo "<td>".$p->NOMBRE_PADRE."</td>";
									echo "<td>".$p->DIRECCION."</td>";
									echo "<td>".$p->DUI."</td>";
									echo "<td>".$p->SEXO."</td>";
									echo "<td>".$p->CODIGO_EXPEDIENTE."</td>";
									echo "<td><a href='paciente/editar/".$pacienteId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";


						 ?>
                							 <button type="button"  title="Eliminar Paciente" onclick="eliminarPaciente('<?php echo $pacienteId?>','<?php echo $p->NOMBRES ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
						 <?php 

										echo "<a href='paciente/expediente/".$pacienteId."' ><button type='button' title='Expediente Perfil' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-file'></span> </button></td>";
									echo "</tr>";
								}
							} 
						  ?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
	
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		$("#pacientes").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
		});
	});
</script>
<div class="modal fade text-center" id="modal_nuevo" >
  <div class="modal-dialog" style="width: 80%">
    <div class="modal-content">
    </div>
  </div>
</div>