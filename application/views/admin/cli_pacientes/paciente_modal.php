<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 
	$primer_nombre= array(
		'name' 		=> 'primer_nombre',
		'id'		=> 'primer_nombre',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Primer nombre',
		'onkeypress'	=> 'return validarn(event)'

	);

	$segundo_nombre= array(
		'name' 		=> 'segundo_nombre',
		'id'		=> 'segundo_nombre',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Segundo nombre',
		'onkeypress'	=> 'return validarn(event)'

	);

	$tercer_nombre= array(
		'name' 		=> 'tercer_nombre',
		'id'		=> 'tercer_nombre',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Tercer nombre',
		'onkeypress'	=> 'return validarn(event)'

	);		

	$primer_apellido= array(
		'name' 		=> 'primer_apellido',
		'id'		=> 'primer_apellido',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Primer apellido',
		'onkeypress'	=> 'return validarn(event)'

	);
	$segundo_apellido= array(
		'name' 		=> 'segundo_apellido',
		'id'		=> 'segundo_apellido',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Segundo apellido',
		'onkeypress'	=> 'return validarn(event)'

	);
	$tercer_apellido= array(
		'name' 		=> 'tercer_apellido',
		'id'		=> 'tercer_apellido',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Apellido casada',
		'onkeypress'	=> 'return validarn(event)'

	);			

	$fecha_nac= array(
		'name' 		=> 'fecha_nacimiento',
		'id'		=> 'fecha_nacimiento',
		'value'		=> set_value('codigo',''),
		'type'		=> 'date',
		'class'		=> 'form-control',

	);				

	$nombre_madre= array(
		'name' 		=> 'nombre_madre',
		'id'		=> 'nombre_madre',
		'value'		=> set_value('nombre_madre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre completo',

	);				
	$responsable= array(
		'name' 		=> 'responsable',
		'id'		=> 'responsable',
		'value'		=> set_value('responsable',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre completo',

	);					

	$nombre_padre= array(
		'name' 		=> 'nombre_padre',
		'id'		=> 'nombre_padre',
		'value'		=> set_value('nombre_padre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre completo',
	);					

	$direccion= array(
		'name' 		=> 'direccion',
		'id'		=> 'direccion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>4,
		'class'		=> 'form-control',
		'placeholder' => 'Direcci&oacute;n',
	);

	$dui= array(
		'name' 		=> 'dui',
		'id'		=> 'dui',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '01234567-8',
	);

	$masculino= array(
		'name' 		=> 'genero',
		'id'		=> 'masculino',
		'value'		=> set_value('genero','M'),
		'type'		=> 'radio'
	);	

	$femenino= array(
		'name' 		=> 'genero',
		'id'		=> 'femenino',
		'value'		=> set_value('genro','F'),
		'type'		=> 'radio'
	);		

	$codigo_expediente= array(
		'name' 		=> 'codigo_expediente',
		'id'		=> 'codigo_expediente',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'P12345'
	);


?>
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
	</div>
	<div class="modal-body">
		
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div class="form-group">
				<label for='primer_nombre' class="form-label col-sm-2">Primer nombre<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($primer_nombre);?>
				</div>
				<label for='segundo_nombre' class="form-label col-sm-2">Segundo nombre:</label>
				<div class="col-sm-4">
					<?php echo form_input($segundo_nombre);?>
				</div>				
			</div>
			<div class="form-group">
				<label for='tercer_nombre' class="form-label col-sm-2">Tercer nombre:</label>
				<div class="col-sm-4">
					<?php echo form_input($tercer_nombre);?>
				</div>
				<label for='primer_apellido' class="form-label col-sm-2">Primer apellido<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($primer_apellido);?>
				</div>				
			</div>
			<div class="form-group">
				<label for='segundo_apellido' class="form-label col-sm-2">Segundo apellido:</label>
				<div class="col-sm-4">
					<?php echo form_input($segundo_apellido);?>
				</div>
				<label for='tercer_apellido' class="form-label col-sm-2">Apellido de Casada:</label>
				<div class="col-sm-4">
					<?php echo form_input($tercer_apellido);?>
				</div>				
			</div>
			<div class="form-group">
				<label for='fecha_nacimiento' class="form-label col-sm-2">Fecha de nacimiento<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($fecha_nac);?>
				</div>
				<label for='genero' class="form-label col-sm-2">Genero:</label>
				<div class="col-sm-4">                
	                <div class="col-sm-4 radio">
	                    <label>Masculino <?php echo form_radio($masculino); ?></label>
	                </div>

	                <div class="col-sm-4 radio">
	                    <label>Femenino <?php echo form_radio($femenino);?></label>
	                </div>
				</div>				
			</div>		
			<div class="form-group">
				<label for='nombre_madre' class="form-label col-sm-2">Nombre Madre :</label>
				<div class="col-sm-4">
					<?php echo form_input($nombre_madre);?>
				</div>
				<label for='nombre_padre' class="form-label col-sm-2">Nombre Padre :</label>
				<div class="col-sm-4">                
					<?php echo form_input($nombre_padre);?>
				</div>				
			</div>				
			<div class="form-group">
				<label for='nombre_madre' class="form-label col-sm-2">Responsable :</label>
				<div class="col-sm-4">
					<?php echo form_input($responsable);?>
				</div>				
			</div>		
			<div class="form-group">
				<label for='direccion' class="form-label col-sm-2">Direcci&oacute;n:</label>
				<div class="col-sm-4">
					<?php echo form_textarea($direccion);?>
				</div>
				<label for='dui' class="form-label col-sm-2">Dui:</label>
				<div class="col-sm-4">                
					<?php echo form_input($dui);?>
				</div>				
				<sp
			</div>
			<!--<div class="form-group">
				<label for="codigo_expediente" class="form-label col-sm-2">Codigo Expediente:</label>
				<div class="col-sm-4">
					<?php echo form_input($codigo_expediente); ?>
				</div>
			</div>	-->					

		</form>
	</div>
	<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonPaciente.js');?>"></script>