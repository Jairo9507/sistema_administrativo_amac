,,
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"catalogos/cat_unidad_medida"
        } );
    }
</script>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="content-title">Lista de tipos de productos</h3>
			</div>
			<div class="row">
				<div class="col-sm-2 pull-right">
					<a href="cat_tipo_producto/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>
					
				</div>
				
			</div>			
			<div class="box-body">
				<div id="mensaje"></div>
				<table id="tipos" class="table table-bordered table-striped">
					<thead>
						<th>Descripcion</th>
						<th>Estado</th>
						<th></th>
					</thead>
					<tbody>
						<?php 
							if ($tipos) {
								foreach ($tipos as $t) {
									$tipoId=base64_encode($t->id_tipo_producto);
									echo "<tr>";
									echo "<td>".$t->descripcion."</td>";
									if ($t->ESTADO==1) {
									echo "<td>ACTIVO</td>";
									}
									echo "<td><a href='cat_tipo_producto/editar/".$tipoId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
						
						?>
									<button type="button"  title="Eliminar tipo de producto" onclick="eliminartipo('<?php echo $tipoId?>','<?php echo $t->descripcion?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>

						<?php 
									echo "</td>";
									echo "</tr>";
								}
							} else{
								echo "<tr><td colspan='3'>No hay informaci&oacute;n</td></tr>";
							}
						 ?>
					</tbody>
				</table>
			</div>
		</div>
		
	</section>
	
</div>
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#tipos").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"        
      }    
		});		
	});
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>
<div class="modal fade text-center" id="modal_nuevo" >
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>	
</script>