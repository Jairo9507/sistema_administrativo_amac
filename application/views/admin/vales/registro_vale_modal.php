<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
 <?php

	$num_vale=array(
		'name' 		=> 'num_vale',
		'id'		=> 'num_vale',
		'value'		=> set_value('num_vale',''),
		'type'		=> 'text',
		'style'		=> 'width:259px; position:relative; left: 54px; ',
		'class'		=> 'form-control',
		'placeholder' => '00005'		
	);
	$fecha =array(
		'name' 		=> 'fecha',
		'id'		=> 'fecha',
		'value'		=> set_value('fecha',''),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'style'		=> 'width: 235px; left:20px;',
		'placeholder' => ''
	);

	$kilometraje=array(
		'name' 		=> 'kilometraje',
		'id'		=> 'kilometraje',
		'value'		=> set_value('kilometraje',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:240px; position:relative; left: 19px;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);

	$galones=array(
		'name' 		=> 'galones',
		'id'		=> 'galones',
		'value'		=> set_value('galones',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:260px; position:relative; left: 57px;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$valor=array(
		'name' 		=> 'valor',
		'id'		=> 'valor',
		'value'		=> set_value('valor',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:240px; position:relative; left: 19px;',
		'placeholder' => '00.00',
		// 'onkeypress'	=> 'return SoloNumerosDecimales3(e, valInicial, nEntero, nDecimal)'		
	);

	$observaciones =array(
		'name'  => 'observaciones',
		'id'    => 'observaciones',
		'value' => set_value('observaciones', ''),
		'type'  =>  'textarea',
		'rows'  =>  4,
		'class' =>  'form-control', 
		'style' =>  'width:273px; position: relative; left: 55px;',
		'placeholder' => 'Descripcion',
		'onkeypress'  => 'return validar(event)'	
	);

	$empleado= array(
		'name'  => 'empleado',
		'id'    => 'empleado',
		'value' => set_value('observaciones', ''),
		'type'  =>  'text',
		'class' =>  'form-control', 
		'style' =>  'width:263px; position: relative; left: 55px;',
		'placeholder' => 'Nombre de Empleado',
		'onkeypress'  => 'return validar(event)'	
	);


?> 

<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>
	</div>
	<div class="modal-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div id="mensaje2"></div>
			<div class="form-group">
				<label for="num_vale" class="col-sm-2 form-label">Número de vale: <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					  <?php echo form_input($num_vale);?> 
				</div>
				<label for="fecha" class="col-sm-2 form-label">Fecha: <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4" style="left: 20px;">
					  <?php echo form_input($fecha);?>
				</div>				
			</div>
			<div class="form-group">
				<label for="codigo" class="col-sm-2 form-label">N° de Vehículo: <span style="color: #F20A06; font-size: 15px;">*</span>: </label>
				<div class="col-sm-4">
					<select class="form-control" id="codigo" style="width: 70%; right: 40px;">
						<option value="0">Seleccione Número de vehículo</option>
						<?php
						if($codigo){
							foreach ($codigo as $c) {
								echo "<option value='".$c->PLACA."'>".$c->CODIGO_VEHICULO."</option>";
							}
						}
						?> 
					</select>
				</div>
				<label for="codigo" class="col-sm-2 form-label">Placa: <span style="color: #F20A06; font-size: 15px;">*</span> </label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="placa" id="placa" style="width: 65%; position: relative;left: 5%;" disabled="true">
				</div>
			</div>
			<div class="form-group">
				<label for="empleado" class="col-sm-2 form-label">Empleado:</label>
				<div class="col-sm-4">
					<?php
						echo form_input($empleado); 
				 	?>					
				</div>
				<label for="kilometraje" class="col-sm-2 form-label">Kilometraje<span style="color: #F20A06; font-size: 15px;">*</span></label>
				<div class="col-sm-4">
					<?php echo form_input($kilometraje);?>
				</div>	
			</div>
			<div class="form-group">		
				<label for="galones" class="col-sm-2 form-label">Cantidad de galones<span style="color: #F20A06; font-size: 15px;">*</span></label>
				<div class="col-sm-4">
					 <?php echo form_input($galones);?>
				</div>
					<label for="valor" class="col-sm-2 form-label ">Valor $: <span style="color: #F20A06; font-size: 15px;">*</span></label>
				<div class="col-sm-4">

					 <?php echo form_input($valor);?>
				</div>
			</div>				
		
			<div class="form-group ">
				<label for="observaciones" class="col-sm-2 form-label">Misi&oacute;n:</label>
				<div class="col-sm-4">
					 <?php echo form_textarea($observaciones);?> 
				</div>	
			</div>											
		</form>										
	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>		
	</div>
		
</div>	
</div>

<script type="text/javascript">

    $(document).ready(function(){
        
        $("#codigo").select2({
             dropdownParent: $("#modal_vales")
        });

        $("select#codigo").change(function() {
   			$("#placa").attr("disabled", true);
   			var placa = $("select#codigo").val();
   			console.log(placa);
   			$("#placa").val(placa);
   			
        });
        

      	$("#btnGuardar").click(function() {
      		var Vale = new Object();
      		var idVehiculo 	= document.getElementById("codigo");
      		// Vale.placa 			= idVehiculo.options[idVehiculo.selectedIndex].text;
      		Vale.id 			= '';
      		Vale.num_vale 		= $("input#num_vale").val();
      		Vale.fecha 			= $("input#fecha").val(); 		
      		Vale.placa 			= $("select#codigo").val();
      		Vale.num_equipo 	= idVehiculo.options[idVehiculo.selectedIndex].text;
      		Vale.empleado 		= $("input#empleado").val();
      		Vale.galones 		= $("input#galones").val();
      		Vale.valor 			= $("input#valor").val();
      		Vale.kilometraje    = $("input#kilometraje").val();
      		Vale.observaciones 	= $("#observaciones").val();

      		console.log(Vale);

      		if (Vale.codigo =="" || Vale.placa==0 || Vale.fecha==0 || Vale.num_equipo==0  || Vale.galones==0 || Vale.valor==0) {
      			$("#mensaje2").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese campos obligarios</div>");
      		}else{
      		$("#modal_vales").modal('hide');
      		$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
      		var DatosJson = JSON.stringify(Vale);
      		$.post(baseurl+'vales/registro_vales/guardar',{
      			valePost : DatosJson
      		}, function(data, textStatus) {
      			$("#mensaje").html(data.response_msg);
      			location.reload();
      		},"json").fail(function(response) {
      			$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
      			console.log('Error: ' + response.responseText);
      		});;
      		return false;
      	 }
      	});
    });


</script>