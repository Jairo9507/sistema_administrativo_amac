 <html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
            <head>

            </head>

            <body>
                <?php 
                    $filename = "Excelreport.xls";
                    header("Content-Type: application/vnd.ms-excel"); //mime type
                    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                    header("Cache-Control: max-age=0"); //no cache
                
                 ?>                 
                     <table >
                        <thead>
                            <tr>
                                <th>Nombre </th>
                                <th>Medico refiere</th>
                                <th>Medico receptor</th>
                                <th>Motivo referencia</th>
                                <th>Fecha referencia</th>
                                <th></th>                               
                            </tr>
                        </thead>
                        <tbody>
                         <?php
                                if ($referencias) {
                                    foreach ($referencias as $r) {
                                        $referenciaId=base64_encode($r->ID_REFERENCIA);
                                        echo "<tr>";
                                        echo "<td>".$r->nombre_paciente."</td>";
                                        echo "<td>".$r->nombre_doctor_refiere."</td>";
                                        echo "<td>".$r->nombre_doctor_recibe."</td>";
                                        echo "<td>".$r->referencia."</td>";
                                        echo "<td>".$r->fecha."</td>";
                                        echo "<td></td>";
                                        echo "</tr>";
                                    }
                                }
                            ?>
                              
                        </tbody>
                     </table>
            </body>
</html>    
	