<!--<script type="text/javascript" src="<?php echo base_url()?>js/JsValidacion.js"></script>-->
<script type="text/javascript">
	var baseurl = "<?php echo base_url() ?>"


</script>
<input type="hidden" name="id" id="id" value="<?php echo @$referencia[0]->ID_REFERENCIA?>">
<input type="hidden" name="idmedico" id="idMedicoRecibe" value="<?php echo @$referencia[0]->ID_MEDICO_RECIBE?>">
<?php 

$nombrecompleto=explode(" ",@$referencia[0]->nombre_doctor_recibe);

$primerNombre = array(
	'type'	=> 'text',
	'id'	=> 'primerNombre',
	'name'	=> 'txtNombreDoc',
	'class'	=> 'form-control',
	'onkeypress'  => 'return validarn(event);',
    'placeholder' =>  'Primer nombre médico refiere',
    'value'		=> set_value('primerNombre',@$nombrecompleto[0])   
);

$segundoNombre = array(
	'type'	=> 'text',
	'id'	=> 'segundoNombre',
	'name'	=> 'txtSegundoNombre',
	'class'	=> 'form-control',
	'onkeypress' => 'return validarn(evenet);',
	'placeholder' => 'Segundo nombre médico refiere',
    'value'		=> set_value('segundoNombre',@$nombrecompleto[1])	
);


$primerApellido = array(
	'type'	=> 'text',
	'id'	=> 'primerApellido',
	'name'	=> 'txtPrimerApellido',
	'class'	=> 'form-control',
	'onkeypress' => 'return validarn(evenet);',
	'placeholder' => 'Primer apellido médico refiere', 
    'value'		=> set_value('primerApellido',@$nombrecompleto[2])	
);

$segundoApellido = array(
	'type'	=> 'text',
	'id'	=> 'segundoApellido',
	'name'	=> 'txtSegundoApellido',
	'class'	=> 'form-control',
	'onkeypress' => 'return validarn(evenet);',
	'placeholder' => 'Segundo apellido médico refiere' ,
    'value'		=> set_value('segundoApellido',@$nombrecompleto[3])	
);




$correoDoc = array(
	'type'	=> 'email',
	'id'	=> 'correoDoc',
	'name'	=> 'txtCorreoDoc',
	'class'	=> 'form-control',
	'placeholder' =>  'example@globalclinik.com',
	'value'		=> set_value('CORREO_MEDICO_RECIBE',@$referencia[0]->CORREO_MEDICO_RECIBE)
);

$nombreClinica = array(
	'type'	=> 'text',
	'id'	=> 'nombreClinica',
	'name'	=> 'txtnombreClinica',
	'class'	=> 'form-control',
	'placeholder' =>  'Clinica del medico que se refiere',
	'value'		=> set_value('CORREO_MEDICO_RECIBE',@$referencia[0]->NOMBRE_CLINICA)
);

$motivoReferencia = array(
	'name'	=> 'motivoReferencia',
	'id'	=> 'motivoReferencia',
	'rows'	=> '4',
	'cols'	=> '4',
	'class'	=> 'form-control',
	'required' => 'true',
	'style' => 'resize: none' ,
	'value' => set_value('motivoReferencia',@$referencia[0]->referencia)
);

$si = array(
	'type'		=> 'radio',
	'value'		=> 'si', 
	'name'		=> 'confirmacion',
	'id' 		=> 'si',
	
);	

$no = array(
	'type'	=> 'radio',
	'value'	=> 'no', 
	'name'	=> 'confirmacion',
	'id' 	=> 'no'
);

 ?>
 <script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>

<div class="content-wrapper">
	<section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>					
	</section>
	<section class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="header-title">Datos</h3>
			</div>
			<div class="box-body">
						<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<!--<div class="form-group ">
				<label for='descripcion' class="form-label col-sm-12">¿El médico que se refiere el paciente esta registrado en el sistema?</label>
				<div class="col-sm-4 col-sm-offset-2">
					<div class="form-check form-check-inline col-sm-6">
						<label class="form-check-label">
							<?php 
								echo form_radio($si);
							?>Sí
						</label>						
					</div>
					<div class="form-check form-check-inline col-sm-6">
						<label class="form-check-label">
							<?php 
								echo form_radio($no);
							?>No
						</label>						
					</div>

				</div>
			</div>-->
			<?php if(@$referencia[0]->ID_MEDICO_RECIBE==NULL){ ?>
			<div class="form-group">
				<label for="txtNombreDoc" class="form-label col-sm-2" id="labelPrimernombre">Primer nombre:</label>
				<div class="col-md-4">
					<?php 
						echo form_input($primerNombre) 
					?>
				</div>
				<label for="txtSegundoNombre" class="form-label col-sm-2" id="labelSegundonombre">Segundo nombre:</label>		
				<div class="col-md-4">
					<?php 
						echo form_input($segundoNombre); 
					?>
				</div>								
			</div>
			<div class="form-group">
				<label for="txtNombreDoc" class="form-label col-sm-2" id="labelPrimerapellido">Primer apellido:</label>
				<div class="col-md-4">
					<?php 
						echo form_input($primerApellido) 
					?>
				</div>
				<label for="txtSegundoNombre" class="form-label col-sm-2" id="labelSegundoapellido">Segundo apellido:</label>		
				<div class="col-md-4">
					<?php 
						echo form_input($segundoApellido); 
					?>
				</div>								
			</div>
			<div class="form-group">
				<label for="txtCorreoDoc" class="form-label col-sm-2" id="labelCorreo">Correo:</label>
				<div class="col-md-4" id="divCorreoDoc">
					<?php 
						echo form_input($correoDoc) 
					?>
				</div>
				<label for="especialidad" class="form-label col-sm-2" id="labelEspecialidad">Especialidad:</label>		
				<div class="col-md-4" id="divlistEspecialidad">
					<select class="form-control" id="listEspecialidad">
						<option value="">Seleccione </option>
						<?php 
							if ($especialidades) {
								foreach ($especialidades as $e) {
									if ($e->DESCRIPCION==@$referencia[0]->ESPECIALIDAD) {
									echo "<option value='".$e->ID_CAT_ESPECIALIDAD."' selected>".$e->DESCRIPCION."</option>";
									} else {
									echo "<option value='".$e->ID_CAT_ESPECIALIDAD."'>".$e->DESCRIPCION."</option>";										
									}

								}
							}
						?>
					</select>
				</div>								
			</div>
			<div class="form-group">

				<label for="txtNombreClinica" class="form-label col-sm-2" id="labelClinica">Clinica:</label>
				<div class="col-md-4" id="divNombreClinica">
					<?php 
						echo form_input($nombreClinica) 
					?>
				</div>
				<label for="cmbPaciente" class="form-label col-sm-2" >Paciente:</label>
				<div class="col-md-4">
					<select class="form-control" id="listPacientes">
						<option value="">Seleccione</option>
						<?php 
							if ($pacientes) {
								foreach ($pacientes as $p) {
									if (@$referencia[0]->ID_PACIENTE==$p->ID_PACIENTE) {
										echo "<option value='".$p->ID_PACIENTE."' selected>".$p->nombre_completo."</option>";
									} else {
										echo "<option value='".$p->ID_PACIENTE."'>".$p->nombre_completo."</option>";
									}
									
									
								}
							}
						 ?>
					</select>
				</div>
			</div>

		<?php } else { ?>
			<div class="form-group">
				<label for="especialidad" class="form-label col-sm-2" id="labelEspecialidad">Especialidad:</label>
				<div class="col-md-4" id="divlistEspecialidad">
					<select class="form-control" id="listEspecialidad">
						<option value="">Seleccione </option>
						<?php 
							if ($especialidades) {
								foreach ($especialidades as $e) {
									if (@$referencia[0]->ID_CAT_ESPECIALIDAD==$e->ID_CAT_ESPECIALIDAD) {
										echo "<option value='".$e->ID_CAT_ESPECIALIDAD."' selected>".$e->DESCRIPCION."</option>";
									} else {
										echo "<option value='".$e->ID_CAT_ESPECIALIDAD."'>".$e->DESCRIPCION."</option>";
									}
									
									
								}
							}
						?>
					</select>
				</div>				
			</div>

			<div class="form-group">
				
				<label for="doctores" class="form-label col-sm-2" id="labelDoctor">Doctor:</label>
				<div class="col-md-4" id="divlistDoctor">
					<select class="form-control" id="listDoctores" disabled="true">
						<option value="">Seleccione </option>
						<?php 
							if ($medicos) {
								foreach ($medicos as $m) {
									if (@$referencia[0]->ID_MEDICO_RECIBE==$m->ID_MEDICO) {
										echo "<option value='".$m->ID_MEDICO."' selected>".$m->nombre_completo."</option>";
									} else {
										echo "<option value='".$m->ID_MEDICO."'>".$m->nombre_completo."</option>";
									}
									
								}
							} 
							
						 ?>
					</select>
				</div>
				<label for="cmbPaciente" class="form-label col-sm-2" >Paciente:</label>
				<div class="col-md-4">
					<select class="form-control" id="listPacientes">
						<option value="">Seleccione</option>
						<?php 
							if ($pacientes) {
								foreach ($pacientes as $p) {
									if (@$referencia[0]->ID_PACIENTE==$p->ID_PACIENTE) {
										echo "<option value='".$p->ID_PACIENTE."' selected>".$p->nombre_completo."</option>";
									} else {
										echo "<option value='".$p->ID_PACIENTE."'>".$p->nombre_completo."</option>";
									}
									
									
								}
							}
						 ?>
					</select>
				</div>
			</div>
			<?php } ?>
			
							
			<div class="form-group">
				<label for="Referencia" class="form-label col-sm-2" >Motivo referencia:</label>	
				<div class="col-sm-12">
					<?php echo form_textarea($motivoReferencia) ?>
				</div>							
			</div>

		</form>
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar();">Cerrar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonReferencia.js');?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"clinica/referencia"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"clinica/referencia"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    } 
</script>
