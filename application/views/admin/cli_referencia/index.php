<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

 ?>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"clinica/referencia"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"clinica/referencia"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
function ImprimirPDF(ReferenciaId){
	document.getElementById('mensaje_registro').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>";
    	 var Referencia 		 = new Object();
		Referencia.Id      	 = ReferenciaId;
		var DatosJson = JSON.stringify(Referencia);
		console.log(DatosJson);
		$.post(baseurl+"referencia/referenciaPost",
		{
			Referenciapost: DatosJson
		}, function(data,textStatus){
			console.log(JSON.parse(data));
			var ventimp=window.open(' ','popimpr', "width=400,height=400");
			ventimp.document.write(JSON.parse(data));
			ventimp.document.close();ventimp.print();
			ventimp.close();
		$("#mensaje_registro").html("<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Referencia Imprimida Correctamente, La Información de Actualizara en 5 Segundos <meta http-equiv='refresh' content='5'></div>");	
		}).fail(function(response){	
		console.log('Error: ' + response.responseText);
		});;
		
}
function EliminarReferencia(Paciente,ReferenciaId){
	alertify.confirm("Eliminar Referencia","Eliminar la referencia del Paciente "+Paciente+", Recuerda Una vez Eliminado No podras Recuperarlo",function(){
		document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando Referencia...</center></div></div>";
    	 var Referencia 		 = new Object();
		Referencia.id      	 = ReferenciaId;
		var DatosJson = JSON.stringify(Referencia);
		console.log(DatosJson);
		$.post(baseurl+"clinica/referencia/eliminar",
		{
			ReferenciaPost: DatosJson
		}, function(data, textStatus){
			$("#mensaje").html(data.response_msg);
		},"json"
		).fail(function(response){
		console.log('Error: ' + response.responseText);
		});;

	},function(){});
}
</script>
<div class="content-wrapper">
	<section class="content-header"> 
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="header-title">Lista de Referencias</h3>
			</div>
                	<div class="row">
                		<div class="col-sm-2 pull-right">
                            <a href="referencia/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                		</div>
                	</div>			
			<div class="box-body">				
				<div class="row">
					<div class="col-sm-12">
					 <table id="referencias" class="table table-bordered table-striped">
					 	<thead>
					 		<tr>
					 			<th>Nombre paciente</th>
					 			<th>Medico refiere</th>
					 			<th>Medico receptor</th>
					 			<th>Motivo referencia</th>
					 			<th>Fecha referencia</th>
					 			<th></th>					 			
					 		</tr>
					 	</thead>
					 	<tbody>
						 <?php
						 		if ($referencias) {
						 			foreach ($referencias as $r) {
						 				$referenciaId=base64_encode($r->ID_REFERENCIA);
						 				echo "<tr>";
						 				echo "<td>".$r->nombre_paciente."</td>";
						 				echo "<td>".$r->nombre_doctor_refiere."</td>";
						 				echo "<td>".$r->nombre_doctor_recibe."</td>";
						 				echo "<td>".$r->referencia."</td>";
						 				echo "<td>".date('d-m-Y',strtotime($r->fecha))."</td>";
						 				echo "<td><a href='referencia/editar/".$referenciaId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>&nbsp; ";
						 			
							?>
									 <button type="button" onclick="EliminarReferencia('<?php echo $r->nombre_paciente;?>','<?php echo $referenciaId;?>');" title="Eliminar Usuario" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>&nbsp;  
							<?php 		//echo '  <a class="button"  href="referencia/excel_create" class="btn btn-blueviolet btn-xs btn-mini"> Excel Download</a>';
										echo '<a href="referencia/vistaPDF/'.$referenciaId.'" data-target="#modal_referencia" data-toggle="modal" data-backdrop="static" data-keyboard="false"><button type="button" title="PDF" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span></button></a></td>';
						 				echo "</tr>";
						 			}
						 		}
							 ?>
					 	</tbody>
					 </table>						
					</div>
				</div>
			</div>
			
		</div>
	</section>
	
</div>
<div class="modal fade text-center"  id="modal_nuevo">
  <div class="modal-dialog" >
    <div class="modal-content">
    </div>
  </div>
</div>
<div class="modal fade text-center"  id="modal_referencia">
  <div class="modal-dialog" >
    <div class="modal-content">
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#referencias").DataTable({
		     paging      : true,
		      lengthChange: true,
		      searching   : true,
		      ordering    : true,
		      "order" : [],
		      info        : true,
		      autoWidth   : false,
		      language: {
		        search:'Buscar:',
		        order: 'Mostrar Entradas',
		        paginate: {
		            first:"Primero",
		            previous:"Anterior",
		            next:"Siguiente",
		            last:"Ultimo"
		        },
		        emptyTable: "No hay informacion disponible",
		        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
		        lengthMenu:"Mostrar _MENU_ Entradas",
		        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
		      }    
		    });
	});
</script>