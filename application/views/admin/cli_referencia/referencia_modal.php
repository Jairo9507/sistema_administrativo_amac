
<!--<script type="text/javascript" src="<?php echo base_url()?>js/JsValidacion.js"></script>-->
<script type="text/javascript">
	var baseurl = "<?php echo base_url() ?>"
	 function reload() {
    window.location.reload();
  }
  $(document).ready(function(){
	
  });
</script>
<input type="hidden" name="id" id="id" value="<?php echo @$referencia[0]->ID_REFERENCIA?>">
<?php 


$primerNombre = array(
	'type'	=> 'text',
	'id'	=> 'primerNombre',
	'name'	=> 'txtNombreDoc',
	'class'	=> 'form-control',
	'onkeypress'  => 'return validarn(event);',
    'placeholder' =>  'Primer nombre médico refiere'   
);

$segundoNombre = array(
	'type'	=> 'text',
	'id'	=> 'segundoNombre',
	'name'	=> 'txtSegundoNombre',
	'class'	=> 'form-control',
	'onkeypress' => 'return validarn(evenet);',
	'placeholder' => 'Segundo nombre médico refiere'
);


$primerApellido = array(
	'type'	=> 'text',
	'id'	=> 'primerApellido',
	'name'	=> 'txtPrimerApellido',
	'class'	=> 'form-control',
	'onkeypress' => 'return validarn(evenet);',
	'placeholder' => 'Primer apellido médico refiere'
);

$segundoApellido = array(
	'type'	=> 'text',
	'id'	=> 'segundoApellido',
	'name'	=> 'txtSegundoApellido',
	'class'	=> 'form-control',
	'onkeypress' => 'return validarn(evenet);',
	'placeholder' => 'Segundo apellido médico refiere' 
);




$correoDoc = array(
	'type'	=> 'email',
	'id'	=> 'correoDoc',
	'name'	=> 'txtCorreoDoc',
	'class'	=> 'form-control',
	'placeholder' =>  'example@globalclinik.com'
);

$nombreClinica = array(
	'type'	=> 'text',
	'id'	=> 'nombreClinica',
	'name'	=> 'txtnombreClinica',
	'class'	=> 'form-control',
	'placeholder' =>  'Clinica del medico que se refiere'
);

$motivoReferencia = array(
	'name'	=> 'motivoReferencia',
	'id'	=> 'motivoReferencia',
	'rows'	=> '4',
	'cols'	=> '4',
	'class'	=> 'form-control',
	'required' => 'true',
	'style' => 'resize: none' 
);

$si = array(
	'type'		=> 'radio',
	'value'		=> 'si', 
	'name'		=> 'confirmacion',
	'id' 		=> 'si',
	'checked'	=> 'true',
	'class'		=>'form-check-input'
);	

$no = array(
	'type'	=> 'radio',
	'value'	=> 'no', 
	'name'	=> 'confirmacion',
	'id' 	=> 'no',
	'class'		=>'form-check-input'
);

 ?>
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
	</div>
	<div class="modal-body">
		
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div class="form-group ">
				<label for='descripcion' class="form-label col-sm-12">¿El médico que se refiere el paciente esta registrado en el sistema?</label>
				<div class="col-sm-4 col-sm-offset-2">
					<div class="form-check form-check-inline col-sm-6">
						<label class="form-check-label">
							<?php 
								echo form_radio($si);
							?>Sí
						</label>						
					</div>
					<div class="form-check form-check-inline col-sm-6">
						<label class="form-check-label">
							<?php 
								echo form_radio($no);
							?>No
						</label>						
					</div>

				</div>
			</div>
			<div class="form-group">
				<label for="txtNombreDoc" class="form-label col-sm-2" id="labelPrimernombre">Primer nombre:</label>
				<div class="col-md-4">
					<?php 
						echo form_input($primerNombre) 
					?>
				</div>
				<label for="txtSegundoNombre" class="form-label col-sm-2" id="labelSegundonombre">Segundo nombre:</label>		
				<div class="col-md-4">
					<?php 
						echo form_input($segundoNombre); 
					?>
				</div>								
			</div>
			<div class="form-group">
				<label for="txtNombreDoc" class="form-label col-sm-2" id="labelPrimerapellido">Primer apellido:</label>
				<div class="col-md-4">
					<?php 
						echo form_input($primerApellido) 
					?>
				</div>
				<label for="txtSegundoNombre" class="form-label col-sm-2" id="labelSegundoapellido">Segundo apellido:</label>		
				<div class="col-md-4">
					<?php 
						echo form_input($segundoApellido); 
					?>
				</div>								
			</div>
			<div class="form-group">
				<label for="txtCorreoDoc" class="form-label col-sm-2" id="labelCorreo">Correo:</label>
				<div class="col-md-4" id="divCorreoDoc">
					<?php 
						echo form_input($correoDoc) 
					?>
				</div>
				<label for="especialidad" class="form-label col-sm-2" id="labelEspecialidad">Especialidad:</label>		
				<div class="col-md-4" id="divlistEspecialidad">
					<select class="form-control" id="listEspecialidad">
						<option value="">Seleccione </option>
						<?php 
							if ($especialidades) {
								foreach ($especialidades as $e) {
									echo "<option value='".$e->ID_CAT_ESPECIALIDAD."'>".$e->DESCRIPCION."</option>";
								}
							}
						?>
					</select>
				</div>								
			</div>
			<div class="form-group">
				<label for="doctores" class="form-label col-sm-2" id="labelDoctor">Doctor:</label>
				<div class="col-md-4" id="divlistDoctor">
					<select class="form-control" id="listDoctores" disabled="true">
						<option value="">Seleccione </option>
					</select>
				</div>
				<label for="cmbPaciente" class="form-label col-sm-2" >Paciente:</label>
				<div class="col-md-4">
					<select class="form-control" id="listPacientes">
						<option value="">Seleccione</option>
						<?php 
							if ($pacientes) {
								foreach ($pacientes as $p) {
									echo "<option value='".$p->ID_PACIENTE."'>".$p->nombre_completo."</option>";
								}
							}
						 ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="txtNombreClinica" class="form-label col-sm-2" id="labelClinica">Clinica:</label>
				<div class="col-md-4" id="divNombreClinica">
					<?php 
						echo form_input($nombreClinica) 
					?>
				</div>
	
								
			</div>
			<div class="form-group">
				<label for="Referencia" class="form-label col-sm-2" >Motivo referencia:</label>	
				<div class="col-sm-12">
					<?php echo form_textarea($motivoReferencia) ?>
				</div>							
			</div>

		</form>
	</div>
	<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url();?>js/JsonReferencia.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#listPacientes").select2();
	});
</script>