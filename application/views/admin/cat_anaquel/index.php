<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"catalogos/cat_anaqueles"
        } );
    }
    function eliminarAnaquel(id,nombre){
    	alertify.confirm("Eliminar anaquel","¿Seguro de eliminar el anaquel "+nombre+"?",function(){
            document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
			var anaquel=new Object();
			anaquel.id=id;
			console.log(anaquel);
			var DatosJson=JSON.stringify(anaquel);
			$.post(baseurl+'catalogos/cat_anaqueles/eliminar',{
				AnaquelPost:DatosJson
			},function(data,textStatus){
				$("#mensaje").html(data.response_msg);
			},"json").fail(function(response){
              $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
 
			});

    	},function(){

    	});
    }
</script>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="content-title">Lista de Anaqueles</h3>
			</div>
                	<div class="row">
                		<div class="col-sm-2 pull-right">
                            <a href="cat_anaqueles/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                		</div>
                	</div>						
			<div class="box-body">
				<table id="anaqueles" class="table table-bordered table-striped">
					<thead>
						<th>C&oacute;digo de anaquel</th>
						<th>Descripcion</th>
						<th>Estante</th>
						<th>Numero</th>
						<th>Estado</th>
						<th></th>
					</thead>
					<tbody>
						<?php 
							if ($anaqueles) {
								foreach ($anaqueles as $a) {
									echo "<tr>";
									$anaquelId=base64_encode($a->ID_ANAQUEL);
									echo "<td>".$a->CODIGO."</td>";
									echo "<td>".$a->DESCRIPCION."</td>";
									echo "<td>".$a->ESTANTE."</td>";
									echo "<td>".$a->NUMERO_ANAQUEL."</td>";
									if ($a->ESTADO==1) {
										echo "<td>Activo</td>";
									}
									echo "<td><a href='cat_anaqueles/editar/".$anaquelId."' ><button type='button' title='Editar Anaquel' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
								
						 ?>
                				<button type="button"  title="Eliminar Anaquel" onclick="eliminarAnaquel('<?php echo $anaquelId?>','<?php echo $a->CODIGO ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>	
                		<?php 
                			echo "</td>";
                			echo "</tr>";
                			}
						}
                		 ?>					 
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#anaqueles").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
		});
	});
</script>
<div class="modal fade text-center" id="modal_nuevo">
  <div class="modal-dialog" >
    <div class="modal-content">
    </div>
  </div>
</div>