<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 
	$codigo= array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',@$anaquel[0]->CODIGO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'ES-01',
		'disabled'	=>true

	);

	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('descripcion',@$anaquel[0]->DESCRIPCION),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion',
		'onkeypress'	=> 'return validarn(event)'

	);

	$numero_anaquel= array(
		'name' 		=> 'numero_anaquel',
		'id'		=> 'numero_anaquel',
		'value'		=> set_value('numero_anaquel',@$anaquel[0]->NUMERO_ANAQUEL),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0,
		'max'	=>6,
		'onkeypress'	=> 'return validarNumeros(event)'
	);


 ?>
 <input type="hidden" name="id" id="id" value="<?php echo @$anaquel[0]->ID_ANAQUEL?>">
 <div class="content-wrapper">
 	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?> 		
 	</section>
 	<section class="content">
 		<div class="box">
 			<div id="mensaje"></div>
 			<div class="box-header">
				<h3 class="content-title">Datos</h3> 				
 			</div>
 			<div class="box-body">
 						<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div class="form-group">
				<label class="form-label col-sm-4" for="codigo">C&oacute;digo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-6">
					<?php echo form_input($codigo); ?>
				</div>		
			</div>
			<div class="form-group">
				<label class="form-label col-sm-4" for="descripcion">Descripci&oacute;n:</label>
				<div class="col-sm-6">
					<?php echo form_textarea($descripcion); ?>
				</div>						
			</div>
			<div class="form-group">
				<label class="form-label col-sm-4" for="numero_anaquel">Numero<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-6">
					<?php echo form_input($numero_anaquel); ?>
				</div>				
			</div>
			<div class="form-group">
				<label class="form-label col-sm-4" for="estante">Estante<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-6">
					<select class="form-control" id="estante_id">
						<option value="0">Seleccione un estante</option>
							<?php 
								if ($estantes) {
									foreach ($estantes as $e) {
										if($e->ID_ESTANTE==@$anaquel[0]->ID_ESTANTE){
											echo "<option value='".$e->ID_ESTANTE."' selected>".$e->CODIGO_ESTANTE." ".$e->DESCRIPCION."</option>";
										}else {
											echo "<option value='".$e->ID_ESTANTE."'>".$e->CODIGO_ESTANTE." ".$e->DESCRIPCION."</option>";
										}
									}
								}
							 ?>
					</select>
				</div>				
			</div>
		</form>
 			</div>
 			<div class="box-footer">
				<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
				<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	 				
 			</div>
 			
 		</div>
 		
 	</section>
 </div>
 <script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_anaqueles"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"catalogos/cat_anaqueles"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsonAnaquel.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>