<?php
defined('BASEPATH') OR exit('No direct script access allowed');


?>

<?php 
	$general=array(
		'name' 		=> 'general',
		'id'		=> 'general',
		'value'		=> set_value('general',@$consulta[0]->DATOS_GENERALES),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'antechan form-control'
	);
	
	$familiar=array(
		'name' 		=> 'familiar',
		'id'		=> 'hereditario_familiar',
		'value'		=> set_value('familiar',@$consulta[0]->HEREDITARIO_FAMILIAR),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'antechan form-control'
	);

	$personales_no_patologicos=array(
		'name' 		=> 'personales_no_patologicos',
		'id'		=> 'personales_no_patologicos',
		'value'		=> set_value('personales_no_patologicos',@$consulta[0]->PERSONAL_NOPATOLOGICO),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'antechan form-control'
	);

	$personales_patologicos=array(
		'name' 		=> 'personales_patologicos',
		'id'		=> 'personales_patologicos',
		'value'		=> set_value('personales_patologicos',@$consulta[0]->PERSONAL_PATOLOGICO),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'antechan form-control'
	);

	$otra_alergia=array(
		'name' 		=> 'otra_alergia',
		'id'		=> 'otra_alergia',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'alechan form-control',
		'disabled'	=>''
	);


	$primera_menstruacion=array(
		'name' 		=> 'primera_menstruacion',
		'id'		=> 'primera_menstruacion',
		'value'		=> set_value('primera_menstruacion',@$consulta[0]->PRIMERA_MENSTRUACION),
		'type'		=> 'number',
		'class'		=> 'menchan form-control',
		'min'		=>0
	);

	$caracteristicas = array(
	  '' => 'Seleccionar',
	  'Amenorrea' => 'Amenorrea',
	  'Hipermenorrea' => 'Hipermenorrea',
	  'Menorragia' => 'Menorragia',
	  'Menorrea' => 'Menorrea',
	  'Oligomenorrea'=>'Oligomenorrea',
	  'Polihipermenorrea'=>'Polihipermenorrea',
	  'Polimenorrea'=>'Polimenorrea'
	);		

	$ivsa=array(
		'name' 		=> 'ivsa',
		'id'		=> 'ivsa',
		'value'		=> set_value('ivsa',@$consulta[0]->IVSA),
		'type'		=> 'number',
		'class'		=> 'menchan form-control',
		'min'		=>0
	);

	$menopausia=array(
		'name' 		=> 'menopausia',
		'id'		=> 'menopausia',
		'value'		=> set_value('menopausia',@$consulta[0]->MENOPAUSIA),
		'type'		=> 'number',
		'class'		=> 'menchan form-control',
		'min'		=>0
	);

	$otros_menstruacion=array(
		'name' 		=> 'otros_menstruacion',
		'id'		=> 'otros_menstruacion',
		'value'		=> set_value('otros_menstruacion',@$consulta[0]->OTROS_MENSTRUACION),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'menchan form-control'
	);

	$total_embarazo=array(
		'name' 		=> 'total_embarazo',
		'id'		=> 'total_embarazo',
		'value'		=> set_value('total_embarazo',@$consulta[0]->TOTAL_EMBARAZO),
		'type'		=> 'number',
		'class'		=> 'embchan form-control',
		'min'		=>0
	);

	$no_partos=array(
		'name' 		=> 'no_partos',
		'id'		=> 'no_partos',
		'value'		=> set_value('numero_partos',@$consulta[0]->NUMERO_PARTOS),
		'type'		=> 'number',
		'class'		=> 'embchan form-control',
		'min'		=>0
	);	

	$no_cesareas=array(
		'name' 		=> 'no_cesareas',
		'id'		=> 'no_cesareas',
		'value'		=> set_value('numero_cesareas',@$consulta[0]->NUMERO_CESAREAS),
		'type'		=> 'number',
		'class'		=> 'embchan form-control',
		'min'		=>0
	);	

	$no_abortos=array(
		'name' 		=> 'no_abortos',
		'id'		=> 'no_abortos',
		'value'		=> set_value('numero_abortos',@$consulta[0]->NUMERO_ABORTOS),
		'type'		=> 'number',
		'class'		=> 'embchan form-control',
		'min'		=>0
	);	

	$nacidos_vivos=array(
		'name' 		=> 'nacidos_vivos',
		'id'		=> 'nacidos_vivos',
		'value'		=> set_value('nacido_vivos',@$consulta[0]->NACIDO_VIVOS),
		'type'		=> 'number',
		'class'		=> 'embchan form-control',
		'min'		=>0
	);	

	$vivos_actuales=array(
		'name' 		=> 'vivos_actuales',
		'id'		=> 'vivos_actuales',
		'value'		=> set_value('vivos_actuales',@$consulta[0]->VIVOS_ACTUALES),
		'type'		=> 'number',
		'class'		=> 'embchan form-control',
		'min'		=>0
	);	

	$otros_embarazo=array(
		'name' 		=> 'otros_embarazo',
		'id'		=> 'otros_embarazo',
		'value'		=> set_value('otros_embarazo',@$consulta[0]->OTROS_EMBARAZO),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'embchan form-control'
	);

	$fecha_ultima_papanicolau=array(
		'name' 		=> 'fecha_ultima_papanicolau',
		'id'		=> 'fecha_ultima_papanicolau',
		'value'		=> set_value('fecha_ultima_papanicolau',date('Y-m-d',strtotime(@$consulta[0]->ULTIMA_PAPANICOLAU))),
		'type'		=> 'date',
		'class'		=> 'intechan form-control'
	);		

	$fecha_ultima_colposcopia=array(
		'name' 		=> 'fecha_ultima_colposcopia',
		'id'		=> 'fecha_ultima_colposcopia',
		'value'		=> set_value('fecha_ultima_colposcopia',date('Y-m-d',strtotime(@$consulta[0]->ULTIMA_COLPOSCOPIA))),
		'type'		=> 'date',
		'class'		=> 'intechan form-control'
	);	

	$fecha_ultima_mamografia=array(
		'name' 		=> 'fecha_ultima_mamografia',
		'id'		=> 'fecha_ultima_mamografia',
		'value'		=> set_value('ultima_mamografia',date('Y-m-d',strtotime(@$consulta[0]->ULTIMA_MAMOGRAFIA))),
		'type'		=> 'date',
		'class'		=> 'intechan form-control'
	);		

	$parejas_sexuales=array(
		'name' 		=> 'parejas_sexuales',
		'id'		=> 'parejas_sexuales',
		'value'		=> set_value('parejas_sexuales',@$consulta[0]->PAREJAS_SEXUALES),
		'type'		=> 'number',
		'class'		=> 'intechan form-control',
		'min'		=>0
	);

	$metodos_anticonceptivos=array(
		'name' 		=> 'metodos_anticonceptivos',
		'id'		=> 'metodos_anticonceptivos',
		'value'		=> set_value('metodos_anticonceptivos',@$consulta[0]->METODOS_ANTICONCEPTIVOS),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'intechan form-control'
	);

	$flujos_vaginales=array(
		'name' 		=> 'flujos_vaginales',
		'id'		=> 'flujos_vaginales',
		'value'		=> set_value('flujos_vaginales',@$consulta[0]->FLUJOS_VAGINALES),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'intechan form-control'
	);

	$procedimientos_ginecologicos=array(
		'name' 		=> 'procedimientos_ginecologicos',
		'id'		=> 'procedimientos_ginecologicos',
		'value'		=> set_value('procedimientos_ginecologicos',@$consulta[0]->PROCEDIMIENTOS_GINECOLOGICOS),
		'type'		=> 'textarea',
		'rows'		=>5,
		'class'		=> 'intechan form-control'
	);	

	$habitos=array(
		'name' 		=> 'habitos',
		'id'		=> 'habitos',
		'value'		=> set_value('habitos',@$consulta[0]->HABITOS),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'intechan form-control'
	);

	$cirujias_previas=array(
		'name' 		=> 'cirujias_previas',
		'id'		=> 'cirujias_previas',
		'value'		=> set_value('cirujias_previas',@$consulta[0]->CIRUJIAS_PREVIAS),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'intechan form-control'
	);	

	$otros_intereses=array(
		'name' 		=> 'otros_intereses',
		'id'		=> 'otros_intereses',
		'value'		=> set_value('otros_intereses',@$consulta[0]->OTROS_INTERES),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'intechan form-control'
	);

	$sintoma=array(
		'name' 		=> 'sintoma',
		'id'		=> 'sintoma',
		'value'		=> set_value('sintoma',@$consulta[0]->SINTOMA),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'conschan form-control'
	);

	$observacion=array(
		'name' 		=> 'observacion',
		'id'		=> 'observacion',
		'value'		=> set_value('observacion',@$consulta[0]->OBSERVACION),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'conschan form-control'
	);	

	$tratamiento_previo=array(
		'name' 		=> 'tratamiento_previo',
		'id'		=> 'tratamiento_previo',
		'value'		=> set_value('tratamiento_previo',@$consulta[0]->TRATAMIENTO_PREVIO),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'conschan form-control'
	);	

	$peso=array(
		'name' 		=> 'peso',
		'id'		=> 'peso',
		'value'		=> set_value('peso',@$consulta[0]->PESO),
		'type'		=> 'number',
		'placeholder'=>'KG',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$talla=array(
		'name' 		=> 'talla',
		'id'		=> 'talla',
		'value'		=> set_value('talla',@$consulta[0]->TALLA),
		'type'		=> 'number',
		'placeholder'=>'M',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);		

	$cadera=array(
		'name' 		=> 'cadera',
		'id'		=> 'cadera',
		'value'		=> set_value('cadera',@$consulta[0]->CADERA),
		'type'		=> 'number',
		'placeholder'=>'cm',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);		

	$cintura=array(
		'name' 		=> 'cintura',
		'id'		=> 'cintura',
		'value'		=> set_value('cintura',@$consulta[0]->CINTURA),
		'type'		=> 'number',
		'placeholder'=>'cm',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);		

	$imc=array(
		'name' 		=> 'imc',
		'id'		=> 'imc',
		'value'		=> set_value('imc',@$consulta[0]->IMC),
		'type'		=> 'number',
		'placeholder'=>'',
		'readonly'	=>'true',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);			

	$temperatura=array(
		'name' 		=> 'temperatura',
		'id'		=> 'temperatura',
		'value'		=> set_value('temperatura',@$consulta[0]->TEMPERATURA),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);		

	$presion_arterial=array(
		'name' 		=> 'presion_arterial',
		'id'		=> 'presion_arterial',
		'value'		=> set_value('presion_arterial',@$consulta[0]->PRESION_ARTERIAL),
		'type'		=> 'text',
		'placeholder'=>'mm/Hg',
		'class'		=> 'explochan form-control'
	);		

	$frecuencia_cardiaca=array(
		'name' 		=> 'frecuencia_cardiaca',
		'id'		=> 'frecuencia_cardiaca',
		'value'		=> set_value('frecuencia_cardiaca',@$consulta[0]->FRECUENCIA_CARDIACA),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);

	$frecuencia_respiratoria=array(
		'name' 		=> 'frecuencia_respiratoria',
		'id'		=> 'frecuencia_respiratoria',
		'value'		=> set_value('frecuencia_respiratoria',@$consulta[0]->FRECUENCIA_RESPIRATORIA),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);			

	$cintura_abdominal=array(
		'name' 		=> 'cintura_abdominal',
		'id'		=> 'cintura_abdominal',
		'value'		=> set_value('cintura_abdominal',@$consulta[0]->CINTURA_ABDOMINAL),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$pliegue_abdominal=array(
		'name' 		=> 'pliegue_abdominal',
		'id'		=> 'pliegue_abdominal',
		'value'		=> set_value('pliegue_abdominal',@$consulta[0]->PLIEGUE_ABDOMINAL),
		'type'		=> 'number',
		'placeholder'=>'mm/Hg',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$grasa_corporal=array(
		'name' 		=> 'grasa_corporal',
		'id'		=> 'grasa_corporal',
		'value'		=> set_value('grasa_corporal',@$consulta[0]->GRASA_CORPORAL),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$fuerza_mano=array(
		'name' 		=> 'fuerza_mano',
		'id'		=> 'fuerza_mano',
		'value'		=> set_value('fuerza_mano',@$consulta[0]->FUERZA_MANO),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$trigliceridos=array(
		'name' 		=> 'trigliceridos',
		'id'		=> 'trigliceridos',
		'value'		=> set_value('trigliceridos',@$consulta[0]->TRIGLICERIDOS),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$colesterol=array(
		'name' 		=> 'colesterol',
		'id'		=> 'colesterol',
		'value'		=> set_value('colesterol',@$consulta[0]->COLESTEROL),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$hdl=array(
		'name' 		=> 'hdl',
		'id'		=> 'hdl',
		'value'		=> set_value('hdl',@$consulta[0]->HDL),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$ldl=array(
		'name' 		=> 'ldl',
		'id'		=> 'ldl',
		'value'		=> set_value('ldl',@$consulta[0]->LDL),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$creatinina=array(
		'name' 		=> 'creatinina',
		'id'		=> 'creatinina',
		'value'		=> set_value('creatinina',@$consulta[0]->CREATININA),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$acido_urico=array(
		'name' 		=> 'acido_urico',
		'id'		=> 'acido_urico',
		'value'		=> set_value('acido_urico',@$consulta[0]->ACIDO_URICO),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);		

	$hemoglobina=array(
		'name' 		=> 'hemoglobina',
		'id'		=> 'hemoglobina',
		'value'		=> set_value('hemoglobina',@$consulta[0]->HEMOGLOBINA),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$cabeza=array(
		'name' 		=> 'cabeza',
		'id'		=> 'cabeza',
		'value'		=> set_value('cabeza',@$consulta[0]->CABEZA),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);	

	$cuello=array(
		'name' 		=> 'cuello',
		'id'		=> 'cuello',
		'value'		=> set_value('cuello',@$consulta[0]->CUELLO),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);	

	$torax=array(
		'name' 		=> 'torax',
		'id'		=> 'torax',
		'value'		=> set_value('torax',@$consulta[0]->TORAX),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);

	$abdomen=array(
		'name' 		=> 'abdomen',
		'id'		=> 'abdomen',
		'value'		=> set_value('abdomen',@$consulta[0]->ABDOMEN),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);

	$pulso=array(
		'name' 		=> 'pulso',
		'id'		=> 'pulso',
		'value'		=> set_value('pulso',@$consulta[0]->PULSOS),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);					

	$facies=array(
		'name' 		=> 'facies',
		'id'		=> 'facies',
		'value'		=> set_value('facies',@$consulta[0]->FACIES),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);		

	$miembros_inferiores=array(
		'name' 		=> 'miembros_inferiores',
		'id'		=> 'miembros_inferiores',
		'value'		=> set_value('miembros_inferiores',@$consulta[0]->MIEMBROS_INFERIORES),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);		

	$pie=array(
		'name' 		=> 'pie',
		'id'		=> 'pie',
		'value'		=> set_value('pie',@$consulta[0]->PIE),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);		

	$genitales=array(
		'name' 		=> 'genitales',
		'id'		=> 'genitales',
		'value'		=> set_value('genitales',@$consulta[0]->GENITALES),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);	

	$otro_exploracion=array(
		'name' 		=> 'otro_exploracion',
		'id'		=> 'otro_exploracion',
		'value'		=> set_value('otro_exploracion',@$consulta[0]->OTROS_EXPLORACION),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);	

	$glucosa_laboratorio=array(
		'name' 		=> 'glucosa_laboratorio',
		'id'		=> 'glucosa_laboratorio',
		'value'		=> set_value('codigo',''),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$glucosa_capilar=array(
		'name' 		=> 'glucosa_capilar',
		'id'		=> 'glucosa_capilar',
		'value'		=> set_value('glucosa_capilar',@$consulta[0]->GLUCOSA_CAPILAR),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$glicosalida=array(
		'name' 		=> 'glicosalida',
		'id'		=> 'glicosalida',
		'value'		=> set_value('glicosalida',@$consulta[0]->GLICOSALIDA),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);	

	$transaminasa=array(
		'name' 		=> 'transaminasa',
		'id'		=> 'transaminasa',
		'value'		=> set_value('transaminasa',@$consulta[0]->TRANSAMINASA),
		'type'		=> 'number',
		'placeholder'=>'',
		'class'		=> 'explochan form-control',
		'min'		=>0
	);		

	$observaciones_gluscosa=array(
		'name' 		=> 'observaciones_glucosa',
		'id'		=> 'observaciones_glucosa',
		'value'		=> set_value('observaciones_glucosa',@$consulta[0]->OBSERVACIONES),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'explochan form-control'
	);

	$diagnostico=array(
		'name' 		=> 'diagnostico',
		'id'		=> 'diagnostico',
		'value'		=> set_value('diagnostico',@$consulta[0]->DIAGNOSTICO),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'conschan form-control'
	);

	$indicaciones_consulta=array(
		'name' 		=> 'indicaciones_consulta',
		'id'		=> 'indicaciones_consulta',
		'value'		=> set_value('indicaciones_consulta',@$consulta[0]->INDICACIONES),
		'type'		=> 'textarea',
		'placeholder'=>'',
		'rows'		=>3,
		'class'		=> 'conschan form-control'
	);

	$presentacion=array(
		'name' 		=> 'presentacion',
		'id'		=> 'presentacion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'placeholder'=>'',
		'disabled'	=>'',
		'class'		=> 'medichan form-control'
	);

	$dosis=array(
		'name' 		=> 'dosis',
		'id'		=> 'dosis',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'placeholder'=>'',
		'class'		=> 'medichan form-control',
		'disabled'	=>'true',
	);

	$indicaciones=array(
		'name' 		=> 'indicaciones',
		'id'		=> 'indicaciones',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'placeholder'=>'',
		'class'		=> 'medichan form-control',
		'disabled'	=>'true'
	);

	$cantidad_receta=array(
		'name'=>'cantidad_receta',
		'id'=>'cantidad_receta',
		'value'=>set_value('codigo',''),
		'type' =>'number',
		'placeholder'=>'',
		'class'		=> 'medichan form-control',
		'disabled'	=>'true',
		'min'		=>1		
	);

 ?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/tabs-horizontal.css');?>">
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<input type="hidden" name="idConsulta" id="idConsulta" value="<?php echo @$consulta[0]->ID_CONSULTA?>">
<input type="hidden" name="idAntecedente" id="idAntecedente" value="<?php echo @$consulta[0]->ID_ANTECEDENTE?>">
<input type="hidden" name="idPaciente" id="idPaciente" value="<?php echo @$consulta[0]->ID_PACIENTE?>">
<input type="hidden" name="idEmbarazo" id="idEmbarazo" value="<?php echo @$consulta[0]->ID_EMBARAZO?>">
<input type="hidden" name="idMenstruacion" id="idMenstruacion" value="<?php echo @$consulta[0]->ID_MENSTRUACION?>">
<input type="hidden" name="idInformacion" id="idInformacion" value="<?php echo @$consulta[0]->ID_INFORMACION_INTERES?>">
<input type="hidden" name="idCita" id="idCita" value="<?php echo @$consulta[0]->ID_CITA?>">
<input type="hidden" name="idExploracion" id="idExploracion" value="<?php echo @$consulta[0]->ID_EXPLORACION?>">
<div class="content-wrapper">
	<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>						
	</section>
	<section class="content">
		<div class="box">
	      	<div id="mensaje"></div>			
			<div class="box-header">
				<h3 class="header-title">Datos</h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" name="formulario" id="formulario" role="form">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1" data-toggle="tab" ><span class="glyphicon glyphicon-align-left"></span> Resumen</a>
							</li>
							<li >
								<a href="#tab_2" data-toggle="tab"><span class="glyphicon glyphicon glyphicon-list-alt"></span> Antecedentes</a>
							</li>					
							<li>
								<a href="#tab_3" data-toggle="tab"><span class="glyphicon glyphicon-align-right"></span> Consulta</a>
							</li>
						</ul>						
					</div>	
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="col-sm-12">
								<div class="panel panel-success">
									<div class="panel-heading">
										<h3 class="panel-title center">Historial</h3>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-12">
												<?php 
													if ($consultas) {
														foreach ($consultas as $c) {																		
												 ?>
												 <div class="col-sm-4">
												 	<div class="small-box bg-olive">
												 		<div class="inner">
												 			<span><?php echo $c->ENFERMEDAD_NOMBRE ?></span>
												 			<p><?php echo $c->FECHA_CONSULTA ?></p>
												 		</div>
												 		<div class="icon">
												 			<i class=""></i>
												 		</div>
												 		<a class="small-box-footer btn-describe" >
												 			<i class="fa fa-eye"></i>
												 		</a>
												 	</div>
												 	<div class="desc_history" style="display: none;">
												 		<ul class="products-list product-list-in-box">
												 			<li class="item">
												 				<div class="product-info">
												 					<a class="product-title">Diagn&oacute;stico</a>
												 					<span class="product-description"><?php echo $c->DIAGNOSTICO ?></span>
												 				</div>
												 			</li>
												 			<li class="item">
												 				<div class="product-info">
												 					<a class="product-title">S&iacute;ntomas</a>
												 					<span class="product-description"><?php echo $c->SINTOMA ?></span>
												 				</div>
												 			</li>
												 			<li class="item">
												 				<div class="product-info">
												 					<a class="product-title">EXAMENES</a>
												 					<span class="product-description"><?php 
												 						if ($estudioscitas) {
												 							foreach ($estudioscitas as $e) {
												 								echo "<span class='product-description'>".$e->DESCRIPCION."</span>";
												 							}
												 						}
												 					?></span>
												 				</div>
												 			</li>
												 			<li class="item">
												 				<div class="product-info">
												 					<a class="product-title">Diagnostico</a>
												 					<span class="product-description"><?php echo $c->DIAGNOSTICO ?></span>
												 				</div>
												 			</li>	
												 		</ul>
												 	</div>
												 </div>
												 <?php 
												 	}
												}
												  ?>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="tab-pane" id="tab_2">
							<div class="col-sm-12">
								<div class="panel panel-info">
									<div class="panel-heading">
										<h3 class="panel-title">Antecedentes</h3>
									</div>
									<div class="panel-body">
										<div class="nav-tabs-custom">
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab_personal" data-toggle="tab" > Personal</a>
												</li>
												<li >
													<a href="#tab_alergia" data-toggle="tab">Alergias</a>
												</li>					
												<li>
													<a href="#tab_menstruacion" data-toggle="tab">Menstruaciones</a>
												</li>
												<li>
													<a href="#tab_embarazo" data-toggle="tab">Embarazos</a>
												</li>
												<li>
													<a href="#tab_interes" data-toggle="tab">Datos de Interes</a>
												</li>
											</ul>						
										</div>	
										<div class="tab-content">
											<div class="tab-pane active" id="tab_personal">
												<div class="form-group col-sm-6">
													<label for="general" class="col-sm-12 form-label">Datos Generales:</label>
													<div class="col-sm-10">
														<?php echo form_textarea($general); ?>
													</div>
													<label for="personales_no_patologicos" class="col-sm-12 form-label">Personales no Patol&oacute;gicos:</label>
													<div class="col-sm-10">
														<?php echo form_textarea($personales_no_patologicos); ?>
													</div>
																									
												</div>
												<div class="form-group col-sm-6">
													<label for="hereditario_familiar" class="col-sm-12">Hereditarios y familiares: </label>
													<div class="col-sm-10">
														<?php echo form_textarea($familiar); ?>
													</div>													
													<label for="personales_patologicos" class="col-sm-12 form-label">Personales Patol&oacute;gicos:</label>
													<div class="col-sm-10">
														<?php echo form_textarea($personales_patologicos); ?>
													</div>
												</div>
												<div class="form-group col-sm-12">
													<label for="enfermedades" class="form-label col-sm-6 col-sm-offset-3">Enfermedades del CIE10</label>
													<div class="col-sm-6 col-sm-offset-3">
														<select class="form-control js-example-basic-single " id="listEnfermedades" style="width: 80%;">
															<option value="0">Seleccionar Enfermedad</option>
															<?php 
																	if ($enfermedades) {
																		foreach ($enfermedades as $e) {
																			echo "<option value='".$e->ID_CAT_ENFERMEDAD."'>".$e->CODIGO." ".$e->DESCRIPCION."</option>";
																		}
																	}

															 ?>
														</select>											
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4 col-sm-offset-3">
														<select name="enfermedades" id="enfermedades" class="pseudoantechan form-control" size="7" readonly="true">
															<?php 
																if ($enfermedades_pacientes) {
																	foreach ($enfermedades_pacientes as $e) {
																		echo "<option value='".$e->ID_CLI_CAT_ENFERMEDAD."'>".$e->CODIGO." ".$e->DESCRIPCION."</option>";
																	}
																}

															 ?>

														</select>														
													</div>													
												</div>
											</div>
											<div class="tab-pane" id="tab_alergia">
												<div class="form-group col-sm-12">
													<div class="form-check form-check-inline col-sm-6">
													  <input class="form-check-input" type="radio" name="exampleRadios" id="radio_alergia" value="1" checked>
													  <label class="form-check-label" for="radio_alergia">
													    Alerg&iacute;as
													  </label>  
													</div>
													<div class="form-check form-check-inline col-sm-6">
													  <input class="form-check-input" type="radio" name="exampleRadios" id="radio_alergia_sustancia" value="1" >
													  <label class="form-check-label" for="radio_alergia_sustancia">
													    Alerg&iacute;as por sustancia
													  </label>
													</div>														
												</div>
												<div class="form-group col-sm-12">
													<div class="col-sm-6">
														<select class="form-control js-example-basic-single " id="listAlergias" style="width: 80%;">
															<option value="">Seleccionar</option>
															<?php 
																if ($alergias) {
																	foreach ($alergias as $a) {
																		echo "<option value='".$a->ID_CAT_OTRA_ALERGIA."'>".$a->DESCRIPCION."</option>";
																	}
																}

															 ?>
														</select>
													</div>
													<div class="col-sm-6">
														<select class="form-control js-example-basic-single " id="listOtrasAlergias" style="width: 80%;" disabled="true">
															<option value="">Seleccionar</option>
															<?php 
																if ($sustancias) {
																	foreach ($sustancias as $s) {
																		echo "<option value='".$s->ID_CAT_ALERGIA_SUSTANCIA."'>".$s->DESCRIPCION."</option>";
																	}
																}															
															 ?>
														</select>														
													</div>
												</div>
												<div class="form-group col-sm-12">
													<div class="form-check form-check-inline col-sm-5 col-sm-offset-5">
													  <input class="form-check-input" type="radio" name="exampleRadios" id="radio_otras_alergias" value="1" >
													  <label class="form-check-label" for="radio_otras_alergias">
													    Otra Alerg&iacute;a
													  </label>
													</div>
													<div class="col-sm-6 col-sm-offset-3">
														<?php echo form_input($otra_alergia); ?>
													</div>													
												</div>
												<div class="form-group col-sm-12">
													<div class="col-sm-6 col-sm-offset-3">
														<button type="button" class="btn btn-primary" id="btnAlergia"><span class="glyphicon glyphicon-plus" ></span> Agregar</button>
													</div>
													<div class="col-sm-6 col-sm-offset-3" style="padding-top: 50px;">
														<select class="pseudoantechan form-control" multiple="" size="7" readonly="true" id="listaAlergiasPaciente">
															<?php 
																if ($alergias_paciente) {
																	foreach ($alergias_paciente as $a) {
																		echo "<option value='".$a->ID_ALERGIA_PACIENTE."'>".$a->nombre_alergia."</option>";
																	}
																}
															 ?>
														</select>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab_menstruacion">
												<div class="row">
													<div class="form-group">
														<div class="col-md-6 col-sm-12">
															<label for="primera_menstruacion" class="col-sm-8 form-label">Edad de la primera menstruaci&oacute;n</label>
															<div class="col-sm-4">
																<?php echo form_input($primera_menstruacion); ?>
															</div>
														</div>
														<div class="col-md-6 col-sm-12">
															<label class="col-sm-8 form-label" for="ivsa">Edad de inicio de vida sexual:</label>
															<div class="col-sm-4">
																<?php echo form_input($ivsa) ?>
															</div>
														</div>	
													</div>
													<div class="form-group">
														<div class="col-md-6 col-sm-12">
															<label for="menopausia" class="form-label col-sm-8">Menopausia</label>
															<div class="col-sm-4">
																<?php echo form_input($menopausia); ?>
															</div>
														</div>
														<div class="col-md-6 col-sm-12">
															<label for="caracteristicas" class="form-label col-sm-6">Caracteristicas</label>
															<div class="col-sm-6">
																<?php 	echo form_dropdown('caracteristicas',$caracteristicas,@$consulta[0]->CARACTERISTICAS,'class="menchan form-control" style="min-height:80px;" id="caracteristicas"') ?>
															</div>
														</div>
													</div>
													<div class="form-group col-md-10 col-sm-12">
														<label for="otros" class="form-label col-sm-2">Otros</label>
														<div class="col-sm-10">
															<?php echo form_textarea($otros_menstruacion) ?>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab_embarazo">
												<div class="form-group">
													<label for="total_embarazo" class="col-sm-2 form-label">Total de embarazos</label>
													<div class="col-sm-4">
														<?php echo form_input($total_embarazo); ?>
													</div>
													<label for="no_partos" class="col-sm-2 form-label">No. de partos</label>
													<div class="col-sm-4">
														<?php echo form_input($no_partos); ?>
													</div>
												</div>
												<div class="form-group">
													<label for="no_cesareas" class="col-sm-2 form-label">No. de ces&aacute;reas</label>
													<div class="col-sm-4">
														<?php echo form_input($no_cesareas); ?>
													</div>
													<label for="no_abortos" class="col-sm-2 form-label">No. de abortos</label>
													<div class="col-sm-4">
														<?php echo form_input($no_abortos); ?>
													</div>
												</div>
												<div class="form-group">
													<label for="nacidos_vivos" class="col-sm-2 form-label">Nacidos vivos</label>
													<div class="col-sm-4">
														<?php echo form_input($nacidos_vivos); ?>
													</div>
													<label for="vivos_actuales" class="col-sm-2 form-label">Vivos actuales</label>
													<div class="col-sm-4">
														<?php echo form_input($vivos_actuales); ?>
													</div>
												</div>
												<div class="form-group">
													<label for="otros_embarazo" class="col-sm-6 col-sm-offset-5 form-label">Otros:</label>
													<div class="col-sm-6 col-sm-offset-3">
														<?php echo form_textarea($otros_embarazo); ?>
													</div>
												</div>												
											</div>
											<div class="tab-pane" id="tab_interes">
												<div class="form-group col-sm-6">
													<div class="col-sm-12">
														<label class="col-sm-4 form-label" for="fecha_ultima_papanicolau">Fecha de &uacute;ltima papanicolau</label>
														<div class="col-sm-8">
															<?php echo form_input($fecha_ultima_papanicolau); ?>
														</div>
													</div>
													<div class="col-sm-12">
														<label class="col-sm-4 form-label" for="fecha_ultima_colposcopia">Fecha de &uacute;ltima colposcop&iacute;a</label>
														<div class="col-sm-8">
															<?php echo form_input($fecha_ultima_colposcopia); ?>
														</div>										
													</div>
													<div class="col-sm-12">
														<label class="col-sm-4 form-label" for="fecha_ultima_mamogorafia"> Fecha de &uacute;ltima mamograf&iacute;a</label>
														<div class="col-sm-8">
															<?php echo form_input($fecha_ultima_mamografia); ?>
														</div>
														
													</div>												
												</div>
												<div class="form-group col-sm-6">
													<div class="col-sm-12">
														<div class="col-sm-12">
														<label for="procedimientos_ginecologicos">Procedimientos ginecol&oacute;gicos</label>
															<?php echo form_textarea($procedimientos_ginecologicos); ?>
														</div>
													</div>
												</div>
												<div class="col-sm-12 form-group">
													<label for="parejas_sexuales" class="col-sm-2 form-label">No. Parejas sexuales</label>
													<div class="col-sm-4">
														<?php echo form_input($parejas_sexuales) ?>
													</div>
													<label for="flujos_vaginales" class="col-sm-2 form-label">Flujos vaginales</label>
													<div class="col-sm-4">
														<?php echo form_textarea($flujos_vaginales); ?>
													</div>
												</div>
												<div class="form-group col-sm-12">
													<label for="metodos_anticonceptivos" class="col-sm-2 form-label">M&eacute;todos anticonceptivos</label>
													<div class="col-sm-4">
														<?php echo form_textarea($metodos_anticonceptivos); ?>
													</div>
													<label for="habitos" class="col-sm-2 form-label">H&aacute;bitos</label>
													<div class="col-sm-4">
														<?php echo form_textarea($habitos); ?>
													</div>
												</div>
												<div class="form-group col-sm-12">
													<label for="cirujias_previas" class="col-sm-2 form-label">Cirujias previas</label>
													<div class="col-sm-4">
														<?php echo form_textarea($cirujias_previas); ?>
													</div>
													<label for="otros_intereses" class="col-sm-2 form-label">Otros</label>
													<div class="col-sm-4">
														<?php echo form_textarea($otros_intereses); ?>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_3">
							<div class="col-sm-12">
								<div class="panel panel-warning">
									<div class="panel-heading">
										<h3 class="panel-title">Consulta</h3>
									</div>
									 <div class="panel-body">
										<div class="nav-tabs-custom">
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab_padecimiento" data-toggle="tab" > Padecimientos</a>
												</li>
												<li >
													<a href="#tab_exploracion" data-toggle="tab">Exploraci&oacute;n</a>
												</li>					
												<li>
													<a href="#tab_diagnostico" data-toggle="tab">Diagn&oacute;stico</a>
												</li>
												<li>
													<a href="#tab_solicitud" data-toggle="tab">Solicitud de Estudios</a>
												</li>

											</ul>						
										</div>
										<div class="tab-content">
											<div class="tab-pane active" id="tab_padecimiento">
												<div class="form-group col-sm-12">
													<label for="sintoma" class="col-sm-2 form-label">Sintomas</label>
													<div class="col-sm-10">
														<?php echo form_textarea($sintoma); ?>
													</div>
												</div>
												<div class="form-group col-sm-12">
													<label for="observacion" class="col-sm-2 form-label">Observaci&oacute;n</label>
													<div class="col-sm-4">
														<?php echo form_textarea($observacion); ?>
													</div>
													<label for="tratamiento_previo" class="col-sm-2 form-label">Tratamientos previos</label>
													<div class="col-sm-4">
														<?php echo form_textarea($tratamiento_previo); ?>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab_exploracion">
												<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 bhoechie-tab-container">
													<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bhoechie-tab-menu">
														<div class="list-group">
															<a href="#" class="list-group-item text-center active">
																<img src="<?php echo base_url('img/consulta/drop3.png');?>" class="img-responsive">
															</a>
															<a href="#" class="list-group-item text-center"><img src="<?php echo base_url('img/consulta/drop6.png');?>" class="img-responsive">
															</a>
															<a href="#" class="list-group-item text-center">
																<img src="<?php echo base_url('img/consulta/drop5.png');?>" class="img-responsive">
															</a>
															<a href="#" class="list-group-item text-center">
																<img src="<?php echo base_url('img/consulta/drop3.png');?>" class="img-responsive">
															</a>
														</div>
													</div>
													<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 bhoechie-tab">
														<div class="bhoechie-tab-content active">
															<center><h2 style="margin-top: 0;color:#55518a">Signos vitales</h2></center>
															<div class="form-group">				
																<div class="col-sm-2">
																	<label for="peso" class="form-label">Peso</label>							
																	<?php echo form_input($peso); ?>
																</div>
																<div class="col-sm-2">
																	<label for="talla" class="form-label">Estatura</label>
																	<?php echo form_input($talla); ?>
																</div>								
																<div class="col-sm-2">
																	<label for="cadera" class="form-label">Cadera</label>
																	<?php echo form_input($cadera); ?>
																</div>
																<div class="col-sm-2">
																	<label for="cintura" class="form-label">Cintura</label>
																	<?php echo form_input($cintura); ?>
																</div>
																<div class="col-sm-3">
																	<label for="imc" class="form-label">IMC</label>
																	<?php echo form_input($imc); ?>
																</div>		
															</div>
															<div class="form-group">
																<div class="col-sm-3">
																	<label for="temperatura" class="form-label">Temperatura</label>
																	<?php echo form_input($temperatura) ?>
																</div>
																<div class="col-sm-3">
																	<label for="presion_arterial" class="form-label">Presi&oacute;n arterial</label>
																	<?php echo form_input($presion_arterial); ?>
																</div>
																<div class="col-sm-3">
																	<label for="frecuencia_cardiaca" class="form-label">F. card&iacute;aca</label>
																	<?php echo form_input($frecuencia_cardiaca); ?>
																</div>
																<div class="col-sm-3">
																	<label for="frecuencia_respiratoria" class="form-label">F. respiratoria</label>
																	<?php echo form_input($frecuencia_respiratoria); ?>
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-3">
																	<label for="cintura_abdominal" class="form-label">Cintura abdominal</label>
																	<?php echo form_input($cintura_abdominal); ?>
																</div>
																<div class="col-sm-3">
																	<label for="pliegue_abdominal" class="form-label">Pliegue abdominal</label>
																	<?php echo form_input($pliegue_abdominal); ?>
																</div>
																<div class="col-sm-3">
																	<label for="grasa_corporal" class="form-label">%Grasa corporal</label>
																	<?php echo form_input($grasa_corporal); ?>
																</div>
																<div class="col-sm-3">
																	<label for="fuerza_mano" class="form-label">Fuerza mano</label>
																	<?php echo form_input($fuerza_mano); ?>
																</div>
															</div>
														</div>
														<div class="bhoechie-tab-content">
															<center><h2 style="margin-top: 0;color:#55518a">Colesterol y triglic&eacute;ridos</h2></center>
															<div class="form-group">
																<div class="col-sm-3">
																	<label for="trigliceridos" class="form-label">Trigliceridos</label>
																	<?php echo form_input($trigliceridos); ?>
																</div>
																<div class="col-sm-3">
																	<label for="colesterol" class="form-label">Colesterol</label>
																	<?php echo form_input($colesterol); ?>
																</div>
																<div class="col-sm-3">
																	<label for="hdl" class="form-label">HDL</label>
																	<?php echo form_input($hdl); ?>
																</div>
																<div class="col-sm-3">
																	<label for="ldl" class="form-label">LDL</label>
																	<?php echo form_input($ldl); ?>
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-3">
																	<label for="creatinina" class="form-label">Creatinina</label>
																	<?php echo form_input($creatinina); ?>
																</div>
																<div class="col-sm-3">
																	<label for="acido_urico" class="form-label">Acido &Uacute;rico</label>
																	<?php echo form_input($acido_urico); ?>
																</div>
															</div>									
															<center><h2 style="margin-top: 0;color:#55518a">Hemoglobina</h2></center>
															<div class="form-group">
																<div class="col-sm-3">
																	<label for="hemoglobina" class="form-label">Hemoglobina</label>
																	<?php echo form_input($hemoglobina); ?>
																</div>																
															</div>
														</div>
														<div class="bhoechie-tab-content">
															<center><h2 style="margin-top: 0;color:#55518a">Exploraci&oacute;n F&iacute;sica</h2></center>
															<div class="col-sm-2">
																<img src="<?php echo base_url('img/consulta/p1.png');?>" class="img img-responsive pm1">
																<img src="<?php echo base_url('img/consulta/p2.png');?>" class="img img-responsive pm2">
																<img src="<?php echo base_url('img/consulta/p3.png');?>" class="img img-responsive pm3">
																<img src="<?php echo base_url('img/consulta/p4.png');?>" class="img img-responsive pm4">

															</div>
															<div class="form-group col-sm-5">
																<div class="col-sm-12">
																	<label for="cabeza" class="form-label">Cabeza</label>
																	<?php echo form_textarea($cabeza); ?>
																</div>
																<div class="col-sm-12">
																	<label for="torax" class="form-label">Torax</label>
																	<?php echo form_textarea($torax); ?>
																</div>							
																<div class="col-sm-12">
																	<label for="pulso" class="form-label">Pulsos</label>
																	<?php echo form_textarea($pulso); ?>
																</div>
																<div class="col-sm-12">
																	<label for="miembros_inferiores" class="form-label">Miembros inferiores</label>
																	<?php echo form_textarea($miembros_inferiores); ?>
																</div>
																<div class="col-sm-12">
																	<label for="genitales" class="form-label">Genitales</label>
																	<?php echo form_textarea($genitales); ?>
																</div>
																		
															</div>
															<div class="form-group col-sm-5">
																<div class="col-sm-12">
																	<label for="cuello" class="form-label">Cuello</label>
																	<?php echo form_textarea($cuello); ?>
																</div>
																<div class="col-sm-12">
																	<label for="abdomen" class="form-label">Abdomen</label>
																	<?php echo form_textarea($abdomen); ?>
																</div>
																<div class="col-sm-12">
																	<label for="facies" class="form-label">Facies</label>
																	<?php echo form_textarea($facies); ?>
																</div>		
																<div class="col-sm-12">
																	<label for="pie" class="form-label">Pie</label>
																	<?php echo form_textarea($pie); ?>
																</div>		
																<div class="col-sm-12">
																	<label for="otro_exploracion" class="form-label">Otros</label>
																	<?php echo form_textarea($otro_exploracion); ?>
																</div>				
															</div>
														</div>
														<div class="bhoechie-tab-content">
															<center><h2 style="margin-top: 0;color:#55518a">Glucosa</h2></center>
															<div class="form-group">
																<div class="col-sm-3">
																	<label for="glucosa_laboratorio" class="form-label">Laboratorio</label>									<?php  echo form_input($glucosa_laboratorio)?>
																</div>
																<div class="col-sm-3">
																	<label for="glucosa_capilar" class="form-label">Capilada</label>
																	<?php echo form_input($glucosa_capilar); ?>
																</div>
																<div class="col-sm-3">
																	<label for="glicosalida" class="form-label">Glicolisada</label>
																	<?php echo form_input($glicosalida); ?>
																</div>
																<div class="col-sm-3">
																	<label for="transaminasa" class="form-label">Transaminasa</label>
																	<?php echo form_input($transaminasa); ?>
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-6">
																	<label for="observaciones_glucosa" class="form-label">Observaciones</label>
																	<?php echo form_textarea($observaciones_gluscosa); ?>
																</div>
															</div>
														</div>
													</div>
														
												</div>
											</div>
											<div class="tab-pane" id="tab_diagnostico">
												<div class="form-group col-sm-6">
													<div class="col-sm-12">
														 <label for="diagnostico" class="form-label">Diagn&oacute;stico Escrito:</label>
														 <?php echo form_textarea($diagnostico); ?>
													</div>
													<div class="col-sm-12">
														<label for="indicaciones_consulta" class="form-label">Indicaciones M&eacute;dicas:</label>
														<?php echo form_textarea($indicaciones_consulta); ?>
													</div>
													<div class="col-sm-12">
														<label for="enfermedades" class="form-label col-sm-12 c">Diagn&oacute;nostico del CIE10</label>
														<div class="col-sm-12" id="select_enfermedad_diagnostico" >
															<select class="form-control js-example-basic-single " id="diagnosticolistEnf" style="width: 80%;">
																<option value="">Seleccionar Enfermedad</option>
																<?php 
																		if ($enfermedades) {
																			foreach ($enfermedades as $e) {
																				if ($e->ID_CAT_ENFERMEDAD==@$consulta[0]->ID_CAT_ENFERMEDAD) {
																					echo "<option value='".$e->ID_CAT_ENFERMEDAD."' selected>".$e->CODIGO." ".$e->DESCRIPCION."</option>";
																				} else {
																					echo "<option value='".$e->ID_CAT_ENFERMEDAD."'>".$e->CODIGO." ".$e->DESCRIPCION."</option>";
																				}
																				
																			}
																		}

																 ?>
															</select>										
														</div>												
													</div>

												</div>
												<div class="form-group col-sm-6">
													<div class="col-sm-12">
														<label for="enfermedad_diagnostico" class="form-label">Medicamento</label>
														<select class="form-control js-example-basic-single " id="listMedicamento" style="width: 100%;">
															<option value="">Seleccionar Medicamento</option>
															<?php 
																	if ($medicamentos) {
																		foreach ($medicamentos as $m) {
								                                                echo "<option value='".$m->ID_MEDICAMENTO."'>".$m->COD_PRODUCTO." ".$m->NOMBRE_PRODUCTO."-".$m->FORMA_FARMACEUTICA."-".$m->PRESENTACION."-".$m->CONCENTRACION."</option>";
																		}
																	}

															 ?>
														</select>
													</div>
													<div class="col-sm-12">
														<label for="presentacion" class="form-label">Presentaci&oacute;n</label>
														<?php echo form_input($presentacion); ?>
													</div>
													<div class="col-sm-12">
														<label for="dosis" class="form-label">Concentraci&oacute;n</label>
														<?php echo form_input($dosis); ?>
													</div>
													<div class="col-sm-12">
														<label for="indicaciones" class="form-label">Indicaciones</label>
														<?php echo form_input($indicaciones); ?>
													</div>
													<div class="col-sm-12">
														<label for="cantidad_receta" class="form-label"> Cantidad</label>
														<?php echo form_input($cantidad_receta) ?>
													</div>

												</div>
												<div class="form-group col-sm-12">
													<div class="col-sm-12">							
														<button type="button" class="btn btn-primary pull-right" id="btnReceta"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	
														
													</div>
													<div class="col-sm-6 col-sm-offset-6 table-responsive">
														<table class="table table-hover" >
															<thead>
																<th>Medicamento</th>
																<th>Presentaci&oacute;n</th>
																<th>Dosis</th>
																<th>Indicaciones</th>
																<th>Cantidad</th>
																<th style="display: none;">ID_RECETA</th>
																<th></th>
															</thead>
															<tbody id="recetaTablabody">
																<?php 
																	if ($medicamentosconsulta) {
																		foreach ($medicamentosconsulta as $m) {
																			echo "<tr>";
																			echo "<td>".$m->NOMBRE_PRODUCTO."</td>";
																			echo "<td>".$m->PRESENTACION."</td>";
																			echo "<td>".$m->DOSIS."</td>";
																			echo "<td>".$m->INDICACIONES."</td>";
																			echo "<td>".$m->CANTIDAD."</td>";
																			echo "<td style='display:none;' class='Idreceta'>".$m->ID_RECETA."</td>";
																			echo "<td><button type='button'  title='Eliminar Medicamento' class='btn btn-danger btn-xs EliminarMedicamento'><span class='glyphicon glyphicon-remove'></span></button></td>";
																			echo "</tr>";
																		}
																	}
																 ?>
															</tbody>
														</table>
													</div>	
												</div>
											</div>
											<div class="tab-pane" id="tab_solicitud">
												<div class="form-group">

													<div class="col-sm-6 col-sm-offset-3">
														<label for="estudios" class="col-sm-6 control-label">Examenes Clinicos:</label>
														<select id="listEstudios" class="form-control js-example-basic-single " style="width: 80%;">
															<option value="0">Seleccione un Estudio</option>
															<?php 
																if ($estudios) {
																	foreach ($estudios as $e) {
																		echo "<option value='".$e->ID_CAT_ESTUDIO_CLINICO."'>".$e->DESCRIPCION."</option>";
																	}
																}
															 ?>
														</select>					
														
													</div>												
												</div>
												<div class="form-group">	
													<table id="estudios" class="table table-bordered table-striped">
															<thead>
																<th>Examen</th>
																<th>Estado</th>
																<th>ID</th>
																<th>Respuesta</th>
																<th style="display: none;">ID_detalle</th>
																<th></th>
															</thead>
															<tbody id="tablebodyEstudios">
																<?php 
																	if ($estudioscitas) {
																		foreach ($estudioscitas as $ec) {
																			echo "<tr>";
																			echo "<td>".$ec->DESCRIPCION."</td>";
																			echo "<td>".$ec->ESTADO."</td>";
																			echo "<td class='Idestudio'>".$ec->ID_CAT_ESTUDIO_CLINICO."</td>";
																			echo "<td style='display:none;' class='Iddetalle'>".$ec->ID_DETALLE_ESTUDIOS."</td>";
																			echo "<td> <textarea type='text' value='".$ec->LECTURA."' name='estudio' rows='3' class='form-control estchan' id='estudio_".$ec->ID_CAT_ESTUDIO_CLINICO."' >".$ec->LECTURA."</textarea></td>";
																			echo "<td><button type='button'  title='Eliminar Estudio' class='btn btn-danger btn-xs btnEliminar'><span class='glyphicon glyphicon-remove'></span></button> <buton type='button' title='Guardar Lectura' class='btn btn-primary btn-xs btnModificar'><span class='glyphicon glyphicon-saved'></span></button></td>";
																			echo "</tr>";
																		}
																	}
																	 ?>			
																</tbody>
														</table>
														<button type="button" class="btn btn-primary" id="btnGuardarExamen" ><span class="glyphicon glyphicon-plus" ></span> Agregar Examen</button>													
												</div>												
											</div>
										</div>	
																			 	
									 </div>
								</div>
							</div>
						</div>
					</div>				
				</form>
			</div>

		</div>
			<div class="box-footer">

					<button type="submit" class="btn btn-primary pull-right" id="btnGuardar" onclick="salir()"><span class="glyphicon glyphicon-saved" ></span> Finalizar</button>						
			</div>		
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonConsulta.js');?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"clinica/consulta"

        } );
    }
    function salir()
    {
    	alertify.confirm("¿Desea Regresar?","Asegurese de que los datos de la consulta esten debidamente completados",function(){
    		  window.location.href = baseurl+"clinica/consulta"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }    
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"clinica/cita"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
    $(document).ready(function(){
    	$("#listEnfermedades").select2();
    	$("#listAlergias").select2();
    	$("#listOtrasAlergias").select2();
    	$("#listMedicamento").select2(); 
    	$("#diagnosticolistEnf").select2();
    	$("#listEstudios").select2();
    });
    function eliminarfila()
    {
        document.getElementById("tablebodyEstudios").deleteRow(0);
    }    	    
</script>