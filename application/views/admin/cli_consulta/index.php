<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  $fecha_hora=array(
    'name'    => 'fecha_hora',
    'id'    => 'fecha_hora',
    'value'   => set_value('fecha_hora',date('d/m/Y H:i')),
    'type'    => 'text',
    'class'   => 'form-control',
    'placeholder' => ''
  );  
?>


<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>

<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"clinica/consulta"
        } );
    }

</script>
<div class="content-wrapper">
	<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>						
	</section>
	<section class="content">
		<div class="box">
	      	<div id="mensaje"></div>			
			<div class="box-header">
				<h3 class="header-title">Lista de Consultas</h3>
			</div>
			<div class="box-body">
				<div class="col-sm-9">
					
					<div id="calendar">
						
					</div>									
					
				</div>
        <div class="row">
            <div class="col-sm-8">
            <label for="fecha_hora" class="col-sm-2 control-label">Fecha:</label>
                    <div class="col-sm-4">
                        <div class='input-group date' id='datetimepicker1'>
                        <?php
                          echo form_input($fecha_hora);
                        ?>                  
                            <span class="input-group-addon">
                                <span class="fa fa-clock-o"></span>
                            </span>
                        </div>
                    </div>            
            </div>          
        </div>
        <div class="row">
          <div class="col-sm-9">

            <table id="consultas" class="table table-bordered table-striped">
              <thead>
                  <th>Fecha consulta</th>
                  <th>Paciente</th>
                  <th>Expediente</th>
                  <th>Motivo</th>
                  <th></th>              
              </thead>
              <tbody id="citasTableBody">
                <?php
                    if ($citas) {
                       foreach ($citas as $c) {
                        $citaId=base64_encode($c->ID_CITA);
                          echo "<tr>";
                          echo "<td>".date('d-m-Y',strtotime($c->FECHA_CONSULTA))."</td>";
                          echo "<td>".$c->nombre_paciente."</td>";
                          echo "<td>".$c->CODIGO_EXPEDIENTE."</td>";
                          echo "<td>".$c->ASUNTO."</td>";
                          echo "<td><a href='consulta/editar/".$citaId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
                          echo "</tr>";
                       }
                     }  
                 ?>
              </tbody>
            </table>
          </div>          
        </div>

			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
  $(document).ready(function(){
        $.datetimepicker.setLocale('es');

jQuery('#fecha_hora').datetimepicker({
   format: 'd/m/Y H:i',
    value: new Date(),
    lang:'es'
});

    $("#fecha_hora").blur(function(){
      var fecha=new Object();
      var html='';
      fecha.fecha=$(this).val();
      console.log(fecha);
      var DatosJson=JSON.stringify(fecha);
      $.post(baseurl+'clinica/consulta/actualizarTabla',{
        FechaPost:DatosJson
      },function(data,textStatus){
          data.forEach(function(element){
            //console.log(element.ID_CITA);
              html+='<tr>';
              html+='<td>'+element.FECHA_CONSULTA+'</td>';
              html+='<td>'+element.nombre_paciente+'</td>';
              html+='<td>'+element.CODIGO_EXPEDIENTE+'</td>';
              html+='<td>'+element.ASUNTO+'</td>';
              html+='<td><a href="consulta/editar/'+btoa(element.ID_CITA)+'" ><button type="button" title="Editar Perfil" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span> </button> </a>';
              html+='</tr>';
          });
          console.log(html);
          $("#citasTableBody").html(html);          
        //console.log(data);
      },"json").fail(function(response){
        $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
        console.log('Error: ' + response.responseText);
      });
    }) 

$("#consultas").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
    });

  });
document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: [ 'interaction','dayGrid', 'timeGrid', 'list','timeGridPlugin' ],
          header: {
               left: 'prev,next today',
               center: 'title',
               right: 'dayGridMonth,listMonth'
          },           
          locale: 'es',   
          defaultView: 'listWeek',

          navLinks: true, // can click day/week names to navigate views
          editable: true,
          eventLimit: true, // allow "more" link when too many events
          events: baseurl+'clinica/cita/obtenerCitas',
		 eventClick: function(info) {
              var id=btoa(info.event.id);
              console.log(info.event);
              var cita=new Object();
              cita.id=info.event.id;
              var DatosJson=JSON.stringify(cita);
              console.log(cita);
              $.post(baseurl+'clinica/consulta/consultaCrear',{
                CitaPost: DatosJson
              },function(data,textStatus){
                console.log(data);
              window.location.href=baseurl+'clinica/consulta/nuevo/'+id;

              },"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
              });;
              
             info.el.style.borderColor = 'red';
		        // change the border color just for fun
		         info.el.style.borderColor = 'red';
		  },      
          contentHeight: 300,
          height: 600,
          allDayText:"",
          eventOrder: "id",
         /*eventRender: function(info) {
            var tooltip = new Tooltip(info.el, {
              title: info.event.extendedProps.description,
              placement: 'top',
              trigger: 'hover',
              container: 'body'
            });
          }*/                     
                  
        });

        calendar.setOption('locale', 'es');

        calendar.render();
      });

</script>
