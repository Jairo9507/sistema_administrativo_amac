<?php
defined('BASEPATH') OR exit('No direct script access allowed');


?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h4 class="content-title">Reportes personalizados de Ingenier&iacute;a y Arquitectura</h4>	
			</div>
			<div class="box-body">
        <iframe src="<?php echo base_url('reportes/reportes_diarios_proyectos/reporteDiarioPDF/').implode('/', $datos);?>" style='width: 100%; height: 30em;' id="orden"></iframe>				
			</div>
		</div>
	</section>
</div>
