<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h4 class="content-title">Reportes Personalizados</h4>
			</div>

			<div class="box-body">
				<div id="mensaje"></div>
				<div class="pad margin no-print" style="width: 75%;position: relative;bottom: 40px;left: 11%;">
					<div class="callout callout-info" style="margin-bottom: 0!important;">
						<h4><i class="fa fa-info"></i> Nota:</h4>
						Complete los campos adecuadamente
					</div>
				</div>
				<form class="form-inline" method="POST" action="<?php echo base_url();?>reportes/reportes_requisicion/vistaPDF">					
					<div class="row">
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha inicio: </label>
								<input type="date" name="fechaInicio" class="form-control" style="width: 75%;" id="fechaInicio">
							</div>
						</div>
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha Final: </label>
								<input type="date" name="fechaFinal" class="form-control" style="width: 75%;" id="fechaFinal">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4" style="left: 3%;">
							 <div class="form-group">
							 	<label class="form-label col-sm-12">Correlativo:</label>
							 	<div class="col-sm-12">
							 		<input type="text" name="correlativo" id="correlativo" class="form-control">
							 	</div>
							 </div>
						</div>	

					</div>

					<div class="row no-print">
						<div class="col-xs-12">
							<button type="button" class="btn btn-success pull-right" onclick="limpiar();"><i class="glyphicon glyphicon-refresh"></i> Limpiar
							</button>
						<a href="<?php echo base_url();?>reportes/reportes_requisicion/vistaPDF"><button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;" id="datos">
								<i class="fa fa-download"></i> Generar PDF
							</button> </a>
						</div>
					</div>
				</form>

			</div>
		</div>
	</section>
</div>
<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	function limpiar() {

		$("input#fechaInicio").val("");
		$("input#fechaFin").val("");
		$("#correlativo").val("");
		
	}	
	$(document).ready(function(){
		$("#datos").click(function(e){
			 if($("input#fechaFinal").val()==='' && $("input#correlativo").val()===''){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el principio de el rango de fecha a filtrar</div>");
			} else if($("input#fechaInicio").val()==='' && $("input#correlativo").val()===''){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el final de el rango de fecha a filtrar</div>");
			} else if($("input#fechaFin").val()==='' && $("input#fechaInicio").val()==='' && $("input#correlativo").val()===''){
			 		e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El documento no se puede generar sin datos</div>");
			} else if($("input#fechaFin").val()!=='' && $("input#fechaInicio").val()!=='' && $("input#correlativo").val()!==''){

			 		e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El documento no se puede generar con los 3 datos, ingrese un intervalo de fecha o un c&oacute;digo de requisici&oacute;n</div>");		
			}
		});
	});
</script>