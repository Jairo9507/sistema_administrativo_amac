<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h4 class="content-title">Reportes Personalizados</h4>				
			</div>
			<div class="box-body">
				<div id="mensaje"></div>
				<div class="pad margin no-print" style="width: 75%;position: relative;bottom: 40px;left: 11%;">
					<div class="callout callout-info" style="margin-bottom: 0!important;">
						<h4><i class="fa fa-info"></i> Nota:</h4>
						Complete los campos adecuadamente
					</div>
				</div>
				<form class="form-inline" method="POST" action="" id="form">					

					<div class="row">
						<div class="col-md-6" style="left: 4%;">
							<div class="form-group">
								<label class="form-label col-sm-12">Tipo de reporte: <span style="color: #F20A06; font-size: 15px;">*</span> </label>
								<select class="form-control" name="tipoReporte" id="tipoReporte" >
									<option value="">Seleccione</option>
									<option value="1">Bitacora diaria/fotográfica</option>
									<option value="2">Ingreso de material</option>
									<option value="3">Consumo diario</option>
								</select>
							</div>
						</div>
						<div class="col-md-6" style="left: 4%;">
							<div class="form-group">
								<label for="idProyecto" class="form-label col-sm-12">Proyecto: <span style="color: #F20A06; font-size: 15px;">*</span> </label>
								<select class="form-control" id="idProyecto" name="idProyecto" style="width: 75%;" >
									<option value="">Seleccione</option>
									<?php
									if ($proyectos) {
										foreach ($proyectos as $p) {
											echo "<option value='".$p->ID_PROYECTO."'>".$p->CODIGO_PROYECTO." ".$p->NOMBRE_PROYECTO."</option>";
										}
									} else {
										echo "<option>No hay proyectos disponibles</option>";
									}

									?>
								</select>	

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6" style="left: 4%;">
							<div class="form-group">
								<label>Fecha inicio: <span style="color: #F20A06; font-size: 15px;">*</span></label>
								<input type="date" name="fechaInicio"  class="form-control" style="width: 75%;" id="fechaInicio">
							</div>
						</div>
						<div class="col-md-6" style="left: 4%;" id="fechaFindiv">
							<div class="form-group">
								<label>Fecha Final : <span style="color: #F20A06; font-size: 15px;">*</span></label>
								<input type="date" name="fechaFinal"  class="form-control" style="width: 75%;" id="fechaFin">
							</div>
						</div>
					</div>
					<br/>
					<div class="row no-print">
						<div class="col-xs-12">
							<button type="button" class="btn btn-success pull-right" onclick="limpiar();"><i class="glyphicon glyphicon-refresh"></i> Limpiar
							</button>
							<a href="<?php echo base_url();?>reportes/reporte_consumo_quincenal/reporte_detallado"><button type="submit" class="btn btn-info pull-right" style="margin-left:5px; margin-right: 5px; display: none;" id="excel">
								<i class="fa fa-download"></i> Generar Detallado
							</button></a>
						<a href="<?php echo base_url();?>reportes/reporte_consumo_quincenal/vistaPDF"><button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;" id="datos">
								<i class="fa fa-download"></i> Generar PDF
							</button> </a>
						</div>
					</div>
				</form>				
			</div>
		</div>
	</section>

</div>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl="<?php echo base_url();?>";
	function limpiar() {
		$("select#tipoReporte").val('');
		$("select#idProyecto").val('');
		$("input#fechaInicio").val('');
		$("input#fechaFin").val('');
	}	
	$(document).ready(function(){
		limpiar();
		$("#form").attr('action',baseurl+'reportes/reportes_diarios_proyectos/vistaPDF');
		$("#tipoReporte").on('change',function(){
			var val=$(this).val();
			if (val==1) {
				$("#fechaFindiv").css('display','none');
			} else {
				$("#fechaFindiv").css('display','');
			}
		})
		$("#datos").click(function(e){
			var tipoReporte=$("select#tipoReporte").val(),
			idProyecto=$("select#idProyecto").val(),
			fechaInicio=$("input#fechaInicio").val(),
			fechaFin=$("input#fechaFin").val();

			if (tipoReporte == '') {
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el tipo de reporte que desea generar</div>");								
			}else if(idProyecto==''){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el proyecto del cual quiere generar el reporte</div>");				
			}else if ((fechaInicio == '' || fechaFin=='') && tipoReporte != 1) {
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese ambas fechas de filtrado de registros</div>");								
			}

		});
	});
</script>

