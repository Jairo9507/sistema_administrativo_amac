<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h4 class="content-title">Reportes Personalizados</h4>
			</div>

			<div class="box-body">
				<div id="mensaje"></div>
				<div class="pad margin no-print" style="width: 75%;position: relative;bottom: 40px;left: 11%;">
					<div class="callout callout-info" style="margin-bottom: 0!important;">
						<h4><i class="fa fa-info"></i> Nota:</h4>
						Complete los campos adecuadamente
					</div>
				</div>
				<form class="form-inline" method="POST" action="<?php echo base_url();?>reportes/reportes_inventario_documental/vistaPDF">					
					<div class="row">
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha inicio:</label>
								<input type="date" name="fechaInicio"  class="form-control" style="width: 75%;" id="fechaInicio">
							</div>
						</div>
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha Final :</label>
								<input type="date" name="fechaFinal"  class="form-control" style="width: 75%;" id="fechaFin">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4" style="left: 3%;">
							 <div class="form-group">
							 	<label class="form-label col-sm-12">Tipo de reporte <span style="color: #F20A06; font-size: 15px;">*</span> :</label>
							 	<div class="col-sm-12">
							 		<select class="form-control js-example-basic-single" id="estado" name="estado" style="width: 100%;">
							 			<option value="">Seleccionar tipo</option>
							 			<option value="1">Tabla De retenci&oacute;n Documental</option>
							 			<option value="2">Tabla De valoraci&oacute;n Documental</option>
							 			<option value="3">Documentos Almacenados</option>
							 			<option value="4">Documentos prestados</option>
							 			<option value="5">Documentos Proximos a Vencer</option>
							 			<option value="6">Documentos Eliminados</option>
							 		</select>
							 	</div>
							 </div>
						</div>	
						<div class="col-md-4" style="left: 2%;">
							<div class="form-group">
								<label class="form-label col-sm-12">Dependencia :</label>
								<div class="col-sm-12">
									<select class="form-control js-example-basic-single" id="unidad_id" style="width: 100%;" name="unidad_id" >
										<option value="">Seleccione</option>
										<?php 

												if ($unidades) {
													foreach ($unidades as $u) {
														echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
													}
												}
										 ?>
									</select>
								</div>
							</div>
						</div>
					</div>

					<div class="row no-print">
						<div class="col-xs-12">
							<button type="button" class="btn btn-success pull-right" onclick="limpiar();"><i class="glyphicon glyphicon-refresh"></i> Limpiar
							</button>
						<a href="<?php echo base_url();?>reportes/reportes_inventario_documental/vistaPDF"><button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;" id="datos">
								<i class="fa fa-download"></i> Generar 
							</button> </a>
						</div>
					</div>
				</form>

			</div>
		</div>
	</section>
</div>

<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	function limpiar() {

		$("input#fechaInicio").val("");
		$("input#fechaFin").val("");
		$("#estado").val("");
		$("#unidad_id").val("");
		
	}	
	$(document).ready(function(){
		$("#unidad_id").select2();
		$("#estado").change(function(){
			if ($(this).val()=="1") {
				$("input#fechaInicio").prop("disabled",false);
				$("input#fechaFin").prop("disabled",false);
				$("#unidad_id").prop("disabled",false);				
				alertify.alert("Informacion","Complete las fechas y Seleccione una dependencia");
			} else if($(this).val()=="2"){
				$("input#fechaInicio").prop("disabled",false);
				$("input#fechaFin").prop("disabled",false);
				$("#unidad_id").prop("disabled",false);				
				alertify.alert("Informacion","Complete las fechas y Seleccione una dependencia");
			} else if($(this).val()=="3"){
				alertify.alert("Informacion","Este opcion no requiere mas datos");
				$("input#fechaInicio").prop("disabled",true);
				$("input#fechaFin").prop("disabled",true);
				$("#unidad_id").prop("disabled",true);
			} else if($(this).val()=="4"){
				$("input#fechaInicio").prop("disabled",false);
				$("input#fechaFin").prop("disabled",false);
				$("#unidad_id").prop("disabled",false);				
				alertify.alert("Informacion","Complete el intervalo de fechas para conocer los prestamos realizados, solo si es necesario escoja una dependencia");
			} else if($(this).val()=="5"){
				$("input#fechaInicio").prop("disabled",false);
				$("input#fechaFin").prop("disabled",false);
				$("#unidad_id").prop("disabled",false);				
				alertify.alert("Informacion","Complete el intervalo de fechas para conocer que documentos se vencen en el mismo y la unidad si desea conocer los de una dependencia especifica");
			} else if($(this).val()=="6"){
				$("input#fechaInicio").prop("disabled",false);
				$("input#fechaFin").prop("disabled",false);
				$("#unidad_id").prop("disabled",false);				
				alertify.alert("Informacion","Complete el intervalo de fechas para conocer que documentos se vencieron en el mismo y la unidad especifica de los documentos ");
			} else {
				$("input#fechaInicio").prop("disabled",false);
				$("input#fechaFin").prop("disabled",false);
				$("#unidad_id").prop("disabled",false);						
			}
		});
		$("#datos").click(function(e){

			if($("#estado").val()==='' ){
			 	e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione El tipo de reporte a generar</div>");
			} else if( $("input#fechaInicio").val()===''  && ($("#estado").val()!=='3')){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el principio de el rango de fecha a filtrar</div>");				
			} else if($("input#fechaFin").val()==='' && ($("#estado").val()!=='3' )){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el final de el rango de fecha a filtrar</div>");						
			} else if(($("#estado").val()==='1' || $("#estado").val()==='2') && $("#unidad_id").val()===''){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese la dependencia de la cual se generara la tabla de retenci&oacute;n y/o valoraci&oacute;n</div>");					
			}
		});
	});
</script>