<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h4 class="content-title">Reportes Personalizados</h4>
			</div>

			<div class="box-body">
				<div id="mensaje"></div>
				<div class="pad margin no-print" style="width: 75%;position: relative;bottom: 40px;left: 11%;">
					<div class="callout callout-info" style="margin-bottom: 0!important;">
						<h4><i class="fa fa-info"></i> Nota:</h4>
						Complete los campos adecuadamente
					</div>
				</div>
				<form class="form-inline" method="POST" action="<?php echo base_url();?>reportes/kardex/vistaPDF">					
					<div class="row">
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha inicio<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<input type="date" name="fechaInicio" class="form-control" style="width: 75%;" id="fechaInicio">
							</div>
						</div>
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha Final<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<input type="date" name="fechaFinal" class="form-control" style="width: 75%;" id="fechaFin">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4" style="left: 3%;">
							 <div class="form-group">
							 	<label class="form-label col-sm-12">Tipo Reporte<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
							 	<div class="col-sm-12">
							 		<select class="form-control js-example-basic-single" id="tipo" name="tipo" style="width: 90%;">
							 			<option value="">Seleccionar tipo</option>
							 			<option value ="1">Completo</option>
							 			<option value="2">Por Producto</option>
							 		</select>
							 	</div>
							 </div>
						</div>	
						<div class="col-md-6" style="left: 2%;display: none;" id="prueba">
							 <div class="form-group">
								<label>Producto</label>
								<div>
									<select class="form-control js-example-basic-single" id="producto" name="producto"  style="width: 100%;">
										<option value="0">Seleccionar Producto</option>
										<?php if ($productos) {
											foreach ($productos as $producto) {
												echo "<option value='".$producto->ID_PRODUCTO."'>".$producto->NOMBRE_PRODUCTO." ".$producto->MEDICAMENTO."</option>";
											}
										} 
										?>
									</select>
								</div>
							</div>
						</div>	

					</div>

					<div class="row no-print">
						<div class="col-xs-12">
							<button type="button" class="btn btn-success pull-right" onclick="limpiar();"><i class="glyphicon glyphicon-refresh"></i> Limpiar
							</button>
						<a href="<?php echo base_url();?>reportes/kardex/vistaPDF"><button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;" id="datos">
								<i class="fa fa-download"></i> Generar PDF
							</button> </a>
						</div>
					</div>
				</form>

			</div>
		</div>
	</section>
</div>
<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	baseurl="<?php echo base_url();?>";
	// function limpiar() {

	// 	$("#producto").select2();
	// 	$("input#fechaFin").val("");
	// 	$("#unidad_id").val("");
	// 	$("#vehiculo_id").val("");
	// }	
	$(document).ready(function(){
		$("#producto").select2();
		$("#tipo").select2();
		if ($("#tipo").val()==="2") {
			var select = document.getElementById('prueba');
			select.style.display="block";
		}
		$("#tipo").change(function() {
			var select = document.getElementById('prueba');
			if ($("select#tipo").val()==2) {
			select.style.display   = 'block';
			}else{
				select.style.display   = 'none';
			}
			
		});
		
		$("#datos").click(function(e){
			 if($("input#fechaInicio").val()===''){
			 	e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la fecha de inicio de filtrado del reporte</div>");
			} else if($("input#fechaFin").val()===''){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la fecha de finalizaci&oacute;n de filtrado del reporte </div>");				
			} else if($("#unidad_id").val()===''){
					e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la unidad de los vehiculos a filtrar</div>");				
			}
		});
	});
</script>