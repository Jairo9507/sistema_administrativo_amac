<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h4 class="content-title">Reportes Personalizados</h4>
			</div>

			<div class="box-body">
				<div id="mensaje"></div>
				<div class="pad margin no-print" style="width: 75%;position: relative;bottom: 40px;left: 11%;">
					<div class="callout callout-info" style="margin-bottom: 0!important;">
						<h4><i class="fa fa-info"></i> Nota:</h4>
						Complete los campos adecuadamente
					</div>
				</div>
				<form class="form-inline" method="POST" action="<?php echo base_url();?>reportes/reportes_consumo_vales/vistaPDF">					
					<div class="row">
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha inicio<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<input type="date" name="fechaInicio" class="form-control" style="width: 75%;" id="fechaInicio">
							</div>
						</div>
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha Final<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<input type="date" name="fechaFinal" class="form-control" style="width: 75%;" id="fechaFin">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4" style="left: 3%;">
							 <div class="form-group">
							 	<label class="form-label col-sm-12">Unidades<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
							 	<div class="col-sm-12">
							 		<select class="form-control js-example-basic-single" id="unidad_id" name="unidad_id" style="width: 90%;">
							 			<option value="">Seleccionar Unidad</option>
							 			<?php 
							 				if ($unidades) {
							 					foreach ($unidades as $u) {
							 						echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
							 					}
							 				}
							 			 ?>
							 		</select>
							 	</div>
							 </div>
						</div>	
						<div class="col-md-4" style="left: 2%;">
							 <div class="form-group">
							 	<label class="form-label col-sm-12">Vehiculos:</label>
							 	<div class="col-sm-12">
							 		<select class="form-control js-example-basic-single" id="vehiculo_id" name="vehiculo_id" style="width: 100%;" disabled="true">
							 			<option value="0">Seleccionar Equipo</option>
							 			
							 		</select>
							 	</div>
							 </div>
						</div>							

					</div>
					<div class="row">
						<div class="col-md-4" style="left:3%;">
							<div class="form-group" >
								<label class="col-sm-12 form-label">Revisado por <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<div class="col-md-12">
									<input type="text" name="autoriza" id="autoriza" class="form-control">
								</div>
							</div>
						</div>
					</div>

					<div class="row no-print">
						<div class="col-xs-12">
							<button type="button" class="btn btn-success pull-right" onclick="limpiar();"><i class="glyphicon glyphicon-refresh"></i> Limpiar
							</button>
						<a href="<?php echo base_url();?>reportes/reportes_consumo_vales/vistaPDF"><button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;" id="datos">
								<i class="fa fa-download"></i> Generar PDF
							</button> </a>
						</div>
					</div>
				</form>

			</div>
		</div>
	</section>
</div>
<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	baseurl="<?php echo base_url();?>";
	function limpiar() {

		$("input#fechaInicio").val("");
		$("input#fechaFin").val("");
		$("#unidad_id").val("");
		$("#vehiculo_id").val("");
	}	
	$(document).ready(function(){
		$("#vehiculo_id").select2();
		$("#unidad_id").select2();
		$("#unidad_id").change(function(){
			$("#vehiculo_id").attr("disabled",true);
			var html='';
			var unidad=new Object();
			unidad.id=$(this).val();
			console.log(unidad);
			var DatosJson=JSON.stringify(unidad);
			$.post(baseurl+'reportes/reportes_consumo_vales/obtenerVehiculosUnidad',{
				UnidadPost:DatosJson
			},function(data,textStatus){
				 html+='<option value="">Seleccione</option>';
				 console.log(data.valor);
				data.valor.forEach(function(element){
					//console.log(element);
					html+="<option value='"+element.ID_VEHICULO+"'>"+element.CODIGO_VEHICULO+"</option>";
				});
				$("#vehiculo_id").attr("disabled",false);
				$("#vehiculo_id").html(html);
			},"json").fail(function(response){
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
	    					console.log('Error: ' + response.responseText);
			});
		})
		$("#datos").click(function(e){
			 if($("input#fechaInicio").val()===''){
			 	e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la fecha de inicio de filtrado del reporte</div>");
			} else if($("input#fechaFin").val()===''){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la fecha de finalizaci&oacute;n de filtrado del reporte </div>");				
			} else if($("#unidad_id").val()===''){
					e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la unidad de los vehiculos a filtrar</div>");				
			} else if($("#autoriza").val()===''){
					e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la unidad de los vehiculos a filtrar</div>");
			}
		});
	});
</script>