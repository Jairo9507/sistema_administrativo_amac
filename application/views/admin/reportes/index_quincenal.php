<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h4 class="content-title">Reportes Personalizados</h4>				
			</div>
			<div class="box-body">
				<div id="mensaje"></div>
				<div class="pad margin no-print" style="width: 75%;position: relative;bottom: 40px;left: 11%;">
					<div class="callout callout-info" style="margin-bottom: 0!important;">
						<h4><i class="fa fa-info"></i> Nota:</h4>
						Complete los campos adecuadamente
					</div>
				</div>
				<form class="form-inline" method="POST" action="" id="form">					
					<div class="row">
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha inicio<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<input type="date" name="fechaInicio" class="form-control" style="width: 75%;" id="fechaInicio">
							</div>
						</div>
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha Final<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<input type="date" name="fechaFinal" class="form-control" style="width: 75%;" id="fechaFin">
							</div>
						</div>
						<div class="col-md-4" style="right:  2%;">
							<div class="form-group" >
								<label class="form-label col-md-12">Tipo de Reporte <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<div class="col-md-12" >
									<select class="form-control" id="tipo" name="tipo">
										<option value="0">Seleccione</option>
										<option value="1">Consolidado</option>
										<option value="2">Detallado</option>
									</select>
								</div>								
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-4" style="left:3%;">
							<div class="form-group" >
								<label class="col-sm-12 form-label">Autorizaci&oacute;n 1 :</label>
								<div class="col-md-12">
									<input type="text" name="autoriza1" id="autoriza1" class="form-control">
								</div>
							</div>
						</div>

						<div class="col-md-4" style="left:2%;">
							<div class="form-group" >
								<label class="col-sm-12 form-label">Autorizaci&oacute;n 2:</label>
								<div class="col-md-12">
									<input type="text" name="autoriza2" id="autoriza2" class="form-control">
								</div>
							</div>
						</div>						
					</div>



					<div class="row no-print">
						<div class="col-xs-12">
							<button type="button" class="btn btn-success pull-right" onclick="limpiar();"><i class="glyphicon glyphicon-refresh"></i> Limpiar
							</button>
							<a href="<?php echo base_url();?>reportes/reporte_consumo_quincenal/reporte_detallado"><button type="submit" class="btn btn-info pull-right" style="margin-left:5px; margin-right: 5px; display: none;" id="excel">
								<i class="fa fa-download"></i> Generar Detallado
							</button></a>
						<a href="<?php echo base_url();?>reportes/reporte_consumo_quincenal/vistaPDF"><button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;" id="datos">
								<i class="fa fa-download"></i> Generar PDF
							</button> </a>
						</div>
					</div>
				</form>				
			</div>
		</div>
	</section>

</div>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl="<?php echo base_url();?>";
	function limpiar() {

		$("input#fechaInicio").val("");
		$("input#fechaFin").val("");
		$("input#autoriza1").val("");
		$("input#autoriza2").val("");
		$("select#tipo").val("");
	}	
	$(document).ready(function(){
		$("#form").attr('action',baseurl+'reportes/reporte_consumo_quincenal/vistaPDF');
		/*$("#tipo").change(function(){
			var val=$(this).val();
			if (val=='2') {
				$("#form").attr('action',baseurl+'reportes/reporte_consumo_quincenal/reporte_detallado');
				$("#excel").show();
			} else {
				$("#form").attr('action',baseurl+'reportes/reporte_consumo_quincenal/vistaPDF');
				$("#excel").hide();
			}
		})
		$("#excel").click(function(e){
			if ($("input#fechaInicio").val()==='') {
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el principio de el rango de fecha a filtrar</div>");
			} else if($("input#fechaFin").val()===''){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el fin de el rango de fecha a filtrar</div>");
			}
		});*/
		$("#datos").click(function(e){
			if ($("input#fechaInicio").val()==='') {
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el principio de el rango de fecha a filtrar</div>");
			} else if($("input#fechaFin").val()===''){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el fin de el rango de fecha a filtrar</div>");
			}else if($("input#autoriza1").val()==='' && $("select#tipo").val()==='1'){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el nombre de la primer persona que autoriza el documento</div>");
			} else if($("input#autoriza1").val()==='' && $("select#tipo").val()==='1'){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el nombre de la primer persona que autoriza el documento</div>");				
			} else if($("select#tipo").val()==='0'){
				e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el tipo de reporte que desea Generar</div>");
			}
		});
	});
</script>