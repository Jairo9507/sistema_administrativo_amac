<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h4 class="content-title">Reportes Personalizados</h4>
			</div>
			<form method="POST" action="reportes_existencias_bodega/vistaPDF" >
				<div class="box-body">
					<div id="mensaje"></div>
					<div class="pad margin no-print" style="width: 75%;position: relative;bottom: 40px;left: 11%;">
						<div class="callout callout-info" style="margin-bottom: 0!important;">
							<h4><i class="fa fa-info"></i> Nota:</h4>
							Deje en blanco en caso de no necesitar los parámetros<br/>
							Marque en Historico para activar los calendarios en caso de generar un reporte historico, caso contrario dejarlo desmarcado
						</div>
					</div>
					<div class="row">
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Tipo Reporte</label>
								<div>
									<select class="form-control js-example-basic-single" id="tipo" name="tipo" style="width: 75%;">
										<option value="0">Seleccionar Tipo</option>
										<option value="1">Por producto</option>
										<option value="2">Completo</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-8" style="left: 2%;">
							<div class="form-group ">
								<div class="form-check">
								    <input type="checkbox" class="form-check-input" id="historico">
								    <label class="form-check-label" for="historico">Historico</label>
								  </div>									
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-4" style="left: 4%;display: none;" id="prueba">
							<div class="form-group">
								<label>Producto</label>
								<div>
									<select class="form-control js-example-basic-single" id="producto" name="producto" style="width: 75%;">
										<option value="0">Seleccionar Producto</option>
										<?php if ($productos) {
											foreach ($productos as $producto) {
												echo "<option value='".$producto->ID_PRODUCTO."'>".$producto->NOMBRE_PRODUCTO."</option>";
											}
										} 
										?>
									</select>
								</div>
							</div>							
						</div>
						<div class="col-md-8" style="display:none;" id="historico_calendario">
							<div class="col-md-6" style="left: 4%;">
								<div class="form-group">
									<label>Fecha inicio </label>
									<input type="date" name="fechaInicio" class="form-control" style="width: 75%;" id="fechaInicio">
								</div>
							</div>
							<div class="col-md-6" style="left: 4%;">
								<div class="form-group">
									<label>Fecha Final</label>
									<input type="date" name="fechaFinal" class="form-control" style="width: 75%;" id="fechaFinal">
								</div>
							</div>
						</div>						
					</div>
					<div class="row no-print">
						<div class="col-xs-12">
							<button type="button" class="btn btn-success pull-right" onclick="limpiar();"><i class="glyphicon glyphicon-refresh"></i> Limpiar
							</button>

							<button href="<?php echo base_url();?>reportes/reportes_existencias_bodega/vistaPDF" type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;" id="datos" target="blank"><i class="fa fa-download"></i> Generar PDF
							</button>;
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
</div>

<div class="modal fade text-center"  id="pdf">
	<div class="modal-dialog" style="width: 70%">
		<div class="modal-content">
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var base_url = "<?php echo base_url();?>";
	$(document).ready(function () {
		$("#producto").select2();
		$("#tipo").select2();
		


		$("#tipo").change(function() {
			var select = document.getElementById('prueba');
			if ($("select#tipo").val()==1) {
			select.style.display   = 'block';
			}else{
				select.style.display   = 'none';
			}
			
		});

		if ($("#historico").prop('checked')) {
			calendar.style.display ='block';
			$("#fechaInicio").attr('required',true);
			$("#fechaFinal").attr('required',true);			
		}
		$("#historico").click(function(e){
			var calendar = document.getElementById('historico_calendario');
			if ($(this).is(":checked")) {
				calendar.style.display ='block';
				$("#fechaInicio").attr('required',true);
				$("#fechaFinal").attr('required',true);
			} else {
				calendar.style.display= 'none';
				$("#fechaInicio").attr('required',false);
				$("#fechaFinal").attr('required',false);				
			}
		});

		$("#datos").click(function(e){
		 if($("input#fechaInicio").val()==='' && $("#historico").is(":checked") ){
		 	e.preventDefault();
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la fecha de inicio de filtrado del reporte</div>");
		} else if($("input#fechaFin").val()==='' && $("#historico").is(":checked") ){
			e.preventDefault();
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la fecha de finalizaci&oacute;n de filtrado del reporte </div>");				
		} 
	});

	});

	function limpiar() {

		//Limpiar datos al recargar
		$("#tipo").val('0');
		$('#tipo').trigger('change'); 
		$("#historico").prop('checked',false);
		$("#fechaInicio").val('');
		$("#fechaFinal").val('');
		$("#producto").val('0');
		$('#producto').trigger('change'); 
	}
</script>