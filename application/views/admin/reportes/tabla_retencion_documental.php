 <html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
            <head>

            </head>
<style type="text/css">
    table, td, th{
        border 1px solid black;
        padding: 1cm;
    }
</style>
            <body>
                <?php 

                    $filename = htmlspecialchars("Tabla de retención documental ").date('Y-m-d').".xls";
                    header("Content-Type: application/vnd.ms-excel"); //mime type
                    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                    header("Cache-Control: max-age=0"); //no cache
                
                 ?>                 
                     <table border='1'>
                        <thead>


                        </thead>
                        <tbody >
                            <tr>
                                <td colspan="8" style="width: 100% !important; height: 220px !important;"><img src="<?php echo base_url('img/MEMBRETE1-1.jpg')?>"  height="220" /></td>                                
                            </tr>                            
                            <tr>

                                <td colspan="8"   style="height: 100px;font-size: 20px; text-align: center;"><?php echo htmlentities("TABLA DE RETENCIÓN DOCUMENTAL");?></td>
                            </tr>
                            <tr>
                            <?php 

                            		if ($documentos) {

                            			foreach ($documentos as $d) {
                            				echo "<table>";
                            				echo "<tr>";
                            				echo "<td colspan='4' rowspan='2' style='text-align:center; font-size;16px; font-family:arial; font-weight:bold;'>".htmlentities($d->GERENCIA)." ".$d->CODIGO_GERENCIA."</td>";
                            				echo "<td colspan='3' rowspan='2' style='text-align:center; font-size;16px; font-family:arial; font-weight:bold;'>".$d->CODIGO_UNIDAD." ".htmlentities($d->UNIDAD)."</td>";
                            				echo "</tr>";
			
                            				echo "</table>";
                            				echo "<table border='1'>";
                            				echo "<tr>";                            	
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>".htmlentities("CÓDIGO SERIE DOCUMENTAL ")."</td>";
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>".htmlentities("CÓDIGO SUB SERIE DOCUMENTAL ")."</td>";
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>SERIES DOCUMENTALES</td>";
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>".htmlentities("VIGENCIA EN ARCHIVO DE GESTIÓN")."</td>";
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>VIGENCIA EN ARCHIVO CENTRAL</td>";
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>".htmlentities("DISPOSICIÓN FINAL")."</td>";
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>PROCEDIMIENTO</td>";

                            				echo "</tr>";
                            				echo "<tr>";
                            				echo "<td style='text-align:justify; font-size:12px; font-family:arial;'>".$d->SERIE_CODIGO."</td>";
                            				echo "<td style='text-align:justify; font-size:12px; font-family:arial;'>".$d->SUBSERIE_CODIGO."</td>";
                            				echo "<td style='text-align:justify; font-size:12px; font-family:arial;'>".$d->SERIE." <br/> ".$d->OBSERVACIONES."</td>";
                            				echo "<td style='text-align:justify; font-size:12px; font-family:arial;'>".$d->VIGENCIA_GESTION." ".htmlentities("año/s")." </td>";
                            				echo "<td style='text-align:justify; font-size:12px; font-family:arial;'>".$d->VIGENCIA_CENTRAL." ".htmlentities("año/s")."</td>";
                            				echo "<td >".$d->DISPOSICION_FINAL."</td>";
                            				echo "<td></td>";
                            				echo "</tr>";
                            				echo "</table>";
                            			}
                            		}
                             ?>
                            </tr>
                            <tr>
                            	<table style="border:1px solid black;">
                            		<tr>
                            			<td colspan="4"  style="text-align: center; font-size:16px;">
                            				________________________________<br/>
                            				FIRMA DE LA JEFATURA DE LA UNIDAD ARCHIVOS
                            			</td>
                            			<td colspan="4"  style="text-align: center; font-size:16px;">
                            				<?php echo date('M-Y');?><br/>
                            				<?php echo htmlentities("Fecha de Actualización")?>
                            			</td>
                            		</tr>
                            	</table>
                            </tr>
                              
                        </tbody>
                     </table>
            </body>
</html>    
    
