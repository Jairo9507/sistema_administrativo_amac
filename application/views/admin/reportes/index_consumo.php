<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h4 class="content-title">Reportes Personalizados</h4>
			</div>
			<form method="POST" action="reportes_consumo_bodega/vistaPDF" >
				<div class="box-body">
					<div id="mensaje"></div>
					<div class="pad margin no-print" style="width: 75%;position: relative;bottom: 40px;left: 11%;">
						<div class="callout callout-info" style="margin-bottom: 0!important;">
							<h4><i class="fa fa-info"></i> Nota:</h4>
							Deje en blanco en caso de no necesitar los parámetros
						</div>
					</div>
					<div class="row">
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha inicio </label>
								<input type="date" name="fechaInicio" class="form-control" style="width: 75%;" id="fechaInicio">
							</div>
						</div>
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Fecha Final</label>
								<input type="date" name="fechaFinal" class="form-control" style="width: 75%;" id="fechaFinal">
							</div>
						</div>
						<div class="col-md-4" style="left: 4%;">
							<div class="form-group">
								<label>Tipo Reporte</label>
								<div>
									<select class="form-control js-example-basic-single" id="condicion" name="condicion" style="width: 75%;">
										<option value="0">Seleccionar Condición</option>
										<option value="D">Detalle</option>
										<option value="C">Consolidado</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4" style="left: 4%;" id="producto_list">
							<div class="form-group">
								<label>Producto</label>
								<div>
									<select class="form-control js-example-basic-single" id="producto" name="producto" style="width: 75%;">
										<option value="0">Seleccionar Producto</option>
										<?php if ($productos) {
											foreach ($productos as $producto) {
												echo "<option value='".$producto->ID_PRODUCTO."'>".$producto->NOMBRE_PRODUCTO."".$producto->MEDICAMENTO."</option>";
											}
										} 
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-4" style="left: 4%;" id="tipo_list">
							<div class="form-group">
								<label>Consumo por:</label>
								<div>
								<select class="form-control js-example-basic-single" id="tipo" name="tipo"  style="width: 75%;">
									<option value="0">Seleccionar Opción</option>
									<option value='1'>Unidad Administrativa</option>
									<option value='2'>Cuenta Presupuestaria</option>
								</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row no-print">
						<div class="col-xs-12">
							<button type="button" class="btn btn-success pull-right" onclick="limpiar();"><i class="glyphicon glyphicon-refresh"></i> Limpiar
							</button>

							<button href="<?php echo base_url();?>reportes/reportes_consumo_bodega/vistaPDF" type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;" id="datos" target="blank"><i class="fa fa-download"></i> Generar PDF
							</button>;
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
</div>

<div class="modal fade text-center"  id="pdf">
	<div class="modal-dialog" style="width: 70%">
		<div class="modal-content">
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var base_url = "<?php echo base_url();?>";
	$(document).ready(function () {
		$("#producto").select2();
		$("#tipo").select2();
		//$("#condicion").select2();
		$("#tipo").change(function(){			
			var val=$(this).val();
			if (val!='0') {
				$("#producto_list").hide();
			} else {
				$("#producto_list").show();
			}
		});
		$("#producto").change(function(){
			var val=$(this).val();
			if (val!='0') {
				if ($("#condicion").val()!=='1') {
					alertify.alert("El tipo de reporte solo puede ser detalle",function(){
						$("#condicion").val('D');
						//$("#condicion").select2().trigger('change');
						//$("#condicion").prop('disabled',true);
					});
				}
				$("#tipo_list").hide();
			} else {
				$("#condicion").prop('disabled',false);
				$("#tipo_list").show();
			}
		});
		$("#datos").click(function(e){

			 if($("input#fechaInicio").val()===''){
			 	e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la fecha de inicio de filtrado del reporte</div>");
			} else if($("input#fechaFin").val()===''){
			 	e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione la fecha final de filtrado del reporte</div>");			
			} else if($("#condicion").val()==="1"){
				 	e.preventDefault();
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione el tipo de reporte</div>");					
			}
	});

	});

	function limpiar() {

		$("input#fechaInicio").val("");
		$("input#fechaFin").val("");
		$("#proveedor").val("");
		$("#producto").val("");
		$("#contrato").val("");
		$("textarea#concepto").val("");
		$("#tipo").val("");
		$("select#estado").val("");	
		$("#condicion").prop('disabled',false);
		$("#tipo_list").show();
		$("#producto_list").show();		
	}
</script>