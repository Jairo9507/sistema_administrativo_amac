 <html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
            <head>

            </head>
<style type="text/css">
    table, td, th{
        border 1px solid black;
        padding: 1cm;
    }
</style>
            <body>
                <?php 

                    $filename = htmlspecialchars("Tabla de valoración documental ").date('Y-m-d').".xls";
                    header("Content-Type: application/vnd.ms-excel"); //mime type
                    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                    header("Cache-Control: max-age=0"); //no cache
                
                 ?>                 
                     <table >
                        <thead>


                        </thead>
                        <tbody >
                            <tr>
                                <td colspan="6" style="width: 100% !important; height: 220px !important;"><img src="<?php echo base_url('img/MEMBRETE1-1.jpg')?>"  height="220" /></td>                        
                            </tr>                            
                            <tr>

                                <td colspan="6"   style="height: 100px;font-size: 20px; text-align: center;"><?php echo htmlentities("TABLA DE VALORACIÓN DOCUMENTAL");?></td>
                            </tr>
                            <tr>
                            <?php 

                            		if ($documentos) {

                            			foreach ($documentos as $d) {
                            				echo "<table>";
                            				echo "<tr>";
                            				echo "<td colspan='4' rowspan='2' style='text-align:center; font-size;16px; font-family:arial; font-weight:bold;'>ENTIDAD PRODUCTORA: ".$d->CODIGO_GERENCIA." ".htmlentities($d->GERENCIA)."</td>";
                            				echo "<td colspan='2' rowspan='2' style='text-align:center; font-size;16px; font-family:arial; font-weight:bold;'> UNIDAD ADMINISTRATIVA: ".$d->CODIGO_UNIDAD." ".htmlentities($d->UNIDAD)." </td>";
                            				echo "</tr>";
			
                            				echo "</table>";
                            				echo "<table border='1'>";
                            				echo "<tr>";                            	
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>".htmlentities("CORRELATIVO ARCHIVO CENTRAL")."</td>";
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>SERIES O ASUNTOS</td>";
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>VIGENCIA EN ARCHIVO CENTRAL</td>";
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>".htmlentities("DISPOSICIÓN FINAL")."</td>";
                            				echo "<td style='text-align:center; font-size;14px; font-family:arial;'>PROCEDIMIENTOS</td>";
                            				echo "</tr>";
                            				echo "<tr>";
                            				echo "<td style='text-align:justify; font-size:12px; font-family:arial;'>".$d->CORRELATIVO_CENTRAL."</td>";
                            				echo "<td style='text-align:justify; font-size:12px; font-family:arial;'>".$d->SERIE." <br/> ".$d->OBSERVACIONES."</td>";
                            				echo "<td style='text-align:justify; font-size:12px; font-family:arial;'>".$d->VIGENCIA_CENTRAL." ".htmlentities("año/s")."</td>";
                            				echo "<td >".$d->DISPOSICION_FINAL."</td>";
                            				echo "<td></td>";
                            				echo "</tr>";
                            				echo "</table>";
                            			}
                            		}
                             ?>
                            </tr>
                            <tr>
                            	<table style="border:1px solid black;">
                            		<tr>
                            			<td colspan="3">
                            				________________________________<br/>
                            				FIRMA DE APROBACION
                            			</td>
                            			<td colspan="2" style="text-align: center;">
                            				Fecha: <?php echo date('Y-m-d');?>
                            			</td>
                            		</tr>
                            	</table>
                            </tr>
                              
                        </tbody>
                     </table>
            </body>
</html>    
