<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h4 class="content-title">Reportes Personalizados</h4>
			</div>
			<form method="POST" action="reportes_orden_uaci/ordenPDF" >
			<div class="box-body">
				<div id="mensaje"></div>
				<div class="pad margin no-print" style="width: 75%;position: relative;bottom: 40px;left: 11%;">
					<div class="callout callout-info" style="margin-bottom: 0!important;">
						<h4><i class="fa fa-info"></i> Nota:</h4>
						Deje en blanco en caso de no necesitar los parámetros
					</div>
				</div>
				<div class="row">
					<div class="col-md-4" style="left: 4%;">
						<div class="form-group">
							<label>Fecha inicio</label>
							<input type="date" name="fechaInicio" class="form-control" style="width: 75%;" id="fechaInicio">
						</div>
					</div>
					<div class="col-md-4" style="left: 4%;">
						<div class="form-group">
							<label>Fecha Final</label>
							<input type="date" name="fechaFin" class="form-control" style="width: 75%;" id="fechaFin">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4" style="left: 4%;">
						<div class="form-group">
							<label>Proveedor</label>
							<div>
							<select class="form-control js-example-basic-single" id="proveedor" name="proveedor" style="width: 75%;">
								<option value="0">Seleccionar Proveedor</option>
								<?php if ($proveedores) {
									foreach ($proveedores as $proveedor) {
										echo "<option value='".$proveedor->ID_PROVEEDOR."'>".$proveedor->NOMBRE_PROVEEDOR."</option>";
									}
								} ?>
							</select>
							</div>
						</div>
					</div>
					<div class="col-md-4" style="left: 4%;">
						<div class="form-group">
							<label>Producto</label>
							<div>
							<select class="form-control js-example-basic-single" id="producto" name="producto" style="width: 75%;">
								<option value="0">Seleccionar Producto</option>
								<?php if ($productos) {
									foreach ($productos as $producto) {
										echo "<option value='".$producto->ID_PRODUCTO."'>".$producto->NOMBRE_PRODUCTO."</option>";
									}
								} 
								 ?>
							</select>
							</div>
						</div>
					</div>
					<div class="col-md-4" style="left: 4%;">
						<div class="form-group">
							<label>Descripción</label>
							<textarea class="form-control" name="descripcion" id="descripcion" rows="3" placeholder="Escribe aquí.." style="width: 75%;" ></textarea>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4" style="left: 4%;">
						<div class="form-group">
							<label>Concepto</label>
							<textarea class="form-control" name="concepto" id="concepto" rows="3" placeholder="Escribe aquí.." style="width: 75%;" ></textarea>
						</div>
					</div>
					<div class="col-md-4" style="left: 4%;">
						<div class="form-group">
							<label>Cuenta Presupuestaria</label>
							<select class="form-control js-example-basic-single" id="cuenta" name="cuenta"  style="width: 75%;">
								<option value="0">Seleccionar Cuenta</option>
								<?php if ($cuentas) {
									foreach ($cuentas as $cuenta) {
										echo "<option value='".$cuenta->ID_CAT_CUENTA."'>".$cuenta->COD_CUENTA."</option>";
									}
								} ?>
							</select>
						</div>
					</div>
					<div class="col-md-4" style="left: 4%;">
						<div class="form-group">
							<label>Estado de Orden de Compra</label>
							<select class="form-control js-example-basic-single" id="estado" name="estado" style="width: 75%;">
								<option value="8">Seleccionar Estado</option>
								<option value="1">Activas</option>
								<option value="0">Anuladas</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row no-print">
					<div class="col-xs-12">
						<button type="button" class="btn btn-success pull-right" onclick="limpiar();"><i class="glyphicon glyphicon-refresh"></i> Limpiar
						</button>

					<button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;" id="datos" target="blank"><i class="fa fa-download"></i> Generar PDF
					</button>;
					</div>
				</div>
			</div>
		</form>
		</div>
	</section>
</div>

<div class="modal fade text-center"  id="pdf">
  <div class="modal-dialog" style="width: 70%">
    <div class="modal-content">
    </div>
  </div>
</div>

<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var base_url = "<?php echo base_url();?>";
	$(document).ready(function () {
		$("#proveedor").select2();
		$("#producto").select2();
		$("#cuenta").select2();

		// $("#datos").click(function() {
		// 	var Reporte = new Object();

		// 	Reporte.fechaInicio = $("input#fechaInicio").val();
		// 	Reporte.fechaFin 	= $("input#fechaFin").val();
		// 	Reporte.proveedor  	= $("select#proveedor").val();
		// 	Reporte.producto 	= $("select#producto").val();
		// 	Reporte.contrato  	= $("select#contrato").val();
		// 	Reporte.concepto 	= $("textarea#concepto").val();
		// 	Reporte.cuenta 		= $("select#cuenta").val();
		// 	Reporte.estado 		= $("select#estado").val();
		// 	var DatosJson 		= JSON.stringify(Reporte);
		// 	console.log(Reporte);
		// 	console.log(DatosJson);	

		// 	$.post(base_url+'/reportes/reportes_orden_uaci/vistaPDF',{
		// 		datosPost: DatosJson
		// 	},function(data,textStatus){
		// 		console.log(data);
		// 		$("#mensaje").html(data.response_msg);
		// 		window.location.href=baseurl+'reportes_orden_uaci/vistaPDF';
	 //                //location.reload();             	
	 //            },"json").fail(function(response){
	 //            	$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
	 //            	console.log('Error: ' + response.responseText);
	 //            });;
		// });

	});

	function limpiar() {

		$("input#fechaInicio").val("");
		$("input#fechaFin").val("");
		$("#proveedor").val("");
		$("#producto").val("");
		$("#contrato").val("");
		$("textarea#concepto").val("");
		$("#cuenta").val("");
		$("select#estado").val("");	
	}
</script>