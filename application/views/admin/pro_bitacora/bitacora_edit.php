<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 
	$nombre_supervisor= array(
		'name' 		=> 'nombre_supervisor',
		'id'		=> 'nombre_supervisor',
		'value'		=> set_value('nombre_supervisor',@$bitacora[0]->NOMBRE_SUPERVISOR),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre del supervisor',
		'onkeypress'	=> 'return validarn(event)'
	);

	$fecha_inicio= array(
		'name' 		=> 'fecha_inicio',
		'id'		=> 'fecha_inicio',
		'value'		=> set_value('fecha_inicio',date('Y-m-d',strtotime(@$bitacora[0]->FECHA_INICIO))),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'placeholder' =>'dd/mm/yyyy'
	);				

	$hora_creacion= array(
		'name' 		=> 'hora_creacion',
		'id'		=> 'hora_creacion',
		'value'		=> set_value('hora_creacion',date('H:i',strtotime(@$bitacora[0]->HORA_CREACION))),
		'type'		=> 'time',
		'class'		=> 'form-control',
	);

	$clima_dia= array(
		'name' 		=> 'clima_dia',
		'id'		=> 'clima_dia',
		'value'		=> set_value('clima_dia',@$bitacora[0]->CLIMA_DIA),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Describa el clima',
	);
	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('descripcion',@$bitacora[0]->DESCRIPCION),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion',
		'onkeypress'	=> 'return validarn(event)'

	);

?>
<input type="hidden" name="id" id="id" value="<?php echo @$proyecto[0]->ID_PROYECTO?>">

<div class="content-wrapper">
	<section class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Datos</h3>
			</div>
			<div class="box-body">
				<div id="mensaje"></div>
		<form class="form-horizontal" name="formulario" id="formulario" role="form" >
			<input type="hidden" id="idBitacora" name="idBitacora" value="<?php echo @$bitacora[0]->ID_PROYECTO_BITACORA_DIARIA;?>">
			<div class="form-group">
				<label for='idTipoObra' class="form-label col-sm-2">Tipo de obra<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select name="idTipoObra" id="idTipoObra" class="form-control">
						<option>Seleccione una opción</option>
						<?php
							foreach ($tiposObras as $obra) {
								if ($obra->ID_CAT_TIPO_OBRA==@$bitacora[0]->ID_CAT_TIPO_OBRA) {
									echo "<option value=".$obra->ID_CAT_TIPO_OBRA." selected='true'>".$obra->ID_CAT_TIPO_OBRA." - ".$obra->DESCRIPCION."</option>";

								} else {
									echo "<option value=".$obra->ID_CAT_TIPO_OBRA.">".$obra->ID_CAT_TIPO_OBRA." - ".$obra->DESCRIPCION."</option>";

								}
							}
						?>
					</select>
				</div>
				<label for='idProyecto' class="form-label col-sm-2">Proyecto<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<label><?php echo @$bitacora[0]->NOMBRE_PROYECTO; ?></label>
				</div>
			</div>
			<div class="form-group">
				<label for='nombre_supervisor' class="form-label col-sm-2">Nombre del supervisor<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($nombre_supervisor);?>
				</div>
				<label for='fecha_inicio' class="form-label col-sm-2">Fecha de inicio<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($fecha_inicio);?>
				</div>				
			</div>
			<div class="form-group">
				<label for='hora_creacion' class="form-label col-sm-2">Hora de inicio<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($hora_creacion);?>
				</div>
				<label for='clima_dia' class="form-label col-sm-2">Clima del día<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($clima_dia);?>
				</div>				
			</div>
			<div class="form-group">
				<label for='descripcion' class="form-label col-sm-2">Descripción:</label>
				<div class="col-sm-4">
					<?php echo form_textarea($descripcion)?>
				</div>
				<label for='descripcion' class="form-label col-sm-2">Comprobante fotogr&aacute;fico:</label>
				<div class="col-sm-4">
					<div class="col-sm-12">
						<input type="file" id="picture" name="picture" >
					</div>
					<div class="col-sm-9">
						<img id="blah" class="img-responsive" src="<?php echo @$bitacora[0]->COMPROBANTE_FOTOGRAFICO?>" alt="Comprobante fotogr&aacute;fico" />
					</div>
				</div>
			</div>
		</form>
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	
				
			</div>
			
		</div>
		
	</section>
	
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"proyectos/bitacora"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"proyectos/cat_proyecto"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }


    $(document).ready(function(){
    	$("#picture").change(function(){
    		readURL(this);
    	});	
    	$("#idTipoObra").select2({
    		minimumInputLength: 0
    	});


    })
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsonBitacora.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>