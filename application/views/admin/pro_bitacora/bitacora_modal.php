<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 
	$nombre_supervisor= array(
		'name' 		=> 'nombre_supervisor',
		'id'		=> 'nombre_supervisor',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre del supervisor',
		'onkeypress'	=> 'return validarn(event)'
	);

	$fecha_inicio= array(
		'name' 		=> 'fecha_inicio',
		'id'		=> 'fecha_inicio',
		'value'		=> set_value('codigo',''),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'placeholder' =>'dd/mm/yyyy'
	);				

	$hora_creacion= array(
		'name' 		=> 'hora_creacion',
		'id'		=> 'hora_creacion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'time',
		'class'		=> 'form-control',
	);

	$clima_dia= array(
		'name' 		=> 'clima_dia',
		'id'		=> 'clima_dia',
		'value'		=> set_value('clima_dia',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Describa el clima',
	);
	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion',
		'onkeypress'	=> 'return validarn(event)'

	);

?>
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
	</div>
	<div class="modal-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form" >
			<div class="form-group">
				<label for='idTipoObra' class="form-label col-sm-2">Tipo de obra<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select name="idTipoObra" id="idTipoObra">
						<option>Seleccione una opción</option>
						<?php
							foreach ($tiposObras as $obra) {
								echo "<option value=".$obra->ID_CAT_TIPO_OBRA.">".$obra->ID_CAT_TIPO_OBRA." - ".$obra->DESCRIPCION."</option>";
							}
						?>
					</select>
				</div>
				<label for='idProyecto' class="form-label col-sm-2">Proyecto<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<input type="hidden" name="idProyecto2" id="idProyecto2" value="<?php echo $proyecto[0]->ID_PROYECTO;?>">
					<label>
						<?php
							if (!empty($proyecto)) {
								echo $proyecto[0]->NOMBRE_PROYECTO;
							}
						?>						
					</label>
				</div>
			</div>
			<div class="form-group">
				<label for='nombre_supervisor' class="form-label col-sm-2">Nombre del supervisor<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($nombre_supervisor);?>
				</div>
				<label for='fecha_inicio' class="form-label col-sm-2">Fecha de inicio<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($fecha_inicio);?>
				</div>				
			</div>
			<div class="form-group">
				<label for='hora_creacion' class="form-label col-sm-2">Hora de inicio<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($hora_creacion);?>
				</div>
				<label for='clima_dia' class="form-label col-sm-2">Clima del día<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($clima_dia);?>
				</div>				
			</div>
			<div class="form-group">
				<label for='descripcion' class="form-label col-sm-2">Descripción:</label>
				<div class="col-sm-4">
					<?php echo form_textarea($descripcion)?>
				</div>
				<label for="mantenimiento" class="form-label col-sm-2">Mantenimiento : </label>
				<div class="col-sm-4">
					<div class="form-check form-check-inline col-sm-6">
						<input class="form-check-input" type="radio" name="mantenimiento" id="" value="true" >
						<label class="form-check-label" for="radio_alergia">
							SI
						</label>  
					</div>
					<div class="form-check form-check-inline col-sm-6">
						<input class="form-check-input" type="radio" name="mantenimiento"  value="true" >
						<label class="form-check-label" for="radio_alergia">
							NO
						</label>  
					</div>										
				</div>
				<label for='descripcion' class="form-label col-sm-2">Comprobante fotogr&aacute;fico:</label>
				<div class="col-sm-4">
					<div class="col-sm-12">
						<input type="file" id="picture" name="picture">
					</div>
					<div class="col-sm-9">
						<img id="blah" class="img-responsive" src="#" alt="Comprobante fotogr&aacute;fico" />
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonBitacora.js');?>"></script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

	$(document).ready(function(){
    $("#picture").change(function(){
        readURL(this);
    });	
		$("#idTipoObra").select2({
			minimumInputLength: 0
		});


	});
</script>