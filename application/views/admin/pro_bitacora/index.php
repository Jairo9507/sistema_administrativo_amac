<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    var idProyecto2="<?php echo $idProyecto2;?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"proyectos/bitacora"
        } );
    }

function eliminarBitacora(id,fecha){
	alertify.confirm("Eliminar bitacora","¿Seguro de eliminar la bitacora de la fecha "+fecha+"?",function(){
    			 document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
    			 var bitacora=new Object();
    			 bitacora.id=id;
    			 var DatosJson=JSON.stringify(bitacora);
    			 console.log(DatosJson);
    			 $.post(baseurl+'proyectos/bitacora/eliminar',{
    			 	BitacoraPost:DatosJson
    			 },function(data,textStatus){
	                $("#mensaje").html(data.response_msg);
    			 },"json").fail(function(response){
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                        console.log('Error: ' + response.responseText);
    			 });;
	},function(){

		});

		
}
</script>
<div class="content-wrapper">
	<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>				
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="header-title">Registro y control de bitácora diaria</h3>
			</div>
			<div class="row">
				<div class="col-sm-2" style="text-align: center;">
					<label>Proyecto: </label>
				</div>
				<div class="col-sm-10">
					<select name="idProyecto" id="idProyecto" style="width: 60%;">
						<option value="">Seleccione una opción</option>
					<?php
						foreach ($proyectos as $proyecto) {
							if ($idProyecto2 == $proyecto->ID_PROYECTO) {

								echo "<option value='".$proyecto->ID_PROYECTO."'' selected='true'>".$proyecto->CODIGO_PROYECTO." - ".$proyecto->NOMBRE_PROYECTO."</option>";
							} else {
								echo "<option value='".$proyecto->ID_PROYECTO."''>".$proyecto->CODIGO_PROYECTO." - ".$proyecto->NOMBRE_PROYECTO."</option>";
							}
						}
					?>
					</select>
				</div>
			</div>
        	<div class="row" id="boton" style="display: none;">
        		<div class="col-sm-2 pull-right">
                    <a href="bitacora/nuevo?idProyecto2=<?php echo $idProyecto2;?>" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
        		</div>
        	</div>
			<div class="box-body" >
				<div id="mensaje"></div>
				<div id="tabla" style="display: ;">
					<table id="bitacoras" class="table table-bordered table-striped">
						<thead>
							<th>Nombre del proyecto</th>
							<th>Tipo de obra</th>
							<th>Nombre del supervisor</th>
							<th>Fecha de inicio</th>
							<th>Clima del día</th>
							<th>Descripción</th>
							<th></th>
						</thead>
						<tbody>
							<?php 
								if ($bitacoras) {
									foreach ($bitacoras as $b) {
										$bitacoraId=base64_encode($b->ID_PROYECTO_BITACORA_DIARIA);
										echo "<tr>";
										echo "<td>".$b->NOMBRE_PROYECTO."</td>";
										echo "<td>".$b->NOMBRE_OBRA."</td>";
										echo "<td>".$b->NOMBRE_SUPERVISOR."</td>";
										echo "<td>".date("d-m-Y",strtotime($b->FECHA_INICIO))."</td>";
										echo "<td>".$b->CLIMA_DIA."</td>";
										echo "<td>".$b->DESCRIPCION."</td>";
										echo "<td><a href='bitacora/editar/".$bitacoraId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
							?>
	                		<button type="button"  title="Eliminar Bitacora" onclick="eliminarBitacora('<?php echo $bitacoraId?>','<?php echo date("d-m-Y",strtotime($b->FECHA_INICIO)) ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
							<?php 
								echo "</td>";
								echo "</tr>";
									}
								} 
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
	
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#bitacoras").DataTable({
	      paging      : true,
	      lengthChange: true,
	      searching   : true,
	      ordering    : true,
	      "order" : [],
	      info        : true,
	      autoWidth   : false,
	      language: {
	        search:'Buscar:',
	        order: 'Mostrar Entradas',
	        paginate: {
	            first:"Primero",
	            previous:"Anterior",
	            next:"Siguiente",
	            last:"Ultimo"
	        },
	        emptyTable: "No hay informacion disponible",
	        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
	        lengthMenu:"Mostrar _MENU_ Entradas",
	        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
	      }    
		});

		$("#idProyecto").select2({
			minimumInputLength: 0
		});

		$('#idProyecto').on('change', function(){
			if ($(this).val()!='') {
				var url= baseurl+"proyectos/bitacora";
				window.location=url+'?idProyecto2='+$(this).val();				
			}

        });

		if (idProyecto2!="0") {
			$('#boton').show();
			$('#tabla').show();
		} else {
			$('#boton').hide();
			$('#tabla').hide();
		}			


	});


</script>
<div class="modal fade text-center" id="modal_nuevo" >
  <div class="modal-dialog" style="width: 80%">
    <div class="modal-content">
    </div>
  </div>
</div>