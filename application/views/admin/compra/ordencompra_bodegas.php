<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Detalle</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div id="mensaje"></div>
				<table id="proveedor" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>N° Orden Compra</th>
							<th>Proveedor</th>
							<th>Fecha Orden Compra</th>
							<th>Creado por</th>
							<th>Fecha Creación</th>
							<th>Estado</th>
							<th></th>
						</tr>
					</thead>
<tbody>
<?php if($detalle){
foreach ($detalle as $det) {
	$orden = base64_encode($det->ID_ORDEN_COMPRA);
	echo "<tr>";
	echo "<td>".$det->CODIGO_ORDEN."</td>";
	echo "<td>".$det->NOMBRE_PROVEEDOR."</td>";
	echo "<td>".date('d-m-Y',strtotime($det->FECHA_ORDEN))."</td>";
	echo "<td>".$det->USER."</td>";
	echo "<td>".date('d-m-Y',strtotime($det->FECHA_CREACION))."</td>";
	if ($det->ESTADO == 1 && $det->countDetalle>0) {
	echo "<td>ACTIVA</td>";
	} else if($det->ESTADO==1 && $det->countDetalle==0){
		echo "<td>CERRADA</td>";
	}else if($det->ESTADO ==0){
		echo "<td>ANULADA</td>";
	}
		echo "<td><a href='ordencompra_bodegas/editar/".$orden."'><button type='button' title='Editar Orden' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span></button></a>&nbsp;";
		echo ' <a href="ordencompra_bodegas/vistaPDF/'.$orden.'" data-target="#pdf" data-toggle="modal" data-backdrop="static" data-keyboard="false"><button type="button" title="PDF" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span></button></a></td>';
	
	echo "</tr>";
}
} ?> 	
</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->	
	</section>
</div>

<div class="modal fade text-center"  id="pdf">
  <div class="modal-dialog" style="width: 70%">
    <div class="modal-content">
    </div>
  </div>
</div>

<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	$(document).ready(function () {
		$('#proveedor').DataTable(
			{
		pageLength:5,
      "order" : [],
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }
	}
	);
});

function anularOrden(id,numOrden)
    {
    	alertify.confirm("Anular Orden de Compra","¿Seguro que desea anular la Orden N° "+numOrden+"?",function(){
             document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Anulando...</center></div></div>";
             var Orden =new Object();
             Orden.id=id;
             var DatosJson=JSON.stringify(Orden);
             $.post(baseurl+'/compra/ordencompra/anularOrden',{
             	AnularOrdenPost: DatosJson
             },function(data,textStatus){
             	console.log(data);
                $("#mensaje").html(data.response_msg);
                location.reload();             	
             },"json").fail(function(response){
                $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
             });;
              		
    	},function(){

    	});
    }
</script>