<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 

$codigo_orden= array(
  'name' 		=> 'codigo_orden',
  'id'		=> 'codigo_orden',
  'value'		=> set_value('codigo_orden',@$ordencompra[0]->CODIGO_ORDEN_COMPRA),
  'type'		=> 'text',
  'class'		=> 'form-control',
  'placeholder' => '',
  'disabled'	=> '',
  'style'		 => 'width:55%'	

);
$fecha= array(
  'name'        => 'fecha',
  'id'          => 'fecha',
  'value'       => set_value('fecha',date("Y-m-d", strtotime(@$ordencompra[0]->FECHA))),
  'type'        => 'date',
  'class'       => 'form-control',
  'placeholder' => '',
  'style'       => 'width:55%' 

); 

$condPago = array(
  'name'  	    =>  'condPago',
  'id'    	    =>  'condPago',
  'value' 	    =>   set_value('condPago', @$ordencompra[0]->CONDICIONES_PAGO),
  'type'  	    =>  'textarea',
  'class' 	    =>  'form-control',
  'placeholder' => '',
  // 'disabled'    =>'',
  'style'		    => 'width:90%; position: relative; bottom: 19px;height: 90px;'	
);

$concepto = array(
  'name'        =>  'concepto',
  'id'          =>  'concepto',
  'value'       =>   set_value('concepto', @$ordencompra[0]->VIA),
  'type'        =>  'textarea',
  'class'       =>  'form-control',
  'placeholder' => '',
  // 'disabled'    =>'',
  'style'       => 'width:90%; position: relative; bottom: 80px;height: 90px;'  
);

?>
<input type="hidden" name="id_orden_compra" id="id_orden_compra" value="<?php echo @$ordencompra[0]->ID_ORDEN_COMPRA?>">
<input type="hidden" name="id_detalle" id="id_detalle" value="<?php echo @$detalles[0]->ID_DETALLE?>">
<div class="content-wrapper">
  <section class="content-header">
   <?php echo $pagetitle; ?>
   <?php echo $breadcrumb; ?>
 </section>
 <div class="box">
  <div class="box-header">
   <h3 class="header-title"></h3>
 </div>
 <div class="box-body">
   <div id="mensaje"></div>
   <form class="form-horizontal" name="formulario" id="formulario" role="form">
    <div class="form-group">
     <label for="codigo" class="col-sm-2 form-label" style="text-align: center; top: 12px;">Orden de Compra N°: </label>
     <div class="col-sm-4">
      <?php  echo form_input($codigo_orden);?>
    </div>
    <label for="proveedor" class="col-sm-2 form-label" style="text-align: center;top: 12px;">Proveedor: </label>
    <div id="proveedor" class="col-sm-4">
      <select class="form-control" id="proveedor" style="width: 90%">
        <?php
        if($proveedores){
          foreach ($proveedores as $p) {
            if ($p->ID_PROVEEDOR==@$ordencompra[0]->ID_PROVEEDOR) {
              echo "<option value='".$p->ID_PROVEEDOR."' selected>".$p->NOMBRE_PROVEEDOR."</option>";
            }
            echo "<option value='".$p->ID_PROVEEDOR."'>".$p->NOMBRE_PROVEEDOR."</option>";
          }
        }
        ?> 
      </select> 
    </div> 
  </div>
  <div class="form-group">
    <label for="fecha" class="col-sm-2 form-label" style="text-align: center;top: 8px;">Fecha: </label>
    <div id="fecha" class="col-sm-4">
      <?php echo form_input($fecha);?>
    </div> 
    <label for="condPago" class="col-sm-2 form-label" style="text-align: center;top: 10px;">Condición de pago:</label>
    <div class="col-sm-4">
      <h3><?php  echo form_textarea($condPago);?></h3>
    </div>	

  </div>
<div class="form-group">
    <label for="conceptp" class="col-sm-2 form-label" style="text-align: center; bottom: 72px;">Concepto:</label>
    <div class="col-sm-4">
      <?php  echo form_textarea($concepto);?>
    </div>
    <label for="condPago" class="col-sm-2 form-label" style="text-align: center;bottom: 30px;">Tipo de ingreso:</label>
    <div class="col-sm-4" style="bottom: 35px;">
       <select class="form-control js-example-basic-single" id="tipoOrden" style="width: 76%;">
                  <option value="0">Seleccione tipo de orden</option>
                  <option value="1">Productos</option>
                  <option value="2">Obras</option>
                  <option value="3">Bienes y Servicios</option>
                  <option value="4">Recibo</option>
                  <option value="5">Otros</option>
                </select>
    </div>  
</div>
  <div class="form-group">
   <div class="col-sm-3 pull-right">
    <button type="button" class="btn btn-info pull-right" id="ActualizarOrden" style="position: relative;right: 383px; bottom: 70px;" onclick="this.disabled='disabled'"><span class="glyphicon glyphicon-plus"></span>Actualizar</button>
  </div>
</div>
</form>

<!--DIV PARA PRODUCTOS -->
       <div class="row" style="display: none;position: relative;bottom: 54px;" id="formularioProductosNuevos" name="formularioProductosNuevos"> 
        <!-- <div class="row"  id="productosNuevos" name="productosNuevos"> -->
        <form>
          <div class="col-md-3" style="left: 4%;" >
            <div class="form-group">
                <label>Producto: </label>
                <select class="form-control js-example-basic-single" id="productos" style="width: 100%;">
                  <?php if ($productos){
                    foreach ($productos as $pro) {
                      echo "<option value='".$pro->ID_PRODUCTO."'>".$pro->NOMBRE_PRODUCTO." ".$pro->MEDICAMENTO."</option>";
                        }
                      }
                   ?>
                </select>
            </div>
          </div>

          <div class="col-md-2" style="left: 4%;">
              <div class="form-group">
                <label>Cantidad Total: </label>
                <input type="text" class="form-control" id="cantidad" placeholder="100" onkeypress="return justNumbers(event);" style="width: 60%;" >
              </div>
          </div>
          <div class="col-md-2" style="right:1%;">
              <div class="form-group">
                <label>Precio Unitario $: </label>
                <input type="text" class="form-control" id="precio" placeholder="1.00" onkeypress="return justNumbers(event);" style="width: 70%;" >
              </div>
          </div>
          <div class="col-md-2" style="right:5%;">
            <div class="form-group">
              <label>Nombre de catalogo/Descripción: </label>
              <textarea class="form-control" id="descripcionOrden" rows="3" placeholder="Enter ..." style="width: 150%;"></textarea>
            </div>
          </div>
          <form id="formu">
          <div class="col-md-2" style="left: 2%;">
            <div class="form-group">
              <div class="radio" id="destino">
                <label>
                  <input type="radio" name="option" id="d1" value="E">
                  Existencias
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="option" id="d2" value="G">
                  Gastos
                </label>
              </div>
            </div>
          </div>
          </form>
        <div class="col-md-1" style="right:25px; top: 23px;">
              <div class="form-group">
                <button type="button" class="btn btn-primary" id="btnAddProduct" ><span class="glyphicon glyphicon-plus" ></span> </button>
              </div>
          </div>
          </form>
          </div>
<!--FIN DIV PRODUCTOS -->

<!--DIV PARA OTROS PRODUCTOS -->
     <div class="row" style="position: relative;bottom: 60px;display: none;" id="formularioProductosOtros" name="formularioProductosOtros" >
     <!--  <div class="row" id="productosNuevosOtros" name="productosNuevosOtros"> -->
        <form>
          <div class="col-md-3" style="left: 4%;" >
            <div class="form-group">
                <label>Cuenta Presupuestario: </label>
                <input type="text" class="form-control" id="codPresupuestario" placeholder="100" onkeypress="return justNumbers(event);" style="width: 65%;" >
            </div>
          </div>
          <div class="col-md-2" style="right:4%;">
            <div class="form-group">
              <label>Descripción: </label>
              <textarea class="form-control" id="descripcionOrden1" rows="3" placeholder="Enter ..." style="width: 170%;"></textarea>
            </div>
          </div>

          <div class="col-md-2" style="left: 6%;">
              <div class="form-group">
                <label>Cantidad Total: </label>
                <input type="text" class="form-control" id="cantidad1" placeholder="100" onkeypress="return justNumbers(event);" style="width: 65%;" >
              </div>
          </div>
          <div class="col-md-2" style="left: 1%;">
              <div class="form-group">
                <label>Precio Unitario $: </label>
                <input type="text" class="form-control" id="precio1" placeholder="1.00" onkeypress="return justNumbers(event);" style="width: 70%;" >
              </div>
          </div>
        <div class="col-md-1" style="right:30px; top: 23px;">
              <div class="form-group">
                <button type="button" class="btn btn-primary" id="btnAddProduct2" ><span class="glyphicon glyphicon-plus" ></span> </button>
              </div>
          </div>
          </form>
          </div>
<!-- FIN DE DIV DE OTROS PRODUCTOS -->

  <div class="row" id="formularioDetalleOrden" name="detalleOrden" style="position: relative;bottom: 45px;display: none;">
          <div class="col-md-4" style="left: 4%; bottom: 35px;">
            <div class="form-group">
              <label>Detalle</label>
              <textarea class="form-control" id="descripcionOrden3" rows="3" placeholder="Enter ..."></textarea>
            </div>
          </div>
          <div class="col-md-2" style="left: 4%; bottom: 35px;">
              <div class="form-group">
                <label>Total $: </label>
                <input type="text" class="form-control" id="precio3" placeholder="$1.00" onkeypress="return justNumbers(event);" >
              </div>
          </div>
          <div class="col-md-1" style="left:40px; bottom: 12px;">
              <div class="form-group">
                <button type="button" class="btn btn-primary" id="btnAddProduct3" ><span class="glyphicon glyphicon-plus" ></span> </button>
              </div>
          </div>
        </div>


 
<div id="tablaProductos" class="col-sm-12" style="bottom: 30px;">
  <table id="products" class="table table-bordered table-striped"  style="width: 99%;position: initial;left: 35px;">
    <thead>
      <tr>
        <th style="display: none;">IdProducto</th>
        <th style="width: 15%;">Cantidad</th>
        <th style="width: 30%;">Descripción</th>
        <th style="width: 15%;">Precio Unitario</th>
        <th style="width: 15%;">Total</th>
        <th style="width: 1%;"></th>
</tr>
</thead>
<tbody id="tableProducts">
 <?php 
 $totalCompra=0.0;
 if ($detalles) {
  foreach ($detalles as $d) {
    $detalle = base64_encode($d->ID_DETALLE);
    echo "<tr>";

    if ($d->CODIGO_PRESUPUESTARIO == 1) {
      echo "<td style='width: 15%;'></td>";
      echo "<td class='col-xs-1'> <textarea class='form-control' rows='3' placeholder='Enter ...' id='descripción' style='width: 30%;'>".$d->DESCRIPCION."</textarea></td>";
      echo "<td class='col-xs-1' style='width: 15%;'></td>";
      echo "<td class='col-xs-1' style='width: 15%;'>".$d->PRECIO."</td>";
      echo "";
    }else {
      echo '<td class="Idproducto" style="display:none;">'.$d->ID_PRODUCTO.'</td>';      
     echo "<td class='col-xs-1' style='width: 15%;'> <input class='form-control' type='text' id='cantidad_".$d->ID_PRODUCTO."' value='".$d->CANTIDAD_TOTAL."' style='text-align:center;'></td>";
     echo "<td class='col-xs-1' style='width: 30%;'> <textarea class='form-control' rows='3' placeholder='Enter ...' id='descripcion_".$d->ID_PRODUCTO."' style='width:100%;'>".$d->NOMBRE_CATALOGO." /".$d->DESCRIPCION."</textarea></td>";
     echo "<td class='col-xs-1' style='width: 15%;'> <input class='form-control' type='text' id='precio_".$d->ID_PRODUCTO."' value='".$d->PRECIO."' style='text-align:center;'></td>";
     echo "<td class='col-xs-1' style='width: 15%;'>$".number_format($d->TOTAL,4)."</td>";
     $totalCompra=$totalCompra+$d->TOTAL;
              // echo "";

   }
   ?>
   <td style="width: 1%;"><span class='pull-right'><a href='#' onclick='modificar("<?php echo $d->ID_PRODUCTO?>","<?php echo $d->ID_DETALLE?>")'><i class='glyphicon glyphicon-pencil'></i></a></span></td>

   <?php 

   echo "</tr>";                                                
 }
}
?>
</tbody>
</table> 


</div>	
</div> 		
<div class="box-footer">
            <div class="col-sm-2 col-sm-offset-10">
              <input type="text" disabled="true" name="totalCompra" id="totalCompra" class="form-control" value="<?php echo $totalCompra; ?>">
            </div>  
  <button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button> 
  <button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>        
</div>		
</div>

</div>
</div>

<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
    var base_url = "<?php echo base_url();?>";
    $(document).ready(function () {
      var totalCompra=$("#totalCompra").val();      
    $("textarea#concepto").keypress(function(e){
        var value=$(this).val();
        console.log(value.length);
        var maximo=2500;
        if (value.length===maximo) {
          alertify.warning("Ha llegado al maximo de caracteres disponibles");
          return   false;
        }

    });      
      $("#proveedor").select2();
      $("#productos").select2();
      $("#unidad_id").select2();
      $("#pago").change(function() {
        var val = $("select#pago").val();
        var input = document.getElementById('detalleCondicion');
        var input2 = document.getElementById('tipo');
        var input3 = document.getElementById('via');
        var input4 = document.getElementById('unidad')
        if (val == 3) {
          input.style.display   = 'block';
          input2.style.left     = '14%';
          input3.style.left     = '-46%';
          input4.style.position = 'relative';
          input4.style.left     = '-44%';
          input4.style.bottom   = '-35px';
        } else{
         var input = document.getElementById('detalleCondicion');
         input.style.display = 'none';
         input2.style.left = '5%';
         input3.style.left = '6%';
         input4.style.left = '4%';
         input4.style.bottom = '40px';

        $("textarea#detalleCondPago").val("");
          var input2 = document.getElementById('tipo');
        }
      });
    $("#tipoOrden").change(function() {
        var option = $("select#tipoOrden").val();
        var input1 = document.getElementById('formularioProductosNuevos');
        var input2 = document.getElementById('formularioProductosOtros'); 
        var input3 = document.getElementById('formularioDetalleOrden');

        if (option == 1) {
          input1.style.display  = 'block';
          input2.style.display = 'none';
          input.style.display  = 'none';
        }else if (option == 5) {
          input1.style.display  = 'none';
          input2.style.display = 'block';
          input3.style.display = 'none';
        }else if(option == 0) {
          input1.style.display  = 'none';
          input2.style.display = 'none';
          input3.style.display = 'none';
        }else {
          input1.style.display  = 'none';
          input2.style.display = 'none';
          input3.style.display = 'block';
        }
    });

    $("#tipoEntrega").change(function() {
      var val = $("select#tipoEntrega").val();
      var f = document.getElementById('fechaEntrega');
      f.style.display = 'block';
      
    });
      var products=[];
    $('#example1').DataTable({
      pageLength:5,
      "order" : [],
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }            
      
    });

    $("#ActualizarOrden").click(function(e) {

      // var formulario = document.getElementById('productosNuevos');
      // formulario.style.display = 'block';
      var ordenCompra = new Object();
      ordenCompra.id_orden_compra = $("input#id_orden_compra").val();
      ordenCompra.fecha = $("input#fecha").val();
      ordenCompra.proveedor = $("select#proveedor").val();
      ordenCompra.concepto = $("textarea#concepto").val();
      ordenCompra.condPago = $("textarea#condPago").val();
      var DatosJson = JSON.stringify(ordenCompra);
      console.log(DatosJson);
      $.post(base_url+"compra/detalle_ordencompra/actualizarOrden",{
        ordenUpdatePost : DatosJson
      }, function(data, textStatus) {
        $("#mensaje").append(data.response_msg);
      }, "json").fail(function (response) {
       $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
       console.log('Error: ' + response.responseText);
     });
    });

    $("#btnAddProduct").click(function() {
      var product = new Object();
      var html = '';
      var p    = document.getElementById("productos");

      if (document.getElementById('d1').checked) {
        product.destino = document.getElementById('d1').value;
      }else if (document.getElementById('d2').checked) {
        product.destino = document.getElementById('d2').value;
      }

      // var cantidad = parseInt();
      product.nombre = p.options[p.selectedIndex].text;
      product.id_orden_compra = $("input#id_orden_compra").val();
      product.id_producto     = $("select#productos").val();
      product.precio          = $("input#precio").val();
      product.cantidad        = $("input#cantidad").val();
      product.cuenta          = '';
      product.codOrden        = $("input#id_orden_compra").val();
      product.descripcion     = $("textarea#descripcionOrden").val(); 
      product.concepto      = $("textarea#concepto").val();

      products.push(product);
      console.log(products);

      html+='<tr>';
      html+='<td>'+product.cantidad+'</td>';
      html+='<td class="Idproducto" style="display:none;">'+product.id_producto+'</td>';
      // html+='<td>'+product.nombre+'</td>';
      html+='<td>'+product.descripcion+'</td>';
      html+='<td>'+"$"+product.precio+'</td>';
      html+='<td>'+"$"+Number(Math.round(parseFloat(product.precio*product.cantidad)+'e4')+'e-4').toFixed(4)+'</td>';
      html+='<td><button type="button"  title="Eliminar Estudio"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>';
      html+='</tr>';
      totalCompra=parseFloat(totalCompra)+parseFloat(product.precio*product.cantidad);
      $("#totalCompra").val(totalCompra.toFixed(4));
      console.log(html);
      $("#tableProducts").append(html);
      $("input#precio").val("");
      $("input#cantidad").val("");
      $("textarea#descripcionOrden").val("");
      $("input#codPresupuestario").val("");

       
    });
      $("#btnAddProduct2").click(function() {
      var product = new Object();
      var html = '';
      var p = document.getElementById("productos");
      product.nombre = p.options[p.selectedIndex].text;
      product.id_orden_compra = $("input#id_orden_compra").val();
      product.codOrden        = $("input#id_orden_compra").val();
      var   precio2           = '';
      product.precio          = $("input#precio3").val();
      var   cantidad          = '';
      product.descripcion     = $("textarea#descripcionOrden3").val(); 
      product.destino         ='G';
      product.cuenta          =1;
      product.id_producto     ='';
      product.cantidad        ='';
      products.push(product);
      console.log(products);
      html+='<tr>';
      html+='<td>'+cantidad+'</td>';
      html+='<td class="Idproducto" style="display:none;">'+product.id_producto+'</td>';
      // html+='<td>'+product.nombre+'</td>';
      html+='<td>'+product.descripcion+'</td>';
      html+='<td>'+precio2+'</td>';
      html+='<td>'+"$"+product.precio+'</td>';
      html+='<td><button type="button"  title="Eliminar Estudio"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>';
      html+='</tr>';
      totalCompra=parseFloat(totalCompra)+Number(Math.round(parseFloat(product.precio*product.cantidad)+'e4')+'e-4');
      $("#totalCompra").val(totalCompra.toFixed(4));      
      console.log(html);
      $("#tableProducts").append(html);
      $("input#precio").val("");
      $("input#cantidad").val("");
       
    });
     $("#btnAddProduct3").click(function() {
      var product = new Object();
      var html = '';
      var p    = document.getElementById("productos");




        product.destino = 'G';
      // var cantidad = parseInt();
     // product.nombre = p.options[p.selectedIndex].text;
      product.id_orden_compra = $("input#id_orden_compra").val();
      product.id_producto     = 0;
      product.precio          = $("input#precio1").val();
      product.cantidad        = $("input#cantidad1").val();
      product.cuenta          = $("input#codPresupuestario").val();
     //product.destino         = $("input#destino").val();
      // product.tipoEntrega     = $("select#tipoEntrega").val();
      product.codOrden        = $("input#id_orden_compra").val();
      product.descripcion     = $("textarea#descripcionOrden1").val(); 

      products.push(product);
      console.log(products);
      html+='<tr>';
      html+='<td>'+product.cantidad+'</td>';
      html+='<td class="Idproducto" style="display:none;">'+product.id_producto+'</td>';
      // html+='<td>'+product.nombre+'</td>';
      html+='<td>'+product.descripcion+'</td>';
      html+='<td>'+"$"+product.precio+'</td>';
      html+='<td>'+"$"+parseFloat(product.precio*product.cantidad)+'</td>';
      html+='<td><button type="button"  title="Eliminar Estudio"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>';
      html+='</tr>';
      console.log(html);
      $("#tableProducts").append(html);
      $("input#precio1").val("");
      $("input#cantidad1").val("");
      $("textarea#descripcionOrden1").val("");
       
    });

$("#btnGuardar").click(function() {
      alertify.confirm("¿Es la fecha correcta?"+"  "+$("input#fecha").val(),"Si da click en ACEPTAR se guardará el registro",function(){
          document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ base_url +"/img/gif-load.gif'> Cargando...</center></div></div>";

        var Orden = new Object();
        Orden.id = '';
        Orden.tipoOrden = $("select#tipoOrden").val();
        Orden.products = products;
        Orden.codOrden = $("input#id_orden_compra").val();
        console.log(Orden);
        $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ base_url +"/img/gif-load.gif'> Cargando...</center></div></div>");
        var DatosJson = JSON.stringify(Orden);
        $.post(base_url+"compra/ordencompra/agregarPedido",{
            ordenPost: DatosJson
        }, function (data, textStatus) {
            console.log(data);
            $("#mensaje").html(data.response_msg);
           //window.location.href = base_url+"compra/detalle_ordencompra";
           location.reload();
        }, "json").fail(function (response) {
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: '+response.responseText);
        });;
         
      },function(){

      }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    });


    $(document).on("click",".btnEliminar",function(){
      var id=$(this).closest('tr').find(".Idproducto").text();
      console.log(id);
      console.log(products);
      for (var i = 0; i < products.length; i++) {
          if(products[i].id_producto==id){            
           var descuento=parseFloat(products[i].precio*products[i].cantidad);
           //console.log('descuento '+descuento);
           var total=$("#totalCompra").val()-descuento;
           //console.log('total '+total);
           totalCompra=total;
            $("#totalCompra").val(total);        
            products.splice(i,1);            
          }
      }
      console.log(products);
      $(this).closest('tr').remove();
    });
  });


  function salir()
    {
      alertify.confirm("¿Esta seguro de guardar la Orden de Compra?","Click en ACEPTAR para guardar",function(){
          window.location.href = base_url+"compra/detalle_ordencompra"

      },function(){

      }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }
    $("#btnAgregarProducto").click(function(e) {
        $("#btnGuardar").attr("disabled", false);
        var ordenCompra = new Object();
        ordenCompra.id='';
        var tipoOrden = $("select#tipoOrden").val();
        var to   = document.getElementById("pago"); 
        var option = to.options[to.selectedIndex].text;
        var pago = $("select#pago").val();
        //ordenCompra.numFactura  = $("input#numFac").val();
        ordenCompra.id_proveedor  = $("select#proveedor").val();
        ordenCompra.via           = $("textarea#detvia").val();
        ordenCompra.codigoOrden   = $("input#id_orden_compra").val();
        ordenCompra.unidad=$("select#unidad_id").val();
        if (pago==3) {
          ordenCompra.condicionPago = $("textarea#detalleCondPago").val();
        }else{
        ordenCompra.condicionPago = to.options[to.selectedIndex].text;
        }
        ordenCompra.sp            = $("input#codsistema").val();        
        ordenCompra.proveedor     = $("select#proveedor").val();
        ordenCompra.fecha         = $("input#fecha").val();

      if (ordenCompra.proveedor==0) { 
          e.preventDefault();
          e.stopPropagation();
          $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione Proveedor</div>");
        } else if (ordenCompra.fecha=='') {
          e.preventDefault();
          e.stopPropagation();
          $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione Fecha</div>");
        }else if (ordenCompra.condicionPago == 0) {
          e.preventDefault();
          e.stopPropagation();
          $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione Condición de pago</div>");
        }else if (ordenCompra.condicionPago == 3) {
          e.preventDefault();
          e.stopPropagation();
          $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese detalle de condición de pago</div>");
        }else if (tipoOrden == 0) {
          e.preventDefault();
          e.stopPropagation();
          $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione el tipo de orden</div>");
        }else if (tipoOrden == 1) {
        var formulario = document.getElementById('productosNuevos');
        formulario.style.display  = 'block';
        var DatosJson = JSON.stringify(ordenCompra);
           console.log(DatosJson);
        $.post(base_url+"compra/ordencompra/agregarOrdenCompra",{
          ordenCompraPost: DatosJson
        },function(data,textStatus) {
          $("#mensaje").append(data.response_msg);
        },"json").fail(function(response) {
           $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
        console.log('Error: ' + response.responseText);
        });
        }else if(tipoOrden==5){
        var formulario = document.getElementById('productosNuevosOtros');
        formulario.style.display  = 'block';
        var DatosJson = JSON.stringify(ordenCompra);
           console.log(DatosJson);
        $.post(base_url+"compra/ordencompra/agregarOrdenCompra",{
          ordenCompraPost: DatosJson
        },function(data,textStatus) {
          $("#mensaje").append(data.response_msg);
        },"json").fail(function(response) {
           $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
        console.log('Error: ' + response.responseText);
        });

        }else{
          var formulario = document.getElementById('detalleOrden');
        formulario.style.display  = 'block';
        var DatosJson = JSON.stringify(ordenCompra);
           console.log(DatosJson);
        $.post(base_url+"compra/ordencompra/agregarOrdenCompra",{
          ordenCompraPost: DatosJson
        },function(data,textStatus) {
          $("#mensaje").append(data.response_msg);
        },"json").fail(function(response) {
           $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
        console.log('Error: ' + response.responseText);
        });
        }
    });

        function justNumbers(e)
            {
            var keynum = window.event ? window.event.keyCode : e.which;
            if ((keynum == 8) || (keynum == 46))
            return true;
             
            return /\d/.test(String.fromCharCode(keynum));
            }

      function eliminarfila(products)
          {
            console.log(products);
            var i = products.rowIndex;
            console.log(products.rowIndex);
            //document.getElementById("tableProducts").deleteRow(0);
          } 

  
</script>
