<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
 ?>
 <div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Detalle</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="presupuesto" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Cifra</th>
							<th>Detalle</th>
							<th>Total</th>
							<th></th>
						</tr>
					</thead>
					
				</table>
			</div>
		</div>
	</section>
</div>