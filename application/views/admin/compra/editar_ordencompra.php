<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 

$codigo_orden= array(
  'name' 		=> 'codigo_orden',
  'id'		=> 'codigo_orden',
  'value'		=> set_value('codigo_orden',@$ordencompra[0]->CODIGO_ORDEN_COMPRA),
  'type'		=> 'text',
  'class'		=> 'form-control',
  'placeholder' => '',
  'disabled'	=> '',
  'style'		 => 'width:55%'	

);
$sp= array(
  'name'    => 'sp',
  'id'    => 'sp',
  'value'   => set_value('',@$ordencompra[0]->FECHA),
  'type'    => 'text',
  'class'   => 'form-control',
  'placeholder' => '',
  'disabled'  => '',
  'style'    => 'width:55%' 

);

$num_factura =array(
  'name' 		=> 'num_factura',
  'id'		=> 'num_factura',
  'value'		=> set_value('num_factura',@$ordencompra[0]->NOMBRE_PROVEEDOR),
  'type'		=> 'text',
  'class'		=> 'form-control',
  'placeholder' => '',
  'disabled'    =>'',
  'style'		 => 'width:90%'	
);
$total = array(
  'name'  	    =>  'total',
  'id'    	    =>  'total',
  'value' 	    =>   set_value('total', @$ordencompra[0]->CONDICIONES_PAGO),
  'type'  	    =>  'textarea',
  'class' 	    =>  'form-control',
  'placeholder' => '',
  'disabled'    =>'',
  'style'		    => 'width:90%; position: relative; bottom: 19px;height: 90px;'	
);

$factura =array(
  'name'    => 'factura',
  'id'    => 'factura',
  'value'   => set_value('factura',@$ordencompra[0]->NUMERO_FACTURA),
  'type'    => 'text',
  'class'   => 'form-control',
  'placeholder' => '12345',
  'style'    => 'width:90%' 
);


?>
<input type="hidden" name="id_orden_compra" id="id_orden_compra" value="<?php echo @$ordencompra[0]->ID_ORDEN_COMPRA?>">
<input type="hidden" name="id_detalle" id="id_detalle" value="<?php echo @$detalles[0]->ID_DETALLE?>">
<div class="content-wrapper">
  <section class="content-header">
   <?php echo $pagetitle; ?>
   <?php echo $breadcrumb; ?>
 </section>
 <div class="box">
  <div class="box-header">
   <h3 class="header-title"></h3>
 </div>
 <div class="box-body">
   <div id="mensaje"></div>
   <form class="form-horizontal" name="formulario" id="formulario" role="form">
    <div class="form-group">
     <label for="codigo" class="col-sm-2 form-label" style="text-align: center; top: 12px;">Orden de Compra N°: </label>
     <div class="col-sm-4">
      <?php  echo form_input($codigo_orden);?>
    </div>
    <label for="numFactura" class="col-sm-2 form-label" style="text-align: center;top: 12px;">Proveedor: </label>
    <div id="numFactura" class="col-sm-4">
      <?php  echo form_input($num_factura);?>
    </div> 
  </div>
  <div class="form-group">
    <label for="sp" class="col-sm-2 form-label" style="text-align: center;top: 8px;">Fecha: </label>
    <div id="sp" class="col-sm-4">
      <?php  echo form_input($sp);?>
    </div> 
    <label for="codigo" class="col-sm-2 form-label" style="text-align: center;top: 10px;">Condición de pago:</label>
    <div class="col-sm-4">
      <h3><?php  echo form_textarea($total);?></h3>
    </div>						
  </div>
  <div class="form-group">
    <label for="factura" class="col-sm-2 form-label" style="text-align: center;bottom: 72px;">Factura</label>
    <div class="col-sm-4" style="bottom: 73px; width: 23%;">
      <?php echo form_input($factura)?>
    </div>
    <div class="col-sm-4">

    </div>
  </div>
<!--   <div class="form-group">
   <div class="col-sm-3 pull-right">
    <button type="button" class="btn btn-info pull-right" id="btnAgregarProducto" style="position: relative;right: 200px; bottom: 20px;" onclick="this.disabled='disabled'"><span class="glyphicon glyphicon-plus"></span>Agregar productos</button>
  </div>
</div> -->
</form>
<div>
 <!--  <div class="row"  id="productosNuevos" name="productosNuevos" style="display: none;">
    <form > -->
<!--       <div class="col-md-3" style="left: 45px;">
        <div class="form-group">
          <label>Producto: </label>
          <select class="form-control js-example-basic-single" id="productos" style="width: 100%;">
            <?php if ($productos){
              foreach ($productos as $pro) {
                echo "<option value='".$pro->ID_PRODUCTO."'>".$pro->NOMBRE_PRODUCTO."</option>";
              }
            }
            ?>
          </select>
        </div>
      </div> -->
<!-- 
      <div class="col-md-1" style="left: 25px; bottom: 4px;">
        <div class="form-group">
          <label>Cantidad: </label>
          <input type="text" class="form-control" id="cantidad" placeholder="100" onkeypress="return justNumbers(event);" >
        </div>
      </div> -->
<!--       <div class="col-md-1" style="left: 10px; bottom: 4px;">
        <div class="form-group">
          <label>Precio $ :</label>
          <input type="text" class="form-control" id="precio" placeholder="1.00" onkeypress="return justNumbers(event);" >
        </div>
      </div> -->
<!--       <div class="col-md-2" style="left: 5px; bottom: 4px;" >
        <div class="form-group">
          <label>Entrega:  </label>
          <select class="form-control js-example-basic-single" id="tipoEntrega" style="width: 100%;">
            <option value="0">Seleccione opción </option>
            <option value="1">Parcial</option>
            <option value="2">Completa</option>
          </select>
        </div>
      </div> -->
<!--       <div class="col-md-2" id="fechaEntrega" style="bottom: 5px; right: 12px; display: none;">
        <div class="form-group">
          <label>Fecha :</label>
         <input type="date" name="fecha" id="fecha" class="form-control">
        </div>
      </div> -->
<!--       <div class="col-md-1" style=" right: 16px;top: 19px;">
        <div class="form-group">
          <button type="button" class="btn btn-primary" id="btnAddProduct" ><span class="glyphicon glyphicon-plus" ></span> </button>
        </div>
      </div> -->
<!--     </form>
  </div> -->
  <div id="tablaProductos" class="col-sm-12">
    <table id="products" class="table table-bordered table-striped"  style="width: 99%;position: initial;left: 35px;bottom: 77px;">
      <thead>
        <tr>
          <th style="width: 10%;">Cantidad</th>
          <th style="width: 25%;">Nombre Catalogo/Descripción</th>
          <th style="width: 10%;">Precio Unitario</th>
          <th style="width: 10%;">Total</th>
          <th style="width: 10%;">Entrega física</th>
          <th style="width: 15%;">N° de entrega</th>
          <th style="width: 15%;">Fecha de Entrega</th>
          <th style="width: 15%;">Cantidad Restante</th> 
          <th></th>
        </tr>
      </thead>
      <tbody id="tableProducts">
      	<?php 
        if ($detalles) {
          foreach ($detalles as $d) {
            $detalle = base64_encode($d->ID_DETALLE);
            echo "<tr>";
            
             if ($d->ID_PRODUCTO == NULL) {
              echo "<td>".$d->DESCRIPCION."</td>";
              echo "<td class='col-xs-1'></td>";
              echo "<td class='col-xs-1'>".$d->PRECIO."</td>";
              echo "<td class='col-xs-1'> <textarea class='form-control' rows='3' placeholder='Enter ...' id='descripción'></textarea></td>";
              echo "<td class='col-xs-1'></td>";
              echo "";
            }else if ($d->DIFERENCIA == 0 ) {
             echo "<td>".$d->CANTIDAD_TOTAL."</td>";
             echo "<td>".$d->CUENTA." ".$d->NOMBRE_PRODUCTO. " ".$d->MEDICAMENTO." /".$d->DESCRIPCION."</td>";
              echo "<td class='col-xs-1'>$".$d->PRECIO."</td>";
              echo "<td class='col-xs-1'>$".number_format($d->MTOTAL,4)."</td>";
              echo "<td class='col-xs-1'> <input class='form-control' type='text' id='cantidad_".$d->ID_DETALLE."' value='".$d->CANTIDAD_PARCIAL."' style='text-align:center;' disabled='' ></td>";
              echo "<td class='col-xs-1'>
               <select class='form-control js-example-basic-single' id='entrega_".$d->ID_DETALLE."' style='width: 100%;' disabled>"; 
                if((int)$d->NUMERO_ENTREGA==1){
                    echo "<option value = '1' selected='true'>Entrega 1</option>
                        <option value = '2'>Entrega 2</option>
                        <option value = '3'>Entrega 3</option>
                        <option value = '4'>Entrega 4</option>
                        <option value = '5'>Entrega 5</option>";
                } else if((int)$d->NUMERO_ENTREGA==2){
                  echo "<option value = '1'>Entrega 1</option>
                        <option value = '2' selected='true'>Entrega 2</option>
                        <option value = '3'>Entrega 3</option>
                        <option value = '4'>Entrega 4</option>
                        <option value = '5'>Entrega 5</option>";

                } else if((int)$d->NUMERO_ENTREGA==3){
                  echo "<option value = '1'>Entrega 1</option>
                        <option value = '2'>Entrega 2</option>
                        <option value = '3' selected='true'>Entrega 3</option>
                        <option value = '4'>Entrega 4</option>
                        <option value = '5'>Entrega 5</option>";

                } else if((int)$d->NUMERO_ENTREGA==4){
                  echo "<option value = '1'>Entrega 1</option>
                        <option value = '2'>Entrega 2</option>
                        <option value = '3'>Entrega 3</option>
                        <option value = '4' selected='true'>Entrega 4</option>
                        <option value = '5'>Entrega 5</option>";

                } else if((int)$d->NUMERO_ENTREGA==5){
                  echo "<option value = '1'>Entrega 1</option>
                        <option value = '2'>Entrega 2</option>
                        <option value = '3'>Entrega 3</option>
                        <option value = '4'>Entrega 4</option>
                        <option value = '5' selected='true'>Entrega 5</option>";

                } else {
                  echo "<option value = '1'>Entrega 1</option>
                        <option value = '2'>Entrega 2</option>
                        <option value = '3'>Entrega 3</option>
                        <option value = '4'>Entrega 4</option>
                        <option value = '5'>Entrega 5</option>";

                }
              echo "</select></td>";
              echo "<td class='col-xs-1'> <input type='date' name='fecha' id='fecha_".$d->ID_DETALLE."' class='form-control' value='".$d->FECHA_INGRESO."'' disabled ></td>";
              echo "<td class='col-xs-1' style='text-align:center;'>".$d->DIFERENCIA."</td>";
              echo "";
            }else if($d->DIFERENCIA != NULL){
              $date=date('Y-m-d',strtotime(date('d-m-Y').'- 15 days'));
              $value=date('Y-m-d');
              //var_dump($date);
              echo "<td>".$d->CANTIDAD_TOTAL."</td>";
              echo "<td>".$d->CUENTA." ".$d->NOMBRE_PRODUCTO." ".$d->MEDICAMENTO." /".$d->DESCRIPCION."</td>";
              echo "<td class='col-xs-1'>$".$d->PRECIO."</td>";
              echo "<td class='col-xs-1'>$".number_format($d->MTOTAL,4)."</td>";
              echo "<td class='col-xs-1'> <input class='form-control' type='text' id='cantidad_".$d->ID_DETALLE."' value='".$d->CANTIDAD_PARCIAL."' style='text-align:center;' onkeypress='return justNumbers(event);' ></td>";
              echo "<td class='col-xs-1'>
               <select class='form-control js-example-basic-single' id='entrega_".$d->ID_DETALLE."' style='width: 100%;'>
                        <option value = '1'>Entrega 1</option>
                        <option value = '2'>Entrega 2</option>
                        <option value = '3'>Entrega 3</option>
                        <option value = '4'>Entrega 4</option>
                        <option value = '5'>Entrega 5</option>
               </select></td>";
              echo "<td class='col-xs-1'> <input type='date' min='".$date."' max='".$value."' value='".$value."' name='fecha' id='fecha_".$d->ID_DETALLE."' class='form-control'></td>";
              echo "<td class='col-xs-1' style='text-align:center;'>".$d->DIFERENCIA."</td>";
              // echo "";
            }
            ?>
            <?php 
                if($d->DIFERENCIA!='0'){

                
             ?>
            <td><span class='pull-right'><a href='#' onclick='modificar("<?php echo $d->ID_PRODUCTO?>","<?php echo $d->ID_DETALLE?>","<?php echo $d->CANTIDAD_TOTAL?>","<?php echo $d->CANTIDAD_PARCIAL?>", "<?php echo $d->PRECIO?>")'><i class='glyphicon glyphicon-pencil'></i></a></span></td>
            <?php 
              } else {


             ?>
            <td><span class='pull-right'><a href='#'  onclick='modificar("<?php echo $d->ID_PRODUCTO?>","<?php echo $d->ID_DETALLE?>","<?php echo $d->CANTIDAD_TOTAL?>","<?php echo $d->CANTIDAD_PARCIAL?>", "<?php echo $d->PRECIO?>")' style="pointer-events: none;
  cursor: default;"><i class='glyphicon glyphicon-pencil'></i></a></span></td>
             <?php } ?>
            <?php 

            echo "</tr>";                                                
          }
        }

        ?>
      </tbody>
    </table> 
  </div>	
</div> 		
<div class="box-footer">
  <button type="button" class="btn btn-default" onclick="regresar()">Regresar</button>
  <button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span>Guardar</button>         
</div>		
</div>

</div>
</div>

<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
  var base_url = "<?php echo base_url();?>";

  $(document).ready(function() {    
    $("#tipoEntrega").change(function() {
      var val = $("select#tipoEntrega").val();
      var f = document.getElementById('fechaEntrega');
        f.style.display = 'block';
      
    });
    var products=[];
    $("#productos").select2();
    $("#entrega").select2();
    $('#productos_editar').DataTable({
      pageLength:5,
      "order" : [],
      language: {
       search:'Buscar:',
       order: 'Mostrar Entradas',
       paginate: {
        first:"Primero",
        previous:"Anterior",
        next:"Siguiente",
        last:"Ultimo"
      },
      emptyTable: "No hay informacion disponible",
      infoEmpty: "Mostrando 0 de 0 de 0 entradas",
      lengthMenu:"Mostrar _MENU_ Entradas",
      info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
    }            
  });

        $('#example1').DataTable(
    {
      pageLength:5,
      "order" : [],
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
          first:"Primero",
          previous:"Anterior",
          next:"Siguiente",
          last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }
    });

      $("#btnAgregarProducto").click(function(e) {

      var formulario = document.getElementById('productosNuevos');
      formulario.style.display = 'block';
      var ordenCompra = new Object();
      ordenCompra.id_orden_compra = $("input#id_orden_compra").val();
      ordenCompra.codigo_orden = $("input#codigo_orden").val();
      ordenCompra.sp = $("input#sp").val();
      ordenCompra.numFactura = $("input#num_factura").val();
      var DatosJson = JSON.stringify(ordenCompra);
      console.log(DatosJson);
      $.post(base_url+"compra/ordencompra/agregarOrden",{
        ordenCompraPost : DatosJson
      }, function(data, textStatus) {
        $("#mensaje").append(data.response_msg);
      }, "json").fail(function (response) {
       $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
       console.log('Error: ' + response.responseText);
     });
    });
        $("#btnGuardar").click(function() {
          var orden =new Object();
          orden.id=$("input#id_orden_compra").val();
          orden.factura=$("input#factura").val();
          console.log(orden);
          var DatosJson=JSON.stringify(orden);
        $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ base_url +"/img/gif-load.gif'> Cargando...</center></div></div>");

          $.post(base_url+'compra/ordencompra/updateOrden',{
            Ordenpost:DatosJson
          },function(data,textStatus){
          $("#mensaje").html(data.response_msg);
          },"json").fail(function(response){
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);
          });
      /*var Orden = new Object();
      Orden.id = '';
      Orden.id_orden_compra = $("input#id_orden_compra").val();
      Orden.products = products;
      console.log(products);
      $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ base_url +"/img/gif-load.gif'> Cargando...</center></div></div>");
      var DatosJson = JSON.stringify(Orden);
      $.post(base_url+"compra/ordencompra/agregarPedido",{
        ordenPost: DatosJson
      }, function (data, textStatus) {
        console.log(data);
        $("#mensaje").html(data.response_msg);
        location.reload();
      }, "json").fail(function (response) {
        $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
        console.log('Error: '+response.responseText);
      });;*/
    });
    $("#btnAddProduct").click(function() {
      var product = new Object();
      var val = $("select#tipoEntrega").val();
      var html = '';
      var p =document.getElementById("productos");
      product.nombre= p.options[p.selectedIndex].text;
      product.id_producto = $("select#productos").val();
      product.precio  = $("input#precio").val();
      product.cantidad = $("input#cantidad").val();
      product.codOrden = $("input#codigo_orden").val();
      products.push(product);
      console.log(products);
      html+='<tr>';
      html+='<td>'+product.id_producto+'</td>';
      html+='<td>'+product.nombre+'</td>';
      html+='<td class="col-xs-1"> <input type="text" class="form-control" style="text-align:right" value="'+product.cantidad+'"></td>';
      html+='<td class="col-xs-1"></td>';
      html+='<td class="col-xs-1"> <input type="text" class="form-control" style="text-align:right" value="'+'$'+product.precio+'"></td>';
      html+='<td>'+'$'+product.cantidad*product.precio+'<td>';
      html+='<td><button type="button"  title="Eliminar Estudio" onclick="eliminarfila();" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>';
      html+='</tr>';
      console.log(html);
      $("#tableProducts").append(html);
      $("input#precio").val("");
      $("input#cantidad").val("");
    });

  });

function modificar(id_producto, iddetalle, cantTotal, diferencia, precio) {

  // var cantidad_total = cantTotal;
  var product = new Object();
  var cant = $("#cantidad_"+iddetalle).val();
  var parc = parseInt(cant);
  var total = parseInt(diferencia);
  if (parc > cantTotal) {
     $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El ingreso no puede ser mayor al de la orden de compra</div>");
  } else if(parc===0 || parc==='0'){
     $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El ingreso no puede ser cero</div>");
  }else if(parc <= cantTotal){
  //product.precio   = $("#precio_venta_"+id_producto).val();
  product.entrega = $("select#entrega_"+iddetalle).val();
  product.fecha = $("input#fecha_"+iddetalle).val();
  product.cantidad = $("#cantidad_"+iddetalle).val();
  product.id= iddetalle;
  product.id_producto = id_producto;
  product.id_orden_compra = $("input#id_orden_compra").val();
  product.precio = precio;
  //Orden.codOrden = $("input#codigo_orden").val();
  var DatosJson=JSON.stringify(product);
  console.log(DatosJson);
   if (product.fecha!=='') {
    $.post(base_url+'compra/ordencompra_bodegas/agregarEntrega',{
      ordenPost:DatosJson
    },function(data,textStatus){
      console.log(data);
      $("#mensaje").append(data.response_msg);
       location.reload(); 
    },"json").fail(function(response){
      $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
      console.log('Error: ' + response.responseText);
    });;    
   } else {
     $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Fecha de recibido obligatoria</div>");    
   }

}else if (id_producto == '' && cantTotal=='' && diferencia == '') {
  
}
}

function salir()
{
  alertify.confirm("¿Esta seguro de guardar el registro?","Click en ACEPTAR para guardar",function(){
    window.location.href = base_url+"compra/detalle_ordencompra"

  },function(){

  }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

}
function regresar()
{
  alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    window.location.href = base_url+"compra/ordencompra_bodegas"

  },function(){

  }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
} 

function eliminarfila(products)
{
  document.getElementById("tableProducts").deleteRow(0);
} 
function eliminarDato(nombre, iddetalle, id_producto) {
  alertify.confirm("Eliminar Estudio","¿Seguro de eliminar el estudio "+nombre+"?",function(){
   document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ base_url +"/img/gif-load.gif'> Eliminando...</center></div></div>";
   var Producto = new Object();
   Producto.id = iddetalle;
   var DatosJson = JSON.stringify(Producto);
   $.post(base_url+'compra/ordencompra/eliminar',{
    detalleProducto:DatosJson
  }, function (data, textStatus) {
    $("#mensaje").html(data.response_msg);
    document.getElementById("tableProducts").deleteRow(0);
  }, "json").fail(function(response) {
   $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
   console.log('Error: ' + response.responseText);
 });;
},function() {

});;
}

function justNumbers(e)
  {
      var keynum = window.event ? window.event.keyCode : e.which;
      if ((keynum == 8) || (keynum == 46))
      return true;
       
      return /\d/.test(String.fromCharCode(keynum));
  }
</script>


