<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('America/El_Salvador');
?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"archivos/documentos"
        } );
    }

    function eliminarDocumento(id,nombre)
    {
    	alertify.confirm("Eliminar El documento","¿Seguro de eliminar el documento "+nombre+"?",function()
    	{
             document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
			var documento=new Object();
			documento.id=id;
			var DatosJson=JSON.stringify(documento);
			$.post(baseurl+'archivos/documentos/eliminar',{
				DocumentoPost:DatosJson
			},function(data,textStatus){
				$("#mensaje").html(data.response_msg);
			},"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
			});    		
    	},function(){

    	});
    }
</script>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="content-title">Inventario Documental</h3>				
			</div>
					<div class="pad margin no-print" style="width: 75%;position: relative;bottom: 40px;left: 11%;">
						<div class="callout callout-info" style="margin-bottom: 0!important;">
							<h4><i class="fa fa-info"></i> Cod&iacute;go de Colores:</h4>

							<table class="table tale-borderless" style="width:100% !important;">
								
								<tbody>
									<tr>
										<td>Documentos con fecha de vigencia documental ingresada en el sistema vencida</td>
										<td style="background-color:#FF5733 ;"></td>
									</tr>
									<tr>
										<td>Documentos con 90 d&iacute;as/3 meses o menos para vencer la vigencia documental ingresada en el sistema</td>
										<td style="background-color: #C2CE2C;"></td>
									</tr>
									<tr>
										<td>Documentos de los cuales se ha prestado parcialmente o totalmente le contenido de la unidad de almacenaje</td>
										<td style="background-color: #2da4d2;"></td>
									</tr>								
									<tr>
										<td>Documentos almacenados en el sistema sin vencer y/o prestados</td>
										<td style="background-color: #FFFFFF;"></td>
									</tr>								
									
								</tbody>
							</table>
						</div>
					</div>			
                	<div class="row">
                		<div class="col-sm-2 col-sm-offset-8">
                            <a href="documentos/excel_create"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-file"></span> Imprimir</button></a>                			
                		</div>                		
                		<div class="col-sm-2 pull-right">
                            <a href="documentos/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                		</div>
                	</div>			
			<div class="box-body">
				<div class="table-responsive col-sm-12">
				<table id="documentos" class="table table-bordered table-striped">
					<thead>
						<th>Caja No</th>						
						<th>Gerencia</th>
						<th>Departamento</th>
						<th>Codigo</th>
						<th>Correlativo Central</th>
						<th>Fecha Inicio Documentaci&oacute;n</th>
						<th>Fecha Fin Documentaci&oacute;n</th>
						<th>Fecha fin de Vigencia</th>
						<th>Pasillo</th>
						<th>Estante</th>
						<th>Anaquel</th>
						<th>Numero de Folios</th>
						<th>Soporte</th>
						<th>Frecuencia de consulta</th>
						<th>Contenido</th>
						<th>Estado</th>
						<?php if($this->ion_auth->get_perfil()==="8" or $this->ion_auth->is_admin()){
							echo "<th></th>";
						} ?>
					</thead>
					<tbody>
	
						<?php 
							if ($documentos) {
								foreach ($documentos as $d) {
									$date1=date('Y-m-d');
									$documentoId=base64_encode($d->ID_DOCUMENTO);
									$diff = date_diff(new DateTime($date1),new DateTime($d->FECHA_SALIDA));//abs(strtotime($date2) - strtotime($date1));
									$days= $diff->format("%R%a days");
									if (date('Y-m-d',strtotime(date('Y-m-d')))>date('Y-m-d',strtotime($d->FECHA_SALIDA)) && $d->tipo!='H' && $d->tipo!='P' && $days!=90) {
									echo "<tr style='background-color:#FF5733;'>";
									echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";
									echo "<td>".$d->GERENCIA."</td>";
									echo "<td>".$d->DEPARTAMENTO."</td>";
									echo "<td>".$d->CODIGO_DOCUMENTO."</td>";
									echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";
									echo "<td>".date("d-m-Y",strtotime($d->FECHA_INICIO))."</td>";
									echo "<td>".date("d-m-Y",strtotime($d->FECHA_FINAL))."</td>";
									echo "<td>".date('d-m-Y',strtotime($d->FECHA_SALIDA))."</td>";
									echo "<td>".$d->PASILLO."</td>";
									echo "<td>".$d->ESTANTE."</td>";
									echo "<td>".$d->ANAQUEL."</td>";
									echo "<td>".$d->NUMERO_FOLIOS."</td>";
									echo "<td>".$d->SOPORTE."</td>";
									echo "<td>".$d->FRECUENCIA_CONSULTA."</td>";
									echo "<td>".$d->CONTENIDO."</td>";
									echo "<td>Almacenado</td>";
									if ($this->ion_auth->get_perfil()==="8" or $this->ion_auth->is_admin()) {
											echo "<td><a href='documentos/editar/".$documentoId."' ><button type='button' title='Editar Documento' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";															
						 ?>
                							 <button type="button"  title="Eliminar Alergia" onclick="eliminarDocumento('<?php echo $documentoId?>','<?php echo $d->SERIE ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
						 <?php 
									}

									echo "</td>";
									echo "</tr>";						 
									}else if($days<=90 && $days<>0){
									echo "<tr style='background-color:#C2CE2C;'>";
									echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";
									echo "<td>".$d->GERENCIA."</td>";
									echo "<td>".$d->DEPARTAMENTO."</td>";
									echo "<td>".$d->CODIGO_DOCUMENTO."</td>";
									echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";
									echo "<td>".date("d-m-Y",strtotime($d->FECHA_INICIO))."</td>";
									echo "<td>".date("d-m-Y",strtotime($d->FECHA_FINAL))."</td>";
									echo "<td>".date('d-m-Y',strtotime($d->FECHA_SALIDA))."</td>";
									echo "<td>".$d->PASILLO."</td>";
									echo "<td>".$d->ESTANTE."</td>";
									echo "<td>".$d->ANAQUEL."</td>";
									echo "<td>".$d->NUMERO_FOLIOS."</td>";
									echo "<td>".$d->SOPORTE."</td>";
									echo "<td>".$d->FRECUENCIA_CONSULTA."</td>";
									echo "<td>".$d->CONTENIDO."</td>";
									echo "<td>Almacenado</td>";
									if($this->ion_auth->get_perfil()==="8" or $this->ion_auth->is_admin()){
										echo "<td><a href='documentos/editar/".$documentoId."' ><button type='button' title='Editar Documento' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
																			
						?>
                							 <button type="button"  title="Eliminar Alergia" onclick="eliminarDocumento('<?php echo $documentoId?>','<?php echo $d->SERIE ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>						
						<?php
									}
								echo "</td>";
								echo "</tr>";
									} else if($d->ESTADO==1 && $d->tipo=='P' or $d->tipo=='H' ) {
									echo "<tr >";
									echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";
									echo "<td>".$d->GERENCIA."</td>";
									echo "<td>".$d->DEPARTAMENTO."</td>";
									echo "<td>".$d->CODIGO_DOCUMENTO."</td>";
									echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";
									echo "<td>".date("d-m-Y",strtotime($d->FECHA_INICIO))."</td>";
									echo "<td>".date("d-m-Y",strtotime($d->FECHA_FINAL))."</td>";
									echo "<td>".date('d-m-Y',strtotime($d->FECHA_SALIDA))."</td>";
									echo "<td>".$d->PASILLO."</td>";
									echo "<td>".$d->ESTANTE."</td>";
									echo "<td>".$d->ANAQUEL."</td>";
									echo "<td>".$d->NUMERO_FOLIOS."</td>";
									echo "<td>".$d->SOPORTE."</td>";
									echo "<td>".$d->FRECUENCIA_CONSULTA."</td>";
									echo "<td>".$d->CONTENIDO."</td>";	
									echo "<td>Permanente/Historico</td>";
									echo "<td><a href='documentos/editar/".$documentoId."' ><button type='button' title='Editar Documento' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
						  ?>
                				 <button type="button"  title="Eliminar Alergia" onclick="eliminarDocumento('<?php echo $documentoId?>','<?php echo $d->SERIE ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
						  <?php 
									echo "</td>";
									echo "</tr>";								  
									} else if($d->ESTADO==2){
									echo "<tr style='background-color:#2da4d2;'>";
									echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";
									echo "<td>".$d->GERENCIA."</td>";
									echo "<td>".$d->DEPARTAMENTO."</td>";
									echo "<td>".$d->CODIGO_DOCUMENTO."</td>";
									echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";
									echo "<td>".date("d-m-Y",strtotime($d->FECHA_INICIO))."</td>";
									echo "<td>".date("d-m-Y",strtotime($d->FECHA_FINAL))."</td>";
									echo "<td>".date('d-m-Y',strtotime($d->FECHA_SALIDA))."</td>";			
									echo "<td>".$d->PASILLO."</td>";
									echo "<td>".$d->ESTANTE."</td>";
									echo "<td>".$d->ANAQUEL."</td>";
									echo "<td>".$d->NUMERO_FOLIOS."</td>";
									echo "<td>".$d->SOPORTE."</td>";
									echo "<td>".$d->FRECUENCIA_CONSULTA."</td>";
									echo "<td>".$d->CONTENIDO."</td>";	
									echo "<td>Prestado</td>";
									if ($this->ion_auth->get_perfil()==="8" or $this->ion_auth->is_admin()) {
									echo "<td><a href='documentos/editar/".$documentoId."' ><button type='button' title='Editar Documento' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
									
										 	
							?>
		                			<button type="button"  title="Eliminar Alergia" onclick="eliminarDocumento('<?php echo $documentoId?>','<?php echo $d->SERIE ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>					
							<?php
									}
									echo "</td>";
									echo "</tr>";
									} else {
									echo "<tr >";
									echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";
									echo "<td>".$d->GERENCIA."</td>";
									echo "<td>".$d->DEPARTAMENTO."</td>";
									echo "<td>".$d->CODIGO_DOCUMENTO."</td>";
									echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";
									echo "<td>".date("d-m-Y",strtotime($d->FECHA_INICIO))."</td>";
									echo "<td>".date("d-m-Y",strtotime($d->FECHA_FINAL))."</td>";
									echo "<td>".date('d-m-Y',strtotime($d->FECHA_SALIDA))."</td>";				
									echo "<td>".$d->PASILLO."</td>";
									echo "<td>".$d->ESTANTE."</td>";
									echo "<td>".$d->ANAQUEL."</td>";
									echo "<td>".$d->NUMERO_FOLIOS."</td>";
									echo "<td>".$d->SOPORTE."</td>";
									echo "<td>".$d->FRECUENCIA_CONSULTA."</td>";
									echo "<td>".$d->CONTENIDO."</td>";								
									echo "<td>Almacenado</td>";
									if($this->ion_auth->get_perfil()==="8" or $this->ion_auth->is_admin()){
										echo "<td><a href='documentos/editar/".$documentoId."' ><button type='button' title='Editar Documento' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";

																 	
						   ?>
                						<button type="button"  title="Eliminar Alergia" onclick="eliminarDocumento('<?php echo $documentoId?>','<?php echo $d->SERIE ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
						   <?php 
						   			}
									echo "</td>";
									echo "</tr>";								   
									}						 

								}
							}

						    ?>
					</tbody>
				</table>					
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
	$.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
	      var myNum = parseInt($('#mySearch').val());
	      if (isNaN(myNum))  {
	        return true;
	      }
	      
	      var amountmax = parseFloat(data[3]) || 0; // use data for the age column
	      var amountmin = parseFloat(data[2]) || 0; // use data for the age column
	      if ((myNum >= amountmin) && (myNum <= amountmax)) {
	          return true;
	      }
	      return false;
	  });		
	var documentos=	$("#documentos").DataTable({ 
					paging      : true,
					pageLength  : 5,
					lengthChange: true,
					searching   : true,
					ordering    : true,
					"order" : [],
					info        : true,
					autoWidth   : false,
					language: {
						search:'Buscar:',
						order: 'Mostrar Entradas',
						paginate: {
							first:"Primero",
							previous:"Anterior",
							next:"Siguiente",
							last:"Ultimo"
						},
						emptyTable: "No hay informacion disponible",
						infoEmpty: "Mostrando 0 de 0 de 0 entradas",
						lengthMenu:"Mostrar _MENU_ Entradas",
						info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
					}    
				});
	});
</script>
<div class="modal fade text-center" id="modal_nuevo" >
  <div class="modal-dialog" >
    <div class="modal-content" style="width: 900px; right:60px;">
    </div>
  </div>
</div>