<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('America/El_Salvador');
?>
<?php 
	$codigo= array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',@$documento[0]->CODIGO_DOCUMENTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'DOC-01'

	);

	$nombre= array(
		'name' 		=> 'nombre',
		'id'		=> 'nombre',
		'value'		=> set_value('nombre',@$documento[0]->DOCUMENTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Documento 1'

	);

	$numero_orden= array(
		'name' 		=> 'numero_orden',
		'id'		=> 'numero_orden',
		'value'		=> set_value('numero_orden',@$documento[0]->NUMERO_ORDEN),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0
	);

	$asuntos= array(
		'name' 		=> 'asuntos',
		'id'		=> 'asuntos',
		'value'		=> set_value('asuntos',@$documento[0]->ASUNTOS),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'form-control'
	);

	$fecha_entrada= array(
		'name' 		=> 'fecha_entrada',
		'id'		=> 'fecha_entrada',
		'value'		=> set_value('fecha_entrada',date('Y-m-d',strtotime(@$documento[0]->FECHA_ENTRADA))),
		'type'		=> 'date',
		'class'		=> 'form-control'
	);


	$vigencia_gestion= array(
		'name' 		=> 'vigencia_gestion',
		'id'		=> 'vigencia_gestion',
		'value'		=> set_value('codigo',@$documento[0]->VIGENCIA_GESTION),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0
	);

	$vigencia_central= array(
		'name' 		=> 'vigencia_central',
		'id'		=> 'vigencia_central',
		'value'		=> set_value('codigo',@$documento[0]->VIGENCIA_CENTRAL),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0
	);

	$vigencia_documental= array(
		'name' 		=> 'vigencia_documental',
		'id'		=> 'vigencia_documental',
		'value'		=> set_value('codigo',@$documento[0]->VIGENCIA_DOCUMENTAL),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'disabled'=>true,
		'min'	=>0
	);		

	$contenido=array(
		'name' 		=> 'contenido',
		'id'		=> 'contenido',
		'value'		=> set_value('codigo',@$documento[0]->CONTENIDO),
		'type'		=> 'textarea',
		'rows'		=>7,
		'class'		=> 'form-control'
	);

	$procedimiento=array(
		'name' 		=> 'procedimiento',
		'id'		=> 'procedimiento',
		'value'		=> set_value('procedimiento',@$documento[0]->PROCEDIMIENTO),
		'type'		=> 'textarea',
		'rows'		=>7,
		'class'		=> 'form-control'
	);

	$fecha_salida= array(
		'name' 		=> 'fecha_salida',
		'id'		=> 'fecha_salida',
		'value'		=> set_value('fecha_salida',date('Y-m-d',strtotime(@$documento[0]->FECHA_SALIDA))),
		'type'		=> 'date',
		'class'		=> 'form-control'
	);

	$no_cajas= array(
		'name' 		=> 'no_cajas',
		'id'		=> 'no_cajas',
		'value'		=> set_value('no_cajas',@$documento[0]->NO_CAJAS),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0

	);

	$no_carpetas= array(
		'name' 		=> 'no_carpetas',
		'id'		=> 'no_carpetas',
		'value'		=> set_value('no_carpetas',@$documento[0]->NO_CARPETAS),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);

	$no_tomo= array(
		'name' 		=> 'no_tomo',
		'id'		=> 'no_tomo',
		'value'		=> set_value('no_tomo',@$documento[0]->NO_TOMO),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);

	$otros= array(
		'name' 		=> 'otros',
		'id'		=> 'otros',
		'value'		=> set_value('otros',@$documento[0]->OTROS),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);

	$no_folios= array(
		'name' 		=> 'no_folios',
		'id'		=> 'no_folios',
		'value'		=> set_value('no_folios',@$documento[0]->NO_FOLIOS),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0

	);

	$soporte= array(
		''=>'Seleccionar',
		'Papel'=>'Papel',
		'Digital'=>'Digital',
		'Sonoro'=>'Sonoro',
		'Audiovisual'=>'Audiovisual'
	);	

	$notas= array(
		'name' 		=> 'notas',
		'id'		=> 'notas',
		'value'		=> set_value('notas',@$documento[0]->NOTAS),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);		

	$frecuencia_consulta=array(
		'name' 		=> 'frecuencia_consulta',
		'id'		=> 'frecuencia_consulta',
		'value'		=> set_value('codigo',@$documento[0]->FRECUENCIA_CONSULTA),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);		

	$fecha_documento_inicio=array(
		'name' 		=> 'fecha_documento_inicio',
		'id'		=> 'fecha_documento_inicio',
		'value'		=> set_value('codigo',date('Y-m-d',strtotime(@$documento[0]->FECHA_DOCUMENTACION_INICIO))),
		'type'		=> 'date',
		'class'		=> 'form-control'
	);

	$fecha_documento_final=array(
		'name' 		=> 'fecha_documento_final',
		'id'		=> 'fecha_documento_final',
		'value'		=> set_value('codigo',date('Y-m-d',strtotime(@$documento[0]->FECHA_DOCUMENTACION_FINAL))),
		'type'		=> 'date',
		'class'		=> 'form-control'
	);

	$caja_correlativo_gestion=array(
		'name' 		=> 'caja_correlativo_gestion',
		'id'		=> 'caja_correlativo_gestion',
		'value'		=> set_value('codigo_gestion',@$documento[0]->CORRELATIVO_GESTION),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0,
	);

	$caja_correlativo_central=array(
		'name' 		=> 'caja_correlativo_central',
		'id'		=> 'caja_correlativo_central',
		'value'		=> set_value('codigo_central',@$documento[0]->CORRELATIVO_CENTRAL),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0,
		'disabled'=>true
	);	
	$tiempos = array(
	  ''=>'Seleccionar',
	  '5' => '5 a&ntilde;os',
	  '10' => '10 a&ntilde;os',
	  '15' => '15 a&ntilde;os',
	  '20' => '20 a&ntilde;os',
	  'P' => 'Permanente',
	  'H'=>'Historico'
	);	

	$almacenado= array(
		'' =>'Seleccionar',
		'Original' =>'Original',
		'Copia'=> 'Copia'
	);

	$valor_primario= array(
		'' =>'Seleccionar',
		'Administrativo' =>'Administrativo',
		'Jurídico' =>'Jurídico',
		'Legal' =>'Legal',
		'Fiscal' =>'Fiscal',
		'Contabilidad'=> 'Contabilidad'
	);

	$valor_secundario= array(
		'' =>'Seleccionar',
		'De evidencia' =>'De evidencia',
		'Histórico' => 'Histórico',
		'Informativo' => 'Informativo'
	);

	$dispocicion_final= array(
		'' =>'Seleccionar',
		'Conservacion Total'=>'Conservacion Total',
		'Eliminacion' =>'Eliminacion',
		'Imagen' =>'Imagen',
		'Seleccion' => 'Seleccion'
	);

	$frecuencia=array(
		''=>'Seleccionar',
		'Quincenal'=>'Quincenal',
		'Mensual'=>'Mensual',
		'Trimestral'=>'Trimestral',
		'Semestral'=>'Semestral',
		'Anual'=>'Anual',
		'Segun Requerimiento'=>'Segun Requerimiento'
	);

 ?>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/froala_editor.min.css')?>">

  <input type="hidden" name="id" id="id" value="<?php echo @$documento[0]->ID_DOCUMENTO?>">
<input type="hidden" name="codigo" id="codigo" value="<?php echo @$documento[0]->CODIGO_DOCUMENTO?>">
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="content-title">Datos</h3>				
			</div>
			<div class="box-body">
			<form class="form-horizontal" name="formulario" id="formulario" role="form">
				<div class="form-group">
					<label class="form-label col-sm-2" for="serie_id">Serie:<span style="color: #F20A06; font-size: 15px;">*</span></label>
					<div class="col-sm-4">
						<select class="form-control" id="serie_id">
							<option value="">Seleccione</option>
							<?php 
								if ($series) {
									foreach ($series as $s) {
										if ($s->ID_SERIE_DOCUMENTAL==@$documento[0]->SERIE) {
										echo "<option value='".$s->ID_SERIE_DOCUMENTAL."' selected>".$s->NOMBRE."</option>";
										} else {
										echo "<option value='".$s->ID_SERIE_DOCUMENTAL."'>".$s->NOMBRE."</option>";											
										}

									}
								}
							 ?>
						</select>
					</div>
					<label class="form-label col-sm-2" for="subserie_id">Sub serie:</label>
					<div class="col-sm-4">
						<?php 
							if (@$documento[0]->SUBSERIE!=NULL) {
							?>
						<select class="form-control" id="subserie_id" >
							<option value="">Seleccione</option>
							<?php 
								if ($subseries) {
									foreach ($subseries as $ss) {
										if ($ss->ID_SUB_SERIE_DOCUMENTAL==@$documento[0]->SUBSERIE) {
											echo "<option value='".$ss->ID_SUB_SERIE_DOCUMENTAL."' selected> ".$ss->DESCRIPCION."</option>";
										} else {
											echo "<option value='".$ss->ID_SUB_SERIE_DOCUMENTAL."' > ".$ss->DESCRIPCION."</option>";
										}
									}
								}
							 ?>
						</select>	
							<?php	
							} else {

						 ?>
						<select class="form-control" id="subserie_id" disabled="true">
							<option value="">Seleccione</option>

						</select>
						<?php } ?>
					</div>
				</div>
				<div class="form-group">

					<label class="form-label col-sm-2" for="unidades">Gerencia <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<select class="form-control" id="unidad_id">
							<option value="">Seleccione</option>
							<?php 
								if ($unidades) {
									foreach ($unidades as $u) {
										if ($u->ID_UNIDAD==@$documento[0]->ID_GERENCIA) {
											echo "<option value='".$u->ID_UNIDAD."'selected >".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";

										} else {
											echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";

										}
									}
								}
							 ?>
						</select>
					</div>					
					<label class="form-label col-sm-2" for="departamentos">Departamentos:<span style="color: #F20A06; font-size: 15px;">*</span></label>
					<div class="col-sm-4">
						<select class="form-control" id="departamento_id" >
							<option value="">Seleccione</option>
							<?php 
								if ($unidades) {
									foreach ($unidades as $u) {
										if ($u->ID_UNIDAD==@$documento[0]->ID_UNIDAD) {
											echo "<option value='".$u->ID_UNIDAD."' selected>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";

										} else {
											echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";

										}
									}
								}
							 ?>							
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="fecha_documento_inicio">Fecha Inicio documentaci&oacute;n:</label>
					<div class="col-sm-4">
						<?php echo form_input($fecha_documento_inicio); ?>
					</div>
					<label class="form-label col-sm-2" for="fecha_documento_final">Fecha Final documentaci&oacute;n:</label>
					<div class="col-sm-4">
						<?php echo form_input($fecha_documento_final); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="fecha_entrada">Fecha Entrada <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<?php echo form_input($fecha_entrada); ?>
					</div>					
					<label class="form-label col-sm-2" for="vigencia_anios">Tiempo vigencia</label>
					<div class="col-sm-4">
						<?php echo form_dropdown('tiempo',$tiempos,@$documento[0]->TIPO,'class="form-control" id="tiempo"'); ?>
					</div>					
				</div>
				<div class="form-group">

					<label class="form-label col-sm-2" for="fecha_salida">Fecha fin de vigencia:</label>
					<div class="col-sm-4">
						<?php echo form_input($fecha_salida) ?>
					</div>									
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="caja_correlativo_gestion">Correlativo archivo de Gesti&oacute;n</label>
					<div class="col-sm-4">
						<?php echo form_input($caja_correlativo_gestion) ?>
					</div>
					<label class="form-label col-sm-2" for="caja_correlativo_central">Correlativo archivo  Central</label>
					<div class="col-sm-4">
						<?php echo form_input($caja_correlativo_central); ?>
					</div>					
				</div>

				<div class="form-group">
					<label class="form-label col-sm-2" for="vigencia_gestion">Vigencia en archivo de gesti&oacute;n</label>
					<div class="col-sm-4">
						<?php echo form_input($vigencia_gestion) ?>
					</div>
					<label class="form-label col-sm-2" for="vigencia_central">Vigencia en archivo central</label>
					<div class="col-sm-4">
						<?php echo form_input($vigencia_central) ?>
					</div>										
				</div>
				<!--<div class="form-group">
					<label class="form-label col-sm-2" for="vigencia_documental">Vigencia Documental</label>
					<div class="col-sm-4">
						<?php echo form_input($vigencia_documental) ?>
					</div>
				</div>-->

			<!--	<div class="form-group">
					<label class="form-label col-sm-2" for="no_cajas">Total De Cajas ingresadas:</label>
					<div class="col-sm-4">
						<?php echo form_input($no_cajas); ?>
					</div>
					<label class="form-label col-sm-2" for="no_carpetas">Total De Carpetas Ingresadas:</label>
					<div class="col-sm-4">
						<?php echo form_input($no_carpetas) ?>
					</div>
				</div>-->
				<div class="form-group">
					<!--<label class="form-label col-sm-2" for="no_tomo">Numero de Tomos:</label>
					<div class="col-sm-4">
						<?php echo form_input($no_tomo); ?>
					</div>-->
					<label class="form-label col-sm-2" for="no_folios">Numero de folios:</label>
					<div class="col-sm-4">
						<?php echo form_input($no_folios) ?>
					</div>
					<label class="form-label col-sm-2" for="codigo">Soporte:</label>
					<div class="col-sm-4" >
							<?php  echo form_dropdown('soporte',$soporte,@$documento[0]->SOPORTE,'class="form-control" id="soporte"');  ?>
					
					</div>					
				</div>
			<!--	<div class="form-group">
					<label class="form-label col-sm-2" for="nombre">Asuntos:</label>
					<div class="col-sm-4">
						<?php echo form_textarea($asuntos); ?>
					</div>
					<label class="form-label col-sm-2" for="codigo">Otros:</label>
					<div class="col-sm-4">
						<?php echo form_input($otros) ?>
					</div>
				</div>-->
				<!--<div class="form-group">
					<label class="form-label col-sm-2" for="nombre">Notas:</label>
					<div class="col-sm-4">
						<?php echo form_input($notas); ?>
					</div>

				</div>-->
				<div class="form-group">
					<label class="form-label col-sm-2" for="frecuencia">Frecuencia de consulta:</label>
					<div class="col-sm-4">
							<?php  echo form_dropdown('frecuencia_consulta',$frecuencia,@$documento[0]->FRECUENCIA_CONSULTA,'class="form-control" id="frecuencia_consulta"');  ?>

					</div>
					<label class="form-label col-sm-2" for="almacenado">Almacenado</label>	<div class="col-sm-4">
						<?php echo form_dropdown('almacenado',$almacenado,@$documento[0]->ALMACENADO,'class="form-control" id="almacenado"'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="valor_primario">Valor primario: </label>
					<div class="col-sm-4">
						<?php echo form_dropdown('valor_primario',$valor_primario,@$documento[0]->VALOR_PRIMARIO,'class="form-control" id="valor_primario"') ?>
					</div>
					<label class="form-label col-sm-2" for="valor_secundario">Valor secundario: </label>
					<div class="col-sm-4">
						<?php echo form_dropdown('valor_secundario',$valor_secundario,@$documento[0]->VALOR_SECUNDARIO,'class="form-control" id="valor_secundario"') ?>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="dispocicion_final">Dispoci&oacute;n final</label>
					<div class="col-sm-4">
						<?php echo form_dropdown('dispocicion_final',$dispocicion_final,@$documento[0]->DISPOSICION_FINAL,'class="form-control" id="disposicion_final"') ?>
					</div>
					<label class="form-label col-sm-2">Agregar unidades a compartir</label>
					<div class="col-sm-4">
						<select multiple="multiple" id="unidad_comparte" class="form-control" >
							<option value="">Seleccione</option>
							<?php 
								if ($unidades) {
								
									foreach ($unidades as $u) {
									echo "<option value='".$u->ID_UNIDAD."' >".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
	
									}
											
								}
							 ?>
						</select>
					</div>

				</div>			
				<div class="form-group">
					<label class="col-sm-2 form-label">Unidades que comparten</label>
					<div class="col-sm-8">
						<select class="form-control" multiple="multiple" disabled="true" style="width: 80%!important;">
							<?php 
								if ($unidad_comparte) {
									foreach ($unidad_comparte as $uc) {
										echo "<option value=''>".$uc->CODIGO_UNIDAD." ".$uc->DESCRIPCION."</option>";
									}
								}
							 ?>
						</select>
					</div>
				</div>	
				<div class="form-group">
					<label class="form-label col-sm-2" for="pasillo">Pasillo <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<select class="form-control" id="pasillo_id" style="width: 90%!important;">
							<option value="">Seleccione</option>
							<?php 
								if ($pasillos) {
									foreach ($pasillos as $p) {
										if ($p->ID_CAT_PASILLO==@$documento[0]->ID_CAT_PASILLO) {
											echo "<option value='".$p->ID_CAT_PASILLO."' selected>".$p->CODIGO." ".$p->DESCRIPCION."</option>";
										} else {
											echo "<option value='".$p->ID_CAT_PASILLO."'>".$p->CODIGO." ".$p->DESCRIPCION."</option>";
										}
										

									}
								}
							 ?>
						</select>
					</div>									
					<label class="form-label col-sm-2" for="estantes">Estantes <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<select class="form-control" id="estante_id" style="width: 90%!important;">
							<option value="">Seleccione</option>
							<?php 
								if ($estantes) {
									foreach ($estantes as $e) {
										if ($e->ID_ESTANTE==@$documento[0]->ID_ESTANTE) {
										echo "<option value='".$e->ID_ESTANTE."' selected>".$e->CODIGO_ESTANTE." ".$e->DESCRIPCION."</option>";
										} else {
										echo "<option value='".$e->ID_ESTANTE."' >".$e->CODIGO_ESTANTE." ".$e->DESCRIPCION."</option>";											
										}
									}
								}
							 ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="anaqueles">Anaqueles <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<select class="form-control" id="anaquel_id" style="width: 90%!important;">
							<?php 
								if ($anaqueles) {
									foreach ($anaqueles as $a) {

										if ($a->ID_ANAQUEL==@$documento[0]->ID_ANAQUEL) {
											echo "<option value='".$a->ID_ANAQUEL."' selected> ".$a->CODIGO_ANAQUEL." ".$a->NUMERO_ANAQUEL."</option>";
										} else {
											echo "<option value='".$a->ID_ANAQUEL."' > ".$a->CODIGO_ANAQUEL." ".$a->NUMERO_ANAQUEL."</option>";											
										}
									}
								}
							 ?>
						</select>
					</div>					
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="contenido">Contenido:</label>
					<div class="col-sm-8">
						<?php echo form_textarea($contenido); ?>
					</div>					
				</div>	
				<div class="form-group">
					<label class="form-label col-sm-2 " for="procedimiento">Procedimiento:</label>
					<div class="col-sm-8">
						<?php echo form_textarea($procedimiento); ?>
					</div>					
				</div>			

			</form>		
			</div>
			<div class="box-footer">
				<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
				<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>					
			</div>
		</div>
	</section>	
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"archivos/documentos"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"archivos/documentos"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
</script>
<script type="text/javascript">
  	$(document).ready(function(){
  		$("#serie_id").select2();
  		$("#subserie_id").select2();
  		$("#pasillo_id").select2();
  		$("#estante_id").select2();
  		$("#anaquel_id").select2();
  		$("#unidad_id").select2();
  		$("#departamento_id").select2();
  		$("#unidad_comparte").select2();
  	});
  </script>
<script type="text/javascript" src="<?php echo base_url('js/JsonDocumento.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>