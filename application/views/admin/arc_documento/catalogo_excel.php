 <html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40" width="816">
            <head>

            </head>
<style type="text/css">

    table, td, th{
        border 1px solid black;
        padding-top: 0.12cm;
    }

    table table  {
        display: block;
        max-width: 816px;
    }


   

</style>
            <body>
                <?php 
                    
                    $filename = "tabla de plazos documentales ".date('d-m-Y').".xls";
                    header("Content-Type: application/vnd.ms-excel"); //mime type
                    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                    header("Cache-Control: max-age=0"); //no cache
                
                 ?>                 
                     <table >
                        <thead>


                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5"></td>
                                <td colspan="5" style=" display: block; margin-left: auto;  margin-right: auto; width: 50% !important; height: 130px !important;"><img src="<?php echo base_url('img/MEMBRETE1-1.jpg')?>" width="816" height="130"  /></td> 

                            </tr>                            
                            <tr>

                                <td colspan="10"   style="height: 60px;font-size: 20px; text-align: center;"><?php echo htmlentities("TABLA DE PLAZOS DE CONSERVACION DOCUMENTAL");?></td>
                            </tr>
                            <tr>
                            <?php 

                                if ($unidades) {
                                    foreach ($unidades as $u) {
                                        echo "<table style='width:700px;'>";
                                        echo "<tr >";
                                        echo "<td colspan='4' style='padding-right:100px; border:1px solid black;'>FONDO:&#09;AMAC/".$u->CODIGO_UNIDAD." ".htmlentities($u->GERENCIA)."</td>";
                                        echo "<td colspan='1'></td>";
                                        echo "<td style='height25px; border:1px solid black;'>ELABORADO POR:&#09;".htmlentities($empleado)." </td>";
                                        echo "</tr>";
                                        echo "<tr>";
                                        echo "<td colspan='4' style='padding-right:100px; border:1px solid black;'>SUBFONDO:&#09;".htmlentities($u->DEPARTAMENTO)."</td>";
                                        echo "<td colspan='1'></td>";  
                                        echo "<td style='height25px; border:1px solid black;'>FECHA DE ELABORACION:&#09; ".date('d-m-Y')."</td>";                         
                                        echo "</tr>";
                                        echo "<tr>";
                                        echo "<td colspan='4' style='padding-right:100px; border:1px solid black;'>FUNCIONES DE LA UNIDAD ADMINISTRATIVA: &#09;</td>";                
                                        echo "</tr>";
                                        echo "</table>";
                                        echo "<tr></tr>";
                                        $documentos=$this->documentos_model->listarDocumentoExcel($u->ID_UNIDAD);
                                        
                                        foreach ($documentos as $d) {

                                            $unidadescomparten=$this->documentos_model->listarUnidadComparten($d->ID_DOCUMENTO);

                                             echo "<table border='1' style='width:700px;'>";
                                            echo "<tr style='background-color:#D1CDCC;'>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>NUMERO</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>SERIE</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>DESCRIPCION</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>ORIGINAL/COPIA</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>OFICINAS QUE COMPARTEN EL DOCUMENTO</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>SOPORTE</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>TOMO</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>FECHA INICIO DOCUMENTACION</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>FECHA FINAL DOCUMENTACION</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>VIGENCIA DE GESTION</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>VIGENCIA CENTRAL</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>INTERMEDIO HISTORICO</td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>DISPOSICION FINAL </td>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>OBSERVACIONES</td>";
                                            echo "</tr>";
                                            echo "<tr>";
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>".$d->CORRELATIVO_CENTRAL."</td>";                                            
                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;'>".$d->SERIE."</td>";
                                            echo "<td style='text-align:justify; font-size:11px; font-family:Arial;'></td>";                 
                                            echo "<td style='text-align:justify; font-size:11px; font-family:Arial;'>".htmlentities($d->ALMACENADO)."</td>";

                                            echo "<td style='text-align:center; font-size:11px; font-family:Arial;;'>";
                                                foreach ($unidadescomparten as $uc) {
                                                    echo $uc->CODIGO_UNIDAD." ".htmlentities($uc->DESCRIPCION)."<br/>";
                                                }
                                            echo "</td>";
                                            echo "<td style='text-align:justify; font-size:11px; font-family:Arial;'>".htmlentities($d->SOPORTE)."</td>";
                                            echo "<td style='text-align:justify; font-size:11px; font-family:Arial;'>".$d->NO_TOMO."</td>";
                                            echo "<td style='text-align:justify; font-size:11px; font-family:Arial;'>".$d->FECHA_INICIO."</td>";
                                            echo "<td style='text-align:justify; font-size:11px; font-family:Arial;'>".$d->FECHA_FINAL."</td>";
                                            echo "<td style='text-align:justify; font-size:11px; font-family:Arial;'>".$d->VIGENCIA_GESTION."</td>";
                                            echo "<td style='text-align:justify; font-size:11px; font-family:Arial;'>".$d->VIGENCIA_CENTRAL."</td>";
                                            echo "<td style='text-align:justify; font-size:11px; font-family:Arial;'></td>";
                                            echo "<td style='text-align:justify; font-size:11px; font-family:Arial;'>".$d->DISPOSICION_FINAL."</td>";
                                            echo "<td>".htmlentities($d->CONTENIDO)."</td>";                                                                            
                                            echo "</tr>";
                                            echo "</table>";                                                                                    
                                        }


                                        echo "<table >";
                                            echo "<tr colspan='2'></tr>";
                                            echo "<tr>";
                                            echo "<td colspan='2' rowspan='3' style='height:25px; font-size:11px; text-align:center;'>FIRMA DE LA JEFATURA DE LA UNIDAD<br/>".$u->GERENCIA."</td>";
                                            echo "<td style='text-align:center;' colspan='6' rowspan='3'>LUGAR Y FECHA</td>";
                                            echo "<td style='text-align:center;' colspan='6' rowspan='3'>SELLO DEL COMITE INSTITUCIONAL DE SELECCION Y ELIMINACION DE DOCUMENTOS O REPRESENTANTES</td>";   
                                            echo "</tr>";
                                        echo "</table>";
                                    }
                                }
                             ?>
                            </tr>
                              
                        </tbody>
                     </table>
            </body>
</html>    
    