<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
?>

<?php 

	$codigo= array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'DOC-01'

	);

	$nombre= array(
		'name' 		=> 'nombre',
		'id'		=> 'nombre',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Documento 1'

	);

	$numero_orden= array(
		'name' 		=> 'numero_orden',
		'id'		=> 'numero_orden',
		'value'		=> set_value('codigo',''),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0
	);

	$asuntos= array(
		'name' 		=> 'asuntos',
		'id'		=> 'asuntos',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>3,
		'class'		=> 'form-control'
	);

	$contenido=array(
		'name' 		=> 'contenido',
		'id'		=> 'contenido',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>7,
		'class'		=> 'form-control'
	);

	$fecha_entrada= array(
		'name' 		=> 'fecha_entrada',
		'id'		=> 'fecha_entrada',
		'value'		=> set_value('codigo',''),
		'type'		=> 'date',
		'class'		=> 'form-control'
	);

	$vigencia_gestion= array(
		'name' 		=> 'vigencia_gestion',
		'id'		=> 'vigencia_gestion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0
	);

	$vigencia_central= array(
		'name' 		=> 'vigencia_central',
		'id'		=> 'vigencia_central',
		'value'		=> set_value('codigo',''),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0
	);

	$vigencia_documental= array(
		'name' 		=> 'vigencia_documental',
		'id'		=> 'vigencia_documental',
		'value'		=> set_value('codigo',''),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'disabled'=>true,
		'min'	=>0
	);		

	$fecha_salida= array(
		'name' 		=> 'fecha_salida',
		'id'		=> 'fecha_salida',
		'value'		=> set_value('codigo',''),
		'type'		=> 'date',
		'class'		=> 'form-control'
	);

	$no_cajas= array(
		'name' 		=> 'no_cajas',
		'id'		=> 'no_cajas',
		'value'		=> set_value('codigo',''),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0

	);

	$no_carpetas= array(
		'name' 		=> 'no_carpetas',
		'id'		=> 'no_carpetas',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);

	$no_tomo= array(
		'name' 		=> 'no_tomo',
		'id'		=> 'no_tomo',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);

	$otros= array(
		'name' 		=> 'otros',
		'id'		=> 'otros',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);

	$no_folios= array(
		'name' 		=> 'no_folios',
		'id'		=> 'no_folios',
		'value'		=> set_value('codigo',''),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'	=>0

	);



	$notas= array(
		'name' 		=> 'notas',
		'id'		=> 'notas',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);		

	$frecuencia_consulta=array(
		'name' 		=> 'frecuencia_consulta',
		'id'		=> 'frecuencia_consulta',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);		

	$fecha_documento_inicio=array(
		'name' 		=> 'fecha_documento_inicio',
		'id'		=> 'fecha_documento_inicio',
		'value'		=> set_value('codigo',''),
		'type'		=> 'date',
		'class'		=> 'form-control'
	);

	$fecha_documento_final=array(
		'name' 		=> 'fecha_documento_final',
		'id'		=> 'fecha_documento_final',
		'value'		=> set_value('codigo',''),
		'type'		=> 'date',
		'class'		=> 'form-control'
	);

	$caja_correlativo_gestion=array(
		'name' 		=> 'caja_correlativo_gestion',
		'id'		=> 'caja_correlativo_gestion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);

	$caja_correlativo_central=array(
		'name' 		=> 'caja_correlativo_central',
		'id'		=> 'caja_correlativo_central',
		'value'		=> set_value('caja_correlativo_central',''),
		'type'		=> 'text',
		'class'		=> 'form-control'
	);

	$almacenado= array(
		'' =>'Seleccionar',
		'Original' =>'Original',
		'Copia'=> 'Copia'
	);

	$valor_primario= array(
		'' =>'Seleccionar',
		'Administrativo' =>'Administrativo',
		'Jurídico' =>'Jurídico',
		'Legal' =>'Legal',
		'Fiscal' =>'Fiscal',
		'Contabilidad'=> 'Contabilidad'
	);

	$valor_secundario= array(
		'' =>'Seleccionar',
		'De evidencia' =>'De evidencia',
		'Histórico' => 'Histórico',
		'Informativo' => 'Informativo'
	);

	$dispocicion_final= array(
		'' =>'Seleccionar',
		'Conservacion Total'=>'Conservacion Total',
		'Eliminacion' =>'Eliminacion',
		'Imagen' =>'Imagen',
		'Seleccion' => 'Seleccion'
	);
 ?>
 
  <input type="hidden" name="id" id="id" value="">
  <div class="modal-content">
 		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" >
				<span >x</span>
			</button>
			<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
		</div>
		<div class="modal-body">
			<form class="form-horizontal" name="formulario" id="formulario" role="form">
				<div class="form-group">
					<label class="form-label col-sm-2" for="serie_id">Serie:<span style="color: #F20A06; font-size: 15px;">*</span></label>
					<div class="col-sm-4">
						<select class="form-control" id="serie_id" style="width: 100%;">
							<option value="">Seleccione</option>
							<?php 
								if ($series) {
									foreach ($series as $s) {
										echo "<option value='".$s->ID_SERIE_DOCUMENTAL."'>".$s->NOMBRE."</option>";
									}
								}
							 ?>
						</select>
					</div>
					<label class="form-label col-sm-2" for="subserie_id">Sub serie:</label>
					<div class="col-sm-4">
						<select class="form-control" id="subserie_id" disabled="true" style="width: 100%;">
							<option value="">Seleccione</option>
						</select>
					</div>
				</div>
				<div class="form-group">

					<label class="form-label col-sm-2" for="unidades">Gerencia <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<select class="form-control" id="unidad_id" style="width: 100%;">
							<option value="">Seleccione</option>
							<?php 
								if ($unidades) {
									foreach ($unidades as $u) {
										echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
									}
								}
							 ?>
						</select>
					</div>					
					<label class="form-label col-sm-2" for="departamentos">Departamentos:<span style="color: #F20A06; font-size: 15px;">*</span></label>
					<div class="col-sm-4">
						<select class="form-control" id="departamento_id" disabled="true" style="width: 100%;">
							<option value="">Seleccione</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="fecha_documento_inicio">Fecha Extrema Inicio:</label>
					<div class="col-sm-4">
						<?php echo form_input($fecha_documento_inicio); ?>
					</div>
					<label class="form-label col-sm-2" for="fecha_documento_final">Fecha Extrema Final:</label>
					<div class="col-sm-4">
						<?php echo form_input($fecha_documento_final); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="fecha_entrada">Fecha Ingreso A.C <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<?php echo form_input($fecha_entrada); ?>
					</div>					
					<label class="form-label col-sm-2" for="vigencia_anios">Tiempo vigencia</label>
					<div class="col-sm-4">
						<select class="form-control" id="tiempo">
							<option value="">Seleccione</option>
							<option value="3">3 a&ntilde;os</option>
							<option value="5">5 a&ntilde;os</option>
							<option value="10">10 a&ntilde;os</option>
							<option value="15">15 a&ntilde;os</option>
							<option value="20">20 a&ntilde;os</option>
							<option value="P">Permanente</option>
							<option value="H">Historico</option>
						</select>
					</div>					
				</div>
				<div class="form-group">

					<label class="form-label col-sm-2" for="fecha_salida">Fecha fin de vigencia:</label>
					<div class="col-sm-4">
						<?php echo form_input($fecha_salida) ?>
					</div>									
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="caja_correlativo_gestion">Correlativo archivo de Gesti&oacute;n</label>
					<div class="col-sm-4">
						<?php echo form_input($caja_correlativo_gestion) ?>
					</div>
					<label class="form-label col-sm-2" for="caja_correlativo_central">Correlativo archivo  Central</label>
					<div class="col-sm-4">
						<?php echo form_input($caja_correlativo_central); ?>
					</div>					
				</div>

				<div class="form-group">
					<label class="form-label col-sm-2" for="vigencia_gestion">Vigencia en archivo de gesti&oacute;n</label>
					<div class="col-sm-4">
						<?php echo form_input($vigencia_gestion) ?>
					</div>
					<label class="form-label col-sm-2" for="vigencia_central">Vigencia en archivo central</label>
					<div class="col-sm-4">
						<?php echo form_input($vigencia_central) ?>
					</div>										
				</div>
				<div class="form-group">
					<!--<label class="form-label col-sm-2" for="vigencia_documental">Vigencia Documental</label>
					<div class="col-sm-4">
						<?php echo form_input($vigencia_documental) ?>
					</div>-->
					<label class="form-label col-sm-2" for="no_folios">Numero de folios:</label>
					<div class="col-sm-4">
						<?php echo form_input($no_folios) ?>
					</div>
					<label class="form-label col-sm-2" for="codigo">Soporte:</label>
					<div class="col-sm-4" >
						<select class="form-control" id="soporte">
							<option value="">Seleccione</option>
							<option value="Papel">Papel</option>
							<option value="Digital">Digital</option>
							<option value="Sonoro">Sonoro</option>
							<option value="Audiovisual">Audiovisual</option>
						</select>
					</div>
				</div>

			<!--	<div class="form-group">
					<label class="form-label col-sm-2" for="no_cajas">Total De Cajas ingresadas:</label>
					<div class="col-sm-4">
						<?php echo form_input($no_cajas); ?>
					</div>
					<label class="form-label col-sm-2" for="no_carpetas">Total De Carpetas Ingresadas:</label>
					<div class="col-sm-4">
						<?php echo form_input($no_carpetas) ?>
					</div>
				</div>-->
				<div class="form-group">
					<!--<label class="form-label col-sm-2" for="no_tomo">Numero de Tomos:</label>
					<div class="col-sm-4">
						<?php echo form_input($no_tomo); ?>
					</div>-->

				</div>
			<!--	<div class="form-group">
					<label class="form-label col-sm-2" for="nombre">Asuntos:</label>
					<div class="col-sm-4">
						<?php echo form_textarea($asuntos); ?>
					</div>
					<label class="form-label col-sm-2" for="codigo">Otros:</label>
					<div class="col-sm-4">
						<?php echo form_input($otros) ?>
					</div>
				</div>-->
				<div class="form-group">
					<!--<label class="form-label col-sm-2" for="nombre">Notas:</label>
					<div class="col-sm-4">
						<?php echo form_input($notas); ?>
					</div>-->

					<label class="form-label col-sm-2" for="frecuencia">Frecuencia de consulta:</label>
					<div class="col-sm-4">
						<select class="form-control" id="frecuencia_consulta"> 
							<option value="">Seleccione</option>
							<option value="Quincenal">Quincenal</option>
							<option value="Mensual">Mensual</option>
							<option value="Trimestral">Trimestral</option>
							<option value="Semestral">Semestral</option>
							<option value="Anual">Anual</option>
							<option value="Segun Requerimiento">Segun Requerimiento</option>
						</select>
					</div>
					<label class="form-label col-sm-2" for="almacenado">Almacenado</label>	<div class="col-sm-4">
						<?php echo form_dropdown('almacenado',$almacenado,'','class="form-control" id="almacenado"'); ?>
					</div>
				</div>
				<!--<div class="form-group">


				</div>-->
				<div class="form-group">
					<label class="form-label col-sm-2" for="valor_primario">Valor primario: </label>
					<div class="col-sm-4">
						<?php echo form_dropdown('valor_primario',$valor_primario,'','class="form-control" id="valor_primario"') ?>
					</div>
					<label class="form-label col-sm-2" for="valor_secundario">Valor secundario: </label>
					<div class="col-sm-4">
						<?php echo form_dropdown('valor_secundario',$valor_secundario,'','class="form-control" id="valor_secundario"') ?>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="dispocicion_final">Dispoci&oacute;n final</label>
					<div class="col-sm-4">
						<?php echo form_dropdown('dispocicion_final',$dispocicion_final,'','class="form-control" id="disposicion_final"') ?>
					</div>
					<label class="form-label col-sm-2">Unidades Comparten</label>
					<div class="col-sm-4">
						<select multiple="multiple" id="unidad_comparte" class="form-control">
							<option value="">Seleccione</option>
							<?php 
								if ($unidades) {
									foreach ($unidades as $u) {
										echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
									}
								}
							 ?>
						</select>
					</div>

				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="pasillo">Pasillo <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<select class="form-control" id="pasillo_id">
							<option value="">Seleccione</option>
							<?php 
								if ($pasillos) {
									foreach ($pasillos as $p) {
										echo "<option value='".$p->ID_CAT_PASILLO."'>".$p->CODIGO." ".$p->DESCRIPCION."</option>";
									}
								}
							 ?>
						</select>
					</div>									
					<label class="form-label col-sm-2" for="estantes">Estantes <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<select class="form-control" id="estante_id" disabled="true">
							
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="anaqueles">Anaqueles <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<select class="form-control" id="anaquel_id" disabled="true">
							
						</select>
					</div>					
				</div>
				<div class="form-group">
					<label class="form-label col-sm-2" for="contenido">Contenido:</label>
					<div class="col-sm-8">
						<?php echo form_textarea($contenido); ?>
					</div>

				</div>				

			</form>
		</div>  	
		<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	 					
		</div>
  </div>
  <script type="text/javascript" src="<?php echo base_url('js/JsonDocumento.js')?>"></script>

  <script type="text/javascript">
  
  	$(document).ready(function(){

  		$("#serie_id").select2();
  		$("#subserie_id").select2();
  		$("#pasillo_id").select2();
  		$("#estante_id").select2();
  		$("#anaquel_id").select2();
  		$("#unidad_id").select2();
  		$("#departamento_id").select2();
  		$("#unidad_comparte").select2();
  		
  	});
  </script>