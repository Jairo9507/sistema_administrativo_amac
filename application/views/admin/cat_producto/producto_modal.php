<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php

	$cod_producto=array(
		'name' 		=> 'cod_producto',
		'id'		=> 'cod_producto',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '',
		'disabled'	=>''		
	);

	$nombre_producto=array(
		'name' 		=> 'nombre_producto',
		'id'		=> 'nombre_producto',
		'value'		=> set_value('nombre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => ''
	);

	$minimo =array(
		'name' 		=> 'minimo_producto',
		'id'		=> 'minimo_producto',
		'value'		=> set_value('codigo',''),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'		=>0,
		//'placeholder' => '',
		'onkeypress'	=> 'return validarNumeros(event)'		
	);


?>

<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>
	</div>
	<div class="modal-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div class="form-group">
				
				<label for="nombre_producto" class="col-sm-2 form-label">Nombre<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($nombre_producto);?>
				</div>				
				<label for="minimo_producto" class="col-sm-2 form-label">Minimo:</label>
				<div class="col-sm-4">
					<?php echo form_input($minimo);?>
				</div>						
			</div>
			<div class="form-group ">
				<label for="tipo_producto" class="col-sm-2 form-label ">Cuenta contable<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select class="form-control" id="tipo_id" style="width: 90%">
						<option value="0">Seleccione Cuenta contable</option>
						<?php
							if($tipos){
								foreach ($tipos as $tipo) {
									echo "<option value='".$tipo->id_cat_cuenta_contable."'>".$tipo->CODIGO." ".$tipo->descripcion."</option>";
								}
							}
						?>
					</select>
				</div>	
				<label for="unidad_id" class="col-sm-2 form-label">Unidad de medida:</label>
				<div class="col-sm-4">
					<select class="form-control" id="unidad_id" style="width: 90%">
						<option value="">Seleccione una unidad</option>
						<?php
							if($medidas){
								foreach ($medidas as $unidad) {
									echo "<option value='".$unidad->ID_CAT_UNIDAD_MEDIDA."'>".$unidad->DESCRIPCION."</option>";
								}
							}
						?>
					</select>
				</div>											
			</div>
			<div class="form-group">								
				<label for="cuenta_id" class="col-sm-2 form-label">Cuenta Presupuestaria<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select class="form-control" id="cuenta_id" style="width: 90%"> 
						<option value="0">Seleccione una cuenta presupuestaria</option>
						<?php
							if($cuentas){
								foreach ($cuentas as $cuenta) {
									echo "<option value='".$cuenta->id_cat_cuenta."'>".$cuenta->cod_cuenta." ".$cuenta->nombre_cuenta."</option>";
								}
							}
						?>
					</select>
				</div>														
			</div>
		</form>
	</div>	
	<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>		
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('js/JsonProducto.js');?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#tipo_id").select2({
             dropdownParent: $("#modal_nuevo")
        });
        $("#unidad_id").select2({
             dropdownParent: $("#modal_nuevo")
        });
        $("#cuenta_id").select2({
             dropdownParent: $("#modal_nuevo")
        });

    });

</script>