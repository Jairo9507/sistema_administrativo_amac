<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
	$cod_producto=array(
		'name' 		=> 'cod_producto',
		'id'		=> 'cod_producto',
		'value'		=> set_value('codigo',@$producto[0]->COD_PRODUCTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '',
		'disabled'	=>'true'
	);
	$nombre_producto=array(
		'name' 		=> 'nombre_producto',
		'id'		=> 'nombre_producto',
		'value'		=> set_value('nombre',@$producto[0]->NOMBRE_PRODUCTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => ''	
	);
	$minimo =array(
		'name' 		=> 'minimo_producto',
		'id'		=> 'minimo_producto',
		'value'		=> set_value('minimo',@$producto[0]->MINIMO),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'min'=>		0,
		//'placeholder' => '',
		'onkeypress'	=> 'return validarn(event)'		
	);
	$maximo=array(
		'name' 		=> 'maximo_producto',
		'id'		=> 'maximo_producto',
		'value'		=> set_value('maximo',@$producto[0]->MAXIMO),
		'type'		=> 'number',
		'class'		=> 'form-control'
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarn(event)'		
	);
?>
<input type="hidden" name="id" id="id" value="<?php echo @$producto[0]->ID_PRODUCTO?>">
<div class="content-wrapper">
	<div class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
	</div>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Datos</h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" name="formulario" id="formulario" role="form">
					<div class="form-group">
						<label for="cod_producto" class="col-sm-2 form-label">Codigo:</label>
						<div class="col-sm-4">
							<?php echo form_input($cod_producto);?>
						</div>
						<label for="nombre_producto" class="col-sm-2 form-label">Nombre<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
						<div class="col-sm-4">
							<?php echo form_input($nombre_producto);?>
						</div>				
					</div>
					<div class="form-group">
						<label for="minimo_producto" class="col-sm-2 form-label">Minimo:</label>
						<div class="col-sm-4">
							<?php echo form_input($minimo);?>
						</div>				
					</div>
					<div class="form-group">
						
							<label for="tipo_producto" class="col-sm-2 form-label">Cuenta contable<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
							<div class="col-sm-4">
							<select class="form-control" id="tipo_id" style="width: 80%;">
							<option value="">Seleccione una cuenta contable</option>
							<?php
							if($tipos){
							foreach ($tipos as $tipo) {
								if($tipo->id_cat_cuenta_contable==@$producto[0]->ID_TIPO){
									echo "<option value='".$tipo->id_cat_cuenta_contable."' selected>".$tipo->CODIGO." ".$tipo->descripcion."</option>";
								} else {
									echo "<option value='".$tipo->id_cat_cuenta_contable."' >".$tipo->CODIGO." ".$tipo->descripcion."</option>";
								}
							}
							}
							?>
							</select>
						</div>							
					</div>
					<div class="form-group">
						<label for="cuenta_producto" class="col-sm-2 form-label">Cuenta Presupuestaria<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
						<div class="col-sm-4">
							<select class="form-control" id="cuenta_id" style="width: 80%;">
								<option value="0">Seleccione una cuenta presupuestaria</option>
								<?php
									if($cuentas){
										foreach ($cuentas as $cuenta) {
											if ($cuenta->id_cat_cuenta==@$producto[0]->id_cat_cuenta) {
												echo "<option value='".$cuenta->id_cat_cuenta."' selected>".$cuenta->cod_cuenta." ".$cuenta->nombre_cuenta."</option>";
											} else {
												echo "<option value='".$cuenta->id_cat_cuenta."'>".$cuenta->cod_cuenta." ".$cuenta->nombre_cuenta."</option>";
											}
										}
									}
								?>
							</select>
						</div>	
						<label for="unidad_producto" class="col-sm-2 form-label">Unidad de medida:</label>
						<div class="col-sm-4">
							<select class="form-control" id="unidad_id" style="width: 80%;">
								<option value="">Seleccione una unidad</option>
								<?php
									if($medidas){
										foreach ($medidas as $unidad) {
											if($unidad->ID_CAT_UNIDAD_MEDIDA==@$producto[0]->ID_UNIDAD){
												echo "<option value='".$unidad->ID_CAT_UNIDAD_MEDIDA."' selected>".$unidad->DESCRIPCION."</option>";
											} else {
												echo "<option value='".$unidad->ID_CAT_UNIDAD_MEDIDA."'>".$unidad->DESCRIPCION."</option>";
											}
										}
									}
								?>
							</select>
						</div>									
			
					</div>
				</form>		
		<div id="mensaje"></div>				
				<div class="col-sm-12">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab1" data-toggle="tab" aria-expanded="true">Movimientos</a>
							</li>
							<li>
								<a href="#tab2" data-toggle="tab" aria-expanded="false">Inventario Fisico</a>
							</li>
						</ul>
						
					</div>
					<div class="tab-content">
						<div class="tab-pane active " id="tab1">
							<div class="col-sm-6 table-responsive">
								<table id="entradas" class="table table-hovered table-striped">
									<thead>
										<th>Tipo Movimiento</th>
										<th>Fecha Registro</th>
										<th>Cantidad</th>
										<th>Costo Unitario</th>
										<th>Creado por</th>
										<th>Fecha creaci&oacute;n</th>
										
									</thead>
									<tbody>
										<?php 
											if ($movimientos) {
												foreach ($movimientos as $mo) {
													if ($mo->ID_TIPO_MOVIMIENTO==1) {
													echo "<tr>";
													echo "<td>".$mo->TIPO_MOVIMIENTO."</td>";
													echo "<td>".$mo->FECHA."</td>";
													echo "<td>".$mo->CANTIDAD."</td>";
													echo "<td >".$mo->COSTO_UNITARIO."</td>";
													echo "<td>".$mo->USUARIO_CREACION."</td>";
													echo "<td>".$mo->FECHA_CREACION."</td>";;
													echo "</tr>";
													}
												}
											}
														
													
										?>
									</tbody>	
								</table>																<!--										<button title='Editar Movimiento' id='btnHabilitar_<?php echo $mo->ID_MOVIMIENTO?>' class='btn btn-success btn-xs' onclick='desbloquearElementos("<?php echo $mo->ID_MOVIMIENTO ?>")'  ><span class='glyphicon glyphicon-edit'></span></button>
										<button type='button' onclick="modificarMovimiento('<?php echo $mo->ID_MOVIMIENTO?>','<?php echo @$producto[0]->NOMBRE_PRODUCTO?>')" title='guardar cambios' class='btn btn-primary btn-xs' disabled="true"  id='btnEditar_<?php echo $mo->ID_MOVIMIENTO?>'><span class='glyphicon glyphicon-ok'></span></button> -->								
							
							</div>
							<div class="col-sm-6 table-responsive">
								<table id="salidas" class="table table-hovered table-striped">
									<thead>
										<th>Tipo Movimiento</th>
										<th>Fecha Registro</th>
										<th>Cantidad</th>
										<th>Costo Unitario</th>
										<th>Creado por</th>
										<th>Fecha creaci&oacute;n</th>
									</thead>
									<tbody>
										<?php 
											if ($movimientos) {
												foreach ($movimientos as $mo) {
													if ($mo->ID_TIPO_MOVIMIENTO==2) {
													echo "<tr>";
													echo "<td>".$mo->TIPO_MOVIMIENTO."</td>";
													echo "<td>".$mo->FECHA."</td>";
													echo "<td>".$mo->CANTIDAD."</td>";
													echo "<td >".$mo->COSTO_UNITARIO."</td>";
													echo "<td>".$mo->USUARIO_CREACION."</td>";
													echo "<td>".$mo->FECHA_CREACION."</td>";;
													echo "</tr>";


													} 
													
												}
											}
										 ?>													
										
								<!--		<button title='Editar Movimiento' id='btnHabilitar_<?php echo $mo->ID_MOVIMIENTO?>' class='btn btn-success btn-xs' onclick='desbloquearElementos("<?php echo $mo->ID_MOVIMIENTO ?>")'  ><span class='glyphicon glyphicon-edit'></span></button>
										<button type='button' onclick="modificarMovimiento('<?php echo $mo->ID_MOVIMIENTO?>','<?php echo @$producto[0]->NOMBRE_PRODUCTO?>')" title='guardar cambios' class='btn btn-primary btn-xs' disabled="true"  id='btnEditar_<?php echo $mo->ID_MOVIMIENTO?>'><span class='glyphicon glyphicon-ok'></span></button> -->
										
											
									</tbody>
								</table>								
							</div>							

						</div>

						<div class="tab-pane" id="tab2"> 
						 	<table id="inventarios" class="table table-hovered table-striped"> 
						 		<thead> 
						 			<th>Fecha Ingreso</th> 
						 			<th>Existencia</th> 
						 			<th>Creado por</th>
						 			<th>Estado</th> 
						 			<th></th> 
						 		</thead> 
						 		<tbody> 
						 			<?php 
						 			if($inventarios)
						 				{
						 					foreach ($inventarios as $i)
						 					 {
						 					 	echo "<tr>"; 
						 					 	echo "<td>".$i->FECHA_INGRESO."</td>";
						 					 	 echo "<td>".$i->CANTIDAD."</td>"; 
						 					 	 echo "<td>".$i->USUARIO_CREACION."</td>"; 
						 					 	 if ($i->VIGENCIA==1) {
						 					 	 	echo "<td>ACTIVO</td>";
						 					 	 } else {
						 					 	 	echo "<td>CERRADO</td>";
						 					 	 }
						 					 	 
						 					} 

						 		 	} ?> 
						 		</tbody> 
						 	</table> 
						 </div>
					</div>	
					
				</div>
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
			</div>
			
		</div>
		
	</section>
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_producto"
        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"catalogos/cat_producto"
    	},function(){
    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }    
function desbloquearElementos(id)
{
	document.getElementById("btnHabilitar_"+id).disabled=true;
	document.getElementById("cantidad_"+id).disabled=false;
	document.getElementById("precio_"+id).disabled=false;
	document.getElementById("btnEditar_"+id).disabled=false;
}
function desbloquearElementosInv(id)
{
	document.getElementById("btnHabilitar_"+id).disabled=true;
	document.getElementById("cantidad_"+id).disabled=false;
	document.getElementById("btnEditar_"+id).disabled=false;
}    
function modificarMovimiento(id,nombre)
{
	alertify.confirm("Confirmar cambios en movimiento a producto "+nombre,"Si da click en aceptar se modificara el movimiento seleccionado",function(){
            document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>";
            var movimiento=new Object();
            movimiento.id=id;
            movimiento.cantidad=$("#cantidad_"+id).val();
            movimiento.precio=$("#precio_"+id).val().replace("$",'');
            var DatosJson=JSON.stringify(movimiento);
            console.log(movimiento);
            $.post(baseurl+'catalogos/cat_producto/saveMovimiento',{
            	MovimientoPost: DatosJson
            }, function(data,textStatus){
            	console.log(data);
            	$("#mensaje").html(data.response_msg);
            },"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);                 	
            });;
	},function(){
	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
}
function modificarInventario(id,nombre)
{
	alertify.confirm("Confirmar cambios en el inventario del producto "+nombre,"Si da click en aceptar se modifica el inventario seleccionado",function(){
            document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>";
        var inventario=new Object();
        inventario.id=id;
        inventario.cantidad=$("#cantidad_"+id).val();
        inventario.precio=parseFloat($("#precio_"+id).val().replace("$",''));
        var DatosJson=JSON.stringify(inventario);
        $.post(baseurl+'catalogos/cat_producto/saveInventario',{
        	InventarioPost: DatosJson
        },function(data,textStatus){
        	console.log(data);
        	$("#mensaje").html(data.response_msg);
        },"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
        });;
	},function(){
	});
}
$(document).ready(function(){
        $("#tipo_id").select2();
        $("#unidad_id").select2();
        $("#cuenta_id").select2();
        
$("#entradas").DataTable({
	      paging      : true,
	      lengthChange: true,
	      searching   : true,
	      ordering    : true,
	      info        : true,
	      autoWidth   : false,
		  "order" : [],
	      language: {
	        search:'Buscar:',
	        order: 'Mostrar Entradas',
	        paginate: {
	            first:"Primero",
	            previous:"Anterior",
	            next:"Siguiente",
	            last:"Ultimo"
	        },
	        emptyTable: "No hay informacion disponible",
	        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
	        lengthMenu:"Mostrar _MENU_ Entradas",
	        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
	      }	      
		});
$("#salidas").DataTable({
	      paging      : true,
	      lengthChange: true,
	      searching   : true,
	      ordering    : true,
	      info        : true,
	      autoWidth   : false,
		  "order" : [],
	      language: {
	        search:'Buscar:',
	        order: 'Mostrar Entradas',
	        paginate: {
	            first:"Primero",
	            previous:"Anterior",
	            next:"Siguiente",
	            last:"Ultimo"
	        },
	        emptyTable: "No hay informacion disponible",
	        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
	        lengthMenu:"Mostrar _MENU_ Entradas",
	        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
	      }	      
		});
/*$("#inventarios").DataTable({
	      paging      : true,
	      lengthChange: true,
	      searching   : true,
	      ordering    : true,
	      info        : true,
	      autoWidth   : false,
	      "order" : [],
	      language: {
	        search:'Buscar:',
	        order: 'Mostrar Entradas',
	        paginate: {
	            first:"Primero",
	            previous:"Anterior",
	            next:"Siguiente",
	            last:"Ultimo"
	        },
	        emptyTable: "No hay informacion disponible",
	        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
	        lengthMenu:"Mostrar _MENU_ Entradas",
	        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
	      }	
		});*/
});
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsonProducto.js');?>"></script>