<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
		<section class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-header">
					<h3 class="title-header">Productos con existencias minimas</h3>
				</div>
					<div class="box-body">
						<table id="productos" class="table table-bordered table-striped"> 
						<thead>
							<th>Nombre</th>
							<th>Unidad de medida</th>
							<th>Tipo de producto</th>
							<th>Minimo</th>
							<th>Cantidad actual</th>						
							<th>Cuenta Administrativa</th>							
						</thead>
						<tbody>
							<?php 
								if($existencias){
									foreach ($existencias as $e) {
										echo "<tr>";
										echo "<td>".$e->NOMBRE_PRODUCTO."</td>";
										echo "<td>".$e->MEDIDA."</td>";
										echo "<td>".$e->TIPO."</td>";
										echo "<td>".$e->MINIMO."</td>";
										echo "<td>".$e->CANTIDAD_MOVIMIENTOS."</td>";
										echo "<td>".$e->CUENTA."</td>";
										echo "</tr>";
									}
								} else {
									echo "<tr><td colspan='7' style='text-align:center;'>No hay informacion</td></tr>";
								}
							?>
						</tbody>							
						</table>
					</div>
				
				
			</div>
		</section>
</div>