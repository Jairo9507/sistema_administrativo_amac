<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"inventario/requisicion"
        } );
    }
    
</script>

<div class="content-wrapper">
	 <section class="content-header">
	 	<?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
	 </section>
	 <section class="content">
	 	<div class="box"> 
            <div id="mensaje"></div>
            <div class="box-header">
                <h3>Lista de Requisiciones</h3>
            </div>
                        <div class="row">                    
                            <div class="col-sm-2 pull-right">
                                <a href="requisicion/cierre" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-remove-sign"></span> Limpiar</button></a>                           
                              
                            </div>        
                            <div class="col-sm-2 pull-right">
                                <a href="requisicion/historico" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> ver Historicos</button></a>                                       
                            </div>
                            <div class="col-sm-2 pull-right"> 
                                <a href="requisicion/nuevo" ><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>
                            </div>
                        </div>  
            <div class="box-body">
                <table id="requisciones" class="table table-bordered table-striped">
                    <thead>
                        <th>Correlativo</th>
                        <th>Fecha salida</th>
                        <th>Unidad solicita</th>
                        <th>Creada por</th>
                        <th>Fecha creada</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php 
                            if ($requisiciones) {
                                foreach ($requisiciones as $r) {
                                    $requisicionId=base64_encode($r->ID_REQUISICION);
                                    echo "<tr>";       
                                    echo "<td>".$r->CORRELATIVO."</td>";
                                    echo "<td>".date('d-m-Y',strtotime($r->FECHA))."</td>";
                                    echo "<td>".$r->unidad_solicita."</td>";
                                    echo "<td>".$r->USUARIO_CREACION."</td>";
                                    echo "<td>".date('d-m-Y',strtotime($r->FECHA_CREACION))."</td>";
                                    echo "<td><a href='requisicion/editar/".$requisicionId."' ><button type='button' title='Editar Requisicion' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";

                         ?>

                         <?php 

                                echo "</td>";
                                echo "</tr>";
                                }
                            }
                          ?>
                    </tbody>
                </table>
            </div>          
        </div>
	 </section>
</div>
<div class="modal fade text-center" id="modal_nuevo" >
  <div class="modal-dialog" >
    <div class="modal-content">
    </div>
  </div>
</div>
<!-- page script -->
<script>
  $(function () {
    $('#requisciones').DataTable({
      paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }        
    });
  })
</script>