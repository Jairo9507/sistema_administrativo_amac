<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<script type="text/javascript">
	$(document).ready(function(){
		
    $('#productos').DataTable({
      pageLength:5,
      "order" : [],
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }            
      
    });
 
	});
</script>
<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>					
	</div>
	<div class="modal-body">
		<div id="mensaje_productos"></div>
		<table id="productos" class="table table-bordered table-striped">
			<thead>
				<th>Codigo</th>
				<th>Nombre</th>
				<th>Cantidad</th>
				<th></th>
			</thead>
			<tbody>
				<?php 
					if ($productos) {
						foreach ($productos as $p) {
							echo "<tr>";
							echo "<td>".$p->COD_PRODUCTO."</td>";
							echo "<td>".$p->NOMBRE_PRODUCTO."</td>";
							echo "<td class='col-xs-1'> <input type='text' class='form-control' style='text-align:right' id='cantidad_".$p->ID_PRODUCTO."'value='1' ></td>";
							//echo "";

				 ?>
<td><span class='pull-right'><a href='#' onclick='agregar("<?php echo $p->NOMBRE_PRODUCTO?>", "<?php echo $p->ID_PRODUCTO;?>")'><i class='glyphicon glyphicon-plus'></i></a></span></td>				 
				 <?php 

							echo "</tr>";							
						}
					}

				  ?>
			</tbody>
		</table>
	</div>
	<div class="modal-footer">
<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>		
	</div>
</div>
<script type="text/javascript">
	function agregar(nombre, id)
	{
		var html = '';
	    var encabezado = '';
    	var cantidad = $("#cantidad_"+id).val();
    	console.log(cantidad);

    	 if (isNaN(cantidad)) {
	      alert('Esto no es un numero');
	      document.getElementById('cantidad_'+id).focus();
	      return false;
	    }
	    var detalle = new Object();
	    detalle.id_requisicion=$("input#id_requisicion").val();
	    detalle.id_producto=id;
	    detalle.cantidad=cantidad;
	    detalle.id_detalle=$("#detalleid").val();
	    if (detalle.id_detalle===undefined) {detalle.id_detalle='';}
	    console.log(detalle);
	    

	    var DatosJson=JSON.stringify(detalle);
	    console.log(DatosJson);
	    
	    $.post(baseurl+'inventario/requisicion/saveDetalle',{
	    	DetallePost:DatosJson
	    },function(data,textStatus){
	    	console.log("data: "+data.validar);
	    	$("#mensaje_productos").append(data.response_msg);
	    	if(detalle.id_detalle==='' && data.validar===true){
	    		html+="<tr>";
		    	html+="<td style='width:15%'>"+id+"</td>";
	          	html+="<td style='width:50%'>"+nombre+"</td>";
	          	html+="<td style='width:15%'>"+cantidad+"</td>";
		    	html+="</tr>";
	    	} else if (data.validar===true){
	    		
	    		html+="<tr>";
		    	html+="<td style='width:15%'>"+id+"</td>";
	          	html+="<td style='width:50%'>"+nombre+"</td>";
	          	html+="<td class='col-xs-1'> <input type='text' class='form-control' style='text-align:right' id='cantidad_"+detalle.id_producto+"' value="+cantidad+"></td>";
	          	html+="<td><span class='pull-right'><a href='#' onclick='modificar("+nombre+","+data.detalle_id+","+detalle.id_producto+")'><i class='glyphicon glyphicon-pencil'></i></a></span></td>"
		    	html+="</tr>";

	    	} 

	    	$("#tablaProductos").show();
	    	$("#btnGuardar").attr("disabled", false);
	    	$("#tablaProductosbody").append(html);
	    	
	    },"json").fail(function(response){
			$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    		console.log('Error: ' + response.responseText);
	    });;

	}
</script>
