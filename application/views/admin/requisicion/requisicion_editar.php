<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 

	$correlativo= array(
		'name' 		=> 'correlativo',
		'id'		=> 'correlativo',
		'value'		=> set_value('correlativo',@$requisicion[0]->CORRELATIVO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'SR001',
		'disabled'	=> ''

	);

    $concepto= array(
        'name'      => 'concepto',
        'id'        => 'concepto',
        'value'     => set_value('concepto',@$requisicion[0]->CONCEPTO),
        'type'      => 'textarea',
        'rows'      =>4,
        'class'     => 'form-control',
        'placeholder' => 'Concepto',
    );    

    $cantidad= array(
        'name'      => 'cantidad',
        'id'        => 'cantidad',
        'value'     => set_value('correlativo',''),
        'type'      => 'number',
        'class'     => 'form-control',
        'placeholder' => '',
        'min'       =>'1'
    );    

    $existencia=array(
        'name'      => 'existencia',
        'id'        => 'existencia',
        'value'     => set_value('correlativo',''),
        'type'      => 'text',
        'class'     => 'form-control',
        'placeholder' => '',
        'disabled'  => true

    );    

	$fecha =array(
		'name' 		=> 'fecha',
		'id'		=> 'fecha',
		'value'		=> set_value('fecha',date("Y-m-d", strtotime(@$requisicion[0]->FECHA))),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'placeholder' => ''
	);


 ?>
<input type="hidden" name="unidad" id="unidad" value="<?php echo $unidad;?>">
<input type="hidden" name="id" id="id_requisicion" value="<?php echo $requisicion[0]->ID_REQUISICION?>">
<input type='hidden' id='detalleid' value=''>
 <div class="content-wrapper">
 	<section class="content-header">
 		<?php echo $pagetitle; ?>
 		<?php echo $breadcrumb; ?>
 	</section>
 		<div class="box">
 			<div class="box-header">
 		<h3 class="header-title"></h3>
 			</div>
 			<div class="box-body">
 				<div id="mensaje"></div>                
 				<form class="form-horizontal" name="formulario" id="formulario" role="form">
 					<div class="form-group">
 						<label for="codigo" class="col-sm-2 form-label">Codigo</label>
 						<div class="col-sm-4">
 							<?php  echo form_input($correlativo);?>
 						</div>
 						<label for="fecha" class="col-sm-2 form-label">Fecha <span style="color: #F20A06; font-size: 15px;">*</span></label>
 						<div class="col-sm-4">
 							<?php  echo form_input($fecha);?>
 						</div> 						
 					</div>
 					<div class="form-group">
 						<label for="unidad_crea" class="col-sm-2 form-label">Unidad Solicita <span style="color: #F20A06; font-size: 15px;">*</span></label>
 						<div class="col-sm-4">
 							<select id="listUnidad" class="form-control js-example-basic-single">
 								<option value="0">Seleccionar unidad</option>
 								<?php 
 									if ($unidades) {
 										foreach ($unidades as $u) {
 											if ($requisicion[0]->unidad_solicita==$u->ID_UNIDAD) {
                                                echo "<option value=".$u->ID_UNIDAD." selected>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";                                            } else {
                                                echo "<option value=".$u->ID_UNIDAD.">".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
                                            }
                                            
 										}
 									}

 								 ?>
 							</select>
 						</div>
                            <label for="concepto" class="col-sm-2 form-label">Observaciones</label>
                            <div class="col-sm-4">
                                <?php echo form_textarea($concepto); ?>
                            </div>                                              
                        <div class="col-sm-3 ">
                            <button type="button" id="btnProducto" style="" class="btn btn-info"><span class="glyphicon glyphicon-plus" ></span> Agregar productos</button>
                        </div>
 					</div>
                    <div class="form-group " id="form_productos" style="display: none;">
                        <label for="productos" class="col-sm-2 form-label">Productos</label>
                            <div class="col-sm-4">
                                <select id="listProducto" class="form-control js-example-basic-single" style="width:90%!important; ">
                                    <option value="">Seleccionar Producto</option>
                                    <?php  
                                        if ($productos) {
                                            foreach ($productos as $p) {
                                                echo "<option value='".$p->ID_PRODUCTO."'>".$p->COD_PRODUCTO." ".$p->NOMBRE_PRODUCTO."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div> 
                            <label for="cantidad" class="col-sm-2 form-label">Cantidad</label>
                            <div class="col-sm-2">
                                <?php echo form_input($cantidad); ?>
                            </div> 
                        <div class="col-sm-2 pull-right">
                            <button type="button" id="btnAgregar" class="btn btn-primary" ><span class="glyphicon glyphicon-plus"></span> </button>       

                       </div>
                            <label class="col-sm-2 form-label" for="existencia">Existencia</label>
                           <div class="col-sm-2">
                               <?php  echo form_input($existencia);?>
                           </div>                       
                    </div>                    
 				</form>
				<div >
					 <div id="tablaProductos" class="col-sm-12" >
					    <table id="productos_editar" class="table table-bordered table-striped"  style="width: 99%;position: initial;left: 35px;bottom: 77px;">
					              <thead>
					                <tr>
					                  <th style="width: 15%;">Codigo</th>
					                  <th style="width: 50%;">Nombre</th>
					                  <th style="width: 15%;">Cantidad</th>
                                      <th style="width: 10%; display: none;">ID</th>
                                      <th style="width: 10%; display: none;">detalle</th> 
                                      <th></th>
					                </tr>
					              </thead>
					              <tbody id="tablaProductosbody">
					              	<?php 
                                        if ($detalles) {
                                            
                                            foreach ($detalles as $d) {
                                                echo "<tr>";

                                                echo "<td>".$d->COD_PRODUCTO."</td>";
                                                echo "<td>".$d->NOMBRE_PRODUCTO."</td>";
                                                echo "<td class='col-xs-1'> <input type='text' class='form-control' style='text-align:right' id='cantidad_".$d->ID_PRODUCTO."'value='".$d->CANTIDAD."' ></td>";
                                               echo "<td class='Idproducto' style='display:none;'>".$d->ID_PRODUCTO."</td>";
                                                echo "<td class='Iddetalle' style='display:none;'>".$d->ID_DETALLE."</td>";

                                     ?>
                                                <td><button type="button" title="Editar Cantidad" class="btn btn-success btn-xs btnModificar"><i class='glyphicon glyphicon-pencil'></i></button> <button type="button"  title="Eliminar"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>
                                     <?php 

                                                echo "</tr>";                                                
                                            }
                                        }

                                      ?>
					              </tbody>
					    </table>         	
					 </div>	
				</div> 				
 			</div>
 			<div class="box-footer">
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"  onclick="salir()"><span class="glyphicon glyphicon-saved" ></span> Salir</button> 				
 			</div>
 		</div>
 </div>

<div class="modal fade text-center" id="modal_producto">
  <div class="modal-dialog" >
    <div class="modal-content">

    </div>
  </div>
</div>
 <script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_producto"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"inventario/requisicion"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }    

    function salir()
    {
    	alertify.confirm("¿Desea Regresar?","Asegurese de que la requisicion este correcta, de clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"inventario/requisicion"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }
 
    $(document).ready(function(){
    	$("#listUnidad").select2();
        $("#listProducto").select2();
    	//var unidad=$("input#unidad").val();
    	//console.log(unidad); 	

           /* $('#productos_editar').DataTable({
                  pageLength:5,
                  "order" : [],
                  language: {
                    search:'Buscar:',
                    order: 'Mostrar Entradas',
                    paginate: {
                        first:"Primero",
                        previous:"Anterior",
                        next:"Siguiente",
                        last:"Ultimo"
                    },
                    emptyTable: "No hay informacion disponible",
                    infoEmpty: "Mostrando 0 de 0 de 0 entradas",
                    lengthMenu:"Mostrar _MENU_ Entradas",
                    info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
                  }            
             });*/
        $("#fecha").blur(function(){
            date=$(this).val();
            console.log(date);
            var todaysDate = new Date();
            var year = todaysDate.getFullYear(); 
            var month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
            var day = ("0" + todaysDate.getDate()).slice(-2); 
            var cierre=year +"-"+ month +"-"+ day;
            var dt1=new Date(date);
            var dt2 =new Date(cierre);
            var diff=Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
            console.log(diff);
            if (diff>39) {
                    $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ya no se puede ingresar fechas de este mes</div>"); 
                    $(this).val(cierre);        
            }
            
        });              
        $("#btnProducto").click(function(e){
                var requisicion= new Object();
                requisicion.id=$("input#id_requisicion").val();
                requisicion.correlativo=$("input#correlativo").val();
                requisicion.fecha=$("input#fecha").val();
                requisicion.unidad_solicita=$("select#listUnidad").val();               
                requisicion.concepto=$("textarea#concepto").val();
                var DatosJson=JSON.stringify(requisicion);
                $.post(baseurl+'inventario/requisicion/save',{
                    requisicionPost: DatosJson
                }, function(data,textStatus){
                    //console.log(data.response_msg);
                    //$("#"+data.campo).focus();
                    $("#mensaje").append(data.response_msg);
                    $("#form_productos").show();                    
                },"json").fail(function(response){
                $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                        console.log('Error: ' + response.responseText);

                });;


        });  

          $("#listProducto").change(function(){
            var producto=new Object();
            producto.id=$(this).val();
            console.log(producto);
            var DatosJson=JSON.stringify(producto);
            if(producto.id!=''){
                $.post(baseurl+'inventario/requisicion/obtenerExistencia',{
                    ProductoPost:DatosJson
                },function(data,textStatus){
                    console.log(data);
                    $("#existencia").val(data.existencia);
                },"json").fail(function(response){
                    $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                            console.log('Error: ' + response.responseText);                
                });                
            }else {
                $("input#existencia").val("");
            }
        })
        $("#btnAgregar").click(function(){
            
            var producto= new Object();
            producto.id_producto=$("select#listProducto option:selected").val();
            producto.nombre=$("select#listProducto option:selected").text();
            producto.cantidad=$("input#cantidad").val();  
            producto.id_requisicion=$("input#id_requisicion").val(); 
            producto.fecha=$("input#fecha").val();
            producto.id_detalle='';         
            
            var html='';
            if (producto.cantidad!=='' || producto.id_producto==='') {

                html+="<tr>";
                html+="<td style='width:15%'>"+parseInt(producto.nombre)+"</td>";
                html+="<td style='width:50%'>"+producto.nombre+"</td>";
                html+="<td class='col-xs-1'> <input type='text' class='form-control' style='text-align:right' id='cantidad_"+producto.id_producto+"' value="+producto.cantidad+"></td>";

            } else {
                $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Se debe ingresar los datos del  producto</div>");
            }        
            $("#mensaje").html("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");            
        console.log(producto);
        var DatosJson=JSON.stringify(producto);
        console.log(DatosJson);        
        $.post(baseurl+'inventario/requisicion/saveDetalle',{
            DetallePost:DatosJson
        },function(data,textStatus){
            console.log("data: "+data.validar);
                html+="<td><button type='button' title='Editar Cantidad' class='btn btn-success btn-xs btnModificar'><i class='glyphicon glyphicon-pencil'></i></button> <button type='button'  title='Eliminar Estudio'  class='btn btn-danger btn-xs btnEliminar'><span class='glyphicon glyphicon-remove'></span></button></td>"
            html+='<td class="Idproducto" style="display:none;">'+producto.id_producto+'</td>';

            html+='<td class="Iddetalle" style="display:none;">'+data.detalle_id+'</td>';
            html+='</tr>';         
            console.log("html "+html);
            $("#mensaje").html(data.response_msg);            
            if (data.validar===true) {
                $("#tablaProductosbody").append(html);
            }
            $("input#existencia").val("");
            $("input#cantidad").val("");
            $("select#listProducto").val("");
            $("select#listProducto").select2().trigger("change");
            $("#btnGuardar").attr("disabled", false);

        },"json").fail(function(response){
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);
        });;
   
            $("#btnGuardar").attr("disabled",false);
        });
        $(document).on('click', '.btnEliminar', function () {
            // your function here
            var idp=$(this).closest("tr").find(".Idproducto").text();
            var id_de=$(this).closest("tr").find(".Iddetalle").text();
            var cantidad=$("input#cantidad_"+idp).val();            
            var detalle=new Object();
            detalle.id_detalle=id_de;
            detalle.id_producto=idp;
            detalle.id_requisicion=$("input#id_requisicion").val();
            detalle.cantidad= cantidad;
            detalle.fecha= $("input#fecha").val();
            var DatosJson=JSON.stringify(detalle);
            console.log(detalle);
            $("#mensaje").html("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");              
            $.post(baseurl+'inventario/requisicion/deleteDetalle',{
                DetallePost:DatosJson
            },function(data){
                console.log("btnEliminar");
                $("#mensaje").html(data.response_msg);

            },"json").fail(function(response){
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);                
            });;
                $(this).closest("tr").remove();
        });  

        $(document).on('click' ,'.btnModificar',function(){
           
            id_producto=$(this).closest("tr").find(".Idproducto").text();
            iddetalle=$(this).closest("tr").find(".Iddetalle").text();
            console.log(id_producto);
            console.log(iddetalle);
            var html = '';
            var cantidad = $("input#cantidad_"+id_producto).val();
            console.log(cantidad);

            if (isNaN(cantidad)) {
              alert('Esto no es un numero');
              document.getElementById('cantidad_'+id_producto).focus();
              return false;
            }
            var detalle = new Object();
            detalle.id_requisicion=$("input#id_requisicion").val();
            detalle.id_producto=id_producto;
            detalle.cantidad=cantidad;
            detalle.id_detalle=iddetalle;
            detalle.fecha =$("input#fecha").val();            
            console.log(detalle);
            $.post(baseurl+'inventario/requisicion/saveDetalle',{
                DetallePost:JSON.stringify(detalle)
            },function(data,textStatus){
                console.log("btnModificar");
                console.log(data);
                $("#mensaje").append(data.response_msg);
            },"json").fail(function(response){
                $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
            });;

        });
    });
</script>