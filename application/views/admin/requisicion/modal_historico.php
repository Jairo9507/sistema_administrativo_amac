<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 
	$fecha_inicio_historico=array(
		'name' 		=> 'fecha_inicio_historico',
		'id'		=> 'fecha_inicio_historico',
		'type'		=> 'date',
		'class'		=> 'form-control',
		'placeholder' => ''

	);

	$fecha_fin_historico=array(
		'name' 		=> 'fecha_fin_historico',
		'id'		=> 'fecha_fin_historico',
		'type'		=> 'date',
		'class'		=> 'form-control',
		'placeholder' => ''
	);

 ?>
 <div class="modal-content">
 	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
	</div>
	<div class="modal-body">
			<div class="alert alert-info" role="alert">
			  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			  <span class="sr-only">Informacion:</span>
			  Complete los campos con el intervalo de fechas de las requisiciones que desea ver
			</div>
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div class="form-group">
				<label for="fecha_inicio_cierre" class="col-sm-2 form-label">Fecha inicio cierre:</label>
				<div class="col-sm-4">
					<?php echo form_input($fecha_inicio_historico); ?>
				</div>
				<label for="fecha_fin_cierre" class="col-sm-2 form-label">Fecha fin cierre:</label>
				<div class="col-sm-4">
					<?php echo form_input($fecha_fin_historico); ?>
				</div>
			</div>	
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
		<button type="submit" class="btn btn-primary" id="btnGuardar" ><span class="glyphicon glyphicon-saved" ></span> Guardar</button>		
	</div>	
 </div>
 <script type="text/javascript">
 	$(document).ready(function(){

 		$("#btnGuardar").click(function(){
 			var cierre=new Object();
 			cierre.fecha_inicio=$("#fecha_inicio_historico").val();
 			cierre.fecha_fin=$("#fecha_fin_historico").val();
 			var DatosJson=JSON.stringify(cierre);
 			console.log(cierre);
			$("#modal_nuevo").modal('hide');						
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>"); 			
 			$.post(baseurl+'inventario/requisicion/verHistoricos',{
 				CierrePost:DatosJson
 			},function(data,textStatus){
 				$("#mensaje").html(data.response_msg)
 			},"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
 			});;
 		});
 	});

 </script>