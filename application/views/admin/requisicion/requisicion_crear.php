<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 

	$correlativo= array(
		'name' 		=> 'correlativo',
		'id'		=> 'correlativo',
		'value'		=> set_value('correlativo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'SR001',
		'disabled'	=> ''

	);

    $concepto= array(
        'name'      => 'concepto',
        'id'        => 'concepto',
        'value'     => set_value('codigo',''),
        'type'      => 'textarea',
        'rows'      =>4,
        'class'     => 'form-control',
        'placeholder' => 'Concepto',
    );    

    $existencia=array(
        'name'      => 'existencia',
        'id'        => 'existencia',
        'value'     => set_value('correlativo',''),
        'type'      => 'text',
        'class'     => 'form-control',
        'placeholder' => '',
        'disabled'  => true

    );

    $cantidad= array(
        'name'      => 'cantidad',
        'id'        => 'cantidad',
        'value'     => set_value('correlativo',''),
        'type'      => 'number',
        'class'     => 'form-control',
        'placeholder' => '',
        'min'       =>'1'
    );    



	$fecha =array(
		'name' 		=> 'fecha',
		'id'		=> 'fecha',
		'value'		=> set_value('correlativo',''),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'placeholder' => ''
	);


 ?>

<input type="hidden" name="unidad" id="unidad" value="<?php echo $unidad;?>">
 <div class="content-wrapper">
 	<section class="content-header">
 		<?php echo $pagetitle; ?>
 		        <?php echo $breadcrumb; ?>
 	</section>
 		<div class="box">
 			<div class="box-header">
 		<h3 class="header-title"></h3>
 			</div>
 			<div class="box-body">
 				<div id="mensaje"></div>
                <div id="mensaje_productos"></div>
 				<form class="form-horizontal" name="formulario" id="formulario" role="form">
 					<div class="form-group">
 						<label for="codigo" class="col-sm-2 form-label">Codigo</label>
 						<div class="col-sm-4">
 							<?php  echo form_input($correlativo);?>
 						</div>
 						<label for="fecha" class="col-sm-2 form-label">Fecha <span style="color: #F20A06; font-size: 15px;">*</span></label>
 						<div class="col-sm-4">
 							<?php  echo form_input($fecha);?>
 						</div> 						
 					</div>
 					<div class="form-group" >
 						<label for="unidad_crea" class="col-sm-2 form-label">Unidad  Solicita <span style="color: #F20A06; font-size: 15px;">*</span></label>
 						<div class="col-sm-4">
 							<select id="listUnidad" class="form-control js-example-basic-single" style="width:90%!important; ">
 								<option value="0">Seleccionar unidad</option>
 								<?php 
 									if ($unidades) {
 										foreach ($unidades as $u) {
 											echo "<option value=".$u->ID_UNIDAD.">".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
 										}
 									}

 								 ?>
 							</select>
 						</div>
                            <label for="concepto" class="col-sm-2 form-label">Observaciones</label>
                            <div class="col-sm-4">
                                <?php echo form_textarea($concepto); ?>
                            </div> 						
                        <div class="col-sm-3 ">
                            <button type="button" id="btnProducto" style="display:none" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span> Agregar productos</button>
                        </div>
 					</div>
                    <div class="form-group " id="form_productos" style="display: none;">
                        <label for="productos" class="col-sm-2 form-label">Productos</label>
                            <div class="col-sm-4">
                                <select id="listProducto" class="form-control js-example-basic-single" style="width:90%!important; ">
                                    <option value="">Seleccionar Producto</option>
                                    <?php  
                                        if ($productos) {
                                            foreach ($productos as $p) {
                                                echo "<option value='".$p->ID_PRODUCTO."'>".$p->COD_PRODUCTO." ".$p->NOMBRE_PRODUCTO."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div> 
                            <label for="cantidad" class="col-sm-2 form-label">Cantidad</label>
                            <div class="col-sm-2">
                                <?php echo form_input($cantidad); ?>
                            </div> 
                        <div class="col-sm-2 pull-right">
                            <button type="button" id="btnAgregar" class="btn btn-primary" ><span class="glyphicon glyphicon-plus"></span> </button>                      
                       </div>
                            <label class="col-sm-2 form-label" for="existencia">Existencia</label>
                           <div class="col-sm-2">
                               <?php  echo form_input($existencia);?>
                           </div>
                       
                    </div>
                </form> 
                     <div id="tablaProductos" class="col-sm-12" style="display: none;">
                                                                   
					    <table id="" class="table table-bordered table-striped"  style="width: 99%;position: initial;left: 35px;bottom: 77px;">
					              <thead>
					                <tr>
					                  <th style="width: 10%;">Codigo</th>
                                      <th style="width: 10%; display: none;">ID</th>
                                      <th style="width: 10%; display: none;">detalle</th>
					                  <th style="width: 50%;">Nombre</th>
					                  <th style="width: 15%;">Cantidad</th>
                                      <th style="width: 15%;"></th>
					                </tr>
					              </thead>
					              <tbody id="tablaProductosbody">
					              	
					              </tbody>
					    </table>         	
					 </div>				
             
 			</div>
           
 			<div class="box-footer">

			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar" disabled="" onclick="salir();"><span class="glyphicon glyphicon-saved" ></span> Salir</button> 				
 			</div>
 		</div>
 </div>


<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
        
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"inventario/requisicion"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"inventario/requisicion"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }    

    function salir()
    {
    	alertify.confirm("¿Desea Regresar?","Asegurese de que la requisicion este correcta, de clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"inventario/requisicion"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }

    $(document).ready(function(){
        $("#cantidad","#formulario").keypress(function(e){
            var key = e.charCode || e.keyCode || 0;
            $nit = $(this);
            if (key >=48 && key <= 57) {                     
                if ($nit.val().length === 4) {
                    $nit.val($nit.val() + '-');
                } else if($nit.val().length===9)
                    {
                        e.preventDefault();
                    }            

            } else {
                e.preventDefault();
            }            
        });
        $("#listUnidad").select2();
        $("#listProducto").select2();
        var unidad='';
        console.log(unidad);    
        var codigo='';
        var date='';

        $.ajax({
            type: "GET",
            url: baseurl+'inventario/requisicion/obtenerCodigo',
            dataType:"json",
            success: function(data,textStatus){
                console.log("data "+data.codigo);
                if (data.codigo!=null &&  data.codigo!="000") {                 
                    //console.log(data.codigo.substring(data.codigo.length-1));
                    //int=parseInt(data.codigo.substring(data.codigo.length-1));                    
                    //console.log(int);                        
                    codigo=data.codigo;               
                    console.log("codigo: "+codigo);                
                } else {
                    codigo='1';
                    console.log("else "+codigo);
                } 
            }
        });        
        $("#fecha").blur(function(){
            date=$(this).val();
            console.log(date);
            var todaysDate = new Date();
            var year = todaysDate.getFullYear(); 
            var month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
            var day = ("0" + todaysDate.getDate()).slice(-2); 
            var cierre=year +"-"+ month +"-"+ day;
            var dt1=new Date(date);
            var dt2 =new Date(cierre);
            var diff=Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
            console.log(diff);
            if (diff>39) {
                    $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ya no se puede ingresar fechas de este mes</div>"); 
                    $(this).val(cierre);        
            } else if(diff<0){
                    $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se puede ingresar fechas mayores a la actual</div>"); 
                    $(this).val(cierre);                    
            }            
            $("#btnProducto").show();
            console.log("fecha "+date.replace(new RegExp('-','g'),""));
            date=date.replace(new RegExp('-','g'),"");
            $("input#correlativo").val('SR'+date+unidad+'0'+codigo);
        }); 
        $("#listUnidad").change(function(){
            text =$("#listUnidad option:selected").text();
            var Cunidad=text.split(" ")[0];
            console.log(Cunidad);
            unidad=Cunidad
            $("input#correlativo").val('SR'+date+unidad+'0'+codigo);            
        }); 


        $("#btnProducto").click(function(e){
                var requisicion= new Object();
                requisicion.id='';
                requisicion.correlativo=$("input#correlativo").val();
                requisicion.fecha=$("input#fecha").val();
                requisicion.unidad_solicita=$("select#listUnidad").val(); 
                requisicion.concepto=$("textarea#concepto").val();              
                console.log(requisicion);
                var DatosJson=JSON.stringify(requisicion);
                if ($("select#listUnidad").val()!=='0' && $("input#fecha").val()!='') {
                    $.post(baseurl+'inventario/requisicion/save',{
                        requisicionPost: DatosJson
                    }, function(data,textStatus){
                        //console.log(data.response_msg);
                        //$("#"+data.campo).focus();
                        $("#mensaje").append(data.response_msg);
                        $("#form_productos").show();                    
                    },"json").fail(function(response){
                    $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                            console.log('Error: ' + response.responseText);

                    });;                    
                } else {
                    $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Complete los campos mandatorios</div>");                    
                }

        }); 
        
        $("#listProducto").change(function(){
            var producto=new Object();
            producto.id=$(this).val();
            console.log(producto);
            var DatosJson=JSON.stringify(producto);
            if(producto.id!=''){
                $.post(baseurl+'inventario/requisicion/obtenerExistencia',{
                    ProductoPost:DatosJson
                },function(data,textStatus){
                    console.log(data);
                    $("#existencia").val(data.existencia);
                },"json").fail(function(response){
                    $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                            console.log('Error: ' + response.responseText);                
                });                
            }else {
                $("input#existencia").val("");
            }
        })

        $("#btnAgregar").click(function(){
            
            var producto= new Object();
            producto.id_producto=$("select#listProducto option:selected").val();
            producto.nombre=$("select#listProducto option:selected").text();
            producto.cantidad=$("input#cantidad").val();  
            producto.id_requisicion=$("input#id_requisicion").val();
            producto.fecha=$("input#fecha").val(); 
            producto.id_detalle='';
            $("#tablaProductos").show();         
            $("#tablaProductosbody").show();
            var html='';
            if (producto.cantidad!=='' || producto.id_producto==='') {

            html+='<tr>';
            html+='<td >'+parseInt(producto.nombre)+'</td>';
            html+='<td >'+producto.nombre+'</td>'
            html+='<td class="cantidad">'+producto.cantidad+'</td>';
            html+='<td><button type="button"  title="Eliminar"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>';
            html+='<td class="Idproducto" style="display:none;">'+producto.id_producto+'</td>';

            } else {
                $("#mensaje_productos").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Se debe ingresar los datos del  producto</div>");
            }        
            $("#mensaje_productos").html("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");            
        console.log(producto);
        var DatosJson=JSON.stringify(producto);
        console.log(DatosJson);        
        $.post(baseurl+'inventario/requisicion/saveDetalle',{
            DetallePost:DatosJson
        },function(data,textStatus){
            console.log("data: "+data.validar);
            html+='<td class="Iddetalle" style="display:none;">'+data.detalle_id+'</td>';
            html+='</tr>';         
            console.log("html "+html);
            $("#mensaje_productos").html(data.response_msg);            
            if (data.validar===true) {
                $("#tablaProductosbody").append(html);
            }
            $("input#existencia").val("");
            $("input#cantidad").val("");
            $("select#listProducto").val("");
            $("select#listProducto").select2().trigger("change");
            $("#btnGuardar").attr("disabled", false);
            console.log(data);


        },"json").fail(function(response){
            $("#mensaje_productos").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);
        });;
            
            $("#btnGuardar").attr("disabled",false);
        });

        $(document).on('click','.btnEliminar',function(){
            var idp=$(this).closest("tr").find(".Idproducto").text();
            var id_de=$(this).closest("tr").find(".Iddetalle").text();
            var cantidad=$(this).closest("tr").find(".cantidad").text();
            var detalle=new Object();
            detalle.id_detalle=id_de;
            detalle.id_producto=idp;
            detalle.id_requisicion=$("input#id_requisicion").val(); 
            detalle.cantidad=cantidad;
            console.log(detalle);
            var DatosJson=JSON.stringify(detalle);
            $("#mensaje_productos").html("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");     
            $.post(baseurl+'inventario/requisicion/deleteDetalle',{
                DetallePost:DatosJson
            },function(data){
                $("#mensaje_productos").html(data.response_msg);
               $("#existencia").val(data.existencia);               
            },"json").fail(function(response){
            $("#mensaje_productos").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);                
            });
             $(this).closest("tr").remove();

        });
         

    });
</script>