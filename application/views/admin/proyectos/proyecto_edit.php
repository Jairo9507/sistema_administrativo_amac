<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 

	$codigo= array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',@$proyecto[0]->CODIGO_PROYECTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'C&oacute;digo',

	);

	$nombre = array(
		'name' 		=> 'nombre',
		'id'		=> 'nombre',
		'value'		=> set_value('nombre',@$proyecto[0]->NOMBRE_PROYECTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre'
	);

	$alcance= array(
		'name' 		=> 'alcance',
		'id'		=> 'alcance',
		'value'		=> set_value('alcance',@$proyecto[0]->ALCANCE_PROYECTO),
		'type'		=> 'textarea',
		'rows'		=>	2,
		'class'		=> 'form-control',
		'placeholder' => 'Alcance'

	);

	$costo = array(
		'name' 		=> 'costo',
		'id'		=> 'costo',
		'value'		=> set_value('costo',@$proyecto[0]->COSTO_PROYECTO),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'placeholder' => '1.00'
	);

	$fechaInicio= array(
		'name' 		=> 'fechaInicio',
		'id'		=> 'fechaInicio',
		'value'		=> set_value('fechaInicio',date('Y-m-d',strtotime(@$proyecto[0]->FECHA_INICIO_PROYECTO))),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'onkeypress'	=> 'return validarn(event)'
	);	

	$fechaFin = array(
		'name' 		=> 'fechaFin',
		'id'		=> 'fechaFin',
		'value'		=> set_value('fechaFin',!empty(@$proyecto[0]->FECHA_FINAL_PROYECTO)?date('Y-m-d',strtotime(@$proyecto[0]->FECHA_FINAL_PROYECTO)):''),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'onkeypress'	=> 'return validarn(event)'

	);

	$ubicacion= array(
		'name' 		=> 'ubicacion',
		'id'		=> 'ubicacion',
		'value'		=> set_value('ubicacion',@$proyecto[0]->UBICACION),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Ubicaci&oacute;n de la obra'
	);

	$justificante = array(
		'name' 		=> 'justificante',
		'id'		=> 'justificante',
		'value'		=> set_value('justificante',@$proyecto[0]->JUSTIFICANTE_PROYECTO),
		'type'		=> 'textarea',
		'rows'		=>	2,
		'class'		=> 'form-control',
		'placeholder' => 'Justificante'
	);

	$realizador = array(
		'name' 		=> 'realizador',
		'id'		=> 'realizador',
		'value'		=> set_value('realizador',@$proyecto[0]->NOMBRE_REALIZADOR),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre del realizador',
		'onkeypress'	=> 'return validarn(event)'
	);

	$totalDias = array(
		'name' 		=> 'totalDias',
		'id'		=> 'totalDias',
		'value'		=> set_value('totalDias',@$proyecto[0]->TOTAL_DIAS_PROYECTO),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'placeholder' => 'No. dias'
	);
?>

<input type="hidden" name="id" id="id" value="<?php echo @$proyecto[0]->ID_PROYECTO?>">

<div class="content-wrapper">
	<section class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Datos</h3>
			</div>
			<div class="box-body">
				<div id="mensaje"></div>
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div class="form-group">
				<div class="col-sm-6">
					<label for='codigo' class="form-label col-sm-4">C&oacute;digo <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-8">
						<?php echo form_input($codigo);?>
					</div>					
				</div>
				<div class="col-sm-6">
					<label for="nombre" class="form-label col-sm-4">Nombre <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-8">
						<?php echo form_input($nombre);?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label for="alcance" class="form-label col-sm-4">Alcance :</label>
					<div class="col-sm-8">
						<?php echo form_textarea($alcance); ?>						
					</div>
				</div>
				<div class="col-sm-6">
					<label for="justificante" class="form-label col-sm-4">Justificante :</label>
					<div class="col-sm-8">
						<?php echo form_textarea($justificante) ?>
					</div>
				</div>				
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label for="prioridad" class="form-label col-sm-4">Prioridad: </label>
					<div class="col-sm-8">
						<select name="prioridad" id="prioridad" class="form-control col-sm-8">
							<option value="">Seleccione</option>
							<?php 
								if ($prioridades) {
									foreach ($prioridades as $p) {
										if (!empty($proyecto[0]->ID_CAT_PRIORIDAD) && $proyecto[0]->ID_CAT_PRIORIDAD==$p->ID_CAT_PRIORIDAD) {
											echo "<option value='".$p->ID_CAT_PRIORIDAD."' selected='true'>".$p->DESCRIPCION." </option>";
											
										} else {
											echo "<option value='".$p->ID_CAT_PRIORIDAD."'>".$p->DESCRIPCION." </option>";
										}
									}
								}
							?>
						</select>
					</div>
				</div>				
				<div class="col-sm-6">
					<label for="tipoProyecto" class="form-label col-sm-4">Tipo de proyecto: </label>
					<div class="col-sm-8">
						<select name="tipoProyecto" id="tipoProyecto" class="form-control">
							<option value="">Seleccione</option>
							<?php 
								if ($tipos) {
									foreach ($tipos as $t) {
										if (!empty($proyecto[0]->ID_CAT_TIPO_PROYECTO) && $proyecto[0]->ID_CAT_TIPO_PROYECTO==$t->ID_CAT_TIPO_PROYECTO) {
											echo "<option value='".$t->ID_CAT_TIPO_PROYECTO."' selected='true'>".$t->DESCRIPCION." </option>";
										} else {
											echo "<option value='".$t->ID_CAT_TIPO_PROYECTO."'>".$t->DESCRIPCION." </option>";
										}
									}
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label for="fechaInicio" class="form-label col-sm-4">Fecha de Inicio <span style="color: #F20A06; font-size: 15px;">*</span>: </label>
					<div class="col-sm-8">
						<?php echo form_input($fechaInicio);?>
					</div>
				</div>
				<div class="col-sm-6">
					<label for="fechaFin" class="form-label col-sm-4">Fecha de Fin :</label>
					<div class="col-sm-8">
						<?php echo form_input($fechaFin);?>
					</div>					
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label for="totalDias" class="form-label col-sm-4">Total de d&iacute;as (dias calendario) :</label>
					<div class="col-sm-8">
						<?php  echo form_input($totalDias);?>
					</div>
				</div>				
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label for="costo" class="form-label col-sm-4">Costo :</label>
					<div class="col-sm-8">
						<?php echo form_input($costo);?>
					</div>
				</div>
				<div class="col-sm-6">
					<label for="ubicacion" class="form-label col-sm-4">Ubicaci&oacute;n :</label>
					<div class="col-sm-8">
						<?php echo form_textarea($ubicacion); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					 <label for="nombreSupervisor" class="form-label col-sm-4">Supervisor <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					 <div class="col-sm-8">
					 	<select id="nombreSupervisor" name="nombreSupervisor" class="form-control">
					 		<option value="">Seleccione</option>
					 		<?php 
					 			if ($empleados) {
					 				foreach ($empleados as $e) {
					 					if (!empty($proyecto[0]->ID_SUPERVISOR) && $proyecto[0]->ID_SUPERVISOR==$e->ID_EMPLEADO) {
					 						echo "<option value='".$e->ID_EMPLEADO."' selected='true'>".$e->CODIGO_EMPLEADO." ".$e->NOMBRE."</option>";
					 					} else {
					 						echo "<option value='".$e->ID_EMPLEADO."'>".$e->CODIGO_EMPLEADO." ".$e->NOMBRE."</option>";
					 					}
					 					
					 				}
					 			}
					 		?>
					 	</select>
					 </div>
				</div>
				<div class="col-sm-6">
					<label for="realizador" class="form-label col-sm-4">Realizador :</label>
					<div class="col-sm-8">
						<?php echo form_input($realizador)?>
					</div>
				</div>
			</div>
		</form>
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	
				
			</div>
			
		</div>
		
	</section>
	
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"proyectos/proyecto"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"proyectos/proyecto"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsonProyecto.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>