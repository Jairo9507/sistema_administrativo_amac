<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 

	$codigo= array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'C&oacute;digo',

	);

	$nombre = array(
		'name' 		=> 'nombre',
		'id'		=> 'nombre',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'class'		=> 'form-control',
		'rows'		=> 2,
		'placeholder' => 'Nombre'
	);

	$alcance= array(
		'name' 		=> 'alcance',
		'id'		=> 'alcance',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	2,
		'class'		=> 'form-control',
		'placeholder' => 'Alcance'

	);

	$costo = array(
		'name' 		=> 'costo',
		'id'		=> 'costo',
		'value'		=> set_value('codigo',''),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'placeholder' => '1.00'
	);

	$fechaInicio= array(
		'name' 		=> 'fechaInicio',
		'id'		=> 'fechaInicio',
		'value'		=> set_value('codigo',''),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'onkeypress'	=> 'return validarn(event)'
	);	

	$fechaFin = array(
		'name' 		=> 'fechaFin',
		'id'		=> 'fechaFin',
		'value'		=> set_value('fechaFin',''),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'onkeypress'	=> 'return validarn(event)'

	);

	$ubicacion= array(
		'name' 		=> 'ubicacion',
		'id'		=> 'ubicacion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Ubicaci&oacute;n de la obra'
	);

	$justificante = array(
		'name' 		=> 'justificante',
		'id'		=> 'justificante',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	2,
		'class'		=> 'form-control',
		'placeholder' => 'Justificante'
	);

	$realizador = array(
		'name' 		=> 'realizador',
		'id'		=> 'realizador',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre del realizador',
		'onkeypress'	=> 'return validarn(event)'
	);

?>

<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
	</div>
	<div class="modal-body">
		
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div class="form-group">
				<div class="col-sm-6">
					<label for='codigo' class="form-label col-sm-4">C&oacute;digo <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-8">
						<?php echo form_input($codigo);?>
					</div>					
				</div>
				<div class="col-sm-6">
					<label for="nombre" class="form-label col-sm-4">Nombre <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-8">
						<?php echo form_textarea($nombre);?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label for="alcance" class="form-label col-sm-4">Alcance :</label>
					<div class="col-sm-8">
						<?php echo form_textarea($alcance); ?>						
					</div>
				</div>
				<div class="col-sm-6">
					<label for="justificante" class="form-label col-sm-4">Justificante :</label>
					<div class="col-sm-8">
						<?php echo form_textarea($justificante) ?>
					</div>
				</div>				
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label for="prioridad" class="form-label col-sm-4">Prioridad: </label>
					<div class="col-sm-8">
						<select name="prioridad" id="prioridad" class="form-control col-sm-8">
							<option value="">Seleccione</option>
							<?php 
								if ($prioridades) {
									foreach ($prioridades as $p) {
										echo "<option value='".$p->ID_CAT_PRIORIDAD."'>".$p->DESCRIPCION." </option>";
									}
								}
							?>
						</select>
					</div>
				</div>				
				<div class="col-sm-6">
					<label for="tipoProyecto" class="form-label col-sm-4">Tipo de proyecto: </label>
					<div class="col-sm-8">
						<select name="tipoProyecto" id="tipoProyecto" class="form-control">
							<option value="">Seleccione</option>
							<?php 
								if ($tipos) {
									foreach ($tipos as $t) {
										echo "<option value='".$t->ID_CAT_TIPO_PROYECTO."'>".$t->DESCRIPCION." </option>";
									}
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label for="fechaInicio" class="form-label col-sm-4">Fecha de Inicio <span style="color: #F20A06; font-size: 15px;">*</span>: </label>
					<div class="col-sm-8">
						<?php echo form_input($fechaInicio);?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label for="costo" class="form-label col-sm-4">Costo :</label>
					<div class="col-sm-8">
						<?php echo form_input($costo);?>
					</div>
				</div>
				<div class="col-sm-6">
					<label for="ubicacion" class="form-label col-sm-4">Ubicaci&oacute;n :</label>
					<div class="col-sm-8">
						<?php echo form_textarea($ubicacion); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					 <label for="nombreSupervisor" class="form-label col-sm-4">Supervisor <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					 <div class="col-sm-8">
					 	<select id="nombreSupervisor" name="nombreSupervisor" class="form-control">
					 		<option value="">Seleccione</option>
					 		<?php 
					 			if ($empleados) {
					 				foreach ($empleados as $e) {
					 					echo "<option value='".$e->ID_EMPLEADO."'>".$e->CODIGO_EMPLEADO." ".$e->NOMBRE."</option>";
					 				}
					 			}
					 		?>
					 	</select>
					 </div>
				</div>
				<div class="col-sm-6">
					<label for="realizador" class="form-label col-sm-4">Realizador :</label>
					<div class="col-sm-8">
						<?php echo form_input($realizador)?>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonProyecto.js');?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#nombreSupervisor").select2();
	});
</script>