<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 
$descripcion = array(
	'name'  => 'descripcion',
	'id'    => 'descripcion',
	'value' => set_value('codigo', @$tipo[0]->DESTIPO),
	'type'  =>  'textarea',
	'rows'  =>  4,
	'class' =>  'form-control', 
	'placeholder' => 'Descripcion',
	'onkeypress'  => 'return validar(event)'
);
?>

<input type="hidden" name="id" id="id" value="<?php echo @$tipo[0]->ID_TIPO_VEHICULO ?>">
<input type="text" name="id" id="id" value="<?php echo @$tipo[0]->ID_CLASE_VEHICULO ?>">

<div class="content-wrapper">
	<div class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
		
	</div>
<section class="content">
<div class="box">
<div class="box-header">
<h3 class="box-title">Datos</h3>
</div>
<div class="box-body">
<div id="mensaje"></div>
<form class="form-horizontal" name="formulario" id="formulario" role="form"> 
<div class="form-group">
	<div class="col-sm-4">
<label>Clase de vehículo: </label>
<select class="form-control" id="clase_id">
<!-- <option value="0">Seleccione la clase del vehículo</option> -->
<?php
if($clases){
foreach ($clases as $c) {
	if($c->ID_CLASE_VEHICULO==@$tipo[0]->ID_CLASE_VEHICULO){
		echo "<option value='".$c->ID_CLASE_VEHICULO."' selected>".$c->DESCRIPCION."</option>";
	} else {
		echo "<option value='".$c->ID_CLASE_VEHICULO."'>".$c->DESCRIPCION."</option>";
	}
}
}
?>
</select>
</div>
<div class="col-sm-4">
<label for='descripcion' class="form-label col-sm-2">Descripcion:</label>

<?php echo form_textarea($descripcion);?>
</div>
</div>					
</form>
</div>
			<div class="box-footer">
				<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
				<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	
				
			</div>
			
		</div>
		
	</section>
	
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
		alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
			window.location.href = baseurl+"catalogos/cat_tipo_vehiculo"

		} );
	}
	function regresar()
	{
		alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
			window.location.href = baseurl+"catalogos/cat_tipo_vehiculo"

		},function(){

		}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
	}
	$(document).ready(function(){
		$("#descripcion").focus();
		$("#clase_id").select2();

		$("#btnGuardar").click(function(){
			var Tipo = new Object();
			Tipo.id=$("input#id").val();
			Tipo.descripcion=$("textarea#descripcion").val();
			Tipo.id_clase_vehiculo = $("select#clase_id").val();
			if(Tipo.id===undefined){
				Tipo.id='';
			}
			console.log(Tipo);
			var DatosJson=JSON.stringify(Tipo);
			// $("#modal_nuevo").modal('hide');
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");

			$.post(baseurl+'catalogos/cat_tipo_vehiculo/guardar',{
				TipoVehiculoPost: DatosJson
			},function(data,textStatus){
				$("#mensaje").html(data.response_msg);
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
				console.log('Error: ' + response.responseText);
			});;
			return false;
		});
	});
</script>