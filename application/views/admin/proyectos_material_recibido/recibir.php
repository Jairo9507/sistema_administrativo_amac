<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  $nombreProyecto= array(
    'name'    => 'nombreProyecto',
    'id'    => 'nombreProyecto',
    'value'   => set_value('nombreProyecto',@$proyecto[0]->NOMBRE_PROYECTO),
    'type'    => 'textarea',
    'class'   => 'form-control',
    'rows'    => 2,
    'placeholder' => 'C&oacute;digo',
    'disabled' => true
  );

  $nombreRealizador = array(
    'name'    => 'nombreRealizador',
    'id'    => 'nombreRealizador',
    'value'   => set_value('nombreRealizador',@$proyecto[0]->NOMBRE_REALIZADOR),
    'type'    => 'text',
    'class'   => 'form-control',
    'placeholder' => 'Nombre del realizador',
    'rows'        => 2,
    'onkeypress'  => 'return validarn(event)',
    'disabled' => true    
  );

  $nombreSupervisor = array(
    'name'    => 'nombreSupervisor',
    'id'    => 'nombreSupervisor',
    'value'   => set_value('nombreSupervisor',@$proyecto[0]->EMPLEADO),
    'type'    => 'text',
    'class'   => 'form-control',
    'placeholder' => 'Nombre del supervisor',
    'rows'        => 2,
    'onkeypress'  => 'return validarn(event)',
    'disabled' => true    
  );


?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"proyectos/proyecto"
        } );
    }



</script>

<div class="content-wrapper">
	<section class="content-header">
    <?php echo $pagetitle; ?>
    <?php echo $breadcrumb; ?>		
  </section>
  <section class="content">
    <div class="box">
     <div class="box-header">
      <h3 class="header-title">Control de ingresos de material</h3>
    </div>						
    <div class="box-body">

      <form class="form-horizontal" name="formulario" id="formulario" role="form">
        <input type="hidden" name="idProyecto" id="idProyecto" value="<?php echo @$proyecto[0]->ID_PROYECTO; ?>">
        <input type="hidden" name="idMaterialRecibido" id="idMaterialRecibido" value="<?php echo $materialRecibidoId; ?>">
        <div class="form-group">
          <div class="col-sm-12">
            <label class="form-label col-sm-4">Nombre del proyecto :</label>
            <div class="col-sm-8">
              <?php echo form_textarea($nombreProyecto)?>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="form-label col-sm-4">Nombre del realizador :</label>
            <div class="col-sm-8">
              <?php echo form_textarea($nombreRealizador)?>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="form-label col-sm-4">Nombre del supervisor :</label>
            <div class="col-sm-8">
              <?php echo form_textarea($nombreSupervisor)?>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="form-label col-sm-4">Ordenes de compra :</label>
            <div class="col-sm-8">
              <select class="form-control" name="idOrden" id="idOrden" multiple="multiple[]">
                <option value="">Seleccione </option>
                <?php  
                if ($ordenes) {
                  if ($materiales) {
                    foreach ($materiales as $m) {
                      echo "<option value='".$m->ID_ORDEN_COMPRA."' selected='true'>".$m->CODIGO_ORDEN_COMPRA."</option>";
                    }
                  }
                  foreach ($ordenes as $oc ) {

                    echo "<option value='".$oc->ID_ORDEN_COMPRA."'>".$oc->CODIGO_ORDEN."</option>";
                  }
                } else {
                  echo "<option>No hay ordenes de compra disponibles</option>";
                }
                
                ?>
              </select>
            </div>
          </div>
        </div>            
      </form>        
      <div id="mensaje"></div>      
      <table id="materiales" class="table table-bordered table-striped">
       <thead>
        <th>Descripci&oacute;n del material</th>
        <th>Cantidad pedida</th>
        <th>Cantidad recibida</th>
        <th>Fecha</th>   
        <th></th>         
      </thead>
      <tbody id="bodyDetalle">
        <?php 
          if ($detalles) {
            foreach ($detalles as $d) {
              echo "<tr>";
              echo "<td>".$d->DESCRIPCION."</td>";
              echo "<td>".$d->CANTIDAD_TOTAL."</td>";
              echo "<td><input type='text' class='form-control' id='ID_DETALLE_".$d->ID_DETALLE_ORDEN_COMPRA."' value=></td>";
              echo "<td><input type='date' class='form-control' id='FECHA_RECIBIDA_".$d->ID_DETALLE_ORDEN_COMPRA."' value=></td>";
              if ($d->CANTIDAD_TOTAL>$d->CANTIDAD_RECIBIDA) 
              {
                
              
        ?>
            <td><span class='pull-right'><a href='#' onclick='modificar("<?php echo $d->ID_DETALLE_ORDEN_COMPRA;?>","<?php echo $d->CANTIDAD_TOTAL; ?>","<?php echo $d->ID_MATERIAL_RECIBIDO?>")'><i class='glyphicon glyphicon-pencil'></i></a></span></td>
         <?php
               } else {
                    echo "<td><span class='pull-right'><i class='glyphicon glyphicon-pencil'></i></span></td>";
               }
              echo "</tr>";
            }
          } else {
            # code...
          }
          
        ?>
      </tbody>
    </table>
    <?php 
      if ($historicos) {      
    ?>
    <h3 class="text-center">Historico de Entregas</h3>
    <table id="historico" class="table table-bordered table-striped" >
      <thead>
        <th>Material</th>
        <th>Cantidad recibida</th>
        <th>Fecha recibido</th>
      </thead>
      <tbody>
          <?php
          foreach ($historicos as $h) {
            echo "<tr>";
            echo "<td>".$h->DESCRIPCION."</td>";
            echo "<td>".$h->CANTIDAD_RECIBIDA."</td>";
            echo "<td>".date('d-m-Y',strtotime($h->FECHA_RECIBIDA))."</td>";
            echo "</tr>";
          }

          ?>
      </tbody>
    </table>

    <?php } ?>

  </div>

</div>
<div class="box-footer">
  <button type="button" class="btn btn-default" onclick="regresar()">Salir</button>
  <button type="submit" class="btn btn-primary pull-right" id="btnGuardar"  disabled="true"><span class="glyphicon glyphicon-saved" ></span> Agregar</button>  

</div>		
</section>

</div>

<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
    var materialRecibidoId="<?php echo $materialRecibidoId?>";
		$("#materiales").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
		});

    $("#historico").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
    });

    $("#idOrden").select2();
    if ($("#idOrden").val()!=null || $("#idOrden").val()!='') {
      $("#btnGuardar").prop('disabled',false);
    }
    $("#idOrden").on('change',function(e){
      var val=$(this).val();
      if (val != '') {
        var html='';
        var OrdenCompra = new Object();
        OrdenCompra.idOrden=val;
        OrdenCompra.idProyecto=$("input#idProyecto").val();
        //$("#modal_nuevo").modal('hide');

        $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");   
        var DatosJson=JSON.stringify(OrdenCompra);
        console.log(DatosJson);
        $.post(baseurl+'proyectos/proyecto_material_recibido/materialesRecibidos',{
          EntradaPost: DatosJson
        },function(data,textStatus){
          console.log(data);
          $("#materiales").DataTable().destroy();
          if (data.datos != null) {
            data.datos.forEach(function(l){
                  html+='<tr>'
                  html+='<td><input type="hidden" name="ID_DETALLE[]" id="ID_DETALLE_'+l.ID_DETALLE+'" value="'+l.ID_DETALLE+'"  />'+l.DESCRIPCION+'</td>';
                  html+='<td>'+l.CANTIDAD_TOTAL+'</td>';
                  html+='<td><input type="text" class="form-control" name="cantidadRecibida[]" id="cantidadRecibida_'+l.ID_DETALLE+'" /></td>';
                  html+='<td><input type="date" class="form-control" name="fechaRecibida[]" id="fechaRecibida_'+l.ID_DETALLE+'" /></td>';
                  html+='<td></td>';
                  html+='</tr>';
            });            
           // console.log(html);
            if (materialRecibidoId != null || materialRecibidoId!='') {
              $("#bodyDetalle").html(html);
            } else {
              $("#bodyDetalle").append(html);
            }
              $("#materiales").DataTable({
               paging      : true,
               lengthChange: true,
               searching   : true,
               ordering    : true,
               "order" : [],
               info        : true,
               autoWidth   : false,
               language: {
                search:'Buscar:',
                order: 'Mostrar Entradas',
                paginate: {
                  first:"Primero",
                  previous:"Anterior",
                  next:"Siguiente",
                  last:"Ultimo"
                },
                emptyTable: "No hay informacion disponible",
                infoEmpty: "Mostrando 0 de 0 de 0 entradas",
                lengthMenu:"Mostrar _MENU_ Entradas",
                info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
              }    
            });
            $("#mensaje").html('');
            $("#btnGuardar").prop('disabled',false);            
          }
        },"json").fail(function(response){
          $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
          console.log('Error: ' + response.responseText);

        });;     
      }
    });
    $("#btnGuardar").on('click',function(){
      $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");   
        var materialRecibido = new Object();
        materialRecibido.id='';
        materialRecibido.idProyecto=$("input#idProyecto").val();
        materialRecibido.idOrden=$("select#idOrden").val();
        materialRecibido.idDetalleOrden= new Array();
        materialRecibido.cantidadRecibida= new Array();
        materialRecibido.fechaRecibido= new Array();
        materialRecibido.idMaterial='';
        for (var i = 0; i < $("input[name='ID_DETALLE[]']").length; i++) {
           materialRecibido.idDetalleOrden.push($("input[name='ID_DETALLE[]']")[i].value);          
        }
        for (var i = 0; i < $("input[name='fechaRecibida[]']").length; i++) {
          materialRecibido.fechaRecibido.push($("input[name='fechaRecibida[]']")[i].value);
        }
        for (var i = 0; i < $('input[name="cantidadRecibida[]"]').length; i++) {
          materialRecibido.cantidadRecibida.push($('input[name="cantidadRecibida[]"]')[i].value);
        }

        if (materialRecibido.idDetalleOrden.length == 0 && 
          materialRecibido.cantidadRecibida.length == 0 
          && materialRecibido.fechaRecibido.length == 0 ) {
          $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Complete todos los campos</div>");
          return;
        }

        var DatosJson=JSON.stringify(materialRecibido);
        $.post(baseurl+'proyectos/proyecto_material_recibido/save',{
          MaterialPost: DatosJson
        },function(data,textStatus){
          console.log(data);
          $("#mensaje").html(data.response_msg);
        },"json").fail(function(response){
          $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
          console.log('Error: ' + response.responseText);

        });;     

    });


	});
    function modificar(idDetalleEntregado,cantidad,idMaterial)
    {
      $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");   
        var materialRecibido = new Object();
        materialRecibido.idDetalle=idDetalleEntregado;
        materialRecibido.idProyecto=$("input#idProyecto").val();
        materialRecibido.idOrden=$("select#idOrden").val();
        materialRecibido.cantidadRecibida= $("input#ID_DETALLE_"+idDetalleEntregado).val();
        materialRecibido.fechaRecibido= $("input#FECHA_RECIBIDA_"+idDetalleEntregado).val();
        materialRecibido.idMaterial= $("input#idMaterialRecibido").val();
        console.log(materialRecibido);
        if (materialRecibido.cantidadRecibida>cantidad) {
          $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se puede ingresar mas de lo que fue solicitado en la Orden de compra</div>");
          return;
        }

        var DatosJson=JSON.stringify(materialRecibido);
        $.post(baseurl+'proyectos/proyecto_material_recibido/save',{
          MaterialPost: DatosJson
        },function(data,textStatus){
          console.log(data);
          $("#mensaje").html(data.response_msg);
        },"json").fail(function(response){
          $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
          console.log('Error: ' + response.responseText);

        });;     

    }

 function regresar()
    {
      alertify.confirm("¿Desea Regresar?","Click en ACEPTAR para regresar",function(){
          window.location.href = baseurl+"proyectos/proyecto"

      },function(){

      }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }

</script>
