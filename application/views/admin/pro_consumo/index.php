<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    var idProyecto2= "<?php echo $idProyecto2;?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"proyecto/consumo"
        } );
    }

function eliminarPaciente(id,nombre){
	alertify.confirm("Eliminar paciente","¿Seguro de eliminar al paciente "+nombre+"?",function(){
    			 document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
    			 var paciente=new Object();
    			 paciente.id=id;
    			 var DatosJson=JSON.stringify(paciente);
    			 console.log(DatosJson);
    			 $.post(baseurl+'clinica/paciente/eliminar',{
    			 	PacientePost:DatosJson
    			 },function(data,textStatus){
	                $("#mensaje").html(data.response_msg);
    			 },"json").fail(function(response){
					$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                        console.log('Error: ' + response.responseText);
    			 });;
	},function(){

	});
}
</script>

<div class="content-wrapper">
	<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>				
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="header-title">Registro de consumo de materiales por proyecto</h3>
			</div>
			<div class="box-body" >
				<div id="mensaje"></div>
				<div class="row">
					<div class="form-group">
						<label class="form-label col-sm-3">proyecto que consume<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
						<div class="col-sm-3">
							<select name="idProyectoConsumo" id="idProyectoConsumo" style="width: 100%;">
								<option value="">Seleccione una opción</option>
							<?php
								if ($proyectos) {
									foreach ($proyectos as $proyecto) {
										if ($proyecto->ID_PROYECTO==$idProyecto2) {
											echo "<option value='".$proyecto->ID_PROYECTO."' selected='true'>".$proyecto->CODIGO_PROYECTO." - ".$proyecto->NOMBRE_PROYECTO."</option>";
										} else {
											echo "<option value='".$proyecto->ID_PROYECTO."'>".$proyecto->CODIGO_PROYECTO." - ".$proyecto->NOMBRE_PROYECTO."</option>";
										}
									}
								}

							?>
							</select>
						</div>
					</div>
				</div>
			</div>
        	<div class="row" id="botonConsumo" style="display: none;">
        		<div class="col-sm-2 pull-right">
                    <a href="consumo/nuevo?idProyecto2=<?php echo $idProyecto2;?>" ><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
        		</div>
        	</div>			
			<div class="box-body">

				<div style="display: none;" id="tablaConsumo">
					<table id="consumos" class="table table-bordered table-striped">
						<thead>
							<th>Proyecto</th>
							<th>Nombre realizador</th>
							<th>Nombre supervisor</th>
							<th>Fecha de creaci&oacute;n</th>
							<th></th>
						</thead>
						<tbody id="listadoConsumos">
							<?php 
								if ($consumos) {
									foreach ($consumos as $b) {
										$consumoId=base64_encode($b->ID_PROYECTO_CONSUMO);
										echo "<tr>";
										echo "<td>".$b->CODIGO_PROYECTO." ".$b->NOMBRE_PROYECTO."</td>";
										echo "<td>".$b->NOMBRE_REALIZADOR."</td>";
										echo "<td>".$b->NOMBRE_SUPERVISOR."</td>";
										echo "<td>".date('Y-m-d',strtotime($b->FECHA_CREACION))."</td>";
										echo "<td><a href='consumo/editar/".$consumoId."?idProyecto2=".$idProyecto2."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
							?>

							<?php 

								echo "</tr>";
									}
								} 
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
	
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

<script type="text/javascript">
	$(document).ready(function(){

		if (idProyecto2 != '0' && $("#idProyectoConsumo").val()!='') {
			$("#tablaConsumo").show();
			$("#botonConsumo").show();
		} else {
			$("#tablaConsumo").hide();
			$("#botonConsumo").hide();
		}		
		$("#consumos").DataTable({
			paging      : true,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			"order" : [],
			info        : true,
			autoWidth   : false,
			language: {
				search:'Buscar:',
				order: 'Mostrar Entradas',
				paginate: {
					first:"Primero",
					previous:"Anterior",
					next:"Siguiente",
					last:"Ultimo"
				},
				emptyTable: "No hay informacion disponible",
				infoEmpty: "Mostrando 0 de 0 de 0 entradas",
				lengthMenu:"Mostrar _MENU_ Entradas",
				info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
			}    
		});

		$("#idProyectoConsumo").select2({
			minimumInputLength: 0
		});

		$("#idProyectoConsumo").on('change',function(){
			var value=$(this).val();
			if (value != '' ) {
				var url= baseurl+"proyectos/consumo";
				window.location=url+'?idProyecto2='+value;
			}
		});


	});


</script>

</div>