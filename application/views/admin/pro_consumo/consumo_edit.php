<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 
	
	$supervisor= !empty(@$consumo[0]->NOMBRE_SUPERVISOR) ? @$consumo[0]->NOMBRE_SUPERVISOR : @$proyecto[0]->NOMBRE_SUPERVISOR;
	$realizador = !empty(@$consumo[0]->NOMBRE_REALIZADOR) ? @$consumo[0]->NOMBRE_REALIZADOR : @$proyecto[0]->NOMBRE_REALIZADOR;
	
	$nombreSupervisor= array(
		'name' 		=> 'nombreSupervisor',
		'id'		=> 'nombreSupervisor',
		'value'		=> set_value('nombreSupervisor',$supervisor),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre del supervisor',
		'onkeypress'	=> 'return validarn(event)'
	);

	$nombreRealizador = array(
		'name' 		=> 'nombreRealizador',
		'id'		=> 'nombreRealizador',
		'value'		=> set_value('nombreRealizador',$realizador),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre del realizador',
		'onkeypress'	=> 'return validarn(event)'
	);

?>
<div class="content-wrapper">
	<section class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Datos</h3>
			</div>
			<div class="box-body">
				<div id="mensaje"></div>
				<form class="form-horizontal" name="formulario" id="formulario" role="form" >
					<input type="hidden" id="idConsumo" name="idConsumo" value="<?php echo @$consumo[0]->ID_PROYECTO_CONSUMO?>">
					<input type="hidden" id="idProyecto" name="idProyecto" value="<?php echo @$proyecto[0]->ID_PROYECTO;?>">			
					<div class="form-group">
						<label for='idProyecto' class="form-label col-sm-2">Proyecto<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
						<div class="col-sm-3">
							<label><?php echo @$proyecto[0]->CODIGO_PROYECTO." ". @$proyecto[0]->NOMBRE_PROYECTO; ?></label>
						</div>
						<label for='nombreSupervisor' class="form-label col-sm-3">Nombre de supervisor <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
						<div class="col-sm-4">
							<?php echo form_input($nombreSupervisor);?>
						</div>
					</div>
					<div class="form-group">
						<label for="nombreRealizador" class="form-label col-sm-2">Nombre realizador <span style="color: #F20A06; font-size: 15px;">*</span>: </label>
						<div class="col-sm-4">
							<?php echo form_input($nombreRealizador);?>
						</div>
						<div class="col-sm-4">
							<button type="button" class="btn btn-primary pull-right" id="btnAgregar"><span class="glyphicon glyphicon-plus" ></span> Agregar Detalle</button>						
						</div>
					</div>	
					<div id="tblConsumo" style="display: none;">						
						<table id="consumo" class="table table-bordered table-striped">
							<thead>
								<th>Descripcion</th>
								<th>Cantidad Recibida</th>
								<th>Cantidad Consumida</th>
								<th>Fecha Recibida</th>
								<th></th>
							</thead>
							<tbody id="tableConsumoBody">
								<?php 
								if ($materiales) {
									foreach ($materiales as $m) {
										$total=(int)$m->CANTIDAD_RECIBIDA-(int)$m->TOTAL;
										echo "<tr>";
										echo "<td>".$m->DESCRIPCION."</td>";
										echo "<td>".$total."</td>";
										echo "<td><input class='form-control' type='number' min='1' name='cantidadConsumida' id='cantidadConsumida_".$m->ID_MATERIAL_RECIBIDO_DETALLE."'></td>";
										echo "<td><input class='form-control' type='date' name='fechaConsumida' id='fechaConsumida_".$m->ID_MATERIAL_RECIBIDO_DETALLE."'></td>";
										if ($total>0) {
										
										?>
										<td><span class='pull-right'><a href='#' onclick='modificar("<?php echo $m->ID_MATERIAL_RECIBIDO_DETALLE ?>","<?php echo $m->CANTIDAD_RECIBIDA?>")'><i class='glyphicon glyphicon-pencil'></i></a></span></td>						
										<?php
										} else {
											echo "<td><span class='pull-right'><i class='glyphicon glyphicon-pencil'></i></span></td>";
										}										
										echo "</tr>";

									}
								}
								?>
							</tbody>
						</table>					
					</div>
				</form>
				<?php  
					if ($detalles) {				

				?>
					<div class="col-sm-12" id="tblDetalle" >
						<h4 class="text-center">Historico de consumo</h4>
						<table id="historicos" class="table table-bordered table-striped">
							<thead>
								<th>Descripci&oacute;n</th>
								<th>Cantidad consumida</th>
								<th>Fecha consumido</th>
							</thead>
							<tbody id="tblDetalleBody">
								<?php 
									foreach ($detalles as $d) {
										echo "<tr>";
										echo "<td>".$d->DESCRIPCION."</td>";
										echo "<td>".$d->CANTIDAD_CONSUMIDA."</td>";
										echo "<td>".date('d-m-Y',strtotime($d->FECHA_CONSUMO))."</td>";
										echo "</tr>";
									}
								?>
							</tbody>
						</table>
					</div>

				<?php 
					} 
				?>				
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar" onclick="terminar()"><span class="glyphicon glyphicon-saved" ></span> Terminar</button>	
				
			</div>
			
		</div>
		
	</section>
	
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"proyectos/bitacora"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"proyectos/consumo"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }

    function terminar()
    {
    	alertify.confirm("¿Desea Terminar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"proyectos/consumo"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});    	
    }

    function modificar(idDetalle,cantidadRecibida) {
    	var consumoDetalle = new Object();
    	consumoDetalle.idProyectoConsumo=$("input#idConsumo").val();
    	consumoDetalle.idMaterialRecibido=idDetalle;
    	consumoDetalle.cantidadConsumida=$("#cantidadConsumida_"+idDetalle).val();
    	consumoDetalle.fechaConsumida=$("#fechaConsumida_"+idDetalle).val();
			$("#modal_nuevo").modal('hide');    	
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");	
			var DatosJson=JSON.stringify(consumoDetalle);
			console.log(DatosJson);
			$.post(baseurl+'proyectos/consumo/saveConsumoDetalle',{
				detallePost: DatosJson
			},function(data,textStatus){
				console.log(data);
				$("#mensaje").html(data.response_msg);
				$("#historicos").DataTable().destroy();
				var html=''
				data.historicos.forEach(function(l){
					html+='<tr>';
					html+='<td>'+l.DESCRIPCION+'</td>';
					html+='<td>'+l.CANTIDAD_CONSUMIDA+'</td>';
					html+='<td>'+l.FECHA_CONSUMO+'</td>';
					html+='</tr>';
				});
				$("#tblDetalleBody").html(html);
				$("#historicos").DataTable({
					paging      : true,
					lengthChange: true,
					searching   : true,
					ordering    : true,
					"order" : [],
					info        : true,
					autoWidth   : false,
					language: {
						search:'Buscar:',
						order: 'Mostrar Entradas',
						paginate: {
							first:"Primero",
							previous:"Anterior",
							next:"Siguiente",
							last:"Ultimo"
						},
						emptyTable: "No hay informacion disponible",
						infoEmpty: "Mostrando 0 de 0 de 0 entradas",
						lengthMenu:"Mostrar _MENU_ Entradas",
						info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
					}    
				});
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
				console.log('Error: ' + response.responseText);

			});;    		    	
    }

    $(document).ready(function(){

    	if ($("input#idConsumo").val()!='') {
    		$("#tblConsumo").show();
    	}
    
		$("#consumo").DataTable({
			paging      : true,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			"order" : [],
			info        : true,
			autoWidth   : false,
			language: {
				search:'Buscar:',
				order: 'Mostrar Entradas',
				paginate: {
					first:"Primero",
					previous:"Anterior",
					next:"Siguiente",
					last:"Ultimo"
				},
				emptyTable: "No hay informacion disponible",
				infoEmpty: "Mostrando 0 de 0 de 0 entradas",
				lengthMenu:"Mostrar _MENU_ Entradas",
				info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
			}    
		});
		$("#historicos").DataTable({
			paging      : true,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			"order" : [],
			info        : true,
			autoWidth   : false,
			language: {
				search:'Buscar:',
				order: 'Mostrar Entradas',
				paginate: {
					first:"Primero",
					previous:"Anterior",
					next:"Siguiente",
					last:"Ultimo"
				},
				emptyTable: "No hay informacion disponible",
				infoEmpty: "Mostrando 0 de 0 de 0 entradas",
				lengthMenu:"Mostrar _MENU_ Entradas",
				info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
			}    
		});
		$("#btnAgregar").click(function(){
			var consumo = new Object();
			consumo.id=$("input#idConsumo").val();
			consumo.idProyecto=$("input#idProyecto").val();
			consumo.nombreRealizador=$("input#nombreRealizador").val();
			consumo.nombreSupervisor=$("input#nombreSupervisor").val();    	
			$("#modal_nuevo").modal('hide');    	
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");	
			var DatosJson=JSON.stringify(consumo);
			console.log(DatosJson);
			$.post(baseurl+'proyectos/consumo/saveConsumo',{
				consumoPost: DatosJson
			},function(data,textStatus){
				console.log(data);
				$("#mensaje").html(data.response_msg);
				$("#idConsumo").val(data.idConsumo);
				$("#tblConsumo").show();
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
				console.log('Error: ' + response.responseText);

			});;    		

		});

    })
</script>

<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>