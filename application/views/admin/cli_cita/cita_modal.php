<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php
	$asunto=array(
		'name' 		=> 'asunto',
		'id'		=> 'asunto',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Asunto de la cita'
	);

	$fecha_hora=array(
		'name' 		=> 'fecha_hora',
		'id'		=> 'fecha_hora',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => ''
	);	

	$estados = array(
	  'En Sala De Espera' => 'En sala de espera',
	);	

?>


<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
	</div>
	<div class="modal-body">
		<div id="mensaje_cita"></div>
		<form class="form-horizontal" name="formulario" id="formulario" role="form">			
						<div class="form-group">
							<label for='pacientes' class="form-label col-sm-2">Pacientes:</label>
							<div class="col-sm-8">
								<select id="listPaciente" class="form-control js-example-basic-single " style="width: 80%;">
									<option value="0">Seleccione un Paciente</option>
									<?php  
										if ($pacientes) {
											foreach ($pacientes as $p) {
												echo "<option value='".$p->ID_PACIENTE."'>".$p->nombre_completo."</option>";
											}
										}
									?>						
								</select>
							</div>
			                <div class="col-sm-2 ">
			                    <a href="paciente" align="right" ><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
			                </div>
						</div>
			            <div class="form-group">
					        <!--<label for="asunto" class="col-sm-2 control-label">Asunto:</label>
					        <div class="col-sm-4">
					            <?php
					              echo form_input($asunto);
					            ?>
					          </div>    -->        	
					        <label for="estado" class="col-sm-1 control-label">Estado:</label>
					        <div class="col-sm-2">
					            <?php
					              echo form_dropdown('estado',$estados,'','class="form-control" id="estado"')
					            ?>
					          </div>
					        <label for="medicos" class="col-sm-2 control-label">Medico:</label> 
					        <div class="col-sm-5">
								<select id="listMedicos" class="form-control js-example-basic-single " style="width: 80%;">
									<option value="0">Seleccione un Medico</option>
									<?php  
										if ($medicos) {
											foreach ($medicos as $p) {
												echo "<option value='".$p->ID_MEDICO."'>".$p->nombre_completo."</option>";
											}
										}
									?>						
								</select>		        	
					        </div>      					          
			                <div class="col-sm-2">
			                    <button type="button" class="btn btn-primary" id="btnAgregar"><span class="glyphicon glyphicon-plus"></span> Asignar</button>                				
			                </div>
			        	</div>
			        	
			        	<div class="form-group">
					        <!--<label for="fecha_hora" class="col-sm-2 control-label">Fecha:</label>
					        <div class="col-sm-4">
			                <div class='input-group date' id='datetimepicker1'>
					            <?php
					              echo form_input($fecha_hora);
					            ?>                  
			                    <span class="input-group-addon">
			                        <span class="fa fa-clock-o"></span>
			                    </span>
			                </div>
					        </div>-->
 		
			        	</div>		
		</form>	
		<table class="table table-bordered table-striped">
			<thead>
				<th>Doctor</th>
				<th>Paciente</th>
				<th></th>
				<th style="display: none;">Idcita</th>
			</thead>
			<tbody id="TableBodycitas">
				
			</tbody>
		</table>	
	</div>	
	<div class="modal-footer">
		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
		<button type="button" class="btn btn-primary" id="btnGuardar" onclick="salir()"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>			
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonCita.js');?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		jQuery('#fecha_hora').datetimepicker();
		$("#listPaciente").select2();
		$("#listMedicos").select2();
		$("#listEstudios").select2();



	});
    function salir()
    {
    	alertify.confirm("¿Desea Regresar?","Asegurese de que las citas han sido completadas ya que la vista se refrescara",function(){
    		  window.location.href = baseurl+"clinica/cita"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }	
    function eliminarfila()
    {
        document.getElementById("tablebodyEstudios").deleteRow(0);
    }    	
</script>
