<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>



<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>

<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"clinica/cita"
        } );
    }

</script>
<style type="text/css">
  .fc-content{
    padding: 5px;
  }
.fc .fc-row .fc-content-skeleton table, .fc .fc-row .fc-content-skeleton td, .fc .fc-row .fc-mirror-skeleton td{
    padding: 5px;
  }
</style>
<div class="content-wrapper">
	<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>				
	</section>
	<section class="content">
		<div class="box">
      <div id="mensaje"></div>
			<div class="box-header">
       <a href="cita/nuevo" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal">
        <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Asignar pacientes</button></a>
       
   </a>				
			</div>
			<div class="box-body">
				<div class="col-sm-12">
          <div id="calendar">
            
          </div>
        </div>
			</div>
		</div>		
	</section>
</div>
<script type="text/javascript">
  
document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: [ 'interaction','dayGrid', 'timeGrid', 'list','timeGridPlugin' ],
          header: {
               left: 'prev,next today',
               center: 'title',
               right: 'dayGridMonth,timeGridMonth,listMonth'
          },           
          locale: 'es',   
          defaultView: '',

          navLinks: true, // can click day/week names to navigate views
          editable: true,
          eventLimit: true, // allow "more" link when too many events
          events: baseurl+'clinica/cita/obtenerCitas',
          eventClick:function(info){
              var id=btoa(info.event.id);
              window.location.href=baseurl+'clinica/cita/editar/'+id;
              console.log(id);
             info.el.style.borderColor = 'red';
          } 
         /*eventRender: function(info) {
            var tooltip = new Tooltip(info.el, {
              title: info.event.extendedProps.description,
              placement: 'top',
              trigger: 'hover',
              container: 'body'
            });
          }*/                     
                  
        });

        calendar.setOption('locale', 'es');

        calendar.render();
      });

</script>

<div class="modal fade text-center" id="modal_nuevo" >
  <div class="modal-dialog" style="width: 80%;">
    <div class="modal-content">
    </div>
  </div>
</div>