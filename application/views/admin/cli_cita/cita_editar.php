<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php
	$asunto=array(
		'name' 		=> 'asunto',
		'id'		=> 'asunto',
		'value'		=> set_value('asunto',@$cita[0]->ASUNTO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Asunto de la cita'
	);

	$fecha_hora=array(
		'name' 		=> 'fecha_hora',
		'id'		=> 'fecha_hora',
		'value'		=> set_value('fecha_hora',date("Y/m/d H:i:s",strtotime(@$cita[0]->FECHA_HORA))),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => ''
	);	

	$estados = array(
	  '' => 'Seleccionar',
	  'En Sala De Espera' => 'En sala de espera',
	  'En Consulta' => 'En consulta',
	  'Atendida' => 'Atendida',
	  'Cancelada' => 'Cancelada',
	);	

?>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<input type="hidden" name="id" id="id" value="<?php echo @$cita[0]->ID_CITA?>">
<div class="content-wrapper">
	<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>					
	</section>
	<section class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="headet-title">Datos</h3>
			</div>
			<div class="box-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">			
	
			<div class="form-group">
				<label for='pacientes' class="form-label col-sm-2">Pacientes:</label>
				<div class="col-sm-8">
					<select id="listPaciente" class="form-control js-example-basic-single " style="width: 80%;">
						<option value="0">Seleccione un Paciente</option>
						<?php  
							if ($pacientes) {
								foreach ($pacientes as $p) {
									if ($p->ID_PACIENTE==@$cita[0]->ID_PACIENTE) {
										echo "<option value='".$p->ID_PACIENTE."' selected=''>".$p->nombre_completo."</option>";
									} else {
										echo "<option value='".$p->ID_PACIENTE."'>".$p->nombre_completo."</option>";
									}
									
								}
							}
						?>						
					</select>
				</div>
			</div>
			<div class="form-group">
					        <label for="asunto" class="col-sm-2 control-label">Asunto:</label>
					        <div class="col-sm-4">
					            <?php
					              echo form_input($asunto);
					            ?>
					          </div>            	
					        <label for="estado" class="col-sm-2 control-label">Estado:</label>
					        <div class="col-sm-4">
					            <?php
					              echo form_dropdown('estado',$estados,@$cita[0]->ESTADO,'class="form-control" id="estado"')
					            ?>
					          </div>
			</div>
			<div class="form-group">
					        <label for="fecha_hora" class="col-sm-2 control-label">Fecha:</label>
					        <div class="col-sm-4">
			                <div class='input-group date' id='datetimepicker1'>
					            <?php
					              echo form_input($fecha_hora);
					            ?>                  
			                    <span class="input-group-addon">
			                        <span class="fa fa-clock-o"></span>
			                    </span>
			                </div>
					        </div>
					        <label for="medicos" class="col-sm-2 control-label">Medico:</label> 
					        <div class="col-sm-4">
								<select id="listMedicos" class="form-control js-example-basic-single " style="width: 80%;">
									<option value="0">Seleccione un Medico</option>
									<?php  
										if ($medicos) {
											foreach ($medicos as $p) {
												if ($p->ID_MEDICO==@$cita[0]->ID_MEDICO) {
													echo "<option value='".$p->ID_MEDICO."' selected=''>".$p->nombre_completo."</option>";
												} else {
													echo "<option value='".$p->ID_MEDICO."'>".$p->nombre_completo."</option>";
												}
												
											}
										}
									?>						
								</select>		        	
					        </div>       		
			</div>					
			

			</form>		
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
					<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>					
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"clinica/cita"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"clinica/cita"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
</script>
<script type="text/javascript">
	$(document).ready(function(){
		jQuery('#fecha_hora').datetimepicker();
		$("#listPaciente").select2();
		$("#listMedicos").select2();
		$("#listEstudios").select2();

	});
function eliminarfilaDatos(id,nombre)
{
	alertify.confirm("Eliminar Estudio","¿Seguro de eliminar el estudio "+nombre+"?",function(){
             document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
             var estudio=new Object();
             estudio.id=id;
             var DatosJson=JSON.stringify(estudio);
             $.post(baseurl+'clinica/cita/eliminarEstudio',{
             	EstudioCita:DatosJson
             },function(data,textStatus){
             	$("#mensaje").html(data.response_msg);
				document.getElementById("tablebodyEstudios").deleteRow(0);
             },"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);

             });;
	},function(){

	});
}
    function eliminarfila()
    {
        document.getElementById("tablebodyEstudios").deleteRow(0);
    }    	
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsonCita.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

											if ($p->ID_MEDICO==@$cita[0]->ID_MEDICO) {
													echo "<option value='".$p->ID_MEDICO."' selected=''>".$p->nombre_completo."</option>";
												} else {
													echo "<option value='".$p->ID_MEDICO."'>".$p->nombre_completo."</option>";
												}
												
											}