<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 
	$no_jvpm= array(
		'name' 		=> 'jvpm',
		'id'		=> 'jvpm',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'No JVPM'

	);
	

 ?>

 <div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
	</div>
	<div class="modal-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div class="form-group ">
				<label for='primer_nombre' class="form-label col-sm-2">Empleado:</label>
				<div class="col-sm-4">
					<select id="listEmpleado" class="form-control js-example-basic-single " style="width: 80%;">
						<option value="0">Seleccione un Empleado</option>
						<?php  
							if ($empleados) {
								foreach ($empleados as $e) {
									echo "<option value='".$e->ID_EMPLEADO."'>".$e->CODIGO_EMPLEADO." ".$e->nombre_completo."</option>";
								}
							}
						?>						
					</select>
				</div>
				<label for='segundo_nombre' class="form-label col-sm-2">Especialidad:</label>
				<div class="col-sm-4">
					<select id="listEspecialidad" class="form-control js-example-basic-single" style="width: 80%;">
						<option value="0">Seleccione una Especialidad</option>
						<?php 
							if ($especialidades) {
								foreach ($especialidades as $es) {
									echo "<option value='".$es->ID_CAT_ESPECIALIDAD."'>".$es->DESCRIPCION."</option>";
								}
							}
						 ?>
					</select>
				</div>				
			</div>
			<div class="form-group ">
				<label for='jvpm' class="form-label col-sm-2">No JVPM:</label>
				<div class="col-sm-3">
					<?php echo form_input($no_jvpm);?>
				</div>				
			</div>
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
		<button type="submit" class="btn btn-primary" id="btnGuardar" data-dismiss="modal"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
	</div>	
 </div>
 <script type="text/javascript" src="<?php echo base_url('js/JsonMedico.js');?>"></script>
 <script type="text/javascript">
 	
 	$(document).ready(function(){
 		$("#listEmpleado").select2();
 		$("#listEspecialidad").select2();
 	});
 </script>