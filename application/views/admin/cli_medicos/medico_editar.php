<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 


	$no_jvpm= array(
		'name' 		=> 'jvpm',
		'id'		=> 'jvpm',
		'value'		=> set_value('jvpm',@$medico[0]->NO_JVPM),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'No JVPM'		

	);
	

 ?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url($frameworks_dir.'/bootstrap/css/bootstrap.min.css');?>">
  <script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
 <script src="<?php echo base_url();?>assets/frameworks/bootstrap/dist/js/bootstrap.min.js"></script>             
<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<input type="hidden" name="id" id="id" value="<?php echo @$medico[0]->ID_MEDICO?>">
 <div class="content-wrapper">
	<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="header-title">Datos</h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" name="formulario" id="formulario" role="form">
					<div class="form-group ">
						<label for='primer_nombre' class="form-label col-sm-2">Empleado:</label>
						<div class="col-sm-4">
							<select id="listEmpleado" class="form-control js-example-basic-single " style="width: 80%;">
								<option value="0">Seleccione un Empleado</option>
								<?php  
									if ($empleados) {
										foreach ($empleados as $e) {
											if ($e->ID_EMPLEADO==@$medico[0]->ID_EMPLEADO) {
												echo "<option value='".$e->ID_EMPLEADO."' selected>".$e->nombre_completo."</option>";
											} else {
												echo "<option value='".$e->ID_EMPLEADO."'>".$e->nombre_completo."</option>";
											}
											
										}
									}
								?>						
							</select>
						</div>
						<label for='segundo_nombre' class="form-label col-sm-2">Especialidad:</label>
						<div class="col-sm-4">
							<select id="listEspecialidad" class="form-control js-example-basic-single" style="width: 80%;">
								<option value="0">Seleccione una Especialidad</option>
								<?php 
									if ($especialidades) {
										foreach ($especialidades as $es) {
											if ($es->ID_CAT_ESPECIALIDAD==@$medico[0]->ID_CAT_ESPECIALIDAD) {
												echo "<option value='".$es->ID_CAT_ESPECIALIDAD."' selected>".$es->DESCRIPCION."</option>";
											} else {
												echo "<option value='".$es->ID_CAT_ESPECIALIDAD."'>".$es->DESCRIPCION."</option>";
											}
											
										}
									}
								 ?>
							</select>
						</div>				
					</div>
					<div class="form-group ">
						<label for='jvpm' class="form-label col-sm-2">No JVPM:</label>
						<div class="col-sm-3">
							<?php echo form_input($no_jvpm);?>
						</div>				
					</div>
				</form>				
			</div>
		<div class="box-footer">
				<button type="button" class="btn btn-default" onclick="regresar();">Cerrar</button>
				<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	
		</div>			
		</div>

	</section> 	
 </div>
  <script type="text/javascript" src="<?php echo base_url('js/JsonMedico.js');?>"></script>
 <script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"clinica/medico"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"clinica/medico"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }  	
 	$(document).ready(function(){
 		$("#listEmpleado").select2();
 		$("#listEspecialidad").select2();
 	});
 </script>