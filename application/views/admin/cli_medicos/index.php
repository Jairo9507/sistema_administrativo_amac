<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url($frameworks_dir.'/bootstrap/css/bootstrap.min.css');?>">
  <script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
 <script src="<?php echo base_url();?>assets/frameworks/bootstrap/dist/js/bootstrap.min.js"></script>             
<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"clinica/medico"
        } );
    }
    function eliminarMedico(id,nombre)
    {
        alertify.confirm("Eliminar Medico","¿Seguro de eliminar al medico "+nombre+"?",function(){
           document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
           var medico=new Object();
           medico.id=id;
           var DatosJson=JSON.stringify(medico);
           $.post(baseurl+'clinica/medico/eliminar',{
            MedicoPost:DatosJson
           },function(data, textS){
                $("#mensaje").html(data.response_msg);
           },"json").fail(function(response){
                $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
           });

        },function(){

        });
    }
</script>
<div class="content-wrapper">
	<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="header-title">Listado de Medicos</h3>
			</div>
                	<div class="row">
                		<div class="col-sm-2 pull-right">
                            <a href="medico/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                		</div>
                	</div>			
            <div class="box-body">
            	<div id="mensaje"></div>
            	<table id="medicos" class="table table-bordered table-striped">
            		<thead>
            			<th>Nombre</th>
            			<th>Codigo</th>
            			<th>Genero</th>
            			<th>Especialidad</th>
            			<th>No JVPM</th>
            			<th></th>
            		</thead>
            		<?php 
            			if ($medicos) {
            				foreach ($medicos as $m) {
            					$medicoId=base64_encode($m->ID_MEDICO);
            					echo "<tr>";
            					echo "<td>".$m->nombre_completo."</td>";
            					echo "<td>".$m->CODIGO_EMPLEADO."</td>";
            					echo "<td>".$m->SEXO."</td>";
                                echo "<td>".$m->DESCRIPCION."</td>";
            					echo "<td>".$m->NO_JVPM."</td>";
            					echo "<td><a href='medico/editar/".$medicoId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";

            		 ?>
                				<button type="button"  title="Eliminar Medico" onclick="eliminarMedico('<?php echo $medicoId?>','<?php echo $m->nombre_completo ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
            		 <?php 
            		 			echo "</td>";
            					echo "</tr>";
            				}
            			}

            		  ?>
            	</table>
            </div>

		</div>
	</section>
</div>

<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

<script type="text/javascript">
	$(document).ready(function(){

		$("#medicos").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
		});
	});
</script>

<div class="modal fade text-center" id="modal_nuevo" >
  <div class="modal-dialog" style="width: 80%;">
    <div class="modal-content">
    </div>
  </div>
</div>