<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 
	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion',
		'onkeypress'	=> 'return validarn(event)'

	);
?>

<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>		
	</div>
	<div class="modal-body">
		
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div class="form-group">
				<label for='descripcion' class="form-label col-sm-2">Descripcion:</label>
				<div class="col-sm-10">
					<?php echo form_textarea($descripcion);?>
				</div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsonUnidadMedida.js');?>"></script>