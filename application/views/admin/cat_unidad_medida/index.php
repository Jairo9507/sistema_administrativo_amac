<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"catalogos/cat_unidad_medida"
        } );
    }

    function eliminarUnidad(idunidad,nombre)
    {
    	alertify.confirm("Eliminar Unidad de medida","¿Seguro de eliminar la unidad "+nombre+"?",function(){
             document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
             var Unidad=new Object();
             Unidad.id=idunidad;
             var DatosJson=JSON.stringify(Unidad);
             $.post(baseurl+'/catalogos/cat_unidad_medida/eliminar',{
             	UnidadPost: DatosJson
             },function(data,textStatus){
             	console.log(data);
                $("#mensaje").html(data.response_msg);             	
             },"json").fail(function(response){
                $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
             });;
              		
    	},function(){

    	});
    }
</script>
<div class="content-wrapper">
		<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>			
		</section>
		<section class="content">
				<div class="box">
					<div class="box-header">
							<h3 class="content-title">Lista de unidades</h3>
					</div>
						<div class="row">
							<div class="col-sm-2 pull-right">
								<a href="cat_unidad_medida/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>
							</div>
						</div>
					<div class="box-body">
						<div id="mensaje"></div>
						<table id="unidades" class="table table-bordered table-striped">
							<thead>
								<th>Nombre</th>
								<th>Estado</th>
								<th></th>
							</thead>
							<tbody>
								<?php 
									if ($unidades) {
										foreach ($unidades as $u) {
											$unidadId=base64_encode($u->id_cat_unidad_medida);
											echo "<tr>";
											echo "<td>".$u->descripcion."</td>";
											if ($u->ESTADO==1) {
												echo "<td>ACTIVO</td>";
											} 
											echo "<td><a href='cat_unidad_medida/editar/".$unidadId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
								?>
									<button type="button"  title="Eliminar Unidad" onclick="eliminarUnidad('<?php echo $unidadId?>','<?php echo $u->descripcion?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
								<?php 
											echo "</td";
											echo "</tr>";
										}
									}

								?>
							</tbody>
						</table>
					</div>
				</div>
		</section>
</div>
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#unidades").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
		});		
	});
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>
<div class="modal fade text-center" id="modal_nuevo">
  <div class="modal-dialog" >
    <div class="modal-content">
    </div>
  </div>
</div>