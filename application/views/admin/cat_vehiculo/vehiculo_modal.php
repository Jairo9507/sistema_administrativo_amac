<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
 <?php

	$cod_vehiculo=array(
		'name' 		=> 'cod_vehiculo',
		'id'		=> 'cod_vehiculo',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'style'		=> 'width:90%; position:relative; left: 19px; ',
		'class'		=> 'form-control',
		'placeholder' => ''		
	);

	$placa=array(
		'name' 		=> 'placa',
		'id'		=> 'placa',
		'value'		=> set_value('placa',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:90%; position:relative; left: 19px; ',
		'placeholder' => 'P000000-2000',
		' maxlength'  => '7'
			
	);

	$color =array(
		'name' 		=> 'color',
		'id'		=> 'color',
		'value'		=> set_value('codigo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:90%; position:relative; left: 19px; ',
		'onkeypress'	=> 'return validarn(event)'		
	);

	$anio=array(
		'name' 		 => 'anio',
		'id'		 => 'anio',
		'value'		 => set_value('maximo',''),
		'type'		 => 'text',
		'class'		 => 'form-control',
		'style'		 => 'width:90%; position:relative; left: 19px;',
		'maxlength'  => '4',
		//'placeholder' => '',
		'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$capacidad_tanque=array(
		'name' 		=> 'capacidad_tanque',
		'id'		=> 'capacidad_tanque',
		'value'		=> set_value('capacidad_tanque',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:90%; position:relative; left: 19px;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$capacidad_peso=array(
		'name' 		=> 'capacidad_peso',
		'id'		=> 'capacidad_peso',
		'value'		=> set_value('capacidad_peso',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:90%; position:relative; left: 19px;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$asientos=array(
		'name' 		=> 'asientos',
		'id'		=> 'asientos',
		'value'		=> set_value('asientos',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:90%; position:relative; left: 19px;',
		//'placeholder' => '',
		'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$tonelaje=array(
		'name' 		=> 'clasificacion',
		'id'		=> 'clasificacion',
		'value'		=> set_value('tonelaje',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:90%; position:relative; left: 19px;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$peso=array(
		'name' 		=> 'peso',
		'id'		=> 'peso',
		'value'		=> set_value('peso',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:90%; position:relative; left: 19px;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$nmotor=array(
		'name' 		=> 'nmotor',
		'id'		=> 'nmotor',
		'value'		=> set_value('nmotor',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:90%; position:relative; left: 19px;',
		'maxlength'  => '12'
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$nchasis=array(
		'name' 		=> 'nchasis',
		'id'		=> 'nchasis',
		'value'		=> set_value('nchasis',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:90%; position:relative; left: 19px;',
		'maxlength'  => '17'
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$nvin=array(
		'name' 		=> 'nvin',
		'id'		=> 'nvin',
		'value'		=> set_value('nvin',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:90%; position:relative; left: 19px;',
		'maxlength'  => '17'
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
?> 

<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>
	</div>
	<div class="modal-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div id="mensaje2"></div>
			<div class="form-group">
				<label for="cod_vehiculo" class="col-sm-2 form-label">No. de vehiculo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					 <?php echo form_input($cod_vehiculo);?>
				</div>
				<label for="placa" class="col-sm-2 form-label">Placa<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					 <?php echo form_input($placa);?>
				</div>				
			</div>
			<div class="form-group">
				<label for="clase" class="col-sm-2 form-label">Clase de vehículo<span style="color: #F20A06; font-size: 15px;">*</span>: </label>
				<div class="col-sm-4">
				 	<select class="form-control" id="clase" style="width: 90%">
						<option value="0">Seleccione clase de vehiculo</option>
						 <?php
							if($clase){
								foreach ($clase as $c) {
									echo "<option value='".$c->ID_CLASE_VEHICULO."'>".$c->DESCRIPCION."</option>";
								}
							}
						?> 
					</select>
				</div><br>
				<label for="marca" class="col-sm-2 form-label">Tipo de vehículo<span style="color: #F20A06; font-size: 15px;">*</span>: </label>
				<div class="col-sm-4">
					<select class="form-control" id="tipo" style="width: 90% !important" disabled="true">
					</select>
				 	
				</div>
			</div>
			<div class="form-group">
				<label for="modelo" class="col-sm-2 form-label">Marca vehículo<span style="color: #F20A06; font-size: 15px;">*</span>: </label>
				<div class="col-sm-4">
					<select class="form-control" id="marca" style="width: 90%">
						<option value="0">Seleccione marca de vehiculo</option>
						 <?php
							if($marca){
								foreach ($marca as $m) {
									echo "<option value='".$m->ID_VEHICULO_MARCA."'>".$m->DESCRIPCION."</option>";
								}
							}
						?> 
					</select>
				</div>	
				<label for="tipo" class="col-sm-2 form-label">Modelo de vehículo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select class="form-control" id="modelo" style="width: 90% !important" disabled="true">
					</select>
				</div>	
			</div>
			<div class="form-group">		
				<label for="color" class="col-sm-2 form-label">Color<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($color);?>
				</div>
					<label for="anio" class="col-sm-2 form-label ">Año<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($anio);?>
				</div>
			</div>				
		
			<div class="form-group ">
				<label for="cuenta_id" class="col-sm-2 form-label">Clasificaci&oacute;n:</label>
				<div class="col-sm-4">
					<?php echo form_input($tonelaje);?>
				</div>		
				<label for="nmotor" class="col-sm-2 form-label">N° Motor<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($nmotor);?>
				</div>
						
<!--				<label for="asientos" class="col-sm-2 form-label">Número de asientos:</label>
				<div class="col-sm-4">
					<?php echo form_input($asientos);?>
				</div>	-->
			</div>											
<!--			<div class="form-group">
				<label for="capacidad_tanque" class="col-sm-2 form-label">Capacidad tanque:</label>
				<div class="col-sm-4">
					<?php echo form_input($capacidad_tanque);?>
				</div>
				<label for="capacidad_tanque" class="col-sm-2 form-label">Capacidad peso:</label>
				<div class="col-sm-4">
					<?php echo form_input($capacidad_peso);?>
				</div>					

			</div>	-->
			<!--<div class="form-group">
				<label for="cuenta_id" class="col-sm-2 form-label">Peso vehículo:</label>
				<div class="col-sm-4">
					<?php echo form_input($peso);?>
				</div>		
			</div>-->
			<div class="form-group">
				<label for="nchasis" class="col-sm-2 form-label">N° Chasis<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($nchasis);?>
				</div>	
				<label for="nvin" class="col-sm-2 form-label">N° VIN<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($nvin);?>
				</div>	
			</div>
			<div class="form-group">
				<label for="combustible" class="col-sm-2 form-label">Tipo de combustible<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select class="form-control" id="combustible" style="width: 90%">
						<option value="0">Seleccione tipo de combustible</option>
						<?php
						if($combustible){
							foreach ($combustible as $co) {
								echo "<option value='".$co->ID_COMBUSTIBLE."'>".$co->DESCRIPCION."</option>";
							}
						}
						?> 
					</select>
				</div>	
				<label for="unidad" class="col-sm-2 form-label">Departamento<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select class="form-control" id="unidad" style="width: 90%">
						<option value="0">Seleccione Departamento correspondiente</option>
						<?php
						if($unidad){
							foreach ($unidad as $uni) {
								echo "<option value='".$uni->ID_UNIDAD."'>".$uni->DESCRIPCION."</option>";
							}
						}
						?> 
					</select>
				</div>	
			</div>

		</form>										
	</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>		
	</div>
</div>	
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
    $(document).ready(function(){
        $("#clase").select2({
             dropdownParent: $("#modal_nuevo")
        });
        $("#marca").select2({
             dropdownParent: $("#modal_nuevo")
        });
        $("#modelo").select2({
             dropdownParent: $("#modal_nuevo")
        });
        $("#tipo").select2({
             dropdownParent: $("#modal_nuevo")
        });
        $("#combustible").select2({
             dropdownParent: $("#modal_nuevo")
        });
        $("#unidad").select2({
             dropdownParent: $("#modal_nuevo")
        });

        $("select#clase").change(function() {
        	$("#tipo").attr("disabled",true);
        	var option='';
        	var clase = new Object();
        	clase.id = $("select#clase").val();
        	console.log(clase);
        	var DatosJson = JSON.stringify(clase);

        	$.post(baseurl+'catalogos/cat_vehiculo/obtenerTipoId',{
        		clasePost : DatosJson
        	 }, function (data, textStatus) {
        	 	option+='<option value="">Seleccione tipo Vehículo</option>';
        	 	console.log(data);
        	 	data.tipos.forEach(function(element) {
        	 		option+="<option value='"+element.ID_TIPO_VEHICULO+"'>"+element.DESCRIPCION+"</option>"
        	 	});
        	 $("#tipo").attr("disabled",false);
        	 $("#tipo").html(option);
        	 }, "json").fail(function (response) {
        	 	 $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                             console.log('Error: ' + response.responseText);
        	 });
        });

        $("select#marca").change(function() {
        	$("#modelo").attr("disabled",true);
        	var option='';
        	var marca = new Object();
        	marca.id = $("select#marca").val();
        	console.log(marca);
        	var DatosJson = JSON.stringify(marca);

        	$.post(baseurl+'catalogos/cat_vehiculo/obtenerModeloId',{
        		marcaPost : DatosJson
        	 }, function (data, textStatus) {
        	 	option+='<option value="">Seleccione una marca de Vehículo</option>';
        	 	console.log(data);
        	 	data.modelos.forEach(function(element) {
        	 		option+="<option value='"+element.ID_VEHICULO_MODELO+"'>"+element.DESCRIPCION+"</option>"
        	 	});
        	 $("#modelo").attr("disabled",false);
        	 $("#modelo").html(option);
        	 }, "json").fail(function (response) {
        	 	 $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                             console.log('Error: ' + response.responseText);
        	 });
        });

      	$("#placa", "#formulario").keypress(function(e) {
      		var key =  ekeyCode || 0;
      		console.log(key);
      		$placa = $(this);

      		if (key >=48 && key <=57) {
      			if ($placa.val().length ===6) {
      				$placa.val($placa.val()+'-2011')
      			}
      		}else{
      			e.preventDefault();
      		}

      		return (key === 8 ||
	                key === 9 ||
	                key === 46 ||
	                (key >= 48 && key <= 57) ||
	                (key >= 96 && key <= 105));
      	});

      	$("#btnGuardar").click(function() {
      		var Vehiculo = new Object();
      		Vehiculo.id 			= '';
      		Vehiculo.codigo 		= $("input#cod_vehiculo").val();
      		Vehiculo.placa 			= $("input#placa").val().replace("-","");
      		Vehiculo.clase 			= $("select#clase").val();
      		Vehiculo.tipo 			= $("select#tipo").val();
      		Vehiculo.marca 			= $("select#marca").val();
      		Vehiculo.modelo 		= $("select#modelo").val();
      		Vehiculo.color 			= $("input#color").val();
      		Vehiculo.anio 			= $("input#anio").val();
      		Vehiculo.asientos 		= '';//$("input#asientos").val();
      		Vehiculo.capacidad_tanque = '';//$("input#capacidad_tanque").val();
      		Vehiculo.capacidad_peso	 = '';//$("input#capacidad_peso").val();
      		Vehiculo.clasificacion 		= $("input#clasificacion").val();
      		Vehiculo.peso 			= '';//$("input#peso").val();
      		Vehiculo.nmotor 		= $("input#nmotor").val();
      		Vehiculo.nchasis 		= $("input#nchasis").val();
      		Vehiculo.nvin 			= $("input#nvin").val();
      		Vehiculo.combustible 	= $("select#combustible").val();
      		Vehiculo.unidad 		= $("select#unidad").val();

      		console.log(Vehiculo);

      		// if (Vehiculo.id===undefined) {
      		// 	Vehiculo.id='';
      		// }
      		// else 
      		if (Vehiculo.codigo =="" || Vehiculo.placa=="" || Vehiculo.clase==0 || Vehiculo.clase==0 || Vehiculo.tipo==0 || Vehiculo.marca==0 || Vehiculo.modelo==0 || Vehiculo.color===undefined || Vehiculo.anio===undefined || Vehiculo.nmotor===undefined || Vehiculo.nchasis===undefined || Vehiculo.nvin===undefined || Vehiculo.combustible==0 || Vehiculo.unidad==0) {
      			$("#mensaje2").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese campos obligarios</div>");
      		}else{
      		$("#modal_nuevo").modal('hide');
      		$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
      		var DatosJson = JSON.stringify(Vehiculo);
      		$.post(baseurl+'catalogos/cat_vehiculo/guardar',{
      			vehiculoPost : DatosJson
      		}, function(data, textStatus) {
      			$("#mensaje").html(data.response_msg);
      			location.reload();
      		},"json").fail(function(response) {
      			$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Placa ya registrada</div>");
      			console.log('Error: ' + response.responseText);
      		});;
      		return false;
      	 }
      	});
    });


</script>