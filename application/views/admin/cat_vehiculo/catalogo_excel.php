 <html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
            <head>

            </head>
<style type="text/css">
    table, td, th{
        border 1px solid;
        padding: 1cm;
    }
</style>
            <body>
                <?php 

                    $filename = "Catalogo de vehiculos ".date('d-m-Y').".xls";
                    header("Content-Type: application/vnd.ms-excel"); //mime type
                    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                    header("Cache-Control: max-age=0"); //no cache
                
                 ?>                 
                     <table >
                        <thead>


                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="13" style="width: 100% !important; height: 220px !important;"><img src="<?php echo base_url('img/escudo.jpg')?>" width="220" height="220" /></td>                                
                            </tr>                            
                            <tr>

                                <td colspan="13"   style="background-color: #335EFF; height: 100px;font-size: 30px; text-align: center;"><?php echo htmlentities("UNIDAD DE CONTROL DE VEHICULOS Y COMBUSTIBLES CUADRO DE CARACTERISTICAS TECNICAS DE VEHICULOS PROPIEDAD DE LA ALCALDIA DE ANTIGUO CUSCATLAN");?></td>
                            </tr>
                            <tr>
                                <td>Numero de equipo </td>
                                <td>Placa</td>
                                <td>Clase</td>
                                <td>Tipo</td>
                                <td>Marca</td>
                                <td>Modelo</td>
                                <td>Color</td>
                                <td><?php echo htmlentities("Año")?></td>
                                <td>Numero de motor</td>                               
                                <td>Chasis Grabado</td>                           
                                <td>Chasis Vin</td>                                   
                                <td>Clasificaci&oacute;n</td>
                                <td>Departamento Asignado</td>               
                            </tr>

                         <?php
                                if ($vehiculos) {
                                    foreach ($vehiculos as $v) {
                                        echo "<tr>";
                                        echo "<td>".$v->CODIGO_VEHICULO."</td>";
                                        echo "<td>".$v->PLACA."</td>";
                                        echo "<td>".htmlentities($v->CLASE)."</td>";
                                        echo "<td>".htmlentities($v->TIPO)."</td>";
                                        echo "<td>".htmlentities($v->MARCA)."</td>";
                                        echo "<td>".htmlentities($v->MODELO)."</td>";
                                        echo "<td>".htmlentities($v->COLOR)."</td>";
                                        echo "<td>".$v->ANIO."</td>";
                                        echo "<td>".$v->NUMERO_MOTOR."</td>";
                                        echo "<td>".$v->CHASIS_GRABADO."</td>";
                                        echo "<td>".$v->CHASIS_VIN."</td>";
                                        echo "<td>".htmlentities($v->CLASIFICACION)."</td>";
                                        echo "<td>".htmlentities($v->UNIDAD)."</td>";
                                        echo "</tr>";
                                    }
                                }
                            ?>
                              
                        </tbody>
                     </table>
            </body>
</html>    
    