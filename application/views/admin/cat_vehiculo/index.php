<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
		<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>				
		</section>
		<section class="content">
			<div class="box">
				<div class="box-header">
					<h3 class="header-title">Lista de Vehículos</h3>
				</div>
                	<div class="row">
                		<div class="col-sm-2 pull-right">
                            <a href="cat_vehiculo/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                		</div>
                		<div class="col-sm-2 pull-right" style="left: 22px;">
                            <a href="cat_vehiculo/vales" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_vales" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-credit-card"></span> Registro de vales</button></a>               				
                		</div>
                		<div class="col-sm-2 pull-right" style="left: 95px;">
                            <a href="cat_vehiculo/misiones" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_misiones" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-th-list"></span> Misiones</button></a>                			
                		</div>
                		<div class="col-sm-2 pull-right" style="left: 175px;">
                            <a href="cat_vehiculo/excel_create"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-file"></span> Imprimir</button></a>                			
                		</div>                		

                	</div>	

                			
				<div class="box-body">
					<div id="mensaje"></div>
					<table id="vehiculo" class="table table-bordered table-striped">
						<thead>
							<th>Codigo</th>
							<th>Placa</th>
							<th>Color</th>
							<th>Año</th>
							<th>Clase</th>						
							<th>Tipo</th>							
							<th>Modelo</th>
							<th>Combustible</th>
							<th></th>
						</thead>
						<tbody>
							<?php 
								if ($vehiculos) {
									foreach ($vehiculos as $v) {
										$vehiculoId=base64_encode($v->ID_VEHICULO);
										echo "<tr>";
										echo "<td>".$v->CODIGO_VEHICULO."</td>";
										echo "<td>".$v->PLACA."</td>";
										echo "<td>".$v->COLOR."</td>";
										echo "<td>".$v->ANIO."</td>";
										echo "<td>".$v->CLASE."</td>";
										echo "<td>".$v->TIPO."</td>";
										echo "<td>".$v->MODELO."</td>";
										echo "<td>".$v->COMBUSTIBLE."</td>";
										echo "<td><a href='cat_vehiculo/editar/".$vehiculoId."' ><button type='button' title='Editar Vehículo' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";		
	                						
                						?>

                							 <button type="button"  title="Eliminar Vehiculo" onclick="eliminarVehiculo('<?php echo $vehiculoId?>','<?php echo $v->PLACA ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
							<?php
									echo "</td>";
									echo "</tr>";
									}
								} else {

								}								
							?>
						</tbody>
					</table>
				</div>
				
			</div>
			
		</section>
</div>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>

<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>
<script type="text/javascript">
	 var baseurl = "<?php echo base_url();?>";
	$(document).ready(function(){
		$("#vehiculo").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
		});
	});


    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"catalogos/cat_vehiculo"
        } );
    }
    function eliminarVehiculo(idvehiculo,placa){
		alertify.confirm("Eliminar vehículo","¿Seguro de eliminar el vehiculo placa "+placa+"?",function(){
			document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
			var Vehiculo =new Object();
			Vehiculo.id =idvehiculo;
			var DatosJson=JSON.stringify(Vehiculo);
			$.post(baseurl+'/catalogos/cat_vehiculo/eliminar',{
				VehiculoPost: DatosJson
			},function(data,textStatus){
				console.log(data);
				$("#mensaje").html(data.response_msg);
				location.reload();             	
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
				console.log('Error: ' + response.responseText);
			});;
			
		},function(){

		});
	}

</script>
<div class="modal fade text-center"  id="modal_nuevo">
  <div class="modal-dialog" style="width: 90%">
    <div class="modal-content">
    </div>
  </div>
</div>
<div class="modal fade text-center"  id="modal_vales">
  <div class="modal-dialog" style="width: 90%">
    <div class="modal-content">
    </div>
  </div>
</div>
<div class="modal fade text-center"  id="modal_misiones">
  <div class="modal-dialog" style="width: 60%">
    <div class="modal-content">
    </div>
  </div>
</div>