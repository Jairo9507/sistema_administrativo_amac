<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php

	$cod_vehiculo=array(
		'name' 		=> 'cod_vehiculo',
		'id'		=> 'cod_vehiculo',
		'value'		=> set_value('codigo',@$vehiculo[0]->CODIGO_VEHICULO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:70%',
		'disabled' => ''		
	);

	$placa=array(
		'name' 		=> 'placa',
		'id'		=> 'placa',
		'value'		=> set_value('placa',@$vehiculo[0]->PLACA),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '',
		'style'		=> 'width:70%',
		' maxlength'  => '12'
		//'onkeypress'	=> 'return validarn(event)'		
	);

	$color =array(
		'name' 		=> 'color',
		'id'		=> 'color',
		'value'		=> set_value('color',@$vehiculo[0]->COLOR),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:70%',
		//'placeholder' => '',
		'onkeypress'	=> 'return validarn(event)'		
	);

	$anio=array(
		'name' 		=> 'anio',
		'id'		=> 'anio',
		'value'		=> set_value('anio',@$vehiculo[0]->ANIO),
		'type'		=> 'number',
		'class'		=> 'form-control',
		'style'		=> 'width:70%',
		'maxlength'  => '4',
		//'placeholder' => '',
		'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$capacidad_tanque=array(
		'name' 		=> 'capacidad_tanque',
		'id'		=> 'capacidad_tanque',
		'value'		=> set_value('capacidad_tanque',@$vehiculo[0]->CAPACIDAD_TANQUE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:70%;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$capacidad_peso=array(
		'name' 		=> 'capacidad_peso',
		'id'		=> 'capacidad_peso',
		'value'		=> set_value('capacidad_peso',@$vehiculo[0]->CAPACIDAD_PESO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:70%;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$asientos=array(
		'name' 		=> 'asientos',
		'id'		=> 'asientos',
		'value'		=> set_value('asientos',@$vehiculo[0]->ASIENTOS),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:70%;',
		//'placeholder' => '',
		'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$tonelaje=array(
		'name' 		=> 'clasificacion',
		'id'		=> 'clasificacion',
		'value'		=> set_value('tonelaje',@$vehiculo[0]->CLASIFICACION),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:70%;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$peso=array(
		'name' 		=> 'peso',
		'id'		=> 'peso',
		'value'		=> set_value('peso',@$vehiculo[0]->PESO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:70%;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$nmotor=array(
		'name' 		=> 'nmotor',
		'id'		=> 'nmotor',
		'value'		=> set_value('nmotor',@$vehiculo[0]->NMOTOR),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:70%;',
		'maxlength'  => '12'
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$nchasis=array(
		'name' 		=> 'nchasis',
		'id'		=> 'nchasis',
		'value'		=> set_value('nchasis',@$vehiculo[0]->NCHASIS),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:70%;',
		'maxlength'  => '17'
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$nvin=array(
		'name' 		=> 'nvin',
		'id'		=> 'nvin',
		'value'		=> set_value('nvin',@$vehiculo[0]->NVIN),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:70%;',
		'maxlength'  => '17'
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
?>
<input type="hidden" name="id" id="id" value="<?php echo @$vehiculo[0]->ID_VEHICULO?>">

<div class="content-wrapper">
	<div class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
	</div>
	<section class="content">
		<div id="mensaje"></div>
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Datos</h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" name="formulario" id="formulario" role="form">
			<div id="mensaje2"></div>
			<div class="form-group">
				<label for="cod_vehiculo" class="col-sm-2 form-label">No. de vehiculo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					 <?php echo form_input($cod_vehiculo);?>
				</div>
				<label for="placa" class="col-sm-2 form-label">Placa<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					 <?php echo form_input($placa);?>
				</div>				
			</div>
			<div class="form-group">
				<label for="clase" class="col-sm-2 form-label">Clase de vehículo<span style="color: #F20A06; font-size: 15px;">*</span>: </label>
				<div class="col-sm-4">
<select class="form-control" id="clase" style="width: 70%">
<!-- <option value="0">Seleccione clase de vehiculo</option> -->
<?php
if($clase){
foreach ($clase as $c) {
	if ($c->ID_CLASE_VEHICULO==@$vehiculo[0]->ID_CLASE_VEHICULO) {
		echo "<option value='".$c->ID_CLASE_VEHICULO."' selected>".$c->DESCRIPCION."</option>";
	}
	echo "<option value='".$c->ID_CLASE_VEHICULO."'>".$c->DESCRIPCION."</option>";
}
}
?> 
</select>
</div><br>
				<label for="marca" class="col-sm-2 form-label">Tipo de vehículo<span style="color: #F20A06; font-size: 15px;">*</span>: </label>
				<div class="col-sm-4">
					<select class="form-control" id="tipo" style="width: 70%">
					<!-- 	<option value="0">Seleccione tipo de vehiculo</option> -->
						 <?php
							if($tipo){
								foreach ($tipo as $t) {
									if ($t->ID_TIPO_VEHICULO==@$vehiculo[0]->ID_TIPO_VEHICULO) {
										echo "<option value='".$t->ID_TIPO_VEHICULO."' selected>".$t->DESCRIPCION."</option>";
									}else{
									echo "<option value='".$t->ID_TIPO_VEHICULO."'>".$t->DESCRIPCION."</option>";
								}
								}
							}
						?> 
					</select>
				 	
				</div>
			</div>
			<div class="form-group">
				<label for="modelo" class="col-sm-2 form-label">Marca vehículo<span style="color: #F20A06; font-size: 15px;">*</span>: </label>
				<div class="col-sm-4">
					<select class="form-control" id="marca" style="width: 70%">
					<!-- 	<option value="0">Seleccione marca de vehiculo</option> -->
						 <?php
							if($marca){
								foreach ($marca as $m) {
									if ($m->ID_VEHICULO_MARCA==@$vehiculo[0]->ID_VEHICULO_MARCA) {
										echo "<option value='".$m->ID_VEHICULO_MARCA."' selected>".$m->DESCRIPCION."</option>";
									}else{
									echo "<option value='".$m->ID_VEHICULO_MARCA."'>".$m->DESCRIPCION."</option>";
								}
								}
							}
						?> 
					</select>
				</div>	
				<label for="tipo" class="col-sm-2 form-label">Modelo de vehículo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select class="form-control" id="modelo" style="width: 70%">
					<!-- 	<option value="0">Seleccione modelo de vehiculo</option> -->
						 <?php
							if($modelo){
								foreach ($modelo as $mo) {
									if ($mo->ID_VEHICULO_MODELO==@$vehiculo[0]->ID_VEHICULO_MODELO) {
										echo "<option value='".$mo->ID_VEHICULO_MODELO."' selected>".$mo->DESCRIPCION."</option>";
									}else{
									echo "<option value='".$mo->ID_VEHICULO_MODELO."'>".$mo->DESCRIPCION."</option>";
								}
								}
							}
						?> 
					</select>
				</div>	
			</div>
			<div class="form-group">		
				<label for="color" class="col-sm-2 form-label">Color<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($color);?>
				</div>
					<label for="anio" class="col-sm-2 form-label ">Año<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($anio);?>
				</div>
			</div>				
		
<!--			<div class="form-group ">
				<label for="asientos" class="col-sm-2 form-label">Número de asientos:</label>
				<div class="col-sm-4">
					<?php echo form_input($asientos);?>
				</div>	
				<label for="capacidad_tanque" class="col-sm-2 form-label">Capacidad tanque:</label>
				<div class="col-sm-4">
					<?php echo form_input($capacidad_tanque);?>
				</div>
			</div>	-->										
		<div class="form-group">
<!--	<label for="capacidad_tanque" class="col-sm-2 form-label">Capacidad peso:</label>
				<div class="col-sm-4">
					<?php echo form_input($capacidad_peso);?>
				</div>		-->				
				<label for="cuenta_id" class="col-sm-2 form-label">Clasificaci&oacute;n:</label>
				<div class="col-sm-4">
					<?php echo form_input($tonelaje);?>
				</div>
				<label for="nmotor" class="col-sm-2 form-label">N° Motor<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($nmotor);?>
				</div>				
			</div>
			<!--<div class="form-group">
				<label for="cuenta_id" class="col-sm-2 form-label">Peso vehículo:</label>
				<div class="col-sm-4">
					<?php echo form_input($peso);?>
				</div>		

			</div>-->
			<div class="form-group">
				<label for="nchasis" class="col-sm-2 form-label">N° Chasis<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($nchasis);?>
				</div>	
				<label for="nvin" class="col-sm-2 form-label">N° VIN<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<?php echo form_input($nvin);?>
				</div>	
			</div>
			<div class="form-group">
				<label for="combustible" class="col-sm-2 form-label">Tipo de combustible<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select class="form-control" id="combustible" style="width: 70%">
						<option value="0">Seleccione tipo de combustible</option>
						<?php
						if($combustible){
							foreach ($combustible as $co) {
								if ($co->ID_COMBUSTIBLE==@$vehiculo[0]->ID_COMBUSTIBLE) {
									echo "<option value='".$co->ID_COMBUSTIBLE."' selected>".$co->DESCRIPCION."</option>";
								}else{
								echo "<option value='".$co->ID_COMBUSTIBLE."'>".$co->DESCRIPCION."</option>";
							}
							}
						}
						?> 
					</select>
				</div>	
				<label for="unidad" class="col-sm-2 form-label">Unidad administrativa<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select class="form-control" id="unidad" style="width: 70%">
						<option value="0">Seleccione Unidad correspondiente</option>
						<?php
						if($unidad){
							foreach ($unidad as $uni) {
								if ($uni->ID_UNIDAD==@$vehiculo[0]->UNIDAD_PERTENECE) {
									echo "<option value='".$uni->ID_UNIDAD."' selected>".$uni->DESCRIPCION."</option>";
								}
								echo "<option value='".$uni->ID_UNIDAD."'>".$uni->DESCRIPCION."</option>";
							}
						}
						?> 
					</select>
				</div>	
			</div>
		</form>										
				<div class="col-sm-12">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab1" data-toggle="tab" aria-expanded="true">Vales</a>
							</li>
							<li>
								<a href="#tab2" data-toggle="tab" aria-expanded="false">Misiones</a>
							</li>
						</ul>
						
					</div>
 					<div class="tab-content">
						<div class="tab-pane active" id="tab1">
							<table id="vales" class="table table-bordered table-striped">
								<thead>
									<th>Número de vale</th>
									<!-- <th>Número de Equipo</th> -->
									<th>Fecha</th>
									<th>Galones</th>
									<th>Valor ($)</th>
									<th>Kilometraje</th>
									<th></th>
								</thead>
								<tbody>
								  <?php 
										if ($vales) {
											foreach ($vales as $v) {
												echo "<tr>";
												echo "<td class='col-xs-1'><input type='text' class='form-control' style='text-align:right; max-width:100px;' id='numVale_".$v->ID_VALE_COMBUSTIBLE."' value='".$v->NUMERO_VALE."' disabled='true' onkeypress	='return validarNumeros(event)'	/> </td>";
												// echo "<td>".$v->NUMERO_EQUIPO."</td>";
												echo "<td style='width:20%;'>".date('d-m-Y',strtotime($v->FECHA))."</td>";
												echo "<td class='col-xs-1' style='width:20%;'><input type='text' class='form-control' style='text-align:right; max-width:215px;' id='galones_".$v->ID_VALE_COMBUSTIBLE."' value='".$v->GALONES."' disabled='true' onkeypress	='return validarNumeros(event)'/> </td>";
												echo "<td class='col-xs-1' style='30%;'><input type='text' class='form-control' style='text-align:right; max-width:215px;' id='valor_".$v->ID_VALE_COMBUSTIBLE."' value='$".$v->VALOR."' disabled='true' /> </td>";
												echo "<td class='col-xs-1' style='width:20%;'><input type='text' class='form-control' style='text-align:right; max-width:215px;' id='kilometraje_".$v->ID_VALE_COMBUSTIBLE."' value='".$v->KILOMETRAJE."' disabled='true' onkeypress	='return validarNumeros(event)'/> </td>";
												echo "<td>";
												
												
									?>
									<button title='Editar Movimiento' id='btnHabilitar_<?php echo $v->ID_VALE_COMBUSTIBLE?>' class='btn btn-success btn-xs' onclick='desbloquearElementos("<?php echo $v->ID_VALE_COMBUSTIBLE ?>")'  ><span class='glyphicon glyphicon-edit'></span></button>
									<button type='button' onclick="modificarVale('<?php echo $v->ID_VALE_COMBUSTIBLE?>','<?php echo @$vehiculo[0]->PLACA?>')" title='guardar cambios' class='btn btn-primary btn-xs' disabled="true"  id='btnEditar_<?php echo $v->ID_VALE_COMBUSTIBLE?>'><span class='glyphicon glyphicon-ok'></span></button> 
									<?php 
											echo "</td>";
											echo "</tr>";
											}
										}
									 ?>
								</tbody>
							</table>
						</div>
						<div class="tab-pane" id="tab2">
							<table id="misiones" class="table table-bordered table-striped">
								<thead>
									<th>Archivos</th>
									<!-- <th>Fecha Salida</th>
									<th>Fecha Entrada</th>
									<th>Kilometraje Salida</th>
									<th>Kilometraje Entrada</th>
									<th>Destino</th>
									<th>Empleado</th> 
									<th></th>-->
								</thead>
								<tbody>
									<td><a href="<?php echo base_url();?>mision/mision/downloads">SISTEMA BODEGA.xlsx</a></td>
									<!-- <?php 
									if($misiones){
										foreach ($misiones as $m) {
											//echo "<tr>";
												//echo "<td>".$m->ACTIVIDAD."</td>";
											echo "<td>";
											?>
											<select class="form-control" id="actividad" style="width: 100%; right: 43px;" disabled="true">
												<option value="0">Seleccione Actividad</option>
												<?php
												if($actividad){
													foreach ($actividad as $a) {
														if ($a->ID_ACTIVIDAD==@$vehiculo[0]->ID_ACTIVIDAD) {
															echo "<option value='".$a->ID_ACTIVIDAD."' selected>".$a->DESCRIPCION."</option>";
														}
														echo "<option value='".$a->ID_ACTIVIDAD."'>".$a->DESCRIPCION."</option>";
													}
												}
												?> 
											</select>
											<?php
											echo "</td>";
											echo "<td>".$m->FECHA_SALIDA."</td>";
											echo "<td>".$m->FECHA_ENTRADA."</td>";
											echo "<td>".$m->KILOMETRAJE_SALIDA."</td>";
											echo "<td>".$m->KILOMETRAJE_ENTRADA."</td>";
											echo "<td><textarea id='destino' class='form-control' rows='3' disabled='true'>".$m->DESTINO."</textarea></td>";
											//echo "<td>".$m->EMPLEADO."</td>";
											echo "<td>";
											?>
											<select class="form-control" id="empleado" style="width: 103%; right: 41px;" disabled="true">
						<option value="0">Seleccione Empleado</option>
						<?php
						if($empleados){
							foreach ($empleados as $e) {
								if ($e->ID_EMPLEADO==@$vehiculo[0]->ID_EMPLEADO) {
									echo "<option value='".$e->ID_EMPLEADO."' selected>".$e->NOMBRE."</option>";
								}
								echo "<option value='".$e->ID_EMPLEADO."'>".$e->NOMBRE."</option>";
							}
						}
						?> 
					</select>
											<?php
											echo "</td>";
											echo "<td>";
											?>
											<button title='Editar Vale' id='btnHabilitar' class='btn btn-success btn-xs' onclick='desbloquearElementos2("<?php echo $m->ID_CONTROL_MISION ?>")'  ><span class='glyphicon glyphicon-edit'></span></button>
											<button type='button' onclick="modificarMision('<?php echo $m->ID_CONTROL_MISION?>','<?php echo @$vehiculo[0]->PLACA?>')" title='guardar cambios' class='btn btn-primary btn-xs' disabled="true"  id='btnEditar'><span class='glyphicon glyphicon-ok'></span></button> 
											<?php 
											echo "</td>";
											echo "</tr>";
										}
									}
									?> -->
								</tbody>
							</table>							
						</div>
					</div>	
					
				</div>
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
			</div>
			
		</div>
		
	</section>
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_vehiculo"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"catalogos/cat_vehiculo"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }    
$(document).ready(function(){
	  $("#clase").select2();
	  $("#tipo").select2();
	  $("#marca").select2();
	  $("#modelo").select2();
	  $("#combustible").select2();
	  $("#unidad").select2();
	  $("#actividad").select2();
	  $("#empleado").select2();
$("#vales").DataTable({
	      paging      : true,
	      lengthChange: true,
	      searching   : true,
	      ordering    : true,
	      info        : true,
	      autoWidth   : false
		});
$("#misiones").DataTable({
	      paging      : true,
	      lengthChange: true,
	      searching   : true,
	      ordering    : true,
	      info        : true,
	      autoWidth   : false
		});

      	$("#btnGuardar").click(function() {
      		var Vehiculo = new Object();
      		Vehiculo.id 			= $("input#id").val();
      		Vehiculo.codigo 		= $("input#cod_vehiculo").val();
      		Vehiculo.placa 			= $("input#placa").val().replace("-","");
      		Vehiculo.clase 			= $("select#clase").val();
      		Vehiculo.tipo 			= $("select#tipo").val();
      		Vehiculo.marca 			= $("select#marca").val();
      		Vehiculo.modelo 		= $("select#modelo").val();
      		Vehiculo.color 			= $("input#color").val();
      		Vehiculo.anio 			= $("input#anio").val();
      		Vehiculo.asientos 		= '';//$("input#asientos").val();
      		Vehiculo.capacidad_tanque = '';//$("input#capacidad_tanque").val();
      		Vehiculo.capacidad_peso	 = '';//$("input#capacidad_peso").val();
      		Vehiculo.clasificacion		= $("input#clasificacion").val();
      		Vehiculo.peso 			= '';//$("input#peso").val();
      		Vehiculo.nmotor 		= $("input#nmotor").val();
      		Vehiculo.nchasis 		= $("input#nchasis").val();
      		Vehiculo.nvin 			= $("input#nvin").val();
      		Vehiculo.combustible 	= $("select#combustible").val();
      		Vehiculo.unidad 		= $("select#unidad").val();

      		console.log(Vehiculo);
      		// if (Vehiculo.id===undefined) {
      		// 	Vehiculo.id='';
      		// }
      		// else 
      		if (Vehiculo.placa=="" || Vehiculo.clase==0  || Vehiculo.tipo==0 || Vehiculo.marca==0 || Vehiculo.modelo==0 || Vehiculo.color===undefined || Vehiculo.anio===undefined || Vehiculo.nmotor===undefined || Vehiculo.nchasis===undefined || Vehiculo.nvin===undefined || Vehiculo.combustible==0 || Vehiculo.unidad==0) {
      			$("#mensaje2").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese campos obligarios</div>");
      		}else{
      		$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
      		var DatosJson = JSON.stringify(Vehiculo);
      		$.post(baseurl+'catalogos/cat_vehiculo/guardar',{
      			vehiculoPost : DatosJson
      		}, function(data, textStatus) {
      			$("#mensaje").html(data.response_msg);
      			//location.reload();
      		},"json").fail(function(response) {
      			$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
      			console.log('Error: ' + response.responseText);
      		});;
      		return false;
      	 }
      	});
});

function desbloquearElementos(id)
{

	document.getElementById("btnHabilitar_"+id).disabled=true;
	document.getElementById("numVale_"+id).disabled=false;
	document.getElementById("galones_"+id).disabled=false;
	document.getElementById("valor_"+id).disabled=false;
	document.getElementById("kilometraje_"+id).disabled=false;
	document.getElementById("btnEditar_"+id).disabled=false;
}

function desbloquearElementos2(id) {
	document.getElementById("btnHabilitar").disabled=true;
	document.getElementById("actividad").disabled=false;
	document.getElementById("destino").disabled=false;
	document.getElementById("empleado").disabled=false;
	document.getElementById("btnEditar").disabled=false;
}

function modificarVale(id,nombre)
{
	alertify.confirm("Confirmar cambios en vale de vehículo con placa "+nombre,"Si da click en aceptar se modificara el vale seleccionado",function(){
            document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>";
            var vale=new Object();
            vale.id 		 = id;
            vale.numVale     = $("#numVale_"+id).val();
            vale.galones     = $("#galones_"+id).val();
            vale.valor       = $("#valor_"+id).val().replace("$",'');
            vale.kilometraje = $("#kilometraje_"+id).val();
            var DatosJson=JSON.stringify(vale);
            console.log(vale);
            $.post(baseurl+'vales/registro_vales/guardar',{
            	valePost: DatosJson
            }, function(data,textStatus){
            	console.log(data);
            	$("#mensaje").html(data.response_msg);
            },"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);                 	
            });;

	},function(){

	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
}

function modificarMision(id, nombre) {
	alertify.confirm("Confirmar cambios en la misión del vehículo con placa "+nombre,"Si da click en Aceptar se modificara el vale seleccionado",function(){
            document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>";
            var mision=new Object();
            mision.id 		 	   = id;
            mision.idActividad     = $("#actividad").val();
            mision.destino         = $("#destino").val();
            mision.idEmpleado      = $("#empleado").val();
            var DatosJson=JSON.stringify(mision);
            console.log(mision);
            $.post(baseurl+'mision/mision/guardar',{
            	misionPost: DatosJson
            }, function(data,textStatus){
            	console.log(data);
            	$("#mensaje").html(data.response_msg);
            },"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);                 	
            });;

	},function(){

	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
}
</script>
