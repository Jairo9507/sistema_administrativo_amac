<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b><?php echo lang('footer_version'); ?></b> Development
                </div>
                <strong><?php echo lang('footer_copyright'); ?> &copy; 2019-<?php echo date('Y'); ?> <a href="https://globalsolutionslt.com" target="_blank">Global Solutions Latinoamerica</a>  
            </footer>
        </div>

        <script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url();?>assets/frameworks/bootstrap/dist/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="<?php echo base_url();?>js/JsValidacion.js"></script>
        <script src="<?php echo base_url($plugins_dir . '/slimscroll/slimscroll.min.js'); ?>"></script>
        <script src="<?php echo base_url();?>assets/frameworks/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>assets/frameworks/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/fullcalendar/core/main.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/fullcalendar/core/locales-all.js');?>"></script>        
        <script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/fullcalendar/daygrid/main.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/fullcalendar/moment/main.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/fullcalendar/google-calendar/main.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/fullcalendar/timegrid/main.js');?>"></script>        
        <script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/fullcalendar/list/main.js');?>"></script>                
        <script type="text/javascript" src="<?php echo base_url($plugins_dir.'/timepicker/jquery.datetimepicker.full.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url($plugins_dir.'/timepicker/jquery.datetimepicker.full.min.js');?>"></script>        
        <script type="text/javascript" src="<?php echo base_url($plugins_dir.'/timepicker/jquery.datetimepicker.min.js');?>"></script>        



<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/Grid/js/grid.js');?>"></script>
 <script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/multi-select/js/select2.full.min.js');?>"></script>
<?php if ($mobile == TRUE): ?>
        <script src="<?php echo base_url($plugins_dir . '/fastclick/fastclick.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($admin_prefs['transition_page'] == TRUE): ?>
        <script src="<?php echo base_url($plugins_dir . '/animsition/animsition.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'users' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <script src="<?php echo base_url($plugins_dir . '/pwstrength/pwstrength.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'groups' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <script src="<?php echo base_url($plugins_dir . '/tinycolor/tinycolor.min.js'); ?>"></script>
        <script src="<?php echo base_url($plugins_dir . '/colorpickersliders/colorpickersliders.min.js'); ?>"></script>
<?php endif; ?>
        <script src="<?php echo base_url($frameworks_dir . '/adminlte/js/adminlte.min.js'); ?>"></script>
        <script src="<?php echo base_url($frameworks_dir . '/domprojects/js/dp.min.js'); ?>"></script>
<script type="text/javascript">
$(function(){
    $('.sidebar-menu').slimScroll({
        height: '600px',
        color: '#FFFFFF',
        alwaysVisible: true,
        size: '10px',
        railVisible: true
    });
})    
</script>

    </body>
</html>

