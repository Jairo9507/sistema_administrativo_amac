<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <aside class="main-sidebar">
                <section class="sidebar">
<?php if ($admin_prefs['user_panel'] == TRUE): ?>
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">

                        </div>
                        <div class="pull-left info">
                            <h1><?php echo 'Usuario'; //$user_login['firstname'].$user_login['lastname']?></h1>
                            <a href="#"><i class="fa fa-circle text-success"></i> <?php echo lang('menu_online'); ?></a>
                        </div>
                    </div>

<?php endif; ?>
<?php if ($admin_prefs['sidebar_form'] == TRUE): ?>
                    <!-- Search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="<?php echo lang('menu_search'); ?>...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>

<?php endif; ?>
                    <!-- Sidebar menu -->
                    <ul class="sidebar-menu tree" data-widget="tree">
                        <li>
                            <a href="<?php echo site_url('admin'); ?>">
                                <i class="fa fa-home text-primary"></i> <span><?php echo lang('menu_access_website'); ?></span>
                            </a>
                        </li>
                        <?php
                            echo "<li class='header text-uppercase'>Men&uacute; Principal</li>";

                        foreach ($menu as $sidebar) {

                                if($sidebar->Linea==1 && $sidebar->TREEVIEW==0){
                                    echo "<li class=".active_link_controller($sidebar->URL)."><a href=".$sidebar->URL."><i class='fa ".$sidebar->ICONO."'></i> <span>".$sidebar->Descripcion."</span></a></li>" ;                                   
                                } 
                                if($sidebar->Linea==1 && $sidebar->TREEVIEW==1 ){
                                    echo "<li class='treeview ".active_link_controller($sidebar->URL)."'><a href='".$sidebar->URL."'><i class='fa ".$sidebar->ICONO."'></i`> <span>".$sidebar->Descripcion."</span></i><i class='fa fa-angle-left pull-right'></i></a>";
                                    foreach ($submenu as $sm) {
                                        if($sm->Linea==2 && $sm->ID_DEPENDE==$sidebar->IdMenu){
                                            echo "<ul class='treeview-menu'><li class='".active_link_function($sm->URL)."'><a href='".base_url().$sm->URL."'>".$sm->Descripcion."</a></li></ul>";
                                        }                                        
                                    }
                                }
                                                                                   


                        }                            


                        ?>

                    </ul>
                </section>
            </aside>
