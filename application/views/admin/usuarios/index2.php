<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Lista de Empleados</h3>
			</div>
			<div class="row">
				<div class="col-sm-2 pull-right">
					<a align="right" data-backdrop="static" data-keyboard="false" data-target="#nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a> 

				</div>
			</div>
			<div class="box-body">
				
				<table id="user" class="table table-bordered table-striped">
					<thead>
						<th>Codigo Empleado</th>
						<th>Nombre Completo</th>
						<th>Usuario</th>
						<th>Correo</th>
						<th>Perfil</th>
						<th>Fecha de Creación</th>
						<th></th>
					</thead>
					<tbody>
						<?php if ($usuario) {
							foreach ($usuario as $u) {
								$usuarioId = base64_encode($u->ID_USUARIO);
								echo "<tr>";
								echo "<td>".$u->CODIGO."</td>";
								echo "<td>".$u->NOMBRE."</td>";
								echo "<td>".$u->USUARIO."</td>";
								echo "<td>".$u->CORREO."</td>";
								echo "<td>".$u->PERFIL."</td>";
								echo "<td>".$u->FECHA_CREACION."</td>";

								echo "<td><a href='usuario/editar/".$usuarioId."' ><button type='button' title='Editar usuario' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
										?>
									<button type="button"  title="Eliminar usuario" onclick="eliminarUsuario('<?php echo $usuarioId?>','<?php echo $u->NOMBRE?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button> 
										<?php 
										echo "</td";
										echo "</tr>";
							}
						} ?>

					</tbody>
				</table>
			</div>

		</div>
	</section>
</div>

<div class="modal fade text-center" id="nuevo">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Crear Usuario</h4>
				</div>
				<div class="modal-body">
					<div id="mensaje"></div>
					<div id="mensaje_productos"></div>
					<form class="form-horizontal" method="POST">
						<div class="box-body">
							<div class="form-group">
								<label for="codigo" class="col-sm-2 control-label">Codigo*:</label>
								<div class="col-sm-10">
									<select class="form-control js-example-basic-single" id="codigo" style="width: 100%;">
										<option value="">Seleccione</option>
										<?php if ($empleados){
											foreach ($empleados as $e) {
												$id = base64_encode($e->ID_EMPLEADO);
												echo "<option value='".$e->NOMBRE."'>".$e->ID_EMPLEADO." ".$e->NOMBRE."</option>";
											}
										} ?>                  
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="nombre" class="col-sm-2 control-label">Nombre</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="nombre"  name="nombre" readonly>
								</div>
							</div>
							<div class="form-group">
								<label for="usuario" class="col-sm-2 control-label">Usuario:<span style="color: #F20A06; font-size: 15px;">*</span></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="usuario" placeholder="Usuario" name="usuario">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Contraseña:<span style="color: #F20A06; font-size: 15px;">*</span></label>

								<div class="col-sm-10">
									<input type="password" class="form-control" id="contrasena" placeholder="Password">
								</div>
							</div>	
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Correo</label>
								<div class="col-sm-10">
									<input name="correo" type="email" class="form-control" id="correo" placeholder="Correo">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Perfil:<span style="color: #F20A06; font-size: 15px;">*</span></label>
								<div class="col-sm-10">
									<select class="form-control js-example-basic-single" id="perfil" style="width: 100%;">
										<option value="0">Seleccionar un perfil</option>
										<?php 
												if ($perfiles) {
													foreach ($perfiles as $p) {
														echo "<option value='".$p->ID_PERFIL."'>".$p->NOMBRE."</option>";
													}
												}

										 ?>
									</select>	 
								</div>
							</div>							
							<div class="box-body">
								<div class="row">
									<div class="col-md-6">
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
								<button id="guardar" type="button" class="btn btn-info pull-right" ><span class="glyphicon glyphicon-saved"></span>Guardar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery 3 -->
	<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
	<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
	<script type="text/javascript">
		var baseurl = "<?php echo base_url(); ?>";
		$(document).ready(function () {
			$('#codigo').select2();
			$('#perfil').select2();
			$('#user').DataTable({
				pageLength  :5,
				paging      : true,
				lengthChange: true,
				searching   : true,
				ordering    : true,
				"order" : [],
				info        : true,
				autoWidth   : false,
				language: {
					search:'Buscar:',
					order: 'Mostrar Entradas',
					paginate: {
						first:"Primero",
						previous:"Anterior",
						next:"Siguiente",
						last:"Ultimo"
					},
					emptyTable: "No hay informacion disponible",
					infoEmpty: "Mostrando 0 de 0 de 0 entradas",
					lengthMenu:"Mostrar _MENU_ Entradas",
					info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
				}          
			});
		});
		document.getElementById('codigo').onchange = function() {
			var nombre  = $("#codigo").val();
			$("#nombre").val(nombre);
		}
		$('#guardar').click(function(e) {
			
			var cod = document.getElementById("codigo");
			var id = cod.options[cod.selectedIndex].text;
			var per=document.getElementById("perfil");
			var id_per=per.options[per.selectedIndex].index;
			
			var detalle = new Object();
			detalle.id_empleado=id;
			detalle.usuario = $('#usuario').val();
			detalle.contrasena = $('#contrasena').val();
			detalle.correo = $('#correo').val();
			detalle.perfil=id_per;
			detalle.id_detalle='';
			if (detalle.usuario =='' || detalle.contrasena =='') {
				   e.preventDefault();
			        e.stopPropagation();
			        $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese campos requeridos</div>");
			}
			console.log(detalle);
			var DatosJson = JSON.stringify(detalle);
			$.post(baseurl+"admin/usuario/guardar",{
				DetallePost:DatosJson
			}, function (data, textStatus) {
				console.log(data);
				$("#mensaje").append(data.response_msg);
			}, "json").fail(function(response) {
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
				console.log('Error: ' + response.responseText);
			});
		});
		document.getElementById('correo').addEventListener('input', function() {
			campo = event.target;
			valido = document.getElementById('emailOK');

			emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono
    if (emailRegex.test(campo.value)) {
    	valido.innerText = "válido";
    } else {
    	valido.innerText = "incorrecto";
    }
});

		function eliminarUsuario(idusuario,nombre)
	{
		alertify.confirm("Eliminar Usuario","¿Seguro de eliminar usuario "+nombre+"?",function(){
			document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
			var Usuario=new Object();
			Usuario.id=idusuario;
			var DatosJson=JSON.stringify(Usuario);
			$.post(baseurl+'/admin/usuario/eliminar',{
				UsuarioPost: DatosJson
			},function(data,textStatus){
				console.log(data);
				$("#mensaje").html(data.response_msg);
				location.reload();             	
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
				console.log('Error: ' + response.responseText);
			});;
			
		},function(){

		});
	}
</script>

