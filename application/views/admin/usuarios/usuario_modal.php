<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 
	$usuario=array(
		'name' 		=> 'usuario_usuario',
		'id'		=> 'usuario',
		'value'		=> set_value('usuario','','',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Usuario',
		'onkeypress'	=> 'return validarn(event)'
	);


	$contrasena=array(
		'name' 		=> 'contrasena_usuario',
		'id'		=> 'contrasena',
		'value'		=> set_value('usuario','','',''),
		'type'		=> 'password',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Contraseña',
	
	);

	$correo=array(
		'name' 		=> 'correo_usuario',
		'id'		=> 'contrasena',
		'value'		=> set_value('usuario','','',''),
		'type'		=> 'email',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Correo',
		
	);

	$idEmpleado=array(
		'name' 		=> 'idEmpleado_usuario',
		'id'		=> 'contrasena',
		'value'		=> set_value('usuario','','',''),
		'type'		=> 'hidden',
		'rows'		=>	4,
		'class'		=> 'form-control',		
	);
?>
<div class="modal-content">
	<div class="modal-header bg-blue">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><?php echo $pagetitle; ?></h4>
	</div>
	<div class="modal-body">
		<div >
			<form class="form-horizontal" name="formulario" id="formulario" role="form">
				<div class="form-group">
					<div class="col-sm-10">
						<?php echo form_input($idEmpleado);?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-10">
						<?php echo form_input($idEmpleado);?>
					</div>
				</div>
				<div class="form-group">
					<label for="usuario" class="col-sm-2 form-label">Usuario:</label>
					<div class="col-sm-10">
						<?php echo form_input($usuario);?>
					</div>
				</div>
				<div class="form-group">
					<label for="usuario" class="col-sm-2 form-label">Contraseña</label>
					<div class="col-sm-10">
						<?php echo form_password($contrasena);?>
					</div>
				</div>
				<div class="form-group">
					<label for="usuario" class="col-sm-2 form-label">Correo:</label>
					<div class="col-sm-10">
						<?php echo form_input($correo);?>
					</div>
				</div>					
			</form>
		</div>
	</div>
	<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url('js/JsonUsuario.js');?>"></script>