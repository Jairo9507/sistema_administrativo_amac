<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<input type="hidden" name="id" id="id" value="<?php echo @$usuario[0]->ID_USUARIO ?>">

<div class="content-wrapper">
	<div class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>		
		
	</div>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Modificar datos de Usuario</h3>
			</div>
			<div class="box-body">
				<div id="mensaje"></div>
				<form class="form-horizontal" name="formulario" id="formulario" role="form"> 
					<input type="hidden" name="idEmpleado" id="idEmpleado" value="<?php echo @$usuario[0]->CODIGO?>">
					<input type="hidden" name="idUsuarioPerfil" id="idUsuarioPerfil" value="<?php echo @$usuario[0]->ID_USUARIO_PEFIL?>">
					<div class="form-group">
						<label for='usuario' class="form-label col-sm-2" style="left: 73px;">Usuario:</label>
						<div class="col-sm-4">
							<?php 
								if ($this->ion_auth->is_admin()) {
									echo '<input type="text" class="form-control" id="usuario" placeholder="" value="'.@$usuario[0]->USUARIO.'" style="width: 270px; height: 40px;">';
								} else {
									echo '<input type="text" class="form-control" id="usuario" placeholder="" value="'.@$usuario[0]->USUARIO.'" style="width: 270px; height: 40px;" disabled="true">';
								}
							 ?>
						</div><br>
						<label for='contrasena' class="form-label col-sm-2">Contraseña:</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" id="contrasena" placeholder="" value="<?php echo @$usuario[0]->CONTRASENA?>" style="width: 270px; height: 40px;">
						</div><br>
						</div>
					<div class="form-group">
						<label for='correo' class="form-label col-sm-2" style="left: 73px;">Correo:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="correo" placeholder="" value="<?php echo @$usuario[0]->CORREO?>" style="width: 270px; height: 40px;">
						</div>
						<?php 

							if($this->ion_auth->is_admin()){
						 ?>
							<label for='perfil' class="form-label col-sm-2">Perfil:</label>
							<div class="col-sm-4">
							<select class="form-control js-example-basic-single" id="perfil" style="width: 70%;">
							<option value="0">Seleccionar un perfil</option>
							<?php

							if ($perfiles) {

								foreach ($perfiles as $p) {
									if ($p->ID_PERFIL==@$usuario[0]->ID_PERFIL_USUARIO) {
										echo "<option value='".$p->ID_PERFIL."' selected>".$p->NOMBRE."</option>";
									}
									echo "<option value='".$p->ID_PERFIL."'>".$p->NOMBRE."</option>";
								}
							}

							?>
							</select>	 
							</div>
						<?php } else { ?>
							<label for='perfil' class="form-label col-sm-2">Perfil:</label>
							<div class="col-sm-4">
							<select class="form-control js-example-basic-single" id="perfil" style="width: 70%;" disabled="true">
							<option value="0">Seleccionar un perfil</option>
							<?php 
							if ($perfiles) {
								foreach ($perfiles as $p) {
									if ($p->ID_PERFIL==@$usuario[0]->ID_PERFIL_USUARIO) {
										echo "<option value='".$p->ID_PERFIL."' selected>".$p->NOMBRE."</option>";
									}
									echo "<option value='".$p->ID_PERFIL."'>".$p->NOMBRE."</option>";
								}
							}

							?>
							</select>	 
							</div>
							<?php } ?>
					</div>				
				</form>
			</div>
			<div class="box-footer">
				<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
				<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	
				
			</div>
			
		</div>
		
	</section>
	
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
		alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
			window.location.href = baseurl+"admin/usuario"

		} );
	}
	function regresar()
	{
		alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
			window.location.href = baseurl+"admin/usuario"

		},function(){

		}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
	}
	$(document).ready(function(){
		$("#descripcion").focus();
		$("#perfil").select2();

		$("#btnGuardar").click(function(){
			var detalle = new Object();
			detalle.id_empleado=$("input#id").val();
			detalle.id_detalle='1'
			detalle.usuario=$("input#usuario").val();
			detalle.contrasena=$("input#contrasena").val();
			detalle.correo=$("input#correo").val();
			detalle.perfil = $("select#perfil").val();
			detalle.idUsuarioPerfil = $("input#idUsuarioPerfil").val();
			if(detalle.id_empleado===undefined){
				detalle.id='';
			}
			console.log(detalle);
			var DatosJson=JSON.stringify(detalle);
			// $("#modal_nuevo").modal('hide');
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");

			$.post(baseurl+'admin/usuario/guardar',{
				DetallePost: DatosJson
			},function(data,textStatus){
				$("#mensaje").html(data.response_msg);
				console.log(DatosJson)
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
				console.log('Error: ' + response.responseText);
			});;
			return false;
		});
	});
</script>