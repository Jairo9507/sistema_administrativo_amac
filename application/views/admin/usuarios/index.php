<?php
defined('BASEPATH') OR exit('No direct script access allowed');
   
?>



<div class="content-wrapper">
 <section class="content-header">
  <?php echo $pagetitle; ?>
  <?php echo $breadcrumb; ?>
</section>

<section class="content">
 <div class="box">
  <div class="box-header">
    <h3 class="box-title">Lista de Empleados</h3>
  </div>
    <div class="row">
    <div class="col-sm-2 pull-right">
              <a align="right" data-backdrop="static" data-keyboard="false" data-target="#nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a> 
                      
    </div>
  </div>
  <div class="box-body">
    <table id="usuarios" class="table table-bordered table-striped">
      <thead>
        <th>Codigo Empleado</th>
        <th>Nombre Completo</th>
        <th>Usuario</th>
        <th>Correo</th>
        <th>Fecha Creación</th>
        <th></th>
        <tbody>
          <?php if($usuario){
            foreach ($usuario as $u) {
              echo "<tr>";
              echo "<td>".$u->CODIGO."</td>";
              echo "<td>".$u->NOMBRE."</td>";
              echo "<td>".$u->USUARIO."</td>";
              echo "<td>".$u->CORREO."</td>";
              echo "<td>".$u->FECHA_CREACION."</td>";
              echo "<td><a data-target='#crear_usuario' data-toggle='modal' data-backdrop='static' data-keyboard='false' onclick='selEmpleado(\"".$u->CODIGO."\",\"".$u->NOMBRE."\");'><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span></button> </a>
                </td>";
              echo "</tr>";
            }
          } ?>
        </tbody>
      </table>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</section>
</div>

<div class="modal fade text-center" id="nuevo">
  <div class="modal-dialog">
    <div class="modal-content">
     <div class="modal-header bg-blue">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Crear Usuario</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST">
          <div class="box-body">
            <div class="form-group">
            </div>
            <div class="form-group">
              <label for="codEmpleado" class="col-sm-2 control-label">Codigo</label>
              <div class="col-sm-10">
                <select class="form-control js-example-basic-single" id="codigo" style="width: 100%;">
                  <?php if ($empleados){
                    foreach ($empleados as $e) {
                      $id = base64_encode($e->ID_EMPLEADO);
                       echo "<option value='".$e->NOMBRE."'>".$e->ID_EMPLEADO."</option>";

                    }
                  } ?>                  
                </select>
              </div>
            </div>
             <div class="form-group">
              <label for="inputUsuario" class="col-sm-2 control-label">Nombre</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="nombre"  name="nombre" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="inputUsuario" class="col-sm-2 control-label">Usuario</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="usuario" placeholder="Usuario" name="usuario">
              </div>
            </div>
            <div class="form-group">
              <label for="contra" class="col-sm-2 control-label">Contraseña</label>
              <div class="col-sm-10">
                <input name="contrasena" type="password" class="form-control" id="contra" placeholder="Contraseña">
              </div>
            </div>
              <div class="form-group">
              <label for="inputCorreo" class="col-sm-2 control-label">Correo</label>
              <div class="col-sm-10">
                <input name="correo" type="email" class="form-control" id="correo" placeholder="Correo">
              </div>
            </div>
             <div class="box-body">
          <div class="row">
            <div class="col-md-6">
            </div>
          </div>
        </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

  <div class="modal fade text-center" id="crear_usuario">
  <div class="modal-dialog">
    <div class="modal-content">
     <div class="modal-header bg-blue">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Crear Usuario</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" action="<?php echo base_url(); ?>admin/usuario/guardar" method="POST">
          <div class="box-body">
            <div class="form-group">
            </div>
            <div class="form-group">
              <label for="inputUsuario" class="col-sm-2 control-label">Codigo</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="idEmpleado"  name="idEmpleado" readonly>
              </div>
            </div>
             <div class="form-group">
              <label for="inputUsuario" class="col-sm-2 control-label">Nombre</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="nombre"  name="nombre" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="inputUsuario" class="col-sm-2 control-label">Usuario</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="usuario" placeholder="Usuario" name="usuario">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Contraseña</label>
              <div class="col-sm-10">
                <input name="contrasena" type="password" class="form-control" id="contrasena" placeholder="Contraseña">
              </div>
            </div>
              <div class="form-group">
              <label for="inputCorreo" class="col-sm-2 control-label">Correo</label>
              <div class="col-sm-10">
                <input name="correo" type="email" class="form-control" id="correo" placeholder="Correo">
              </div>
            </div>
             <div class="box-body">
          <div class="row">
            <div class="col-md-6">
            </div>
          </div>
        </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary" id="btnActualizar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</section>

</div>


<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/frameworks/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<!-- page script -->
<script type="text/javascript">
  var baseurl='<?php echo base_url();?>';
  $(document).ready(function () {
     $("#codigo").select2();
     var nombre = $("#codigo").select2();
    $('#usuarios').DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }          
    });
  });
  document.getElementById('codigo').onchange = function() {
    var nombre  = $("#codigo").val();
    $("#nombre").val(nombre);
  }
  
  function selEmpleado(idempleado, nombre){
      $('#idEmpleado').val(idempleado);
      $('#nombre').val(nombre);
  }; 
    $('#btnGuardar').click(function(){
    var prueba  = document.getElementById("codigo");
    var id      = prueba.options[prueba.selectedIndex].text;
    var user = $('#usuario').val();
    var contra = $('#contrasena').val();
    var email = $('#correo').val();
    var detalle = new Object();
    detalle.id_empleado = id;
    detalle.usuario = user;
    detalle.contrasena = contra;
    detalle.correo = email;
    detalle.id_detalle='';
    console.log(detalle);
    var DatosJson = JSON.stringify(detalle);
    console.log(DatosJson);
    $.post(baseurl+"admin/usuario/guardar",{
        DetallePost:DatosJson
    }, function(data, textStatus) {
      console.log(data);
      $("#mensaje_productos").append(data.response_msg);

    },"json");
  });



</script>
<!-- <div class="modal fade text-center" id="crear_usuario">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div> -->
