<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="content-title">Lista de Marca de vehículos</h3>
			</div>
<!-- 			<div class="row">

				<div class="col-sm-2 pull-right">
					<a href="cat_actividad/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>
				</div>
			</div> -->
			<br>
			<div class="row">
				<div class="col-md-8" style="width: 65%;left: 15px;">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Lista de Marcas</h3>
						</div>
						<table id="marca" class="table table-bordered table-striped">
							<thead>
								<th>Codigo</th>
								<th>Marca vehículo</th>
								<th>Estado</th>
								<th></th>
							</thead>
							<tbody>
								<?php 
								if ($marca) {
									foreach ($marca as $m) {
										$idmarca= base64_encode($m->ID_VEHICULO_MARCA);
										echo "<tr>";
										echo "<td>".$m->ID_VEHICULO_MARCA."</td>";
										echo "<td>".$m->DESCRIPCION."</td>";
										if ($m->ESTADO==1) {
											echo "<td>ACTIVO</td>";
										} 
										echo "<td><a href='cat_marca/editar/".$idmarca."' ><button type='button' title='Editar marca' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
										?>
										<button type="button"  title="Eliminar marca" onclick="eliminarMarca('<?php echo $idmarca?>','<?php echo $m->DESCRIPCION?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
										<?php 
										echo "</td";
										echo "</tr>";
									}
								}

								?>
							</tbody>
						</table>
					</div>	
				</div>
				<div class="col-md-4">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Agregar Marca de Vehículo</h3>
						</div>
						<form role="form">
							<div class="box-body">
								<div class="form-group">
									<br>
									<label for="exampleInputEmail1">Marca de vehículo</label>
									<br>
									<div id="mensaje"></div>
									<textarea id="descripcion" name="descripcion" class="form-control" rows="3" placeholder="Ingrese nueva marca ..."></textarea>
								</div>
							</div>
						</form>
						<br>
						<div class="modal-footer">
								<button type="button" class="btn btn-default" onclick="limpiar()">Cancelar</button>	&nbsp;							
							<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>				
						</div>

					</div>	
				</div>
			</div>
		</div>
	</section>
</div>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>

<script type="text/javascript">
	function limpiar(){
		$("textarea#descripcion").val('');
	}
	var baseurl = "<?php echo base_url();?>";
	$(document).ready(function() {
		$("#marca").DataTable({
			paging      : true,
			pageLength  : 5,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			"order" : [],
			info        : true,
			autoWidth   : false,
			language: {
				search:'Buscar:',
				order: 'Mostrar Entradas',
				paginate: {
					first:"Primero",
					previous:"Anterior",
					next:"Siguiente",
					last:"Ultimo"
				},
				emptyTable: "No hay informacion disponible",
				infoEmpty: "Mostrando 0 de 0 de 0 entradas",
				lengthMenu:"Mostrar _MENU_ Entradas",
				info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
			} 
		});

		$("#btnGuardar").click(function() {
			var Marca = new Object();
			Marca.id = '';
			Marca.descripcion = $("textarea#descripcion").val();
			var des = $("textarea#descripcion").val();
			console.log(Marca);
			var DatosJson = JSON.stringify(Marca);
			if (des == '') {
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Descripcion es obligatoria</div>");
			}else{
				
				console.log(DatosJson);
				$.post(baseurl+"catalogos/cat_marca/guardar",{
					MarcaPost : DatosJson
				}, function(data, textStatus) {
					$("#mensaje").html(data.response_msg);
					
					if ($("#descripcion").val("") != '') {
				// $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
				location.reload();
			}
		}, "json").fail(function(response) {
			$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
			console.log('Error: ' + response.responseText);
		});;
	}
});
	});

	function eliminarMarca(idmarca,nombre)
	{
		alertify.confirm("Eliminar la marca de vehículo","¿Seguro de eliminar marca "+nombre+"?",function(){
			document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
			var Marca=new Object();
			Marca.id=idmarca;
			var DatosJson=JSON.stringify(Marca);
			$.post(baseurl+'/catalogos/cat_marca/eliminar',{
				MarcaPost: DatosJson
			},function(data,textStatus){
				console.log(data);
				$("#mensaje").html(data.response_msg);
				location.reload();             	
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
				console.log('Error: ' + response.responseText);
			});;
			
		},function(){

		});
	}

</script>