<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
 <?php
	
	$fecha_salida =array(
		'name' 		=> 'fecha_salida',
		'id'		=> 'fecha_salida',
		'value'		=> set_value('fecha_salida',''),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'style'     => 'width:295px; position: relative; left: 59px;',
 		'placeholder' => ''
	);
	$fecha_entrada =array(
		'name' 		=> 'fecha_entrada',
		'id'		=> 'fecha_entrada',
		'value'		=> set_value('fecha_entrada',''),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'style'		=> 'width:295px; position: relative; left: 63px;',
		'placeholder' => ''
	);

	$kilometraje_salida=array(
		'name' 		=> 'kilometraje_salida',
		'id'		=> 'kilometraje_salida',
		'value'		=> set_value('kilometraje_salida',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:298px; position:relative; left: 60px;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$kilometraje_entrada=array(
		'name' 		=> 'kilometraje_entrada',
		'id'		=> 'kilometraje_entrada',
		'value'		=> set_value('kilometraje_entrada',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'style'		=> 'width:298px; position:relative; left: 63px;',
		//'placeholder' => '',
		//'onkeypress'	=> 'return validarNumeros(event)'		
	);
	$destino =array(
		'name'  => 'destino',
		'id'    => 'destino',
		'value' => set_value('destino', ''),
		'type'  =>  'textarea',
		'rows'  =>  4,
		'class' =>  'form-control', 
		'placeholder' => 'Descripcion',
		'style'		  => 'width:300px; position:relative;left:60px;',
		'onkeypress'  => 'return validar(event)'	
	);

?> 

<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h3 class="modal-title"><?php echo $pagetitle; ?></h3>
	</div>
	<div class="modal-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>mision/guardar" method="POST">
			<div id="mensaje2"></div>
			<div class="form-group">
				<label for="placa" class="col-sm-2 form-label">Placa: <span style="color: #F20A06; font-size: 15px;">*</span>:</label>
				<div class="col-sm-4">
					<select class="form-control" id="placa" style="width: 90%;right: 40px;">
						<option value="0">Seleccione Placa</option>
						<?php
						if($placas){
							foreach ($placas as $p) {
								echo "<option value='".$p->ID_VEHICULO."'>".$p->PLACA."</option>";
							}
						}
						?> 
					</select>

				</div>	
				<label for="actividad" class="col-sm-2 form-label">Actividad: <span style="color: #F20A06; font-size: 15px;">*</span></label>
				<div class="col-sm-4">
					<select class="form-control" id="actividad" style="width: 90%; right: 43px;">
						<option value="0">Seleccione Actividad</option>
						<?php
						if($actividad){
							foreach ($actividad as $a) {
								echo "<option value='".$a->ID_ACTIVIDAD."'>".$a->DESCRIPCION."</option>";
							}
						}
						?> 
					</select>

				</div>		
			</div>
			<div class="form-group" style="width: 50%;">
                  <label for="archivo">Registro diario de misiones</label>
                  <input type="text" name="titulo" class="form-control"  id="titulo" style="position: relative;left: 12%;width: 86%;">
                  <input type="file" id="archivo"  name="archivo" style="position: relative;left: 12%;">	

                  		<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>		
	</div>											
		</form>										
	</div>

		
</div>	

</div>

<script type="text/javascript">

    $(document).ready(function(){
        $("#empleado").select2({
             dropdownParent: $("#modal_misiones")
        });
        $("#placa").select2({
             dropdownParent: $("#modal_misiones")
        });
        $("#actividad").select2({
             dropdownParent: $("#modal_misiones")
        });


      	$("#btnGuardar").click(function() {
      		var Mision = new Object();
      		//var idVehiculo 	= document.getElementById("placa");
      		Mision.archivo = $("input#archivo").val();
      		Mision.titulo  = $("input#titulo").val();
      		Mision.archivo = $("input#archivo").val();
      		Mision.id 				= '';
      		Mision.id_vehiculo 	    	= $("select#placa").val();
  

      		console.log(Mision);

      		if (Mision.id_vehiculo ==0 || Mision.id_actividad==0) {
      			$("#mensaje2").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese campos obligarios</div>");
      		}else{
      		$("#modal_misiones").modal('hide');
      		$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
      		var DatosJson = JSON.stringify(Mision);
      		$.post(baseurl+'mision/mision/guardar',{
      			misionPost : DatosJson
      		}, function(data, textStatus) {
      			$("#mensaje").html(data.response_msg);
      			location.reload();
      		},"json").fail(function(response) {
      			$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
      			console.log('Error: ' + response.responseText);
      		});;
      		return false;
      	 }
      	});
    });


</script>