<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"admin/perfil"
        } );
    }
    function eliminarPerfil(idperfil,nombre){
        alertify.confirm("Eliminar Perfil","¿Eliminar el perfil "+nombre+"?",function(){
            document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
            var Perfil =new Object();
            Perfil.Id = idperfil;
            var Datosjson=JSON.stringify(Perfil);
            $.post(baseurl+"admin/perfil/eliminar",{
                deletePerfil: Datosjson
            }, function(data,textStatus){
                
                $("#mensaje").html(data.response_msg);
            }, "json").fail(function(response) {
                        console.log('Error: ' + response.responseText);
                    });;
        },function(){

        });
    }

</script>
<div class="content-wrapper">
	                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="box">
                        <div class="box-header ">
                            <h3 class="box-title">Lista de perfiles</h3>
                            
                        </div>
                        <div class="row">
                            <div class="col-sm-2 pull-right">
                                <a href="perfil/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div id="mensaje"></div>
                            <table id="perfiles" class="table table-bordered table-striped">
                                <thead>
                                    <th>Nombre</th>
                                    <th>Descripcion</th>
                                    <th>Estado</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    <?php 
                                        if($perfil){

                                            foreach ($perfil as $p) {
                                                $idperfil=base64_encode($p->ID_PERFIL_USUARIO);
                                                echo "<tr>";
                                                echo "<td>".$p->NOMBRE."</td>";
                                                echo "<td>".$p->DESCRIPCION."</td>";
                                                if($p->ESTADO==1){
                                                    echo "<td>ACTIVO</td>";
                                                }
                                                echo "<td><a href='perfil/editar/".$idperfil."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
                                                
                                    ?>
                                        <button type="button"  title="Eliminar Perfil" onclick="eliminarPerfil('<?php echo $idperfil?>','<?php echo $p->NOMBRE ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
                                    <?php   
                                    echo "</td>";
                                    echo "</tr>";
                                            }
                                        }

                                    ?>
                                </tbody>    
                            </table>
                        </div>                        
                    </div>
                </section>
</div>



<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

<!-- page script -->
<script>
  $(function () {
    $('#perfiles').DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }            
    });
  })
</script>
<div class="modal fade text-center" id="modal_nuevo">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>