<?php
defined('BASEPATH') OR exit('No direct script access allowed');



?>
<?php 
	$nombre=array(
		'name' 		=> 'nombre_perfil',
		'id'		=> 'nombre',
		'value'		=> set_value('nombre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Perfil',
		'onkeypress'	=> 'return validarn(event)'
	);


	$descripcion=array(
		'name' 		=> 'descripcion_perfil',
		'id'		=> 'descripcion',
		'value'		=> set_value('nombre',''),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion',
		'onkeypress'	=> 'return validarn(event)'
	);
?>

<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h4 class="modal-title"><?php echo $pagetitle; ?></h4>
	</div>
	<div class="modal-body">
		<div >
			<form class="form-horizontal" name="formulario" id="formulario" role="form">
				<div class="form-group">
					<label for="nombre" class="col-sm-2 form-label">Nombre:</label>
					<div class="col-sm-10">
						<?php echo form_input($nombre);?>
					</div>
				</div>
				<div class="form-group">
					<label for="nombre" class="col-sm-2 form-label">Descricpci&oacute;n</label>
					<div class="col-sm-10">
						<?php echo form_textarea($descripcion);?>
					</div>
				</div>				
			</form>
		</div>
	</div>
	<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url('js/JsonPerfil.js');?>"></script>