<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dist/css/alertify.css')?>">


<input type="hidden" name="id" id="id" value="<?php echo @$perfil[0]->ID_PERFIL_USUARIO?>">
<?php 
	$nombre=array(
		'name' 		=> 'nombre_perfil',
		'id'		=> 'nombre',
		'value'		=> set_value('nombre',@$perfil[0]->NOMBRE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Perfil',
		'onkeypress'	=> 'return validarn(event)'
	);


	$descripcion=array(
		'name' 		=> 'descripcion_perfil',
		'id'		=> 'descripcion',
		'value'		=> set_value('nombre',@$perfil[0]->DESCRIPCION),
		'type'		=> 'textarea',
		'rows'		=>4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion',
		'onkeypress'	=> 'return validarn(event)'
	);
?>

<div class="content-wrapper">
	<div class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
	</div>
	<section class="content">
		<div class="col-sm-12">
			<div class="box box-primary">
				<div id="mensaje"></div>
				<div class="box-header">
						<h3 class="box-title">Editar perfil</h3>
				</div>
				<form role="form"  name="formulario" id="formulario">
					<div class="box-body">
						<div class="form-group col-sm-8 col-sm-push-1 ">
							<label for="nombre" class="form-label">Nombre:</label>
								<?php echo form_input($nombre);?>
							
						</div>
						<div class="form-group col-sm-8 col-sm-push-1">
							<label for="nombre" class="form-label">Descricpci&oacute;n</label>
								<?php echo form_textarea($descripcion);?>
							
						</div>

					</div>
					<div class="box-footer">
					<button type="button" class="btn btn-default " onclick="regresar()">Cancelar</button>
					<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>
						
					</div>
				</form>
			</div>
		</div>
			
		</div>
		
	</section>
</div>

<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"admin/perfil"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"admin/perfil"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsonPerfil.js');?>"></script>