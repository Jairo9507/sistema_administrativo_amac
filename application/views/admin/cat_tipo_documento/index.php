<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('codigo',''),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion',
		'onkeypress'	=> 'return validarn(event)'

	);

?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"catalogos/cat_tipo_documento"
        } );
    }
</script>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="content-title">Lista de tipos</h3>
			</div>
			<div id="mensaje"></div>
			<div class="row">
				<div class="col-md-8">
					<div class="box" style="left: 15px;">
						<div class="box-header">
							
						</div>
							<table id="tipos" class="table table-bordered table-striped">
								<thead>
									<th>Descripci&oacute;n</th>
									<th>Estado</th>
									<th></th>
								</thead>
								<tbody>
									<?php 
										if ($tipos) {
											foreach ($tipos as $t) {
												$tipoId=base64_encode($t->ID_CAT_TIPO_DOCUMENTO);
												echo "<tr>";
												echo "<td>".$t->DESCRIPCION."</td>";
												if ($t->ESTADO==1) {
													echo "<td>Activo</td>";
												}
												echo "<td>";												
									 ?>
										<button type="button" title="Editar tipo" class="btn btn-success btn-xs" onclick="actualizarTipo('<?php echo $tipoId?>', '<?php echo $t->DESCRIPCION?>')"><span class='glyphicon glyphicon-edit'></span> </button>&nbsp;
										<button type="button"  title="Eliminar tipo" onclick="eliminarTipo('<?php echo $tipoId?>','<?php echo $t->DESCRIPCION?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
									 <?php 
									 		echo "</td>";
									 		echo "</tr>";
											}
										}

									  ?>
								</tbody>
							</table>						
					</div>
				</div>
				<div class="col-md-4">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Agregar nuevo estante</h3>
						</div>
						<form class="form-horizontal" name="formulario" id="formulario" role="form">
							<div class="form-group">
								<input type="hidden" name="id" id="id" value="">
								<label class="form-label col-sm-2">Descripci&oacute;n<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
								<div class="col-sm-12">
									<?php echo form_textarea($descripcion); ?>
								</div>
							</div>
						</form>
						<div class="box-footer">
								<button type="button" class="btn btn-default" onclick="limpiar()">Cancelar</button>	&nbsp;						
								<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>							
						</div>						
					</div>
				</div>
			</div>
		</div>
	</section>	
</div>
<script type="text/javascript">
	function actualizarTipo(id,nombre){
		$("input#id").val(id);
		$("textarea#descripcion").val(nombre);
	}
	function limpiar()
	{
		$("input#id").val('');
		$("textarea#descripcion").val('');		
	}
	function eliminarTipo(id,nombre){
		alertify.confirm("Eliminar","¿Seguro de eliminar el tipo de documento "+nombre+"?",function(){
            document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
            var tipo=new Object();
            tipo.id=id;
            console.log(tipo);
            var DatosJson=JSON.stringify(tipo);
            $.post(baseurl+'catalogos/cat_tipo_documento/eliminar',{
            	TipoPost:DatosJson
            },function(data,textStatus){
            	$("#mensaje").html(data.response_msg);
            },"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
	    		console.log('Error: ' + response.responseText);
            });
		},function(){

		});
	}
	$(document).ready(function(){
		$("#descripcion").focus();
		$("#tipos").DataTable({ 
			paging      : true,
			pageLength  : 5,
			lengthChange: true,
			searching   : true,
			ordering    : true,
			"order" : [],
			info        : true,
			autoWidth   : false,
			language: {
				search:'Buscar:',
				order: 'Mostrar Entradas',
				paginate: {
					first:"Primero",
					previous:"Anterior",
					next:"Siguiente",
					last:"Ultimo"
				},
				emptyTable: "No hay informacion disponible",
				infoEmpty: "Mostrando 0 de 0 de 0 entradas",
				lengthMenu:"Mostrar _MENU_ Entradas",
				info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
			}    
		});	

		$("#btnGuardar").click(function(){
			var tipo=new Object();
			tipo.id=$("input#id").val();
			tipo.descripcion=$("textarea#descripcion").val();
			console.log(tipo);
			var DatosJson=JSON.stringify(tipo);
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");			
			$.post(baseurl+'catalogos/cat_tipo_documento/save',{
				TipoPost:DatosJson
			},function(data,textStatus){
				$("#mensaje").html(data.response_msg);
					
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
	    		console.log('Error: ' + response.responseText);
			});
		});
	});
</script>