<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"inventario/requisicion"
        } );
    }
</script>

<div class="content-wrapper">
	 <section class="content-header">
	 	<?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
	 </section>
	 <section class="content">
	 	<div class="box">
            <div id="mensaje"></div>
            <div class="box-header">
                <h3>Lista de Prestamos</h3>
            </div>
                            <div class="row">
	                            <div class="col-sm-2 pull-right"> 
	                                <a href="transferencia/nuevo" ><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>
	                            </div>
                        	</div>              
            <div class="box-body">
       				<table id="transferencias" class="table table-bordered table-striped">
       					<thead>
       						<th>C&oacute;digo</th>
       						<th>Fecha De Salida Del Documento</th>
       						<th>Unidad Solicito</th>
       						<th>Creado por</th>
       						<th>Fecha creaci&oacute;n</th>
                  <th>Modificado por</th>
                  <th>Fecha modificaci&oacute;n</th>                  
       						<th></th>
       					</thead>
       					<tbody>
       						<?php 
       							if ($transferencias) {
       								foreach ($transferencias as $t) {
                        $transferenciaId=base64_encode($t->ID_TRANSFERENCIA);
       									echo "<tr>";
       									echo "<td>".$t->CODIGO_SERIE."</td>";
       									echo "<td>".date('d-m-Y',strtotime($t->FECHA_TRANSFERENCIA))."</td>";
       									echo "<td>".$t->UNIDAD."</td>";
       									echo "<td>".$t->USUARIO_CREACION."</td>";
                        echo "<td>".date('d-m-Y',strtotime($t->FECHA_CREACION))."</td>";
                        echo "<td>".$t->USUARIO_MODIFICACION."</td>";
                        $fechamod=!empty($t->FECHA_MODIFICACION) ? date('d-m-Y',strtotime($t->FECHA_MODIFICACION)):"";
                        echo "<td>".$fechamod."</td>";
                        echo "<td><a href='transferencia/editar/".$transferenciaId."' ><button type='button' title='Editar Requisicion' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button></td>";
       									echo "</tr>";

       								}
       							}
       						 ?>
       					</tbody>
       				</table>     	
            </div>	 		
	 	
	 </section>	
</div>

<script type="text/javascript">
	$(document).ready(function(){
    $('#transferencias').DataTable({
      paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas"
      }        
    });		
	})
</script>