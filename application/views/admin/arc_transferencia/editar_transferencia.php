<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 

	$correlativo= array(
		'name' 		=> 'correlativo',
		'id'		=> 'correlativo',
		'value'		=> set_value('correlativo',@$transferencia[0]->CODIGO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'PR001',
		'disabled'	=> ''

	);

    $no_cajas= array(
        'name'      => 'no_cajas',
        'id'        => 'no_cajas',
        'value'     => set_value('no_cajas',@$transferencia[0]->NO_CAJAS),
        'type'      => 'number',
        'class'     => 'form-control',
        'placeholder' => '',
        'min'       =>'1'
    );    



	$fecha =array(
		'name' 		=> 'fecha',
		'id'		=> 'fecha',
		'value'		=> set_value('fecha',date('Y-m-d',strtotime(@$transferencia[0]->FECHA_TRANSFERENCIA))),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'placeholder' => ''
	);

    $observaciones=array(
        'name'      => 'observaciones',
        'id'        => 'observaciones',
        'value'     => set_value('correlativo',''),
        'type'      => 'textarea',
        'class'     => 'form-control',
        'placeholder' => '',
        'rows'  =>3
    );



 ?>

<input type="hidden" name="id" id="id" value="<?php echo @$transferencia[0]->ID_TRANSFERENCIA;?>">
<div class="content-wrapper">
 	<section class="content-header">
 		<?php echo $pagetitle; ?>
 		        <?php echo $breadcrumb; ?>
 	</section>
 	<section class="content">
 		<div class="box">
 			<div id="mensaje"></div>
 			<div class="box-header">
 				<h3 class="header-title">Crear</h3>
 			</div>
 			<div class="box-body">
 				<form class="form-horizontal" name="formulario" id="formulario" role="form">
 					<div class="form-group">
 						<label for="codigo" class="col-sm-2 form-label">Codigo</label>
 						<div class="col-sm-4">
 							<?php  echo form_input($correlativo);?>
 						</div>
 						<label for="fecha" class="col-sm-2 form-label">Fecha</label>
 						<div class="col-sm-4">
 							<?php  echo form_input($fecha);?>
 						</div> 						
 					</div>
 					<div class="form-group" >
 						<label for="unidad_crea" class="col-sm-2 form-label">Unidad Solicita</label>
 						<div class="col-sm-4">
 							<select id="listUnidad" class="form-control js-example-basic-single" style="width:90%!important; ">
 								<option value="0">Seleccionar unidad</option>
 								<?php 
 									if ($unidades) {
 										foreach ($unidades as $u) {
                                            if ($u->ID_UNIDAD==@$transferencia[0]->ID_UNIDAD_SOLICITA) {
                                            echo "<option value=".$u->ID_UNIDAD." selected>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
                                            } else {
                                            echo "<option value=".$u->ID_UNIDAD.">".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
                                            }                                            

 										}
 									}

 								 ?>
 							</select>
 						</div>
 						<label for="encargado_archivo" class="col-sm-2 form-label">Responsable</label>
 						<div class="col-sm-4">
	 						<select id="listEmpleado" class="form-control" style="width:90%!important; ">
	 							<option value="0">Seleccione responsable</option>
	 							<?php 
	 								if ($empleados) {
	 									foreach ($empleados as $e) {
                                            if ($e->ID_EMPLEADO==@$transferencia[0]->ID_RESPONSABLE_ARCHIVO_GESTION) {
                                            echo "<option value='".$e->ID_EMPLEADO."' selected>".$e->CODIGO." ".$e->nombre_completo."</option>";
                                            } else {
                                            echo "<option value='".$e->ID_EMPLEADO."'>".$e->CODIGO." ".$e->nombre_completo."</option>";
                                            }
                                            

	 									}
	 								}
	 							 ?>
	 						</select> 							
 						</div>

 					</div>
 					<div class="form-group">
 						<!--<label for="no_cajas" class="col-sm-2 form-label">No. total de cajas</label>
 						<div class="col-sm-4">
 							<?php echo form_input($no_cajas) ?>
 						</div>-->
                        
                        <div class="col-sm-2 pull-right">
                            <button type="button" id="btnTransferencia" class="btn btn-info"><span class="glyphicon glyphicon-th-list"></span> Crear Detalle</button>

                        </div>                      

 					</div>
 					<div id="group_detalle" style="display: none;">
                        <div class="form-group"  >
                            <label for="serie" class="col-sm-2 form-label">Serie Documental</label>
                            <div class="col-sm-4">
                                <select id="serie_id" class="form-control" style="width: 80%!important">
                                    <option value="">Seleccione</option>
                                    <?php 
                                        if ($series) {
                                            foreach ($series as $s) {
                                                echo "<option value='".$s->ID_SERIE_DOCUMENTAL."'>".$s->NOMBRE."</option>";
                                            }
                                        }
                                     ?>
                                </select>                           
                            </div>
                            <label for="subserie_id" class="form-label col-sm-2">Subserie Documental</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="subserie_id" disabled="true" style="width: 80%!important">

                                </select>                           
                            </div>
                             
                        
                        </div>
                        <div class="form-group">
                            <label for="documentos_id" class="form-label col-sm-2">Documento</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="documentos_id" disabled="true" style="width: 80%!important">
                                    
                                </select>
                            </div> 
                            <label class="form-label col-sm-2" for="vigencia_anios">Tiempo prestamo</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="tiempo">
                                    <option value="">Seleccione</option>
                                    <option value="1">1 D&iacute;a</option>
                                    <option value="3">3 D&iacute;as</option>
                                    <option value="7">1 semana</option>
                                    <option value="30">1 mes</option>
                                    <option value="120">3 meses</option>
                                    <option value="240">6 meses</option>
                                    <option value="365">1 a&ntilde;o</option>
                                </select>
                            </div> 

                        </div>
                        <div class="form-group">
                            <label class="form-label col-sm-2" for="observaciones">Documentos Prestados</label>
                           <div class="col-sm-6">
                               <?php echo form_textarea($observaciones); ?>
                           </div>                           
                                    <div class="col-sm-2 pull-right">
                                        <button type="button" id="btnAgregar" class="btn btn-primary" ><span class="glyphicon glyphicon-plus"></span> </button>                      
                                   </div>                            
                        </div>
                    </div>
                </form>
                <div class="row">
                	<div class="col-sm-12">
	                	<table class="table table-bordered table-striped"  style="width: 99%;position: initial;left: 35px;bottom: 77px; " id="trans_tabla">
	                		<thead>

                                          <th>Correlativo archivo central</th>
	                                      <th style=" display: none;">ID</th>
	                                      <th style=" display: none;">detalle</th>
						                  <th>Fecha Salida</th>
                                          <th>Fecha Regreso Estimada</th>
                                          <th>Documentos Prestados</th>
                                          <th>Fecha De Regreso Real</th>
                                          <th>Estado</th>
	                                      <th style=""></th>                			
	                		</thead>
	                		<tbody id="DocumentosTablaBody">
	                			<?php 
                                    if ($documentos) {

                                        foreach ($documentos as $d) {
                                            $documentoId=base64_encode($d->ID_DOCUMENTO);
                                            echo "<tr>";
                                            echo "<td>".$d->CORRELATIVO_CENTRAL."</td>";

                                            echo "<td style='display:none;' class='Iddocumento'>".$d->ID_DOCUMENTO."</td>";
                                            echo "<td style='display:none;' class='Iddetalle'>".$d->ID_DETALLE_TRANSFERENCIA."</td>";

                                            echo "<td>".date('Y-m-d',strtotime($d->FECHA_SALIDA_ARCHIVO_CENTRAL))."</td>";
                                            echo "<td>".date('d-m-Y',strtotime($d->FECHA_REGRESO_ARCHIVO_CENTRAL))."</td>";
                                            echo "<td>".$d->OBSERVACIONES."</td>";

                                            if (empty($d->FECHA_RETORNO_REAL)) {
                                                echo "<td id='fecha_regreso_real'></td>";
                                            } else {
                                                echo "<td id='fecha_regreso_real'>".date('d-m-Y',strtotime($d->FECHA_RETORNO_REAL))."</td>";
                                            }
                                            if ($d->ESTADO==2) {
                                                echo "<td id='estado_doc' >Prestado</td>";
                                            } else {
                                                echo "<td id='estado_doc' >DEVUELTO</td>";
                                            }
                                            
                                            if ($d->ESTADO==2) {
                                                echo "<td><button type='button'  title='Eliminar Transferencia'  class='btn btn-danger btn-xs btnEliminar'><span class='glyphicon glyphicon-remove'></span></button>";
                                            } else {
                                              echo "<td><button disabled='true' type='button'  title='Eliminar Transferencia'  class='btn btn-danger btn-xs btnEliminar'><span class='glyphicon glyphicon-remove'></span></button>";
                                            }
                                            if ($d->ESTADO==2) {
                                                                                    
                                 ?>
                                    &nbsp;     <button type="button"  title="Regresar" onclick="retornarDocumento('<?php echo $documentoId?>','<?php echo $d->CORRELATIVO_CENTRAL ?>','<?php echo $d->ID_DETALLE_TRANSFERENCIA?>');" class="btn btn-primary btn-xs "><span class="glyphicon glyphicon-arrow-left"></span></button>                                 
                                 <?php 
                                                } else {
                                    ?>
                                    &nbsp;     <button disabled="true" type="button"  title="Regresar" onclick="retornarDocumento('<?php echo $documentoId?>','<?php echo $d->CORRELATIVO_CENTRAL ?>','<?php echo $d->ID_DETALLE_TRANSFERENCIA?>');" class="btn btn-primary btn-xs "><span class="glyphicon glyphicon-arrow-left"></span></button>                                      
                                    <?php               
                                                }                                        
                                            echo "</td>";
                                            echo "</tr>";
                                        }
                                    }
                                  ?>
	                		</tbody>
	                	</table>                		
                	</div>
                </div>  				
 			</div>
            <div class="box-footer">

                    <button type="submit" class="btn btn-primary pull-right" id="btnGuardar"  onclick="salir();"><span class="glyphicon glyphicon-saved" ></span> Salir</button>               
            </div>            
 		</div>
 	</section>
	
</div>
 <script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>

<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
 

    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"inventario/requisicion"

        } );
    }
    function regresar()
    {
        alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
              window.location.href = baseurl+"inventario/requisicion"

        },function(){

        }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }    

    function salir()
    {
        alertify.confirm("¿Desea Regresar?","Asegurese de que la requisicion este correcta, de clic en aceptar regresara a la lista",function(){
              window.location.href = baseurl+"inventario/requisicion"

        },function(){

        }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }

     function retornarDocumento(id,nombre,detalle)
    {
        alertify.confirm("Retornar Documento","Al dar click en aceptar se confirma que el documento "+nombre+" ha sido regresado al inventario documental",function(){
             document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>";
                var documento=new Object();
                documento.id=id;
                documento.id_transferencia=detalle;
                console.log(documento);
                var DatosJson=JSON.stringify(documento);
                $.post(baseurl+'archivos/transferencia/retornoDocumento',{
                    DocumentoPost:DatosJson
                },function(data,textStatus){
                    $("#mensaje").html(data.response_msg);
                    $("#estado_doc").text("Devuelto");
                    $("#fecha_regreso_real").text(data.fecha);
                },"json").fail(function(response){
                    $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                    console.log('Error: ' + response.responseText);                    
                });
        },function(){

        }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
    $(document).ready(function(){
    	        $("#listUnidad").select2();
    	        $("#listEmpleado").select2();
                $("#serie_id").select2();
                $("#subserie_id").select2();
                $("#documentos_id").select2();
        var codigo='';
        var date='';        
        var unidad='';
        var fecha='';

/* 		$.ajax({
            type: "GET",
            url: baseurl+'archivos/transferencia/obtenerCodigo',
            dataType:"json",
            success: function(data,textStatus){
                console.log("data "+data.codigo);
                if (data.codigo!=null &&  data.codigo!="000") {                	
                    console.log(data.codigo.substring(data.codigo.length-1));
                    int=parseInt(data.codigo.substring(data.codigo.length-1));                    
                    console.log(int);                        
                    codigo=int+1;               
                    console.log("codigo: "+codigo);                
                } else {
                    codigo='1';
                    console.log("else "+codigo);
                } 
            }
        });
        $("#fecha").blur(function(){
            date=$(this).val();

            console.log("fecha "+date.replace(new RegExp('-','g'),""));
            date=date.replace(new RegExp('-','g'),"");
            $("input#correlativo").val('PR'+date+unidad+'0'+codigo);
        }); 
        $("#listUnidad").change(function(){
            unidad=$(this).val();
            console.log(unidad);
            $("input#correlativo").val('PR'+date+unidad+'0'+codigo);            
        }); */
        $("#btnTransferencia").click(function(){
        	var transferencia=new Object();
        	transferencia.id=$("input#id").val();
        	transferencia.unidad_solicita=$("select#listUnidad").val();
        	transferencia.codigo=$("input#correlativo").val();
        	transferencia.fecha=$("input#fecha").val();
        	transferencia.responsable=$("select#listEmpleado").val();
        	transferencia.no_cajas='';
        	console.log(transferencia);
        	var DatosJson=JSON.stringify(transferencia);
        		$.post(baseurl+'archivos/transferencia/saveTransferencia',{
        			TransferenciaPost:DatosJson
        		},function(data,textStatus){
                    $("#group_detalle").show();
        			console.log(data.campo)
        			//$("input#id").val(data.campo);
                                $("#btnGuardar").attr("disabled",false);
        		},"json").fail(function(response){
                $("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                        console.log('Error: ' + response.responseText);
        		});
        	
        });

        $("select#tiempo").change(function(){
            var tiempo=$(this).val();

                var d = new Date($("input#fecha").val());
                var year = d.getFullYear();
                var month = d.getMonth();
                var day = d.getDate();
                if(tiempo==1){
                    var c = new Date(year , month, day+2);                                        
                } else if(tiempo==3){
                    var c = new Date(year , month, day+4);
                } else if(tiempo==7) {
                    var c = new Date(year , month, day+8);                                        
                } else if(tiempo==30) {
                    console.log(month);
                    var c = new Date(year , month+1, day+1);
                } else if (tiempo==120){
                   var c = new Date(year , month+3, day+1);                 
                } else if(tiempo==240){
                    var c = new Date(year , month+6, day+1);                
                } else if(tiempo==365){
                    var c = new Date(year+1 , month, day+1);
                }
                //console.log(c.getDate()+'/'+c.getMonth()+'/'+c.getFullYear());
                fecha=c.toISOString().substr(0, 10).replace('T', ' ');
                console.log(c.toISOString().substr(0, 10).replace('T', ' '));
            
        });        

        $("select#serie_id").change(function(){
            $("#subserie_id").attr("disabled",true);
            $("#documentos_id").attr("disabled",true);
            var option1='';
            var option2='';
            var serie=new Object();
            serie.id=$(this).val();
            console.log(serie);
            var DatosJson=JSON.stringify(serie);
            $.post(baseurl+'archivos/transferencia/obtenerSubseries',{
                SeriePost:DatosJson
            },function(data,textStatus){
                 option1+='<option value="">Seleccione</option>';
                 option2+='<option value="">Seleccione</option>';
                 console.log(data.subseries);
                if (data.subseries!=="<option value=''>No contiene información</option>") {
                     data.subseries.forEach(function(element){
                        //console.log(element);
                        option1+="<option value='"+element.ID_SUB_SERIE_DOCUMENTAL+"'>"+element.NOMBRE+"</option>";
                    });

                } else {
 
                }
                console.log(data.documentos);
                data.documentos.forEach(function(element){
                    option2+="<option value='"+element.ID_DOCUMENTO+"'>"+element.CORRELATIVO_CENTRAL+"</option>"
                })
                $("#subserie_id").attr("disabled",false);
                $("#documentos_id").attr("disabled",false);
                $("#subserie_id").html(option1);
                $("#documentos_id").html(option2);
            },"json").fail(function(response){
                    $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                            console.log('Error: ' + response.responseText);
            });
        });        

        $("#subserie_id").change(function(){
            $("#documentos_id").attr("disabled",true);
            var subserie=new Object();
            var html='';
            subserie.id=$(this).val();
            console.log(subserie);
            var DatosJson=JSON.stringify(subserie);
            $.post(baseurl+'archivos/transferencia/obtenerDocumentoSubserie',{
                SubseriePost:DatosJson
            },function(data,textStatus){
                console.log(data);
                html+="<option value=''>Seleccione</option>";
                data.documentos.forEach(function(element){
                     html+="<option value='"+element.ID_DOCUMENTO+"'> "+element.CORRELATIVO_CENTRAL+"</option>";
                });
                $("#documentos_id").attr("disabled",false);
                $("#documentos_id").html(html);
            },"json").fail(function(response){
                    $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                            console.log('Error: ' + response.responseText);
            });
        });

        $("#btnAgregar").click(function(){
            var detalle=new Object();
            var html='';
            detalle.id='';
            detalle.fecha_salida=$("input#fecha").val();
            detalle.fecha_regreso=fecha;
            detalle.id_documento=$("select#documentos_id").val();
            detalle.transferencia=$("input#id").val();
            detalle.observaciones=$("textarea#observaciones").val();
            console.log(detalle);
            var DatosJson=JSON.stringify(detalle);
    $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
            $.post(baseurl+'archivos/transferencia/saveDetalle',{
                DetallePost:DatosJson
            },function(data,textStatus){
                console.log(data);
                $("#mensaje").html(data.response_msg);
                    data.valor.forEach(function(element){
                        var id="'"+btoa(element.ID_DOCUMENTO)+"'";
                        var nombre="'"+element.DOCUMENTO+"'";
                       html+='<tr>';
                        html+='<td>'+element.CODIGO_DOCUMENTO+'</td>';
                        html+='<td>'+element.CORRELATIVO_CENTRAL+'</td>';
                        html+='<td style="display:none;" class="Iddocumento">'+element.ID_DOCUMENTO+'</td>';
                        html+='<td style="display:none;" class="Iddetalle">'+element.ID_DETALLE_TRANSFERENCIA+'</td>';

                        html+="<td>"+element.FECHA_SALIDA_ARCHIVO_CENTRAL+"</td>";
                        html+="<td>"+element.FECHA_REGRESO_ARCHIVO_CENTRAL+"</td>";
                        html+="<td>"+element.OBSERVACIONES+"</td>";                    
                        html+='<td><button type="button"  title="Eliminar Estudio"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button>  <button type="button"  title="Regresar" onclick="retornarDocumento('+id+','+nombre+');" class="btn btn-primary btn-xs "><span class="glyphicon glyphicon-arrow-left"></span></button></td>';
                        html+='</tr>';
                    });
                    console.log(html);
                    $("#DocumentosTablaBody").html(html);
                    //$("#trans_tabla").show();                
            },"json").fail(function(response){
                $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                        console.log('Error: ' + response.responseText);
            });
        });

        $(document).on('click', '.btnEliminar',function(){
            var detalle=new Object();
            detalle.id=$(this).closest("tr").find(".Iddetalle").text();
            detalle.id_documento=$(this).closest("tr").find(".Iddocumento").text();
            console.log(detalle);
            var DatosJson=JSON.stringify(detalle);
            $("#mensaje").html("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
            $.post(baseurl+'archivos/transferencia/deleteDetalle',{
                DetallePost:DatosJson
            },function(data,textStatus){
                $("#mensaje").html(data.response_msg);
            },"json").fail(function(response){
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);                  
            });
            $(this).closest("tr").remove();
        });     
    });
</script>