<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 
	$descripcion= array(
		'name' 		=> 'descripcion',
		'id'		=> 'descripcion',
		'value'		=> set_value('codigo',@$enfermedad[0]->DESCRIPCION),
		'type'		=> 'textarea',
		'rows'		=>	4,
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion',
		'onkeypress'	=> 'return validarn(event)'

	);

	$codigo= array(
		'name' 		=> 'codigo',
		'id'		=> 'codigo',
		'value'		=> set_value('codigo',@$enfermedad[0]->CODIGO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Descripcion'

	);
?>
<input type="hidden" name="id" id="id" value="<?php echo @$enfermedad[0]->ID_CAT_ENFERMEDAD?>">

<div class="content-wrapper">
	<section class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
		
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Datos</h3>
			</div>
			<div class="box-body">
				<div id="mensaje"></div>
				<form class="form-horizontal" name="formulario" id="formulario" role="form"> 
					<div class="form-group">
						<label for='descripcion' class="form-label col-sm-2">Descripcion:</label>
						<div class="col-sm-10">
							<?php echo form_textarea($descripcion);?>
						</div>
					</div>		
					<div class="form-group">
						<label for='descripcion' class="form-label col-sm-2">Codigo:</label>
						<div class="col-sm-10">
							<?php echo form_input($codigo);?>
						</div>
					</div>										
				</form>
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	
				
			</div>
			
		</div>
		
	</section>
	
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_enfermedad"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"catalogos/cat_enfermedad"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
</script>
<script type="text/javascript" src="<?php echo base_url('js/JsonEnfermedad.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>