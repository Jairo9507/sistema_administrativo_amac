<?defined('BASEPATH') OR exit('No direct script access allowed');

?>
<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"catalogos/cat_enfermedad"
        } );
    }
    function eliminarEnfermedad(id,nombre)
    {
        alertify.confirm("Eliminar enfermedad","¿Seguro de eliminar la enfermedad "+nombre+"?",
            function(){
             document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
            var enfermedad=new Object();
            enfermedad.id=id;
            var DatosJson=JSON.stringify(enfermedad);
            $.post(baseurl+'catalogos/cat_enfermedad/eliminar',{
                EnfermedadPost:DatosJson
            },function(data,textStatus){
                console.log(data);
                $("#mensaje").html(data.response_msg);
            },"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);
            });;             

        },function(){

        })
    }
</script>
<div class="content-wrapper">
	<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>			
	</section>
	<section class="content">
		<div class="box">
            <div id="mensaje"></div>
			<div class="box-header">
				<h3 class="header-title">Lista de Enfermedades</h3>
			</div>
                	<div class="row">
                		<div class="col-sm-2 pull-right">
                            <a href="cat_enfermedad/nuevo" align="right" id="btnmodal" data-backdrop="static" data-keyboard="false" data-target="#modal_nuevo" data-toggle="modal"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>                				
                		</div>
                	</div>
            <div class="box-body">
                <div id="mensaje"></div>
            	<table id="enfemedades" class="table table-bordered table-striped">
            		<thead>
            			<th>Nombre</th>
            			<th>Codigo</th>
            			<th>Estado</th>
            			<th></th>
            		</thead>
            		<tbody>
            			<?php 
            				if ($enfermedades) {
            					foreach ($enfermedades as $e) {
            						$enfermedadId=base64_encode($e->ID_CAT_ENFERMEDAD);
            						echo "<tr>";
            						echo "<td>".$e->DESCRIPCION."</td>";
            						echo "<td>".$e->CODIGO."</td>";
            						if ($e->ESTADO==1) {
            							echo "<td>ACTIVO</td>";
            						}
            						echo "<td><a href='cat_enfermedad/editar/".$enfermedadId."' ><button type='button' title='Editar Perfil' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";

            			 ?>
									<button type="button"  title="Eliminar Enfermedad" onclick="eliminarEnfermedad('<?php echo $enfermedadId?>','<?php echo $e->DESCRIPCION ?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>
            			 <?php 
									echo "</td>";
            						echo "</tr>";
            					}
            				}

            			  ?>
            		</tbody>
            	</table>
            </div>										
		</div>
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url('js/JsValidacion.js');?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#enfemedades").DataTable({
     paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }    
		});
	});
</script>
<div class="modal fade text-center" id="modal_nuevo">
  <div class="modal-dialog" >
    <div class="modal-content">
    </div>
  </div>
</div>