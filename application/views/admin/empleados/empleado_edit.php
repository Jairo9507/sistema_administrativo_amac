<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	$codigo_empleado=array(
		'name' 		=> 'codigo_empleado',
		'id'		=> 'codigo_empleado',
		'value'		=> set_value('codigo_empleado',@$empleado[0]->CODIGO_EMPLEADO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'C&oacute;digo'
	);

	$primer_nombre=array(
		'name' 		=> 'primer_nombre',
		'id'		=> 'primer_nombre',
		'value'		=> set_value('primer_nombre',@$empleado[0]->PRIMER_NOMBRE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Primer nombre',
		'onkeypress'	=> 'return validarn(event)'	
	);

	$segundo_nombre=array(
		'name' 		=> 'segundo_nombre',
		'id'		=> 'segundo_nombre',
		'value'		=> set_value('segundo_nombre',@$empleado[0]->SEGUNDO_NOMBRE),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Segundo nombre',
		'onkeypress'	=> 'return validarn(event)'	
	);

	$primer_apellido=array(
		'name' 		=> 'primer_apellido',
		'id'		=> 'primer_apellido',
		'value'		=> set_value('primer_apellido',@$empleado[0]->PRIMER_APELLIDO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Primer apellido',
		'onkeypress'	=> 'return validarn(event)'	
	);

	$segundo_apellido=array(
		'name' 		=> 'segundo_apellido',
		'id'		=> 'segundo_apellido',
		'value'		=> set_value('segundo_apellido',@$empleado[0]->SEGUNDO_APELLIDO),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Segundo apellido',
		'onkeypress'	=> 'return validarn(event)'			
	);

	$apellido_casada=array(
		'name' 		=> 'apellido_casada',
		'id'		=> 'apellido_casada',
		'value'		=> set_value('apellido_casada',@$empleado[0]->APELLIDO_CASADA),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Apellido casada',
		'onkeypress'	=> 'return validarn(event)'	
	);

	$masculino= array(
		'name' 		=> 'genero',
		'id'		=> 'masculino',
		'value'		=> set_value('genero','M'),
		'type'		=> 'radio'
	);	

	$femenino= array(
		'name' 		=> 'genero',
		'id'		=> 'femenino',
		'value'		=> set_value('genro','F'),
		'type'		=> 'radio'
	);		
	$estados = array(
	  '' => 'Seleccionar',
	  'Soltero/a' => 'Soltero/a',
	  'Casado/a' => 'Casado/a',
	  'Divorciado/a' => 'Divorciado/a',
	  'Viudo/a' => 'Viudo/a',
	);	
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dist/css/alertify.css')?>">
<input type="hidden" name="id" id="id" value="<?php echo @$empleado[0]->ID_EMPLEADO?>">
<div class="content-wrapper">
	<div class="content-header">
		<?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>		
	</div>
	<div class="content">
		<div class="box">
			<div id="mensaje"></div>
			<div class="box-header">
				<h3 class="box-title">Editar perfil</h3>				
			</div>
			<div class="box-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
				<div class="form-group">
					<label for="primer_nombre" class="col-sm-2 form-label">Primer nombre<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<?php echo form_input($primer_nombre);?>
					</div>
					<label for="segundo_nombre" class="col-sm-2 form-label">Segundo nombre:</label>
					<div class="col-sm-4">
						<?php echo form_input($segundo_nombre); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="primer_apellido" class="col-sm-2 form-label">Primer apellido<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<?php echo form_input($primer_apellido);?>
					</div>
					<label for="segundo_apellido" class="col-sm-2 form-label">Segundo Apellido:</label>
					<div class="col-sm-4">
						<?php echo form_input($segundo_apellido); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="apellido_casada" class="col-sm-2 form-label">Apellido de casada:</label>
					<div class="col-sm-4">
						<?php echo form_input($apellido_casada); ?>
					</div>
					<label for="estados" class="form-label col-sm-2">Estado civil:</label>
					<div class="col-sm-4">
						<?php 
							echo form_dropdown('estado',$estados,@$empleado[0]->ESTADO_CIVIL,'class="form-control" id="estado"');
						 ?>
					</div>
				</div>			

				<div class="form-group">
					<label for='genero' class="form-label col-sm-2">Genero<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">                
			                <div class="col-sm-4 radio">
			                    <label>Masculino <?php if (@$empleado[0]->SEXO=='M') {
	                  	        		echo '<input type="radio" value="M" name="genero" id="masculino" checked="checked">';  	
	                    			} else {echo '<input type="radio" name="genero" id="masculino" value="M" >';}  ?></label>
			                </div>

			                <div class="col-sm-4 radio">
			                    <label>Femenino <?php if (@$empleado[0]->SEXO=='F') {
	                  	        		echo '<input type="radio" value="F" name="genero" id="femenino" checked="checked">';  	
	                    			} else {echo '<input type="radio" name="genero" id="femenino" value="M" >';}?></label>
			                </div>
					</div>						
					<label for="codigo_empleado" class="col-sm-2 form-label">C&oacute;digo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<?php echo form_input($codigo_empleado); ?>
					</div>
				</div>
				<div class="form-group">
						<label for="unidad_id" class="col-sm-2 form-label">Unidad<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<select id="unidad_id" style="width: 90%!important;">
							<option value="">Seleccione</option>
							<?php 
								if ($unidades) {
									foreach ($unidades as $u) {
										if ($u->ID_UNIDAD==@$empleado[0]->UNIDAD) {
										echo "<option value='".$u->ID_UNIDAD."' selected>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
										} else {
										echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
										}
										
									}
								}
							 ?>
						</select>
					</div>
					<label for="municipio" class="col-sm-2 form-label">Municipio:</label>
					<div class="col-sm-4">
						<select id="municipio_id" style="width: 90%!important;">
							<option value="">Seleccione</option>
							<?php 
								if ($municipios) {
									foreach ($municipios as $m) {
										if ($m->ID_CAT_MUNICIPIO==@$empleado[0]->MUNICIPIO) {
										echo "<option value='".$m->ID_CAT_MUNICIPIO."' selected>".$m->DESCRIPCION."</option>";
										} else {
										echo "<option value='".$m->ID_CAT_MUNICIPIO."'>".$m->DESCRIPCION."</option>";
										}
										

									}
								}
							 ?>
						</select>
					</div>										
				</div>
		</form>
			</div>
			<div class="box-footer">			
					<button type="button" class="btn btn-default " onclick="regresar()">Cancelar</button>
					<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"admin/empleado"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"admin/empleado"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }	
	$(document).ready(function(){
		$("#unidad_id").select2();
		$("#municipio_id").select2();
	})
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/JsonEmpleado.js"></script>