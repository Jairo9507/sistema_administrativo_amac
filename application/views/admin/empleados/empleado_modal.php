<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$codigo_empleado=array(
		'name' 		=> 'codigo_empleado',
		'id'		=> 'codigo_empleado',
		'value'		=> set_value('nombre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'C&oacute;digo'
	);

	$primer_nombre=array(
		'name' 		=> 'primer_nombre',
		'id'		=> 'primer_nombre',
		'value'		=> set_value('nombre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Primer nombre',
		'onkeypress'	=> 'return validarn(event)'	
	);

	$segundo_nombre=array(
		'name' 		=> 'segundo_nombre',
		'id'		=> 'segundo_nombre',
		'value'		=> set_value('nombre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Segundo nombre',
		'onkeypress'	=> 'return validarn(event)'	
	);

	$primer_apellido=array(
		'name' 		=> 'primer_apellido',
		'id'		=> 'primer_apellido',
		'value'		=> set_value('nombre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Primer apellido',
		'onkeypress'	=> 'return validarn(event)'	
	);

	$segundo_apellido=array(
		'name' 		=> 'segundo_apellido',
		'id'		=> 'segundo_apellido',
		'value'		=> set_value('nombre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Segundo apellido',
		'onkeypress'	=> 'return validarn(event)'			
	);

	$apellido_casada=array(
		'name' 		=> 'apellido_casada',
		'id'		=> 'apellido_casada',
		'value'		=> set_value('nombre',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Apellido casada',
		'onkeypress'	=> 'return validarn(event)'	
	);

	$masculino= array(
		'name' 		=> 'genero',
		'id'		=> 'masculino',
		'value'		=> set_value('genero','M'),
		'type'		=> 'radio'
	);	

	$femenino= array(
		'name' 		=> 'genero',
		'id'		=> 'femenino',
		'value'		=> set_value('genro','F'),
		'type'		=> 'radio'
	);		
	$estados = array(
	  '' => 'Seleccionar',
	  'Soltero/a' => 'Soltero/a',
	  'Casado/a' => 'Casado/a',
	  'Divorciado/a' => 'Divorciado/a',
	  'Viudo/a' => 'Viudo/a',
	);	
?>
<input type="hidden" name="id" value="" id="id">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" >
			<span >x</span>
		</button>
		<h4 class="modal-title"><?php echo $pagetitle; ?></h4>
	</div>
	<div class="modal-body">
		<form class="form-horizontal" name="formulario" id="formulario" role="form">
				<div class="form-group">
					<label for="primer_nombre" class="col-sm-2 form-label">Primer nombre<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<?php echo form_input($primer_nombre);?>
					</div>
					<label for="segundo_nombre" class="col-sm-2 form-label">Segundo nombre:</label>
					<div class="col-sm-4">
						<?php echo form_input($segundo_nombre); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="primer_apellido" class="col-sm-2 form-label">Primer apellido<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<?php echo form_input($primer_apellido);?>
					</div>
					<label for="segundo_apellido" class="col-sm-2 form-label">Segundo Apellido:</label>
					<div class="col-sm-4">
						<?php echo form_input($segundo_apellido); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="apellido_casada" class="col-sm-2 form-label">Apellido de casada:</label>
					<div class="col-sm-4">
						<?php echo form_input($apellido_casada); ?>
					</div>
					<label for="estados" class="form-label col-sm-2">Estado civil:</label>
					<div class="col-sm-4">
						<?php 
							echo form_dropdown('estado',$estados,'','class="form-control" id="estado"');
						 ?>
					</div>
				</div>			

				<div class="form-group">
					<label for='genero' class="form-label col-sm-2">Genero<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">                
		                <div class="col-sm-4 ">
		                    <label>Masculino <?php echo form_radio($masculino); ?></label>
		                </div>

		                <div class="col-sm-4 ">
		                    <label>Femenino <?php echo form_radio($femenino);?></label>
		                </div>
					</div>						
					<label for="codigo_empleado" class="col-sm-2 form-label">C&oacute;digo<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<?php echo form_input($codigo_empleado); ?>
					</div>
				</div>
				<div class="form-group">
						<label for="unidad_id" class="col-sm-2 form-label">Unidad<span style="color: #F20A06; font-size: 15px;">*</span>:</label>
					<div class="col-sm-4">
						<select id="unidad_id" style="width: 90%!important;">
							<option value="">Seleccione</option>
							<?php 
								if ($unidades) {
									foreach ($unidades as $u) {
										echo "<option value='".$u->ID_UNIDAD."'>".$u->CODIGO_UNIDAD." ".$u->DESCRIPCION."</option>";
									}
								}
							 ?>
						</select>
					</div>
					<label for="municipio" class="col-sm-2 form-label">Municipio:</label>
					<div class="col-sm-4">
						<select id="municipio_id" style="width: 90%!important;">
							<option value="">Seleccione</option>
							<?php 
								if ($municipios) {
									foreach ($municipios as $m) {
										echo "<option value='".$m->ID_CAT_MUNICIPIO."'>".$m->DESCRIPCION."</option>";
									}
								}
							 ?>
						</select>
					</div>										
				</div>
		</form>
	</div>
	<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>		
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>js/JsonEmpleado.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#unidad_id").select2();
		$("#municipio_id").select2();
	});
</script>