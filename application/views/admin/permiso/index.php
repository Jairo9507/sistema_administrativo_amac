<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url($frameworks_dir.'/bootstrap/css/bootstrap.min.css');?>">
  <script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
 <script src="<?php echo base_url();?>assets/frameworks/bootstrap/dist/js/bootstrap.min.js"></script>             
<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>

<div class="content-wrapper">
	            <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>
                <div class="box box-primary">
                	<div class="box-header">
                		<h3 class="box-title">Privilegios del Sistema</h3>
                	</div>
                	<div class="row">
                		<div class="col-md-12">
                            <div id="mensaje"></div>
		                	<div class="form-group col-md-6">
		                		<label>Perfil</label>
						            <select id="idListPerfil" class="form-control js-example-basic-single">
						            	<option value="0" selected="selected">Elegir Perfil</option>
						            	<?php 
						            		if($perfil){
						            			foreach ($perfil as $p) {
						            				echo "<option value='".$p->ID_PERFIL_USUARIO."'>";
						            				echo "".$p->NOMBRE."</option>";
						            			}
						            		}
						            	?>
						            </select>                		
		                	</div>                			
                		</div>
                	</div>

            <br />

            <div class="row">
                <div class="col-md-12 text-center">

                    <div class="col-md-3">
                        <div class="box box-info">
                            <div class="form-group">
                                <label for="gridModulos" class="form-label col-sm-12">Modulos Registrados</label>
                               <select id="gridModulos" multiple="multiple" name="modulos[]"  class="js-example-basic-single col-sm-12 modulos" disabled="">
                                    <option value="0">Elegir Modulo</option>
                                        <?php 
                                         /*  if($modulos){
                                                
                                                foreach ($modulos as $m) {
                                                    echo "<optgroup label='".$m->DESCRIPCION."'>";
                                                    echo "<option value='".$m->ID_MENU."' >".$m->DESCRIPCION."</option>";
                                                    if($submodulos){
                                                        foreach ($submodulos as $s) {
                                                            if ($s->ID_DEPENDE==$m->ID_MENU) {
                                                                echo "<option value='".$s->ID_MENU."' >".$s->DESCRIPCION."</option>";
                                                            }
                                                        }
                                                    }
                                                    echo "</optgroup>";
                                                }
                                            }*/
                                        ?>
                                </select>

                            </div>
                        </div><!-- /.panel -->
                    </div>

                    <div class="col-md-3">
                        <div class="box box-danger">
                            <div class="form-group">
                                <label for="gridOpcionesD" class="form-label col-md-12">Modulos Denegados</label>
                                <select id="gridOpcionesD" name="modulosdenegados[]" multiple="multiple" disabled="disabled" class="js-example-basic-single col-sm-12" ></select>
                            </div>
                        </div><!-- /.panel -->
                    </div>

                    <div class="col-md-3 ">

                        <div class="btn-group-vertical panel panel-danger" data-toggle="buttons" >
                            <label id="ootorgadas_unselect" class="btn btn-danger glyphicon glyphicon-check">
                                <input type="radio" autocomplete="off" class="">
                            </label>

                            <label id="ootorgadas_one" class="btn btn-default">
                                <input type="radio" autocomplete="off"><span class="glyphicon glyphicon-step-backward"></span> Mover
                            </label>
                        </div>

                        <div class="btn-group-vertical panel panel-success" data-toggle="buttons">
                            <label id="omodulo_unselect" class="btn btn-success glyphicon glyphicon-check">
                                <input type="radio" autocomplete="off" class="">
                            </label>
                            <label id="omodulo_one" class="btn btn-default">
                                <input type="radio" autocomplete="off">Mover <span class="glyphicon glyphicon-step-forward"></span>
                            </label>
                        </div>

                    </div>

                    <div class="col-md-3">
                        <div class="box box-success">
                            <div class="form-group">
                                <label for="gridOpcionesO" class="form-label col-sm-12">Modulos permitidos</label>
                                <select id="gridOpcionesO" multiple="multiple" name="modulospermitidos[]" disabled="disabled"  class="js-example-basic-single col-sm-12" ></select>
                            </div>
                        </div><!-- /.panel -->
                    </div>

                </div> <!-- /.col-md-12 -->
            </div><!-- /.row -->                	

                </div>		
</div>

       
<script type="text/javascript">
        var baseurl='<?php echo base_url();?>';
        var html='';
     $(document).ready(function(){
        $("#idListPerfil").select2();
        $("#gridModulos").select2();
        $("#gridOpcionesD").select2();
        $("#gridOpcionesO").select2();
        $("#idListPerfil").change(function(){
            var perfil=new Object();
            perfil.id=$(this).val();    
            console.log(perfil.id);        
            DatosJson=JSON.stringify(perfil);
            //alert($(this).val());
            //if ($(this).val()==='0') {alert("cero");}
            if (perfil.id!='' && $(this).val()!='0') {
            $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");                
                $.post(baseurl+'admin/permiso/obtenerModulosPermitidos',{
                    PerfilPost:DatosJson
                },function(data,textStatus){
                        var datos=JSON.parse(data);
                        //console.log(datos);
                        html='';
                        datos.forEach(function(element){
                            if (element.ID_DEPENDE==='0') {
                                console.log(element);
                                html +='<optgroup label="'+element.DESCRIPCION+'">'
                                html +='<option value="'+element.ID_MENU+'">'+element.DESCRIPCION+'</option>';
                                datos.forEach(function(submodulo){
                                    if (submodulo.ID_DEPENDE===element.ID_MENU) {
                                        html+='<option value="'+submodulo.ID_MENU+'">'+submodulo.DESCRIPCION+'</option>';
                                    }
                                });
                                html +='</optgroup>';
                            }
                        });
                        console.log(html);
                        $("#gridModulos").removeAttr('disabled');                        
                        if ($("#gridModulos").text()!='') {
                            $("#gridModulos").empty();
                            $("#gridModulos").append(html);
                        } else {
                            $("#gridModulos").append(html);
                        }
                        
                }).fail(function(response){
                    console.log("Error "+response.responseText);
                });;
                $.post(baseurl+'admin/permiso/obtenerModulosDenegados',{
                    PerfilPost:DatosJson
                },function(data,textStatus){
                    var datos=JSON.parse(data);
                    console.log("Denegados "+data);
                    if(datos!=null){
                        html='';
                        datos.forEach(function(element){
                            if (element.ID_DEPENDE==='0') {
                                //console.log("Denegados "+element);
                                html +='<optgroup label="'+element.DESCRIPCION+'">'
                                
                                html +='<option value="'+element.ID_MENU+'"disabled>'+element.DESCRIPCION+'</option>';    
                                   datos.forEach(function(submodulo){
                                    if (submodulo.ID_DEPENDE===element.ID_MENU) {
                                        html+='<option value="'+submodulo.ID_MENU+'">'+submodulo.DESCRIPCION+'</option>';
                                    }
                                });
                                html +='</optgroup>';
                            } else {
                                html +='<option value="'+element.ID_MENU+'">'+element.DESCRIPCION+'</option>';
                            } 
                        });                        

                    }

                    $("#gridOpcionesD").removeAttr('disabled');
                    if ($("#gridOpcionesD").text()!='') {
                        $("#gridOpcionesD").empty();
                        $("#gridOpcionesD").append(html);
                    } else {
                        $("#gridOpcionesD").append(html);
                    }                
                }).fail(function(response){
                    console.log("Error: "+response.responseText);
                });;
                $.post(baseurl+'admin/permiso/obtenerModuloAcceso',{
                    PerfilPost:DatosJson
                },function(data,textStatus){
                    console.log(data);                    
                    var datos=JSON.parse(data);
                    if(datos!=null){
                        html='';
                        datos.forEach(function(element){
                            if (element.ID_DEPENDE==='0') {
                                console.log(element);
                                html +='<optgroup label="'+element.DESCRIPCION+'">'
                                html +='<option value="'+element.ID_MENU+'">'+element.DESCRIPCION+'</option>';
                                datos.forEach(function(submodulo){
                                    if (submodulo.ID_DEPENDE===element.ID_MENU) {
                                        html+='<option value="'+submodulo.ID_MENU+'">'+submodulo.DESCRIPCION+'</option>';
                                    }
                                });
                                html +='</optgroup>';
                            }
                        });                        

                    }
                    $("#gridOpcionesO").removeAttr('disabled');
                    if ($("#gridOpcionesO").text()!='') {
                        $("#gridOpcionesO").empty();
                        $("#gridOpcionesO").append(html);
                    } else {
                        $("#gridOpcionesO").append(html);
                    }
                    $("#mensaje").empty();                   
                }).fail(function(response){
                    console.log("Error: "+response.responseText);
                });;

            } else if($(this).val()==='0') {
               alert("Este no es un perfil de usuario");
               $("#gridModulos").prop("disabled",true);
               $("#gridOpcionesD").prop("disabled",true);
               $("#gridOpcionesO").prop("disabled",true);
            }
        });
        $("#omodulo_one").click(function(){
            $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
            if ($("#gridModulos").val()!='') {
                console.log("gridModulos no vacio");
            var selectedmodulos=$("#gridModulos").val();
            console.log(selectedmodulos);
            var Permiso= new Object();
            Permiso.idusuario=$("#idListPerfil").val();
            Permiso.idpermisos=selectedmodulos;
            console.log(Permiso);
            var DatosJson=JSON.stringify(Permiso);
            $.post(baseurl+'admin/permiso/save',{
                PerfilPost:DatosJson
            },function(data,textStatus){
                console.log(data);
                var datos=JSON.parse(data)
                console.log(datos);
                $("#mensaje").html(datos.response_msg);
            }).fail(function(response){
                console.log("Error: "+response.responseText);
            });;

            } else if($("#gridOpcionesD").val()!='') {
                console.log("gridOpcionesO no vacio "+$("#gridOpcionesD").val());
            var Permiso= new Object();
            var selectedmodulos=$("#gridOpcionesD").val();
            Permiso.idusuario=$("#idListPerfil").val();
            Permiso.eliminar=false;
            Permiso.idpermisos=selectedmodulos;
            console.log(Permiso);
            var DatosJson=JSON.stringify(Permiso);                        
            $.post(baseurl+'admin/permiso/update',{
                PerfilPost:DatosJson
            },function(data,textStatus){
                console.log(data);
                var datos=JSON.parse(data)
                console.log(datos);
                $("#mensaje").html(datos.response_msg);
            }).fail(function(response){
                console.log("Error: "+response.responseText);
            });;

            }            
        });
        $("#ootorgadas_one").click(function(){
            $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");
            console.log(selectedmodulos);
            var Permiso= new Object();
            var selectedmodulos=$("#gridOpcionesO").val();
            Permiso.idusuario=$("#idListPerfil").val();
            Permiso.idpermisos=selectedmodulos;
            Permiso.eliminar=true;
            console.log(Permiso);
            var DatosJson=JSON.stringify(Permiso);                        
            $.post(baseurl+'admin/permiso/update',{
                PerfilPost:DatosJson
            },function(data,textStatus){
                console.log(data);
                var datos=JSON.parse(data)
                console.log(datos);
                $("#mensaje").html(datos.response_msg);
            }).fail(function(response){
                console.log("Error: "+response.responseText);
            });;
        });

    });
</script>