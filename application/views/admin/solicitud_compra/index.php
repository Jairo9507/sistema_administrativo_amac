<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<script src="<?php echo base_url();?>assets/frameworks/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"compra/solicitud_compra"
        } );
    }
</script>

<div class="content-wrapper">
	 <section class="content-header">
	 	<?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
	 </section>
	 <section class="content">
	 	<div class="box"> 
            <div class="box-header">
                <h3>Lista de Solicitudes</h3>
            </div>
                        <div class="row">
                            <div class="col-sm-2 pull-right">
                                <a href="solicitud_compra/nuevo" ><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button></a>
                            </div>
                        </div>  
            <div class="box-body">
                <table id="requisciones" class="table table-bordered table-striped">
                    <thead>
                        <th>Correlativo</th>
                        <th>Unidad solicita</th>
                        <th>Fecha solicita</th>
                        <th>Creada por</th>
                        <th>Fecha creada</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php 
                            if ($solicitudes) {
                                foreach ($solicitudes as $s) {
                                    $solicitudId=base64_encode($s->ID_SOLICITUD);
                                    echo "<tr>";       
                                    echo "<td>".$s->CODIGO."</td>";
                                    echo "<td>".$s->DESCRIPCION."</td>";
                                    echo "<td>".$s->FECHA_SOLICITUD."</td>";
                                    echo "<td>".$s->USUARIO_CREACION."</td>";
                                    echo "<td>".$s->FECHA_CREACION."</td>";
                                    echo "<td><a href='solicitud_compra/editar/".$solicitudId."' ><button type='button' title='Editar Requisicion' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";

                         ?>

                         <?php 

                                echo "</td>";
                                echo "</tr>";
                                }
                            }
                          ?>
                    </tbody>
                </table>
            </div>          
        </div>
	 </section>
</div>

<!-- page script -->
<script>
  $(function () {
    $('#requisciones').DataTable({
      paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      "order" : [],
      info        : true,
      autoWidth   : false,
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
            first:"Primero",
            previous:"Anterior",
            next:"Siguiente",
            last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas"
      }        
    });
  })
</script>