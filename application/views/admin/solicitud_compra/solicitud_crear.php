<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php 

	$correlativo= array(
		'name' 		=> 'correlativo',
		'id'		=> 'correlativo',
		'value'		=> set_value('correlativo',''),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'SC001',
        'disabled'=>''
	);

	$fecha =array(
		'name' 		=> 'fecha',
		'id'		=> 'fecha',
		'value'		=> set_value('correlativo',''),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'placeholder' => ''
	);


 ?>
<input type="hidden" name="unidad" id="unidad" value="<?php echo $unidad;?>">
 <div class="content-wrapper">
 	<section class="content-header">
 		<?php echo $pagetitle; ?>
 		        <?php echo $breadcrumb; ?>
 	</section>
 		<div class="box">
 			<div class="box-header">
 		<h3 class="header-title"></h3>
 			</div>
 			<div class="box-body">
 				<div id="mensaje"></div>
 				<form class="form-horizontal" name="formulario" id="formulario" role="form">
 					<div class="form-group">
 						<label for="codigo" class="col-sm-2 form-label">Codigo</label>
 						<div class="col-sm-4">
 							<?php  echo form_input($correlativo);?>
 						</div>
 						<label for="fecha" class="col-sm-2 form-label">Fecha</label>
 						<div class="col-sm-4">
 							<?php  echo form_input($fecha);?>
 						</div> 						
 					</div>
 					<div class="form-group">

 						<div class="col-sm-3 pull-right">
 							<a href="<?php echo base_url('compra/solicitud_compra');?>/vistaProductos" data-backdrop="static" data-keyboard="false" data-target="#modal_producto" data-toggle="modal" align="right" id="btnProducto"><button type="button" class="btn btn-info" ><span class="glyphicon glyphicon-plus"></span> Agregar productos</button></a>
 						</div>
 					</div>
 				</form>
				<div >
					 <div id="tablaProductos" class="col-sm-12" style="display:none ;">
					    <table id="" class="table table-bordered table-striped"  style="width: 99%;position: initial;left: 35px;bottom: 77px;">
					              <thead>
					                <tr>
					                  <th style="width: 15%;">Codigo</th>
					                  <th style="width: 50%;">Nombre</th>
					                  <th style="width: 15%;">Cantidad</th>
					                </tr>
					              </thead>
					              <tbody id="tablaProductosbody">
					              	
					              </tbody>
					    </table>         	
					 </div>	
				</div> 				
 			</div>
 			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar" disabled="" onclick="salir()"><span class="glyphicon glyphicon-saved" ></span> Guardar</button> 				
 			</div>
 		</div>
 </div>

<div class="modal fade text-center" id="modal_producto">
  <div class="modal-dialog" >
    <div class="modal-content">

    </div>
  </div>
</div>
 <script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_producto"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"compra/solicitud_compra"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }    

    function salir()
    {
    	alertify.confirm("¿Desea Regresar?","Asegurese de que presupuesto este correcto, de clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"compra/solicitud_compra"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }

    $(document).ready(function(){
    	$("#listUnidad").select2();
    	var unidad=$("input#unidad").val();
    	console.log(unidad); 	
    	$.ajax({
    		type: "GET",
    		url: baseurl+'compra/solicitud_compra/obtenerCodigo',
    		success: function(data,textStatus){
    			console.log(data);
    			if (data!=null) {
    			var res=data.slice(1,-1);
    			var f=new Date();
    			var int=parseInt(res.substring(10,13))+1;
    			//console.log("int: "+int);
    			console.log("SC"+f.getDate()+f.getMonth()+f.getFullYear()+unidad+"00"+int);
    			$("#correlativo").val("SC"+f.getDate()+f.getMonth()+f.getFullYear()+unidad+"00"+int);    				
    			} else {
    				$("#correlativo").val("SC"+f.getDate()+f.getMonth()+f.getFullYear()+unidad+"001");
    			}
    		}
    	});
    	$("#btnProducto").click(function(e){
    			var solicitud= new Object();
    			solicitud.id='';
    		  solicitud.correlativo=$("input#correlativo").val();
    			solicitud.fecha=$("input#fecha").val();
    			solicitud.unidad_solicita=$("select#listUnidad").val();
    			
    			if(solicitud.fecha==''){
    				e.preventDefault();
    				e.stopPropagation();
    				$("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La solicitud no se puede registrar sin fecha</div>");
    			} else if(solicitud.unidad_solicita=='' || solicitud.unidad_solicita==0)
    			{
					e.preventDefault();
    				e.stopPropagation();
    				$("#mensaje").append("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La solicitud no se puede registrar sin unidad que solicita</div>");    				
    			}
    			var DatosJson=JSON.stringify(solicitud);
    			$.post(baseurl+'compra/solicitud_compra/save',{
    				SolicitudPost: DatosJson
    			}, function(data,textStatus){
    				console.log(data.response_msg);
    				//$("#"+data.campo).focus();
    				$("#mensaje").append(data.response_msg);
    			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    					console.log('Error: ' + response.responseText);

    			});;

    	});

    });
</script>