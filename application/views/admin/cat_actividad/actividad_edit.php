<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php 
	$descripcion = array(
		'name'  => 'descripcion',
		'id'    => 'descripcion',
		'value' => set_value('codigo', @$actividad[0]->DESCRIPCION),
		'type'  =>  'textarea',
		'rows'  =>  4,
		'class' =>  'form-control', 
		'placeholder' => 'Descripcion',
		'onkeypress'  => 'return validar(event)'
	);
?>

<input type="hidden" name="id" id="id" value="<?php echo @$actividad[0]->ID_ACTIVIDAD ?>">

<div class="content-wrapper">
	<div class="content-header">
			<?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>		
		
	</div>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Datos</h3>
			</div>
			<div class="box-body">
				<div id="mensaje"></div>
				<form class="form-horizontal" name="formulario" id="formulario" role="form"> 
					<div class="form-group">
						<label for='descripcion' class="form-label col-sm-2">Descripcion:</label>
						<div class="col-sm-10">
							<?php echo form_textarea($descripcion);?>
						</div>
					</div>					
				</form>
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>	
				
			</div>
			
		</div>
		
	</section>
	
</div>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_actividad"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"catalogos/cat_actividad"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }
$(document).ready(function(){
	$("#descripcion").focus();

	$("#btnGuardar").click(function(){
			var Actividad= new Object();
			 Actividad.id=$("input#id").val();
			 Actividad.descripcion=$("textarea#descripcion").val();
			if(Actividad.id===undefined){
				Actividad.id='';
			}
			console.log(Actividad);
			var DatosJson=JSON.stringify(Actividad);
			// $("#modal_nuevo").modal('hide');
			$("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");

			$.post(baseurl+'catalogos/cat_actividad/guardar',{
				ActividadPost: DatosJson
			},function(data,textStatus){
				$("#mensaje").html(data.response_msg);
			},"json").fail(function(response){
				$("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    					console.log('Error: ' + response.responseText);
			});;
			return false;
	});
});
</script>