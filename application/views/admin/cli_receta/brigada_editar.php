<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


	$fecha =array(
		'name' 		=> 'fecha',
		'id'		=> 'fecha',
		'type'		=> 'date',
		'class'		=> 'form-control',
		'value'		=>set_value('fecha',date('Y-m-d',strtotime(@$brigada[0]->FECHA))),
		'placeholder' => ''
	);


    $cantidad= array(
        'name'      => 'cantidad',
        'id'        => 'cantidad',
        'value'     => set_value('correlativo',''),
        'type'      => 'number',
        'class'     => 'form-control',
        'placeholder' => '',
        'min'       =>'1'
    );    	


	$comunidad=array(
		'name' 		=> 'comunidad',
		'id'		=> 'comunidad',
		'value'		=> set_value('nombre_paciente',@$brigada[0]->COMUNIDAD),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => 'Nombre de la comunidad'

	);

 ?>
 <input type="hidden" name="id" id="id" value="<?php echo @$brigada[0]->ID_BRIGADA?>">
<div class="content-wrapper">
 	<section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>						
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="header-title">Receta</h3>				
			</div>
			<div id="mensaje"></div>
			<div class="box-body">
				<form class="form-horizontal" name="formulario" id="formulario" role="form"> 
	 					<div class="form-group">
							<label for="fecha_consulta" class="col-sm-2 form-label">Fecha Brigada</label>
		 						<div class="col-sm-4">
		 							<?php  echo form_input($fecha);?>
		 						</div>
							<label for="nombre_medico" class="col-sm-2 form-label">Comunidad</label>
		 						<div class="col-sm-4">
		 							<?php  echo form_input($comunidad);?>
		 						</div> 							
	 					</div>			
	 					<div class="form-group">

	 						<div class="col-sm-3 pull-right">
	                            <button type="button" id="btnProducto" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span> Agregar medicamentos</button>

	 						</div>	 						
	 					</div>
	                    <div class="form-group " id="form_productos" style="display: none;">
	                        <label for="productos" class="col-sm-2 form-label">Productos</label>
	                            <div class="col-sm-4">
	                                <select id="listProducto" class="form-control js-example-basic-single" style="width:90%!important; ">
	                                    <option value="">Seleccionar Medicamento</option>
	                                    <?php  
	                                        if ($productos) {
	                                            foreach ($productos as $p) {
                                                    echo "<option value='".$p->ID_MEDICAMENTO."'>".$p->COD_PRODUCTO." ".$p->NOMBRE_PRODUCTO."-".$p->FORMA_FARMACEUTICA."-".$p->PRESENTACION."-".$p->CONCENTRACION."</option>";
	                                            }
	                                        }
	                                    ?>
	                                </select>
	                            </div> 
	                            <label for="cantidad" class="col-sm-2 form-label">Cantidad</label>
	                            <div class="col-sm-2">
	                                <?php echo form_input($cantidad); ?>
	                            </div> 
	                        <div class="col-sm-2 pull-right">
	                            <button type="button" id="btnAgregar" class="btn btn-primary" ><span class="glyphicon glyphicon-plus"></span> </button>                      
	                       </div>
	                    </div> 						
				</form>			
	            <div id="tablaProductos" class="col-sm-12" >
	                                                                   
						    <table id="" class="table table-bordered table-striped"  style="width: 99%;position: initial;left: 35px;bottom: 77px;">
						              <thead>
						                <tr>
						                  <th style="width: 10%;">Codigo</th>
	                                      <th style="width: 10%; display: none;">ID</th>
	                                      <th style="width: 10%; display: none;">RECETA</th>
                                          <th style="display: none;">IDProducto</th>

						                  <th style="width: 50%;">Nombre</th>
						                  <th style="width: 15%;">Cantidad</th>
	                                      <th style="width: 15%;"></th>
						                </tr>
						              </thead>
						              <tbody id="tablaProductosbody">
						              	<?php 

                                            if ($recetas) {

                                                foreach ($recetas as $r) {
                                                    echo "<tr>";
                                                    echo "<td>".$r->COD_PRODUCTO."</td>";
                                                    echo "<td style='display:none;' class='Idmedicamento'>".$r->ID_MEDICAMENTO."</td>";
                                                    echo "<td style='display:none;' class='Idreceta'>".$r->ID_RECETA."</td>";
                                                    echo "<td>".$r->NOMBRE."</td>";
                                                echo "<td class='col-xs-1'> <input type='text' class='form-control' style='text-align:right' id='cantidad_".$r->ID_RECETA."'value='".$r->CANTIDAD."' ></td>";
                                                //echo "<td class='cantidadReceta'>".$r->CANTIDAD."</td>";
                                                    echo '<td><button type="button" title="Editar Cantidad" class="btn btn-success btn-xs btnModificar"><i class="glyphicon glyphicon-pencil"></i></button> <button type="button"  title="Eliminar Receta"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>';
                                                    echo "</tr>";
                                                }
                                            }
                                         ?>
						              </tbody>
						    </table>         	
				</div>					
			</div>
			<div class="box-footer">
				<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
				<button type="submit" class="btn btn-primary pull-right" id="btnGuardar" disabled="" onclick="salir();"><span class="glyphicon glyphicon-saved" ></span> Guardar</button>			
			</div>
		</div>

	</section>	
</div>
 <script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"inventario/requisicion"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"inventario/requisicion"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }    

    function salir()
    {
    	alertify.confirm("¿Desea Regresar?","Asegurese de que la requisicion este correcta, de clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"inventario/requisicion"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

    }
    $(document).ready(function(){
        $("#cantidad","#formulario").keypress(function(e){
            var key = e.charCode || e.keyCode || 0;
            $nit = $(this);
            if (key >=48 && key <= 57) {                     
                if ($nit.val().length === 4) {
                    $nit.val($nit.val() + '-');
                } else if($nit.val().length===9)
                    {
                        e.preventDefault();
                    }            

            } else {
                e.preventDefault();
            }            
        });
        $("#listProducto").select2();
        $("#btnProducto").click(function(){
        	var brigada=new Object();
        	brigada.id=$("input#id").val();
        	brigada.fecha=$("input#fecha").val();
        	brigada.comunidad=$("input#comunidad").val();

        	console.log(brigada);
        	var DatosJson=JSON.stringify(brigada);
        	$.post(baseurl+'clinica/receta/registrarBrigada',{
        		BrigadaPost:DatosJson
        	},function(data,textStatus){
	        		$("#mensaje").html(data.response_msg);
        		if (data.validar) {

	        		//$("input#id").val(data.campo);
	        		$("#form_productos").show();

        		}
        	},"json").fail(function(response){
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);
        	});
        });

        $("#btnAgregar").click(function(){
        	var receta=new Object();
        	receta.id_receta='';
        	receta.id_brigada=$("input#id").val();
        	receta.cantidad=$("input#cantidad").val();
        	receta.id_medicamento=$("select#listProducto").val();
        	receta.nombre=$("select#listProducto option:selected").text();
            receta.fecha=$("input#fecha").val();
            $("#tablaProductos").show();         
            $("#tablaProductosbody").show();
            var html='';
            if (receta.cantidad!=='' || receta.id_medicamento==='') {

            html+='<tr>';
            html+='<td >'+parseInt(receta.nombre)+'</td>';
            html+='<td >'+receta.nombre+'</td>'
            html+='<td class="cantidadReceta">'+receta.cantidad+'</td>';
            html+='<td><button type="button"  title="Eliminar Receta"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>';
            html+='<td class="Idmedicamento" style="display:none;">'+receta.id_medicamento+'</td>';

            } else {
                $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Se debe ingresar los datos del  producto</div>");
            }        
            $("#mensaje").html("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");                    	
        	console.log(receta);
        	var DatosJson=JSON.stringify(receta);
 			$.post(baseurl+'clinica/receta/registrarMovimientoBrigada',{
				RecetaPost:DatosJson
			},function(data,textStatus){
	            console.log("data: "+data.validar);
	            html+='<td class="Idreceta" style="display:none;">'+data.receta_id+'</td>';
	            html+='</tr>';         
	            console.log("html "+html);
	            $("#mensaje").html(data.response_msg);            
	            if (data.validar===true) {
	                $("#tablaProductosbody").append(html);
	            }

	            $("#btnGuardar").attr("disabled", false);

			},"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);				
			});       	
        });
        $(document).on('click', '.btnEliminar', function () {
            // your function here
            var idm=$(this).closest("tr").find(".Idmedicamento").text();
            var id_re=$(this).closest("tr").find(".Idreceta").text();
            var cantidad=$(this).closest("tr").find(".cantidadReceta").text();
            var detalle=new Object();
            detalle.id_receta=id_re;
            detalle.id_medicamento=idm;
            detalle.id_brigada=$("input#id").val(); 
            detalle.cantidad=cantidad;
            console.log(detalle);
            var DatosJson=JSON.stringify(detalle);
            $("#mensaje").html("<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Cargando...</center></div></div>");              
            $.post(baseurl+'clinica/receta/deleteDetalle',{
                DetallePost:DatosJson
            },function(data){
                $("#mensaje").html(data.response_msg);
                $("#btnGuardar").attr("disabled", false);               
            },"json").fail(function(response){
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);                
            });;
             $(this).closest("tr").remove();
        }); 

        $(document).on('click','.btnModificar',function(){
              var idm=$(this).closest("tr").find(".Idmedicamento").text();
            var id_re=$(this).closest("tr").find(".Idreceta").text();
            var cantidad=$(this).closest("tr").find(".cantidadReceta").text();
            var detalle=new Object();
            detalle.id_receta=id_re;
            detalle.id_medicamento=idm;
            detalle.id_brigada=$("input#id").val();
            detalle.cantidad=$("input#cantidad_"+id_re).val();
            console.log(detalle);
            var DatosJson=JSON.stringify(detalle);
            $.post(baseurl+'clinica/receta/registrarMovimientoBrigada',{
                RecetaPost:DatosJson
            },function(data,textStatus){
                $("#mensaje").html(data.response_msg);
                $("#btnGuardar").attr("disabled", false);               
            },"json").fail(function(response){
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: ' + response.responseText);      
            }); 
        });  	
    });
</script>