<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  $fecha_hora=array(
    'name'    => 'fecha_hora',
    'id'    => 'fecha_hora',
    'value'   => set_value('fecha_hora',date('Y/m/d H:i')),
    'type'    => 'text',
    'class'   => 'form-control',
    'placeholder' => ''
  );  
?>
<script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url($frameworks_dir.'/alertify/alertify.min.js')?>"></script>

<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";
    function recargar(){
    alertify.alert("Datos almacenado exitosamente","Se refrescara la vista para continuar",function(){
        window.location.href = baseurl+"clinica/receta"
        } );
    }
    function eliminarBrigada(id,fecha,nombre)
    {
    	alertify.confirm("Eliminar Brigada","¿Seguro de eliminar la brigada realizada en "+nombre+" en la fecha "+fecha+"?",function(){
            document.getElementById('mensaje_brigada').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
            var brigada=new Object();
            brigada.id=id;
            var DatosJson=JSON.stringify(brigada);
            $.post(baseurl+'clinica/receta/eliminarBrigada',{
            	BrigadaPost:DatosJson
            },function(data,textStatus){
            	$("#mensaje_brigada").html(data.response_msg);
            },"json").fail(function(response){
        $("#mensaje_brigada").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
        console.log('Error: ' + response.responseText);
            });    		
    	},function(){

    	});
    }
    function CancelarReceta(id,paciente)
    {
    	alertify.confirm("Pagar receta","¿Se efectuar&aacute; el pago de la receta del paciente "+paciente+"?",function(){
            //var url="clinica/receta/salida/"+id;    		
            document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ baseurl +"/img/gif-load.gif'> Eliminando...</center></div></div>";
           	//document.getElementById('recetaid_'+id).href=url;
            var consulta=new Object();
            consulta.id=id;
            var DatosJson=JSON.stringify(consulta);
            $.post(baseurl+'clinica/receta/cancelarReceta',{
            	ConsultaPost:DatosJson
            },function(data,textStatus){
            	$("#mensaje").html(data.response_msg);
            	//$("#recetaid_"+id).attr("href","");
            },"json").fail(function(response){
		        $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
		        console.log('Error: ' + response.responseText);
            });
    	},function(){

    	});
    }
</script>
<div class="content-wrapper">
		<section class="content-header">
            <?php echo $pagetitle; ?>
            <?php echo $breadcrumb; ?>						
	</section>
	<section class="content">
		<div class="box">
	      	<div id="mensaje"></div>		
			<div class="box-header">
				<h3 class="header-title">Lista de recetas</h3>
			</div>
			<div class="box-body">
			
		        <div class="row">
		            <div class="col-sm-8">
		            <label for="fecha_hora" class="col-sm-2 control-label">Fecha:</label>
		                    <div class="col-sm-4">
		                        <div class='input-group date' id='datetimepicker1'>
		                        <?php
		                          echo form_input($fecha_hora);
		                        ?>                  
		                            <span class="input-group-addon">
		                                <span class="fa fa-clock-o"></span>
		                            </span>
		                        </div>
		                    </div>            
		            </div>          
		        </div>
		        <div class="row">
		        	<div class="col-sm-12">
						<table id="consultas" class="table table-bordered table-striped">
							<thead>
								<th>Fecha consulta</th>
								<th>Doctor</th>
								<th>Nombre Paciente</th>
								<th>Codigo expediente</th>
								<th>Diagnositico</th>
								<th></th>
							</thead>
							<tbody id="recetasTableBody">
								<?php 
									if ($consultas) {
										foreach ($consultas as $c) {
											$consultaId=base64_encode($c->ID_CONSULTA);
											echo "<tr>";
											echo "<td>".date('d-m-Y',strtotime($c->FECHA_CONSULTA))."</td>";
											echo "<td>".$c->nombre_medico."</td>";
											echo "<td>".$c->nombre_paciente."</td>";
											echo "<td>".$c->CODIGO_EXPEDIENTE."</td>";
											echo "<td>".$c->DIAGNOSTICO."</td>";
											if ($c->ESTADO=='T') {
												echo "<td><a  id='recetaid_".$consultaId."'><button type='button' title='' class='btn btn-success btn-xs' disabled='true'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
											} else {
												echo "<td><a href='receta/salida/".$consultaId."' disabled='true'><button type='button' title='' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";												
											}
								 ?>
                				<button type="button"  title="Cancelar receta" onclick="CancelarReceta('<?php echo $consultaId?>','<?php echo $c->nombre_paciente;?>');" class="btn btn-info btn-xs "><span class="glyphicon glyphicon-usd"></span></button>&nbsp;	
								 <?php 
										echo ' <a href="receta/vistaPDF/'.$consultaId.'" data-target="#modal_receta" data-toggle="modal" data-backdrop="static" data-keyboard="false"><button type="button" title="PDF" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-file"></span></button></a></td>';
											echo "</tr>";
										}
									}
								  ?>
							</tbody>
						</table>		    		        		
		        	</div>
    	
		        </div>	
	

			</div>	  

			<div class="box-header">
				<h3 class="header-title">Lista de Brigadas</h3>
			</div>
               <div class="row">
                			<div class="col-sm-2 pull-right">
                                <a href="receta/brigada" "><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Brigada</button></a>                				
                			</div>
                </div>			  
			<div class="box-body">
       	      	<div id="mensaje_brigada"></div>
				<div class="row">		        	
				        	<div class="col-sm-12">
				        		<table id="brigadas" class="table table-bordered table-striped">
				        			<thead>
				        				<th>Fecha realizada</th>
				        				<th>Comunidad</th>
				        				<th>Creado por</th>
				        				<th>Fecha creaci&oacute;n</th>
				        				<th>Modificado por</th>
				        				<th>Fecha modificaci&oacute;n</th>
				        				<th>Estado</th>
				        				<th></th>
				        			</thead>
				        			<tbody>
				        				<?php 
				        					if ($brigadas) {
				        						foreach ($brigadas as $b) {
				        							$brigadaId=base64_encode($b->ID_BRIGADA);
				        							echo "<tr>";
				        							echo "<td>".date('d-m-Y',strtotime($b->FECHA))."</td>";
				        							echo "<td>".$b->COMUNIDAD."</td>";
				        							echo "<td>".$b->USUARIO_CREACION."</td>";
				        							echo "<td>".$b->FECHA_CREACION."</td>";
				        							echo "<td>".$b->USUARIO_MODIFICACION."</td>";
				        							echo "<td>".$b->FECHA_MODIFICACION."</td>";
				        							if ($b->ESTADO=1) {
				        								echo "<td>ACTIVO</td>";
				        							}
				        							echo "<td><a href='receta/editarBrigada/".$brigadaId."' ><button type='button' title='' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-edit'></span> </button> </a>";
				        				 ?>
                				<button type="button"  title="Eliminar Anaquel" onclick="eliminarBrigada('<?php echo $brigadaId?>','<?php echo $b->FECHA ?>','<?php echo $b->COMUNIDAD;?>');" class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-trash"></span></button>	
				        				 <?php 
													echo "</td>";
				        							echo "</tr>";
				        						}
				        					}
				        				  ?>
				        			</tbody>
				        		</table>
				        	</div>
				</div>						
			</div>    				
		</div>
		
	</section>
</div>
<div class="modal fade text-center"  id="modal_receta">
  <div class="modal-dialog" >
    <div class="modal-content">
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$.datetimepicker.setLocale('es');
		jQuery('#fecha_hora').datetimepicker({
					
		   format: 'd/m/Y H:i',
		    value: new Date()			
		});

    $("#fecha_hora").blur(function(){
      var fecha=new Object();
      var html='';
      fecha.fecha=$(this).val();
      console.log(fecha);
      var DatosJson=JSON.stringify(fecha);
      $.post(baseurl+'clinica/receta/actualizarTabla',{
        FechaPost:DatosJson
      },function(data,textStatus){
      		console.log(data);
          data.forEach(function(element){
            console.log(element);
            	var id="'"+btoa(element.ID_CONSULTA)+"'";
            	var fecha="'"+element.FECHA_CONSULTA+"'";
              html+='<tr>';
              html+='<td>'+element.FECHA_CONSULTA+'</td>';
              html+='<td>'+element.nombre_medico+'</td>';
              html+='<td>'+element.nombre_paciente+'</td>';
              html+='<td>'+element.CODIGO_EXPEDIENTE+'</td>';
              html+='<td>'+element.DIAGNOSTICO+'</td>';
              if (element.ESTADO=='T') {
              html+='<td><a  id="recetaid_'+btoa(element.ID_CONSULTA)+'"><button type="button" title="" class="btn btn-success btn-xs" disabled="true"><span class="glyphicon glyphicon-edit"></span></button></a> <button type="button"  title="Cancelar receta" onclick="CancelarReceta('+id+','+fecha+');" class="btn btn-info btn-xs "><span class="glyphicon glyphicon-usd"></span></button>';
				html+=' <a href="receta/vistaPDF/'+btoa(element.ID_CONSULTA)+'" target="_blank" data-target="#modal_receta" data-toggle="modal" data-backdrop="static" data-keyboard="false"><button type="button" title="PDF" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-file"></span></button></a></td>';                   	


              } else {
              html+='<td><a href="receta/salida/'+btoa(element.ID_CONSULTA)+'" class="linkreceta" ><button type="button" title="" class="btn btn-success btn-xs" ><span class="glyphicon glyphicon-edit"></span></button></a> <button type="button"  title="Cancelar receta" onclick="CancelarReceta('+id+','+fecha+');" class="btn btn-info btn-xs "><span class="glyphicon glyphicon-usd"></span></button>';         
              }
              html+=' <a href="receta/vistaPDF/'+btoa(element.ID_CONSULTA)+'" target="_blank" data-target="#modal_receta" data-toggle="modal" data-backdrop="static" data-keyboard="false"><button type="button" title="PDF" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-file"></span></button></a></td>';
              html+='</tr>';
          });
          console.log(html);
          $("#recetasTableBody").html(html);          
        //console.log(data);
      },"json").fail(function(response){
        $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
        console.log('Error: ' + response.responseText);
      });
    }) 		
		$("#consultas").DataTable({
		     paging      : true,
		      lengthChange: true,
		      searching   : true,
		      ordering    : true,
		      "order" : [],
		      info        : true,
		      autoWidth   : false,
		      language: {
		        search:'Buscar:',
		        order: 'Mostrar Entradas',
		        paginate: {
		            first:"Primero",
		            previous:"Anterior",
		            next:"Siguiente",
		            last:"Ultimo"
		        },
		        emptyTable: "No hay informacion disponible",
		        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
		        lengthMenu:"Mostrar _MENU_ Entradas",
		        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
		      }    
		    });
		$("#brigadas").DataTable({
		     paging      : true,
		      lengthChange: true,
		      searching   : true,
		      ordering    : true,
		      "order" : [],
		      info        : true,
		      autoWidth   : false,
		      language: {
		        search:'Buscar:',
		        order: 'Mostrar Entradas',
		        paginate: {
		            first:"Primero",
		            previous:"Anterior",
		            next:"Siguiente",
		            last:"Ultimo"
		        },
		        emptyTable: "No hay informacion disponible",
		        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
		        lengthMenu:"Mostrar _MENU_ Entradas",
		        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
		      }    
		    });		

	});
</script>