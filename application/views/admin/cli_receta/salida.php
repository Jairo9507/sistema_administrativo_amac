<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


	$fecha =array(
		'name' 		=> 'fecha',
		'id'		=> 'fecha',
		'value'		=> set_value('fecha',date("Y-m-d", strtotime(@$consulta[0]->FECHA_CONSULTA))),
		'type'		=> 'date',
		'class'		=> 'form-control',
		'placeholder' => '',
		'disabled'		=>true
	);

	$nombre_paciente=array(
		'name' 		=> 'nombre_paciente',
		'id'		=> 'nombre_paciente',
		'value'		=> set_value('nombre_paciente',@$consulta[0]->nombre_paciente),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '',
		'disabled'	=>true

	);


	$nombre_medico=array(
		'name' 		=> 'nombre_paciente',
		'id'		=> 'nombre_paciente',
		'value'		=> set_value('nombre_paciente',@$consulta[0]->nombre_medico),
		'type'		=> 'text',
		'class'		=> 'form-control',
		'placeholder' => '',
		'disabled'	=>true

	);

 ?>

 <div class="content-wrapper">
 	<section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>						
	</section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="header-title">Datos</h3>
			</div>
			<div id="mensaje"></div>
			<div class="box-body">
				<form class="form-horizontal" name="formulario" id="formulario" role="form">
 					<div class="form-group">
						<label for="fecha_consulta" class="col-sm-2 form-label">Fecha consulta</label>
	 						<div class="col-sm-4">
	 							<?php  echo form_input($fecha);?>
	 						</div>
						<label for="nombre_medico" class="col-sm-2 form-label">Medico</label>
	 						<div class="col-sm-4">
	 							<?php  echo form_input($nombre_medico);?>
	 						</div> 							
 					</div>
 					<div class="form-group">
						<label for="nombre_paciente" class="col-sm-2 form-label">Paciente</label>
	 						<div class="col-sm-4">
	 							<?php  echo form_input($nombre_paciente);?>
	 						</div> 			 						
 					</div>
				</form>
				<table id="recetas" class="table table-bordered table-striped" >
					<thead>
						<th>C&oacute;digo</th>
						<th>Nombre</th>
						<th>Cantidad</th>
						<th style="display: none;">Idproducto</th>
						<th style="display: none;">Idreceta</th>
						<th></th>
					</thead>
					<tbody>
						<?php 
							if ($recetas) {
								foreach ($recetas as $r) {
				                    echo "<tr>";
                                    echo "<td>".$r->COD_PRODUCTO."</td>";
                                    echo "<td>".$r->NOMBRE_PRODUCTO."</td>";
                                    echo "<td class='col-xs-1'> <input disabled='true' type='text' class='form-control' style='text-align:right' id='cantidad_".$r->ID_PRODUCTO."'value='".$r->CANTIDAD."' ></td>";
                                    echo "<td class='Idproducto' style='display:none;'>".$r->ID_PRODUCTO."</td>";
                                    echo "<td class='Idreceta' style='display:none;'>".$r->ID_RECETA."</td>";
                                    if ($r->existe<=0) {
									echo '<td><button type="button" title="Editar Cantidad" class="btn btn-success btn-xs btnRegistrar"><i class="glyphicon glyphicon-ok"></i></button> </td>';
                                    } else {
									echo '<td><button type="button" disabled="true" title="Editar Cantidad" class="btn btn-success btn-xs btnRegistrar"><i class="glyphicon glyphicon-ok"></i></button> </td>';                                    	
                                    }

									echo "</tr>";
								}
							}
						 ?>
					</tbody>
					
				</table>
				
			</div>
			<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="regresar()">Cancelar</button>
			<button type="submit" class="btn btn-primary pull-right" id="btnGuardar"  onclick="salir()"><span class="glyphicon glyphicon-saved" ></span> Guardar</button> 				
			</div>
		</div>
	</section>
 	
 </div>
 <script src="<?php echo base_url($frameworks_dir . '/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/alertify/alertify.min.js'); ?>"></script>
 <script type="text/javascript">
var baseurl = "<?php echo base_url();?>";
	function recargar(){
    alertify.alert("Datos actualizados exitosamente","Datos actualizados exitosamente ¿Desea regresar a la lista?",function(){
        window.location.href = baseurl+"catalogos/cat_medicamento"

        } );
    }
    function regresar()
    {
    	alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    		  window.location.href = baseurl+"catalogos/cat_medicamento"

    	},function(){

    	}).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    }     	
 	$(document).ready(function(){
 		

		$(document).on('click','.btnRegistrar',function(){
			var receta=new Object();
			receta.id_producto=$(this).closest("tr").find(".Idproducto").text();
			receta.id_receta=$(this).closest("tr").find(".Idreceta").text();
			receta.cantidad=$("input#cantidad_"+receta.id_producto).val();
			console.log(receta);
			var DatosJson=JSON.stringify(receta);
			$.post(baseurl+'clinica/receta/registrarMovimiento',{
				RecetaPost:DatosJson
			},function(data,textStatus){
				$("#mensaje").html(data.response_msg);
			},"json").fail(function(response){
               $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
                console.log('Error: ' + response.responseText);				
			});
			$(this).attr('disabled',true);
		});
 	});	
 </script>