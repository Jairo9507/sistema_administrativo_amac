<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class mision extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("mision_model");
		$this->load->helper('download');
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Registro Misiones por vehículo","mision/mision");
	}

	public function index(){
		if (!$this->ion_auth->logged_in() ) {
			redirect("auth/login","refresh");
		} else {
			$this->page_title->push('Misiones de vehiculos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            // $this->data['vales']=$this->registro_vales_model->listarVales();

            $this->template->admin_render("admin/mision/index",$this->data);

		}
	}

	public function guardar(){
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login","refresh");
		} else {
			$config['upload_path'] = '/archivos/misiones/';
        	$config['allowed_types'] = 'pdf|xlsx|docx';
        	$config['max_size'] = '20048';

        	$this->load->library('upload',$config);
        	

   //      	if (!$this->upload->do_upload("archivo")) {
   //          $data['errorArch'] = $this->upload->display_errors();
			// $this->load->view('layout/header');
			// $this->load->view('layout/menu');
			// $this->load->view('vupload',$data);
			// $this->load->view('layout/footer');
   //      } else {
        	$file_info = $this->upload->data();
			$Mision=json_decode($this->input->post("misionPost"));
			$response=array(
				'response_msg'=>'',
				'campo' =>''
			);

			if($Mision->id==''){
				$file_info = $this->upload->data();
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$registrarMision=array(
					'ID_VEHICULO' => $Mision->id_vehiculo,
					'NOMBRE' => $Mision->titulo,
					'RUTA' =>$Mision->archivo,
					'FECHA' => date('Y-m-d H:i:s'),
					// 'KILOMETRAJE_ENTRADA' => $Mision->kilometraje_entrada,
					// 'DESTINO' => $Mision->destino,
					// 'ID_EMPLEADO' =>$Mision->empleado,
					// 'FECHA_CREACION'   =>date('Y-m-d H:i:s'),
					// 'USUARIO_CREACION' => $usuario,
					// 'ID_ACTIVIDAD'	  =>$Mision->id_actividad,
				);
				$this->mision_model->guardarMision($registrarMision);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
			} else {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$updateMision=array(
					'ID_ACTIVIDAD' => $Mision->idActividad,
					'USUARIO_MODIFICACION' => $usuario,
					'FECHA_MODIFICACION'  => date('Y-m-d H:i:s'),
					'ID_EMPLEADO'  => $Mision->idEmpleado,
					'DESTINO' 	  => strtoupper($Mision->destino),
				);
				$this->mision_model->updateMision($Mision->id,$updateMision);
				$response['response_msg']='<script>recargar();</script>';
			}
		}
	
		echo json_encode($response);
	}


	public function guardarArchivo($titulo, $archivo)
	{
		$registro = array(
			'RUTA'  => $archivo,
			'FECHA' => date('Y-m-d H:i:s')
		);
		return $this->db->guardarArchivoMision('misiones', $registro);
	}

	public function downloads(){
        $name='SISTEMA BODEGA.xlsx';
        $data = file_get_contents('./archivos/misiones/'.$name); 
        force_download($name,$data); 
     
	}

}