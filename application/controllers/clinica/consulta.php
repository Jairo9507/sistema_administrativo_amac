<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class consulta extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cli_consulta_model");
		$this->load->model("usuario_model");
		$this->load->model("cli_medico_model");
		$this->load->model("cli_receta_model");
		$this->breadcrumbs->unshift(1,"Consulta","clinica/consulta");	
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Consultas');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
            if (empty($medico)) {
            	$medico=1;
            }

            $this->data['citas']=$this->cli_consulta_model->listarCitaMedico($medico);

            $this->template->admin_render("admin/cli_consulta/index",$this->data);
		}

	}



	public function nuevo($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Crear consulta');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
					$this->breadcrumbs->unshift(2,"Consulta","clinica/consulta/nuevo");	
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $id=base64_decode($id);
            $paciente=$this->cli_consulta_model->ObtenerIdPacienteCita($id);
            $consulta=$this->cli_consulta_model->obtenerIdConsulta($id);
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['enfermedades']=$this->cli_consulta_model->listarEnfermedades();
            $this->data['sustancias']=$this->cli_consulta_model->listarAlergiaSustancias();
            $this->data['alergias']=$this->cli_consulta_model->listarOtrasAlergias();
            $this->data['medicamentos']=$this->cli_consulta_model->listarMedicamentos();
            $this->data['estudios']=$this->cli_consulta_model->listarEstudios();
            $this->data['estudioscitas']=$this->cli_consulta_model->listarEstudiosCita($id);
            $this->data['consulta']=$this->cli_consulta_model->listarConsultaCita($id);
            $this->data['enfermedades_pacientes']=$this->cli_consulta_model->listarEnfermedadPaciente($paciente);
            $this->data['alergias_paciente']=$this->cli_consulta_model->listarAlergiasPaciente($paciente);
            $this->data['consultas']=$this->cli_consulta_model->listarHistorial($paciente);
            $this->data['medicamentosconsulta']=$this->cli_consulta_model->listarMedicamentosCita($consulta);
            $this->template->admin_render("admin/cli_consulta/consulta_crear",$this->data);
		}

	}	
	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Modificar consulta');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Consulta","clinica/consulta/editar");				
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $id=base64_decode($id);
            $paciente=$this->cli_consulta_model->ObtenerIdPacienteCita($id);
            $consulta=$this->cli_consulta_model->obtenerIdConsulta($id);
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['enfermedades']=$this->cli_consulta_model->listarEnfermedades();
            $this->data['sustancias']=$this->cli_consulta_model->listarAlergiaSustancias();
            $this->data['alergias']=$this->cli_consulta_model->listarOtrasAlergias();
            $this->data['medicamentos']=$this->cli_consulta_model->listarMedicamentos();
            $this->data['estudios']=$this->cli_consulta_model->listarEstudios();
            $this->data['estudioscitas']=$this->cli_consulta_model->listarEstudiosCita($id);
            $this->data['consulta']=$this->cli_consulta_model->listarConsultaCita($id);
            $this->data['enfermedades_pacientes']=$this->cli_consulta_model->listarEnfermedadPaciente($paciente);
            $this->data['alergias_paciente']=$this->cli_consulta_model->listarAlergiasPaciente($paciente);
            $this->data['consultas']=$this->cli_consulta_model->listarHistorial($paciente);
            $this->data['medicamentosconsulta']=$this->cli_consulta_model->listarMedicamentosCita($consulta);
            $this->template->admin_render("admin/cli_consulta/consulta_crear",$this->data);
		}

	}	

	public function actualizarTabla()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$datos=json_decode($this->input->post("FechaPost"));

			$medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
            if (empty($medico)) {
            	$medico=1;
            }
            $date= new \DateTime($datos->fecha);
            
			$consultas=$this->cli_consulta_model->listarCitasFecha($medico,date('Y-m-d',strtotime($date->format('d/m/Y'))));
			$json=json_encode($consultas);

		}
		echo $json;
	}

	public function consultaCrear()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$cita=json_decode($this->input->post("CitaPost"));
			
			$datos=$this->cli_consulta_model->buscarPacienteCita($cita->id);
			
			$updateCita= array(
				'ESTADO' =>'En consulta'
			);
			$registrarConsulta=array(
				'ID_CITA'=>$cita->id,
				'FECHA_CONSULTA'=>date('Y-m-d H:i:s'),
				'ESTADO'=>'A'
			);
			$this->cli_consulta_model->updateCita($cita->id,$updateCita);
			$id_consulta=$this->cli_consulta_model->saveConsulta($registrarConsulta);
			$encontrado=$this->cli_consulta_model->ExisteAntecedente($datos[0]->ID_PACIENTE);
			if ($encontrado==false) {
				$registrarEmbarazo=array('TOTAL_EMBARAZO'=>null);
				$id_embarazo=$this->cli_consulta_model->saveEmbarazo($registrarEmbarazo);

				$registrarMenstruacion=array('PRIMERA_MENSTRUACION'=>null);
				$id_menstruacion=$this->cli_consulta_model->saveMenstruacion($registrarMenstruacion);
				$registrarInformacion=array('ULTIMA_PAPANICOLAU'=>null);			
				$id_informacion=$this->cli_consulta_model->saveInformacionInteres($registrarInformacion);
				$registrarAntecedente= array(
					'ID_PACIENTE'=>$datos[0]->ID_PACIENTE,
					'ID_MENSTRUACION'=>$id_menstruacion,
					'ID_EMBARAZO'=>$id_embarazo,
					'ID_INFORMACION_INTERES'=>$id_informacion,
				);
				$this->cli_consulta_model->saveAntecedente($registrarAntecedente);
			}
			$registrarExploracion=array(
				'ID_CONSULTA'=>$id_consulta
			);
			$this->cli_consulta_model->saveExploracion($registrarExploracion);
			$response=array(
				'response_msg'=>'OK'
			);
		}
		echo json_encode($response);
	}

	public function saveAntecedentes()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$antecedente=json_decode($this->input->post("AntecedentePost"));
			
			$response=array(
				'response_msg'=>''
			);
			$updateAntecedente= array(
				'DATOS_GENERALES'=>$antecedente->datos_generales,
				'HEREDITARIO_FAMILIAR'=>$antecedente->hereditario,
				'PERSONAL_NOPATOLOGICO'=>$antecedente->personal_npatologico,
				'PERSONAL_PATOLOGICO'=>$antecedente->personal_patologico
			);

			$this->cli_consulta_model->updateAntecedente($antecedente->id,$updateAntecedente);
			$response['response_msg']='<script>alertify.success("Los registros fueron guardados adecuadamente");</script>';
		}
		echo json_encode($response);
		
	}

	public function saveEnfermedadPaciente()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$enfermedad=json_decode($this->input->post("EnfermedadPost"));
			$response=array(
				'response_msg'=>''
			);
			$registrarEnfermedad= array(
				'ID_ENFERMEDAD'=>$enfermedad->id,
				'ID_PACIENTE'=>$enfermedad->id_paciente
			);
			$this->cli_consulta_model->saveEnfermedadPaciente($registrarEnfermedad);
			$response['response_msg']='<script>alertify.success("El registro fue guardado adecuadamente");</script>';
		}
		echo json_encode($response);
	}

	public function saveAlergiaPaciente()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$alergia=json_decode($this->input->post("AlergiaPost"));
			$response=array(
				'response_msg'=>''
			);
			$registrarAlergia;
			if ($alergia->alergia_id!='') {
				$registrarAlergia=array(
					'ID_PACIENTE'=>$alergia->id_paciente,
					'ID_ALERGIA'=>$alergia->alergia_id
				);
			} else if($alergia->sustancia_id!='') {
				$registrarAlergia=array(
					'ID_PACIENTE'=>$alergia->id_paciente,
					'ID_OTRA_ALERGIA'=>$alergia->sustancia_id
				);
			} else if($alergia->otra_alergia!=''){
				$registrarAlergia=array(
					'ID_PACIENTE'=>$alergia->id_paciente,
					'OTRA_ALERGIA'=>$alergia->otra_alergia
				);				
			}
			$this->cli_consulta_model->saveAlergiaPaciente($registrarAlergia);
			$response['response_msg']='<script>alertify.success("El registro fue guardado adecuadamente");</script>';
		}
		echo json_encode($response);
	}

	public function saveMenstruacion()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$menstruacion=json_decode($this->input->post("MenstruacionPost"));
			$response=array(
				'response_msg'=>''
			);
			$updateMenstruacion=array(
				'PRIMERA_MENSTRUACION'=>$menstruacion->primera_menstruacion,
				'CARACTERISTICAS'=>$menstruacion->caracteristicas,
				'IVSA'=>$menstruacion->ivsa,
				'MENOPAUSIA'=>$menstruacion->menopausia,
				'OTROS'=>$menstruacion->otros
			);
			$this->cli_consulta_model->updateMenstruacion($menstruacion->id,$updateMenstruacion);
			$response['response_msg']='<script>alertify.success("Los registros fueron guardados adecuadamente");</script>';

		}
		echo json_encode($response);		
	}

	public function saveEmbarazo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$embarazo=json_decode($this->input->post("EmbarazoPost"));
			$response=array(
				'response_msg'=>''
			);
			$updateEmbarazo=array(
				'TOTAL_EMBARAZO'=>$embarazo->total_embarazo,
				'NUMERO_PARTOS'=>$embarazo->no_partos,
				'NUMERO_CESAREAS'=>$embarazo->no_cesareas,
				'NUMERO_ABORTOS'=>$embarazo->no_abortos,
				'NACIDO_VIVOS'=>$embarazo->nacidos_vivos,
				'VIVOS_ACTUALES'=>$embarazo->vivos_actuales,
				'OTROS'=>$embarazo->otros
			);
			$this->cli_consulta_model->updateEmbarazo($embarazo->id,$updateEmbarazo);
			$response['response_msg']='<script>alertify.success("Los registros fueron guardados adecuadamente");</script>';			
		}
		echo json_encode($response);
	}

	public function saveIntereses()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$interes=json_decode($this->input->post("InteresPost"));
			$response=array(
				'response_msg'=>''
			);
			$updateInteres=array(
				'ULTIMA_PAPANICOLAU'=>$interes->ultima_papanicolau,
				'ULTIMA_COLPOSCOPIA'=>$interes->ultima_colposcopia,
				'ULTIMA_MAMOGRAFIA'=>$interes->ultima_mamografia,
				'PAREJAS_SEXUALES'=>$interes->parejas_sexuales,
				'METODOS_ANTICONCEPTIVOS'=>$interes->metodos_anticonceptivos,
				'FLUJOS_VAGINALES'=>$interes->flujos_vaginales,
				'PROCEDIMIENTOS_GINECOLOGICOS'=>$interes->procedimientos_ginecologicos,
				'HABITOS'=>$interes->habitos,
				'OTROS'=>$interes->otros,
				'CIRUJIAS_PREVIAS'=>$interes->cirujias_previas
			);
			$this->cli_consulta_model->updateInteres($interes->id,$updateInteres);
			$response['response_msg']='<script>alertify.success("Los registros fueron guardados adecuadamente");</script>';			

		}
		echo json_encode($response);
		
	}

	public function saveDiagnostico(){
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$consulta=json_decode($this->input->post("ConsultaPost"));
			$response=array(
				'response_msg'=>''
			);
			$updateConsulta;		
			if($consulta->enfermedad==''){
				$updateConsulta=array(
					'SINTOMA'=>$consulta->sintoma,
					'TRATAMIENTO'=>$consulta->tratamiento,
					'DIAGNOSTICO'=>$consulta->diagnostico,
					'OBSERVACION' =>$consulta->observacion,
					'INDICACIONES'=>$consulta->indicaciones,
					'ESTADO'=>'T'
				);
			} else {
			$updateConsulta=array(
				'SINTOMA'=>$consulta->sintoma,
				'TRATAMIENTO'=>$consulta->tratamiento,
				'DIAGNOSTICO'=>$consulta->diagnostico,
				'OBSERVACION' =>$consulta->observacion,
				'INDICACIONES'=>$consulta->indicaciones,				
				'ID_CAT_ENFERMEDAD'=>$consulta->enfermedad,
				'ESTADO'=>'T'
			);				
			}
			$updateCita= array(
				'ESTADO'=>'Atendida'
			);
			$this->cli_consulta_model->updateCita($consulta->id_cita,$updateCita);
			
			$this->cli_consulta_model->updateConsulta($consulta->id,$updateConsulta);
			$response['response_msg']='<script>alertify.success("Los registros fueron guardados adecuadamente");</script>';			
		}
		echo json_encode($response);
	}

	public function saveExploracion()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$exploracion=json_decode($this->input->post("ExploracionPost"));
			$response=array(
				'response_msg'=>''
			);
			$updateExploracion=array(
				'PESO'=>$exploracion->peso,
				'TALLA'=>$exploracion->talla,
				'CADERA'=>$exploracion->cadera,
				'CINTURA'=>$exploracion->cintura,
				'FRECUENCIA_CARDIACA'=>$exploracion->frecuencia_cardiaca,
				'FRECUENCIA_RESPIRATORIA'=>$exploracion->frecuencia_respiratoria,
				'PRESION_ARTERIAL'=>$exploracion->presion_arterial,
				'GLUCOSA_LABORATORIO'=>$exploracion->glucosa_laboratorio,
				'GLUCOSA_CAPILAR'=>$exploracion->glucosa_capilar,
				'IMC'=>$exploracion->imc,
				'FECHA_EXPLORACION'=>date('Y-m-d H:i:s'),
				'COLESTEROL'=>$exploracion->colesterol, 
				'TRIGLICERIDOS'=>$exploracion->trigliceridos,
				'HEMOGLOBINA'=>$exploracion->hemoglobina,
				'CABEZA'=>$exploracion->cabeza,
				'CUELLO'=>$exploracion->cuello
			);
			$this->cli_consulta_model->updateExploracion($exploracion->id,$updateExploracion);
			$response['response_msg']='<script>alertify.success("Los registros fueron guardados adecuadamente");</script>';			
		}
		echo json_encode($response);
		
	}

	public function saveReceta()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$receta=json_decode($this->input->post("RecetaPost"));
			$response= array(
				'response_msg'=>'',
				'receta_id'
			);
			$producto=$this->cli_receta_model->listarProductoId($receta->id_medicamento);
			$inventario=$this->cli_receta_model->listarInventario($producto);
			$historico=$this->cli_receta_model->listarHistorico($producto,date('d-m-Y'));
			if (empty($inventario) && empty($historico)) {

				$response['response_msg']='<script>alertify.error("Debe existir un inventario del medicamento");</script>';						
			} else if($inventario[0]->CANTIDAD<$receta->cantidad){
				$response['response_msg']='<script>alertify.error("No hay suficientes existencias de este medicamento");</script>';				
			} else {
				$registrarReceta=array(
					'ID_CONSULTA'=>$receta->id_consulta,
					'FECHA_RECETA'=>date('Y-m-d H:i:s'),
					'DOSIS'=>$receta->dosis,
					'INDICACIONES'=>$receta->indicaciones,
					'ID_MEDICAMENTO'=>$receta->id_medicamento,
					'CANTIDAD'=>$receta->cantidad
				);
				$response['receta_id']=$this->cli_consulta_model->saveReceta($registrarReceta);
				$response['response_msg']='<script>alertify.success("Los registros fueron guardados adecuadamente");</script>';			

			}
		}
		echo json_encode($response);	
	}

	public function deleteReceta()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$receta=json_decode($this->input->post("RecetaPost"));

			$response =array(
				'response_msg'=>''
			);
			$this->cli_consulta_model->deleteReceta($receta->id);
			$response['response_msg']='<script>alertify.success("Registro eliminado adecuadamente");</script>';						
		}
		echo json_encode($response);
	}

	public function updateEstudio()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$estudio=json_decode($this->input->post("EstudioPost"));
			$response=array(
				'response_msg'=>''
			);
			$updateEstudio=array(
				'ESTADO'=>'LEIDO',
				'LECTURA'=>$estudio->respuesta
			);
			$this->cli_consulta_model->updateEstudio($estudio->id,$updateEstudio);
			$response['response_msg']='<script>alertify.success("Registro actualizado correctamente adecuadamente");</script>';
		}
		echo json_encode($response);
	}

	public function addEstudio()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$estudio=json_decode($this->input->post("EstudioPost"));
			$response=array(
				'response_msg'=>'',
				'detalle_id'=>''
			);
			$registrarEstudio=array(
				'ESTADO'=>'SOLICITADO',
				'ID_CITA'=>$estudio->cita,
				'ID_CAT_ESTUDIO_CLINICO'=>$estudio->id
			);
			$response['detalle_id']=$this->cli_consulta_model->saveEstudio($registrarEstudio);
			$response['response_msg']='<script>alertify.success("Registro actualizado correctamente adecuadamente");</script>';
		}
		echo json_encode($response);		
	}

}