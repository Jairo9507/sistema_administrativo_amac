<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class medico extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cli_medico_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Medicos","clinica/medico");			
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Medicos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['medicos']=$this->cli_medico_model->listarMedicos();

            $this->template->admin_render("admin/cli_medicos/index",$this->data);			
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nuevo Medico');
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['empleados']=$this->cli_medico_model->listarEmpleados();
			$this->data['especialidades']=$this->cli_medico_model->listarEspecialidades();
			$this->load->view("admin/cli_medicos/modal_medicos",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Medicos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->breadcrumbs->unshift(2,"Editar Medicos","clinica/medico/editar");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            $id=base64_decode($id);
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['medico']=$this->cli_medico_model->listarMedicoId($id);
			$this->data['empleados']=$this->cli_medico_model->listarEmpleados();
			$this->data['especialidades']=$this->cli_medico_model->listarEspecialidades();

            $this->template->admin_render("admin/cli_medicos/medico_editar",$this->data);			
		}		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$medico=json_decode($this->input->post("MedicoPost"));
			$id=base64_decode($medico->id);
			$response= array(
				'response_msg'=>''
			);
			$deleteMedico=array(
				'ESTADO'=>0
			);
			$this->cli_medico_model->deleteMedico($id,$deleteMedico);
			$response['response_msg']="<script>recargar();</script>";
		}
		echo json_encode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$medico=json_decode($this->input->post("MedicoPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			$existe=$this->cli_medico_model->listarMedicoExiste($medico->id_empleado);
			if ($medico->id_empleado=='' OR $medico->id_empleado=='0') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre del medico es obligatorio</div>";
			} else if($medico->id_especialidad=='' OR $medico->id_especialidad=='0') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La especialidad del medico es obligatorio</div>";

			} else if($medico->no_jvpm==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El Numero de la junta de vigilancia de la profesion medica(JVPM) es obligatorio</div>";

			} else if(!empty($existe)){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Este medico ya fue registrado en el sistema</div>";
			} else {
				if ($medico->id=='') {
					$registrarMedico= array(
						'ID_EMPLEADO'=>$medico->id_empleado,
						'ID_CAT_ESPECIALIDAD'=>$medico->id_especialidad,
						'NO_JVPM'=>$medico->no_jvpm,
						'VER_AGENDA'=>1
					);
					$this->cli_medico_model->saveMedico($registrarMedico);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$updateMedico=array(
						'ID_EMPLEADO'=>$medico->id_empleado,
						'ID_CAT_ESPECIALIDAD'=>$medico->id_especialidad,
						'NO_JVPM'=>$medico->no_jvpm,
						'VER_AGENDA'=>1
					);
					$this->cli_medico_model->updateMedico($medico->id,$updateMedico);
					$response['response_msg']="<script>recargar();</script>";
				}
				
			}
			
		}
		echo json_encode($response);
	}

}