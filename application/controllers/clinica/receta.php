<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class receta extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cli_consulta_model");
		$this->load->model("usuario_model");
		$this->load->model("cli_medico_model");
		$this->load->model("cli_receta_model");
		$this->breadcrumbs->unshift(1,"Recetas","clinica/receta");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Recetas');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
            if (empty($medico)) {
            	$medico=1;
            }

            $this->data['consultas']=$this->cli_receta_model->listarConsultas();
            $this->data['brigadas']=$this->cli_receta_model->listarBrigadas();
            $this->template->admin_render("admin/cli_receta/index",$this->data);
		}
		
	}

	public function salida($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Receta');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Salida receta","clinica/receta/salida");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           $id=base64_decode($id);
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
            if (empty($medico)) {
            	$medico=1;
            }
            $this->data['consulta']=$this->cli_receta_model->listarConsultaId($id);
            $this->data['recetas']=$this->cli_receta_model->listarRecetaConsulta($id);

            $this->template->admin_render("admin/cli_receta/salida",$this->data);			
		}
		
	}

	public function brigada()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Brigada');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Receta para brigadas","clinica/receta/");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
            if (empty($medico)) {
            	$medico=1;
            }			
            $this->data['productos']=$this->cli_receta_model->listarMedicamentos();
            $this->template->admin_render("admin/cli_receta/brigada",$this->data);            
		}
		
	}	

	public function editarBrigada($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Brigada');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Editar receta","clinica/receta/editarBrigada");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
            if (empty($medico)) {
            	$medico=1;
            }			
            $id=base64_decode($id);
            $this->data['productos']=$this->cli_receta_model->listarMedicamentos();
            $this->data['brigada']=$this->cli_receta_model->listarBrigadaId($id);
            $this->data['recetas']=$this->cli_receta_model->listarRecetasBrigada($id);
            $this->template->admin_render("admin/cli_receta/brigada_editar",$this->data);            
		}

	}

	public function eliminarBrigada()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$brigada=json_decode($this->input->post("BrigadaPost"));
			$id=base64_decode($brigada->id);
			$response=array(
				'response_msg'=>''
			);
			$deleteBrigada=array(
				'ESTADO'=>0
			);
			$this->cli_receta_model->deleteBrigada($id,$deleteBrigada);
			$response['response_msg']='<script>recargar();</script>';
		}
		echo json_encode($response);
	}
	public function vistaPDF($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Receta Documento");
			$this->data['pagetitle']=$this->page_title->show();

			$this->data['Idreceta']=base64_decode($id);
			$this->load->view('admin/cli_receta/receta_pdf',$this->data); 			
		}
		
	}	

	public function recetaPDF($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			if($id==null){
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "receta_".date('Y-m-d');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
	           exit;
		        }else {

	            $recetas=$this->cli_receta_model->listarRecetaPDF($id);
	            $tipoConsulta= $this->cli_receta_model->countConsulta($recetas[0]->ID_PACIENTE);
	            if (empty($recetas)) {
					$html="Ha ocurrido un error al realizar la operación";
					$nombre_archivo = "receta_".date('Y-m-d');
			        $this->load->library('pdf');
		            $this->pdf->loadHtml($html);
		            $this->pdf->setPaper('A4');
		            $this->pdf->set_option('defaultMediaType', 'all');
		            $this->pdf->set_option('isFontSubsettingEnabled', true);            
		            $this->pdf->render();

		            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));

		        } else {
		            $examenes=$this->cli_receta_model->listarExamenesConsulta($recetas[0]->ID_CITA);
		            $html='
		            <!DOCTYPE html>
		            <html>
		            <head>
		            <title>Receta M&eacute;dica</title>
		            </head>
		            <body id="muestra">
		            <style>

		            @page{
		            margin:1.2cm;
		            }

		            div, table{  
		            font-family: Arial;
		            }

		            #firma{
		            	padding-top:20px;
			        }


		            #logo_clinica{
		            width:100%;
		            display: inline-block;
		            float:left;

		            }

		            #page_break{
					  page-break-after: avoid!important;	            	
		            }
		            #logo_sistema{
		                width:20%;
		                display: inline-block;
		                float:right;
		            }

		            #encabezado
		            {
		                width:100%;
		                display:inline-block;
		            }


		            table{
		                clear:both;
		                text-align: left;
		                border-collapse:collapse; 
		                table-layout:fixed;
		                width: 100%;

		            }

		            table td {
		                word-wrap:break-word;
		                padding:2px 0px 3px 0px !important;	                         
		            }

		            table tr {
						border-top: 1px solid #B8B7B7;
						border-bottom: 1px solid #B8B7B7;		            	
		            }

					#firma{
					width: 90%;
					position:absolute;
					text-align: center;
					bottom:3%;
					}	            

		            </style>

		            ';
		            (int)$tipoConsulta[0]->contarCitas>1?$tipoConsulta="Consulta Subsecuente":$tipoConsulta="Primera Vez";

		               $html.='<div style="">
		                <div id="logo_clinica" > 
							<img src="img/MEMBRETE1-1.jpg" style="width:750px; height:70px;">
		                </div>

		                <div id="encabezado">
			              	<table style="border-spacing: 0px; width:100%; table-layout:fixed;">
			                	<thead>
			                		<tr>
			                			<th style="width:10%;"></th>
			                			<th style="width:80%;"></th>
			                			<th style="width:10%;"></th>
			                		</tr>
			                	</thead>
			                	<tbody>
			                		<tr>
			                			<td colspan="3" style="width:100% ;font-size:13px; text-align:center" ><b>RECETA MEDICA</b></td>
			                		</tr>
			                		<tr style="margin:0px">
			                			<td style="width:10% ;font-size:11px;" ><b>FECHA</b> '.date('d/m/Y',strtotime($recetas[0]->FECHA_CONSULTA)).'</td>
										<td style=" text-align:left width:80%; font-size:13px;" ><h3>CLINICA MUNICIPAL DE ANTIGUO CUSCATLAN</h3></td>
			                			
			                			<td style="width:100%;font-size:11px; "><b>TIPO DE CONSULTA</b>: &nbsp;&nbsp;&nbsp;'.$recetas[0]->ESPECIALIDAD.'</td>
			                		</tr>
			                		<tr style="margin:0px">
			                			<td style="width:40%;font-size:11px; "><b>PACIENTE</b>:</td>
				                		<td colspan="2" style="width:60%; font-size:11px;">'.$recetas[0]->CODIGO_EXPEDIENTE.'&nbsp;&nbsp;'.strtoupper($recetas[0]->nombre_paciente).'</td>
			                		</tr>


			                	</tbody>
			                </table>	              
		                </div>

						<table style="width:100%; table-layout:fixed; height:5%; padding-bottom:20px;">
							<thead>
								<tr>
									<!--<th style="width:20%; text-align:left;font-size:11px;">CODIGO</th>-->
									<th style="width:80%; text-align:center;font-size:11px;font-size:11px;">DESCRIPCION</th>
									<th style="width:10%; text-align:center;font-size:11px;">CANTIDAD</th>
								</tr>
							</thead>
							<tbody>
								
																				
						';
						$contar=1;
						foreach ($recetas as $r) {
							$html.="<tr>
							<!--<td>".$r->COD_PRODUCTO."</td>-->
							<td style='text-align:left;font-size:14px;'>".$contar.". ".$r->NOMBRE_PRODUCTO." ".$r->FORMA_FARMACEUTICA." ".$r->PRESENTACION." ".$r->CONCENTRACION." ".strtoupper($r->INDICACIONES)." ".strtoupper($r->MENSAJE)."</td>
							<td style='text-align:center;font-size:14px;'>".$r->CANTIDAD."</td>
							</tr>";
							$contar++;
						}

						//$contare=1;
						/*if ($examenes) {
							foreach ($examenes as $e) {
								$html.='<tr>
									<td style="text-align:left;font-size:11px;">'.$contare.'. '.$e->DESCRIPCION.'</td>
								</tr>';
								$contare++;
							}
						} else {

						}	*/				

							$html.='
							<tr>
								<td colspan="2" style="text-align:left;font-size:14px;">'.strtoupper($recetas[0]->OBSERVACIONES).'</td>
							</tr>
							</tbody>
						</table>';


						/*$html.="
						</tbody>
						</table>";*/
						$contare=1;
						/*$html.='
						<table style="style="width:100%; table-layout:fixed; height:5%; ">
							<thead>
								<tr>
									<th>Examenes</th>
								</tr>

							</thead>
							<tbody>'; 
							if ($examenes) {
								foreach ($examenes as $e) {
									$html.='<tr>
										<td style="text-align:left;font-size:11px;">'.$contare.'. '.$e->DESCRIPCION.'</td>
									</tr>';
									$contare++;
								}
							} else {

							}

							$html.='</tbody>
						</table>';*/
						$html.='
		                <table style="style="width:100%; table-layout:fixed;" >
		                	<thead>
		                		<tr>
		                			<th style="width:75%;"></th>
		                			<th style="width:15%;"></th>
		                		</tr>

		                	</thead>
		                	<tbody>
		                		<tr>
		                		<td style="font-size:13px;">DR.'.$recetas[0]->nombre_medico.'</td>
		                		<td style="width:100%;font-size:13px;">
					               _______________________________________
					                <br><span>Firma y Sello</span>
					            </td>
		                		</tr>
		                	</tbody>
		                </table></div>
						';

		               $html.='<div style="">
		                <div id="logo_clinica" > 
							<img src="img/MEMBRETE1-1.jpg" style="width:750px; height:70px;">
		                </div>

		                <div id="encabezado">
			              	<table style="border-spacing: 0px; width:100%; table-layout:fixed;">
			                	<thead>
			                		<tr>
			                			<th style="width:10%;"></th>
			                			<th style="width:80%;"></th>
			                			<th style="width:10%;"></th>
			                		</tr>
			                	</thead>
			                	<tbody>
			                		<tr>
			                			<td colspan="3" style="width:100% ;font-size:13px; text-align:center" ><b>RECETA MEDICA</b></td>
			                		</tr>			                	
			                		<tr style="margin:0px">
			                			<td style="width:10% ;font-size:11px;" ><b>FECHA</b> '.date('d/m/Y',strtotime($recetas[0]->FECHA_CONSULTA)).'</td>
										<td style=" text-align:left width:80%; font-size:13px;" ><h3>CLINICA MUNICIPAL DE ANTIGUO CUSCATLAN</h3></td>
			                			
			                			<td style="width:100%;font-size:11px; "><b>TIPO DE CONSULTA</b>: &nbsp;&nbsp;&nbsp;'.$recetas[0]->ESPECIALIDAD.'</td>
			                		</tr>
			                		<tr style="margin:0px">
			                			<td style="width:40%;font-size:11px; "><b>PACIENTE</b>:</td>
				                		<td colspan="2" style="width:60%; font-size:11px;">'.$recetas[0]->CODIGO_EXPEDIENTE.'&nbsp;&nbsp;'.strtoupper($recetas[0]->nombre_paciente).'</td>
			                		</tr>


			                	</tbody>
			                </table>	              
		                </div>

						<table style="width:100%; table-layout:fixed; height:5%; padding-bottom:20px;">
							<thead>
								<tr>
									<!--<th style="width:20%; text-align:left;font-size:11px;">CODIGO</th>-->
									<th style="width:80%; text-align:center;font-size:11px;font-size:11px;">DESCRIPCION</th>
									<th style="width:10%; text-align:center;font-size:11px;">CANTIDAD</th>
								</tr>
							</thead>
							<tbody>
								
																				
						';
						$contar=1;
						foreach ($recetas as $r) {
							$html.="<tr>
							<!--<td>".$r->COD_PRODUCTO."</td>-->
							<td style='text-align:left;font-size:14px;'>".$contar.". ".$r->NOMBRE_PRODUCTO." ".$r->FORMA_FARMACEUTICA." ".$r->PRESENTACION." ".$r->CONCENTRACION." ".strtoupper($r->INDICACIONES)." ".strtoupper($r->MENSAJE)."</td>
							<td style='text-align:center;font-size:14px;'>".$r->CANTIDAD."</td>
							</tr>";
							$contar++;
						}

						//$contare=1;
						/*if ($examenes) {
							foreach ($examenes as $e) {
								$html.='<tr>
									<td style="text-align:left;font-size:11px;">'.$contare.'. '.$e->DESCRIPCION.'</td>
								</tr>';
								$contare++;
							}
						} else {

						}	*/				

							$html.='
							<tr>
								<td colspan="2" style="text-align:left;font-size:14px;">'.strtoupper($recetas[0]->OBSERVACIONES).'</td>
							</tr>
							</tbody>
						</table>';


						/*$html.="
						</tbody>
						</table>";*/
						$contare=1;
						/*$html.='
						<table style="style="width:100%; table-layout:fixed; height:5%; ">
							<thead>
								<tr>
									<th>Examenes</th>
								</tr>

							</thead>
							<tbody>'; 
							if ($examenes) {
								foreach ($examenes as $e) {
									$html.='<tr>
										<td style="text-align:left;font-size:11px;">'.$contare.'. '.$e->DESCRIPCION.'</td>
									</tr>';
									$contare++;
								}
							} else {

							}

							$html.='</tbody>
						</table>';*/
						$html.='
		                <table style="style="width:100%; table-layout:fixed;" >
		                	<thead>
		                		<tr>
		                			<th style="width:75%;"></th>
		                			<th style="width:15%;"></th>
		                		</tr>

		                	</thead>
		                	<tbody>
		                		<tr>
		                		<td style="font-size:13px;">DR.'.$recetas[0]->nombre_medico.'</td>
		                		<td style="width:100%;font-size:13px;">
					               _______________________________________
					                <br><span>Firma y Sello</span>
					            </td>
		                		</tr>
		                	</tbody>
		                </table></div>
						';		                //var_dump($html);exit;				
		            $this->load->library('pdf');
		            $nombre_archivo = "RECETA".$recetas[0]->nombre_paciente." ".$recetas[0]->FECHA_CONSULTA."";
					$paper_size = array(0,0,105,816);
		            $this->pdf->loadHtml($html);
		            $this->pdf->setPaper('letter','portrait');
		            $this->pdf->set_option('defaultMediaType', 'all');
		            $this->pdf->set_option('isFontSubsettingEnabled', true);            
		            $this->pdf->render();

		            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
		        

		        }
	        }
		}
		
	}
	public function cancelarReceta()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$consulta=json_decode($this->input->post("ConsultaPost"));
			$id=base64_decode($consulta->id);
			$response=array(
				'response_msg'=>''
			);
			$updateConsulta=array(
				'ESTADO'=>'C'
			);
			$this->cli_receta_model->updateConsulta($id,$updateConsulta);
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro actualizado adecuadamente</div>";
		}
		echo json_encode($response);
	}

	public function actualizarTabla()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$datos=json_decode($this->input->post("FechaPost"));

			$medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
            if (empty($medico)) {
            	$medico=1;
            }
            $date = new \DateTime($datos->fecha);
			$consultas=$this->cli_receta_model->listarCitasFecha($medico,date('Y-m-d',strtotime($date->format('d/m/Y'))));
			$json=json_encode($consultas);

		}
		echo $json;
	}

	public function registrarMovimiento()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$receta=json_decode($this->input->post("RecetaPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>'',
				'validar'=>true,
				'receta_id'=>''
			);
			$inventario=$this->cli_receta_model->listarInventario($receta->id_producto);
			$historico=$this->cli_receta_model->listarHistorico($receta->id_producto,date('Y-m-d'));
			if (empty($inventario)) {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe existir un inventario</div>";		
			}else if($receta->cantidad>$inventario[0]->CANTIDAD){
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La salida de medicamento no puede ser mayor a las existencias</div>";
					$response['validar']=false;
			} else if ($receta->id_receta!='') {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());				
						if($historico[0]->CANTIDAD-$receta->cantidad==0)
						{
							$updateHistorico= array(
								'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD-$receta->cantidad,
								'ESTADO'=>0
							);
							$this->requisicion_model->updateHistorico($historico[0]->ID_PRODUCTO,$updateHistorico);
						}else if($historico[0]->CANTIDAD-$receta->cantidad<0){
								/*if (empty($historico)) {
									break;
								}*/
								$restante=$historico[0]->CANTIDAD-$receta->cantidad;
								while(!empty($historico) or $restante!=0){
									if ($restante<0) {
									$updateHistorico= array(
										'CANTIDAD_INVENTARIO'=>0,
										'ESTADO'=>0
									);
										$registrarMovimiento=array(
											'FECHA'=>date('Y-m-d H:i:s'),
											'ID_PRODUCTO'=>$receta->id_producto,
											'CANTIDAD'=>$historico[0]->CANTIDAD_INVENTARIO,
											'ID_TIPO_MOVIMIENTO'=>2,
											'USUARIO_CREACION'=>$usuario,
											'FECHA_CREACION'=>date('Y-m-d H:i:s'),
											'COSTO_UNITARIO'=>$costo,
											'ID_RECETA'=>$receta->id_receta,
											'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
										);
											
										$this->cli_receta_model->saveMovimiento($registrarMovimiento);										
									$restante=$receta->cantidad-$historico[0]->CANTIDAD;
									
									$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);	
									} else {			
										if ($restante<=$historico[0]->CANTIDAD && $restante!=0) {
											$updateHistorico= array(
												'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD-$restante
											);
											$registrarMovimiento=array(
												'FECHA'=>date('Y-m-d H:i:s'),
												'ID_PRODUCTO'=>$receta->id_producto,
												'CANTIDAD'=>$restante,
												'ID_TIPO_MOVIMIENTO'=>2,
												'USUARIO_CREACION'=>$usuario,
												'FECHA_CREACION'=>date('Y-m-d H:i:s'),
												'COSTO_UNITARIO'=>$costo,
												'ID_RECETA'=>$receta->id_receta,
												'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
											);
											
											$this->cli_receta_model->saveMovimiento($registrarMovimiento);									
										$restante=$restante-$restante;
										$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);											
										} else {
											$updateHistorico= array(
												'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD-$restante							
											);
											$restante=$restante-$restante;
											
											$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
														
										}									
									}

									if($restante!=0){
										$historico=$this->cli_receta_model->listarHistorico($receta->id_producto,date('Y-m-d'));
									} else {
										$historico=NULL;
									}				
									empty($historico);

								}									

							} else {
									$updateHistorico= array(
										'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$receta->cantidad,
									);
									$registrarMovimiento=array(
										'FECHA'=>date('Y-m-d H:i:s'),
										'ID_PRODUCTO'=>$receta->id_producto,
										'CANTIDAD'=>$receta->cantidad,
										'COSTO_UNITARIO'=>$historico[0]->PRECIO,
										'ID_TIPO_MOVIMIENTO'=>2,
										'USUARIO_CREACION'=>$usuario,
										'FECHA_CREACION'=>date('Y-m-d H:i:s'),
										'ID_RECETA'=>$receta->id_receta,
										'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
									);

									$this->cli_receta_model->saveMovimiento($registrarMovimiento);									
									$restante=$receta->cantidad-$historico[0]->CANTIDAD;
									
									$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);								
							}						
						if ($inventario[0]->CANTIDAD-$receta->cantidad==0) {

							$updateInventario=array(
								'USUARIO_MODIFICACION'=>$usuario,
								'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
								'VIGENCIA'=>0,
								'FECHA_FIN_VIGENCIA'=>date('Y-m-d H:i:s')
							);
							$this->cli_receta_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);
						}
							$updateInventario=array(
								'USUARIO_MODIFICACION'=>$usuario,
								'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
								'CANTIDAD'=>(int)$inventario[0]->CANTIDAD-(int)$receta->cantidad,
							);
							 $this->cli_receta_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);
						$costo=$this->cli_receta_model->ultimoCosto($receta->id_producto);
						
						$response['response_msg']="<script>alertify.success('Registro guardado Exitosamente');</script>";

				
			} else if($receta->id_brigada!='' && $receta->id_receta!=''){
				$updateReceta=array(
						'CANTIDAD'=>$receta->cantidad
					);
					$registro=$this->cli_receta_model->listarRecetaId($receta->id_receta);
					$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
					//var_dump($registro);var_dump($receta);exit;
						if($historico[0]->CANTIDAD-$receta->cantidad==0)
						{
							$updateHistorico= array(
								'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD-$receta->cantidad,
								'ESTADO'=>0
							);
							$this->requisicion_model->updateHistorico($historico[0]->ID_PRODUCTO,$updateHistorico);
						} else if($historico[0]->CANTIDAD-$receta->cantidad<0){
								$restante=$historico[0]->CANTIDAD-$receta->cantidad;
								while(!empty($historico) or $restante!=0){
									if ($restante<0) {
									$updateHistorico= array(
										'CANTIDAD_INVENTARIO'=>0,
										'ESTADO'=>0
									);
										$registrarMovimiento=array(
											'FECHA'=>date('Y-m-d H:i:s'),
											'ID_PRODUCTO'=>$receta->id_producto,
											'CANTIDAD'=>$historico[0]->CANTIDAD_INVENTARIO,
											'ID_TIPO_MOVIMIENTO'=>2,
											'USUARIO_CREACION'=>$usuario,
											'FECHA_CREACION'=>date('Y-m-d H:i:s'),
											'COSTO_UNITARIO'=>$costo,
											'ID_RECETA'=>$receta->id_receta,
											'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
										);
											
										$this->cli_receta_model->saveMovimiento($registrarMovimiento);										
									$restante=$receta->cantidad-$historico[0]->CANTIDAD_INVENTARIO;
									
									$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);	
									} else {			
										if ($restante<=$historico[0]->CANTIDAD_INVENTARIO && $restante!=0) {
											$updateHistorico= array(
												'CANTIDAD'=>$historico[0]->CANTIDAD_INVENTARIO-$restante
											);
											$registrarMovimiento=array(
												'FECHA'=>date('Y-m-d H:i:s'),
												'ID_PRODUCTO'=>$receta->id_producto,
												'CANTIDAD'=>$restante,
												'ID_TIPO_MOVIMIENTO'=>2,
												'USUARIO_CREACION'=>$usuario,
												'FECHA_CREACION'=>date('Y-m-d H:i:s'),
												'COSTO_UNITARIO'=>$costo,
												'ID_RECETA'=>$receta->id_receta,
												'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
											);
											
											$this->cli_receta_model->saveMovimiento($registrarMovimiento);									
										$restante=$restante-$restante;
										$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);											
										} else {
											$updateHistorico= array(
												'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$restante							
											);
											$restante=$restante-$restante;
											
											$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
														
										}									
									}

									if($restante!=0){
										$historico=$this->cli_receta_model->listarHistorico($receta->id_producto);
									} else {
										$historico=NULL;
									}				
									empty($historico);

								}	
					if ($inventario[0]->CANTIDAD-$receta->cantidad==0) {

								$updateInventario=array(
									'USUARIO_MODIFICACION'=>$usuario,
									'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
									'VIGENCIA'=>0,
									'FECHA_FIN_VIGENCIA'=>date('Y-m-d H:i:s')
								);
								$this->cli_receta_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);
					}
					if ((int)$registro[0]->CANTIDAD-(int)$receta->cantidad<0) {
						$updateHistorico= array(
							'CANTIDAD'=>$historico[0]->CANTIDAD-((int)$receta->cantidad-(int)$registro[0]->CANTIDAD)
						);
						$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
						$updateInventario= array(
									'CANTIDAD'=>(int)$inventario[0]->CANTIDAD-((int)$receta->cantidad-(int)$registro[0]->CANTIDAD)
								);
								$this->cli_receta_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);	
					} else {					
						$updateHistorico= array(
							'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO+((int)$registro[0]->CANTIDAD-(int)$receta->cantidad)
						);
						$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);								
								$updateInventario= array(
									'CANTIDAD'=>(int)$inventario[0]->CANTIDAD+((int)$registro[0]->CANTIDAD-(int)$receta->cantidad)
								);
								$this->cli_receta_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);
					}
						$updateReceta= array(
							'CANTIDAD'=>$receta->cantidad
						);
						$this->cli_receta_model->updateReceta($receta->id_receta,$updateReceta);
						$response['response_msg']="<script>alertify.success('Registro guardado Exitosamente');</script>";								
						}else{
							$updateHistorico= array(
								'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$receta->cantidad,
							);

							$registrarMovimiento=array(
								'FECHA'=>date('Y-m-d H:i:s'),
								'ID_PRODUCTO'=>$receta->id_producto,
								'CANTIDAD'=>$receta->cantidad,
								'COSTO_UNITARIO'=>$historico[0]->PRECIO,
								'ID_TIPO_MOVIMIENTO'=>2,
								'USUARIO_CREACION'=>$usuario,
								'FECHA_CREACION'=>date('Y-m-d H:i:s'),
								'ID_REQUISICION'=>$receta->id_requisicion,
								'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
							);

							$this->cli_receta_model->saveMovimiento($registrarMovimiento);									
							$restante=$receta->cantidad-$historico[0]->CANTIDAD_INVENTARIO;
									
							$this->cli_receta_model_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);	
						}				
			} else  {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>";
			}
		}
		echo json_encode($response);
	}

	public function registrarBrigada()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$brigada=json_decode($this->input->post("BrigadaPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>'',
				'validar'=>true
			);
			if ($brigada->fecha=='') {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se puede ingresare la brigada sin la fecha</div>";
				$response['validar']=false;
			} else if($brigada->comunidad=='') {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La en que se dio la atenci&oacute;n es obligatorio</div>";
				$response['validar']=false;				
			} else {
				if($brigada->id==''){
						$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
						$registrarBrigada=array(
							'FECHA_BRIGADA'=>$brigada->fecha,
							'COMUNIDAD'=>$brigada->comunidad,
							'USUARIO_CREACION'=>$usuario,
							'FECHA_CREACION'=>date('Y-m-d H:i:s')
						);
					$id=$this->cli_receta_model->saveBrigada($registrarBrigada);
					$response['campo']=$id;
				} else {
						$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
						$updateBrigada=array(
						'FECHA_BRIGADA'=>$brigada->fecha,
						'COMUNIDAD'=>$brigada->comunidad,
						'USUARIO_MODIFICACION'=>$usuario,
						'FECHA_MODIFICACION'=>date('Y-m-d H:i:s')
					);
					$this->cli_receta_model->updateBrigada($brigada->id,$updateBrigada);
				}
			}
		}
			echo json_encode($response);
		
	}

	public function registrarMovimientoBrigada(){
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
				$receta=json_decode($this->input->post("RecetaPost"));

				$response=array(
					'response_msg'=>'',
					'campo'=>'',
					'validar'=>true,
					'receta_id'=>''
				);
				$producto=$this->cli_receta_model->listarProductoId($receta->id_medicamento);
				$inventario=$this->cli_receta_model->listarInventario($producto);
				$historico=$this->cli_receta_model->listarHistorico($producto,$receta->fecha);		
				if (empty($inventario)) {
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe existir un inventario</div>";
					$response['validar']=false;		
				}else if($receta->cantidad>$inventario[0]->CANTIDAD){
						$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La salida de medicamento no puede ser mayor a las existencias</div>";
						$response['validar']=false;
				} else if($receta->id_receta=='' && $receta->id_brigada!=''){
					$registrarReceta=array(
						'ID_BRIGADA'=>$receta->id_brigada,
						'ID_MEDICAMENTO'=>$receta->id_medicamento,
						'FECHA_RECETA'=>date('Y-m-d H:i:s'),
						'CANTIDAD'=>$receta->cantidad
					);
					$id=$this->cli_receta_model->saveReceta($registrarReceta);
					$response['receta_id']=$id;				
					$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
						if($historico[0]->CANTIDAD_INVENTARIO-$receta->cantidad==0)
						{
							$updateHistorico= array(
								'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD-$receta->cantidad,
								'ESTADO'=>0
							);
							$this->requisicion_model->updateHistorico($historico[0]->ID_PRODUCTO,$updateHistorico);
						} else if($historico[0]->CANTIDAD_INVENTARIO-$receta->cantidad<0){
								$restante=$historico[0]->CANTIDAD_INVENTARIO-$receta->cantidad;

								while(!empty($historico) or $restante!=0){
									if ($restante<0) {
									$updateHistorico= array(
										'CANTIDAD_INVENTARIO'=>0,
										'ESTADO'=>0
									);
										$registrarMovimiento=array(
											'FECHA'=>date('Y-m-d H:i:s'),
											'ID_PRODUCTO'=>$receta->id_producto,
											'CANTIDAD'=>$historico[0]->CANTIDAD_INVENTARIO,
											'ID_TIPO_MOVIMIENTO'=>2,
											'USUARIO_CREACION'=>$usuario,
											'FECHA_CREACION'=>date('Y-m-d H:i:s'),
											'COSTO_UNITARIO'=>$costo,
											'ID_BRIGADA'=>$receta->id_brigada,
											'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
										);
											
										$this->cli_receta_model->saveMovimiento($registrarMovimiento);										
									$restante=$receta->cantidad-$historico[0]->CANTIDAD_INVENTARIO;
									
									$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);	
									} else {			
										if ($restante<=$historico[0]->CANTIDAD_INVENTARIO && $restante!=0) {
											$updateHistorico= array(
												'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$restante
											);
											$registrarMovimiento=array(
												'FECHA'=>date('Y-m-d H:i:s'),
												'ID_PRODUCTO'=>$receta->id_producto,
												'CANTIDAD'=>$restante,
												'ID_TIPO_MOVIMIENTO'=>2,
												'USUARIO_CREACION'=>$usuario,
												'FECHA_CREACION'=>date('Y-m-d H:i:s'),
												'COSTO_UNITARIO'=>$costo,
												'ID_BRIGADA'=>$receta->id_brigada,
												'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
											);
											
											$this->cli_receta_model->saveMovimiento($registrarMovimiento);									
										$restante=$restante-$restante;
										$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);											
										} else {
											$updateHistorico= array(
												'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD-$restante							
											);
											$restante=$restante-$restante;
											
											$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
														
										}									
									}

									if($restante!=0){
										$historico=$this->cli_receta_model->listarHistorico($receta->id_producto,date('Y-m-d H:i:s'));
									} else {
										$historico=NULL;
									}				
									empty($historico);

								}
							if ($inventario[0]->CANTIDAD-$receta->cantidad==0) {

								$updateInventario=array(
									'USUARIO_MODIFICACION'=>$usuario,
									'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
									'VIGENCIA'=>0,
									'FECHA_FIN_VIGENCIA'=>date('Y-m-d H:i:s')
								);
								$this->cli_receta_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);
						$response['response_msg']="<script>alertify.success('Registro guardado Exitosamente');</script>";								
							}								
						} else {
							$updateHistorico= array(
								'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$receta->cantidad,
							);
							
							$producto=$this->cli_receta_model->listarProductoId($receta->id_medicamento,date('Y-m-d H:i:s'));							
							$registrarMovimiento=array(
								'FECHA'=>date('Y-m-d H:i:s'),
								'ID_PRODUCTO'=>$producto,
								'CANTIDAD'=>$receta->cantidad,
								'COSTO_UNITARIO'=>$historico[0]->PRECIO,
								'ID_TIPO_MOVIMIENTO'=>2,
								'USUARIO_CREACION'=>$usuario,
								'FECHA_CREACION'=>date('Y-m-d H:i:s'),
								'ID_BRIGADA'=>$receta->id_brigada,
								'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
							);
							$this->cli_receta_model->saveMovimiento($registrarMovimiento);								
									
							$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);						
							$response['response_msg']="<script>alertify.success('Registro guardado Exitosamente');</script>";						
						}											
				}			
		}
		echo json_encode($response);
	}
	public function deletereceta()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$receta=json_decode($this->input->post("recetaPost"));
			$response=array(
				'response_msg'=>''				
			);
			$producto=$this->cli_receta_model->listarProductoId($receta->id_medicamento);
			$inventario=$this->cli_receta_model->listarInventario($producto);
			$historico=$this->cli_receta_model->listarHistorico($producto);			
			$this->cli_receta_model->deleteMovimiento($producto,$receta->id_receta);
			$this->cli_receta_model->deletereceta($receta->id_receta);
			$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
			$updateHistorico= array(
				'CANTIDAD_INVENTARIO'=>(int)$historico[0]->CANTIDAD_INVENTARIO+(int)$receta->cantidad
			);
			$this->cli_receta_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
			$updateInventario=array(
				'USUARIO_MODIFICACION'=>$usuario,
				'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
				'CANTIDAD'=>(int)$inventario[0]->CANTIDAD+(int)$receta->cantidad
			);

			$this->cli_receta_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);
							
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro actualizado adecuadamente</div>";
		
		}
			echo json_encode($response);
	}
}