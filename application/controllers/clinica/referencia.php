<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class referencia extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cli_consulta_model");
		$this->load->model("usuario_model");
		$this->load->model("cli_medico_model");
		$this->load->model("cli_referencia_model");
		$this->breadcrumbs->unshift(1,"Referencia","clinica/referencia");

	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Referencias');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
            if (empty($medico)) {
            	$medico=1;
            }
            $this->data['referencias']=$this->cli_referencia_model->listarReferencias();

            $this->template->admin_render("admin/cli_referencia/index",$this->data);
			
		}
		
	}

    public function excel_create()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login",'refresh');
        } else {
            $this->data['referencias']=$this->cli_referencia_model->listarReferencias();
            $this->load->view("admin/cli_referencia/excel",$this->data);
        }
        
    }

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nueva Referencia");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['pacientes']=$this->cli_referencia_model->listarPacientes();
			$this->data['especialidades']=$this->cli_referencia_model->listarEspecialidad();
			$this->load->view("admin/cli_referencia/referencia_modal",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Referencia');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Editar Referencia","clinica/referencia/editar");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
            if (empty($medico)) {
            	$medico=1;
            }
            $id=base64_decode($id);
            $this->data['referencia']=$this->cli_referencia_model->listarReferenciaId($id);
			$this->data['pacientes']=$this->cli_referencia_model->listarPacientes();
			$this->data['especialidades']=$this->cli_referencia_model->listarEspecialidad();
			$this->data['medicos']=$this->cli_referencia_model->listarMedico();
            $this->template->admin_render("admin/cli_referencia/referencia_editar",$this->data);
			
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$referencia=json_decode($this->input->post("ReferenciaPost"));
			$id=base64_decode($referencia->id);
			$deleteReferencia=array('ESTADO'=>0);
			$this->cli_referencia_model->deleteReferencia($id,$deleteReferencia);
			$response= array(
            	'response_msg' => ''
        	);
        	$response['response_msg']='<script>recargar();</script>';
		}
		echo json_encode($response);
	}

	public function listarMedicoEspecialidad()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$especialidad=json_decode($this->input->post("EspecialidadPost"));

			$response=array(
				'response_msg'=>'',
				'valor'=>''
			);
			$valor=$this->cli_referencia_model->listarMedicoEspecialidad($especialidad->id);
			
			if ($especialidad->id=='' or $especialidad->id=='0') {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Selecci&oacute;ne una opcion valida</div>";				
				$response['valor']="<option value=''>No contiene información</option>";				
			} else if(empty($valor)){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No hay doctor con esa especialidad</div>";				
				$response['valor']="<option value=''>No contiene información</option>";	
			} else {
				$response['valor']=$valor;

			}
				$json=json_encode($response);			
		}
		echo $json ;
		
	}
	public function vistaPDF($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Referencia Documento");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['Idreferencia']=base64_decode($id);
			$this->load->view('admin/cli_referencia/referencia_pdf',$this->data); 			
		}
		
	}	

	public function referenciaPDF($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			if($id==null){
	           $html="Ha ocurrido un error al realizar la operacion";
	           $this->mpdf->setDisplayMode('fullpage');
	           $this->mpdf->WriteHTML($html);
	           $this->mpdf->Output();
	           exit;
	        }else {
            $referencia=$this->cli_referencia_model->referenciaDocumento($id);
            $html='
            <!DOCTYPE html>
            <html>
            <head>
            <title>Referencia Clinica</title>
            </head>
            <body id="muestra">
            <style>

            @page{
            margin-top:     3%;
            margin-bottom:  5%;
            margin-right:   5%;
            margin-left:    5%;
            }

            div, table{  
            font-family: Arial;
            }

            #logo_clinica{
            width:20%;
            display: inline-block;
            float:left;
            }

            #logo_sistema{
                width:20%;
                display: inline-block;
                float:right;
            }

            #encabezado
            {
                width:60%;
                display:inline-block;
                float:left;
            }


            table{
                clear:both;
                text-align: left;
                height:auto;
                border-collapse:collapse; 
                table-layout:fixed;
                                width: 100%;

            }

            table td {
                word-wrap:break-word;
                padding:10px 0px 10px 0px;
            }

            </style>

            ';
            foreach ($referencia as $dt) {
               $html.='<div>
                <div id="logo_clinica" > 
                  <img src="img/escudo_mini.png" style="width:100; height=100;">
                </div>

                <div id="encabezado">
                <h3>Clinica Municipal de Antiguo Cuscatlan</h3>
                <h3>HOJA DE REFERENCIA Y CONSULTA EXTERNA</h3>
                </div>
                <table>
                <tr>
                    <td style="font-weight:bold;">Nombre del Paciente: </td><td>'.$dt->NOMBREPACIENTE.'</td>
                    <td style="font-weight:bold;">Edad: </td><td>'.$dt->EDAD.'</td>
                    <td style="font-weight:bold;">Sexo: </td><td>'.$dt->SEXO.'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Especialidad del medico que refiere:</td><td> '.$dt->EMEDICOREFIERE.'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Nº de EXPEDIENTE: </td><td>'.$dt->EXPEDIENTE.'</td>
                    <td style="font-weight:bold;">Fecha de Referencia: </td><td>'.$dt->FECHA_REFERENCIA.'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Clinica que env&iacute;a la referencia:</td><td>Clinica de Antiguo Cuscatlan </td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Especialidad del medico al que se refiere: </td><td>'.$dt->EMREFERIDO.'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Clinica a la que se refiere: </td><td>'.$dt->NOMBRE_CLINICA.'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Impresi&oacute;n Diagnostica:</td><td> '.$dt->IMPRESION.'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Datos positivos al interrogatorio y examen fisico:</td>                    
                </tr>
                <tr>
                    <td style="font-weight:bold;"> CABEZA: '.$dt->CABEZA.' CUELLO: '.$dt->CUELLO.' ABDOMEN: '.$dt->ABDOMEN.'
                    PULSOS: '.$dt->PULSOS.' FACIES: '.$dt->FACIES.' MIEMBROS INFERIORES: '.$dt->MIEMBROS_INFERIORES.' PIE:'.$dt->PIE.' GENITALES: '.$dt->GENITALES.' OTROS: '.$dt->OTROS.'
                    </td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">To: '.$dt->TEMPERATURA.' FR: '.$dt->FRECUENCIA_RESPIRATORIA.' FC: '.$dt->FRECUENCIA_CARDIACA.' TA:'.$dt->PRESION_ARTERIAL.' IMC: '.$dt->IMC.' GLUCOSA: '.$dt->GLUCOSA_CAPILAR.'</td>
                </tr>                
                <tr>
                    <td style="font-weight:bold;">Examens Realizados: </td>
                                        
                ';
            $examen=$this->cli_referencia_model->listaExamenesCita($dt->ID_CITA);
            if($examen!=null){
                foreach ($examen as $ex) {
                    $html.='<td>'.$ex->DESCRIPCION.'</td>';
                }
            } else {
                $html.='</tr><tr><td>No se realizaron examenes</td>';
            }
            $html.='
            </tr>
            <tr>
                <td style="font-weight:bold;">Doctor que realiza la referencia </td>            
            </tr>
            <tr>
                <td> Dr.'.$dt->NOMBREDOCTOR.'</td>
            </tr>
            </table>
            <div id="firma">
                _______________________________________
                <br><span>Firma y Sello</span>
            </div>';                
            }
             $this->load->library('pdf');
            $nombre_archivo = "REFERENCIA".$referencia[0]->NOMBREPACIENTE." ".$referencia[0]->FECHA_REFERENCIA."";
            $this->pdf->loadHtml($html);
            $this->pdf->setPaper('A4');
            $this->pdf->set_option('defaultMediaType', 'all');
            $this->pdf->set_option('isFontSubsettingEnabled', true);            
            $this->pdf->render();
            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
        
        }
		}
		
	}

	public function save(){
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
		 	$response = array(
		    		'response_msg' => '', 
		    		'campo' => '',
		            'error_msg'=> ''
		    	);
	    	$Referencia = json_decode($this->input->post('ReferenciaPost'));
    	if ($Referencia->confirmacion=="si" && $Referencia->idMedicoRecibe == "") {
    		$response['campo'] = "doctor";
    		$response['response_msg'] = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El campo doctor refiere es obligatorio, seleccione una especialidad y luego un doctor</div>";
    	}
    	elseif ($Referencia->confirmacion=="si" && $Referencia->idPaciente == ""){
    			$response['campo'] = "paciente";
    			$response['response_msg'] = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione un paciente, el campo es obligatorio</div>";
    	}
    	elseif ($Referencia->confirmacion=="no" && $Referencia->primerNombre=="") {
    		$response['campo'] = "primerNombre";
    		$response['response_msg'] = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el primer nombre del médico que refiere</div>";
    	}
    	elseif ($Referencia->confirmacion=="no" && $Referencia->segundoNombre=="") {
    		$response['campo'] = "segundoNombre";
    		$response['response_msg'] = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el segundo nombre del médico que refiere</div>";
       	}
       	elseif ($Referencia->confirmacion=="no" && $Referencia->primerApellido=="") {
       		$response['campo'] = "primerApellido";
    		$response['response_msg'] = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el primer apellido del médico que refiere</div>";
       	}
       	elseif($Referencia->confirmacion=="no" && $Referencia->segundoApellido==""){
       		$response['campo'] = "segundoApellido";
    		$response['response_msg'] = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el segundo apellido del médico que refiere</div>";	
       	}
    	elseif ($Referencia->confirmacion=="no" && $Referencia->correoDoc=="") {
    		$response['campo'] = "nombreDoc";
    		$response['response_msg'] = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese el correo del médico que refiere</div>";
    	}
    	
    	elseif ($Referencia->motivo =="") {
    		$response['campo'] = "motivoReferencia";
    		$response['response_msg'] = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El motivo de la referencia es un campo obligatorio </div>";
    	}
    	 elseif($Referencia->confirmacion=="no"&& $Referencia->nombreClinica==""){
            $response['campo'] = "nombreClinica";
            $response['response_msg'] = "<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese la clinica del medico que se va a referir</div>";
        }    	else {
    		if($Referencia->confirmacion=="si"){
				 $medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
				    if (empty($medico)) {
				       $medico=1;
				     }
				               
                if($Referencia->Id==""){
                    $registrarReferencia = array(
                        'ID_MEDICO_REFIERE'     => $medico,
                        'ID_MEDICO_RECIBE'      => $Referencia->idMedicoRecibe,
                        'ID_PACIENTE'           => $Referencia->idPaciente,
                        'NOMBRE_MEDICO_RECIBE'  => $Referencia->primerNombre.' '.$Referencia->segundoNombre.' '.$Referencia->primerApellido.' '.$Referencia->segundoApellido,
                        'CORREO_MEDICO_RECIBE'  => $Referencia->correoDoc,
                        'NOMBRE_CLINICA'        => $Referencia->nombreClinica,
                        'MOTIVO_REFERENCIA'     => $Referencia->motivo,
                        'FECHA_REFERENCIA'      => date("Y-m-d H:i:s")
                    );
                    $this->cli_referencia_model->saveReferencia($registrarReferencia);
				    $response['response_msg'] ="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado correctamente, se actualizara la informaci&oacute;n en unos segundos<script>location.reload();</script></div>";                                    
                } else {
						$fechaReferencia = date('Y-m-d H:i:s');
		                $especialidad= $this->cli_referencia_model->listarEspecialidadId($Referencia->idEspecialidad);
		                $nombreDoc=$Referencia->primerNombre.' '.$Referencia->segundoNombre.' '.$Referencia->primerApellido.' '.$Referencia->segundoApellido;
						$medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
				            if (empty($medico)) {
				            	$medico=1;
				            }                

                    $updateReferencia = array(
                        'ID_MEDICO_REFIERE'     => $medico,
                        'ID_MEDICO_RECIBE'      => $Referencia->idMedicoRecibe,
                        'ID_PACIENTE'           => $Referencia->idPaciente,
                        'NOMBRE_MEDICO_RECIBE'  => $nombreDoc,
                        'NOMBRE_CLINICA'        => $Referencia->nombreClinica,                        
                        'MOTIVO_REFERENCIA'     => $Referencia->motivo,
                        'FECHA_REFERENCIA'      => date("Y-m-d H:i:s")
                    );
                    $this->cli_referencia_model->updateReferencia($updateReferencia,$Referencia->Id);
                	$response['response_msg']='<script>recargar();</script>';
                }

    		}
    		else if ($Referencia->confirmacion=="no"){
    			/*$registrarPersona = array(
    				'PRIMER_NOMBRE' 		=>  ucwords($Referencia->primerNombre) ,
    				'SEGUNDO_NOMBRE' 		=> ucwords($Referencia->segundoNombre),
    				'PRIMER_APELLIDO' 		=> ucwords($Referencia->primerApellido),
    				'SEGUNDO_APELLIDO' 		=> ucwords($Referencia->segundoApellido),
    				'FECHA_REFERENCIA'		=> '1997-01-01',
    				'SEXO'					=> 'M',
    				'CORREO_ELECTRONICA'	=> $Referencia->correoDoc
    			);
    			$idPersona = $this->usuarios_model->savePersona($registrarPersona);

    			$registrarUsuario = array(
    				'ID_PERFIL_USUARIO' => 2 ,
    				'ID_PERSONA'		=> $idPersona,
    				'NOMBRE_USUARIO'	=> $Referencia->correoDoc,
    				'PASSWORD'			=> crypt('1234','$5$rounds=5000$$$$'),
    				'ESTADO_USUARIO'	=> 'I',
    				'URL_FOTO_USUARIO'	=> 'img/hombe/docM.png'
    			);
    			$idUsuario = $this->usuarios_model->saveUsuarios($registrarUsuario);
    			$idClinica = $this->usuarios_model->MostrarIdClinica();
    			$registrarMedico = array(
    				'ID_CLINICA' 	=> $idClinica,
    				'ID_PERSONA' 	=> $idPersona,
    				'VER_AGENDA'	=> 'N'
    			);
    			$codMedico = $this->clinica_model->saveMedico($registrarMedico);*/

    			$fechaReferencia = date('Y-m-d H:i:s');
                $especialidad= $this->cli_referencia_model->listarEspecialidadId($Referencia->idEspecialidad);
                $nombreDoc=$Referencia->primerNombre.' '.$Referencia->segundoNombre.' '.$Referencia->primerApellido.' '.$Referencia->segundoApellido;
				$medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
		            if (empty($medico)) {
		            	$medico=1;
		            }                
                if($Referencia->Id=="")
                {
                    $registrarReferencia = array(
                        'ID_MEDICO_REFIERE'		=>$medico,
                        'ID_PACIENTE'           => $Referencia->idPaciente,
                        'NOMBRE_MEDICO_RECIBE'  => $nombreDoc,
                        'CORREO_MEDICO_RECIBE'  => $Referencia->correoDoc,
                        'ESPECIALIDAD'          => $especialidad,
                        'MOTIVO_REFERENCIA'     => $Referencia->motivo,
                        'NOMBRE_CLINICA'        => $Referencia->nombreClinica,                        
                        'FECHA_REFERENCIA'      => $fechaReferencia
                    );
                    $this->cli_referencia_model->saveReferencia($registrarReferencia);
                    $response['response_msg'] ="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado correctamente, se actualizara la informaci&oacute;n en unos segundos<script>location.reload();</script></div>";
                } else {

                    $updateReferencia = array(
                        'ID_PACIENTE'           => $Referencia->idPaciente,
                        'NOMBRE_MEDICO_RECIBE'  => $nombreDoc,
                        'CORREO_MEDICO_RECIBE'  => $Referencia->correoDoc,
                        'ESPECIALIDAD'          => $especialidad,
                        'MOTIVO_REFERENCIA'     => $Referencia->motivo,
                        'NOMBRE_CLINICA'        => $Referencia->nombreClinica,                        
                        'FECHA_REFERENCIA'      => $fechaReferencia
                    );
                    $this->cli_referencia_model->updateReferencia($updateReferencia,$Referencia->Id);
                    $response['response_msg']='<script>recargar();</script>';
                }
			
    		}	
    		
    	}
    	echo json_encode($response);
	   			
		}
		
	}

}