<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class paciente extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cli_paciente_model");
		$this->load->model("cli_consulta_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Pacientes","clinica/paciente");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Pacientes');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['pacientes']=$this->cli_paciente_model->listarPacientes();

            $this->template->admin_render("admin/cli_pacientes/index",$this->data);			
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nuevo Paciente');
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/cli_pacientes/paciente_modal",$this->data);
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Paciente');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Paciente","clinica/paciente/editar");	
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $id=base64_decode($id);
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['paciente']=$this->cli_paciente_model->listarPacienteId($id);

            $this->template->admin_render("admin/cli_pacientes/paciente_editar",$this->data);			
		}		
	}

	public function expediente($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Expediente');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Expediente","clinica/paciente/expediente");	
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $id=base64_decode($id);
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['consultas']=$this->cli_paciente_model->listarHistorial($id);

            $this->data['consulta']=$this->cli_paciente_model->listarConsultaCita($id);
            $this->data['sustancias']=$this->cli_paciente_model->listarAlergiaSustancias();
            $this->data['alergias']=$this->cli_paciente_model->listarOtrasAlergias();
            $this->data['alergias_paciente']=$this->cli_paciente_model->listarAlergiasPaciente($id);
             $this->data['enfermedades_pacientes']=$this->cli_paciente_model->listarEnfermedadPaciente($id);
           
            $this->template->admin_render("admin/cli_pacientes/expediente_clinico",$this->data);			
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$paciente=json_decode($this->input->post("PacientePost"));
			$id=base64_decode($paciente->id);
			$response=array(
				'response_msg'=>''
			);
			$deletePaciente=array(
				'ESTADO'=>0
			);
			$this->cli_paciente_model->deletePaciente($id,$deletePaciente);
			$response['response_msg']="<script>recargar();</script>";
		}
		echo json_encode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$paciente=json_decode($this->input->post("PacientePost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);

			if ($paciente->primer_nombre=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El primer nombre del paciente es obligatorio</div>";

			} else if($paciente->primer_apellido=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El primer apelido del paciente es obligatorio</div>";			
			}  else if($paciente->fecha_nacimiento==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La fecha de nacimiento del paciente es obligatorio</div>";				
			}else {

				if ($paciente->id=='') {
					$registrarPaciente=array(
						'PRIMER_NOMBRE'=>$paciente->primer_nombre,
						'SEGUNDO_NOMBRE'=>$paciente->segundo_nombre,
						'TERCER_NOMBRE'=>$paciente->tercer_nombre,
						'PRIMER_APELLIDO'=>$paciente->primer_apellido,
						'SEGUNDO_APELLIDO'=>$paciente->segundo_apellido,
						'TERCER_APELLIDO'=>$paciente->tercer_apellido,
						'FECHA_NACIMIENTO'=>$paciente->fecha_nacimiento,
						'NOMBRE_MADRE'=>$paciente->nombre_madre,
						'NOMBRE_PADRE'=>$paciente->nombre_padre,
						'RESPONSABLE'=>$paciente->responsable,						
						'DIRECCION'=>$paciente->direccion,
						'DUI'=>$paciente->dui,
						'SEXO'=>$paciente->genero				
					);
					$id=$this->cli_paciente_model->savePaciente($registrarPaciente);
					$registrarExpediente=array(
						'ID_PACIENTE'=>$id,
						'CODIGO_EXPEDIENTE'=>$paciente->codigo_expediente
					);
					$this->cli_paciente_model->saveExpediente($registrarExpediente);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$updatePaciente=array(
						'PRIMER_NOMBRE'=>$paciente->primer_nombre,
						'SEGUNDO_NOMBRE'=>$paciente->segundo_nombre,
						'TERCER_NOMBRE'=>$paciente->tercer_nombre,
						'PRIMER_APELLIDO'=>$paciente->primer_apellido,
						'SEGUNDO_APELLIDO'=>$paciente->segundo_apellido,
						'TERCER_APELLIDO'=>$paciente->tercer_apellido,
						'FECHA_NACIMIENTO'=>$paciente->fecha_nacimiento,
						'NOMBRE_MADRE'=>$paciente->nombre_madre,
						'NOMBRE_PADRE'=>$paciente->nombre_padre,
						'RESPONSABLE'=>$paciente->responsable,
						'DIRECCION'=>$paciente->direccion,
						'DUI'=>$paciente->dui,
						'SEXO'=>$paciente->genero							
					);
					$this->cli_paciente_model->updatePaciente($paciente->id,$updatePaciente);
					$response['response_msg']="<script>recargar();</script>";			
				}				
			}
		}
		echo json_encode($response);
	}

	public function obtenerCodigo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo=$this->cli_paciente_model->ultimoExpediente();
			$cod=array('codigo'=>$codigo);
			$json=json_encode($cod);

		}
		echo $json;		
	}

}