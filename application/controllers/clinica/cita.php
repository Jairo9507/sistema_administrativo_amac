<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cita extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cli_cita_model");
		$this->load->model("usuario_model");
		$this->load->model("cli_medico_model");
		$this->breadcrumbs->unshift(1,"Citas","clinica/cita");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Asignaciones');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());


            $this->template->admin_render("admin/cli_cita/index",$this->data);
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nueva cita");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['pacientes']=$this->cli_cita_model->listarPacientes();
			$this->data['medicos']=$this->cli_cita_model->listarMedico();
			$this->data['estudios']=$this->cli_cita_model->listarEstudios();			
			$this->load->view("admin/cli_cita/cita_modal",$this->data);
		}
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Cita');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
				$this->breadcrumbs->unshift(2,"Editar Citas","clinica/cita/editar");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $id=base64_decode($id);

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$this->data['pacientes']=$this->cli_cita_model->listarPacientes();
			$this->data['medicos']=$this->cli_cita_model->listarMedico();			
			$this->data['cita']=$this->cli_cita_model->listarCitaId($id);
			$this->data['estudios_cita']=$this->cli_cita_model->listarEstudiosCita($id);
			$this->data['estudios']=$this->cli_cita_model->listarEstudios();
            $this->template->admin_render("admin/cli_cita/cita_editar",$this->data);
		}

	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$cita=json_decode($this->input->post("CitaPost"));
			$id=base64_decode($cita->id);
			$response =array(
				'response_msg'=>''
			);
			$deleteMedico=array(
				'ESTADO'=>'Cancelada'
			);
			$this->cli_cita_model->deleteCita($id,$deleteMedico);
			$response['response_msg']="<script>recargar();</script>";

		}
		echo json_encode($response);		
	}

	public function eliminarCita()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$cita=json_decode($this->input->post("CitaPost"));
			$response=array(
				'response_msg'=>''
			);
			$this->cli_cita_model->deleteCitaF($cita->id_cita);
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro Eliminado correctamente</div>";			
		}
		echo json_encode($response);
	}

	public function eliminarEstudio()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$cita=json_decode($this->input->post("EstudioCita"));

			$response= array(
				'response_msg'=>''
			);
			$this->cli_cita_model->deleteEstudioCita($cita->id);
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro Eliminado correctamente</div>";
		}
		echo json_encode($response);
		
	} 

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$cita=json_decode($this->input->post("CitaPost"));	
			$response=array(
				'response_msg'=>'',
				'campo'=>'',
				'cita_id'=>''
			);
			
			if ($cita->id_paciente=='' or $cita->id_paciente=='0') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La cita no puede realizarse sin los datos del paciente</div>";
				
			} else if($cita->estado==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El estado de la cita  es obligatorio</div>";

			} else if($cita->id_medico=='' or $cita->id_medico=='0') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre del medico es obligatorio</div>";
			} else {
				if ($cita->id=='') {
					$registrarCita=array(
						'ID_MEDICO'=>$cita->id_medico,
						'ID_PACIENTE'=>$cita->id_paciente,
						'FECHA_HORA'=>date('Y-m-d H:i:s'),
						'ESTADO'=>$cita->estado,
						'ASUNTO'=>'CITA'
					);
					$id=$this->cli_cita_model->saveCita($registrarCita);
					$response['cita_id']=$id;
					$response['response_msg']="<script>alertify.success('Registro guardado correctamente ')</script>";
				} else {
					$updateCita=array(
						'ID_MEDICO'=>$cita->id_medico,
						'ID_PACIENTE'=>$cita->id_paciente,
						'FECHA_HORA'=>date('Y-m-d H:i:s'),
						'ESTADO'=>$cita->estado
					);
					$this->cli_cita_model->updateCita($cita->id,$updateCita);
					for ($i=0; $i <sizeof($cita->estudios) ; $i++) { 
						if (!$this->cli_cita_model->EstudioExiste($cita->estudios[$i]->id,$cita->id)) {
							$registrarEstudio= array(
								'ESTADO' =>$cita->estudios[$i]->estado,
								'ID_CITA'=>$cita->id,
								'ID_CAT_ESTUDIO_CLINICO'=>$cita->estudios[$i]->id
							);
							$this->cli_cita_model->saveEstudioCita($registrarEstudio);							
						}
					}					
					$response['response_msg']="<script>recargar();</script>";
					
				}
				
			}
			


		}
		echo json_encode($response);
	}

	public function obtenerCitas()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {

			$medico=$this->cli_medico_model->obtenerIdMedico($this->ion_auth->get_user_id());
			if (empty($medico)) {
				$medico=1;				
			}
			$cita;
			$cita=$this->cli_cita_model->listarCitas($medico);
			$data=array();
			foreach ($cita as $c) {
				$data[] = array(
					'id' => $c->ID_CITA,
					'title'=> 'Paciente: '.$c->nombre_paciente.PHP_EOL.' Doctor: '.$c->nombre_medico,
					'start'=>$c->FECHA_INICIO,
					'end'=>$c->FECHA_FINAL
				);
			}

		}

		echo json_encode($data);
	}



	
}