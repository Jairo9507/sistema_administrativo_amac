<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class proyecto extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("proyecto_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Proyectos","catalogos/proyecto");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Proyectos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['proyectos']=$this->proyecto_model->listarProyecto();

            $this->template->admin_render("admin/proyectos/index",$this->data);			
		}
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nuevo proyecto");
			$this->data['pagetitle']=$this->page_title->show();

			$this->data['tipos'] = $this->proyecto_model->listarTiposProyectos();
			$this->data['prioridades'] = $this->proyecto_model->listarPrioridades();
            $this->data['empleados']=$this->usuario_model->listarEmpleados();

			$this->load->view("admin/proyectos/proyecto_modal",$this->data);
			
		}
		
	}	
	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Editar proyecto");
			$this->data['pagetitle']=$this->page_title->show();
			$id=base64_decode($id);
			$this->breadcrumbs->unshift(2,"Editar Proyecto","proyectos/proyecto/proyecto_edit");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['proyecto']=$this->proyecto_model->listarProyectoId($id);

			$this->data['tipos'] = $this->proyecto_model->listarTiposProyectos();
			$this->data['prioridades'] = $this->proyecto_model->listarPrioridades();
            $this->data['empleados']=$this->usuario_model->listarEmpleados();

			$this->template->admin_render("admin/proyectos/proyecto_edit",$this->data);			
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$tipo=json_decode($this->input->post("TipoPost"));

			$id=base64_decode($tipo->id);
			$response=array(
				'response_msg'=>''				
			);
			$deleteProyecto=array(
				'ESTADO' =>0
			);
			$this->proyecto_model->deleteProyecto($id,$deleteProyecto);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
		
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$proyecto=json_decode($this->input->post("ProyectoPost"));			
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($proyecto->codigo=='') {
				
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El c&oacute;digo del proyecto es obligatorio</div>";
			} else if($proyecto->nombre=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre del proyecto es obligatorio</div>";
			} else if($proyecto->fechaInicio==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La fecha inicio del proyecto es obligatoria</div>";				
			} else if($proyecto->supervisor==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El supervisor del proyecto es obligatorio</div>";		
			}else {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				if ($proyecto->id=='') {
					$registrarProyecto=array(
						'CODIGO_PROYECTO' => strtoupper($proyecto->codigo),
						'NOMBRE_PROYECTO' => strtoupper($proyecto->nombre),
						'ALCANCE_PROYECTO' => strtoupper($proyecto->alcance),
						'COSTO_PROYECTO' => (double)$proyecto->costo,
						'FECHA_INICIO_PROYECTO' => date('Y-m-d',strtotime($proyecto->fechaInicio)),
						'ID_SUPERVISOR' => $proyecto->supervisor,
						'ID_CAT_TIPO_PROYECTO' => $proyecto->tipo,
						'ID_CAT_PRIORIDAD' => $proyecto->prioridad,
						'UBICACION' => strtoupper($proyecto->ubicacion),
						'JUSTIFICANTE_PROYECTO'=> strtoupper($proyecto->justificante),
						'NOMBRE_REALIZADOR' => strtoupper($proyecto->realizador),
						'USUARIO_CREACION' => $usuario,
						'FECHA_CREACION' => date('Y-m-d')
					);
					$this->proyecto_model->saveProyecto($registrarProyecto);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$updateProyecto=array(
						'CODIGO_PROYECTO' => strtoupper($proyecto->codigo),
						'NOMBRE_PROYECTO' => strtoupper($proyecto->nombre),
						'ALCANCE_PROYECTO' => strtoupper($proyecto->alcance),
						'COSTO_PROYECTO' => (float)$proyecto->costo,
						'FECHA_INICIO_PROYECTO' => date('Y-m-d',strtotime($proyecto->fechaInicio)),
						'ID_SUPERVISOR' => $proyecto->supervisor,
						'ID_CAT_TIPO_PROYECTO' => $proyecto->tipo,
						'ID_CAT_PRIORIDAD' => $proyecto->prioridad,
						'UBICACION' => strtoupper($proyecto->ubicacion),
						'JUSTIFICANTE_PROYECTO'=> strtoupper($proyecto->justificante),
						'NOMBRE_REALIZADOR' => strtoupper($proyecto->realizador),
						'USUARIO_MODIFICACION' => $usuario,
						'FECHA_MODIFICACION' => date('Y-m-d')
					);
					$this->proyecto_model->updateProyecto($proyecto->id,$updateProyecto);
					$response['response_msg']="<script>recargar();</script>";
				}
			}
		}
		echo json_encode($response);

	}	

}
