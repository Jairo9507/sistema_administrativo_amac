<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class bitacora extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("pro_proyecto_bitacora_diaria_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Proyectos","proyectos/bitacora");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Proyectos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $idProyecto2=$this->input->get('idProyecto2', TRUE) ? $this->input->get('idProyecto2', TRUE) : "0";
            $this->data['bitacoras']=$this->pro_proyecto_bitacora_diaria_model->listarBitacorasDiaria($idProyecto2);
            $this->data['proyectos']=$this->pro_proyecto_bitacora_diaria_model->listarProyectos();
            $this->data['idProyecto2']= $idProyecto2;

            $this->template->admin_render("admin/pro_bitacora/index",$this->data);			
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nueva bitácora');
			$this->data['pagetitle']=$this->page_title->show();
			$idProyecto2=$this->input->get('idProyecto2', TRUE) ? $this->input->get('idProyecto2', TRUE) : "0";

			$this->data['proyecto']=$this->pro_proyecto_bitacora_diaria_model->obtenerNombreProyecto($idProyecto2);
			$this->data['tiposObras']=$this->pro_proyecto_bitacora_diaria_model->listarTiposObras();

			$this->load->view("admin/pro_bitacora/bitacora_modal",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Editar bitacora");
			$this->data['pagetitle']=$this->page_title->show();
			$id=base64_decode($id);
			$this->breadcrumbs->unshift(2,"Editar bitácora","proyectos/bitacora_edit");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['bitacora']=$this->pro_proyecto_bitacora_diaria_model->listarBitacorasDiariaId($id);

			$this->data['proyectos']=$this->pro_proyecto_bitacora_diaria_model->listarProyectos();
			$this->data['tiposObras']=$this->pro_proyecto_bitacora_diaria_model->listarTiposObras();

			$this->template->admin_render("admin/pro_bitacora/bitacora_edit",$this->data);			
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$tipo=json_decode($this->input->post("BitacoraPost"));
			$id=base64_decode($tipo->id);
			$response=array(
				'response_msg'=>''				
			);

			$this->pro_proyecto_bitacora_diaria_model->deleteBitacora($id);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
		
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$bitacora=json_decode($this->input->post("BitacoraPost"));			
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($bitacora->idCatTipoObra=='') {
				
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No ha seleccionado el tipo de obra</div>";
			} else if($bitacora->nombreSupervisor==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre del supervisor es obligatorio</div>";
			} else if($bitacora->fechaInicio==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La fecha de inicio es obligatoria</div>";		
			} else if($bitacora->horaCreacion==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La hora de inicio es obligatoria</div>";		
			} else if($bitacora->climaDia==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El clima del día es obligatorio</div>";		
			} else if($bitacora->descripcion==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La descripción es obligatoria</div>";		
			} else {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				if ($bitacora->id=='') {
					$registrarBitacora=array(
						'ID_CAT_TIPO_OBRA' => $bitacora->idCatTipoObra,
						'ID_PROYECTO' => $bitacora->idProyecto,
						'NOMBRE_SUPERVISOR' => strtoupper($bitacora->nombreSupervisor),
						'FECHA_INICIO' => date('Y-m-d',strtotime($bitacora->fechaInicio)),
						'HORA_CREACION' => $bitacora->horaCreacion,
						'CLIMA_DIA' => strtoupper($bitacora->climaDia),
						'DESCRIPCION' => strtoupper($bitacora->descripcion),
						'COMPROBANTE_FOTOGRAFICO' => $bitacora->comprobanteFotografico,
						'MANTENIMIENTO' => $bitacora->mantenimiento == 'true' ? TRUE : FALSE,
						'USUARIO_CREACION' => $usuario,
						'FECHA_CREACION' => date('Y-m-d')
					);
					$this->pro_proyecto_bitacora_diaria_model->saveBitacora($registrarBitacora);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$updateBitacora=array(
						'ID_CAT_TIPO_OBRA' => $bitacora->idCatTipoObra,
						'NOMBRE_SUPERVISOR' => strtoupper($bitacora->nombreSupervisor),
						'FECHA_INICIO' => date('Y-m-d',strtotime($bitacora->fechaInicio)),
						'HORA_CREACION' => $bitacora->horaCreacion,
						'CLIMA_DIA' => strtoupper($bitacora->climaDia),
						'DESCRIPCION' => strtoupper($bitacora->descripcion),
						'COMPROBANTE_FOTOGRAFICO' => $bitacora->comprobanteFotografico,
						'MANTENIMIENTO' => $bitacora->mantenimiento == 'true' ? TRUE : FALSE,
						'USUARIO_MODIFICACION' => $usuario,
						'FECHA_MODIFICACION' => date('Y-m-d')
					);
					$this->pro_proyecto_bitacora_diaria_model->updateBitacora($bitacora->id,$updateBitacora);
					$response['response_msg']="<script>recargar();</script>";
				}
			}
		}
		echo json_encode($response);

	}



}
?>