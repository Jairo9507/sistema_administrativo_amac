<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class proyecto_material_recibido extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("proyecto_model");
		$this->load->model("proyecto_material_recibido_model");
		$this->load->model("ordencompra_bodegas_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Proyectos","proyectos/proyecto");		
	}



	public function recibir($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Materiales recibidos");
			$this->data['pagetitle']=$this->page_title->show();
			$id=base64_decode($id);
			$this->breadcrumbs->unshift(2,"Materiales recibidos","proyectos/proyecto_material_recibido/recibir");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
			$unidad=$this->ion_auth->get_unidad_empleado();
			if (empty($unidad) || $unidad=='52' ) {
				$unidad=59;
			}

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['proyecto']=$this->proyecto_model->listarProyectoId($id);
			$this->data['ordenes']=$this->ordencompra_bodegas_model->listarDetalleProyectos($unidad);
            $materialRecibidoId=$this->proyecto_material_recibido_model->obtenerMaterialRecibidoId($id);
            $this->data['materiales']=$this->proyecto_material_recibido_model->listarOrdenesMateriales($materialRecibidoId);
            $this->data['detalles']= $this->proyecto_material_recibido_model->listarDetallesCompra($materialRecibidoId);
            $this->data['historicos']=$this->proyecto_material_recibido_model->listarDetallesMaterial($materialRecibidoId);
            $this->data['materialRecibidoId']=$materialRecibidoId;

			$this->template->admin_render("admin/proyectos_material_recibido/recibir",$this->data);
			
		}
		
	}	

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$tipo=json_decode($this->input->post("TipoPost"));

			$id=base64_decode($tipo->id);
			$response=array(
				'response_msg'=>''				
			);
			$deleteProyecto=array(
				'ESTADO' =>0
			);
			$this->proyecto_model->deleteProyecto($id,$deleteProyecto);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
		
	}

	public function materialesRecibidos()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$entrada=json_decode($this->input->post("EntradaPost"));			
			$response=array(
				'response_msg'=>'',
				'campo'=>'',
				'datos'=>''
			);
			if ($entrada->idProyecto=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El proyecto no esta activo</div>";
			} else if($entrada->idOrden==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione una orden de compra valida</div>";
			} else {
				$ordenes=implode(",",$entrada->idOrden);
				$listarMateriales=$this->ordencompra_bodegas_model->listarDetalleMateriales($ordenes);
				$response['datos']=$listarMateriales;
			}
			
			echo json_encode($response);
		}
	}	

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$material=json_decode($this->input->post("MaterialPost"));			
			$response=array(
				'response_msg'=>'',
				'campo'=>'',
				'datos'=>''
			);

			if ($material->idProyecto==''  ) {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El proyecto no esta activo</div>";
			} else if($material->idOrden=='' ){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione una orden de compra valida</div>";
			} else if ($material->idMaterial=='' ) {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$registarMaterialRecibido= array(
					'ID_PROYECTO' => $material->idProyecto,
					'USUARIO_CREACION' => $usuario,
					'FECHA_CREACION' => date('Y-m-d H:i:s')
				);

				$registroMaterialRecibidoId=$this->proyecto_material_recibido_model->saveMaterialRecibido($registarMaterialRecibido);
				for ($i=0; $i <count($material->idOrden) ; $i++) { 
					$registroMaterialOrden= array(
						'ID_MATERIAL_RECIBIDO'=>$registroMaterialRecibidoId,
						'ID_ORDEN_COMPRA' => $material->idOrden[$i],
					);
					$this->proyecto_material_recibido_model->saveMaterialRecibidoOrden($registroMaterialOrden);
				}

				for ($i=0; $i <count($material->idDetalleOrden) ; $i++) { 
					$registrarMaterialDetalle= array(
						'ID_DETALLE_ORDEN_COMPRA'=>$material->idDetalleOrden[$i],
						'ID_PROYECTO_MATERIAL_RECIBIDO' => $registroMaterialRecibidoId,
						'CANTIDAD_RECIBIDA' => $material->cantidadRecibida[$i],
						'FECHA_RECIBIDA' => $material->fechaRecibido[$i]
					);
					$this->proyecto_material_recibido_model->saveMaterialOrdenDetalle($registrarMaterialDetalle);
				}
				$response['response_msg']="<script>recargar();</script>";
			} else if($material->idMaterial!='' ) {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$updateMaterialRecibido= array(
					'USUARIO_MODIFICACION' => $usuario,
					'FECHA_MODIFICACION' => date('Y-m-d H:i:s')
				);
				$this->proyecto_material_recibido_model->updateMaterialRecibido($material->idMaterial,$updateMaterialRecibido);
				if ($material->idDetalle != '') {
					$registrarMaterialDetalle= array(
						'ID_DETALLE_ORDEN_COMPRA'=>$material->idDetalle,
						'ID_PROYECTO_MATERIAL_RECIBIDO' => $material->idMaterial,
						'CANTIDAD_RECIBIDA' => $material->cantidadRecibida,
						'FECHA_RECIBIDA' => $material->fechaRecibido
					);
					$this->proyecto_material_recibido_model->saveMaterialOrdenDetalle($registrarMaterialDetalle);
				} 
				

				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro actualizado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
			}

			
			echo json_encode($response);			
		}
		
	}




}
