<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class consumo extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("pro_proyecto_consumo_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Proyectos","proyectos/consumo");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Consumo');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $idProyecto2=$this->input->get('idProyecto2', TRUE) ? $this->input->get('idProyecto2', TRUE) : "0";


            $this->data['proyectos']=$this->pro_proyecto_consumo_model->listarProyectos();

            $this->data['consumos']=$this->pro_proyecto_consumo_model->listarConsumos($idProyecto2);
            $this->data['idProyecto2']=$idProyecto2;
            $this->template->admin_render("admin/pro_consumo/index",$this->data);			
		}
		
	}


	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nuevo consumo del proyecto");
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Nuevo consumo","proyectos/consumo");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $idProyecto2=$this->input->get('idProyecto2', TRUE) ? $this->input->get('idProyecto2', TRUE) : "0";


            $this->data['idProyecto2']=$idProyecto2;
            $this->data['proyecto']=$this->pro_proyecto_consumo_model->listarProyectoId($idProyecto2);
            $this->data['materiales']=$this->pro_proyecto_consumo_model->listarIngresos($idProyecto2);
            $this->data['detalles']='';
			$this->template->admin_render("admin/pro_consumo/consumo_edit",$this->data);
			
		}
		
	}	
	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$id=base64_decode($id);
			$this->page_title->push("Editar consumo del proyecto");
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Nuevo consumo","proyectos/consumo");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $idProyecto2=$this->input->get('idProyecto2', TRUE) ? $this->input->get('idProyecto2', TRUE) : "0";


            $this->data['idProyecto2']=$idProyecto2;
            $this->data['proyecto']=$this->pro_proyecto_consumo_model->listarProyectoId($idProyecto2);
            $this->data['materiales']=$this->pro_proyecto_consumo_model->listarIngresos($idProyecto2);
            $this->data['consumo']=$this->pro_proyecto_consumo_model->listarConsumoId($id);
            $this->data['detalles']=$this->pro_proyecto_consumo_model->listarDetalles($id);

			$this->template->admin_render("admin/pro_consumo/consumo_edit",$this->data);			
		}		
	}

	public function saveConsumo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$consumo=json_decode($this->input->post("consumoPost"));			
			$response=array(
				'response_msg'=>'',
				'campo'=>'',
				'idConsumo' => array()
			);
			if ($consumo->idProyecto=='') {
				
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Problemas con los datos del proyecto</div>";
			} else if($consumo->nombreRealizador == ''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Complete el nombre del realizador</div>";
			} else if($consumo->nombreSupervisor == ''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Complete el nombre del supervisor</div>";
			} else {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				if ($consumo->id == '') {
					$registrarConsumo= array(
						'ID_PROYECTO' => $consumo->idProyecto,
						'NOMBRE_REALIZADOR' => $consumo->nombreRealizador,
						'NOMBRE_SUPERVISOR' => $consumo->nombreSupervisor,
						'USUARIO_CREACION' => $usuario,
						'FECHA_CREACION' => date('Y-m-d')
					);
					$idConsumo= $this->pro_proyecto_consumo_model->saveConsumo($registrarConsumo);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente </script></div>";	
					$response['idConsumo']= $idConsumo;				
				} else {
					$updateConsumo= array(
						'NOMBRE_REALIZADOR' => $consumo->nombreRealizador,
						'NOMBRE_SUPERVISOR' => $consumo->nombreSupervisor,
						'USUARIO_MODIFICACION' => $usuario,
						'FECHA_MODIFICACION' => date('Y-m-d')
					);
					$this->pro_proyecto_consumo_model->updateConsumo($consumo->id,$updateConsumo);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro  adecuadamente </script></div>";					

				}
				
			}
			echo json_encode($response);
		}	
	}

	public function saveConsumoDetalle()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$detalle=json_decode($this->input->post("detallePost"));			
			$response=array(
				'response_msg'=>'',
				'campo'=>'',
				'historicos'=>'',
			);
			if ($detalle->idProyectoConsumo=='') {
				
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Problemas con los datos del proyecto</div>";
			} else if($detalle->idMaterialRecibido == ''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se encontro datos del material recibido</div>";
			} else if($detalle->fechaConsumida == ''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese una fecha valida</div>";
			}  else if($detalle->cantidadConsumida == ''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese una cantidad valida</div>";
			} else {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$registrarConsumoDetalle= array(
					'ID_PROYECTO_CONSUMO'=> $detalle->idProyectoConsumo,
					'ID_MATERIAL_RECIBIDO_DETALLE'=> $detalle->idMaterialRecibido,
					'CANTIDAD_CONSUMIDA'=> $detalle->cantidadConsumida,
					'FECHA_CONSUMO' => $detalle->fechaConsumida

				);
				$this->pro_proyecto_consumo_model->saveConsumoDetalle($registrarConsumoDetalle);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente</div>";
				$response['historicos']=$this->pro_proyecto_consumo_model->listarDetalles($detalle->idProyectoConsumo);	
				
			}
			echo json_encode($response);
		}		
	}



}
?>