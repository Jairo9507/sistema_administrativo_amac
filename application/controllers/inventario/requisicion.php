<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class requisicion extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("requisicion_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Requisiciones","inventario/requisicion");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Requisiciones');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $unidad=$this->ion_auth->get_unidad_empleado();

            if ($unidad==null) {
            	$unidad=42;

            } 

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['requisiciones']=$this->requisicion_model->listarRequisiciones($unidad);
            
            $this->template->admin_render("admin/requisicion/index",$this->data);
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nueva Requisici&oacute;n');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Nueva Requisici&oacute;n","inventario/requisicion/requisicion_crear");
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $unidad=$this->ion_auth->get_unidad_empleado();
            if ($unidad==null) {
            	$unidad=42;
            } 

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			$this->data['productos']=$this->requisicion_model->listarProductos($unidad);           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['unidades']=$this->requisicion_model->listarUnidadAdministrativa();
			$this->data['unidad']=$this->ion_auth->get_unidad_empleado();			
            $this->template->admin_render("admin/requisicion/requisicion_crear",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Requisici&oacute;n');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Requisici&oacute;n","inventario/requisicion/requisicion_editar");

			$id=base64_decode($id);
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			$unidad=$this->ion_auth->get_unidad_empleado();
            if (empty($unidad)) {
            	$unidad=42;
            } 

            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['unidades']=$this->requisicion_model->listarUnidadAdministrativa();
			$this->data['unidad']=$this->ion_auth->get_unidad_empleado();
			$this->data['requisicion']=$this->requisicion_model->listarRequisicionId($id);
			$this->data['detalles']=$this->requisicion_model->listarDetalles($id);
			$this->data['productos']=$this->requisicion_model->listarProductos($unidad);
            $this->template->admin_render("admin/requisicion/requisicion_editar",$this->data);			
		}		
	}

	public function cierre()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Cierre de Salidas");
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/requisicion/modal_cierre",$this->data);
		}
		
	}

	public function historico()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Intervalo de Historicos");
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/requisicion/modal_historico",$this->data);
		}
		
	}	

	public function cerrarRequisicion()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$cierre=json_decode($this->input->post("CierrePost"));

			$response=array(
				'response_msg'=>''				
			);
			$updateRequisicion=array(
				'ESTADO' =>2
			);
			$this->requisicion_model->cerrarRequisicion($cierre->fecha_inicio,$cierre->fecha_fin,$updateRequisicion);
			$response['response_msg']='<script>recargar();</script>';
						
		}
		echo json_encode($response);
	}

	public function verHistoricos()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$cierre=json_decode($this->input->post("CierrePost"));

			$response=array(
				'response_msg'=>''				
			);
			$updateRequisicion=array(
				'ESTADO' =>1
			);
			$this->requisicion_model->cerrarRequisicion($cierre->fecha_inicio,$cierre->fecha_fin,$updateRequisicion);
			$response['response_msg']='<script>recargar();</script>';
						
		}
		echo json_encode($response);
	}

	public function vistaProductos()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Lista de productos");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['productos']=$this->requisicion_model->listarProductos();			
			$this->load->view("admin/requisicion/modal_productos",$this->data);
					
		}		
	}

	public function obtenerCodigo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo = $this->requisicion_model->ultimoCodigo();	
			$cod=array('codigo'=>$codigo);
			$json=json_encode($cod);
			
		}
		echo $json;
		
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$requisicion=json_decode($this->input->post("requisicionPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if($requisicion->fecha==''){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La requisicion no se puede registrar sin fecha</div>";
			} else if($requisicion->unidad_solicita=='' or $requisicion->unidad_solicita=='0'){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La requisicion no se puede registrar sin unidad que solicita</div>";
			}else if($requisicion->fecha>date('Y-m-d')){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La requisicion no se puede registrar con una fecha mayor a la actual</div>";
			} else if($requisicion->id==''){
				$unidad=$this->ion_auth->get_unidad_empleado();
				if ($unidad==null) {
					$unidad=42;
				}
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$registrarRequisicion= array(
					'CORRELATIVO'=>$requisicion->correlativo,
					'FECHA'=>$requisicion->fecha." ".date('H:i:s'),
					'ID_UNIDAD_CREA'=>$unidad,
					'CONCEPTO'=>$requisicion->concepto,
					'ID_UNIDAD_SOLICITA'=>$requisicion->unidad_solicita,
					'USUARIO_CREACION'=>$usuario,
					'FECHA_CREACION'=>date('Y-m-d H:i:s')
				);

				$id=$this->requisicion_model->saveRequisicion($registrarRequisicion);
				$registrarRequiscionEmpleado=array(
					'ID_REQUISICION'=>$id,
					'ID_EMPLEADO_USUARIO'=>$this->ion_auth->get_user_id()
				);				
				$this->requisicion_model->saveRequisicionEmpleado($registrarRequiscionEmpleado);
				//var_dump($id);exit;
				$response['response_msg']='<input type="hidden" name="id" id="id_requisicion" value="'.$id.'">';
			} else {
				$unidad=$this->ion_auth->get_unidad_empleado();
				if ($unidad==null) {
					$unidad=42;
				}
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$updateRequisicion= array(					
					'FECHA'=>$requisicion->fecha." ".date('H:i:s'),
					'ID_UNIDAD_CREA'=>$unidad,
					'CONCEPTO'=>$requisicion->concepto,					
					'ID_UNIDAD_SOLICITA'=>$requisicion->unidad_solicita,
					'USUARIO_MODIFICACION'=>$usuario,
					'FECHA_MODIFICACION'=>date('Y-m-d H:i:s')
				);
				$this->requisicion_model->updateRequisicion($requisicion->id,$updateRequisicion);
			}
		}
		echo json_encode($response);
		
	}


	public function obtenerExistencia()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$producto=json_decode($this->input->post("ProductoPost"));			
			$inventario=$this->requisicion_model->listarInventario($producto->id);
			$existencia;
			
			if (empty($inventario)) {
				$existencia=0;
			} else {
				$existencia=$inventario[0]->CANTIDAD;
			}
			$cantidad=array("existencia"=>$existencia);
			$json=json_encode($cantidad);
		}
		echo $json;
	}
	public function saveDetalle()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$detalle=json_decode($this->input->post("DetallePost"));
			$response=array(
				'response_msg'=>'',
				'detalle_id'=>'',
				'validar' =>true,
				'existencia'=>''
			);

			if($detalle->id_producto==''){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione un producto de la lista</div>";
				$response['validar']=false;				
			}else if($detalle->cantidad=='' or $detalle->cantidad=='0'){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Ingrese una cantidad valida o superior a cero</div>";
				$response['validar']=false;
			} else {			

				$inventario=$this->requisicion_model->listarInventario($detalle->id_producto);	
				$historico=$this->requisicion_model->listarHistorico($detalle->id_producto,$detalle->fecha);
				$productoexistente=$this->requisicion_model->listarProductoExistente($detalle->id_producto,$detalle->id_requisicion,$detalle->cantidad);
				if (empty($inventario) || empty($historico)) {
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Debe existir un inventario del producto, tener existencias suficientes  y/o la fecha que se ingreso en la requisici&oacute;n es menor a la fecha de ingreso del historico  </div>";
					$response['validar']=false;

				} else if(!empty($productoexistente) && $detalle->id_detalle=='') {
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Los datos de este producto ya se encuentran agregados a la requisici&oacute;n</div>";
					$response['validar']=false;
				}else if($detalle->cantidad>$inventario[0]->CANTIDAD){
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La salida de producto no puede ser mayor a las existencias</div>";
					$response['validar']=false;
				}else {		
					if ($detalle->id_requisicion!='' && $detalle->id_detalle=='') {
						$registrardetalle=array(
							"CANTIDAD"=>$detalle->cantidad,
							"ID_PRODUCTO"=>$detalle->id_producto,
							"ID_REQUISICION"=>$detalle->id_requisicion
						);
						$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
						$costo=$this->requisicion_model->ultimoCosto($detalle->id_producto);
						if($historico[0]->CANTIDAD_INVENTARIO-$detalle->cantidad==0)
						{
							$updateHistorico= array(
								'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$detalle->cantidad,
								'FECHA_FIN_VIGENCIA'=>date('Y-m-d H:i:s'),
								'ESTADO'=>0
							);
							$this->requisicion_model->updateHistorico($historico[0]->ID_PRODUCTO,$updateHistorico);
						}else if($historico[0]->CANTIDAD_INVENTARIO-$detalle->cantidad<0){
								
								$restante=$historico[0]->CANTIDAD_INVENTARIO-$detalle->cantidad;	
								while(!empty($historico) or $restante!=0){
									if ($restante<0) {
									$updateHistorico= array(
										'CANTIDAD_INVENTARIO'=>0,
										'FECHA_FIN_VIGENCIA'=>date('Y-m-d H:i:s'),
										'ESTADO'=>0
									);
									$registrarMovimiento=array(
										'FECHA'=>$detalle->fecha." ".date('H:i:s'),
										'ID_PRODUCTO'=>$detalle->id_producto,
										'CANTIDAD'=>$historico[0]->CANTIDAD_INVENTARIO,
										'COSTO_UNITARIO'=>$historico[0]->PRECIO,
										'ID_TIPO_MOVIMIENTO'=>2,
										'USUARIO_CREACION'=>$usuario,
										'FECHA_CREACION'=>date('Y-m-d H:i:s'),
										'ID_REQUISICION'=>$detalle->id_requisicion,
										'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
									);
									$this->requisicion_model->saveMovimiento($registrarMovimiento);									
									$restante=$detalle->cantidad-$historico[0]->CANTIDAD_INVENTARIO;
									
									$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);	
									} else {			
										if ($restante<=$historico[0]->CANTIDAD_INVENTARIO && $restante!=0) {
											$updateHistorico= array(
												'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$restante
											);
											$registrarMovimiento=array(
												'FECHA'=>$detalle->fecha." ".date('H:i:s'),
												'ID_PRODUCTO'=>$detalle->id_producto,
												'CANTIDAD'=>$restante,
												'COSTO_UNITARIO'=>$historico[0]->PRECIO,
												'ID_TIPO_MOVIMIENTO'=>2,
												'USUARIO_CREACION'=>$usuario,
												'FECHA_CREACION'=>date('Y-m-d H:i:s'),
												'ID_REQUISICION'=>$detalle->id_requisicion,
												'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO												

											);
											$this->requisicion_model->saveMovimiento($registrarMovimiento);								
										$restante=$restante-$restante;
										$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);											
										} else {
											$updateHistorico= array(
												'CANTIDAD'=>$historico[0]->CANTIDAD_INVENTARIO-$restante							
											);
											$restante=$restante-$restante;
											
											$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
														
										}									
									}

									if($restante!=0){
										$historico=$this->requisicion_model->listarHistorico($detalle->id_producto,$detalle->fecha);
									} else {
										$historico=NULL;
									}				
									empty($historico);

								}
						} else {
									$updateHistorico= array(
										'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$detalle->cantidad,
									);
									$registrarMovimiento=array(
										'FECHA'=>$detalle->fecha." ".date('H:i:s'),
										'ID_PRODUCTO'=>$detalle->id_producto,
										'CANTIDAD'=>$detalle->cantidad,
										'COSTO_UNITARIO'=>$historico[0]->PRECIO,
										'ID_TIPO_MOVIMIENTO'=>2,
										'USUARIO_CREACION'=>$usuario,
										'FECHA_CREACION'=>date('Y-m-d H:i:s'),
										'ID_REQUISICION'=>$detalle->id_requisicion,
										'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
									);
									$this->requisicion_model->saveMovimiento($registrarMovimiento);									
									$restante=$detalle->cantidad-$historico[0]->CANTIDAD;
									
									$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);							
						}						
						if ($inventario[0]->CANTIDAD-$detalle->cantidad==0) {

							$updateInventario=array(
								'USUARIO_MODIFICACION'=>$usuario,
								'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
								'VIGENCIA'=>0,
								'FECHA_FIN_VIGENCIA'=>date('Y-m-d H:i:s')
							);
							$this->requisicion_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);
						}


							$updateInventario=array(
								'USUARIO_MODIFICACION'=>$usuario,
								'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
								'CANTIDAD'=>$inventario[0]->CANTIDAD-$detalle->cantidad,
							);
							 $this->requisicion_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);						
						
						$response['detalle_id']=$this->requisicion_model->saveDetalleRequisicion($registrardetalle);
						
						$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente</div>";
						$response['existencia']=$inventario[0]->CANTIDAD-$detalle->cantidad;
					} else if($detalle->id_requisicion!='' && $detalle->id_detalle!=''){
						$updatedetalle=array(
							"CANTIDAD"=>$detalle->cantidad
						);
						$registro=$this->requisicion_model->listarDetalleId($detalle->id_detalle);
						$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
						$historico=$this->requisicion_model->listarHistorico($detalle->id_producto,$detalle->fecha);
							if ($historico[0]->CANTIDAD_INVENTARIO-$detalle->cantidad==0) {
								$updateHistorico= array(
									'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$detalle->cantidad,
									'FECHA_FIN_VIGENCIA'=>date('Y-m-d H:i:s'),
									'ESTADO'=>0
								);
									$registrarMovimiento=array(
										'FECHA'=>$detalle->fecha." ".date('H:i:s'),
										'ID_PRODUCTO'=>$detalle->id_producto,
										'CANTIDAD'=>$detalle->cantidad,
										'COSTO_UNITARIO'=>$historico[0]->PRECIO,
										'ID_TIPO_MOVIMIENTO'=>2,
										'USUARIO_CREACION'=>$usuario,
										'FECHA_CREACION'=>date('Y-m-d H:i:s'),
										'ID_REQUISICION'=>$detalle->id_requisicion,
										'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
									);
									$this->requisicion_model->saveMovimiento($registrarMovimiento);												
								$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
							}else if($historico[0]->CANTIDAD_INVENTARIO-$detalle->cantidad<0){
								/*if (empty($historico)) {
									break;
								}*/
								$restante=$historico[0]->CANTIDAD_INVENTARIO-$detalle->cantidad;	
								while(!empty($historico) or $restante!=0){
									if ($restante<0) {
									$updateHistorico= array(
										'CANTIDAD_INVENTARIO'=>0,
										'FECHA_FIN_VIGENCIA'=>date('Y-m-d H:i:s'),
										'ESTADO'=>0
									);
									if (empty($detalle->fecha)) {
										$detalle->fecha=date('Y-m-d H:i:s');
									}
									$registrarMovimiento=array(
										'FECHA'=>$detalle->fecha." ".date('H:i:s'),
										'ID_PRODUCTO'=>$detalle->id_producto,
										'CANTIDAD'=>$historico[0]->CANTIDAD_INVENTARIO,
										'COSTO_UNITARIO'=>$historico[0]->PRECIO,
										'ID_TIPO_MOVIMIENTO'=>2,
										'USUARIO_CREACION'=>$usuario,
										'FECHA_CREACION'=>date('Y-m-d H:i:s'),
										'ID_REQUISICION'=>$detalle->id_requisicion,
										'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
									);
									$this->requisicion_model->saveMovimiento($registrarMovimiento);										
									$restante=$detalle->cantidad-$historico[0]->CANTIDAD_INVENTARIO;
									
									$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);	
									} else {				
										if ($historico[0]->CANTIDAD_INVENTARIO<=$restante && $restante!=0) {
											$updateHistorico= array(
												'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$restante,
												'FECHA_FIN_VIGENCIA'=>date('Y-m-d H:i:s'),
												'ESTADO'=>0
											);
											$registrarMovimiento=array(
												'FECHA'=>$detalle->fecha." ".date('H:i:s'),
												'ID_PRODUCTO'=>$detalle->id_producto,
												'CANTIDAD'=>$restante,
												'COSTO_UNITARIO'=>$historico[0]->PRECIO,
												'ID_TIPO_MOVIMIENTO'=>2,
												'USUARIO_CREACION'=>$usuario,
												'FECHA_CREACION'=>date('Y-m-d H:i:s'),
												'ID_REQUISICION'=>$detalle->id_requisicion,
												'ID_HISTORICO_PERTENECE'=>$historico[0]->ID_HISTORICO_INVENTARIO
											);
											$this->requisicion_model->saveMovimiento($registrarMovimiento);															
										$restante=$historico[0]->CANTIDAD_INVENTARIO-$restante;
										$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);											
										} else {
											$updateHistorico= array(
												'CANTIDAD_INVENTARIO'=>$historico[0]->CANTIDAD_INVENTARIO-$restante							
											);
											$restante=$restante-$restante;
											
											$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
														
										}									
									}

									if($restante!=0){
										$historico=$this->requisicion_model->listarHistorico($detalle->id_producto,$detalle->fecha);
									} else {
										$historico=NULL;
									}				
									empty($historico);

								}
							}

							if ($inventario[0]->CANTIDAD-$detalle->cantidad==0) {

								$updateInventario=array(
									'USUARIO_MODIFICACION'=>$usuario,
									'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
									'VIGENCIA'=>0,
									'FECHA_FIN_VIGENCIA'=>date('Y-m-d H:i:s')
								);
								$this->requisicion_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);
							}
							if ((int)$registro[0]->CANTIDAD-(int)$detalle->cantidad<0) {
								if (!empty($historico)) {
									$updateHistorico=array(
										'CANTIDAD_INVENTARIO'=>(int)$historico[0]->CANTIDAD_INVENTARIO-((int)$detalle->cantidad-(int)$registro[0]->CANTIDAD)
									);
									$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
								}
								$updateInventario= array(
									'CANTIDAD'=>(int)$inventario[0]->CANTIDAD-((int)$detalle->cantidad-(int)$registro[0]->CANTIDAD)
								);
								$this->requisicion_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);							
							} else {
								if (!empty($historico)) {
									$updateHistorico= array(
										'CANTIDAD_INVENTARIO'=>(int)$historico[0]->CANTIDAD_INVENTARIO+((int)$registro[0]->CANTIDAD-(int)$detalle->cantidad)
									);
									$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
								}
								$updateInventario= array(
									'CANTIDAD'=>(int)$inventario[0]->CANTIDAD+((int)$registro[0]->CANTIDAD-(int)$detalle->cantidad)
								);
								$this->requisicion_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);
							}												
						$this->requisicion_model->updateDetalleRequisicion($detalle->id_detalle,$updatedetalle);
							$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro actualizado adecuadamente</div>";
							$response['existencia']=$inventario[0]->CANTIDAD;
					} else {
						$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>";
					}
				}
			}	
		}
		echo json_encode($response);
		
	}

	public function deleteDetalle()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$detalle=json_decode($this->input->post("DetallePost"));
			$response=array(
				'response_msg'=>''		
			);
			$inventario=$this->requisicion_model->listarInventario($detalle->id_producto);	
			$historico=$this->requisicion_model->listarHistorico($detalle->id_producto,$detalle->fecha);
			$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());					
			$this->requisicion_model->deleteDetalle($detalle->id_detalle);
			$this->requisicion_model->deleteMovimiento($detalle->id_producto,$detalle->id_requisicion);
			$updateHistorico=array(
				'CANTIDAD_INVENTARIO'=>(int)$historico[0]->CANTIDAD_INVENTARIO+(int)$detalle->cantidad
			);
			$this->requisicion_model->updateHistorico($historico[0]->ID_HISTORICO_INVENTARIO,$updateHistorico);
			$updateInventario=array(
				'USUARIO_MODIFICACION'=>$usuario,
				'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
				'CANTIDAD'=>(int)$inventario[0]->CANTIDAD+(int)$detalle->cantidad,
			);
			$this->requisicion_model->updateInventario($inventario[0]->ID_INVENTARIO_FISICO,$updateInventario);			
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro actualizado adecuadamente</div>";
		}
			echo json_encode($response);
	}
}