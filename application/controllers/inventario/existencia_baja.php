<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class existencia_baja extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("existencia_baja_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Existencias bajas","inventario/existencia_baja");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Existencias Bajas');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           	$unidad=$this->ion_auth->get_unidad_empleado();
           	if ($unidad==null) {
           		$unidad=52;
           	}
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['existencias'] = $this->existencia_baja_model->listarExistenciasBajas($unidad);

            $this->template->admin_render("admin/existencia_baja/index",$this->data);

			
		}
		
	}
}