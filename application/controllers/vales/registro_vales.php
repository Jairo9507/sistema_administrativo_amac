<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class registro_vales extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("registro_vales_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Registro de vales de gasolina","vales/registro_vales");
	}

		public function index(){
		if (!$this->ion_auth->logged_in() ) {
			redirect("auth/login","refresh");
		} else {
			$this->page_title->push('Vales de gasolina');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            // $this->data['vales']=$this->registro_vales_model->listarVales();

            $this->template->admin_render("admin/vales/index",$this->data);

		}
	}

	public function nuevo()
	{
		if(!$this->ion_auth->logged_in() ){
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Registro de vales");
			$this->data['pagetitle']=$this->page_title->show();
			// $this->data['clase']=$this->cat_vehiculo_model->listarClase();
			// $this->data['tipo']=$this->cat_vehiculo_model->listarTipo();
			// $this->data['modelo']=$this->cat_vehiculo_model->listarModelo();
			// $this->data['combustible']=$this->cat_vehiculo_model->listarCombustible();
			// $this->data['marca']=$this->cat_vehiculo_model->listarMarca();
			// $this->data['unidad']=$this->cat_vehiculo_model->listarUnidad();
			$this->load->view("admin/vales/registro_vale_modal",$this->data);
		}
	}

	public function guardar(){
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login","refresh");
		} else {
			$Vale=json_decode($this->input->post("valePost"));
			$response=array(
				'response_msg'=>'',
				'campo' =>''
			);

			if($Vale->id==''){
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$idVehiculo = $this->registro_vales_model->obtenerId($Vale->placa);
				$registrarVale=array(
					'ID_VEHICULO' => $idVehiculo[0]->ID_VEHICULO,
					'NUMERO_VALE' => $Vale->num_vale,
					'USUARIO_CREACION' =>$usuario,
					'FECHA_CREACION'   =>date('Y-m-d H:i:s'),
					'FECHA' => $Vale->fecha,
					'PLACA' => $Vale->placa,
					'KILOMETRAJE'=>$Vale->kilometraje,
					'NUMERO_EQUIPO' => $Vale->num_equipo,
					'NOMBRE_EMPLEADO' =>$Vale->empleado,
					'GALONES' =>$Vale->galones,
					'VALOR' =>$Vale->valor,
					'OBSERVACIONES' => strtoupper($Vale->observaciones),
					'ESTADO' => 1,
				);
				$this->registro_vales_model->guardarVale($registrarVale);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
			} else {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$updateVale=array(
					'NUMERO_VALE' => $Vale->numVale,
					'USUARIO_MODIFICACION' =>$usuario,
					'FECHA_MODIFICACION'   =>date('Y-m-d H:i:s'),
					'GALONES' 	   =>$Vale->galones,
					'VALOR' 	   =>$Vale->valor,
					'KILOMETRAJE'  =>$Vale->kilometraje,
				);
				$this->registro_vales_model->updateVale($Vale->id, $updateVale);
				$response['response_msg']='<script>recargar();</script>';
			}
		}
		echo json_encode($response);
	}

	
}