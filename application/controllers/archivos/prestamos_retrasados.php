<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class prestamos_retrasados extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("prestamos_retrasados_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Prestamos retrasados","archivos/prestamos_retrasados");
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Prestamos retrasados');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           	$unidad=$this->ion_auth->get_unidad_empleado();
           	if ($unidad==null) {
           		$unidad=52;
           	}
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['prestamos'] = $this->prestamos_retrasados_model->listarRetrasos();

            $this->template->admin_render("admin/arc_prestamo_retrasado/index",$this->data);

			
		}
		
	}

}