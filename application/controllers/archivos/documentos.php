<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class documentos extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("documentos_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Documentos","archivos/documentos");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Documentos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['documentos']=$this->documentos_model->listarDocumentos();

			$this->template->admin_render("admin/arc_documento/index",$this->data);			
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nuevo Deposito');
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['series']=$this->documentos_model->listarSeries();
			$this->data['tipos']=$this->documentos_model->listarTipos();
			$this->data['pasillos']=$this->documentos_model->listarPasillos();
			$this->data['unidades']=$this->documentos_model->listarUnidades();
			$this->data['correlativo']=$this->documentos_model->obtenerCorrelativoCentral();
			$this->load->view("admin/arc_documento/documento_modal",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Deposito');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Editar Documento","archivos/documentos/editar");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$id=base64_decode($id);
			$this->data['series']=$this->documentos_model->listarSeries();
			$this->data['subseries']=$this->documentos_model->listarSubseries();
			$this->data['tipos']=$this->documentos_model->listarTipos();
			$this->data['pasillos']=$this->documentos_model->listarPasillos();
			$this->data['estantes']=$this->documentos_model->listarEstantes();
			$this->data['documento']=$this->documentos_model->listarDocumentoId($id);
			$this->data['unidad_comparte']=$this->documentos_model->listarUnidadComparten($id);
			$this->data['unidades']=$this->documentos_model->listarUnidades();
			$this->data['anaqueles']=$this->documentos_model->listarAnaqueles();			
			$this->template->admin_render("admin/arc_documento/documento_editar",$this->data);
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$documento=json_decode($this->input->post("DocumentoPost"));
			$id=base64_decode($documento->id);
			$response=array(
				'response_msg'=>''
			);
			$deleteDocumento=array(
				'ESTADO'=>0,
				'FECHA_SALIDA'=>date('Y-m-d H:i:s')
			);
			$this->documentos_model->deleteDocumento($id,$deleteDocumento);
			$response['response_msg']="<script>recargar();</script>";
		}
		echo json_encode($response);
	}

	public function obtenerCodigo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo = $this->documentos_model->obtenerCodigo();	
			$cod =array('codigo'=>$codigo);
			$json=json_encode($cod);
			
		}
		echo $json;
		
	}		

	public function obtenerCorrelativoCentral()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo = $this->documentos_model->obtenerCorrelativoCentral();	
			$cod =array('codigo'=>$codigo);
			$json=json_encode($cod);
			
		}
		echo $json;
		
	}			

	public function obtenerDepartamentos()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$departamento=json_decode($this->input->post("DepartamentoPost"));			
			$response=array(
				'response_msg'=>'',
				'valor'=>''
			);
			$valor=$this->documentos_model->listarDepartamentos($departamento->id);
			if ($departamento->id=='' or $departamento->id=='0') {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Selecci&oacute;ne una opcion valida</div>";	
				$response['valor']="<option value=''>No contiene información</option>";				
			} else if(empty($valor)){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Esta serie no contiene subseries</div>";				
				$response['valor']="<option value=''>No contiene información</option>";	
			} else {
				$response['valor']=$valor;
			}
			
		}
		echo json_encode($response);
		
	}

	public function obtenerSubseries()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$serie=json_decode($this->input->post("SeriePost"));			
			$response=array(
				'response_msg'=>'',
				'valor'=>''
			);
			$valor=$this->documentos_model->listarSubseriesId($serie->id);
			if ($serie->id=='' or $serie->id=='0') {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Selecci&oacute;ne una opcion valida</div>";	
				$response['valor']="<option value=''>No contiene información</option>";				
			} else if(empty($valor)){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Esta serie no contiene subseries</div>";				
				$response['valor']="<option value=''>No contiene información</option>";	
			} else {
				$response['valor']=$valor;
			}
		}
		echo json_encode($response);
	}

	public function obtenerEstantes()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$pasillo=json_decode($this->input->post("PasilloPost"));
			$response=array(
				'response_msg'=>'',
				'valor'=>''
			);
			$valor=$this->documentos_model->listarEstantesId($pasillo->id);
			if($pasillo->id=='' or $pasillo->id=='0'){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Selecci&oacute;ne una opcion valida</div>";	
				$response['valor']="<option value=''>No contiene información</option>";				
			} if(empty($valor)){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Esta serie no contiene subseries</div>";				
				$response['valor']="<option value=''>No contiene información</option>";	
			} else {
				$response['valor']=$valor;
			}			
		}
		echo json_encode($response);
	}

	public function obtenerAnaqueles()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$estante=json_decode($this->input->post("EstantePost"));
			
			$response=array(
				'response_msg'=>'',
				'valor'=>''
			);
			$valor=$this->documentos_model->listarAnquelesId($estante->id);
			if($estante->id=='' or $estante->id=='0'){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Selecci&oacute;ne una opcion valida</div>";	
				$response['valor']="<option value=''>No contiene información</option>";				
			} if(empty($valor)){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Esta serie no contiene subseries</div>";				
				$response['valor']="<option value=''>No contiene información</option>";	
			} else {
				$response['valor']=$valor;
			}			

		}
		echo json_encode($response);		
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$documento=json_decode($this->input->post("DocumentoPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($documento->codigo=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El c&oacute;digo del documento es obligatorio</div>";
			}  else if($documento->fecha_entrada=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La fecha de entrada del documento es obligatoria</div>";
			} else if($documento->unidad=='' or $documento->unidad=='0'){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La unidad a la que el documento pertenece es obligatoria</div>";
			} else if($documento->departamento=='' && $documento->departamento=='0'){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El departamento al que el documento pertenece es obligatorio</div>";
			}  else if($documento->anaquel=='' or $documento->anaquel=='0'){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La ubicaci&oacute;n en donde se almacenara es obligatorio</div>";				
			}else if($documento->serie=='' or $documento->serie=='0'){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La serie documental de la caja es obligatorio</div>";				
			} else {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				if ($documento->id=='') {
					
					if($documento->subserie==''){
						$documento->subserie=NULL;
					}  
					$registrarDocumento=array(
						'CODIGO_DOCUMENTO'=>$documento->codigo,
						'FECHA_ENTRADA'=>$documento->fecha_entrada,
						'FECHA_SALIDA'=>$documento->fecha_salida,
						'NUMERO_FOLIOS'=>$documento->no_folios,
						'ID_SERIE_DOCUMENTAL'=>$documento->serie,
						'ID_SUB_SERIE_DOCUMENTAL'=>$documento->subserie,
						'ID_CAT_ANAQUEL'=>$documento->anaquel,
						'ID_UNIDAD'=>$documento->departamento,
						'ID_GERENCIA'=>$documento->unidad,
						'TIPO'=>$documento->tipo,
						'VIGENCIA_GESTION'=>$documento->vigencia_gestion,
						'VIGENCIA_CENTRAL'=>$documento->vigencia_central,
						'VIGENCIA_DOCUMENTAL'=>$documento->vigencia_documental,
						'FRECUENCIA_CONSULTA'=>$documento->frecuencia,
						'CONTENIDO'=>$documento->contenido,
						'FECHA_DOCUMENTACION_INICIO'=>$documento->fecha_inicio,
						'FECHA_DOCUMENTACION_FINAL'=>$documento->fecha_final,
						'CORRELATIVO_GESTION'=>$documento->correlativo_gestion,
						'CORRELATIVO_CENTRAL'=>$documento->correlativo_central,
						'FECHA_SALIDA'=>$documento->fecha_salida,
						'ALMACENADO'=>$documento->almacenado,
						'VALOR_PRIMARIO'=>$documento->valor_primario,
						'VALOR_SECUNDARIO'=>$documento->valor_secundario,
						'DISPOSICION_FINAL'=>$documento->disposicion_final,
						'SOPORTE' =>$documento->soporte,
						'USUARIO_CREACION'=>$usuario,
						'FECHA_CREACION'=>date('Y-m-d H:i:s')
					);

					$id=$this->documentos_model->saveDocumento($registrarDocumento);
					for ($i=0; $i <sizeof($documento->unidad_comparte) ; $i++) { 
						$registarUnidadDocumento= array(
							'ID_DOCUMENTO'=>$id,
							'ID_UNIDAD'=>$documento->unidad_comparte[$i]
						);
						$this->documentos_model->saveUnidadComparte($registarUnidadDocumento);
					}

					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado correctamente, se actualizara la informaci&oacute;n en unos segundos<script>location.reload();</script></div>";
				} else {

					 if($documento->subserie==''){
						$documento->subserie=NULL;
					} 

				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
					//if ($this->ion_auth->get_perfil()=="8") {
					for ($i=0; $i <sizeof($documento->unidad_comparte) ; $i++) { 
							$registarUnidadDocumento= array(
								'ID_DOCUMENTO'=>$documento->id,
								'ID_UNIDAD'=>$documento->unidad_comparte[$i]
							);
							$this->documentos_model->saveUnidadComparte($registarUnidadDocumento);
					}

						$updateDocumento=array(
							'FECHA_ENTRADA'=>$documento->fecha_entrada,
							'FECHA_SALIDA'=>$documento->fecha_salida,
							'NUMERO_FOLIOS'=>$documento->no_folios,
							'ID_SERIE_DOCUMENTAL'=>$documento->serie,
							'ID_SUB_SERIE_DOCUMENTAL'=>$documento->subserie,
							'ID_CAT_ANAQUEL'=>$documento->anaquel,
							'ID_GERENCIA'=>$documento->unidad,
							'ID_UNIDAD'=>$documento->departamento,
							'TIPO'=>$documento->tipo,
							'VIGENCIA_GESTION'=>$documento->vigencia_gestion,
							'VIGENCIA_CENTRAL'=>$documento->vigencia_central,
							'VIGENCIA_DOCUMENTAL'=>$documento->vigencia_documental,
							'FRECUENCIA_CONSULTA'=>$documento->frecuencia,
							'CONTENIDO'=>$documento->contenido,
							'FECHA_DOCUMENTACION_INICIO'=>$documento->fecha_inicio,
							'FECHA_DOCUMENTACION_FINAL'=>$documento->fecha_final,
							'CORRELATIVO_GESTION'=>$documento->correlativo_gestion,
							'FECHA_SALIDA'=>$documento->fecha_salida,
							'ALMACENADO'=>$documento->almacenado,
							'VALOR_PRIMARIO'=>$documento->valor_primario,
							'VALOR_SECUNDARIO'=>$documento->valor_secundario,
							'DISPOSICION_FINAL'=>$documento->disposicion_final,
							'SOPORTE' =>$documento->soporte,		
							'PROCEDIMIENTO'=>$documento->procedimiento,
							'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
							'USUARIO_MODIFICACION'=>$usuario
						);
					//} else {
						/*$updateDocumento=array(
							'FECHA_ENTRADA'=>$documento->fecha_entrada,
							'FECHA_SALIDA'=>$documento->fecha_salida,
							'NUMERO_FOLIOS'=>$documento->no_folios,
							'ID_SERIE_DOCUMENTAL'=>$documento->serie,
							'ID_SUB_SERIE_DOCUMENTAL'=>$documento->subserie,
							'ID_CAT_ANAQUEL'=>$documento->anaquel,
							'TIPO'=>$documento->tipo,
							'VIGENCIA_GESTION'=>$documento->vigencia_gestion,
							'VIGENCIA_CENTRAL'=>$documento->vigencia_central,
							'VIGENCIA_DOCUMENTAL'=>$documento->vigencia_documental,
							'FRECUENCIA_CONSULTA'=>$documento->frecuencia,
							'CONTENIDO'=>$documento->contenido,
							'FECHA_DOCUMENTACION_INICIO'=>$documento->fecha_inicio,
							'FECHA_DOCUMENTACION_FINAL'=>$documento->fecha_final,
							'CORRELATIVO_GESTION'=>$documento->correlativo_gestion,
							'CORRELATIVO_CENTRAL'=>$documento->correlativo_central,
							'FECHA_SALIDA'=>$documento->fecha_salida,
							'ALMACENADO'=>$documento->almacenado,
							'VALOR_PRIMARIO'=>$documento->valor_primario,
							'VALOR_SECUNDARIO'=>$documento->valor_secundario,
							'DISPOSICION_FINAL'=>$documento->disposicion_final,
							'SOPORTE' =>$documento->soporte,									
							'PROCEDIMIENTO'=>$documento->procedimiento,
							'FECHA_MODIFICACION'=>date('Y-m-d H:i:s'),
							'USUARIO_MODIFICACION'=>$usuario
						);*/						
					//}

					$this->documentos_model->updateDocumento($documento->id,$updateDocumento);
					$response['response_msg']="<script>recargar();</script>";
				}
				
			}
			
		}
		echo json_encode($response);
	}

	public function excel_create()
	{
        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login",'refresh');
        } else {
            $empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
            if ($empleado==null) {
            	$empleado='Admin';
            }
            $this->data['empleado']=$empleado;        	
            $this->data['unidades']=$this->documentos_model->listarUnidadesExcel();
            $this->load->view("admin/arc_documento/catalogo_excel",$this->data);
        }		
	}

}