<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class transferencia extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("archivo_transferencia_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Prestamos","archivos/transferencia");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Prestamos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            //$unidad=$this->ion_auth->get_unidad_empleado();
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['transferencias']=$this->archivo_transferencia_model->listarTransferencias();

            $this->template->admin_render("admin/arc_transferencia/index",$this->data);            
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nuevo Prestamo');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
     		$this->breadcrumbs->unshift(2,"Nuevo Prestamo","archivos/transferencia/nuevo");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            //$unidad=$this->ion_auth->get_unidad_empleado();
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['unidades']=$this->archivo_transferencia_model->listarUnidades();
            $this->data['series']=$this->archivo_transferencia_model->listarSeries();
            $this->data['empleados']=$this->archivo_transferencia_model->listarEmpleados();
            $this->template->admin_render("admin/arc_transferencia/crear_transferencia",$this->data);            

		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Prestamo');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
     		$this->breadcrumbs->unshift(2,"Editar Prestamo","archivos/transferencia/editar");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            //$unidad=$this->ion_auth->get_unidad_empleado();
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           	$id=base64_decode($id);
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['unidades']=$this->archivo_transferencia_model->listarUnidades();
            $this->data['series']=$this->archivo_transferencia_model->listarSeries();
            $this->data['empleados']=$this->archivo_transferencia_model->listarEmpleados();
            $this->data['documentos']=$this->archivo_transferencia_model->listarDetallesId($id);
            $this->data['transferencia']=$this->archivo_transferencia_model->listarTransferenciaId($id);
            $this->template->admin_render("admin/arc_transferencia/editar_transferencia",$this->data);			
		}
		
	}

	public function obtenerCodigo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo = $this->archivo_transferencia_model->ultimoCodigo();	
			$cod=array('codigo'=>$codigo);
			$json=json_encode($cod);
			
		}
		echo $json;	
	}

		public function obtenerSubseries()
		{
			if (!$this->ion_auth->logged_in()) {
				redirect("auth/login",'refresh');
			} else {
				$serie=json_decode($this->input->post("SeriePost"));			
				$response=array(
					'response_msg'=>'',
					'subseries'=>'',
					'documentos'=>''
				);
				$subserie=$this->archivo_transferencia_model->listarSubseriesId($serie->id);

				$documento=$this->archivo_transferencia_model->listarDocumentoSeries($serie->id);
				if ($serie->id=='' or $serie->id=='0') {
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Selecci&oacute;ne una opcion valida</div>";	
					$response['subseries']="<option value=''>No contiene información</option>";		
					$response['documentos']="<option value=''>No contiene información</option>";								
				} else if(empty($subserie) && empty($documento)){
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Esta serie no contiene subseries</div>";				
					$response['subseries']="<option value=''>No contiene información</option>";	
				} else if(empty($documento)){ 
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Esta serie no contiene subseries</div>";
					$response['documentos']="<option value=''>No contiene información</option>";	

				}
					$response['subseries']=$subserie;
					$response['documentos']=$documento;
				

			}
		echo json_encode($response);
	}

	public function obtenerDocumentoSubserie()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$subserie=json_decode($this->input->post("SubseriePost"));
			$response=array(
				'response_msg'=>'',
				'documentos'=>''
			);
			$documentos=$this->archivo_transferencia_model->listarDocumentoSubseries($subserie->id);
				if ($subserie->id=='' or $subserie->id=='0') {
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Selecci&oacute;ne una opcion valida</div>";	
					$response['documentos']="<option value=''>No contiene información</option>";								
				} else if(empty($documentos)){ 
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Esta serie no contiene subseries</div>";
					$response['documentos']="<option value=''>No contiene información</option>";	

				}else {
					$response['documentos']=$documentos;
				}

		}
		echo json_encode($response);
	}

	public function saveTransferencia()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$transferencia=json_decode($this->input->post("TransferenciaPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if($transferencia->fecha==''){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La requisicion no se puede registrar sin fecha</div>";
			} else if($transferencia->unidad_solicita=='' or $transferencia->unidad_solicita=='0'){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La requisicion no se puede registrar sin unidad que solicita</div>";
			} else {
				if ($transferencia->id=='') {
					$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
					$registrarTransferencia=array(
						'ID_UNIDAD_SOLICITA'=>$transferencia->unidad_solicita,
						'FECHA_TRANSFERENCIA'=>$transferencia->fecha,
						'CODIGO'=>$transferencia->codigo,
						'ID_RESPONSABLE_ARCHIVO_GESTION'=>$transferencia->responsable,
						'NO_CAJAS'=>$transferencia->no_cajas,
						'USUARIO_CREACION'=>$usuario,
						'FECHA_CREACION'=>date('Y-m-d H:i:s')
					);
					$id=$this->archivo_transferencia_model->saveTransferencia($registrarTransferencia);
					$response['campo']=$id;
				} else {
					$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
					$updateTransferencia=array(
						'ID_UNIDAD_SOLICITA'=>$transferencia->unidad_solicita,
						'FECHA_TRANSFERENCIA'=>$transferencia->fecha,
						'CODIGO'=>$transferencia->codigo,
						'ID_RESPONSABLE_ARCHIVO_GESTION'=>$transferencia->responsable,
						'NO_CAJAS'=>$transferencia->no_cajas,
						'USUARIO_MODIFICACION'=>$usuario,
						'FECHA_MODIFICACION'=>date('Y-m-d H:i:s')
					);
					$this->archivo_transferencia_model->updateTransferencia($transferencia->id,$updateTransferencia);
				}
				
			}			
		}
		echo json_encode($response);	
	}

	public function saveDetalle()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$detalle=json_decode($this->input->post("DetallePost"));
			$response=array(
				'response_msg'=>'',
				'valor'=>'',
				'campo'=>''
			);
			if (empty($detalle)) {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione un valor de la lista</div>";				
			} else {
					if ($detalle->fecha_salida=='') {
						$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La fecha de transferencia de salida </div>";
					} else if($detalle->id_documento==''){
						$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Seleccione el documento a transferir </div>";
					} else {
						if ($detalle->id=='') {
							$registrarTransferencia=array(
								'ID_TRANSFERENCIA'=>$detalle->transferencia,
								'FECHA_SALIDA_ARCHIVO_CENTRAL'=>$detalle->fecha_salida,
								'FECHA_REGRESO_ARCHIVO_CENTRAL'=>$detalle->fecha_regreso,
								'ID_DOCUMENTO'=>$detalle->id_documento,
								'OBSERVACIONES'=>$detalle->observaciones
							);
							$updateDocumento =array(
								'ESTADO'=>2,
							);
							$this->archivo_transferencia_model->updateDocumento($detalle->id_documento,$updateDocumento);
							$this->archivo_transferencia_model->saveDetalle($registrarTransferencia);
							$response['valor']=$this->archivo_transferencia_model->listarDetallesId($detalle->transferencia);
						}
					}
			}
		}
		echo json_encode($response);	
	}

	public function deleteDetalle()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$detalle=json_decode($this->input->post("DetallePost"));
			$response=array(
				'response_msg'=>''
			);
			$updateDocumento=array(
				'ESTADO'=>1
			);
			$this->archivo_transferencia_model->deleteDetalle($detalle->id);
			$this->archivo_transferencia_model->updateDocumento($detalle->id_documento,$updateDocumento);
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro actualizado adecuadamente</div>";			
		}
		echo json_encode($response);
	}

	public function retornoDocumento()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$documento=json_decode($this->input->post("DocumentoPost"));

			$response=array(
				'response_msg'=>'',
				'fecha'=>''
			);
			$updateDocumento= array(
				'ESTADO'=>1
			);
			$updateTransferencia=array(
				'FECHA_RETORNO_REAL'=>date('Y-m-d')
			);
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El documento ha sido regresado al inventario</div>";	
			$response['fecha']=date('d-m-Y');
			$this->archivo_transferencia_model->updateDetalleTransferencia($documento->id_transferencia,$updateTransferencia);
			$this->archivo_transferencia_model->updateDocumento(base64_decode($documento->id),$updateDocumento);					
		}
		echo json_encode($response);
	}
}