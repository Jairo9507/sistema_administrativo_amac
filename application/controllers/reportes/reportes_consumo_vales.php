<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_consumo_vales extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("reportes_consumo_vales_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Reportes","reportes/reportes_consumo_vales");	
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes de consumo de vales');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['vehiculos']=$this->reportes_consumo_vales_model->listarVehiculos();
			$this->data['unidades']=$this->reportes_consumo_vales_model->listarUnidades();
			/*Obtener lista de actividades */
			$this->template->admin_render("admin/reportes/index_vales",$this->data);			
		}		
	}

	public function obtenerVehiculosUnidad()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$unidad=json_decode($this->input->post("UnidadPost"));
			$response=array(
				'response_msg'=>'',
				'valor'=>''
			);
			$valor=$this->reportes_consumo_vales_model->listarUnidadVehiculo($unidad->id);
			if ($unidad->id=='' or $unidad->id=='0') {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Selecci&oacute;ne una opcion valida</div>";	
				$response['valor']="<option value=''>No contiene información</option>";				
			} else if(empty($valor)){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Esta serie no contiene subseries</div>";				
				$response['valor']="<option value=''>No contiene información</option>";	
			} else {
				$response['valor']=$valor;
			}

		}
		echo json_encode($response);		
	}

	public function vistaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes de Inventario documental');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Reportes vista","reportes/reportes_inventario_documental/vistaPDF");		
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());			
			$datos= array(
				'fechaInicio'=>$_POST['fechaInicio'],
				'fechaFinal'=>$_POST['fechaFinal'],
				'idVehiculo'=>$_POST['vehiculo_id'],
				'idUnidad'=>$_POST['unidad_id'],
				'autoriza'=>$_POST['autoriza']
			);
			$this->data['datos']=$datos;

			$this->template->admin_render("admin/reportes/consumo_vales_pdf",$this->data);			
		}
		
	}

	public function consumoPDF($p1,$p2,$p3,$p4,$p5)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->load->library("pdf");
			if (empty($p1) && empty($p2) && empty($p3) && empty($p4)) {
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de proveedores de".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
           		exit;
			} else {
				$vehiculo='';
				if ($p3!='' or $p3=='0') {
					$vehiculo=" and v.ID_VEHICULO=".$p3;
				}
				
				$detalle=$this->reportes_consumo_vales_model->listarVales($p1,$p2,$vehiculo,$p4);
				$sumagalones=0.0;
				$sumavalor=0.0;
            	$empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
            	$unidad=$this->ion_auth->get_unidad_empleado();
   				
            	$jefatura=$this->reportes_consumo_vales_model->nombreJefatura($unidad);
            	if ($empleado==null) {
            		$empleado='Admin';
            	} 
            	if ($jefatura==null) {
            		$jefatura='Admin';
            	}
				$html='
			            <!DOCTYPE HTML PUBLIC ">
			            <html>
			            <head>
			            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

			            <title>Consumo de vales</title>
			            <style type="text/css">

			            @page {
			                margin: 1.4cm;
			            }


			        #header,
			        #footer {
			          position: fixed;
			          left: 0;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			          
			      }
			      #footer {
			          bottom: 0;
			          border-top: 0.1pt solid #aaa;
			      }
						#logo_clinica{
						    width:80%;
						    display: inline-block;
						    float:left;
						}

						#logo_sistema{
						    width:20%;
						    display: inline-block;
						    float:right;
						}
						#encabezado
						{
						    width:100%;
							padding-top:100px;
						    display:inline-block;
						    float:right;
						}
						body {
						   font-family: Sans-Serif;
						    margin: 0.5cm 0;
						    text-align: justify;
						}
						#tet{
						    border-radius:15px;
						    width: 13%;
						    padding: 3% 5%;
						    display: inline-block;
						    float:right;
						    text-align: center;
						    border:1px solid red;
						    color: red;
						}
						#prueba{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						#prueba2{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						 #foot{
						    width: 90%;
						    position:absolute;
						    text-align: center;
						    bottom:-5%;
						    }
						table{
						    clear:both;
						    text-align: left;
						    height:auto;
						    border-collapse:collapse; 
						    table-layout:fixed;
						    width: 100%;
						}
						#cuerpo{
						    position: abosolute;
						    text-align: center;
						    bottom=12%;
						}

						table td {
						    word-wrap:break-word;
						    table-layout:auto;

						}



						.page-number {
						  text-align: center;
						}

						.page-number:before {
						  content: "Página " counter(page);
						}

						hr {
						  page-break-after: always;
						  border: 0;
						}

						</style>
						</head>';
						$html.='
						<body>
							<div id="header">
							<div id="logo_clinica"></div>
							<div id="encabezado">
							    <h3 style="text-align:center;height:0%;">Alcaldía Municipal de Antiguo Cuscatlan</h3>
							    <h5 style="text-align:center;height:0%;">Departamento de la Libertad, El Salvador, C.A</h5>
							    <h5 style="text-align:center;">PBX:2511-0100/2511-0121</h5>
							    <h3 style="text-align:justify;">Consumo de vales de '.$detalle[0]->CODIGO_UNIDAD.' '.$detalle[0]->UNIDAD.'</h3>
							    <h4 style="text-align:justify">Fecha de impresi&oacute;n: '.date('d-m-Y').'</h4>
							</div>
							</div>	
							<table  style="border:1px solid; width: 100%;">
								<thead  >
								<tr>
									<th style="border:1px solid;width:8%;">Numero de equipo</th>
									<th style="border:1px solid;width:16%;">placa</th>
									<th style="border:1px solid;width:10%;">fecha</th>
									<th style="border:1px solid;width:8%;">galones</th>
									<th style="border:1px solid;width:8%;">valor</th>
									<th style="border:1px solid;width:10%;">kilometraje</th>
									<th style="border:1px solid;width:12%;">numero de vale</th>
								</tr>
								</thead>
								<tbody>';
								foreach ($detalle as $dt) {
									$html.='
									<tr style="padding-bottom:25px; ">
									<td style="border:1px solid" >'.$dt->CODIGO_VEHICULO.'</td>
									<td style="border:1px solid">'.$dt->PLACA.'</td>
									<td style="border:1px solid">'.$dt->FECHA.'</td>
									<td style="border:1px solid">'.$dt->GALONES.'</td>
									<td style="border:1px solid">'.$dt->VALOR.'</td>
									<td style="border:1px solid">'.$dt->KILOMETRAJE.'</td>
									<td style="border:1px solid">'.$dt->NUMERO_VALE.'</td>
									</tr>
									';
									$sumagalones=(float)$sumagalones+(float)$dt->GALONES;
									$sumavalor=(float)$sumavalor+(float)$dt->VALOR;
								}

							$html.='
							<tr>
							<td style="border:1px solid"></td>
							<td style="border:1px solid"></td>
							<td style="border:1px solid"></td>
							<td style="border:1px solid">'.number_format((float)$sumagalones,2).'</td>
							<td style="border:1px solid">'.number_format((float)$sumavalor,2).'</td>
							<td style="border:1px solid"></td>
							<td style="border:1px solid"></td>
							</tr>
							</tbody>
							</table>';
					
					$html.='
					<div id="foot">
					<table>
					<tr>
					<td style="text-align:center;width:10%;"></td>
					<td style="text-align:center;">__________________</td>
					<td style="text-align:center;">__________________</td>
					<td style="text-align:center;">__________________</td>
					</tr>
					<tr>
					<td style="text-align:center;"></td>
					<td style="text-align:center;">Autorizado por <br/>'.str_replace("%20"," ",$p5).'</td>
					<td style="text-align:center;">Realizado por <br/>'.$empleado.'</td>
					<td style="text-align:center;"> Jefe de Control de Vehiculos y Combustibles <br/>'.$jefatura.'</td>

					</tr>
					</table>
					</div>
					</body></html>
					';          							
		            $nombre_archivo = "Conumo de vales de".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
				$fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('Arial','bold');
				$canvas->page_text(500, 35, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 12, array(0, 0, 0));	            
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));				
			}
		}
		
	}
}
