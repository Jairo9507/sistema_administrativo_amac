<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_orden_uaci extends Admin_Controller //el controlador debe llmarse igual que el archivo
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model("menu_model");
        $this->load->model("rep_orden_uaci_model");
        $this->load->model("usuario_model");
        $this->breadcrumbs->unshift(1,"Reportes","reportes/reportes_orden_uaci");   //La ruta es la carpeta del controlador, la cual esta en controllers->catalogos/cat_anaqueles
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login",'refresh');
        } else {
            $this->page_title->push('Reportes de Ordenes de Compra');
            $this->data['pagetitle']=$this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
            
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /*Obtener lista de actividades */

            $this->data['proveedores'] = $this->rep_orden_uaci_model->listarProveedor();
            $this->data['productos'] = $this->rep_orden_uaci_model->listarProductos();
            $this->data['cuentas'] = $this->rep_orden_uaci_model->listarCuentas();
            $this->template->admin_render("admin/reportes/index_uaci",$this->data); //la ruta depende de la carpeta donde se este la vista OJO-> en este caso la vista esta en la carpeta View -> admin -> cat_anaque->index
        }
    }

    public function vistaPDF()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login",'refresh');
        } else {
                   
            $this->page_title->push("Orden de Compra");
            $this->data['pagetitle']=$this->page_title->show();
            $this->load->view('admin/reportes/orden_compra_pdf',$this->data);           
        }
    }

        public function ordenPDF()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login",'refresh');
        } else {
            $fecha = date('d-m-Y');
            $fechaInicio         = $_POST['fechaInicio'];
            $fechaFin            = $_POST['fechaFin'];
            $proveedor           = $_POST['proveedor'];
            $producto            = $_POST['producto'];
            $descripcion         = $_POST['descripcion'];
            $concepto            = $_POST['concepto'];
            $cuenta              = $_POST['cuenta'];
            $estado              = $_POST['estado']; 
            
            // var_dump($estado);exit;
            
            if($fechaInicio == '' and $fechaFin == '' and $proveedor==0 and $producto==0 and $descripcion=='' and $concepto=='' and $cuenta==0 and $estado==8 ){
               $html="Ha ocurrido un error al realizar la operación";
               $this->mpdf->setDisplayMode('fullpage');
               $this->mpdf->WriteHTML($html);
               $this->mpdf->Output();
               exit;
            }else {
                 // var_dump($fechaInicio, $fechaFin, $proveedor, $producto, $descripcion, $concepto, $cuenta, $estado);exit;
                $detalle = $this->rep_orden_uaci_model->obtenerReporte($fechaInicio, $fechaFin, $proveedor, $producto, $descripcion, $concepto, $cuenta, $estado);
                // var_dump($detalle);exit;
                $suma=0.0;
                $empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
                if ($empleado==null) {
                    $empleado='Admin';
                }
            $html='
            <!DOCTYPE HTML PUBLIC ">
            <html>
            <head>
            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

            <title>Orden de Compra</title>
            <style type="text/css">

            @page {
                margin: 1.3cm;
            }


        #header,
        #footer {
          position: fixed;
          left: 0;
          right: 0;
          font-size: 0.9em;
      }
      #header {
          top: 0;
          
      }
      #footer {
          bottom: 0;
          border-top: 0.1pt solid #aaa;
      }
    #logo_clinica{
    width:20%;
    display: inline-block;
    float:left;
    }

    #logo_sistema{
        width:20%;
        display: inline-block;
        float:right;
    }
    #encabezado
    {
        width:60%;
        display:inline-block;
        float:left;
    }
    body {
       font-family: Sans-Serif;
        margin: 0.5cm 0;
        text-align: justify;
    }
    #tet{
        border-radius:15px;
        width: 13%;
        padding: 3% 5%;
        display: inline-block;
        float:right;
        text-align: center;
        border:1px solid red;
        color: red;
    }
    #prueba{
        position: relative;
        top: 10%;
        text-align:center;
        left: 2%;
      }
    #prueba2{
        position: relative;
        top: 10%;
        text-align:center;
        left: 2%;
      }
     #foot{
        width: 90%;
        position:absolute;
        text-align: center;
        bottom:-5%;
        padding:-20px;
        }
    table{
        clear:both;
        text-align: left;
        height:auto;
        border-collapse:collapse; 
        table-layout:fixed;
        width: 100%;
    }

    #cuerpo{
        position: abosolute;
        text-align: center;
        bottom=12%;
    }

    table td {
        word-wrap:break-word;
        padding:10px 0px 10px 0px;
    }

    .page-number {
      text-align: center;
    }

    .page-number:before {
      content: "Página " counter(page);
    }

    hr {
      page-break-after: always;
      border: 0;
    }

</style>
</head>';
$html.='
<body>
<div id="header">
<div id="logo_clinica" > 
<img src="img/escudo_mini.png" style="width:100; height=100;">
</div>
<div id="encabezado">
    <h3 style="text-align:center;height:0%;">Alcaldía Municipal de Antiguo Cuscatlan</h3>
    <h5 style="text-align:center;height:0%;">Departamento de la Libertad, El Salvador, C.A</h5>
    <h5 style="text-align:center;">PBX:2511-0100/2511-0121</h5>
    <h4 style="text-align:center;">REPORTE DE ORDENES DE COMPRA</h4>
</div>

<div style="font-size:11px;text-align:center;">Creado por: '.$empleado.'</div>
<div style="font-size:11px;text-align:center;">Fecha:'.$fecha.' </div>
</div>
';
$html.='

<div id="prueba">

        <table style="border:1px solid;" >
        <thead>
        <tr>
        <th style="text-align:center;border:1px solid;width:15%;">N° ORDEN COMPRA</th>
        <th style="text-align:center;border:1px solid;width:20%;">FECHA</th>
        <th style="text-align:center;border:1px solid;width:30%;">PROVEEDOR</td>
        <th style="text-align:center;border:1px solid;">MONTO TOTAL</th>
        <th style="text-align:center;border:1px solid;">CONDICIÓN DE PAGO</th>
        </tr>
        </thead>
        </table>
    </div>
    ';
    if ($detalle) {
        foreach ($detalle as $det) {

        $html.='
        <div id="prueba">
            <table style="border:1px solid;">
            <tbody>
            <tr>
            <td style="text-align:center;border:1px solid;font-size:11px;width:15%;">'.$det->CODIGO_ORDEN_COMPRA.'</td>
            <td style="text-align:center;border:1px solid;font-size:11px;width:20%;">'.$det->FECHA.'</td>
            <td style="text-align:center;border:1px solid;font-size:11px;width:30%;">'.$det->PROVEEDOR.'</td>
            <td style="text-align:center;border:1px solid;font-size:11px;">$'.$det->MTOTAL.'</td>
            <td style="text-align:center;border:1px solid;font-size:11px;">'.$det->CONDICIONES_PAGO.'</td>
            </tr>';
            $suma=(float)$suma+(float)$det->MTOTAL;
            $html.='</tbody>
            </table>
        </div>
            ';
            }
           $html.='
        <div id="prueba">
            <table style="border:1px solid;">
            <tbody>
            <tr>
            <td style="text-align:center;solid;font-size:11px;width:15%;background-color:#B4BDBD"></td>
            <td style="text-align:center;font-size:11px;width:20%;background-color:#B4BDBD"></td>
            <td style="text-align:center;font-size:11px;width:30%;background-color:#B4BDBD"><b>TOTAL EN DÓLARES</b></td>
            <td style="text-align:center;border:1px solid;font-size:11px;background-color:#B4BDBD"><b>$'.number_format($suma,2).'</b></td>
            <td style="text-align:center;font-size:11px;background-color:#B4BDBD"></td>
            </tr>';
            $html.='</tbody>
            </table>
        </div>
            ';
        }


$html.='
<div id="foot">
<table>
    <tr>
    <td style="text-align:center;width:10%;"></td>
    <td style="text-align:center;">______________</td>
    <td style="text-align:center;">______________</td>
    </tr>
    <tr>
    <td style="text-align:center;"></td>
    <td style="text-align:center;">Firma</td>
    <td style="text-align:center;">Sello</td>
    </tr>
</table>
</div>
</body></html>
';          
            $this->load->library('pdf');
            $nombre_archivo = "REPORTE ORDEN COMPRA ".$detalle[0]->ID_ORDEN_COMPRA." ".$detalle[0]->FECHA."";
            $this->pdf->loadHtml($html);
            $this->pdf->setPaper('legal','landscape');
            $this->pdf->set_option('defaultMediaType', 'all');
            $this->pdf->set_option('isFontSubsettingEnabled', true);            
            $this->pdf->render();
            $fontMetrics = $this->pdf->getFontMetrics();
            $canvas = $this->pdf->get_canvas();
            $font = $fontMetrics->getFont('Arial','bold');
            $canvas->page_text(845, 20, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 10, array(0, 0, 0));
            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
        
        }
    }
        
  }
}