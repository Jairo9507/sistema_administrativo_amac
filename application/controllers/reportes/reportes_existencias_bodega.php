<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_existencias_bodega extends Admin_Controller //el controlador debe llmarse igual que el archivo
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("reportes_existencias_bodegas_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Reportes","reportes/reportes_existencias_bodega");	//La ruta es la carpeta del controlador, la cual esta en controllers->catalogos/cat_anaqueles
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			/*Obtener lista de productos y cuentas */
			$unidad=$this->ion_auth->get_unidad_empleado();
            $this->data['productos'] = $this->reportes_existencias_bodegas_model->listarProductos($unidad);
            //$this->data['unidad'] = $this->reportes_existencias_bodegas_model->listarUnidades();
            

			$this->template->admin_render("admin/reportes/index_existencias",$this->data); //la ruta depende de la carpeta donde se este la vista OJO-> en este caso la vista esta en la carpeta View -> admin -> cat_anaque->index
		}
	}

	public function vistaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reporte de Existencias');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Reportes vista","reportes/reportes_existencias_bodega/vistaPDF");		
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			//$cuentas = $this->reportes_existencias_bodegas_model->listarCuentas();
			$fechaInicio='1';
			$fechaFinal='1';
			if (!empty($_POST['fechaInicio'])) {
				$fechaInicio=$_POST['fechaInicio'];
			}
			if (!empty($_POST['fechaFinal'])) {
				$fechaFinal=$_POST['fechaFinal'];
			}

			$datos= array(
				'producto'	=>$_POST['producto'],
				'tipo'		=>$_POST['tipo'],
				'fechaInicio' => $fechaInicio,
				'fechaFinal' => $fechaFinal
			);
			// var_dump($datos);exit;
			$this->data['datos']=$datos;
			// var_dump($datos);exit;

			$this->template->admin_render("admin/reportes/existencias_bodega_pdf",$this->data);	
		}
	}

	public function existenciaPDF($producto, $tipo,$fechaInicio,$fechaFinal)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
		
            $this->load->library('pdf');
            if(empty($producto)  && empty($tipo)){
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de proveedores de".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
           		exit;

            } else {
            	$detalle='';
            	$empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
            	$bodega =$this->reportes_existencias_bodegas_model->obtenerBodega($this->ion_auth->get_user_id());
            	
            	$jefatura=$this->reportes_existencias_bodegas_model->nombreJefatura($this->ion_auth->get_unidad_empleado());
            	
            	if ($empleado==null) {
            		$empleado='Admin';
            	}
            	if ($jefatura==null) {
            		$jefatura='Admin';
            	}
            	
            	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
 
				$fecha2 =  date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
				$fecha = date('d-m-Y H:i:s');
				if ((!empty($fechaInicio) && $fechaInicio != "1" ) && (!empty($fechaFinal) && $fechaFinal !="1" ) ) {
						$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
						$fecha1=date('d',strtotime($fechaInicio))." de ".$meses[date('n',strtotime($fechaInicio))-1]." de ".date('Y',strtotime($fechaInicio));
						$fecha2=date('d',strtotime($fechaFinal))." de ".$meses[date('n',strtotime($fechaFinal))-1]." de ".date('Y',strtotime($fechaFinal));
				}
				$html='
			            <!DOCTYPE HTML PUBLIC ">
			            <html>
			            <head>
			            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

			            <title>Reporte de existencias</title>
			            <style type="text/css">

			            @page {
			                margin: 1.3cm;
			            }


			        #header,
			        #footer {
			          position: fixed;
			          left: 0;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			          
			      }
			      #footer {
			          bottom: 0;
			          border-top: 0.1pt solid #aaa;
			      }
						#logo_clinica{
						    width:80%;
						    display: inline-block;
						    float:left;
						}

						#logo_sistema{
						    width:20%;
						    display: inline-block;
						    float:right;
						}
						#encabezado
						{
						    width:100%;
							padding-top:100px;
						    display:inline-block;
						    float:right;
						}
						body {
						   font-family: Sans-Serif;
						    margin: 0.5cm 0;
						    text-align: justify;
						}
						#tet{
						    border-radius:15px;
						    width: 13%;
						    padding: 3% 5%;
						    display: inline-block;
						    float:right;
						    text-align: center;
						    border:1px solid red;
						    color: red;
						}
						#prueba{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						#prueba2{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						 #foot{
						    width: 90%;
						    position:absolute;
						    text-align: center;
						    bottom:-5%;
						    padding:-5px;
						    }
						table{
						    clear:both;
						    text-align: left;
						    height:auto;
						    border-collapse:collapse; 
						    table-layout:fixed;
						    width: 100%;
						}
						#cuerpo{
						    position: abosolute;
						    text-align: center;
						    bottom=12%;
						}

						table td {
						    word-wrap:break-word;
						    padding:10px 0px 10px 0px;

						}

						.page-number {
						  text-align: center;
						}

						.page-number:before {
						  content: "Página" counter(page);
						}

						hr {
						  page-break-after: always;
						  border: 0;
						}

						</style>
						</head>';
						$rango='';

						if ((!empty($fechaInicio) && $fechaInicio != "1" ) && (!empty($fechaFinal) && $fechaFinal !="1" )) {
							$rango='DESDE: '.$fecha1.' HASTA: '.$fecha2;
						} else {
							$rango='al '.$fecha2.' ';
						}						
						$html.='
						<body>
							<div id="header">
							<div id="logo_clinica" > 
							<img src="img/MEMBRETE1-1.jpg" style="width:450; height=100;">
							</div>
							<div id="logo_sistema">
								<div style="font-size:12px;text-align:center;">Creado por: '.$empleado.'</div>
								<div style="font-size:12px;text-align:center;">Fecha:'.$fecha.' </div>
							</div>
							<div id="encabezado">

							    <h3 style="text-align:center;">Existencias  para  '.$bodega[0]->DESCRIPCION.'  '.$rango.' </h3>
							</div>
							<p></p>						
							</div>';
							// var_dump($condicion, $tipo, $producto);
							// exit;
							if ($tipo==1) {
								$totalunidades=0.0;
								// var_dump($condicion, $tipo);exit;
							$html.='<table  style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th style="width:15%;text-align:center;font-size:14px;">Cuenta Presupuestaria</th>
									<th style="width:15%;text-align:center;font-size:14px;">Código Contable</th>
									<th style="width:27%;text-align:left;font-size:14px;">Nombre Producto</th>
									<th style="width:15%;text-align:center;font-size:14px;">Cantidad</th>
									<th style="width:12%;text-align:center;font-size:14px;">Costo Unitario</th>
									<th style="width:15%;text-align:left;font-size:14px;">Costo Total</th>
								</tr>
								</thead>
								<tbody>';
								 	$detalle = $this->reportes_existencias_bodegas_model->obtenerReporteDetalle( $producto, $bodega[0]->ID_UNIDAD,0,$fechaInicio,$fechaFinal);
								 	// var_dump($detalle);exit;
								 	$sumacuenta=0.0;
								 	// $nombreCuenta = '';
								 	$sumaTotal=0.0;
								 	foreach ($detalle as $det) {
									$html.='
									<tr>
									<td style="text-align:center;font-size:11px;">'.$det->COD_CUENTA.'</td>
									<td style="text-align:center;font-size:11px;">'.$det->CODIGO.'</td>
									<td style="text-align:left;font-size:11px;">'.$det->NOMBRE_PRODUCTO.' '.$det->MEDICAMENTO.'</td>
									<td style="text-align:center;font-size:11px;">'.number_format($det->CANTIDAD).'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format($det->PRECIO,4).'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format((number_format((float)$det->PRECIO,4)*(float)$det->CANTIDAD),4).'</td>
									</tr>';
									$val=number_format((number_format((float)$det->PRECIO,4)*(float)$det->CANTIDAD),4);
									$val = str_replace(",",".",$val);
									$val = preg_replace('/\.(?=.*\.)/', '', $val);
									//var_dump(floatval($val));
									//var_dump($sumacuenta);	
									$sumacuenta=(float)$sumacuenta+(float)$val;


									// $nombreCuenta = $det->COD_CUENTA.' '.$det->NOMBRE_CUENTA;
									$totalunidades=(float)$totalunidades+(float)$det->CANTIDAD;
									}
									if ($sumacuenta!=0) {
									$sumaTotal=(float)$sumaTotal+(float)$sumacuenta;
								}

							$html.='
								<tr>
							<td colspan="3"  style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>EXISTENCIA TOTAL  </b></td>
							<td  style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>'.$totalunidades.'</b></td>
							<td colspan="3" style="text-align:center;background-color:#B4BDBD;border:1px solid;font-size:11px;"><b>$'.number_format($sumaTotal,4).'</b></td>
							</tr>';
							$html.='
							</tbody>
							</table>';
						}elseif ($tipo==2 ) {
							$cuentas = $this->reportes_existencias_bodegas_model->listarCuentas();
							$totalunidades=0.0;
							$html.='<table  style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th style="width:15%;text-align:center;font-size:14px;">Cuenta Presupuestaria</th>
									<th style="width:15%;text-align:center;font-size:14px;">Código Contable</th>
									<th style="width:27%;text-align:left;font-size:14px;">Nombre Producto</th>
									<th style="width:15%;text-align:center;font-size:14px;">Cantidad</th>
									<th style="width:12%;text-align:left;font-size:14px;">Costo Unitario</th>
									<th style="width:15%;text-align:left;font-size:14px;">Costo Total</th>
								</tr>
								</thead>
								<tbody>';
								$sumacuentaTotal=0.0;
								foreach ($cuentas as $cuen) {
								 	$detalle = $this->reportes_existencias_bodegas_model->obtenerReporteDetalle(0, $bodega[0]->ID_UNIDAD, $cuen->ID_CAT_CUENTA,$fechaInicio,$fechaFinal);
								 	$sumacuenta=0.0;
								 	$nombreCuenta = '';
								 	//var_dump($detalle);exit;
									$totalexistenciacantidad=0.0;
								 	foreach ($detalle as $det) {
									$html.='
									<tr>
									<td style="text-align:center;height: 3px;font-size:11px;">'.$det->COD_CUENTA.'</td>
									<td style="text-align:center;height: 3px;font-size:11px;">'.$det->CODIGO.'</td>
									<td style="text-align:left;height: 3px;font-size:11px;">'.$det->NOMBRE_PRODUCTO.' '.$det->MEDICAMENTO.'</td>
									<td style="text-align:center;height: 3px;font-size:11px;">'.number_format($det->CANTIDAD).'</td>
									<td style="text-align:left;height: 3px;font-size:11px;">$ '.number_format($det->PRECIO,4).'</td>
									<td style="text-align:left;height: 3px;font-size:11px;">$ '.number_format((number_format((float)$det->PRECIO,4)*(float)$det->CANTIDAD),4).'</td>
									</tr>
									';
									$val=number_format((number_format((float)$det->PRECIO,4)*(float)$det->CANTIDAD),4);
									$val = str_replace(",",".",$val);
									$val = preg_replace('/\.(?=.*\.)/', '', $val);
									//var_dump(floatval($val));
									//var_dump($sumacuenta);	
									$sumacuenta=(float)$sumacuenta+(float)$val;
									

									$totalexistenciacantidad=(float)$totalexistenciacantidad+(float)$det->CANTIDAD;
									$totalunidades=(float)$totalunidades+(float)$det->CANTIDAD;
									$nombreCuenta = $det->COD_CUENTA.'  '.$det->NOMBRE_CUENTA;
								}
								if ($sumacuenta!=0) {
									$sumacuentaTotal=(float)$sumacuentaTotal+(float)$sumacuenta;
								$html.='
								<tr style="border:1px solid;">
								
							<td colspan="3" style="text-align:center; background-color:#B4BDBD;font-size:11px;"><b>TOTAL EXISTENCIAS CUENTA '.$nombreCuenta.'</b></td>
							<td   style="text-align:center;  solid;background-color:#B4BDBD;font-size:11px;"><b>'.number_format($totalexistenciacantidad).'</b></td>	
							<td style="text-align:left;  solid;background-color:#B4BDBD;font-size:11px;"></td>
							<td colspan="2" style="text-align:left;background-color:#B4BDBD;font-size:11px;"><b>$ '.number_format($sumacuenta,4).'</b></td>
							</tr>
								';
								}

							}

							$html.='
								<tr>
							<td colspan="3"  style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>EXISTENCIAS TOTALES   </b></td>
							<td style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>'.$totalunidades.'</b></td>							
							<td colspan="3" style="text-align:center;background-color:#B4BDBD;border:1px solid;font-size:11px;"><b>$'.number_format($sumacuentaTotal,4).'</b></td>
							</tr>';
							$html.='
							</tbody>
							</table>';
						 }
					$html.='
					<div id="foot">
					<table>
					<tr>
					<td style="text-align:center;width:10%;"></td>
					<td style="text-align:center;width:85%;">_________________________</td>
					<td style="text-align:center;"></td>
					</tr>
					<tr>
					<td style="text-align:center;"></td>
					<td style="text-align:center;">Jefe: '.$jefatura.'</td>
					<td style="text-align:center;"></td>
					</tr>
					</table>
					</div>
					</body></html>
					';          
					 //var_dump($html);exit;					
		            $nombre_archivo = "Reporte de existencias ".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('Arial','bold');
				$canvas->page_text(480, 20, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 10, array(0, 0, 0));
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));            	
            	
            }			
		}
		
	}
}