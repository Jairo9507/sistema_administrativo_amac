<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_insumos_bodega extends Admin_Controller //el controlador debe llmarse igual que el archivo
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("reportes_insumos_bodega_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Reportes","reportes/reportes_insumos_bodega");	//La ruta es la carpeta del controlador, la cual esta en controllers->catalogos/cat_anaqueles
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reporte de Compras');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			/*Obtener lista de actividades */
			$unidad=$this->ion_auth->get_unidad_empleado();
			if (empty($unidad)) {
				$unidad=44;
			}
            $this->data['productos'] = $this->reportes_insumos_bodega_model->listarProductos($unidad);
            $this->data['unidad'] = $this->reportes_insumos_bodega_model->listarUnidades();
            $this->data['cuentas'] = $this->reportes_insumos_bodega_model->listarCuentas();

			$this->template->admin_render("admin/reportes/index_insumo",$this->data); //la ruta depende de la carpeta donde se este la vista OJO-> en este caso la vista esta en la carpeta View -> admin -> cat_anaque->index
		}
	}

	public function vistaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reporte de Compras');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Reportes vista","reportes/reportes_insumos_bodega/vistaPDF");		
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$datos= array(
				'fechaInicio'	=>$_POST['fechaInicio'],
				'fechaFinal'	=>$_POST['fechaFinal'],
				'producto'		=>$_POST['producto'],
				// 'unidad'		=>$_POST['unidad'],
				'condicion'		=>$_POST['condicion'],
				'tipo'		=>$_POST['tipo']

			);
			//var_dump($datos);
			$this->data['datos']=$datos;
			// var_dump($datos);exit;

			$this->template->admin_render("admin/reportes/insumo_bodegas_pdf",$this->data);	
		}
	}

	public function insumosPDF($fechaInicio, $fechaFinal, $producto, $condicion, $tipo)
	{
			if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
		
            $this->load->library('pdf');
            if(empty($fechaInicio) && empty($fechaFinal) && empty($producto) && empty($condicion) && empty($tipo)){
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de proveedores de".date('Y-m-d');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
           		exit;

            } else {
            	$fecha = date('d-m-Y H:i:s');
            	$detalle='';

            	// var_dump($fechaInicio, $fechaFinal, $producto, $condicion, $tipo); 
            	// exit; 

     //        	if ($condicion=='C' || $condicion==0 ) {

					// $detalle=$this->reportes_consumo_bodega_model->obtenerReporteConsolidado($fechaInicio, $fechaFinal, $producto, $cuenta, $unidad);  
					//  // var_dump($detalle);exit;
					          		
     //        	} else if($condicion=='D'){
     //        		$detalle = $this->reportes_consumo_bodega_model->obtenerReporteDetalle($fechaInicio, $fechaFinal, $producto, $cuenta, $unidad);
     //        		// var_dump($detalle);exit;
            	
     //        	} 
            	$unidad=$this->ion_auth->get_unidad_empleado();            	
            	$sumaunidad=0.0;
				$sumavalor=0.0;


            	$empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
            	
            	
            	$bodega =$this->reportes_insumos_bodega_model->obtenerBodega($this->ion_auth->get_user_id());
            	// var_dump($bodega);exit;
            	$jefatura=$this->reportes_insumos_bodega_model->nombreJefatura($this->ion_auth->get_unidad_empleado());
            	
            	if ($empleado==null) {
            		$empleado='Admin';
            	}
            	if ($jefatura==null) {
            		$jefatura='Admin';
            	}
            	if ($bodega==null) {
            		$bodega='Administración';
            	}

            	$reporten='';
            	if ($condicion=='D') {
            		$reporten='REPORTE DETALLADO DE COMPRAS DE '.$bodega=$bodega[0]->DESCRIPCION.'';
            		if ($tipo==1) {
            			$reporten.=' POR CUENTA PRESUPUESTARIA';
            		} else if($tipo==2){
            			$reporten.=' POR UNIDAD ADMINISTRATIVA';
            		}
            	} else if($condicion=='C'){
            		$reporten='REPORTE CONSOLIDADO DE COMPRAS DE '.$bodega=$bodega[0]->DESCRIPCION.'';
            		if ($tipo==1) {
            			$reporten.=' POR CUENTA PRESUPUESTARIA';
            		} else if($tipo==2){
            			$reporten.=' POR UNIDAD ADMINISTRATIVA';
            		}            		
            	}              	
						$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
						$fecha1=date('d',strtotime($fechaInicio))." de ".$meses[date('n',strtotime($fechaInicio))-1]." de ".date('Y',strtotime($fechaInicio));
						$fecha2=date('d',strtotime($fechaFinal))." de ".$meses[date('n',strtotime($fechaFinal))-1]." de ".date('Y',strtotime($fechaFinal));            	

				$html='
	            <!DOCTYPE HTML PUBLIC ">
	            <html>
	            <head>
	            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

	            <title>Reporte de insumos</title>
	            <style type="text/css">

	            @page {
	                margin: 1.3cm;
	            }


			        #header,
			        #footer {
			          position: fixed;
			          left: 0;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			          
			      }
			      #footer {
			          bottom: 0;
			          border-top: 0.1pt solid #aaa;
			      }
						#logo_clinica{
						    width:80%;
						    display: inline-block;
						    float:left;
						}

						#logo_sistema{
						    width:20%;
						    display: inline-block;
						    float:right;
						}
						#encabezado
						{
						    width:100%;
							padding-top:100px;
						    display:inline-block;
						    float:right;
						}
						body {
						   font-family: Sans-Serif;
						    margin: 0.5cm 0;
						    text-align: justify;
						}
						#tet{
						    border-radius:15px;
						    width: 13%;
						    padding: 3% 5%;
						    display: inline-block;
						    float:right;
						    text-align: center;
						    border:1px solid red;
						    color: red;
						}
						#prueba{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						#prueba2{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						 #foot{
						    width: 90%;
						    position:absolute;
						    text-align: center;
						    bottom:-3%;
						    padding:-20px;
						    }
						table{
						    clear:both;
						    text-align: left;
						    height:auto;
						    border-collapse:collapse; 
						    table-layout:fixed;
						    width: 100%;
						}
						#cuerpo{
						    position: abosolute;
						    text-align: center;
						    bottom=12%;
						}

						table td {
						    word-wrap:break-word;
						    padding:10px 0px 10px 0px;

						}

						.page-number {
						  text-align: center;
						}

						.page-number:before {
						  content: "Página" counter(page);
						}

						hr {
						  page-break-after: always;
						  border: 0;
						}

						</style>
						</head>';
						$html.='
						<body>
							<div id="header">
							<div id="logo_clinica" > 
							<img src="img/MEMBRETE1-1.jpg" style="width:450; height=100;">

							</div>
							<div id="logo_sistema">
								<div style="font-size:12px;text-align:center;">Creado por: '.$empleado.'</div>
								<div style="font-size:12px;text-align:center;">Fecha:'.$fecha.' </div>
							</div>
							<div id="encabezado">

							    <h3 style="text-align:center;">'.$reporten.'</h3>
							    <p style="text-align:center;position:relative;bottom:2px;">Rango: '.$fecha1.' al '.$fecha2.'</p>
							</div>
							<p></p>	
	
							</div>';
							$totalunidades=0.0;
							if ($condicion=='D' && $tipo==1 && $producto==0) {
								$unidad = $this->reportes_insumos_bodega_model->obtenerUnidadAdministrativa();
								
								// var_dump($condicion, $tipo);exit;
							$html.='<table  style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th style="width:5%;text-align:center;font-size:14px;">Fecha</th>
									<th style="width:10%;text-align:center;font-size:14px;">Cuenta Presupuestaria</th>
									<th style="width:6%;text-align:center;font-size:14px;">Código Contable</th>
									<th style="width:15%;text-align:justify;font-size:14px;">Nombre del Producto</th>
									<th style="width:5%;text-align:center;font-size:14px;">Cantidad</th>
									<th style="width:5%;text-align:center;font-size:14px;">Costo Unitario</th>
									<th style="width:5%;text-align:center;font-size:14px;">Costo Total</th>
								</tr>
								</thead>
								<tbody>';
								$sumaunidadTotal=0.0;
								 foreach ($unidad as $uni) {
								 	$detalle = $this->reportes_insumos_bodega_model->obtenerReporteDetalle($fechaInicio, $fechaFinal, $producto, 0,$uni->ID_UNIDAD);
								 	$sumaunidad=0.0;
								 	$nombreUnidad = '';
								 	$sumacantidadunidad=0.0;
								 	foreach ($detalle as $det) {
									$html.='
									<tr>
									<td style="text-align:center;font-size:11px;">'.date('d-m-Y',strtotime($det->FECHA)).'</td>
									<td style="text-align:center;font-size:11px;">'.$det->COD_CUENTA.'</td>
									<td style="text-align:center;font-size:11px;">'.$det->CODIGO.'</td>
									<td style="text-align:justify;font-size:11px;">'.$det->NOMBRE_PRODUCTO.' '.$det->MEDICAMENTO.'</td>
									<td style="text-align:center;font-size:11px;">'.number_format($det->CANTIDAD).'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format((float)$det->COSTO_UNITARIO,4).'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format(((float)$det->CANTIDAD*number_format((float)$det->COSTO_UNITARIO,4)),4).'</td>
									</tr>';
									$sumaunidad=(float)$sumaunidad+floatval(str_replace(",","",$det->COSTO_TOTAL));
									$totalunidades=(float)$totalunidades+(float)$det->CANTIDAD;
									$sumacantidadunidad=(float)$sumacantidadunidad+(float)$det->CANTIDAD;
									$nombreUnidad = $det->CODIGO_UNIDAD.' '.$det->DESCRIPCION;
									}
									if ($sumaunidad!=0) {
									$sumaunidadTotal=(float)$sumaunidadTotal+(float)$sumaunidad;
								$html.='
								<tr style="border:1px solid;">
							<td colspan="4" style="text-align:center; background-color:#B4BDBD;font-size:11px;"><b>TOTAL '.$nombreUnidad.'</b></td>
							<td style="text-align:center; background-color:#B4BDBD;font-size:11px;"><b>'.number_format($sumacantidadunidad).'</b></td>
							<td style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>'.number_format($totalunidades).'</b></td>							
							<td colspan="2" style="text-align:center;background-color:#B4BDBD;font-size:11px;"><b>$ '.number_format($sumaunidad,4).'</b></td>
							</tr>
								';
								}
									
							}

							$html.='
								<tr>
							<td colspan="5"  style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>INSUMOS TOTAL   </b></td>
							<td colspan="2" style="text-align:center;background-color:#B4BDBD;border:1px solid;font-size:11px;"><b>$'.number_format($sumaunidadTotal,4).'</b></td>
							</tr>';
							$html.='
							</tbody>
							</table>';
						}elseif ($condicion=='D'  && $tipo==2 && $producto==0) {
							$cuentas = $this->reportes_insumos_bodega_model->obtenerCuentas();
							$totalunidades=0.0;
							$html.='<table  style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th style="width:5%;text-align:center;font-size:14px;">Fecha</th>
									<th style="width:10%;text-align:center;font-size:14px;">Cuenta Presupuestaria</th>
									<th style="width:6%;text-align:center;font-size:14px;">Código Contable</th>
									<th style="width:15%;text-align:justify;font-size:14px;">Nombre del Producto</th>
									<th style="width:5%;text-align:center;font-size:14px;">Cantidad</th>
									<th style="width:5%;text-align:center;font-size:14px;">Costo Unitario</th>
									<th style="width:5%;text-align:center;font-size:14px;">Costo Total</th>
								</tr>
								</thead>
								<tbody>';
								$sumacuentaTotal=0.0;
								 $sumacantidadTotal=0;								
								foreach ($cuentas as $cuen) {
								 	$detalle = $this->reportes_insumos_bodega_model->obtenerReporteDetalle($fechaInicio, $fechaFinal, $producto, $cuen->id_cat_cuenta, $unidad);
								 	$sumacuenta=0.0;
								 	$nombreCuenta = '';
								 	//var_dump($detalle);exit;
									$sumacantidadcuenta=0.0;
									 	foreach ($detalle as $det) {
									$html.='
									<tr>
									<td style="text-align:center;height: 3px;font-size:11px;">'.date('d-m-Y',strtotime($det->FECHA)).'</td>
									<td style="text-align:center;height: 3px;font-size:11px;">'.$det->COD_CUENTA.'</td>
									<td style="text-align:center;height: 3px;font-size:11px;">'.$det->CODIGO.'</td>
									<td style="text-align:justify;height: 3px;font-size:11px;">'.$det->NOMBRE_PRODUCTO.' '.$det->MEDICAMENTO.'</td>
									<td style="text-align:center;font-size:11px;">'.number_format($det->CANTIDAD).'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format((float)$det->COSTO_UNITARIO,4).'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format(((float)$det->CANTIDAD*number_format((float)$det->COSTO_UNITARIO,4)),4).'</td>
									</tr>';
									$sumacuenta=(float)$sumacuenta+floatval(str_replace(",","",$det->COSTO_TOTAL));
									$sumacantidadcuenta=(float)$sumacantidadcuenta+(float)$det->CANTIDAD;
									$totalunidades=(float)$totalunidades+(float)$det->CANTIDAD;
									$nombreCuenta = $det->COD_CUENTA.'  '.$det->NOMBRE_CUENTA;
								}
								if ($sumacuenta!=0) {
									$sumacuentaTotal=(float)$sumacuentaTotal+(float)$sumacuenta;
								$html.='
								<tr style="border:1px solid;">
							<td colspan="4" style="text-align:center; background-color:#B4BDBD;font-size:11px;"><b>TOTAL '.$nombreCuenta.'</b></td>
							<td style="text-align:center; background-color:#B4BDBD;font-size:11px;"><b>'.number_format($sumacantidadcuenta).'</b></td>
							<td colspan="2" style="text-align:center;background-color:#B4BDBD;font-size:11px;"><b>$ '.number_format($sumacuenta,4).'</b></td>
							</tr>
								';
								$sumacantidadTotal=$sumacantidadTotal+$sumacantidadcuenta;
								}

							}

							$html.='
								<tr>
							<td colspan="4"  style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>COMPRAS TOTAL   </b></td>
							<td style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>'.number_format($sumacantidadTotal).'</b></td>
							<td colspan="2" style="text-align:center;background-color:#B4BDBD;border:1px solid;font-size:11px;"><b>$'.number_format($sumacuentaTotal,4).'</b></td>
							</tr>';
							$html.='
							</tbody>
							</table>';
						 }else if ($condicion=='D' && $tipo==0 && $producto!=0) {
						 	$totalunidades=0.0;
							$html.='<table  style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th style="width:5%;text-align:center;font-size:14px;">Fecha</th>
									<th style="width:10%;text-align:center;font-size:14px;">Cuenta Presupuestaria</th>
									<th style="width:6%;text-align:center;font-size:14px;">Código Contable</th>
									<th style="width:15%;text-align:justify;font-size:14px;">Nombre del Producto</th>
									<th style="width:5%;text-align:center;font-size:14px;">Cantidad</th>
									<th style="width:5%;text-align:center;font-size:14px;">Costo Unitario</th>
									<th style="width:5%;text-align:center;font-size:14px;">Costo Total</th>
								</tr>
								</thead>
								<tbody>';
								 	// var_dump($fechaInicio,$fechaFinal,$producto);
								 	$detalle = $this->reportes_insumos_bodega_model->obtenerReporteDetalle($fechaInicio, $fechaFinal,$producto,0,0);
								 	$sumaproductosTotal=0.0; 
								 	foreach ($detalle as $det) {
								 		// var_dump($detalle);exit;
									$html.='
									<tr>
									<td style="text-align:center;font-size:11px;">'.date('d-m-Y',strtotime($det->FECHA)).'</td>
									<td style="text-align:center;font-size:11px;">'.$det->COD_CUENTA.'</td>
									<td style="text-align:center;font-size:11px;">'.$det->CODIGO.'</td>
									<td style="text-align:justify;font-size:11px;">'.$det->NOMBRE_PRODUCTO.' '.$det->MEDICAMENTO.'</td>
									<td style="text-align:center;font-size:11px;">'.number_format($det->CANTIDAD).'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format((float)$det->COSTO_UNITARIO,4).'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format(((float)$det->CANTIDAD*number_format((float)$det->COSTO_UNITARIO,4)),4).'</td>
									</tr>';
									$totalunidades=(float)$totalunidades+(float)$det->CANTIDAD;
									$sumaproductosTotal=(float)$sumaproductosTotal+floatval(str_replace(",","",$det->COSTO_TOTAL));
								}

							$html.='
								<tr>
							<td colspan="4"  style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>COMPRAS TOTAL </b></td>
							<td  style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>'.number_format($totalunidades).'</b></td>
							<td colspan="2" style="text-align:center;background-color:#B4BDBD;border:1px solid;font-size:11px;"><b>$'.number_format($sumaproductosTotal,4).'</b></td>
							</tr>';
							
							$html.='
							</tbody>
							</table>';
							
						}else if($condicion=='C' && $tipo==1){
						
							$html.='<table  style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th style="text-align:center;font-size:14px;">Código Unidad</th>
									<th style="text-align:justify;font-size:14px;">Unidad Administrativa</th>
									<th style="text-align:center;font-size:14px;">Valor</th>
								</tr>
								</thead>
								<tbody>';
								 	// $producto=0;
								 	$detalle = $this->reportes_insumos_bodega_model->obtenerReporteConsolidado($fechaInicio, $fechaFinal, $producto, 0, 1);
								 	$sumaTotalUnidades=0.0;
								 	foreach ($detalle as $det) {
									$html.='
									<tr>
									<td style="text-align:center;font-size:11px;">'.$det->CODIGO_UNIDAD.'</td>
									<td style="text-align:justify;font-size:11px;">'.$det->DESCRIPCION.'</td>
									<td style="text-align:center;font-size:11px;">$ '.$det->VALOR.'</td>
									</tr>
									';
									$sumaTotalUnidades=(float)$sumaTotalUnidades+(float)$det->VALOR;
							}
							$html.='
								<tr>
							<td colspan="3"  style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>INSUMOS TOTAL DE UNIDADES EN DOLARES:  </b></td>
							<td style="text-align:center;background-color:#B4BDBD;border:1px solid;font-size:11px;"><b>$'.number_format($sumaTotalUnidades,4).'</b></td>
							</tr>';
							$html.='
							</tbody>
							</table>';
						 }elseif ($condicion=='C' && $tipo==2) {
							$html.='<table  style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th style="text-align:center;width:10%;font-size:14px;">Cuenta Prespuetaria</th>
									<th style="text-align:center;width:10%;font-size:14px;">Cuenta Contable</th>
									<th style="text-align:center;width:12%;font-size:14px;">Descripción de la Cuenta</th>
									<th style="text-align:center;width:12%;font-size:14px;">Valor</th>
								</tr>
								</thead>
								<tbody>';
								$producto=0;
								 $detalle = $this->reportes_insumos_bodega_model->obtenerReporteConsolidado($fechaInicio, $fechaFinal, $producto, 1, 0);
								 $sumaTotalCuentas=0.0;
								 foreach ($detalle as $det) {
									$html.='
									<tr>
									<td style="text-align:center;font-size:11px;">'.$det->COD_CUENTA.'</td>
									<td style="text-align:center;font-size:11px;">'.$det->CODIGO.'</td>									
									<td style="text-align:justify;font-size:11px;">'.$det->NOMBRE_CUENTA.'</td>
									<td style="text-align:center;font-size:11px;">$ '.$det->VALOR.'</td>
									</tr>
									';
									$sumaTotalCuentas=(float)$sumaTotalCuentas+(float)$det->VALOR;
								}
								$html.='
								<tr>
							<td colspan="3"  style="text-align:center;  border:1px solid;background-color:#B4BDBD;font-size:11px;"><b>INSUMOS TOTAL DE CUENTAS EN DOLARES:  </b></td>
							<td style="text-align:center;background-color:#B4BDBD;border:1px solid;font-size:11px;"><h3><b>$'.number_format($sumaTotalCuentas,4).'</b></h3></td>
							</tr>';
							$html.='
							</tbody>
							</table>';
						}
					$html.='
					<div id="foot">
					<table>
					<tr>
					<td style="text-align:center;width:10%;"></td>
					<td style="text-align:center;width:85%;">_________________________</td>
					<td style="text-align:center;"></td>
					</tr>
					<tr>
					<td style="text-align:center;"></td>
					<td style="text-align:center;">Jefe: '.$jefatura.'</td>
					<td style="text-align:center;"></td>
					</tr>
					</table>
					</div>
					</body></html>
					';          
					// var_dump($detalle);exit;					
		            $nombre_archivo = "Reporte de insumos ".date('Y-m-d');
	            $this->pdf->loadHtml($html);
	            // $this->pdf->setPaper('legal','landscape');
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('Arial','bold');
				$canvas->page_text(480, 20, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 10, array(0, 0, 0));
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));            	
            	
            }			
		}
		
	}
}