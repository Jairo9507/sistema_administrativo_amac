<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class kardex extends Admin_Controller //el controlador debe llmarse igual que el archivo
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("kardex_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Reportes","reportes/kardex");	//La ruta es la carpeta del controlador, la cual esta en controllers->reportes/kardex
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$unidad=$this->ion_auth->get_unidad_empleado();
			/*Obtener lista de actividades */

            $this->data['productos'] = $this->kardex_model->listarProductos($unidad);

			$this->template->admin_render("admin/reportes/index_kardex",$this->data); //la ruta depende de la carpeta donde se este la vista OJO-> en este caso la vista esta en la carpeta View -> admin -> cat_anaque->index
		}
	}


	public function vistaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Kardex');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Reportes vista","reportes/kardex/vistaPDF");		
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$datos= array(
				'fechaInicio'	=>$_POST['fechaInicio'],
				'fechaFinal'	=>$_POST['fechaFinal'],
				'tipo'			=>$_POST['tipo'],
				'producto'		=>$_POST['producto'],
			);
			
			 $this->data['datos']=$datos;
			// var_dump($datos);exit;

			$this->template->admin_render("admin/reportes/kardex_pdf",$this->data);	
		}
	}

	public function kardexPDF($fechaInicio, $fechaFinal, $tipo, $producto)
	{
			if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
		
            $this->load->library('pdf');
            if(empty($fechaInicio) && empty($fechaFinal) && empty($producto) && empty($condicion) && empty($tipo)){
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de proveedores de".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
           		exit;

            } else {
            	$fecha = date('d-m-Y H:i:s');
            	$detalle='';
            	$entradas=array();
            	// var_dump($fechaInicio, $fechaFinal, $producto,  $tipo); 
            	// exit; 

     //        	if ($condicion=='C' || $condicion==0 ) {

					// $detalle=$this->reportes_consumo_bodega_model->obtenerReporteConsolidado($fechaInicio, $fechaFinal, $producto, $cuenta, $unidad);  
					//  // var_dump($detalle);exit;
					          		
     //        	} else if($condicion=='D'){
     //        		$detalle = $this->reportes_consumo_bodega_model->obtenerReporteDetalle($fechaInicio, $fechaFinal, $producto, $cuenta, $unidad);
     //        		// var_dump($detalle);exit;
            	
     //        	} 
    //         	$sumaunidad=0.0;
				// $sumavalor=0.0;


            	$empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());

            	
            	$bodega =$this->kardex_model->obtenerBodega($this->ion_auth->get_user_id());
            	// var_dump($bodega);exit;
            	$jefatura=$this->kardex_model->nombreJefatura($this->ion_auth->get_unidad_empleado());
            	$total=0.0;
            	if ($empleado==null) {
            		$empleado='Admin';
            	}
            	if ($jefatura==null) {
            		$jefatura='Admin';
            	}
            	$bandera='';
            	if ($tipo==1) {
            		$bandera="Reporte ";
            	}
						$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
						$fecha1=date('d',strtotime($fechaInicio))." de ".$meses[date('n',strtotime($fechaInicio))-1]." de ".date('Y',strtotime($fechaInicio));
						$fecha2=date('d',strtotime($fechaFinal))." de ".$meses[date('n',strtotime($fechaFinal))-1]." de ".date('Y',strtotime($fechaFinal));            	

				$html='
			            <!DOCTYPE HTML PUBLIC ">
			            <html>
			            <head>
			            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

			            <title>Kardex</title>
			            <style type="text/css">

			            @page {
			                margin: 1cm;
			            }


			        #header,
			        #footer {
			          position: fixed;
			          left: 0;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			          
			      }
			      #footer {
					  position: fixed;
					  left: 0;
					  right: 0;
				      color: #aaa;
					  font-size: 0.9em;
					}
					#footer table {
						width: 100%;
						border-collapse: collapse;
						border: none;
					}
						#logo_clinica{
						    width:80%;
						    display: inline-block;
						    float:left;
						}

						#logo_sistema{
						    width:20%;
						    display: inline-block;
						    float:right;
						}
						#encabezado
						{
						    width:100%;
						    display:inline-block;
						    float:right;
						    padding-top:120px;
						}
						body {
						   font-family: Sans-Serif;
						     margin: 18pt 18pt 24pt 18pt;
						    text-align: justify;
						}
						#tet{
						    border-radius:15px;
						    width: 13%;
						    padding: 3% 5%;
						    display: inline-block;
						    float:right;
						    text-align: center;
						    border:1px solid red;
						    color: red;
						}
						#prueba{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						#prueba2{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						 #foot{
						    width: 90%;
						    position:absolute;
						    text-align: center;
						    bottom:1%;
						    padding:-20px;

						    }
						table{
						    clear:both;
						    text-align: left;
						    height:auto;
						    border-collapse:collapse; 
						    table-layout:fixed;
						    width: 100%;	
							word-wrap:break-word;						    
							page-break-inside:auto !important;						    			
						}
						table thead tr th{
							 page-break-inside:auto !important;
						}
						#cuerpo{
						    position: abosolute;
						    text-align: center;
						    bottom=12%;
						}

						table td th{
							word-wrap:break-word;
						    padding:5px 0px 10px 0px;
							page-break-inside:avoid !important;
						}



						.page-number {
						  text-align: center;
						}

						.page-number:before {
						  content: "Página" counter(page);
						}

						.page_break {
						  page-break-after: always;
						  
						}

						</style>
						</head>';
							if ($tipo==2) 
							{
							$html.='
							<body>
								<div id="header">
								<div id="logo_clinica" > 
								<img src="img/MEMBRETE1-1.jpg" style="width:800px; height:120px;">

								</div>
								<div id="logo_sistema" >
									<div style="font-size:12px;text-align:center;">CREADO POR: '.$empleado.'</div>
									<div style="font-size:12px;text-align:center;">FECHA:'.$fecha.' </div>
								</div>
								<div id="encabezado">

								    <h3 style="text-align:center; font-size:14px;">'.$bandera.'Kardex de '.$bodega=$bodega[0]->DESCRIPCION.'</h3>
								    <p style="text-align:center;font-size:14px;position:relative;bottom:2px;">Rango: '.$fecha1.' al '.$fecha2.'</p>
								</div>
								<p></p>	
		
								</div>

								';
							} else if($tipo==1)
							{
								$html.='
								<body>
									<div id="header">
									<div id="logo_clinica" > 
										<img src="img/MEMBRETE1-1.jpg" style="width:900px; height:120px;">

									</div>
									<div id="logo_sistema" >
										<div style="font-size:12px;text-align:center;">CREADO POR: '.$empleado.'</div>
										<div style="font-size:12px;text-align:center;">FECHA:'.$fecha.' </div>
									</div>
									<div id="encabezado">

									    <h3 style="text-align:center; font-size:14px;">'.$bandera.'Kardex de '.$bodega=$bodega[0]->DESCRIPCION.'</h3>
									    <p style="text-align:center;font-size:14px;position:relative;bottom:2px;">Rango: '.$fecha1.' al '.$fecha2.'</p>
									</div>
									<p></p>	
			
									</div>

									';
							}
							$unidad=$this->ion_auth->get_unidad_empleado();
							if ($tipo==1) {
							$productos = $this->kardex_model->listarProductos($unidad);
							foreach ($productos as $pro) {	
							$html.='
							<div >
							<table border="1" style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid page-break-inside:avoid !important;">
								<tr style="page-break-inside: avoid;">
									<th colspan="4" style="text-align:center;">Nombre Producto:  </th>
									<th colspan="9" style="text-align:center;">'.$pro->NOMBRE_PRODUCTO.' '.$pro->MEDICAMENTO.'</th>
								</tr>';
								$html.='
								<tr style="page-break-inside: avoid;">
								<th colspan="4" style="text-align:center;">Cuenta: </th>
								<th colspan="9" style="text-align:center;">'.$pro->cod_cuenta.'</th>
								</tr>
								<tr>
									<th colspan="4" style="text-align:center;">Código del Producto:</th>
									<th colspan="9" style="text-align:center;">'.$pro->COD_PRODUCTO.'</th>
								</tr>
								';
								$html.='<tr style="page-break-inside: avoid;">
									<th rowspan="2" style="text-align:center;width:12%;">Fecha</th>
									<th rowspan="2" style="text-align:center;width:18%;">Factura</th>									
									<th colspan="2" rowspan= "2" style="text-align:center;width:20%;">Descripción</th>
									<th colspan="3" style="text-align:center; width:30%;">Entradas</th>
									<th colspan="3" style="text-align:center;width:30%;">Salidas</th>
									<th colspan="3" style="text-align:center;width:30%;">Saldos</th>
								</tr>
								<tr>
									<th style="text-align:center;width:9%;font-size:13px;">Cantidad</th>
									<th style="text-align:center;width:9%;font-size:13px;">Precio Unitario</th>
									<th style="text-align:center;width:11%;font-size:13px;">Precio Total</th>
									<th style="text-align:center;width:9%;font-size:13px;">Cantidad</th>
									<th style="text-align:center;width:9%;font-size:13px;">Precio Unitario</th>
									<th style="text-align:center;width:9%;font-size:13px;">Precio Total</th>
									<th style="text-align:center;width:8%;font-size:13px;">Cantidad</th>
									<th style="text-align:center;width:9%;font-size:13px;">Precio Unitario</th>
									<th style="text-align:center;width:13%;font-size:13px;">Precio Total</th>									
								</tr>';
								$existencias = $this->kardex_model->obtenerExistencias($fechaInicio,$fechaFinal,$pro->ID_PRODUCTO);

							
							$suma1=0.0;
								$html.='</thead>
								';
								$detalle = $this->kardex_model->obtenerKardex($fechaInicio, $fechaFinal, $pro->ID_PRODUCTO);
									 $suma = 0.0;
									$suma = (float)$suma + (float)$total;
									$tentradas=0.0;
									$tsalidas=0.0;
									$centradas=0.0;
									$csalidas=0.0;
									$ctentradas=0.0;
									$ctsalida=0.0;	
									$idmovimiento='';
									$sumas=0.0;								
									$anterior='';
									$compra='';
									$idsalidas='';
								$html.='<tbody>';
								if ($existencias) {
									foreach ($existencias as $exi) {
										//$suma1 = (float)$suma1+(float)$exi->CANTIDAD;
									$html.='
									<tr>
											<td style="text-align:center;font-size:11px;width:10%;"></td>
											<td style="text-align:center;font-size:11px;width:11%;"></td>
											<td colspan="2" style="text-align:center;font-size:11px;width:22%;">Saldo Inicial...</td>
											<td style="text-align:center;font-size:11px;width:11%;"></td>
											<td style="text-align:center;font-size:11px;width:11%;"></td>
											<td style="text-align:center;font-size:11px;width:10%;"></td>
											<td style="text-align:center;font-size:11px;width:11%;"></td>
											<td style="text-align:center;font-size:11px;width:11%;"></td>
											<td style="text-align:center;font-size:11px;width:11%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;">'.$exi->CANTIDAD.'</td>
											<td style="text-align:center;font-size:11px;width:12%;">$'.number_format((float)$exi->PRECIO,4).'</td>									
											<td  style="text-align:center;font-size:11px;width:12%;"><b>$'.number_format(((float)$exi->CANTIDAD*(float)$exi->PRECIO),4).'</b></td>
										</tr>
									';

									$total = (float)$suma1;
								}
								} else {

									$html.='
									<tr>
											<td style="text-align:center;font-size:11px;width:11%;"></td>
											<td style="text-align:center;font-size:11px;width:11%;"></td>											
											<td colspan="2" style="text-align:center;font-size:11px;width:25%;">Saldo Inicial...</td>
											<td style="text-align:center;font-size:11px;width:12%;">0</td>

											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:11%;"></td>											
											<td style="text-align:center;font-size:11px;width:11%;"></td>
											<td style="text-align:center;font-size:11px;width:11%;"><b></b></td>
											<td style="text-align:center;font-size:11px;width:12%;">0</td>											
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>																						
										</tr>
									';
								}								
								foreach ($detalle as $det) {									
									$suma = (float)$suma + (float)$det->CANTIDAD_ENTRADA-(float)$det->CANTIDAD_SALIDA;
										$tentradas=	(float)$tentradas + (float)$det->CANTIDAD_ENTRADA;
										$tsalidas= (float)$tsalidas + (float)$det->CANTIDAD_SALIDA;
										$centradas=(float)$centradas+((float)$det->COSTO_ENTRADA*(float)$det->CANTIDAD_ENTRADA);
										$csalidas=(float)$csalidas+((float)$det->COSTO_SALIDA*(float)$det->CANTIDAD_SALIDA);


							
											if (empty($compra) && $det->ID_ORDEN_COMPRA!='0') {
												$compra.=$det->ID_ORDEN_COMPRA;
											} else if($det->ID_ORDEN_COMPRA!='0') {
												$compra.=','.$det->ID_ORDEN_COMPRA;
											}



											if (empty($idsalidas) ) {
												$idsalidas.=$det->ID_MOVIMIENTO;
											} else if(!empty($idsalidas))  {
												$idsalidas.=','.$det->ID_MOVIMIENTO;
												//$anterior=$this->kardex_model->entradasAnteriores($pro->ID_PRODUCTO,$fechaInicio,$fechaFinal,$det->FECHA_MOVIMIENTO,$idsalidas);
											}

											$html.='
												
												<tr>';

											if ((float)$det->CANTIDAD_ENTRADA>0) {					
												$anterior=$this->kardex_model->entradasAnteriores($pro->ID_PRODUCTO,$fechaInicio,$fechaFinal,$det->FECHA_MOVIMIENTO,$idsalidas,$compra);

												$html.='
													<td style="text-align:center;font-size:11px;width:12%;">'.date('d-m-Y',strtotime($det->FECHA_MOVIMIENTO)).'</td>
													<td style="text-align:center;font-size:11px;width:18%;">'.$det->MOVIMIENTO.' '.$det->CORRELATIVO.'</td>
													<td colspan="2" style="text-align:center;font-size:11px;width:15%;">'.$det->NOMBRE_PROVEEDOR.' '.$det->UNIDAD_SOLICITA.'</td>
													<td style="text-align:center;font-size:11px;width:11%;">'.number_format($det->CANTIDAD_ENTRADA).'</td>
													<td style="text-align:center;font-size:11px;width:12%;">$'.number_format((float)$det->COSTO_ENTRADA,4).'</td>
													<td style="text-align:center;font-size:11px;width:12%;">$'.number_format(((float)$det->CANTIDAD_ENTRADA*number_format((float)$det->COSTO_ENTRADA,4)),4).'</td>
													<td style="text-align:center;font-size:11px;wdth:11%;"></td>
													<td style="text-align:center;font-size:11px;width:11%;"></td>
													<td style="text-align:center;font-size:11px;width:11%;"></td>
												<td style="text-align:center;font-size:11px;width:11%;">';
												foreach ($anterior as $a) {
														$html.=' '.number_format($a->CANTIDAD).'<br/>';
												}
												$html.='</td>';
												$html.='<td style="text-align:center;font-size:11px;width:12%;">';
												foreach ($anterior as $a) {
													$html.=' $'.number_format((float)$a->PRECIO,4).'<br/>';
												}
												$html.='</td>';
												$html.='<td style="text-align:center;font-size:11px;width:12%;">';
												foreach ($anterior as $a) {

													$html.=' $'.number_format(((float)number_format($a->CANTIDAD)*number_format((float)$a->PRECIO,4)),4).'<br/>';
												}

												$html.='</td>';
											} else if((float)$det->CANTIDAD_SALIDA>0) {
											//var_dump($idmovimiento);
													
												if ($idmovimiento=='') {
													$idmovimiento.=$det->ID_MOVIMIENTO;
												} else {
													$idmovimiento.=','.$det->ID_MOVIMIENTO;
												}

												$entradas=$this->kardex_model->ObtenerPrimeraExistencia($fechaInicio,$pro->ID_PRODUCTO,$fechaFinal,$det->FECHA_MOVIMIENTO,$idmovimiento);
												
												$html.='
													<td style="text-align:center;font-size:11px;width:11%;">'.date("d-m-Y",strtotime($det->FECHA_MOVIMIENTO)).'</td>
													<td style="text-align:center;font-size:11px;width:18%;">'.$det->MOVIMIENTO.' '.$det->CORRELATIVO.'</td>
													<td colspan="2" style="text-align:center;font-size:11px;width:20%;">'.$det->NOMBRE_PROVEEDOR.' '.$det->UNIDAD_SOLICITA.'</td>
													<td style="text-align:center;font-size:11px;width:11%;"></td>
													<td style="text-align:center;font-size:11px;width:11%;"></td>
													<td style="text-align:center;font-size:11px;width:11%;"></td>
													<td style="text-align:center;font-size:11px;width:11%;">'.number_format($det->CANTIDAD_SALIDA).'</td>
													<td style="text-align:center;font-size:11px;width:11%;">$'.number_format((float)$det->COSTO_SALIDA,4).'</td>
													<td style="text-align:center;font-size:11px;width:11%;">$'.number_format((((float)$det->CANTIDAD_SALIDA*number_format((float)$det->COSTO_SALIDA,4))),4).'</td>';
												$html.='<td  style="text-align:center;font-size:11px;width:11%;">';
												//$sumas=(float)$sumas+(float)$det->CANTIDAD_SALIDA;
													for ($i=0; $i <count($entradas) ; $i++) {
														if ($i>0) {
														 	$idanterior=$entradas[$i-1]->ID_HISTORICO_INVENTARIO;
														 } else {
														 	$idanterior=$entradas[$i]->ID_HISTORICO_INVENTARIO;
														 }

														if (number_format((float)$entradas[$i]->PRECIO,2)==number_format((float)$det->COSTO_SALIDA,2) && $idanterior==$entradas[$i]->ID_HISTORICO_INVENTARIO) {
															$html.=''.number_format(((float)$entradas[$i]->CANTIDAD-(float)$entradas[$i]->SALIDAS)).'<br/>';	
														} else{
															$html.=''.number_format((float)$entradas[$i]->CANTIDAD).'<br/>';														
														}
													}									
												/*foreach ($entradas as $e) {
													//var_dump($e->PRECIO);

													if ($e->PRECIO==$det->COSTO_SALIDA && $idprueba==$e->ID_HISTORICO_INVENTARIO) {
														$html.=''.(float)$e->CANTIDAD-(float)$e->SALIDAS.'<br/>';	
													} else {
														$html.=''.(float)$e->CANTIDAD.'<br/>';
													}
	

												}		*/
												$html.='</td>';
												$html.='<td  style="text-align:center;font-size:11px;width:11%;">';

												foreach ($entradas as $e) {
													$html.='$'.number_format((float)$e->PRECIO,4).'<br/>';
												}
												$html.='</td>';
												$html.='
												<td  style="text-align:center;font-size:11px;width:11%;">';
												foreach ($entradas as $e) {
													$html.='$'.number_format(((float)$e->CANTIDAD-(float)$e->SALIDAS)*number_format((float)$e->PRECIO,4),4).'<br/>';
												}																	
													/*foreach ($entradas as $e) {
														if ($e->PRECIO==$det->COSTO_SALIDA) {
															$html.='$'.number_format((((float)$e->CANTIDAD-(float)$e->SALIDAS)*(float)$e->PRECIO),2).'<br/>';
														} else {
															$html.='$'.number_format((((float)$e->CANTIDAD)*(float)$e->PRECIO),2).'<br/>';
														}														

													}*/

												$html.='</td>';											
											}

												$html.='</tr>
												
											';

								} 
								$html.='<tr>

									<td colspan="4" >TOTALES</td>
									<td style="text-align:center;font-size:11px;width:13%;">'.number_format($tentradas).'</td>
									<td></td>
									<td style="text-align:center;font-size:11px;width:13%;">$'.number_format($centradas,4).'</td>
									<td style="text-align:center;font-size:11px;width:13%;">'.$tsalidas.'</td>
									<td></td>
									<td style="text-align:center;font-size:11px;width:13%;">$'.number_format($csalidas,4).'</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>';

								$saldos=$this->kardex_model->saldosActuales($fechaFinal,$pro->ID_PRODUCTO);
								/*foreach ($saldos as $s) {
								$html.='
								<tr >
								<td colspan="4">Existencia No. '.$s->CODIGO_ORDEN_COMPRA.'</td>
								<td></td>
								<td></td>
								<td></td>	
								<td></td>
								<td></td>
								<td></td>
									<td style="text-align:center;font-size:11px;">'.$s->CANTIDAD.'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format((float)$s->PRECIO,2).'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format(((float)$s->CANTIDAD*(float)$s->PRECIO),2).' </td>
								</tr>';

								}	*/								
							$html.='</tbody></table></div>';
							
						}
									//var_dump($html);exit;
					}else if ($tipo==2) {
						 	$existencias = $this->kardex_model->obtenerExistencias($fechaInicio,$fechaFinal,$producto);
						 	if (empty($existencias)) {
						 		$pro=$this->kardex_model->obtenerProductoId($producto);
						 	$html.='<table border="1" style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th colspan="4" style="text-align:center;">Nombre Producto:  </th>
									<th colspan="9" style="text-align:center;">'.$pro[0]->NOMBRE_PRODUCTO.' '.$pro[0]->MEDICAMENTO.'</th>
								</tr>';
								$html.='
								<tr>
								<th colspan="4" style="text-align:center;">Cuenta: </th>
								<th colspan="9" style="text-align:center;">'.$pro[0]->COD_CUENTA.'</th>
								</tr>
								<tr>
									<th colspan="4" style="text-align:center;">Código del Producto:</th>
									<th colspan="9" style="text-align:center;">'.$pro[0]->COD_PRODUCTO.'</th>
								</tr>
								';
						 	} else {
						 	$html.='<table border="1" style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th colspan="4" style="text-align:center;">Nombre Producto:  </th>
									<th colspan="9" style="text-align:center;">'.$existencias[0]->NOMBRE_PRODUCTO.'</th>
								</tr>';
								$html.='
								<tr>
								<th colspan="4" style="text-align:center;">Cuenta: </th>
								<th colspan="9" style="text-align:center;">'.$existencias[0]->COD_CUENTA.'</th>
								</tr>
								<tr>
									<th colspan="4" style="text-align:center;">Código del Producto:</th>
									<th colspan="9" style="text-align:center;">'.$existencias[0]->COD_PRODUCTO.'</th>
								</tr>
								';						 		
						 	}

						 		
								$html.='<tr>
									<th rowspan="2" style="text-align:center;width:12%;">Fecha</th>
									<th rowspan="2" style="text-align:center;width:15%;">Factura</th>									
									<th colspan="2" rowspan= "2" style="text-align:center;width:15%;">Descripción</th>
									<th colspan="3" style="text-align:center;">Entradas</th>
									<th colspan="3" style="text-align:center;">Salidas</th>
									<th colspan="3" style="text-align:center;">Saldos</th>
								</tr>
								<tr>
									<th style="text-align:center;width:10%;font-size:13px;">Cantidad</th>
									<th style="text-align:center;width:10%;font-size:13px;">Precio Unitario</th>
									<th  style="text-align:center;width:10%;font-size:13px;">Precio Total</th>
									<th style="text-align:center;width:10%;font-size:13px;">Cantidad</th>
									<th style="text-align:center;width:10%;font-size:13px;">Precio Unitario</th>
									<th  style="text-align:center;width:10%;font-size:13px;">Precio Total</th>
									<th style="text-align:center;width:10%;font-size:13px;">Cantidad</th>
									<th style="text-align:center;width:10%;font-size:13px;">Precio Unitario</th>
									<th  style="text-align:center;width:10%;font-size:13px;">Precio Total</th>	
								</tr>
								</thead>
								<tbody>';
									$total=0.0;
									$suma2=0.0;


									if ($existencias) {
										foreach ($existencias as $exi) {
											//$suma2=(float)$suma2+(float)$exi->CANTIDAD;

										$html.='
										<tr>
										<td style="text-align:center;font-size:11px;width:12%;"></td>
										<td style="text-align:center;font-size:11px;width:15%;"></td>
											<td colspan="2" style="text-align:center;font-size:11px;width:15%;">Saldo Inicial...</td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;">'.number_format((int)$exi->CANTIDAD).'</td>
											<td style="text-align:center;font-size:11px;width:12%;">$'.number_format((float)$exi->PRECIO,4).'</td>
											<td  style="text-align:center;font-size:11px;width:12%;"><b>$'.number_format((float)$exi->CANTIDAD*(float)$exi->PRECIO,4).'</b></td>
										
										</tr>';
										$total = (float)$suma2;
										}
									} else {
										$html.='
										<tr>
										<td style="text-align:center;font-size:11px;width:12%;"></td>
										<td style="text-align:center;font-size:11px;width:15%;"></td>										
											<td colspan="2" style="text-align:center;font-size:11px;width:15%;">Saldo Inicial...</td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;"></td>
											<td style="text-align:center;font-size:11px;width:12%;">0</td>
											<td  style="text-align:center;font-size:11px;width:12%;"><b></b></td>
											<td  style="text-align:center;font-size:11px;width:12%;"><b></b></td>						
										</tr>';										
									}

								$detalle = $this->kardex_model->obtenerKardex($fechaInicio, $fechaFinal, $producto);
								$suma = 0.0;
								$suma = (float)$suma + (float)$total;
								$tentradas=0.0;
								$tsalidas=0.0;
								$centradas=0.0;
								$csalidas=0.0;
								$ctentradas=0.0;
								$ctsalida=0.0;	
								$idmovimiento='';
								$idsalidas='';
								$anterior='';
								$compra='';
								$sumas=0.0;
								foreach ($detalle as $det) {									
									$suma = (float)$suma + (float)$det->CANTIDAD_ENTRADA-(float)$det->CANTIDAD_SALIDA;
										$tentradas=	(float)$tentradas + (float)$det->CANTIDAD_ENTRADA;
										$tsalidas= (float)$tsalidas + (float)$det->CANTIDAD_SALIDA;
										$centradas=(float)$centradas+((float)$det->COSTO_ENTRADA*(float)$det->CANTIDAD_ENTRADA);
										$csalidas=(float)$csalidas+((float)$det->COSTO_SALIDA*(float)$det->CANTIDAD_SALIDA);
							
											if (empty($compra) && $det->ID_ORDEN_COMPRA!='0') {
												$compra.=$det->ID_ORDEN_COMPRA;
											} else if($det->ID_ORDEN_COMPRA!='0') {
												$compra.=','.$det->ID_ORDEN_COMPRA;
											}

											if (empty($idsalidas) ) {
												$idsalidas.=$det->ID_MOVIMIENTO;
											} else if(!empty($idsalidas))  {
												$idsalidas.=','.$det->ID_MOVIMIENTO;
												//$anterior=$this->kardex_model->entradasAnteriores($pro->ID_PRODUCTO,$fechaInicio,$fechaFinal,$det->FECHA_MOVIMIENTO,$idsalidas);
											}
											//var_dump(!empty($idsalidas));
											//var_dump($idsalidas);
											$html.='
												<tbody>
												<tr>';

											if ((float)$det->CANTIDAD_ENTRADA>0) {	
												
												$anterior=$this->kardex_model->entradasAnteriores($producto,$fechaInicio,$fechaFinal,$det->FECHA_MOVIMIENTO,$idsalidas,$compra);
												$html.='
													<td style="text-align:center;font-size:11px;width:12%;">'.date('d-m-Y',strtotime($det->FECHA_MOVIMIENTO)).'</td>
													<td style="text-align:center;font-size:11px;width:15%;">'.$det->MOVIMIENTO.' '.$det->CORRELATIVO.'</td>
													<td colspan="2" style="text-align:center;font-size:11px;width:15%;">'.$det->NOMBRE_PROVEEDOR.' '.$det->UNIDAD_SOLICITA.'</td>
													<td style="text-align:center;font-size:11px;width:12%;">'.number_format($det->CANTIDAD_ENTRADA).'</td>
													<td style="text-align:center;font-size:11px;width:12%;">$'.number_format((float)$det->COSTO_ENTRADA,4).'</td>
													<td style="text-align:left;font-size:11px;width:12%;">$'.number_format(((float)$det->CANTIDAD_ENTRADA*number_format((float)$det->COSTO_ENTRADA,4)),4).'</td>
													<td style="text-align:center;font-size:11px;wdth:12%;"></td>
													<td style="text-align:center;font-size:11px;width:12%;"></td>
													<td style="text-align:center;font-size:11px;width:12%;"></td>
												<td style="text-align:center;font-size:11px;width:12%;">';
												foreach ($anterior as $a) {

														$html.=' '.number_format($a->CANTIDAD).'<br/>';

												}
												$html.='</td>';
												//$det->CANTIDAD_ENTRADA
												$html.='<td style="text-align:center;font-size:11px;width:12%;">';
												foreach ($anterior as $a) {
													$html.=' $'.number_format((float)$a->PRECIO,4).'<br/>';
												}

												$html.='</td>';
												$html.='<td style="text-align:left;font-size:11px;width:12%;">';
												foreach ($anterior as $a) {

													$html.=' $'.number_format(((float)number_format($a->CANTIDAD)*number_format((float)$a->PRECIO,4)),4).'<br/>';
												}

												$html.='</td>';
											} else if((float)$det->CANTIDAD_SALIDA>0) {
											//var_dump($idmovimiento);

												if ($idmovimiento=='') {
													$idmovimiento.=$det->ID_MOVIMIENTO;
												} else {
													$idmovimiento.=','.$det->ID_MOVIMIENTO;
												}
												$entradas=$this->kardex_model->ObtenerPrimeraExistencia($fechaInicio,$producto,$fechaFinal,$det->FECHA_MOVIMIENTO,$idmovimiento);
												$html.='
													<td style="text-align:center;font-size:11px;width:11%;">'.date('d-m-Y',strtotime($det->FECHA_MOVIMIENTO)).'</td>
													<td style="text-align:center;font-size:11px;width:18%;">'.$det->MOVIMIENTO.' '.$det->CORRELATIVO.'</td>
													<td colspan="2" style="text-align:center;font-size:11px;width:20%;">'.$det->NOMBRE_PROVEEDOR.' '.$det->UNIDAD_SOLICITA.'</td>
													<td style="text-align:center;font-size:11px;width:11%;"></td>
													<td style="text-align:center;font-size:11px;width:11%;"></td>
													<td style="text-align:center;font-size:11px;width:11%;"></td>
													<td style="text-align:center;font-size:11px;width:11%;">'.number_format($det->CANTIDAD_SALIDA).'</td>
													<td style="text-align:center;font-size:11px;width:11%;">$'.number_format((float)$det->COSTO_SALIDA,4).'</td>
													<td style="text-align:left;font-size:11px;width:11%;">$'.number_format(((float)$det->CANTIDAD_SALIDA*(number_format((float)$det->COSTO_SALIDA,4))),4).'</td>';
												$html.='<td  style="text-align:center;font-size:11px;width:11%;">';
													for ($i=0; $i <count($entradas) ; $i++) {
														if ($i>0) {
														 	$idanterior=$entradas[$i-1]->ID_HISTORICO_INVENTARIO;
														 } else {
														 	$idanterior=$entradas[$i]->ID_HISTORICO_INVENTARIO;
														 }
														 
														if (number_format((float)$entradas[$i]->PRECIO,4)==number_format((float)$det->COSTO_SALIDA,4) && $det->ID_HISTORICO_PERTENECE==$entradas[$i]->ID_HISTORICO_INVENTARIO) {
															$html.=''.number_format((float)$entradas[$i]->CANTIDAD-(float)$entradas[$i]->SALIDAS).'<br/>';	
														} else{
															$html.=''.number_format((float)$entradas[$i]->CANTIDAD).'<br/>';														
														}
													}	//exit;											

												$html.='</td>';
												$html.='<td  style="text-align:center;font-size:11px;width:12%;">';
												foreach ($entradas as $e) {
													$html.='$'.number_format((float)$e->PRECIO,4).'<br/>';
												}
												$html.='</td>';
												$html.='
												<td  style="text-align:center;font-size:11px;width:12%;">';
												foreach ($entradas as $e) {
													$html.='$'.number_format(((float)$e->CANTIDAD-(float)$e->SALIDAS)*number_format((float)$e->PRECIO,4),4).'<br/>';
												}												
													/*for ($i=0; $i <count($entradas) ; $i++) {
														if ($i>0) {
														 	$idanterior=$entradas[$i-1]->ID_HISTORICO_INVENTARIO;
														 } else {
														 	$idanterior=$entradas[$i]->ID_HISTORICO_INVENTARIO;
														 }

														if (number_format((float)$entradas[$i]->PRECIO,2)==number_format((float)$det->COSTO_SALIDA,2) && $idanterior==$entradas[$i]->ID_HISTORICO_INVENTARIO) {
															$html.=''.(float)$entradas[$i]->CANTIDAD-(float)$entradas[$i]->SALIDAS.'<br/>';	
														} else{
															$html.=''.(float)$entradas[$i]->CANTIDAD.'<br/>';														
														}
													}	*/		
												$html.='</td>';	
											}

												$html.='</tr>
												
											';


								} 

								$html.='<tr>		

									<td colspan="4">TOTALES</td>
									<td style="text-align:center;font-size:11px;width:13%;">'.number_format($tentradas).'</td>
									<td></td>
									<td style="text-align:center;font-size:11px;width:13%;">$'.number_format($centradas,4).'</td>
									<td style="text-align:center;font-size:11px;width:13%;">'.$tsalidas.'</td>
									<td></td>
									<td style="text-align:center;font-size:11px;width:13%;">$'.number_format($csalidas,4).'</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>';
								$saldos=$this->kardex_model->saldosActuales($fechaFinal,$producto);

							/*	foreach ($saldos as $s) {
								$html.='
								<tr>
								<td colspan="4">Existencia No. '.$s->CODIGO_ORDEN_COMPRA.'</td>						
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
									<td style="text-align:center;font-size:11px;">'.$s->CANTIDAD.'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format((float)$s->PRECIO,2).'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format(((float)$s->CANTIDAD*(float)$s->PRECIO),2).' </td>
								</tr>';

								}	*/							
							$html.='
							</tbody>
							</table>';

						 }

					$html.='
					<div id="foot" >
					<table>
					<tr>
					<td style="text-align:center;width:10%;"></td>
					<td style="text-align:center;width:85%;">____________________</td>
					<td style="text-align:center;"></td>
					</tr>
					<tr>
					<td style="text-align:center;"></td>
					<td style="text-align:center;font-size:11px;">JEFE: '.$jefatura.'</td>
					<td style="text-align:center;"></td>
					</tr>
					</table>
					</div>
					</body>
					</html>
					';         

		            $nombre_archivo = "Kardex".date('d-m-Y');
	            	$paper_size = array(0,0,1248,816);		            
				$this->pdf->loadHtml($html);
				$this->pdf->setPaper($paper_size,'landscape');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('MuseoSans','bold');
				$canvas->page_text(680, 20, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 10, array(0, 0, 0));					
				
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));            	
            	
            }			
		}
		
	}
	
}