<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 
 */
class reportes_inventario_documental extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("reportes_inventario_documental_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Reportes","reportes/reportes_inventario_documental");	
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes de Inventario documental');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['unidades']=$this->reportes_inventario_documental_model->listarUnidades();

			/*Obtener lista de actividades */
			$this->template->admin_render("admin/reportes/index_documentos",$this->data);			
		}
		
	}

	public function vistaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes de Inventario documental');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Reportes vista","reportes/reportes_inventario_documental/vistaPDF");		
		
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			if (isset($_POST['fechaInicio'])==false or $_POST['fechaInicio']=='') {
				$_POST['fechaInicio']='YYYY-mm-dd';
			} 
			if(isset($_POST['fechaFinal'])==false or $_POST['fechaFinal']==''){
				$_POST['fechaFinal']='YYYY-mm-dd';
			}
			if (isset($_POST['unidad_id'])==false or $_POST['unidad_id']=='') {
				$_POST['unidad_id']="0";
			}
			$fechaInicio=$_POST['fechaInicio'];
			$fechaFinal=$_POST['fechaFinal'];
			$this->data['datos']= array(
				'fechaInicio'=>$fechaInicio,
				'fechaFinal'=>$fechaFinal,
				'estado'=>$_POST['estado'],
				'unidad_id'=>$_POST['unidad_id']
			);

			/*$this->data['documentos']= $this->reportes_inventario_documental_model->listarProximosavencer($fechaInicio,$fechaFinal);
			if (isset($_POST['estado']) && $_POST['estado']=='1') {
				$this->load->view("admin/reportes/tabla_retencion_documental",$this->data);
			} else if(isset($_POST['estado']) && $_POST['estado']=='2'){
				$this->load->view("admin/reportes/tabla_valoracion_documental",$this->data);
			}*/
			$this->template->admin_render("admin/reportes/inventario_pdf",$this->data);

		}		
	}

	public function inventarioPDF($p1,$p2,$p3,$p4)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
            $this->load->library('pdf');
            if(empty($p1) && empty($p2) && empty($p3) && empty($p4)){
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de inventario de".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
           		exit;

            } else {
            	$detalle='';
            	$title='';
            	$unidad='';
            	if ($p3=='3') {
					$detalle=$this->reportes_inventario_documental_model->listarDocumentosAlmacenados();    
					$title='INVENTARIO DOCUMENTAL';        		
            	} else if($p3=='4'){
            		$title= 'REPORTE DE DOCUMENTOS PRESTADOS';
            		$unidad='';
            		if ($p4!='0') {
            			$unidad='AND d.ID_UNIDAD='.$p4;
            		}
            		$detalle=$this->reportes_inventario_documental_model->listarDocumentosPrestados($p1,$p2,$unidad);
            	} else if($p3=='5'){
            		$title='REPORTE DE DOCUMENTOS PROXIMOS A VENCER';
            		if ($p4!='0') {
            			$unidad='AND d.ID_UNIDAD='.$p4;
            		}            		
            		$detalle=$this->reportes_inventario_documental_model->listarProximosavencer($p1,$p2,$unidad);
            	} else if($p3=='6') {
            		$title='REPORTE DE DOCUMENTOS ELIMINADOS';
            		if ($p4!='0') {
            			$unidad='AND d.ID_UNIDAD='.$p4;
            		}            		            		
            		$detalle=$this->reportes_inventario_documental_model->listarDocumentosEliminados($p1,$p2,$unidad);
            	} else if($p3=='1'){
            		$title='TABLA DE RETENCION DOCUMENTAL';
            		$detalle=$this->reportes_inventario_documental_model->listarDocumentosTabla($p1,$p2,$p4);            		
            	} else if($p3=='2'){
            		$title='TABLA DE VALORACION DOCUMENTAL';
            		$detalle=$this->reportes_inventario_documental_model->listarDocumentosTabla($p1,$p2,$p4);           		
            	}

            	$empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
            	if ($empleado==null) {
            		$empleado='Admin';
            	}
     	
				$html='
			            <!DOCTYPE HTML PUBLIC ">
			            <html>
			            <head>
			            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

			            <title>Inventario documental</title>
			            <style type="text/css">

			            @page {
			                margin: 1.3cm;
			            }


			        #header,
			        #foot {
			          position: fixed;
			          left: 0cm;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			      }

			      footer {
			      	position:fixed;
			      	top:0;
			      }

						#logo_clinica{
						    width:80%;
						    display: inline-block;
						    float:left;
						    position:abosolute;
						}

						#logo_sistema{
						    width:20%;
						    display: inline-block;
						    float:right;
						}
						#encabezado
						{
							position:absolute;
							top:65;
						    width:100%;
						    display:inline-block;
						    float:right;
						}
						body {
						   font-family: MuseoSans-300;
						    margin: 0.5cm 0;
						    text-align: justify;
						}
						#tet{
						    border-radius:15px;
						    width: 13%;
						    padding: 3% 5%;
						    display: inline-block;
						    float:right;
						    text-align: center;
						    border:1px solid red;
						    color: red;
						}
						#prueba{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						#prueba2{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						 #foot{
						    width: 90%;
						    position:absolute;
						    text-align: center;
						    top:60%x;
						    }
						table{
						    clear:both;
						    text-align: left;
						    height:auto;
						    border-collapse:collapse; 
						    table-layout:fixed;
						    width: 100%;
						}
						#cuerpo{
						    position: abosolute;
						    text-align: center;
						    bottom=12%;
						}

						table td {
						    word-wrap:break-word;
						    padding:10px 0px 5px 0px;
						}

						.page-number {
						  text-align: center;
						}

						.page-number:before {
						  content: "Página " counter(page);
						}

						hr {
						  page-break-after: always;
						  border: 0;
						  position:relative;
						}

		            header {
		                position: fixed;
		                top: 0cm;
		                left: 0cm;
		                right: 0cm;
		                height: 2cm;


		            }

		            footer {
		                position: fixed; 
		                top: 25.7cm; 
		                left: 0cm; 
		                right: 0cm;
		                height: 2cm;

		             }
						</style>
						</head>';
						$html.='
						<body>


					        <footer>
					    	<table>
							<tr>
							<td style="text-align:center;">JEFE UGDA</td>
							<td style="text-align:center;">ENCARGADO DE ARCHIVO CENTRAL</td>
							<td style="text-align:center;">SELLO</td>
							</tr>
							</table>
						 
					        </footer>						
							<div id="header">
							<div id="logo_clinica" > 
							<img src="img/MEMBRETE1-1.jpg" style="width:1200px; height:100px;">

							</div>
							<div id="logo_sistema">
								<div style="font-size:11px;text-align:center;">CREADO POR: '.$empleado.'</div>
								<div style="font-size:11px;text-align:center;">FECHA:'.date('d-m-Y').' </div>
							</div>
							<div id="encabezado">

							    <h3 style="text-align:center; font-size:18px;"> '.$title.' </h3>
							</div>
							<p></p>	
	
							</div>

						<!--	<footer>
							<div >
							<table>
							<tr>
							<td style="text-align:center;">______________</td>
							<td style="text-align:center;">______________</td>
							<td style="text-align:center;">______________</td>
							</tr>
							<tr>
							<td style="text-align:center; font-size:10px;">JEFE UGDA</td>
							<td style="text-align:center; font-size:10px;">ENCARGADO DE ARCHIVO CENTRAL</td>
							<td style="text-align:center; font-size:10px;">SELLO</td>
							</tr>
							</table>
							</div>
							</footer>-->
							';
						if(empty($detalle)){
							$html.="<div style='padding-top:200px;'>La informarci&oacute;n ingresada no es valida</div>";

							$nombre_archivo = "reporte no valido".date('d-m-Y');
							$paper_size = array(0,0,1248,816);		          
				            $this->pdf->loadHtml($html);
				            $this->pdf->setPaper($paper_size);
				            $this->pdf->set_option('defaultMediaType', 'all');
				            $this->pdf->set_option('isFontSubsettingEnabled', true);            
				            $this->pdf->render();
							$fontMetrics = $this->pdf->getFontMetrics();
							$canvas = $this->pdf->get_canvas();
							$font = $fontMetrics->getFont('Arial','bold');
							$canvas->page_text(1000, 20, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 12, array(0, 0, 0));	            
				            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));      
						}      
							if ($p3=='3' or $p3=='5' or $p3=='6') {
							$html .='	
							<table  border="1" style="width: 100%; table-layout: fixed;">
								<thead >
								<tr>
									<th style="width:10%;text-align:center;font-size:11px;" rowspan="2">CORRELATIVO CENTRAL</th>
									<th style="width:8%;text-align:center;font-size:11px;" rowspan="2">CODIGO SERIE</th>
									<th style="width:8%;text-align:center;font-size:11px;" rowspan="2">CODIGO SUBSERIE</th>
									<th style="width:12%;text-align:center;font-size:11px;" rowspan="2">SERIE</th>
									<th style="width:12%;text-align:center;font-size:11px;" rowspan="2">SUBSERIE</th>
									<th colspan="2" style="width:16%;text-align:center;font-size:11px;" >FECHAS EXTREMAS</th>
									<th colspan="3"  style="width:25%;text-align:center;font-size:11px;" >UBICACION</th>
									<th style="width:12%;text-align:center;font-size:11px;" rowspan="2">UNIDAD</th>
									<th style="width:7%;text-align:center;font-size:11px;" rowspan="2">No. FOLIOS</th>
									<th style="width:10%;text-align:center;font-size:11px;" rowspan="2">SOPORTE</th>
									<th style="width:10%;text-align:center;font-size:11px;" rowspan="2">FRECUENCIA DE CONSULTA</th>
									<th style="width:20%;text-align:center;font-size:11px;" rowspan="2">CONTENIDO</th>
									<th colspan="2" style="width:18%;text-align:center;font-size:11px;" >VIGENCIA DOCUMENTAL</th>
									<th style="width:11%;text-align:center;font-size:11px;" rowspan="2">FECHA FIN DE VIGENCIA</th>
								</tr>
								<tr>
									<th style="width:8%;text-align:center;font-size:10px;">INICIO</th>
									<th style="width:8%;text-align:center;font-size:10px;">FINAL</th>
									<th style="width:7%;text-align:center;font-size:10px;">PASILLO</th>
									<th style="width:7%;text-align:center;font-size:10px;">ESTANTE</th>
									<th style="width:7%;text-align:center;font-size:10px;">ANAQUEL</th>
									<th style="width:8%;text-align:center;font-size:10px;">GESTION</th>
									<th style="width:8%;text-align:center;font-size:10px;">CENTRAL</th>
								</tr>
								</thead>
								<tbody>';
								if ($detalle) {
									foreach ($detalle as $dt) {
										$html.='
										<tr>
										<td style="text-align:center;font-size:11px;">'.$dt->CORRELATIVO_CENTRAL.'</td>									
										<td style="text-align:center;font-size:11px;" >'.$dt->CODIGO_SERIE.'</td>
										<td style="text-align:center;font-size:11px;" >'.$dt->CODIGO_SUBSERIE.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->SERIE.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->SUBSERIE.'</td>
										<td style="text-align:center;font-size:11px;">'.date('d-m-Y',strtotime($dt->FECHA_DOCUMENTACION_INICIO)).'</td>
										<td style="text-align:center;font-size:11px;">'.date('d-m-Y',strtotime($dt->FECHA_DOCUMENTACION_FINAL)).'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->PASILLO.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->ESTANTE.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->ANAQUEL.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->UNIDAD.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->NUMERO_FOLIOS.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->SOPORTE.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->FRECUENCIA_CONSULTA.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->OBSERVACIONES.'</td>				
										<td style="text-align:center;font-size:11px;">'.$dt->VIGENCIA_GESTION.' a&ntilde;o/s</td>
										<td style="text-align:center;font-size:11px;">'.$dt->VIGENCIA_CENTRAL.' a&ntilde;o/s</td>
										<td style="text-align:center;font-size:11px;">'.date('d-m-Y',strtotime($dt->FECHA_SALIDA)).'</td>
										</tr>
										';
									}
								}
							$html.='
							</tbody>
							</table>';
								//var_dump($html);exit;
							} else if($p3=='4' ){

							$html.='	
							<table  border="1" style="width: 100%; table-layout: fixed;">
								<thead >
								<tr>
									<th rowspan="2" style="width:10%;text-align:center;font-size:14px;" >CORRELATIVO CENTRAL</th>
									<th rowspan="2" style="width:12%;text-align:center;font-size:14px;">FECHA DE SALIDA</th>
									<th rowspan="2" style="width:14%;text-align:center;font-size:14px;">FECHA DE REGRESO ESTIMADA</th>
									<th rowspan="2" style="width:18%;text-align:center;font-size:14px;">SERIE/SUBSERIE</th>
									<th rowspan="2" style="width:18%;text-align:center;font-size:14px;">DOCUMENTOS PRESTADOS</th>
									<th rowspan="2" style="width:16%;text-align:center;font-size:14px;">OFICINA PRODUCTORA</th>
									<th rowspan="2" style="width:16%;text-align:center;font-size:14px;">UNIDAD SOLICITANTE</th>
									<th colspan="3" style="width:18%;text-align:center;font-size:14px;">UBICACION</th>
									<th rowspan="2" style="width:14%;text-align:center;font-size:14px;">FECHA DE RETORNO REAL</th>
								</tr>
								<tr>
									<th style="width:7%;text-align:center;font-size:10px;">PASILLO</th>
									<th style="width:7%;text-align:center;font-size:10px;">ESTANTE</th>
									<th style="width:8%;text-align:center;font-size:10px;">ANAQUEL</th>
								</tr>
								</thead>
								<tbody>';
								if ($detalle) {
									foreach ($detalle as $dt) {
										$html.='
										<tr>
										<td style="text-align:center;font-size:11px;">'.$dt->CORRELATIVO_CENTRAL.'</td>				
										<td style="text-align:center;font-size:11px;">'.date('d-m-Y',strtotime($dt->FECHA_SALIDA_ARCHIVO_CENTRAL)).'</td>
										<td style="text-align:center;font-size:11px;">'.date('d-m-Y',strtotime($dt->FECHA_REGRESO_ARCHIVO_CENTRAL)).'</td>

										<td style="text-align:center;font-size:11px;">'.$dt->SERIE.'/'.$dt->SUBSERIE.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->DETALLE.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->UNIDAD.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->UNIDAD_SOLICITA.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->PASILLO.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->ESTANTE.'</td>
										<td style="text-align:center;font-size:11px;">'.$dt->ANAQUEL.'</td>										
										<td style="text-align:center;font-size:11px;">'.$dt->FECHA_RETORNO_REAL.'</td>

										</tr>
										';
									}
								}
							$html.='
							</tbody>
							</table>';
							//var_dump($html);exit;
							} else if($p3=='1' && $detalle) {
								$html.='<table style="width: 100%; table-layout: fixed;" border="1">
										<thead style="border-bottom:1px solid;">
											<tr>
												<th  colspan="6" style="width:50%; text-align:center; font-size:18px;">OFICINA PRODUCTORA<br/>'.$detalle[0]->CODIGO_UNIDAD.' </th>
												<th colspan="6"  style="width:50%;text-align:center; font-size:18px;"> '.strtoupper($detalle[0]->UNIDAD).'</th>

											</tr>
											<tr>
												<th  colspan="2" style="width:10%;text-align:center; font-size:14px;">CODIGO</th>
												<th rowspan="2" colspan="4" style="width:20%;text-align:center; font-size:14px;">SERIES DOCUMENTALES</th>
												<th  colspan="2" style="width:18%;text-align:center; font-size:14px;">RETENCION EN AÑOS</th>
												<th rowspan="2" style="width:15%;text-align:center; font-size:14px;">DISPOSICION FINAL</th>
												<th colspan="3" rowspan="2" style="width:20%; text-align:center; font-size:14px;">PROCEDIMIENTO</th>
											</tr>
											<tr>
												<th style="width:10%;text-align:center; font-size:14px;">SERIE</th>
												<th style="width:10%;text-align:center; font-size:14px;">SUB SERIE</th>
												<th style="width:10%;text-align:center; font-size:14px;">ARCHIVO GESTION</th>
												<th style="width:10%;text-align:center; font-size:14px;">ARCHIVO CENTRAL</th>
											</tr>


										</thead>
										<tbody>
										';
								foreach ($detalle as $dt) {
									$html.='
											<tr>
												<td style="text-align:center; font-size:14px;">'.$dt->CODIGO_SERIE.'</td>
												<td style="text-align:center; font-size:14px;">'.$dt->CODIGO_SUBSERIE.'</td>
												<td colspan="4" style="text-align:center; font-size:14px;">'.$dt->SERIE.'<br/> 
												'.$dt->SUBSERIE.'<br/>
												'.$dt->TIPOLOGIA_SERIE.'<br/>
												'.$dt->TIPOLOGIA_SUBSERIE.'</td>
												<td style="text-align:center; font-size:14px;">'.$dt->VIGENCIA_GESTION.'</td>
												<td style="text-align:center; font-size:14px;">'.$dt->VIGENCIA_CENTRAL.'</td>
												<td style="text-align:center; font-size:14px;">'.$dt->DISPOSICION_FINAL.'</td>
												<td colspan="3 style="text-align:center; font-size:14px;">'.strip_tags(html_entity_decode($dt->PROCEDIMIENTO)).'</td>
											</tr>

										';
								}
								$html.='</tbody>
									</table>
									<table style="width: 100%; table-layout: fixed; border:1px solid; padding-top:20px;">
									<tr >
										<td style="text-align:center; font-size:14px;">__________________________</td>
										<td style="text-align:center; font-size:14px;">___________________________</td>
										<td style="text-align:center; font-size:14px;">'.date('d-m-Y').'</td>
									</tr>
									<tr>
										<td style="text-align:center; font-size:14px;">Encargado/a de '.$detalle[0]->UNIDAD.'</td>
										<td style="text-align:center; font-size:14px;">Encargado/a de Archivo Central</td>
										<td style="text-align:center; font-size:14px;">FECHA DE ACTUALIZACION</td>
									</tr>
									</table>
								';

							} else if($p3=='2' && $detalle)
							{
								$html.='<table style="width: 100%; table-layout: fixed;" border="1">
										<thead style="border-bottom:1px solid;">
											<tr>
												<th  colspan="6" style="width:50%;text-align:center; font-size:18px;">OFICINA PRODUCTORA<br/>'.$detalle[0]->CODIGO_UNIDAD.' </th>
												<th colspan="6"  style="width:50%;text-align:center; font-size:18px;"> '.strtoupper($detalle[0]->UNIDAD).'</th>

											</tr>
											<tr>
												<th  colspan="2" style="width:20%;text-align:center; font-size:14px;">CODIGO</th>
												<th rowspan="2" colspan="4" style="width:20%;text-align:center; font-size:14px;">SERIES DOCUMENTALES</th>
												<th rowspan="2" style="width:15%;text-align:center; font-size:14px;">RETENCION EN ARCHIVO CENTRAL</th>
												<th rowspan="2" style="width:30%;text-align:center; font-size:14px;">DISPOSICION FINAL</th>
												<th colspan="4" rowspan="2" style="width:40%;text-align:center; font-size:14px;">PROCEDIMIENTO</th>
											</tr>
											<tr>
												<th style="width:10%;text-align:center; font-size:14px;">SERIE</th>
												<th style="width:10%;text-align:center; font-size:14px;">SUB SERIE</th>
												
											</tr>


										</thead>
										<tbody>
										';
								foreach ($detalle as $dt) {
									$html.='

											<tr>
												<td style="text-align:center; font-size:14px;">'.$dt->CODIGO_SERIE.'</td>
												<td style="text-align:center; font-size:14px;">'.$dt->CODIGO_SUBSERIE.'</td>
												<td colspan="4" style="text-align:center; font-size:14px;">'.$dt->SERIE.'<br/> 
												'.$dt->SUBSERIE.'<br/>
												'.$dt->TIPOLOGIA_SERIE.'<br/>
												'.$dt->TIPOLOGIA_SUBSERIE.'</td>
												<td style="text-align:center; font-size:14px;" >'.$dt->VIGENCIA_GESTION.'</td>
												<td style="text-align:center; font-size:14px;">'.$dt->DISPOSICION_FINAL.'</td>
												<td colspan="4" style="text-align:center; font-size:14px;">'.html_entity_decode($dt->PROCEDIMIENTO).'</td>
											</tr>

										';
								}
								$html.='</tbody>
									</table>
									<table style="width: 100%; table-layout: fixed; border:1px solid; padding-top:20px;">
									<tr>
										<td style="text-align:center; font-size:14px;">___________________________</td>

										<td style="text-align:center; font-size:14px;">'.date('d-m-Y').'</td>
									</tr>
									<tr>

										<td style="text-align:center; font-size:14px;">Encargado/a de '.$detalle[0]->UNIDAD.'</td>
										<td style="text-align:center; font-size:14px;">FECHA</td>
									</tr>
									</table>
								';								
							}
					if($p3!='1' && $p3!='2'){
						$html.='
						</body></html>
						';
					}          							
		            $nombre_archivo = "Inventario Documental ".date('d-m-Y');
		            //var_dump($html);exit;
				$paper_size = array(0,0,1248,816);		            
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper($paper_size);
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
				$fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('Arial','bold');
				$canvas->page_text(1050, 20, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 12, array(0, 0, 0));	            
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
            }			
		}
		
	}
}