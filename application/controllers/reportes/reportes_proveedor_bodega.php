<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 
 */
class reportes_proveedor_bodega extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("reportes_proveedor_bodegas_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Reportes","reportes/reportes_proveedor_bodega");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes de Proveedor por Bodega');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			/*Obtener lista de actividades */

            $this->data['proveedores'] = $this->reportes_proveedor_bodegas_model->listarProveedores();
            $this->data['unidades']=$this->reportes_proveedor_bodegas_model->listarUnidades();
			$this->template->admin_render("admin/reportes/index_proveedor",$this->data);
		}
		
	}

	public function vistaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes de Proveedor por Bodega');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Reportes vista","reportes/reportes_proveedor_bodega/vistaPDF");		
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$datos= array(
				'fechaInicio'=>$_POST['fechaInicio'],
				'fechaFinal'=>$_POST['fechaFinal'],
				'idUnidad'=>$this->ion_auth->get_unidad_empleado(),
				'idProveedor'=>$_POST['proveedor_id']
			);
			$this->data['datos']=$datos;

			$this->template->admin_render("admin/reportes/proveedores_compra_pdf",$this->data);	
		}
		
	}

	public function reportePDF($p1,$p2,$p3,$p4)
	{

		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
            $this->load->library('pdf');		
			if (empty($p1) && empty($p2) && empty($p3) && empty($p4)) {
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de proveedores de".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
           		exit;
			} else {
				$proveedor='';
				if ($p4!='0') {
					$proveedor=" and p.ID_PROVEEDOR=".$p4;
				}
				$detalle=$this->reportes_proveedor_bodegas_model->listarProveedoresUnidad($p1,$p2,$p3,$proveedor);
                $empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
                if ($empleado==null) {
                    $empleado='Admin';
                }
				$bodega =$this->reportes_proveedor_bodegas_model->obtenerBodega($this->ion_auth->get_user_id());

						$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
						$fecha1=date('d')." de ".$meses[date('n',strtotime($p2))-1]." de ".date('Y');                

				//var_dump($detalle);exit;
				$html='
			            <!DOCTYPE HTML PUBLIC ">
			            <html>
			            <head>
			            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

			            <title>Orden de Compra</title>
			            <style type="text/css">

			            @page {
			                margin: 1.1cm;
			            }


			        #header,
			        #footer {
			          position: fixed;
			          left: 0;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			          
			      }
			      #footer {
			          bottom: 0;
			          border-top: 0.1pt solid #aaa;
			      }
						#logo_clinica{
						    width:80%;
						    display: inline-block;
						    float:left;
						}

						#logo_sistema{
						    width:20%;
						    display: inline-block;
						    float:right;
						}
						#encabezado
						{
						    width:100%;
							padding-top:100px;
						    display:inline-block;
						    float:right;
						}
						body {
						   font-family: Sans-Serif;
						    margin: 0.5cm 0;
						    text-align: justify;
						}
						#tet{
						    border-radius:15px;
						    width: 13%;
						    padding: 3% 5%;
						    display: inline-block;
						    float:right;
						    text-align: center;
						    border:1px solid red;
						    color: red;
						}
						#prueba{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						#prueba2{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						 #foot{
						    width: 90%;
						    position:absolute;
						    text-align: center;
						    bottom:-1%;
						    }
						table{
						    clear:both;
						    text-align: left;
						    height:auto;
						    border-collapse:collapse; 
						    table-layout:fixed;
						    width: 100%;
						}
						#cuerpo{
						    position: abosolute;
						    text-align: center;
						    bottom=12%;
						}

						table td {
						    word-wrap:break-word;
						    padding:10px 0px 10px 0px;
						}

						.page-number {
						  text-align: center;
						}

						.page-number:before {
						  content: "Página " counter(page);
						}

						hr {
						  page-break-after: always;
						  border: 0;
						}

						</style>
						</head>';
						$html.='
						<body>
							<div id="header">
							<div id="logo_clinica" > 
							<img src="img/MEMBRETE1-1.jpg" style="width:450; height=100;">

							</div>
							<div id="logo_sistema">
								<div style="font-size:12px;text-align:center;">Creado por: '.$empleado.'</div>
								<div style="font-size:12px;text-align:center;">Fecha:'.date('d-m-Y').' </div>
							</div>
							<div id="encabezado">

							    <h3 style="text-align:center;">Reporte de Proveedores de '.$bodega=$bodega[0]->DESCRIPCION.' al '.$fecha1.'</h3>

							</div>
							<p></p>	
	
							</div>	
							<table  style="width: 100%; table-layout:fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th style="width:12%;">Proveedor</th>
									<th style="width:10%;"># de orden</th>
									<th style="width:16%;">Fecha de orden</th>
									<th style="width:18%;">NIT</th>
									<th style="width:12%;">Telefono</th>
									<th style="width:14%;">Direcci&oacute;n</th>
								</tr>
								</thead>
								<tbody>';
								foreach ($detalle as $dt) {
									$html.='
									<tr>
									<td>'.$dt->NOMBRE_PROVEEDOR.'</td>
									<td>'.$dt->CODIGO_ORDEN_COMPRA.'</td>
									<td>'.date('d-m-Y',strtotime($dt->FECHA)).'</td>
									<td>'.$dt->NIT.'</td>
									<td>'.$dt->TELEFONO.'</td>
									<td>'.$dt->DIRECCION.'</td>
									</tr>
									';
								}
							$html.='
							</tbody>
							</table>';
					
					$html.='
					<div id="foot">
					<table>
					<tr>
					<td style="text-align:center;width:10%;"></td>
					<td style="text-align:center;">______________</td>
					<td style="text-align:center;">______________</td>
					</tr>
					<tr>
					<td style="text-align:center;"></td>
					<td style="text-align:center;">Firma</td>
					<td style="text-align:center;">Sello</td>
					</tr>
					</table>
					</div>
					</body></html>
					';          							
		            $nombre_archivo = "reporte de proveedores ".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
				$fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('Arial','bold');
				$canvas->page_text(480, 20, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 10, array(0, 0, 0));
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
			}
			
		}
		
	}
}