<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 
 */
class reportes_diarios_proyectos extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("reportes_diarios_proyectos_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Reportes","reportes/reportes_diarios_proyectos");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['proyectos']=$this->reportes_diarios_proyectos_model->listarProyectosSelect();
			$this->template->admin_render("admin/reportes/index_proyectos_diarios",$this->data); 
		}
		
	}

	public function vistaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Reportes","reportes/reportes_diarios_proyectos/vistaPDF");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());



			$datos= array(
				'fechaInicio'=>!empty($_POST['fechaInicio'])? $_POST['fechaInicio'] : "1",
				'fechaFin'=>!empty($_POST['fechaFinal'])? $_POST['fechaFinal']: "1",
				'tipo'=>!empty($_POST['tipoReporte']) ? $_POST['tipoReporte'] : "1",
				'idProyecto' => !empty($_POST['idProyecto']) ? $_POST['idProyecto'] : "0"
			);

			$this->data['datos']=$datos;
			$this->template->admin_render("admin/reportes/reporte_diario_pdf",$this->data); 			
		}
		
	}



	public function reporteDiarioPDF($fechaInicio,$fechaFin,$tipo,$idProyecto)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->load->library("pdf");
			if (empty($fechaInicio) && empty($fechaFinal) && empty($autoriza1) && empty($autoriza2) && empty($tipo)) {
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de proveedores de".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
			} else {
				$fecha = date('d-m-Y');
				$total = 0.0;

				$empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
				if ($empleado == null) {
					$empleado='Admin';
				}

				if ($tipo == 1) {
					$detalle=$this->reportes_diarios_proyectos_model->listarBitacorasReporte($idProyecto,$fechaInicio,$fechaFin);
				} else if($tipo == 2) {
					$detalle=$this->reportes_diarios_proyectos_model->listarEntradaMaterial($idProyecto,$fechaInicio,$fechaFin);
				} else if($tipo == 3) {
					$detalle= $this->reportes_diarios_proyectos_model->listarSalidaMaterial($idProyecto,$fechaInicio,$fechaFin);					
				} else {
					$detalle=array();
				}
				
				$html='
			            <!DOCTYPE HTML PUBLIC ">
			            <html>
			            <head>
			            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

			            <title>Kardex</title>
			            <style type="text/css">

			            @page {
			                margin: 1cm;
			            }


			        #header,
			        #footer {
			          position: fixed;
			          left: 0;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			          
			      }
			      #footer {
					  position: fixed;
					  left: 0;
					  right: 0;
				      color: #aaa;
					  font-size: 0.9em;
					}
					#footer table {
						width: 100%;
						border-collapse: collapse;
						border: none;
					}
						#logo_clinica{
						    width:80%;
						    display: inline-block;
						    float:left;
						    padding-top:20px;
						}

						#logo_sistema{
						    width:20%;
						    display: inline-block;
						    float:right;
						}
						#encabezado
						{
						    width:100%;
						    display:inline-block;
						    float:right;
						    padding-top:130px;
						}
						body {
						   font-family: Sans-Serif;
						     margin: 18pt 18pt 24pt 18pt;
						    text-align: justify;
						}
						#tet{
						    border-radius:15px;
						    width: 13%;
						    padding: 3% 5%;
						    display: inline-block;
						    float:right;
						    text-align: center;
						    border:1px solid red;
						    color: red;
						}
						#prueba{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						#prueba2{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						 #foot{
						    width: 100%;
						    position:absolute;
						    text-align: center;
						    bottom:1%;
						    padding:-20px;

						    }
						table{
						    clear:both;
						    text-align: left;
						    height:auto;
						    border-collapse:collapse; 
						    table-layout:fixed;
						    width: 100%;	
							word-wrap:break-word;						    
							page-break-inside:auto !important;						    			
						}
						table thead tr th{
							 page-break-inside:auto !important;
						}
						#cuerpo{
						    position: abosolute;
						    text-align: center;
						    bottom=12%;
						}

						table td th{
							word-wrap:break-word;
						    padding:5px 0px 10px 0px;
							page-break-inside:avoid !important;
						}



						.page-number {
						  text-align: center;
						}

						.page-number:before {
						  content: "Página" counter(page);
						}

						.page_break {
						  page-break-after: always;
						  
						}

						</style>
						</head>';
				$html.='
				<body>
				<div id="header">
				<div id="logo_clinica" > 
				<img src="img/MEMBRETE1-1.jpg" style="width:750px; height:120px;">

				</div>
				<div id="logo_sistema" >
				<div style="font-size:12px;text-align:center;">CREADO POR: '.$empleado.'</div>
				<div style="font-size:12px;text-align:center;">FECHA:'.$fecha.' </div>
				</div>
				';
				if ($tipo==2) {
					$html.='<div id="encabezado">
				<h3 style="text-align:center; font-size:14px;">DEPRTAMENTO DE INGENIERIA Y ARQUITECTURA</h3>
				<p style="text-align:center;font-size:14px;position:relative;bottom:2px;">CONTROL DE INGRESOS DE MATERIAL</p>
				</div>';
				} else if($tipo==3)
				{
					$html.='<div id="encabezado">
					<h3 style="text-align:center; font-size:14px;">DEPRTAMENTO DE INGENIERIA Y ARQUITECTURA</h3>
					<p style="text-align:center;font-size:14px;position:relative;bottom:2px;">CONTROL DE Gastos DE MATERIAL</p>
					</div>';
				} 
				
				$html.='
				<p></p>	

				</div>

				';

				if($tipo==1 && !empty($detalle)){
					$mantenimeinto='';
					foreach ($detalle as $d) {
						$mantenimiento= $d->MANTENIMIENTO == true ? 'SI' : 'NO';
					$html.='<table border="0" style="width: 100%; table-layout: fixed; padding-bottom:25px;">
						<tbody>
							<tr>
								<td style="font-size:14px;text-align:left;" >T&eacute;cnico supervisor de la obra: </td>
								<td style="font-size:14px;text-align:justify;"> '.$d->EMPLEADO.'</td>
								<td style="font-size:14px;text-align:left;" >Hoja No. </td>
								<td style="font-size:14px;text-align:justify;">____________</td>							
							</tr>
							<tr>
								<td style="font-size:14px;text-align:left;">Tipo de obra: </td>
								<td style="font-size:14px;text-align:justify;">'.$d->TIPO_OBRA.'</td>
								<td style="font-size:14px;text-align:left;">Mantenimiento: </td>
								<td style="font-size:14px;text-align:justify;">'.$mantenimiento.'</td>								
							</tr>
							<tr>
								<td style="font-size:14px;text-align:left;" >Nombre de la obra:</td>
								<td style="font-size:14px;text-align:justify;">'.$d->CODIGO_PROYECTO.' '.$d->NOMBRE_PROYECTO.'</td>
							</tr>					
							<tr>
								<td style="font-size:14px;text-align:left;" >Nombre del realizador:</td>
								<td style="font-size:14px;text-align:justify;">'.$d->NOMBRE_REALIZADOR.' </td>
							</tr>
							<tr>
								<td style="font-size:14px;text-align:left;" >Fecha de inicio:</td>
								<td style="font-size:14px;text-align:justify;">'.date('Y-m-d',strtotime($d->FECHA_INICIO)).'</td>
								<td style="font-size:14px;text-align:left;" >Clima:</td>
								<td style="font-size:14px;text-align:justify;">'.$d->CLIMA_DIA.'</td>								
							</tr>																
							<tr>
								<td></td>
								<td></td>
								<td style="font-size:14px;text-align:left;" >Hora de inicio:</td>
								<td style="font-size:14px;text-align:justify;">'.date('H:m',strtotime($d->HORA_CREACION)).'</td>
							</tr>			
						</tbody>
					</table>';
					$html.='<table border="1" style="width: 100%; table-layout: fixed; border-bottom:1px;">
						<thead>
							<tr>
								<th  style="font-size:16px;text-align:center;background-color:#A7A4A4;">DESCRIPCION</th>
							</tr>
						</thead>
						<tbody>
					';						
						$html.='<tr >';
						$html.='<td style="font-size:12px;text-align:justify;padding: 5px; 0px;10px;0px;">'.$d->DESCRIPCION.'</td>';
						$html.='</tr>';
						$html.='<tr><td style="padding:5px;"><img style="width:auto;height:120px;" src="'.$d->COMPROBANTE_FOTOGRAFICO.'"></td></tr>';
						$html.='</tbody></table>';
						$html.='
						<div  id="foot" style="bottom:25%; left:10%; position:fixed !important; ">
							<table border="1" style="width:600px">
								<tbody >
									<tr>
									<td colspan="2" style="text-align:center;width:25%; font-size:12px; padding-bottom:15px;">REMITENTE</td>
									<td colspan="2" style="text-align:center;width:25%; font-size:12px;padding-bottom:15px;">RECIBE</td>								
									</tr>
									<tr>
										<td style="text-align:center;width:25%; font-size:12px;padding-bottom:60px;" >FIRMA</td>
										<td>'.date('m/d/Y').'</td>
										<td style="text-align:center;width:25%; font-size:12px;padding-bottom:60px;" >FIRMA</td>
										<td>'.date('m/d/Y').'</td>									
									</tr>
									<tr>
										<td colspan="2" style="text-align:center;width:25%; font-size:12px;padding-bottom:90px;">Nombre</td>
										<td colspan="2" style="text-align:center;width:25%; font-size:12px;padding-bottom:90px;">Nombre</td>									
									</tr>								
								</tbody>
							</table>

						</div>

						';     		
					
					}


					//var_dump($html);exit;
										
				} else if ($tipo==2 && !empty($detalle)) {
					$html.='<table border="0" style="width: 100%; table-layout: fixed; padding-bottom:25px;">
						<tbody>
							<tr>
								<td style="font-size:14px;text-align:left;" >NOMBRE DEL PROYECTO</td>
								<td style="font-size:14px;text-align:justify;"> '.$detalle[0]->CODIGO_PROYECTO.' '.$detalle[0]->NOMBRE_PROYECTO.'</td>

							</tr>
							<tr>
								<td style="font-size:14px;text-align:left;">NOMBRE DEL REALIZADOR</td>
								<td style="font-size:14px;text-align:justify;">'.$detalle[0]->NOMBRE_REALIZADOR.'</td>
							</tr>
							<tr>
								<td style="font-size:14px;text-align:left;" >NOMBRE DEL SUPERVISOR</td>
								<td style="font-size:14px;text-align:justify;">'.$detalle[0]->EMPLEADO.'</td>
							</tr>							
						</tbody>
					</table>';
					$html.='<table border="1" style="width: 100%; table-layout: fixed; border-bottom:1px;">
						<thead>
							<tr>
								<th colspan="3" style="font-size:16px;text-align:center;background-color:#A7A4A4;">ENTRADA DE MATERIAL</th>
							</tr>
							<tr>
								<th style="font-size:14px;text-align:justify;background-color:#A7A4A4;">DESCRIPCION DEL MATERIAL</th>
								<th style="font-size:14px;text-align:center;background-color:#A7A4A4;">CANTIDAD</th>
								<th style="font-size:14px;text-align:center;background-color:#A7A4A4;">FECHA</th>
							</tr>
						</thead>
						<tbody>
					';
					foreach ($detalle as $d) {
						$html.='<tr >';
						$html.='<td style="font-size:12px;text-align:justify;padding: 5px; 0px;10px;0px;">'.$d->DESCRIPCION.'</td>
								<td style="font-size:12px;text-align:center; padding: 5px; 0px;10px;0px;">'.$d->CANTIDAD_RECIBIDA.'</td>
								<td style="font-size:12px;text-align:center;padding: 5px; 0px;10px;0px; ">'.date('Y-m-d',strtotime($d->FECHA_RECIBIDA)).'</td>';
						$html.='</tr>';
					}

					$html.='</tbody></table>';
				} else if($tipo == 3 && !empty($detalle)){
	
					$html.='<table border="0" style="width: 100%; table-layout: fixed; padding-bottom:25px;">
						<tbody>
							<tr>
								<td style="font-size:14px;text-align:left;" >NOMBRE DEL PROYECTO</td>
								<td style="font-size:14px;text-align:justify;"> '.$detalle[0]->CODIGO_PROYECTO.' '.$detalle[0]->NOMBRE_PROYECTO.'</td>

							</tr>
							<tr>
								<td style="font-size:14px;text-align:left;">NOMBRE DEL REALIZADOR</td>
								<td style="font-size:14px;text-align:justify;">'.$detalle[0]->NOMBRE_REALIZADOR.'</td>
							</tr>
							<tr>
								<td style="font-size:14px;text-align:left;" >NOMBRE DEL SUPERVISOR</td>
								<td style="font-size:14px;text-align:justify;">'.$detalle[0]->NOMBRE_SUPERVISOR.'</td>
							</tr>							
						</tbody>
					</table>';
					$html.='<table border="1" style="width: 100%; table-layout: fixed; border-bottom:1px;">
						<thead>
							<tr>
								<th colspan="3" style="font-size:16px;text-align:center;background-color:#2E2D2D;">GASTO DE MATERIAL</th>
							</tr>
							<tr>
								<th style="font-size:14px;text-align:justify;background-color:#2E2D2D;">DESCRIPCION DEL MATERIAL</th>
								<th style="font-size:14px;text-align:center;background-color:#2E2D2D;">CANTIDAD</th>
								<th style="font-size:14px;text-align:center;background-color:#2E2D2D;">FECHA</th>
							</tr>
						</thead>
						<tbody>
					';

					foreach ($detalle as $d) {
						$html.='<tr >';
						$html.='<td style="font-size:12px;text-align:justify;padding: 5px; 0px;10px;0px;">'.$d->DESCRIPCION.'</td>
								<td style="font-size:12px;text-align:center; padding: 5px; 0px;10px;0px;">'.$d->CANTIDAD_CONSUMIDA.'</td>
								<td style="font-size:12px;text-align:center;padding: 5px; 0px;10px;0px; ">'.date('Y-m-d',strtotime($d->FECHA_CONSUMO)).'</td>';
						$html.='</tr>';
					}

					$html.='</tbody></table>';
					//var_dump($html);exit;
				} else {
					$html.='No hay datos validos';
				}
				
				if ($tipo == 2 || $tipo == 3) {
					$html.='
					<div id="foot" >
					<table style="width:720px;">
					<tr>
					<td style="text-align:center;width:25%; font-size:12px;">FIRMA DEL REALIZADOR</td>
					<td style="text-align:center;width:25%;">____________________</td>

					<td style="text-align:center;width:25%; font-size:12px;">FIRMA DEL SUPERVISOR</td>
					<td style="text-align:center;width:25%;">____________________</td>
					</tr>
					</table>
					</div>
					</body>
					</html>
					';         
				} else if($tipo==1){
					$html.='
					</body></html>';
				}
				

				//var_dump($html);exit;
		        $nombre_archivo = "Rerporte de consumo vales ".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            // $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('Arial','bold');
				$canvas->page_text(480, 15, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 10, array(0, 0, 0));
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0)); 				
			}
			
		}
		
	}
}