<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 
 */
class reporte_consumo_quincenal extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("reportes_consumo_quincenal_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Reportes","reportes/reporte_consumo_quincenal");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());


			$this->template->admin_render("admin/reportes/index_quincenal",$this->data); 
		}
		
	}

	public function vistaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Reportes","reportes/reporte_consumo_quincenal/vistaPDF");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			if (!isset($_POST['autoriza1']) or $_POST['autoriza1']=='') {
				$_POST['autoriza1']='0';
			} 
			 if(!isset($_POST['autoriza2']) or $_POST['autoriza2']==''){
				$_POST['autoriza2']='0';
			}

			$datos= array(
				'fechaInicio'=>$_POST['fechaInicio'],
				'fechaFin'=>$_POST['fechaFinal'],
				'autoriza1'=>$_POST['autoriza1'],
				'autoriza2'=>$_POST['autoriza2'],
				'tipo'=>$_POST['tipo']
			);

			$this->data['datos']=$datos;
			$this->template->admin_render("admin/reportes/quincenal_pdf",$this->data); 			
		}
		
	}

	public function reporte_detallado()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$fechaInicio=$_POST['fechaInicio'];
			$fechaFinal=$_POST['fechaFinal'];
			$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
			$fecha1=$meses[date('n',strtotime($fechaFinal))-1]." de ".date('Y');			
			$this->data['fechaFin']=$fecha1;
			//$this->data['vales']=$this->reportes_consumo_quincenal_model->listarVales($fechaInicio,$fechaFinal);
			$this->data['fechas']=$this->reportes_consumo_quincenal_model->listarDias($fechaInicio,$fechaFinal);
			$this->load->view("admin/reportes/reporte_quincenal_detallado",$this->data);
		}
		
	}

	public function quincenalPDF($fechaInicio,$fechaFin,$autoriza1,$autoriza2,$tipo)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->load->library("pdf");
			if (empty($fechaInicio) && empty($fechaFinal) && empty($autoriza1) && empty($autoriza2) && empty($tipo)) {
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de proveedores de".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
			} else {
				$fecha = date('d-m-Y');
				$total = 0.0;

				$empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
				if ($empleado == null) {
					$empleado='Admin';
				}
				$vales = $this->reportes_consumo_quincenal_model->listarVales($fechaInicio,$fechaFin);
				$html='
	            <!DOCTYPE HTML PUBLIC ">
	            <html>
	            <head>
	            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

	            <title>Reporte de consumo</title>
	            <style type="text/css">

	            @page {
	                margin: 1.3cm;

	            }
			        #header,
			        #footer {
			          position: fixed;
			          left: 0;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			          
			      }
			      #footer {
			          bottom: 0;
			          border-top: 0.1pt solid #aaa;
	     		 }
				#logo_clinica{
				    width:80%;
				    display: inline-block;
				    float:left;
				}

				#logo_sistema{
				    width:20%;
				    display: inline-block;
				    float:right;
				}
				#encabezado
				{
				    width:100%;
					padding-top:200px;
				    display:inline-block;
				    float:right;
				}
				body {
				   font-family: Sans-Serif;
				    margin: 0.5cm 0;
				    text-align: justify;
				}
				#tet{
				    border-radius:15px;
				    width: 13%;
				    padding: 3% 5%;
				    display: inline-block;
				    float:right;
				    text-align: center;
				    border:1px solid red;
				    color: red;
				}
				#prueba{
				    position: relative;
				    top: 40%;
				    text-align:center;
				    left: 2%;
				  }
				#prueba2{
				    position: relative;
				    top: 40%;
				    text-align:center;
				    left: 2%;
				  }
				 #foot{
				    width: 90%;
				    position:absolute;
				    text-align: center;
				    bottom:-3%;
				    padding:-20px;
				    }
				table{
				    clear:both;
				    text-align: left;
				    height:auto;
				    border-collapse:collapse; 
				    table-layout:fixed;
				    width: 100%;
				}
				#cuerpo{
				    position: abosolute;
				    text-align: center;
				    bottom=12%;
				}

				table td {
				    word-wrap:break-word;
				    padding:15px 0px 10px 0px;

				}

				.page-number {
				  text-align: center;
				}

				.page-number:before {
				  content: "Página" counter(page);
				}

				hr {
				  page-break-after: always;
				  border: 0;
				}

				</style>
				</head>';
				if ($tipo==1) {
					
						$html.='
						<body>
							<div id="header">
							<div id="logo_clinica" > 
							<img src="img/MEMBRETE1-1.jpg" style="width:800; height=50;">

							</div>
							<div id="logo_sistema">
								<div style="font-size:12px;text-align:center;">Creado por: '.$empleado.'</div>
								<div style="font-size:12px;text-align:center;">Fecha:'.$fecha.'</div>
							</div>
							<div id="encabezado">

							    <h3 style="text-align:center;">Reporte de consumo de combustible por mision o departamento</h3>
							    <p style="text-align:center;position:relative;bottom:2px;">DESDE '.$fechaInicio.' HASTA '.$fechaFin.'</p>
							</div>
							<p></p>	
							</div>';

							$html.='<table style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th style="width:5%;text-align:center;font-size:14px;">EQUIPO N°</th>
									<th style="width:5%;text-align:center;">N° PLACA</th>
									<th style="width:5%;text-align:center;font-size:14px;">FECHA</th>
									<th style="width:10%;text-align:justify;font-size:14px;">DEPARTAMENTO O MISIÓN A REALIZAR</th>
									<th style="width:5%;text-align:center;font-size:14px;">N° VALE</th>
									<th style="width:5%;text-align:center;font-size:14px;">VALOR</th>
								</tr>
								</thead>
								<tbody>';
								$total=0.0;
								foreach ($vales as $v) {
									$html.='
									<tr>
									<td style="text-align:center;font-size:11px;">'.$v->NUMERO_EQUIPO.'</td>
									<td style="text-align:center;font-size:11px;">'.$v->PLACA.'</td>
									<td style="text-align:center;font-size:11px;">'.date('d-m-Y',strtotime($v->FECHA)).'</td>
									<td style="text-align:justify;font-size:11px;">'.$v->OBSERVACIONES.'</td>
									<td style="text-align:center;font-size:11px;">'.$v->NUMERO_VALE.'</td>
									<td style="text-align:center;font-size:11px;">$'.number_format($v->VALOR,2).'</td>
									</tr>';
									$total = (float)$total+(float)$v->VALOR;
								}
							
								$html.='
								<tr>
									<td style="text-align:center;font-size:11px;"></td>
									<td style="text-align:center;font-size:11px;"></td>
									<td style="text-align:center;font-size:11px;"></td>
									<td style="text-align:justify;font-size:11px;"></td>
									<td style="text-align:center;font-size:11px;"></td>
									<td style="text-align:justify;font-size:14px;"><b>$'.number_format($total,2).'</b></td>
								</tr>
								';
							$html.='
							</tbody>
							</table>';
						}else if($tipo==2){
						$html.='
						<body>
							<div id="header">
							<div id="logo_clinica" > 
							<img src="img/MEMBRETE1-1.jpg" style="width:800; height=50;">

							</div>
							<div id="logo_sistema">
								<div style="font-size:12px;text-align:center;">Creado por: '.$empleado.'</div>
								<div style="font-size:12px;text-align:center;">Fecha:'.$fecha.'</div>
							</div>
							<div id="encabezado">

							    <h3 style="text-align:center;">Reporte de consumo de combustible por mision o departamento</h3>
							    <p style="text-align:center;position:relative;bottom:2px;">DESDE '.$fechaInicio.' HASTA '.$fechaFin.'</p>
							</div>
							<p></p>	
							</div>';							
							$fechas=$this->reportes_consumo_quincenal_model->listarDias($fechaInicio,$fechaFin);
							$vehiculos=$this->reportes_consumo_quincenal_model->listarVehiculos();
							$totalvpu=0.0;
							$totalgpu=0.0;
							$sumatotalvpu=0.0;
							$sumatotalgpu=0.0;																
								$html.='<table style="width: 100%; table-layout: fixed;" border="1">
								<thead style="border-bottom:1px solid;">
								<tr>
								<td rowspan="2" style="text-align:center;font-size:11px;width:6%;">No. Placa</td>
								<td rowspan="2" style="text-align:center;font-size:11px;width:6%;">No. Equipo</td>';
								foreach ($fechas as $f) {
									$html.='<td colspan="3"style="text-align:center;font-size:11px;width:9%;" >'.$f->DIA_SEMANA.' '.$f->DIA_FECHA.'</td>';
								}
								$html.='
								<td rowspan="2" style="text-align:center;font-size:11px;width:6%;">Total  Valor</td>
								<td rowspan="2" style="text-align:center;font-size:11px;width:7%;">Total Galones</td>
								</tr>
								<tr>';
								foreach ($fechas as $f) {
									$html.='<td style="text-align:center;font-size:8px;width:3%;">$</td>
											<td style="text-align:center;font-size:8px;width:4%;">Galones</td>
											<td style="text-align:center;font-size:8px;width:2%;">Vale</td>';
								}
								$html.='</tr>
								</thead>';
								$html.='<tbody>';
								foreach ($vehiculos as $v) {
									$html.= '<tr><td style="text-align:center;font-size:8px;width:6%;">'.$v->PLACA.'</td>
									<td style="text-align:center;font-size:8px;width:6%;">'.$v->CODIGO_VEHICULO.'</td>';

									foreach ($fechas as $f) {
									$vales=$this->reportes_consumo_quincenal_model->listarValesDetalle($f->selected_date,$v->ID_VEHICULO);											
											if (empty($vales)) {
												$html.='<td style="text-align:center;font-size:8px;width:3%; padding:5px 0px 0px 0px;">0</td>
												<td style="text-align:center;font-size:8px;width:3%;">0.00</td>
												<td></td>';

											} else  {

												foreach ($vales as $va) {

													$totalgpu=(float)$totalgpu+(float)$va->GALONES;
													$totalvpu=(float)$totalvpu+(float)$va->VALOR;  
													$html.='<td style="text-align:center;font-size:8px;width:4%;">'.number_format((float)$va->VALOR,2).'</td>
														<td style="text-align:center;font-size:8px;width:4%;">'.number_format((float)$va->GALONES,2).'</td>
														<td style="text-align:center;font-size:8px;width:2%;">'.$va->NUMERO_VALE.'</td>';										
												}			
																		
											}
									}
											$html.='<td style="text-align:center;font-size:8px;width:5%;">'.number_format((float)$totalvpu,2).'</td>
											<td style="text-align:center;font-size:8px;width:5%;">'.number_format((float)$totalgpu,2).'</td>';
												$sumatotalgpu=(float)$sumatotalgpu+(float)$totalgpu;
												$sumatotalvpu=(float)$sumatotalvpu+(float)$totalvpu;											
											$totalgpu=0.0;
											$totalvpu=0.0;		
								}
						//var_dump($html);exit;								
									$html.='</tr>
										<tr>
										<td colspan="2" style="text-align:center;font-size:10px;">TOTALES</td>
									';
											$totales=$this->reportes_consumo_quincenal_model->obtenerTotales($fechaInicio,$fechaFin);	
											foreach ($totales as $t) {

												$html.='<td style="text-align:center;font-size:7px;">'.number_format((float)$t->TOTAL_VALOR,2).'</td>
													<td style="text-align:center;font-size:7px;">'.number_format((float)$t->TOTAL_GALONES,2).'</td>
													<td style="text-align:center;font-size:10px;"></td>';
												}
										
														
									
									/*$html.='</tr>
									<tr>
									<td colspan="2" style="text-align:center;font-size:10px;">TOTALES</td>
									<td style="text-align:center;font-size:8px;"></td>
									<td></td>
									<td></td>
									<td></td>
														
									</tr>';*/
								$html.='<td style="text-align:center;font-size:8px;">'.number_format((float)$sumatotalvpu,2).'</td>
								<td style="text-align:center;font-size:8px;">'.number_format((float)$sumatotalgpu,2).'</td>
								</tr>

								</tbody></table>';

						}


						if ($tipo=='1') {
							$html.='				<div id="foot">
					<table>
					<tr>

					<td style="text-align:center;width:100%;">__________________</td>
					<td style="text-align:center;width:100%;">__________________</td>
					</tr>
					<tr>
				
					<td style="text-align:center;font-size:11px;">'.str_replace("%20"," ",$autoriza1).'</td>
					<td style="text-align:center;font-size:11px;">'.str_replace("%20"," ",$autoriza2).'</td>
					</tr>
					</table>
					</div>';
						}
							$html.='
					</body></html>
					';      
		            $nombre_archivo = "Rerporte de consumo vales ".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('legal','landscape');
	            // $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('Arial','bold');
				$canvas->page_text(846, 25, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 10, array(0, 0, 0));
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0)); 				
			}
			
		}
		
	}
}