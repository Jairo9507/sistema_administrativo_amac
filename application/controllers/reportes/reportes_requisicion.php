<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_requisicion extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("reportes_requisicion_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Reportes","reportes/reportes_requisicion");	
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes Requisicion');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			
			$this->template->admin_render("admin/reportes/index_requisicion",$this->data);
		}
		
	}

	public function vistaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Reportes Requisicion');
			$this->data['pagetitle']=$this->page_title->show();
					$this->breadcrumbs->unshift(2,"Reportes","reportes/reportes_requisicion/vistaPDF");	
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$fechaInicio;$fechafinal;$correlativo;

			if (isset($_POST['fechaInicio'])==false or $_POST['fechaInicio']=='') {			
				$fechaInicio='0';
			} else {
				$fechaInicio=$_POST['fechaInicio'];				
			}
			if(isset($_POST['fechaFinal'])==false or $_POST['fechaFinal']=='') {
				$fechafinal='0';
			} else {
				$fechafinal=$_POST['fechaFinal'];				
			}

			if (isset($_POST['correlativo'])==false or $_POST['correlativo']=='') {
				$_POST['correlativo']='0';
				$correlativo=$_POST['correlativo'];
			} else {
				$correlativo=$_POST['correlativo'];
			}
			$datos= array(
				'fechaInicio'=>$fechaInicio,
				'fechaFinal'=>$fechafinal,
				'correlativo'=>$correlativo
			);

			$this->data['datos']=$datos;
			$this->template->admin_render("admin/reportes/reporte_requisicion_pdf",$this->data);

		}
		
	}

	public function requisicionPDF($p1,$p2,$p3)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->load->library('pdf');
            if(empty($p1) && empty($p2) && empty($p3)){
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de requisiciones ".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
           		exit;

            } else{
            	$requisiciones='';
            	$encabezado='';
            	$title='';
            	$unidad=$this->ion_auth->get_unidad_empleado();
            	if (empty($unidad)) {
            		$unidad=52;
            	}
            	if ($p1!='0' && $p2!='0') {
            		$title='Reporte De Requisiciones De '.$this->reportes_requisicion_model->obtenerUnidad($unidad);
            		$requisiciones=$this->reportes_requisicion_model->listarRequisicionesPDF($unidad,$p1,$p2);
            	} else if($p3!='0') {
            		$encabezado=$this->reportes_requisicion_model->listarRequisicionCodigo($p3);
            		$title='Requisicion de '.$encabezado[0]->CODIGO_UNIDAD." ".$encabezado[0]->unidad_solicita;
            	}

            	$empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
            	if ($empleado==null) {
            		$empleado='Admin';
            	}
						$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
						$fecha1=date('d',strtotime($p1))." de ".$meses[date('n',strtotime($p1))-1]." de ".date('Y',strtotime($p1));
						$fecha2=date('d',strtotime($p2))." de ".$meses[date('n',strtotime($p2))-1]." de ".date('Y',strtotime($p2));                   	
				$html='
			            <!DOCTYPE HTML PUBLIC ">
			            <html>
			            <head>
			            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

			            <title>Requisicion</title>
			            <style type="text/css">

			            @page {
			                margin: 1cm;
			            }


			        #header,
			        #footer {
			          position: fixed;
			          left: 0;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			          
			      }
			      #footer {
			          bottom: 0;
			          border-top: 0.1pt solid #aaa;
			      }
						#logo_clinica{
						    width:80%;
						    display: inline-block;
						    float:left;
						}

						#logo_sistema{
						    width:20%;
						    display: inline-block;
						    float:right;
						}
						#encabezado
						{
						    width:100%;
							padding-top:120px;
						    display:inline-block;
						    float:right;
						}
						body {
						   font-family: Sans-Serif;
						    margin: 0.5cm 0;
						    text-align: justify;
						}
						#tet{
						    border-radius:15px;
						    width: 13%;
						    padding: 3% 5%;
						    display: inline-block;
						    float:right;
						    text-align: center;
						    border:1px solid red;
						    color: red;
						}
						#prueba{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						#prueba2{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						 #foot{
						    width: 90%;
						    position:absolute;
						    text-align: center;
						    bottom:3%;
						    }
						table{
						    clear:both;
						    text-align: left;
						    height:auto;
						    border-collapse:collapse; 
						    table-layout:fixed;
						    width: 100%;
						}
						#cuerpo{
						    position: abosolute;
						    text-align: center;
						    bottom=12%;
						}

						table td {
						    word-wrap:break-word;
						    padding:10px 0px 10px 0px;
						}

						.page-number {
						  text-align: center;
						}

						.page-number:before {
						  content: "Página " counter(page);
						}

						hr {
						  page-break-after: always;
						  border: 0;
						}

						</style>
						</head>';						
						if($encabezado){
							$html.='
								<body style="max-height:100px;">
								<div id="header">
								<div id="logo_clinica" > 
								<img src="img/MEMBRETE1-1.jpg" style="width:1000px; height:120px;">

								</div>
								<div id="logo_sistema" >
									<div style="font-size:12px;text-align:center;">CREADO POR: '.$empleado.'</div>
									<div style="font-size:12px;text-align:center;">FECHA:'.date('d-m-Y').' </div>
								</div>
								<div id="encabezado">

								    <h3 style="text-align:center; font-size:14px;">'.$title.'</h3>
								    <p style="text-align:center;font-size:14px;position:relative;bottom:2px;">Rango: '.$fecha1.' al '.$fecha2.'</p>
								</div>
								<p></p>	
		
								</div>
										<table  style="width: 100%; table-layout: fixed; height=15%;">
									<thead style="">
										<tr>
											<td style="font-weight:bold;width:15%;">Fecha: </td>
											<td>'.$encabezado[0]->FECHA.'</td>
											<td style="font-weight:bold;width:25%;">Unidad solicitante: </td>
											<td>'.$encabezado[0]->CODIGO_UNIDAD.' '.$encabezado[0]->unidad_solicita.'</td>											
										</tr>
										<tr>
											<td style="font-weight:bold;width:15%;">C&oacute;digo: </td>
											<td>'.$encabezado[0]->CORRELATIVO.'</td>
											<td style="font-weight:bold;width:25%;">Observaciones: </td>
											<td>'.$encabezado[0]->CONCEPTO.'</td>											
										</tr>
									</thead>
											
									<div id="prueba" style="position:relative; top:2%;">
									<h4 style="text-align:center;">DETALLE</h4>
									<table style="height: 5%; border: 0.5pt solid black;">
										<thead>
										<tr>
											<th style="text-align:center;border:1px solid;width:20%;">C&oacute;digo</th>
											<th style="text-align:center;border:1px solid;width:50%;">Nombre</th>
											<th style="text-align:center;border:1px solid;width:10%;">Cantidad</th>
										</tr>
										</thead>
										<tbody>
									';
							$detalle=$this->reportes_requisicion_model->listarDetalleRequisicion($encabezado[0]->ID_REQUISICION);

								if ($detalle) {
								foreach ($detalle as $d) {
									$html.='
										<tr>
											<td style="text-align:center;border:1px solid; font-size:11px; ">'.$d->COD_PRODUCTO.'</td>
											<td style="text-align:justify;border:1px solid;font-size:11px; "> '.$d->NOMBRE_PRODUCTO.'</td>
											<td style="text-align:center;border:1px solid;font-size:11px; ">'.$d->CANTIDAD.'</td>
										</tr>
									';
								}
								$html.='
																	
								</tbody>
								</table>
								<table>
									<tr>
									<td style="text-align:center;"></td>
									<td style="text-align:center; width:40%;">___________________________</td>
									<td style="text-align:center; width:40%;">__________________________</td>

									</tr>
									<tr>
									<td style="text-align:center;"></td>
									<td style="text-align:center;">Firma y Sello</td>
									<td style="text-align:center;">Recibido por</td>
									</tr>	
								</table>
								</div>									
								<div">
								</div>';
							}									
							//var_dump($html);exit;
						}
						if ($requisiciones) {
							$html.='
								<body>
								<div id="header">
								<div id="logo_clinica" > 
								<img src="img/MEMBRETE1-1.jpg" style="width:1000px; height:120px;">

								</div>
								<div id="logo_sistema" >
									<div style="font-size:11px;text-align:center;">CREADO POR: '.$empleado.'</div>
									<div style="font-size:11px;text-align:center;">FECHA:'.date('d-m-Y').' </div>
								</div>
								<div id="encabezado" >

								    <h3 style="text-align:center; font-size:14px;">'.$title.'</h3>
								    <p style="text-align:center;font-size:14px;position:relative;bottom:2px;">Rango: '.$fecha1.' al '.$fecha2.'</p>
								</div>
								<p></p>	
		
								</div>								
									<!--<div id="header">
									<div id="logo_clinica" > 
											<img src="img/escudo_mini.png" style="width:100; height=100;">
											</div>
											<div id="encabezado">
											    <h3 style="text-align:center;height:0%;">Alcaldía Municipal de Antiguo Cuscatlan</h3>
											    <h5 style="text-align:center;height:0%;">Departamento de la Libertad, El Salvador, C.A</h5>
											    <h5 style="text-align:center;">PBX:2511-0100/2511-0121</h5>
											    <h3 style="text-align:center;">Reporte De Requisiciones De '.$this->reportes_requisicion_model->obtenerUnidad($unidad).'</h3>

											</div>
												<div style="font-size:11px;text-align:center; padding-top:50px;">Creado por: '.$empleado.'</div>
												<div style="font-size:11px;text-align:center;">Fecha:'.date('d-m-Y').' </div>					
											</div>	-->
										<table  style="width: 100%; table-layout: fixed;">
									<thead style="border-bottom:1px solid;">
										<tr>
											<th style="width:25%;">C&oacute;digo</th>
											<th style="width:12%;">Fecha de Salida</th>
											<th style="width:15%;">Unidad Solicita</th>
											<th style="width:20%;">Observaciones</th>
											<th style="width:10%;">Creada por</th>
										</tr>		
									</thead>
									';
									}
								$html.='								

								<tbody>';
									if ($requisiciones) {
										foreach ($requisiciones as $dt) {
									$html.='
									<tr>
									<td >'.$dt->CORRELATIVO.'</td>
									<td>'.date('d-m-Y',strtotime($dt->FECHA)).'</td>
									<td>'.$dt->CODIGO_UNIDAD.' '.$dt->unidad_solicita.'</td>
									<td>'.$dt->CONCEPTO.'</td>
									<td>'.$dt->USUARIO_CREACION.'</td>
									</tr>
									';
								}
									}
							$html.='
							</tbody>
							</table>';
					
					

					/*$html.='
					<div id="foot">
					<table>
					<tr>
					<td style="text-align:center;width:10%;"></td>
					<td style="text-align:center;">______________</td>
					<td style="text-align:center;">______________</td>
					<td style="text-align:center;">Creado por</td>
					</tr>
					<tr>
					<td style="text-align:center;"></td>
					<td style="text-align:center;">Firma</td>
					<td style="text-align:center;">Sello</td>
					<td style="text-align:center;">'.$empleado.'</td>
					</tr>
					</table>
					</div>'*/ 
					$html.='
					</body></html>
					';          		

		            $nombre_archivo = "Requisicion ".date('d-m-Y');
		           	$paper_size = array(0,0,1248,816);
	            $this->pdf->loadHtml($html);

	            $this->pdf->setPaper($paper_size);
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
				$fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('Arial');
				$canvas->page_text(480, 20, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 10, array(0, 0, 0));
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));                	
            	
            }
		}
		
	}	
}