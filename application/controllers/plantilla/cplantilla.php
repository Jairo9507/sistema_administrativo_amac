<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class ordenCompra extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->model('ordencompra_model');
	}

	public function index(){
		if(!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
			redirect('auth/login', 'refresh');
		} else {
			$this->page_title->push('TITULO'); //TITULO DE LA PAGINA
			$this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /* Obtener lista de empleados*/
            // $this->data['usuario']=$this->usuario_model->listarEmpleados();

            $this->template->admin_render('admin/plantilla/plantilla', $this->data);
            /* Obtener los perfiles para crear usuario */

		}
	}
}