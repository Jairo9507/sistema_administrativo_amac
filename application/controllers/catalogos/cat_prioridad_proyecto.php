<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class cat_prioridad_proyecto extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_prioridad_proyecto_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Prioridades de proyectos","catalogos/cat_prioridad_proyecto");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Prioridades');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['prioridades']=$this->cat_prioridad_proyecto_model->listarPrioridad();

            $this->template->admin_render("admin/cat_prioridad_proyecto/index",$this->data);			
		}
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nueva Prioridad");
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/cat_prioridad_proyecto/prioridad_modal",$this->data);
			
		}
		
	}	
	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Editar Prioridad");
			$this->data['pagetitle']=$this->page_title->show();
			$id=base64_decode($id);
			$this->breadcrumbs->unshift(2,"Editar Prioridad","catalogos/cat_alergia_sustancia/alergia_edit");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['prioridad']=$this->cat_prioridad_proyecto_model->listarPrioridadId($id);
			$this->template->admin_render("admin/cat_prioridad_proyecto/prioridad_edit",$this->data);			
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$prioridad=json_decode($this->input->post("prioridadPost"));
			$id=base64_decode($prioridad->id);
			$response=array(
				'response_msg'=>''				
			);
			$deletePrioridad=array(
				'ESTADO' =>0
			);
			$this->cat_prioridad_proyecto_model->deletePrioridad($id,$deletePrioridad);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
		
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$prioridad=json_decode($this->input->post("prioridadPost"));			
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($prioridad->descripcion=='') {
				
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre de la prioridad es obligatoria</div>";
			}
			if ($prioridad->id=='') {
				$registrarPrioridad=array(
					'DESCRIPCION'=>strtoupper($prioridad->descripcion)
				);
				$this->cat_prioridad_proyecto_model->savePrioridad($registrarPrioridad);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
			} else {
				$updatePrioridad=array(
					'DESCRIPCION'=>strtoupper($prioridad->descripcion)
				);
				$this->cat_prioridad_proyecto_model->updatePrioridad($prioridad->id,$updatePrioridad);
				$response['response_msg']="<script>recargar();</script>";
			}
		}
		echo json_encode($response);

	}	

}
