<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_anaqueles extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_anaqueles_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Anaqueles","catalogos/cat_anaqueles");	
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Anaqueles');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());

			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['anaqueles']=$this->cat_anaqueles_model->listarAnaqueles();
			$this->template->admin_render("admin/cat_anaquel/index",$this->data);
		}
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nuevo Anaquel');
			$this->data['pagetitle']=$this->page_title->show();			
			$this->data['estantes']=$this->cat_anaqueles_model->listarEstantes();
			$this->load->view("admin/cat_anaquel/anaquel_modal",$this->data);
		}
		
	}

	public function obtenerCodigo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo=$this->cat_anaqueles_model->obtenerCodigo();
			$cod=array("codigo"=>$codigo);
			$json=json_encode($cod);
		}
		echo $json;
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Anaqueles');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
				$this->breadcrumbs->unshift(2,"Editar Anaquel","catalogos/cat_anaqueles/editar");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());

			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$id=base64_decode($id);
			$this->data['estantes']=$this->cat_anaqueles_model->listarEstantes();
			$this->data['anaquel']=$this->cat_anaqueles_model->listaranaquelId($id);

			$this->template->admin_render("admin/cat_anaquel/anaquel_editar",$this->data);
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$anaquel=json_decode($this->input->post("AnaquelPost"));
			$id=base64_decode($anaquel->id);
			$response= array(
				'response_msg'=>''
			);
			$deleteAnaquel=array(
				'ESTADO'=>0
			);
			$this->cat_anaqueles_model->deleteAnaquel($id,$deleteAnaquel);
			$response['response_msg']='<script>recargar();</script>';
		}
		echo json_encode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$anaquel=json_decode($this->input->post("AnaquelPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($anaquel->codigo=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El codigo del estante es obligatorio</div>";
			} else if($anaquel->numero=='' or $anaquel->numero=='0') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El numero del anaquel es necesario para identificaci&oacute;n</div>";				
			} else if($anaquel->estante=='' or $anaquel->estante=='0'){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Identifique de cual estante proviene el anaquel</div>";				
			} else {
				if ($anaquel->id=='') {
					$registrarAnaquel=array(
						'DESCRIPCION'=>$anaquel->descripcion,
						'ID_ESTANTE'=>$anaquel->estante,
						'CODIGO'=>$anaquel->codigo,
						'NUMERO_ANAQUEL'=>$anaquel->numero
					);
					$this->cat_anaqueles_model->saveAnaquel($registrarAnaquel);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$updateAnaquel=array(
						'DESCRIPCION'=>$anaquel->descripcion,
						'ID_ESTANTE'=>$anaquel->estante,
						'NUMERO_ANAQUEL'=>$anaquel->numero
					);
					$this->cat_anaqueles_model->updateAnaquel($anaquel->id,$updateAnaquel);
					$response['response_msg']='<script>recargar();</script>';
				}
				
			}
			echo json_encode($response);
		}
		
	}
}