<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_otras_alergias extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_otras_alergias_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Otras Alergias","catalogos/cat_otras_alergias");			
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Otras Alergias');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['alergias']=$this->cat_otras_alergias_model->listarAlergias();

            $this->template->admin_render("admin/cat_otras_alergias/index",$this->data);			
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nuevo Estudio");
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/cat_otras_alergias/alergia_modal",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Otras Alergias');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Editar Otras Alergias","catalogos/cat_otras_alergias/editar");			
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $id=base64_decode($id);

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['alergia']=$this->cat_otras_alergias_model->listarAlergiaId($id);

            $this->template->admin_render("admin/cat_otras_alergias/alergia_edit",$this->data);			
		}

	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$alergiaId=json_decode($this->input->post("AlergiaPost"));
			$id=base64_decode($alergiaId->id);
			$response=array(
				'response_msg'=>''
			);
			$deleteAlergia=array(
				'ESTADO'=>0
			);
			$this->cat_otras_alergias_model->deleteAlergia($id,$deleteAlergia);
			$response['response_msg']="<script>recargar();</script>";
		}
		echo json_encode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$alergia=json_decode($this->input->post("AlergiaPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);

			if ($alergia->descripcion=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre de la alergia es obligatoria</div>";				
				
			}
			if ($alergia->id=='') {
				$registrarAlergia=array(
					'DESCRIPCION'=>strtoupper($alergia->descripcion)
				);
				$this->cat_otras_alergias_model->saveAlergia($registrarAlergia);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
			} else {
				$updateAlergia=array(
					'DESCRIPCION'=>strtoupper($alergia->descripcion)
				);
				$this->cat_otras_alergias_model->updateAlergia($alergia->id,$updateAlergia);
				$response['response_msg']="<script>recargar();</script>";
			}
		}
		echo json_encode($response);
	}
}