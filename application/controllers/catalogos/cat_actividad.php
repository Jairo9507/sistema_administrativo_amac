<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_actividad extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_actividad_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Actividades","catalogos/cat_actividad");	
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Actividades');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /*Obtener lista de actividades */

            $this->data['actividades'] = $this->cat_actividad_model->listarActividad();

            $this->template->admin_render("admin/cat_actividad/index",$this->data);
        }
	}
	public function guardar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$Actividad = json_decode($this->input->post("ActividadPost"));
			$response=array(
				'campo' =>'',
				'response_msg'=>''
			);
			if ($Actividad->descripcion == '') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La descripción de la actividad es obligatoria</div>";	
			}if ($Actividad->id =='' && $Actividad->descripcion != '') {
				$registroActvidad = array('DESCRIPCION' =>strtoupper($Actividad->descripcion),
										  'ESTADO'      => 1);
				$this->cat_actividad_model->guardarActividad($registroActvidad);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos<script>location.reload();</script></div>";
			}else{
				$updateActividad = array(
					'descripcion'=>strtoupper($Actividad->descripcion));
				$this->cat_actividad_model->updateActividad($Actividad->id, $updateActividad);
				$response['response_msg']='<script>recargar();</script>';

			}
		}
		echo json_encode($response);
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');		
		} else {
			$this->page_title->push("Editar Actividad");
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Actividad","admin/cat_actividad/actividad_edit");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $id=base64_decode($id);
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['actividad']=$this->cat_actividad_model->listarActividadId($id);

            $this->template->admin_render("admin/cat_actividad/actividad_edit",$this->data);
		}
	}

		public function eliminar()
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$ActividadId=json_decode($this->input->post("ActividadPost"));
			$Id=base64_decode($ActividadId->id);
			$response =array(
				'response_msg'=>''
			);
			$deleteActividad= array(
				'ESTADO' =>0
			);
			$this->cat_actividad_model->deleteActividad($Id,$deleteActividad);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
	}
}