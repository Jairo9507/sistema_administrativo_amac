<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_marca extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_marca_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Marca","catalogos/cat_marca");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Marca');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			/*Obtener Marca de vehículo */

			$this->data['marca'] = $this->cat_marca_model->listarMarca();

			$this->template->admin_render("admin/cat_marca/index",$this->data);
		}
	}

	public function guardar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$Marca = json_decode($this->input->post("MarcaPost"));
			$response=array(
				'campo' =>'',
				'response_msg'=>''
			);
			if ($Marca->descripcion == '') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La descripción de la actividad es obligatoria</div>";	
			}if ($Marca->id =='' && $Marca->descripcion != '') {
				$registroMarca = array('DESCRIPCION' =>strtoupper($Marca->descripcion),
					'ESTADO'      => 1);
				$this->cat_marca_model->guardarMarca($registroMarca);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos<script>location.reload();</script></div>";
			}else{
				$updateMarca = array(
					'descripcion'=>strtoupper($Marca->descripcion));
				$this->cat_marca_model->updateMarca($Marca->id, $updateMarca);
				$response['response_msg']='<script>recargar();</script>';

			}
		}
		echo json_encode($response);
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');		
		} else {
			$this->page_title->push("Editar Marca de vehículo");
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Marca","admin/cat_marca/marca_edit");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			$id=base64_decode($id);
			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['marca']=$this->cat_marca_model->listarMarcaId($id);

			$this->template->admin_render("admin/cat_marca/marca_edit",$this->data);
		}
	}

	public function eliminar()
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$MarcaId=json_decode($this->input->post("MarcaPost"));
			$Id=base64_decode($MarcaId->id);
			$response =array(
				'response_msg'=>''
			);
			$deleteMarca = array(
				'ESTADO' =>0
			);
			$this->cat_marca_model->deleteMarca($Id,$deleteMarca);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
	}

}