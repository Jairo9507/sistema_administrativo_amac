<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
setlocale(LC_ALL,'es_SV');
/**
 * 
 */
class cat_vehiculo extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_vehiculo_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Vehículos","catalogos/cat_vehiculo");
	}

	public function index(){
		if (!$this->ion_auth->logged_in() ) {
			redirect("auth/login","refresh");
		} else {
			$this->page_title->push('Vehículos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['vehiculos']=$this->cat_vehiculo_model->listarVehiculos();

            $this->template->admin_render("admin/cat_vehiculo/index",$this->data);

		}
	}

	public function nuevo()
	{
		if(!$this->ion_auth->logged_in() ){
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nuevo Vehículo");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['clase']=$this->cat_vehiculo_model->listarClase();
			$this->data['combustible']=$this->cat_vehiculo_model->listarCombustible();
			$this->data['marca']=$this->cat_vehiculo_model->listarMarca();
			$this->data['unidad']=$this->cat_vehiculo_model->listarUnidad();
			$this->load->view("admin/cat_vehiculo/vehiculo_modal",$this->data);
		}
	}

	public function guardar(){
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login","refresh");
		} else {
			$Vehiculo=json_decode($this->input->post("vehiculoPost"));
			$response=array(
				'response_msg'=>'',
				'campo' =>''
			);

			if($Vehiculo->id==''){
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$registrarVehiculo=array(
					'CODIGO_VEHICULO' => strtoupper($Vehiculo->codigo),
					'PLACA' => $Vehiculo->placa,
					'COLOR' => strtoupper($Vehiculo->color),
					'ANIO' => $Vehiculo->anio,
					'CAPACIDAD_TANQUE' =>$Vehiculo->capacidad_tanque,
					'ASIENTOS' =>$Vehiculo->asientos,
					'CAPACIDAD_PESO' =>$Vehiculo->capacidad_peso,
					'PESO_VEHICULO' =>$Vehiculo->peso,
					'NUMERO_MOTOR' =>strtoupper($Vehiculo->nmotor),
					'CHASIS_GRABADO' =>strtoupper($Vehiculo->nchasis),
					'CHASIS_VIN' =>strtoupper($Vehiculo->nvin),
					'CLASIFICACION'=>strtoupper($Vehiculo->clasificacion),
					'ID_COMBUSTIBLE' =>$Vehiculo->combustible,
					'USUARIO_CREACION' =>$usuario,
					'FECHA_CREACION' =>date('Y-m-d H:i:s'),
					'UNIDAD_PERTENECE' =>$Vehiculo->unidad,
					'ID_TIPO_VEHICULO' =>$Vehiculo->tipo,
					'ID_VEHICULO_MODELO' =>$Vehiculo->modelo,
					'ID_CLASE_VEHICULO' =>$Vehiculo->clase,
					'ID_VEHICULO_MARCA' =>$Vehiculo->marca,
					'ESTADO' => 1,
				);
				$this->cat_vehiculo_model->guardarVehiculo($registrarVehiculo);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
			} else {
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$updateVehiculo=array(
					'PLACA' => $Vehiculo->placa,
					'COLOR' => strtoupper($Vehiculo->color),
					'ANIO' => $Vehiculo->anio,
					'CAPACIDAD_TANQUE' =>$Vehiculo->capacidad_tanque,
					'ASIENTOS' =>$Vehiculo->asientos,
					'CAPACIDAD_PESO' =>$Vehiculo->capacidad_peso,
					'PESO_VEHICULO' =>$Vehiculo->peso,
					'NUMERO_MOTOR' =>strtoupper($Vehiculo->nmotor),
					'CHASIS_GRABADO' =>strtoupper($Vehiculo->nchasis),
					'CHASIS_VIN' =>strtoupper($Vehiculo->nvin),
					'CLASIFICACION'=>strtoupper($Vehiculo->clasificacion),			
					'ID_COMBUSTIBLE' =>$Vehiculo->combustible,
					'USUARIO_CREACION' =>$usuario,
					'FECHA_CREACION' =>date('Y-m-d H:i:s'),
					'UNIDAD_PERTENECE' =>$Vehiculo->unidad,
					'ID_TIPO_VEHICULO' =>$Vehiculo->tipo,
					'ID_VEHICULO_MODELO' =>$Vehiculo->modelo,
					'ID_CLASE_VEHICULO' =>$Vehiculo->clase,
					'ID_VEHICULO_MARCA' =>$Vehiculo->marca,
				);
				$this->cat_vehiculo_model->updateVehiculo($Vehiculo->id,$updateVehiculo);
				$response['response_msg']='<script>recargar();</script>';
			}
		}
		echo json_encode($response);
	}

public function editar($id)
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Editar Vehículo");
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Editar Vehículo","catalogos/cat_vehiculo/vehiculo_edit");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$this->data['clase']=$this->cat_vehiculo_model->listarClase();
			$this->data['tipo']=$this->cat_vehiculo_model->listarTipo();
			$this->data['modelo']=$this->cat_vehiculo_model->listarModelo();
			$this->data['combustible']=$this->cat_vehiculo_model->listarCombustible();
			$this->data['marca']=$this->cat_vehiculo_model->listarMarca();
			$this->data['unidad']=$this->cat_vehiculo_model->listarUnidad();
			$this->data['actividad']=$this->cat_vehiculo_model->listarActividad();
			$this->data['empleados']=$this->cat_vehiculo_model->listarEmpleados();
			$id=base64_decode($id);
			$this->data['vehiculo']=$this->cat_vehiculo_model->listarVehiculoId($id);
			$this->data['vales']=$this->cat_vehiculo_model->listarValesId($id);
			$this->data['misiones']=$this->cat_vehiculo_model->listarMisionesId($id);
			$this->template->admin_render("admin/cat_vehiculo/vehiculo_edit",$this->data);
		}
	}

		public function eliminar()
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$Vehiculo=json_decode($this->input->post("VehiculoPost"));
			$id=base64_decode($Vehiculo->id);
			$response= array(
				'response_msg'=>'' 
			);
			$eliminarVehiculo= array(
				'ESTADO' => 0
			);
			$this->cat_vehiculo_model->eliminarVehiculo($id,$eliminarVehiculo);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
	}

	public function vales()
	{
		if(!$this->ion_auth->logged_in() ){
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Registro de vales de gasolina");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['empleados']=$this->cat_vehiculo_model->listarEmpleados();
			$this->data['codigo']=$this->cat_vehiculo_model->listarCodigos();
			$this->load->view("admin/vales/registro_vale_modal",$this->data);
		}
	}

	public function misiones()
	{
		if(!$this->ion_auth->logged_in() ){
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Registro de misiones por vehículo");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['empleados']=$this->cat_vehiculo_model->listarEmpleados();
			$this->data['placas']=$this->cat_vehiculo_model->listarPlacas();
			$this->data['actividad']=$this->cat_vehiculo_model->listarActividad();
			$this->load->view("admin/mision/mision_modal",$this->data);
		}
	}

	public function obtenerTipoId()
	{
		if (!$this->ion_auth->logged_in()) {
				redirect("auth/login",'refresh');
			} else {
				$clase=json_decode($this->input->post("clasePost"));			
				$response=array(
					'response_msg'=>'',
					'tipo'=>'',
				);
				$tipo=$this->cat_vehiculo_model->obtenerTipoId($clase->id);
				if ($clase->id=='' or $clase->id=='0') {
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Selecci&oacute;ne una opcion valida</div>";	
					$response['tipo']="<option value=''>No contiene información</option>";								
				} else if(empty($tipo)){
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Esta clase no tiene tipo de vehículos</div>";				
					$response['tipo']="<option value=''>No contiene información</option>";	
				}else {
					$response['tipos']=$tipo;
				}
			}
		echo json_encode($response);
		
	}

	public function obtenerModeloId()
	{
		if (!$this->ion_auth->logged_in()) {
				redirect("auth/login",'refresh');
			} else {
				$marca=json_decode($this->input->post("marcaPost"));			
				$response=array(
					'response_msg'=>'',
					'modelo'=>'',
				);
				$modelo=$this->cat_vehiculo_model->obtenerModeloId($marca->id);
				if ($marca->id=='' or $marca->id=='0') {
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Selecci&oacute;ne una opcion valida</div>";	
					$response['modelo']="<option value=''>No contiene información</option>";								
				} else if(empty($modelo)){
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Esta clase no tiene tipo de vehículos</div>";				
					$response['modelo']="<option value=''>No contiene información</option>";	
				}else {
					$response['modelos']=$modelo;
				}
			}
		echo json_encode($response);
	}

	public function excel_create()
	{
        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login",'refresh');
        } else {
            $this->data['vehiculos']=$this->cat_vehiculo_model->listarVehiculosExcel();
            $this->load->view("admin/cat_vehiculo/catalogo_excel",$this->data);
        }		
	}

}