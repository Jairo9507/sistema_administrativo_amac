<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class cat_tipo_proyecto extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_tipo_proyecto_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Tipos de proyectos","catalogos/cat_tipo_proyecto");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Tipos de proyectos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['tipos']=$this->cat_tipo_proyecto_model->listarTipoProyecto();

            $this->template->admin_render("admin/cat_tipo_proyecto/index",$this->data);			
		}
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nuevo proyecto");
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/cat_tipo_proyecto/tipo_proyecto_modal",$this->data);
			
		}
		
	}	
	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Editar Tipo de proyecto");
			$this->data['pagetitle']=$this->page_title->show();
			$id=base64_decode($id);
			$this->breadcrumbs->unshift(2,"Editar Prioridad","catalogos/cat_tipo_proyecto/alergia_edit");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['tipo']=$this->cat_tipo_proyecto_model->listarTipoProyectoId($id);
			$this->template->admin_render("admin/cat_tipo_proyecto/tipo_proyecto_edit",$this->data);			
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$tipo=json_decode($this->input->post("TipoPost"));

			$id=base64_decode($tipo->id);
			$response=array(
				'response_msg'=>''				
			);
			$deleteTipoProyecto=array(
				'ESTADO' =>0
			);
			$this->cat_tipo_proyecto_model->deleteTipoProyecto($id,$deleteTipoProyecto);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
		
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$tipo=json_decode($this->input->post("TipoPost"));			
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($tipo->descripcion=='') {
				
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre del tipo de proyecto es obligatoria</div>";
			}
			if ($tipo->id=='') {
				$registrarTipoProyecto=array(
					'DESCRIPCION'=>strtoupper($tipo->descripcion)
				);
				$this->cat_tipo_proyecto_model->saveTipoProyecto($registrarTipoProyecto);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
			} else {
				$updateTipoProyecto=array(
					'DESCRIPCION'=>strtoupper($tipo->descripcion)
				);
				$this->cat_tipo_proyecto_model->updateTipoProyecto($tipo->id,$updateTipoProyecto);
				$response['response_msg']="<script>recargar();</script>";
			}
		}
		echo json_encode($response);

	}	

}
