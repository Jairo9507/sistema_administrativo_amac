<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_modelo_vehiculo extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_modelo_vehiculo_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Modelo de Vehículo","catalogos/cat_modelo_vehiculo");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Modelo');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			/*Obtener Tipos de vehículos */

			$this->data['modelo'] = $this->cat_modelo_vehiculo_model->listarModelo();
			$this->data['marca'] = $this->cat_modelo_vehiculo_model->listarMarca();

			$this->template->admin_render("admin/cat_modelo_vehiculo/index",$this->data);
		}
	}

	public function guardar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$Modelo = json_decode($this->input->post("ModeloVehiculoPost"));
			$response=array(
				'campo' =>'',
				'response_msg'=>''
			);
			if ($Modelo->descripcion == '') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La descripción de la actividad es obligatoria</div>";	
			}if ($Modelo->id =='' && $Modelo->descripcion != '') {
				$regitroModelo = array('DESCRIPCION' =>strtoupper($Modelo->descripcion),
					'ESTADO'      => 1,
					'ID_VEHICULO_MARCA' => $Modelo->id_vehiculo_marca);
				$this->cat_modelo_vehiculo_model->guardarModelo($regitroModelo);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos<script>location.reload();</script></div>";
			}else{
				$updateModelo = array(
					'descripcion'=>strtoupper($Modelo->descripcion),
					'ID_VEHICULO_MARCA' => $Modelo->id_vehiculo_marca);
				$this->cat_modelo_vehiculo_model->updateModeloVehiculo($Modelo->id, $updateModelo);
				$response['response_msg']='<script>recargar();</script>';

			}
		}
		echo json_encode($response);
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');		
		} else {
			$this->page_title->push("Editar Modelo Vehículo");
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Modelo Vehículo","catalogos/cat_modelo_vehiculo/editar");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			$id=base64_decode($id);
			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['marcas'] = $this->cat_modelo_vehiculo_model->listarMarca();
			$this->data['modelo']=$this->cat_modelo_vehiculo_model->listarModeloVehiculoId($id);


			$this->template->admin_render("admin/cat_modelo_vehiculo/modelo_vehiculo_edit",$this->data);
		}
	}

	public function eliminar()
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$ModeloVehiculoId=json_decode($this->input->post("ModeloVehiculoPost"));
			$Id=base64_decode($ModeloVehiculoId->id);
			$response =array(
				'response_msg'=>''
			);
			$deleteModelo = array(
				'ESTADO' =>0
			);
			$this->cat_modelo_vehiculo_model->deleteModeloVehiculo($Id,$deleteModelo);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
	}

}