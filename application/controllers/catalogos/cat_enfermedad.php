<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_enfermedad extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_enfermedad_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Enfermedades","catalogos/cat_enfermedad");				
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
				redirect("auth/login",'refresh');
			} else {
			$this->page_title->push('Enfermedades');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['enfermedades']=$this->cat_enfermedad_model->listarEnfermedades();

			$this->template->admin_render("admin/cat_enfermedad/index",$this->data);				
			}				
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nueva Enfermedad");
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/cat_enfermedad/enfermedad_modal",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Enfermedades');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Enferemedad","catalogos/cat_enfermedad/editar");
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Editar Enferemedad","catalogos/cat_enfermedad/editar");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $id=base64_decode($id);

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['enfermedad']=$this->cat_enfermedad_model->listarEnfermedadId($id);

			$this->template->admin_render("admin/cat_enfermedad/enfermedad_edit",$this->data);							
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$Enfermedad=json_decode($this->input->post("EnfermedadPost"));
			$id=base64_decode($Enfermedad->id);
			
			$response=array(
				'response_msg'=>''
			);
			$deleteEnfermedad=array(
				'ESTADO'=>0
			);
			$this->cat_enfermedad_model->deleteEnfermedad($id,$deleteEnfermedad);
			$response['response_msg']='<script>recargar();</script>';
		}
		echo json_encode($response);	
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$Enfermedad=json_decode($this->input->post("EnfermedadPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($Enfermedad->descripcion=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre de la Enfermedad es obligatoria</div>";
			} else if($Enfermedad->codigo=='')
			{
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El codigo de la Enfermedad es obligatoria</div>";
			}
			if ($Enfermedad->id=='') {
				$registrarEnfermedad=array(
					'DESCRIPCION'=>strtoupper($Enfermedad->descripcion),
					'CODIGO'=>strtoupper($Enfermedad->codigo)
				);
				$this->cat_enfermedad_model->saveEnfermedad($registrarEnfermedad);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
			} else {
				$updateEnferemedad=array(
					'DESCRIPCION'=>strtoupper($Enfermedad->descripcion),
					'CODIGO'=>strtoupper($Enfermedad->codigo)
				);
				$this->cat_enfermedad_model->updateEnfermedad($Enfermedad->id,$updateEnferemedad);
				$response['response_msg']="<script>recargar();</script>";
			}
			
		}
		echo json_encode($response);
	}

}