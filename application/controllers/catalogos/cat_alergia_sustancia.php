<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_alergia_sustancia extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_alergia_sustancia_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Alergias a Sustancias","catalogos/cat_alergia_sustancia");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Alergias');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['alergias']=$this->cat_alergia_sustancia_model->listarAlergiaSustancia();

            $this->template->admin_render("admin/cat_alergia_sustancia/index",$this->data);
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nueva Alergia");
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/cat_alergia_sustancia/alergia_modal",$this->data);
			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Editar Alergia");
			$this->data['pagetitle']=$this->page_title->show();
			$id=base64_decode($id);
			$this->breadcrumbs->unshift(2,"Editar Alergia","catalogos/cat_alergia_sustancia/alergia_edit");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['alergia']=$this->cat_alergia_sustancia_model->listarAlergiaSustanciaId($id);
			$this->template->admin_render("admin/cat_alergia_sustancia/alergia_edit",$this->data);			
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$alergia=json_decode($this->input->post("AlergiaPost"));
			$id=base64_decode($alergia->id);
			$response=array(
				'response_msg'=>''				
			);
			$deleteAlergia=array(
				'ESTADO' =>0
			);
			$this->cat_alergia_sustancia_model->deleteAlergiaSustancia($id,$deleteAlergia);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
		
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$alergia=json_decode($this->input->post("AlergiaPost"));			
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($alergia->descripcion=='') {
				
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre de la alergia es obligatoria</div>";
			}
			if ($alergia->id=='') {
				$registrarAlergia=array(
					'DESCRIPCION'=>strtoupper($alergia->descripcion)
				);
				$this->cat_alergia_sustancia_model->saveAlergiaSustancia($registrarAlergia);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
			} else {
				$updateAlergia=array(
					'DESCRIPCION'=>strtoupper($alergia->descripcion)
				);
				$this->cat_alergia_sustancia_model->updateAlergiaSustancia($alergia->id,$updateAlergia);
				$response['response_msg']="<script>recargar();</script>";
			}

		}
		echo json_encode($response);
	}
}