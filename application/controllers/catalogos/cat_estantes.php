<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_estantes extends Admin_Controller //el controlador debe llmarse igual que el archivo
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_estantes_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Estantes","catalogos/cat_estantes");	//La ruta es la carpeta del controlador, la cual esta en controllers->catalogos/cat_anaqueles
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Estantes');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			/*Obtener lista de actividades */

            $this->data['estantes'] = $this->cat_estantes_model->listarEstantes();
            $this->data['pasillos']=$this->cat_estantes_model->listarPasillos();

			$this->template->admin_render("admin/cat_estantes/index",$this->data); //la ruta depende de la carpeta donde se este la vista OJO-> en este caso la vista esta en la carpeta View -> admin -> cat_anaque->index
		}
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nuevo Estante');
			$this->data['pagetitle']=$this->page_title->show();
            $this->data['pasillos']=$this->cat_estantes_model->listarPasillos();
            $this->data['correlativo']=$this->cat_estantes_model->obtenerCorrelativo();
			$this->load->view("admin/cat_estantes/estante_modal",$this->data);            
		}
		
	}

	public function obtenerCodigo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo=$this->cat_estantes_model->obtenerCorrelativo();
			$cod =array('codigo'=>$codigo);
			$json=json_encode($cod);
		}
		echo $json;
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$estante=json_decode($this->input->post("EstantePost"));
			$id=base64_decode($estante->id);
			$response =array(
				'response_msg'=>''
			);
			$deleteEstante=array(
				'ESTADO'=>0
			);
			$this->cat_estantes_model->deleteEstante($id,$deleteEstante);
			$response['response_msg']='<script>recargar();</script>';
		}
		echo json_encode($response);
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Estantes');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
		$this->breadcrumbs->unshift(2,"Editar Estante","catalogos/cat_estantes/editar");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			/*Obtener lista de actividades */
			$id=base64_decode($id);
            $this->data['pasillos']=$this->cat_estantes_model->listarPasillos();
            $this->data['estante']=$this->cat_estantes_model->listarEstantesId($id);
            $this->data['correlativo']=$this->cat_estantes_model->obtenerCorrelativo();

			$this->template->admin_render("admin/cat_estantes/estante_editar",$this->data); //la ruta depende de la carpeta donde se este la vista OJO-> en este caso la vista esta en la carpeta View -> admin -> cat_anaque->index
		}
	}

	public function guardar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$Estante = json_decode($this->input->post("estantePost"));
			$response=array(
				'campo' =>'',
				'response_msg'=>''
			);
			if ($Estante->id =='') {
				$registroEstante = array(
					'DESCRIPCION' =>strtoupper($Estante->descripcion),
					'CODIGO_ESTANTE' => $Estante->codigo,
					'ESTADO'      => 1,
					'ID_CAT_PASILLO'=>$Estante->pasillo
				);
				$this->cat_estantes_model->guardarEstante($registroEstante);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos<script>location.reload();</script></div>";
			}else{
				$updateEstante = array(
					'DESCRIPCION'=>strtoupper($Estante->descripcion),
					'CODIGO_ESTANTE' => $Estante->codigo,
					'ID_CAT_PASILLO'=>$Estante->pasillo					
				);

				$this->cat_estantes_model->updateEstante($Estante->id, $updateEstante);
				$response['response_msg']='<script>recargar();</script>';

			}
		}
		echo json_encode($response);
	}
}