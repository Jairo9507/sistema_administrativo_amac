<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_unidad_medida extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_unidad_medida_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Unidades de medida","catalogos/cat_unidad_medida");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Unidades de medida');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['unidades']=$this->cat_unidad_medida_model->listarUnidadesMedida();

            $this->template->admin_render("admin/cat_unidad_medida/index",$this->data);
			
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nueva Unidad');
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/cat_unidad_medida/unidad_medida_modal",$this->data);
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');		
		} else {
			$this->page_title->push("Editar Unidad");
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Unidad","admin/cat_unidad_medida/unidad_medida_edit");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $id=base64_decode($id);
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['unidad']=$this->cat_unidad_medida_model->listarUnidadMedidaId($id);

            $this->template->admin_render("admin/cat_unidad_medida/unidad_medida_edit",$this->data);
		}
		
	}

	public function eliminar()
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$UnidadId=json_decode($this->input->post("UnidadPost"));
			$Id=base64_decode($UnidadId->id);
			$response =array(
				'response_msg'=>''
			);
			$deleteUnidad= array(
				'ESTADO' =>0
			);
			$this->cat_unidad_medida_model->deleteUnidadMedida($Id,$deleteUnidad);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$Unidad=json_decode($this->input->post("UnidadPost"));
			$response=array(
				'campo' =>'',
				'response_msg'=>''
			);
			if($Unidad->descripcion=='')
			{
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La descripci&oacute;n de la unidad es obligatoria</div>";				
			}
			if ($Unidad->id=='') {
				$registrarUnidad= array(
					'descripcion' =>strtoupper($Unidad->descripcion)
				);
				$this->cat_unidad_medida_model->saveUnidadMedida($registrarUnidad);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";

			} else {
				$updateUnidadMedida= array(
					'descripcion'=>strtoupper($Unidad->descripcion)
				);
				$this->cat_unidad_medida_model->updateUnidadMedida($Unidad->id,$updateUnidadMedida);
				$response['response_msg']='<script>recargar();</script>';
			}
			
		}
		echo json_encode($response);
	}
}