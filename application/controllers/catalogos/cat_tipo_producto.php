<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_tipo_producto extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_tipo_producto_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Tipo de productos","catalogos/cat_tipo_producto");		

	}

	public function index()
	{
	 	if (!$this->ion_auth->logged_in()) {
	 		redirect("auth/login",'refresh');
	 	} else {
			$this->page_title->push('Tipos de productos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['tipos']=$this->cat_tipo_producto_model->listarTipos();

            $this->template->admin_render("admin/cat_tipo_producto/index",$this->data);
	 		
	 	}
	 	
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nuevo tipo");
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/cat_tipo_producto/tipo_producto_modal",$this->data);

		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Editar tipo");
			$this->data['pagetitle']=$this->page_title->show();
			$id=base64_decode($id);
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['tipo']=$this->cat_tipo_producto_model->listarTipoProductoId($id);

            $this->template->admin_render("admin/cat_tipo_producto/tipo_producto_edit",$this->data);			
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$tipoId=json_decode($this->input->post("TipoPost"));
			$id=base64_decode($tipoId->id);
			$response= array(
				'response_msg'=>''
			);
			$deleteTipo=array(
				'ESTADO'=>0
			);
			$this->cat_tipo_producto_model->deleteTipoProducto($id,$deleteTipo);
			$response['response_msg']='<script>recargar();</script>';
		}
		echo json_decode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$tipo=json_decode($this->input->post("TipoPost"));
			$response =array(
				'response_msg'=>'',
				'campo'=>''
			);
			if($tipo->descripcion==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La descripci&oacute;n de la unidad es obligatoria</div>";
			}

			if($tipo->id=='')
			{
				$registrarTipo=array(
					'descripcion'=> strtoupper($tipo->descripcion)
				);
				$this->cat_tipo_producto_model->saveTipoProducto($registrarTipo);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";				
			} else {
				$updateTipo=array(
					'descripcion' =>strtoupper($tipo->descripcion)
				);
				$this->cat_tipo_producto_model->updateTipoProducto($tipo->id,$updateTipo);
				$response['response_msg']='<script>recargar();</script>';
			}

		}
		echo json_encode($response);
	}

	
}