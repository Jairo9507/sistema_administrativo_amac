<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_combustible extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_combustible_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Combustible","catalogos/cat_combustible");
	}

		public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Combustibles');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /*Obtener lista de Clase de Vehículos */

            $this->data['combustible'] = $this->cat_combustible_model->listarCombustible();

            $this->template->admin_render("admin/cat_combustible/index",$this->data);
        }
	}

	public function guardar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$Combustible = json_decode($this->input->post("CombustiblePost"));
			$response=array(
				'campo' =>'',
				'response_msg'=>''
			);
			if ($Combustible->descripcion == '') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La descripción de la actividad es obligatoria</div>";	
			}if ($Combustible->id =='' && $Combustible->descripcion != '') {
				$registroCombustible = array('DESCRIPCION' =>strtoupper($Combustible->descripcion),
										  'ESTADO'      => 1);
				$this->cat_combustible_model->guardarCombustible($registroCombustible);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos<script>location.reload();</script></div>";
			}else{
				$updateCombustible = array(
					'descripcion'=>strtoupper($Combustible->descripcion));
				$this->cat_combustible_model->updateCombustible($Combustible->id, $updateCombustible);
				$response['response_msg']='<script>recargar();</script>';

			}
		}
		echo json_encode($response);
	}

		public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');		
		} else {
			$this->page_title->push("Editar Tipo Combustible");
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Actividad","admin/cat_combustible/combustible_edit");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $id=base64_decode($id);
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['combustible']=$this->cat_combustible_model->listarCombustibleId($id);

            $this->template->admin_render("admin/cat_combustible/combustible_edit",$this->data);
		}
	}

	public function eliminar()
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$CombustibleId=json_decode($this->input->post("CombustiblePost"));
			$Id=base64_decode($CombustibleId->id);
			$response =array(
				'response_msg'=>''
			);
			$deleteCombustible= array(
				'ESTADO' =>0
			);
			$this->cat_combustible_model->deleteCombustible($Id,$deleteCombustible);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
	}


}