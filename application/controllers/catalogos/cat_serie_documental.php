<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class cat_serie_documental extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_serie_documental_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Series documentales","catalogos/cat_serie_documental");	
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Series Documentales');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());

			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$this->data['series']=$this->cat_serie_documental_model->listarSeries();

			$this->template->admin_render("admin/cat_serie_documental/index",$this->data);			
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nueva serie Documental');
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/cat_serie_documental/serie_modal",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Series Documentales');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
		$this->breadcrumbs->unshift(2,"Editar Serie","catalogos/cat_serie_documental/editar");				
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			$id=base64_decode($id);
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$this->data['serie']=$this->cat_serie_documental_model->listarSerieId($id);

			$this->template->admin_render("admin/cat_serie_documental/editar_serie",$this->data);			
		}

	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$serie=json_decode($this->input->post("SeriePost"));
			$id=base64_decode($serie->id);
			$response =array(
				'response_msg'=>''
			);
			$deleteSerie=array(
				'ESTADO'=>0
			);
			$this->cat_serie_documental_model->deleteSerie($id,$deleteSerie);
			$response['response_msg']="<script>recargar();</script>";
		}
		echo json_encode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$serie=json_decode($this->input->post("SeriePost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($serie->nombre=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre de la serie documental es obligatorio</div>";
			}/*else if($serie->codigo==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El c&oacute;digo de la serie documental es obligatorio</div>";				
			}*/ else {
				if ($serie->id=='') {
					$registrarSerie= array(
						'NOMBRE'=>strtoupper($serie->nombre),
						'DESCRIPCION'=>$serie->descripcion,
						'CODIGO'=>$serie->codigo,
						'TIPOLOGIA'=>$serie->tipologia
					);
					$this->cat_serie_documental_model->saveSerie($registrarSerie);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";				
				} else {
					$updateSerie=array(
						'NOMBRE'=>strtoupper($serie->nombre),
						'DESCRIPCION'=>$serie->descripcion,
						'CODIGO'=>$serie->codigo,
						'TIPOLOGIA'=>$serie->tipologia,
					);
					$this->cat_serie_documental_model->updateSerie($serie->id,$updateSerie);
					$response['response_msg']="<script>recargar();</script>";
				}
				
			}
			echo json_encode($response);
		}
		
	}
}