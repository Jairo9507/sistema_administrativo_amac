<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 

 */
class cat_producto extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_producto_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Productos","catalogos/cat_producto");
	}

	public function index(){
		if (!$this->ion_auth->logged_in() ) {
			redirect("auth/login","refresh");
		} else {
			$this->page_title->push('Productos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
	           $unidad=$this->ion_auth->get_unidad_empleado();
	           if ($unidad==null) {
	           		$unidad=42;
	           }
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $this->data['productos']=$this->cat_producto_model->listarProductos($unidad);

            $this->template->admin_render("admin/cat_producto/index",$this->data);

		}
	}

	public function nuevo()
	{
		if(!$this->ion_auth->logged_in() ){
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Nuevo Producto");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['proveedores']=$this->cat_producto_model->listarProveedor();
			$this->data['tipos']=$this->cat_producto_model->listarTipoproducto();
			$this->data['cuentas']=$this->cat_producto_model->listarCuentaAdministrativa();
			$this->data['medidas']=$this->cat_producto_model->listarUnidadMedida();
			$this->load->view("admin/cat_producto/producto_modal",$this->data);
		}
	}

	public function editar($id)
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Editar Producto");
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Editar Producto","catalogos/cat_producto/producto_edit");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$this->data['proveedores']=$this->cat_producto_model->listarProveedor();
			$this->data['tipos']=$this->cat_producto_model->listarTipoproducto();
			$this->data['cuentas']=$this->cat_producto_model->listarCuentaAdministrativa();
			$this->data['medidas']=$this->cat_producto_model->listarUnidadMedida();
			$id=base64_decode($id);
			$this->data['producto']=$this->cat_producto_model->listarProductoId($id);
			$this->data['movimientos']=$this->cat_producto_model->listarMovimientosProductos($id);
			$this->data['inventarios']=$this->cat_producto_model->listarInventarioProductos($id);
			$this->template->admin_render("admin/cat_producto/producto_edit",$this->data);
		}
	}

	public function eliminar()
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$producto=json_decode($this->input->post("productoPost"));
			$id=base64_decode($producto->id);
			$response= array(
				'response_msg'=>'' 
			);
			$deleteProducto= array(
				'ACTIVO' => 0
			);
			$this->cat_producto_model->deleteProducto($id,$deleteProducto);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
	}

	public function saveMovimiento()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$movimiento=json_decode($this->input->post("MovimientoPost"));

			$response= array(
				'response_msg'=>''
			);
			$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
						
			$updateMovimiento=array(
				'CANTIDAD'=>$movimiento->cantidad,
				'COSTO_UNITARIO'=>$movimiento->precio,
				'USUARIO_MODIFICACION'=>$usuario,
				'FECHA_MODIFICACION'=>date('Y-m-d H:i:s')
			);
			$this->cat_producto_model->updateMovimiento($movimiento->id,$updateMovimiento);
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";

		}
		echo json_encode($response);
	}

	public function saveInventario()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$inventario=json_decode($this->input->post("InventarioPost"));
			$response =array(
				'response_msg'=>''
			);
			$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
			$updateInventario=array(
				'CANTIDAD'=>$inventario->cantidad,
				'PRECIO'=>$inventario->precio,
				'USUARIO_MODIFICACION'=>$usuario,
				'FECHA_MODIFICACION'=>date('Y-m-d H:i:s')
			);			
			$this->cat_producto_model->updateInventario($inventario->id,$updateInventario);
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
		}
		echo json_encode($response);
		
	}

	public function obtenerCodigo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo = $this->cat_producto_model->obtenerCodigo();
			$cod =array('codigo'=>$codigo);

			$json=json_encode($cod);
			
		}
		echo $json;
		
	}	

	public function save(){
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login","refresh");
		} else {
			$Producto=json_decode($this->input->post("productoPost"));
			$response=array(
				'response_msg'=>'',
				'campo' =>''
			);
			$existe=$this->cat_producto_model->obtenerProductoNombre($Producto->nombre,$Producto->cuenta,$Producto->cuenta_contable);
			if($Producto->nombre==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre del producto es obligatoria</div>";
			}  else if($Producto->cuenta=='' || $Producto->cuenta=="0") {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La cuenta presupuestaria del producto es obligatoria</div>";
			} else if($Producto->cuenta_contable=='' || $Producto->cuenta_contable=='0') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La cuenta contable del producto es obligatoria</div>";
			} else if(!empty($existe)){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Este producto ya ha sido registrado bajo la misma cuenta presupuestaria y contable</div>";
			} else {

				if($Producto->id==''){
					$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
					$unidad=$this->ion_auth->get_unidad_empleado();
					if ($Producto->unidad=='') {
						$Producto->unidad=null;
					} 
					$registrarProducto=array(
						'COD_PRODUCTO' => $Producto->codigo,
						'NOMBRE_PRODUCTO' => strtoupper($Producto->nombre),
						'MINIMO' => $Producto->minimo,
						'USUARIO_CREACION' =>$usuario,
						'FECHA_CREACION' =>date('Y-m-d H:i:s'),
						'INV_CAT_CUENTA_CONTABLE' =>$Producto->cuenta_contable,
						'ID_CAT_CUENTA' =>$Producto->cuenta,
						'ID_CAT_UNIDAD_MEDIDA' =>$Producto->unidad,
						'id_unidad_administrativa'=>$unidad
					);
					$this->cat_producto_model->saveProducto($registrarProducto);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
					if ($Producto->unidad=='') {
						$Producto->unidad=null;
					} 
					$updateProducto=array(
						'NOMBRE_PRODUCTO' => strtoupper($Producto->nombre),
						'MINIMO' => $Producto->minimo,
						'USUARIO_MODIFICACION' =>$usuario,
						'FECHA_MODIFICACION' =>date('Y-m-d H:i:s'),
						'INV_CAT_CUENTA_CONTABLE' =>$Producto->cuenta_contable,
						'ID_CAT_CUENTA' =>$Producto->cuenta,
						'ID_CAT_UNIDAD_MEDIDA' =>$Producto->unidad,
					);
					$this->cat_producto_model->updateProducto($Producto->id,$updateProducto);
					$response['response_msg']='<script>recargar();</script>';
				}				
			}

		}
		echo json_encode($response);
	}

	public function vistaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Lista de productos");
			$this->data['pagetitle']=$this->page_title->show();

			$this->load->view('admin/cat_producto/lista_pdf',$this->data); 			
		}
		
	}
	public function vistaLevantaminetoPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Lista de productos");
			$this->data['pagetitle']=$this->page_title->show();

			$this->load->view('admin/cat_producto/levantamiento_pdf',$this->data); 			
		}
		
	}		

	public function listaPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->load->library("pdf");
	           $unidad=$this->ion_auth->get_unidad_empleado();
	           if ($unidad==null) {
	           		$unidad=52;
	           }
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $detalle=$this->cat_producto_model->listarPDF($unidad);

            if (empty($detalle)) {
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de productos de".$this->cat_producto_model->nombreUnidad($unidad)."".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
           		exit;            	
            } else {
				$empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
            	if ($empleado==null) {
            		$empleado='Admin';
            	}            	
				$html='
			            <!DOCTYPE HTML PUBLIC ">
			            <html>
			            <head>
			            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

			            <title>Lista de productos</title>
			            <style type="text/css">

			            @page {
			                margin: 1.2cm;
			            }


			        #header,
			        #footer {
			          position: fixed;
			          left: 0;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			          
			      }
			      #footer {
			          bottom: 0;
			          border-top: 0.1pt solid #aaa;
			      }
						#logo_clinica{
						    width:80%;
						    display: inline-block;
						    float:left;
						}

						#logo_sistema{
						    width:20%;
						    display: inline-block;
						    float:right;
						}
						#encabezado
						{
						    width:100%;
							padding-top:140px;
						    display:inline-block;
						    float:right;
						}
						body {
						   font-family: Sans-Serif;
						    margin: 0.5cm 0;
						    text-align: justify;
						}
						#tet{
						    border-radius:15px;
						    width: 13%;
						    padding: 3% 5%;
						    display: inline-block;
						    float:right;
						    text-align: center;
						    border:1px solid red;
						    color: red;
						}
						#prueba{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						#prueba2{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						 #foot{
						    width: 90%;
						    position:absolute;
						    text-align: center;
						    bottom:-1%;
						    }
						table{
						    clear:both;
						    text-align: left;
						    height:auto;
						    border-collapse:collapse; 
						    table-layout:fixed;
						    width: 100%;
						}
						#cuerpo{
						    position: abosolute;
						    text-align: center;
						    bottom=12%;
						}

						table td {
						    word-wrap:break-word;
						    table-layout:auto;
						}



						.page-number {
						  text-align: center;
						}


						hr {
						  page-break-after: always;
						  border: 0;
						}

						</style>
						</head>';
						$html.='
						<body>
							<div id="header">
							<div id="logo_clinica" > 
							<img src="img/MEMBRETE1-1.jpg" style="width:650; height=100;">

							</div>
							<div id="logo_sistema">
								<div style="font-size:12px;text-align:center;">Creado por: '.$empleado.'</div>
								<div style="font-size:12px;text-align:center;">Fecha:'.date('d-m-Y H:i:s').' </div>
							</div>
							<div id="encabezado">

							    <h3 style="text-align:center;">CATALOGO DE PRODUCTOS DE '.$this->cat_producto_model->nombreUnidad($unidad).'</h3>
							</div>
							<p></p>	
	
							</div>
							<table  style="width: 100%;">
								<thead  style="border-bottom:1px solid;">
								<tr>
									<th style="width:15%; text-align:center;">Cuenta Presupuestaria</th>
									<th style="width:10%; text-align:center;">Cuenta Contable</th>
									<th style="width:28%; text-align:center;">C&oacute;digo</th>
									<th style="width:50%; text-align:left;">Nombre De Producto</th>
									<th style="width:12%; text-align:center;">Unidad De Medida</th>			
								</tr>
								</thead>
								<tbody>';
								foreach ($detalle as $dt) {
									$html.='
									<tr >
									<td style="text-align:center">'.$dt->NOMBRE_CUENTA.'</td>						
									<td style="text-align:center">'.$dt->TIPO.'</td>
									<td style="text-align:center">'.$dt->COD_PRODUCTO.'</td>			
									<td style="text-align:justify;">'.$dt->NOMBRE_PRODUCTO.'</td>
									<td style="text-align:center">'.$dt->UNIDAD.'<td>
									</tr>
									';
								}
							$html.='
							</tbody>
							</table>';
					
					$html.='
					<div id="foot">
					<table>
					<tr>
					<td style="text-align:center;width:10%;"></td>
					<td style="text-align:center;">______________</td>
					<td style="text-align:center;">______________</td>
					</tr>
					<tr>
					<td style="text-align:center;"></td>
					<td style="text-align:center;">Firma</td>
					<td style="text-align:center;">Sello</td>
					</tr>
					</table>
					</div>
					</body></html>
					';          

				$nombre_archivo = "Catalogo de productos de ".$this->cat_producto_model->nombreUnidad($unidad)."".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('legal','landscape');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
				$fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('sans-serif');
				$canvas->page_text(845, 20, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 12, array(0, 0, 0));	            
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));            	
            }
		}
		
	}

	public function levantamientoPDF()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->load->library("pdf");
	           $unidad=$this->ion_auth->get_unidad_empleado();
	           if ($unidad==null) {
	           		$unidad=52;
	           }
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $detalle=$this->cat_producto_model->levantamientoFisico($unidad);
            $fecha = date('d-m-Y H:i:s');

            if (empty($detalle)) {
				$html="Ha ocurrido un error al realizar la operación";
				$nombre_archivo = "reporte de productos de".$this->cat_producto_model->nombreUnidad($unidad)."".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
           		exit;            	
            } else {
				$empleado=$this->usuario_model->listarNombreEmpleado($this->ion_auth->get_user_id());
            	if ($empleado==null) {
            		$empleado='Admin';
            	}            	
				$html='
			            <!DOCTYPE HTML PUBLIC ">
			            <html>
			            <head>
			            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

			            <title>Reporte de consumo</title>
			            <style type="text/css">

			            @page {
			                margin: 1.3cm;
			            }


			        #header,
			        #footer {
			          position: fixed;
			          left: 0;
			          right: 0;
			          font-size: 0.9em;
			      }
			      #header {
			          top: 0;
			          
			      }
			      #footer {
			          bottom: 0;
			          border-top: 0.1pt solid #aaa;
			      }
						#logo_clinica{
						    width:80%;
						    display: inline-block;
						    float:left;
						}

						#logo_sistema{
						    width:20%;
						    display: inline-block;
						    float:right;
						}
						#encabezado
						{
						    width:100%;
							padding-top:140px;
						    display:inline-block;
						    float:right;
						}
						body {
						   font-family: Sans-Serif;
						    margin: 0.5cm 0;
						    text-align: justify;
						}
						#tet{
						    border-radius:15px;
						    width: 13%;
						    padding: 3% 5%;
						    display: inline-block;
						    float:right;
						    text-align: center;
						    border:1px solid red;
						    color: red;
						}
						#prueba{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						#prueba2{
						    position: relative;
						    top: 40%;
						    text-align:center;
						    left: 2%;
						  }
						 #foot{
						    width: 90%;
						    position:absolute;
						    text-align: center;
						    bottom:-5%;
						    padding:-5px;
						    }
						table{
						    clear:both;
						    text-align: left;
						    height:auto;
						    border-collapse:collapse; 
						    table-layout:fixed;
						    width: 100%;
						}
						#cuerpo{
						    position: abosolute;
						    text-align: center;
						    bottom=12%;
						}

						table td {
						    word-wrap:break-word;
						    padding:10px 0px 10px 0px;

						}

						.page-number {
						  text-align: center;
						}

						.page-number:before {
						  content: "Página" counter(page);
						}

						hr {
						  page-break-after: always;
						  border: 0;
						}

						</style>
						</head>';
						$html.='
						<body>
							<div id="header">
							<div id="logo_clinica" > 
							<img src="img/MEMBRETE1-1.jpg" style="width:450; height=100;">

							</div>
							<div id="logo_sistema">
								<div style="font-size:12px;text-align:center;">Creado por: '.$empleado.'</div>
								<div style="font-size:12px;text-align:center;">Fecha:'.$fecha.' </div>
							</div>
							<div id="encabezado">

							    <h3 style="text-align:center;">LEVANTAMIENTO FISICO DE  '.$this->cat_producto_model->nombreUnidad($unidad).'</h3>
							</div>
							<p></p>	
	
							</div>';
						
								// var_dump($detalle);exit;
							$html.='<table  style="width: 100%; table-layout: fixed;">
								<thead style="border-bottom:1px solid;">
								<tr>
									<th style="width:8%;text-align:center;">Cuenta Presupuestaria</th>
									<th style="width:8%;text-align:center;">Código Contable</th>
									<th style="width:15%;text-align:center;">Nombre del Producto</th>
									<th style="width:8%;text-align:center;">Cantidad</th>
									<th style="width:8%;text-align:center;">Existencia Física</th>
									<th style="width:8%;text-align:center;">Diferencia</th>
								</tr>
								</thead>
								<tbody>';
								 
								foreach ($detalle as $det) {
									// var_dump($det->COD_CUENTA);exit;
									$html.='
									<tr>
									<td style="text-align:center;font-size:11px;">'.$det->COD_CUENTA.'</td>
									<td style="text-align:center;font-size:11px;">'.$det->CODIGO.'</td>
									<td style="text-align:justify;font-size:11px;">'.$det->NOMBRE_PRODUCTO.'</td>
									<td style="text-align:center;font-size:11px;">'.$det->EXISTENCIAS_SISTEMA.'</td>
									<td style="text-align:center;font-size:11px;">__________</td>
									<td style="text-align:center;font-size:11px;">__________</td>
									</tr>';
							}
							$html.='
							</tbody>
							</table>';
					$html.='
					<div id="foot">
					</div>
					</body></html>
					';          
					// var_dump($detalle);exit;					
		            $nombre_archivo = "Reporte de consumo ".date('d-m-Y');
	            $this->pdf->loadHtml($html);
	            $this->pdf->setPaper('A4');
	            $this->pdf->set_option('defaultMediaType', 'all');
	            $this->pdf->set_option('isFontSubsettingEnabled', true);            
	            $this->pdf->render();
	            $fontMetrics = $this->pdf->getFontMetrics();
				$canvas = $this->pdf->get_canvas();
				$font = $fontMetrics->getFont('Arial','bold');
				$canvas->page_text(475, 28, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 10, array(0, 0, 0));
	            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));            	
            }
		}
		
	}


}