<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_estudio_clinico extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_estudio_clinico_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Estudios Clinicos","catalogos/cat_estudio_clinico");				
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Estudios');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['estudios']=$this->cat_estudio_clinico_model->listarEstudios();

            $this->template->admin_render("admin/cat_estudio_clinico/index",$this->data);			
		}
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else{
			$this->page_title->push("Nuevo Estudio");
			$this->data['pagetitle']=$this->page_title->show();
			$this->load->view("admin/cat_estudio_clinico/estudio_modal",$this->data);

		}
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Estudio');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Editar Estudio Clinico","catalogos/cat_estudio_clinico/editar");	
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            $id=base64_decode($id);

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['estudio']=$this->cat_estudio_clinico_model->listarEstudioId($id);

            $this->template->admin_render("admin/cat_estudio_clinico/estudio_editar",$this->data);		
        }
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$estudioId=json_decode($this->input->post("EstudioPost"));
			$id=base64_decode($estudioId->id);
			$response= array(
				'response_msg'=>''
			);
			$deleteEstudio= array(
				'ESTADO'=>0
			);
			$this->cat_estudio_clinico_model->deleteEstudio($id,$deleteEstudio);
			$response['response_msg']="<script>recargar();</script>";
		}
		echo json_encode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$estudio=json_decode($this->input->post("EstudioPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);

			if ($estudio->descripcion=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre del estudio clinico es obligatorio</div>";				
			} else if($estudio->tipo==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El tipo de estudio  es obligatorio</div>";
			}

			if ($estudio->id=='') {
				$registrarEstudio=array(
					'DESCRIPCION'=>strtoupper($estudio->descripcion),
					'TIPO'=>strtoupper($estudio->tipo)
				);
				$this->cat_estudio_clinico_model->saveEstudio($registrarEstudio);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
			} else {
				$updateEstudio=array(
					'DESCRIPCION'=>strtoupper($estudio->descripcion),
					'TIPO'=>strtoupper($estudio->tipo)
				);
				$this->cat_estudio_clinico_model->updateEstudio($estudio->id,$updateEstudio);
				$response['response_msg']="<script>recargar();</script>";
			}
			
			
		}
		echo json_encode($response);
	}
}