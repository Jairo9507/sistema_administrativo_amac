<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_pasillos extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_pasillos_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Pasillos","catalogos/cat_pasillos");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Pasillos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());

			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['pasillos']=$this->cat_pasillos_model->listarPasillos();
			$this->template->admin_render("admin/cat_pasillos/index",$this->data);						
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nuevo Pasillo');
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['unidades']=$this->cat_pasillos_model->listarUnidades();
			$this->load->view("admin/cat_pasillos/pasillos_modal",$this->data);
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Pasillo');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Editar Pasillo","catalogos/cat_pasillos/editar");	
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());

			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$id=base64_decode($id);
			$this->data['pasillo']=$this->cat_pasillos_model->listarPasilloId($id);
			$this->data['unidades']=$this->cat_pasillos_model->listarUnidades();			

			$this->template->admin_render("admin/cat_pasillos/pasillos_editar",$this->data);						
		
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$pasillo=json_decode($this->input->post("PasilloPost"));
			$id=base64_decode($pasillo->id);
			$response =array(
				'response_msg'=>''
			);
			$deletePasillo=array(
				'ESTADO'=>0
			);
			$this->cat_pasillos_model->deletePasillo($id,$deletePasillo);
			$response['response_msg']='<script>recargar();</script>';
		}
		echo json_encode($response);
	}

	public function obtenerCodigo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo=$this->cat_pasillos_model->obtenerCodigo();
			$cod=array('codigo'=>$codigo);
			$json=json_encode($cod);
		}
		
		echo $json;
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$pasillo=json_decode($this->input->post("PasilloPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($pasillo->codigo=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El c&oacute;digo del pasillo es obligatorio</div>";
				
			} else if($pasillo->numero=='' or $pasillo->numero=='0') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El codigo del pasillo es obligatorio</div>";
			} else {
				if ($pasillo->id=='') {
					$registrarPasillo=array(
						'DESCRIPCION'=>$pasillo->descripcion,
						'CODIGO'=>$pasillo->codigo,
						'NUMERO_ESTANTES'=>$pasillo->numero
					);
					$this->cat_pasillos_model->savePasillo($registrarPasillo);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$updatePasillo=array(
						'DESCRIPCION'=>$pasillo->descripcion,
						'CODIGO'=>$pasillo->codigo,
						'NUMERO_ESTANTES'=>$pasillo->numero				
					);
					$this->cat_pasillos_model->updatePasillo($pasillo->id,$updatePasillo);
					$response['response_msg']="<script>recargar();</script>";
				}
				
			}
			echo json_encode($response);
		}
		
	}
}