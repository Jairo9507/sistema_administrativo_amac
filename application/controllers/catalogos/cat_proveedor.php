<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_proveedor extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_proveedor_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Proveedores","catalogos/cat_proveedor");
	}

	public function index(){
		if (!$this->ion_auth->logged_in() ) {
			redirect("auth/login",'refresh');
		} else{
			$this->page_title->push('Proveedores');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['proveedores']=$this->cat_proveedor_model->listarProveedores();

            $this->template->admin_render('admin/cat_proveedor/index',$this->data);
		}
	}

	public function nuevo() {
		if(!$this->ion_auth->logged_in() ){
			redirect("auth/login","refresh");
		} else {
			$this->page_title->push("Nuevo Proveedor");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['rubro']=$this->cat_proveedor_model->listarRubros();

			$this->load->view("admin/cat_proveedor/proveedor_modal",$this->data);
		}
	}

	public function editar($id){
		if (!$this->ion_auth->logged_in() ) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Editar Proveedor");
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Proveedor","catalogos/cat_proveedor/proveedor_edit");
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$id= base64_decode($id);	
			$this->data['proveedor']=$this->cat_proveedor_model->listarProveedorId($id);
			$this->data['rubro']=$this->cat_proveedor_model->listarRubros();
			$this->template->admin_render("admin/cat_proveedor/proveedor_edit",$this->data);
		}
	}

	public function eliminar(){
		if(!$this->ion_auth->logged_in() ){
			redirect("auth/login",'refresh');
		} else {
			$proveedor=json_decode($this->input->post("proveedorPost"));
			
			$id=base64_decode($proveedor->id);
			$response= array(
				'response_msg'=>'' 
			);
			$deleteProveedor= array(
				'ESTADO' => 0
			);
			$this->cat_proveedor_model->deleteProveedor($id,$deleteProveedor);
			$response['response_msg']='<script>recargar();</script>';
			
		}
		echo json_encode($response);		
	}

	public function save(){
		if (!$this->ion_auth->logged_in() ) {
			redirect("auth/login",'refresh');
		} else {
			$Proveedor=json_decode($this->input->post("proveedorPost"));
			$response=array(
				'response_msg'=>'',
				'campo' =>''
			);
			if($Proveedor->nombre==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre del proveedor es obligatorio</div>";

			} else if($Proveedor->telefono=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El telefono del proveedor es obligatorio</div>";

			} else if(strlen($Proveedor->telefono)<8){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El telefono debe llevar 8 d&iacute;gitos</div>";

			} else if($Proveedor->fax!='' && strlen($Proveedor->fax)<8){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El fax debe llevar 8 d&iacute;gitos</div>";
			} else if($Proveedor->contacto_celular!='' && strlen($Proveedor->contacto_celular)<8){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El celular del contacto debe llevar 8 d&iacute;gitos</div>";
			}else if($Proveedor->contacto_telefono!='' && strlen($Proveedor->contacto_telefono)<8){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El celular del contacto debe llevar 8 d&iacute;gitos</div>";
			} else if($Proveedor->nit==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nit del proveedor es obligatorio</div>";

			} else if(!empty($encontrado=$this->cat_proveedor_model->buscarProveedor($Proveedor->nit)) && $Proveedor->id==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nit ".$Proveedor->nit." ya esta registrado con el Proveedor ".$encontrado[0]->NOMBRE_PROVEEDOR."</div>";
			} else {
				if($Proveedor->id=='')
				{
					if ($Proveedor->rubro=='') {
						$Proveedor->rubro=null;
					}
					$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
					$registarProveedor=array(
						'ID_CAT_RUBRO' => $Proveedor->rubro,
						'NOMBRE_PROVEEDOR' => ucwords($Proveedor->nombre),
						'DIRECCION' => $Proveedor->direccion,
						'TELEFONO' => $Proveedor->telefono,
						'FAX' => $Proveedor->fax,
						'CORREO' => $Proveedor->correo,
						'PAGINA_WEB' => $Proveedor->web,
						'PRIMER_NOMBRE_PERSONA_CONTACTO'=> ucwords($Proveedor->contacto_primer_nombre),
						'SEGUNDO_NOMBRE_PERSONA_CONTACTO' => ucwords($Proveedor->contacto_segundo_nombre),
						'PRIMER_APELLIDO_PERSONA_CONTACTO' =>ucwords($Proveedor->contacto_primer_apellido),
						'SEGUNDO_APELLIDO_PERSONA_CONTACTO'=> ucwords($Proveedor->contacto_segundo_apellido),
						'CELULAR_CONTACTO' => $Proveedor->contacto_celular,
						'TELEFONO_CONTACTO' => $Proveedor->contacto_telefono,
						'NRC' =>$Proveedor->nrc,
						'NIT' =>$Proveedor->nit,
						'USUARIO_CREACION' =>$usuario,
						'FECHA_CREACION' => date('Y-m-d H:i:s'),
						'ESTADO' =>1
					);
					$this->cat_proveedor_model->saveProveedor($registarProveedor);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";					
					

				} else {
					if ($Proveedor->rubro=='0') {
						$Proveedor->rubro='';
					}					
					$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
					$updateProveedor =array(
						'ID_CAT_RUBRO' => $Proveedor->rubro,
						'NOMBRE_PROVEEDOR' => ucwords($Proveedor->nombre),
						'DIRECCION' => $Proveedor->direccion,
						'TELEFONO' => $Proveedor->telefono,
						'FAX' => $Proveedor->fax,
						'CORREO' => $Proveedor->correo,
						'PAGINA_WEB' => $Proveedor->web,
						'PRIMER_NOMBRE_PERSONA_CONTACTO'=> ucwords($Proveedor->contacto_primer_nombre),
						'SEGUNDO_NOMBRE_PERSONA_CONTACTO' => ucwords($Proveedor->contacto_segundo_nombre),
						'PRIMER_APELLIDO_PERSONA_CONTACTO' =>ucwords($Proveedor->contacto_primer_apellido),
						'SEGUNDO_APELLIDO_PERSONA_CONTACTO'=> ucwords($Proveedor->contacto_segundo_apellido),
						'CELULAR_CONTACTO' => $Proveedor->contacto_celular,
						'TELEFONO_CONTACTO' => $Proveedor->contacto_telefono,
						'NRC' =>$Proveedor->nrc,
						'NIT' =>$Proveedor->nit,
						'USUARIO_MODIFICACION' =>$usuario,
						'FECHA_MODIFICACION' => date('Y-m-d H:i:s')
					);

					$this->cat_proveedor_model->updateProveedor($Proveedor->id,$updateProveedor);
					$response['response_msg']="<script>recargar();</script>";				
				}

			}
		}
		echo json_encode($response);
	}

}