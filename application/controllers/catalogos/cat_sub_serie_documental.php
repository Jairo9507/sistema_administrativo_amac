<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class cat_sub_serie_documental extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_sub_serie_documental_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Sub series documentales","catalogos/cat_sub_serie_documental");			
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Sub series Documentales');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());

			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$this->data['subseries']=$this->cat_sub_serie_documental_model->listarSubseries();

			$this->template->admin_render("admin/cat_sub_serie_documental/index",$this->data);

		}
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nueva Sub serie Documental');
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['series']=$this->cat_sub_serie_documental_model->listarSeries();
			$this->load->view("admin/cat_sub_serie_documental/subserie_modal",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Sub series Documentales');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->breadcrumbs->unshift(2,"Editar Sub series documentales","catalogos/cat_sub_serie_documental/editar");			
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			$id=base64_decode($id);
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			$this->data['subserie']=$this->cat_sub_serie_documental_model->listarSubserieId($id);
			$this->data['series']=$this->cat_sub_serie_documental_model->listarSeries();

			$this->template->admin_render("admin/cat_sub_serie_documental/subserie_editar",$this->data);
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$subserie=json_decode($this->input->post("SubseriePost"));
			$id=base64_decode($subserie->id);
			$response=array(
				'response_msg'=>''
			);
			$deleteSubserie=array(
				'ESTADO'=>0
			);
			$this->cat_sub_serie_documental_model->deleteSubserie($id,$deleteSubserie);
			$response['response_msg']="<script>recargar();</script>";
		}
		echo json_encode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$subserie=json_decode($this->input->post("SubseriePost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($subserie->serie=='' or $subserie->serie=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La serie a la que pertenece es obligatoria</div>";
			} else if($subserie->nombre=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre de la sub serie es obligatorio </div>";
			} /*else if($subserie->codigo==''){
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El c&oacute;digo de la sub serie es obligatorio </div>";
			}*/ else {
				if ($subserie->id=='') {
					$registrarSubserie=array(
						'NOMBRE'=>strtoupper($subserie->nombre),
						'DESCRIPCION'=>$subserie->descripcion,
						'CODIGO'=>$subserie->codigo,
						'ID_SERIE_DOCUMENTAL'=>$subserie->serie,
						'TIPOLOGIA'=>$subserie->tipologia
					);
					$this->cat_sub_serie_documental_model->saveSubserie($registrarSubserie);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$updateSubserie=array(
						'NOMBRE'=>strtoupper($subserie->nombre),
						'DESCRIPCION'=>$subserie->descripcion,
						'CODIGO'=>$subserie->codigo,
						'ID_SERIE_DOCUMENTAL'=>$subserie->serie,
						'TIPOLOGIA'=>$subserie->tipologia
					);
					$this->cat_sub_serie_documental_model->updateSubserie($subserie->id,$updateSubserie);
					$response['response_msg']="<script>recargar();</script>";
				}
				
			}
			echo json_encode($response);
		}
		
	}
}