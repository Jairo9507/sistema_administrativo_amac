<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_tipo_documento extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_tipo_documento_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Tipo de documentos","catalogos/cat_tipo_documento");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Tipo de documento');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());

			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['tipos']=$this->cat_tipo_documento_model->listarTiposDocumentos();

			$this->template->admin_render("admin/cat_tipo_documento/index",$this->data);
		}
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$tipo=json_decode($this->input->post("TipoPost"));
			$id=base64_decode($tipo->id);

			$response=array(
				'response_msg'=>''
			);
			$deleteTipo=array(
				'ESTADO'=>0
			);
			$this->cat_tipo_documento_model->deleteTipoDocumento($id,$deleteTipo);
			$response['response_msg']='<script>recargar();</script>';
		}
		echo json_encode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$tipo=json_decode($this->input->post("TipoPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($tipo->descripcion=='') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre del tipo de documento es obligatorio</div>";
			} else {
				if ($tipo->id=='') {
					$registrarTipo= array(
						'DESCRIPCION'=>$tipo->descripcion
					);
					$this->cat_tipo_documento_model->saveTipoDocumento($registrarTipo);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$updateTipo=array(
						'DESCRIPCION'=>$tipo->descripcion
					);
					$this->cat_tipo_documento_model->updateTipoDocumento(base64_decode($tipo->id),$updateTipo);
					$response['response_msg']="<script>recargar();</script>";
				}
				
			}
		}
		echo json_encode($response);
	}

}