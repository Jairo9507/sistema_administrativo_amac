<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class cat_tipo_vehiculo extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_tipo_vehiculo_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Tipo Vehículo","catalogos/cat_tipo_vehiculo");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Marca');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			/*Obtener Tipos de vehículos */

			$this->data['tipo'] = $this->cat_tipo_vehiculo_model->listarTipoVehiculo();
			$this->data['clase'] = $this->cat_tipo_vehiculo_model->listarClases();

			$this->template->admin_render("admin/cat_tipo_vehiculo/index",$this->data);
		}
	}

	public function guardar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$Tipo = json_decode($this->input->post("TipoVehiculoPost"));
			$response=array(
				'campo' =>'',
				'response_msg'=>''
			);
			if ($Tipo->descripcion == '') {
				$response['campo']='';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La descripción de la actividad es obligatoria</div>";	
			}if ($Tipo->id =='' && $Tipo->descripcion != '') {
				$registroTipo = array('DESCRIPCION' =>strtoupper($Tipo->descripcion),
					'ESTADO'      => 1,
					'ID_CLASE_VEHICULO' => $Tipo->id_clase_vehiculo);
				$this->cat_tipo_vehiculo_model->guardarTipoVehiculo($registroTipo);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos<script>location.reload();</script></div>";
			}else{
				$updateTipo = array(
					'descripcion'=>strtoupper($Tipo->descripcion),
					'ID_CLASE_VEHICULO' => $Tipo->id_clase_vehiculo);
				$this->cat_tipo_vehiculo_model->updateTipoVehiculo($Tipo->id, $updateTipo);
				$response['response_msg']='<script>recargar();</script>';

			}
		}
		echo json_encode($response);
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');		
		} else {
			$this->page_title->push("Editar Tipo Vehiculo");
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Tipo Vehículo","admin/cat_tipo_vehiculo/tipo_vehiculo_edit");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			$id=base64_decode($id);
			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['clases'] = $this->cat_tipo_vehiculo_model->listarClases();
			$this->data['tipo']=$this->cat_tipo_vehiculo_model->listarTipoVehiculoId($id);


			$this->template->admin_render("admin/cat_tipo_vehiculo/tipo_vehiculo_edit",$this->data);
		}
	}

	public function eliminar()
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$TipoVehiculoId=json_decode($this->input->post("TipoVehiculoPost"));
			$Id=base64_decode($TipoVehiculoId->id);
			$response =array(
				'response_msg'=>''
			);
			$deleteTipo = array(
				'ESTADO' =>0
			);
			$this->cat_tipo_vehiculo_model->deleteTipoVehiculo($Id,$deleteTipo);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
	}
}