<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class permiso extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("permiso_model");
		$this->breadcrumbs->unshift(1,"Perfil","admin/permiso");
	}

	public function index()
	{
		if(!$this->ion_auth->logged_in() ){
			redirect('auth/login', 'refresh');
		} else {
			$this->page_title->push('Permisos');
			$this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /*Listar Perfiles*/
            $this->data['perfil']=$this->permiso_model->listarPerfiles();


            $this->template->admin_render("admin/permiso/index",$this->data);			
		}
	}



	public function obtenerModulosDenegados()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$perfil=json_decode($this->input->post("PerfilPost"));
			
			$modulosdenegados=array();
			$modulospadres= array();
			$modulos=array();
			$modulosdenegados=$this->permiso_model->PermisosDenegados($perfil->id);	
				
			foreach ($modulosdenegados as $m) {  
				$modulospadres=$this->permiso_model->listarModulosLineaUno($m->ID_DEPENDE);
			}
			//var_dump($modulospadres);
			$json=json_encode($modulosdenegados);

		}
		echo $json;
	}

	public function obtenerModulosPermitidos()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$perfil=json_decode($this->input->post("PerfilPost"));
			$modulos=array();
			$modulos=$this->permiso_model->modulosPermitidos($perfil->id);
			$json=json_encode($modulos);
		}
		echo $json;
	}

	public function obtenerModuloAcceso()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$perfil=json_decode($this->input->post("PerfilPost"));
			$modulosacceso=array();
			$modulosacceso=$this->permiso_model->modulosAcceso($perfil->id);
			$json=json_encode($modulosacceso);
		}
		echo $json;
	}

	public function save(){
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {			
			$modulo=json_decode($this->input->post("PerfilPost"));			
			$response=array(
				'response_msg'=>''
			);

			for ($i=0; $i < sizeof($modulo->idpermisos); $i++) { 
				if (!$this->permiso_model->moduloExiste($modulo->idpermisos[$i],$modulo->idusuario)) {
					$registrarPermiso=array(
						'ID_MENU'=>$modulo->idpermisos[$i],
						'ID_PERFIL_USUARIO'=>$modulo->idusuario,
						'ACTIVO' =>1
					);
				$this->permiso_model->savePermisos($registrarPermiso);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$nombre=$this->permiso_model->moduloNombre($modulo->idpermisos[$i]);
					$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El perfil Seleccionado ya posee permiso para el modulo o submodulo ".$nombre." </div>";
					break;
				}
				
			}

			
		}
		echo json_encode($response);
	}

	public function update()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$modulo=json_decode($this->input->post("PerfilPost"));			
			$response=array(
				'response_msg'=>''
			);
			if ($modulo->eliminar==true) {
				for ($i=0; $i < sizeof($modulo->idpermisos); $i++) { 
						$updatePermiso=array(
							'ACTIVO'=>0
						);
						$idacceso=$this->permiso_model->moduloId($modulo->idpermisos[$i],$modulo->idusuario);
						$this->permiso_model->updatePermiso($idacceso,$updatePermiso);
					}
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro Actualizado adecuadamente, La Información de Actualizara en unos Segundos <script>location.reload();</script></div>";	
					} else {
					for ($j=0; $j < sizeof($modulo->idpermisos); $j++) { 
						$updatePermiso=array(
							'ACTIVO'=>1
						);
						$idacceso=$this->permiso_model->moduloId($modulo->idpermisos[$j],$modulo->idusuario);	
						$this->permiso_model->updatePermiso($idacceso,$updatePermiso);
					}
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro Actualizado adecuadamente, La Información de Actualizara en unos Segundos <script>location.reload();</script></div>";
			}
							
		}
		echo json_encode($response);
	}

}