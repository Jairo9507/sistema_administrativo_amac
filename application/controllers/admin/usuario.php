<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');
class usuario extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->model('usuario_model');
        $this->breadcrumbs->unshift(1,"Usuarios","admin/usuario");
	}

	public function index(){

		if(!$this->ion_auth->logged_in() ){
			redirect('auth/login', 'refresh');
		} else {
			$this->page_title->push('Usuarios');
			$this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /* Obtener lista de empleados*/

            $this->data['usuario']=$this->usuario_model->listarUsuarios();


            $this->data['empleados']=$this->usuario_model->listarEmpleados();
            $this->data['perfiles']=$this->usuario_model->listarPerfil();            

            $this->template->admin_render('admin/usuarios/index2', $this->data);
            /* Obtener los perfiles para crear usuario */

		}
	}

	public function listarPerfil(){
		$this->input->POST('ESTADO');
		$resultado=$this->usuario_model->listarPerfil();
		echo json_encode($resultado);
	}
	
	public function guardar(){

		if(!$this->ion_auth->logged_in()){
				redirect('auth/login', 'refresh');
		} else {
			$detalle=json_decode($this->input->post("DetallePost"));
			$response= array('response_msg' => '',
						'campo'=>'');
			if ($detalle->id_empleado!='' && $detalle->id_detalle=='' && $detalle->usuario !='' && $detalle->contrasena !='') {
				$registrarUsuario = array(
					"IP_ADDRESS" =>'127.0.0.0',
					"USUARIO" => $detalle->usuario,
					"CONTRASENA" => MD5($detalle->contrasena),
					"CORREO_ELECTRONICO" => $detalle->correo,
					"FECHA_CREACION" => date('Y-m-d H:i:s'),
					"ESTADO_USUARIO" =>1,
					"ID_EMPLEADO" => $detalle->id_empleado
				);
				$id=$this->usuario_model->guardar($registrarUsuario);
				$registrarPerfil=array(
					'ID_USUARIO'=>$id,
					'ID_PERFIL_USUARIO'=>$detalle->perfil
				);
				$this->usuario_model->guardarPerfil($registrarPerfil);
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente<script>location.reload();</script></div>";
			} else{
				$updateUsuario = array(
					'USUARIO' => $detalle->usuario,
					'CONTRASENA' => MD5($detalle->contrasena),
					'CORREO_ELECTRONICO' => $detalle->correo);
				$this->usuario_model->updateUsuario($detalle->id_empleado, $updateUsuario);
				$updateUsuarioPerfil = array(
					'ID_PERFIL_USUARIO' => $detalle->perfil  
				);
				$this->usuario_model->updateUsuarioPerfil($detalle->idUsuarioPerfil, $updateUsuarioPerfil);
				$response['response_msg']='<script>recargar();</script>';
				// $response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>";
			}	
		}
		echo json_encode($response);
	}
	
	public function obtenerDatosEmpleado(){

		if(!$this->ion_auth->logged_in() ){
				redirect('auth/login', 'refresh');
		} else {
			$empleado = json_decode($this->input->post("empleadoPost"));
			$datos = array();
			$datos = $this->usuario_model->obtenerEmpleadoId($empleado->id);	
			$json = json_encode($datos);		
		}
		echo $json;
	}

		public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');		
		} else {
			$this->page_title->push("Editar Datos Usuario");
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Usuario","admin/usuario/usuario_edit");
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			$id=base64_decode($id);
			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			$this->data['usuario']=$this->usuario_model->listarUsuarioId($id);
			$this->data['perfiles']=$this->usuario_model->listarPerfil(); 

			$this->template->admin_render("admin/usuarios/usuario_edit",$this->data);
		}
	}

	public function eliminar()
	{
		if(!$this->ion_auth->logged_in()){
			redirect("auth/login",'refresh');
		} else {
			$UsuarioId=json_decode($this->input->post("UsuarioPost"));
			$Id=base64_decode($UsuarioId->id);
			$response =array(
				'response_msg'=>''
			);
			$deleteUsuario = array(
				'ESTADO_USUARIO' =>0
			);
			$this->usuario_model->deleteUsuario($Id,$deleteUsuario);
			$response['response_msg']='<script>recargar();</script>';
			echo json_encode($response);
		}
	}

}