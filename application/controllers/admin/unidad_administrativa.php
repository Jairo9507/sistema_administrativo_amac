<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');


/**
 * 
 */
class unidad_administrativa extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->model('unidades_administrativa_model');
        $this->breadcrumbs->unshift(1,"Unidades administrativas","admin/unidad_administrativa");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Unidades Administrativas');
			$this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['unidades']=$this->unidades_administrativa_model->listarUnidades();

            $this->template->admin_render("admin/unidades/index",$this->data);			
		}
		
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nueva Unidad Administrativa');
			$this->data['pagetitle'] = $this->page_title->show();
            $this->data['unidades']=$this->unidades_administrativa_model->listarGerencias();			
			$this->load->view("admin/unidades/unidad_modal",$this->data);
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Unidad');
			$this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2,"Editar","admin/unidad_administrativa/editar");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			
			$id= base64_decode($id);
			$this->data['unidad']=$this->unidades_administrativa_model->listarUnidadId($id);
			$this->data['unidades']=$this->unidades_administrativa_model->listarGerencias();			
			$this->template->admin_render("admin/unidades/unidad_edit",$this->data);						
		}
		
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$unidad=json_decode($this->input->post("UnidadPost"));
			$id=base64_decode($unidad->id);
			$response= array(
				'response_msg'=>''
			);
			$deleteUnidad= array(
				'ESTADO'=>0
			);
			$this->unidades_administrativa_model->deleteUnidad($id,$deleteUnidad);
			$response['response_msg']='<script>recargar();</script>';
		}
		echo json_encode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$unidad=json_decode($this->input->post("UnidadPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			$existe=$this->unidades_administrativa_model->listarUnidadCodigo($unidad->codigo);

			if ($unidad->codigo=='') {
				$response['campo']='codigo';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El c&oacute;digo de la unidad es obligatorio</div>";
			} else if($unidad->descripcion=='') {
				$response['campo']='descripcion';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El nombre de la unidad es obligatorio</div>";
			} else if(!empty($existe)){
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El codigo de la unidad esta asignado a la unidad ".$existe[0]->DESCRIPCION."</div>";
			} else {
				if ($unidad->id=='') {
					if ($unidad->dependencia=='') {
						$unidad->dependencia=null;
					}
					$registrarUnidad= array(
						'CODIGO_UNIDAD'=>strtoupper($unidad->codigo),
						'DESCRIPCION'=>$unidad->descripcion,
						'UNIDAD_DEPENDE'=>$unidad->dependencia
					);
					$this->unidades_administrativa_model->saveUnidad($registrarUnidad);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					if($unidad->dependencia=='') {
						$unidad->dependencia=null;	
					}
					$updateUnidad= array(
						'DESCRIPCION'=>$unidad->descripcion,
						'UNIDAD_DEPENDE'=>$unidad->dependencia						
					);
					$this->unidades_administrativa_model->updateUnidad($unidad->id,$updateUnidad);
					$response['response_msg']="<script>recargar();</script>";
				}
				
			}
			
		}
		echo json_encode($response);		
	}
}