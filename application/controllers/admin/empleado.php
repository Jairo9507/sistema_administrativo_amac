<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');

/**
 * 
 */
class empleado extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->model('empleado_model');
        $this->breadcrumbs->unshift(1,"Empleados","admin/empleado");		
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Empleados');
			$this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['empleados']=$this->empleado_model->listarEmpleados();

            $this->template->admin_render("admin/empleados/index",$this->data);			
		}
	}

	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nuevo Empleado');
			$this->data['pagetitle'] = $this->page_title->show();
            $this->data['unidades']=$this->empleado_model->listarUnidades();
            $this->data['municipios']=$this->empleado_model->listarMunicipios();		
			$this->load->view("admin/empleados/empleado_modal",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Empleado');
			$this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2,"Editar Empleado","admin/empleado/editar");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            $id=base64_decode($id);
            $this->data['empleado']=$this->empleado_model->listarEmpleadoId($id);
            $this->data['unidades']=$this->empleado_model->listarUnidades();
            $this->data['municipios']=$this->empleado_model->listarMunicipios();
            $this->template->admin_render("admin/empleados/empleado_edit",$this->data);			

		}
	}

	public function eliminar()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$empleado=json_decode($this->input->post("EmpleadoPost"));
			$id=base64_decode($empleado->id);
			$response=array(
				'response_msg'=>''
			); 
			$deleteEmpleado=array(
				'ESTADO'=>0
			);
			$this->empleado_model->deleteEmpleado($id,$deleteEmpleado);
			$response['response_msg']="<script>recargar();</script>";
		}
		echo json_encode($response);
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$empleado=json_decode($this->input->post("EmpleadoPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			if ($empleado->primer_nombre=='') {
				$response['campo']='primer_nombre';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El primer nombre de el empleado es obligatorio</div>";
			}else if($empleado->primer_apellido==''){
				$response['campo']='primer_apellido';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El apellido del empleado es obligatorio</div>";
			} else if($empleado->genero==''){
				$response['campo']='genero';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>El genero del empleado es obligatorio</div>";				
			} else if($empleado->unidad==''){
				$response['campo']='unidad';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La unidad a la que pertenece es obligatorio</div>";	
			} else {
				if ($empleado->id=='') {
					if($empleado->municipio==''){
						$empleado->municipio=null;
					}
					$registrarEmpleado= array(
						'CODIGO_EMPLEADO'=>$empleado->codigo,
						'PRIMER_NOMBRE'=>$empleado->primer_nombre,
						'SEGUNDO_NOMBRE'=>$empleado->segundo_nombre,
						'PRIMER_APELLIDO'=>$empleado->primer_apellido,
						'SEGUNDO_APELLIDO'=>$empleado->segundo_apellido,
						'APELLIDO_CASADA'=>$empleado->apellido_casada,
						'SEXO'=>$empleado->genero,
						'ESTADO_CIVIL'=>$empleado->estado,
						'ID_UNIDAD'=>$empleado->unidad,
						'ID_CAT_MUNICIPIO'=>$empleado->municipio
					);
					$this->empleado_model->saveEmpleado($registrarEmpleado);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";		
				} else {
					if($empleado->municipio==''){
						$empleado->municipio=null;
					}					
					$updateEmpleado= array(
						'CODIGO_EMPLEADO'=>$empleado->codigo,
						'PRIMER_NOMBRE'=>$empleado->primer_nombre,
						'SEGUNDO_NOMBRE'=>$empleado->segundo_nombre,
						'PRIMER_APELLIDO'=>$empleado->primer_apellido,
						'SEGUNDO_APELLIDO'=>$empleado->segundo_apellido,
						'APELLIDO_CASADA'=>$empleado->apellido_casada,
						'SEXO'=>$empleado->genero,
						'ESTADO_CIVIL'=>$empleado->estado,
						'ID_UNIDAD'=>$empleado->unidad,
						'ID_CAT_MUNICIPIO'=>$empleado->municipio
					);
					$this->empleado_model->updateEmpleado($empleado->id,$updateEmpleado);
					$response['response_msg']="<script>recargar();</script>";
				}
				
			}
			
		}
		echo json_encode($response);
	}
}