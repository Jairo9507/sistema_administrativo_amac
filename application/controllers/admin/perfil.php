<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class perfil extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->model("perfil_model");
        $this->breadcrumbs->unshift(1,"Perfil","admin/perfil");		
	}

	public function index(){
		if(!$this->ion_auth->logged_in() ){
			redirect('auth/login', 'refresh');
		} else{
			$this->page_title->push('Perfiles');
			$this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /*obtener perfiles*/
            $this->data['perfil']=$this->perfil_model->listarPerfiles();

            $this->template->admin_render("admin/perfiles/index",$this->data);
            
		}
	}
	public function nuevo(){
		if(!$this->ion_auth->logged_in() OR !$this->ion_auth->is_admin()){
				redirect('auth/login', 'refresh');
		} else {
			$this->page_title->push('Guardar Perfil');
			$this->data['pagetitle'] = $this->page_title->show();

			$this->load->view("admin/perfiles/perfil_modal",$this->data);				
		}
	}

	public function editar($id){
		if(!$this->ion_auth->logged_in() ){
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Edtiar Perfil');
			$this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2,"Editar","admin/perfil/editar");
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			
			$id= base64_decode($id);			
			$this->data['perfil']=$this->perfil_model->ListarPerfilId($id);
			$this->template->admin_render("admin/perfiles/perfil_edit",$this->data);
		}
	}

	public function eliminar(){
		if(!$this->ion_auth->logged_in() ){
			redirect("auth/login",'refresh');
		} else {
			$perfil=json_decode($this->input->post("deletePerfil"));
			$perfilId=base64_decode($perfil->Id);
			$deletePerfil=array(
				'ESTADO' =>0
			);
			$response=array(
				'response_msg' =>''
			);
			$this->perfil_model->deletePerfil($perfilId,$deletePerfil);
			$response['response_msg']="<script>recargar();</script>";
			echo json_encode($response);
		}
	}

	public function save(){
		if(!$this->ion_auth->logged_in() OR !$this->ion_auth->is_admin()){
			redirect("auth/login",'refresh');
		}else{
			$Perfil=json_decode($this->input->post("perfilPost"));
			$response=array(
				'campo' => '',
				'response_msg' =>''
			);
			if($Perfil->Descripcion==''){
				$response['campo']='descripcion';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La descripci&oacute;n es obligatoria</div>";
			} else if ($Perfil->Nombre==''){
				$response['campo']='nombre';
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>La descripcion es obligatoria</div>";
			} else {
				if($Perfil->Id==''){
					$RegistrarPerfil=array(
						'NOMBRE' => ucwords($Perfil->Nombre),
						'DESCRIPCION' => ucwords($Perfil->Descripcion)
					);
					$this->perfil_model->savePerfil($RegistrarPerfil);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente, La Información se Actualizara en unos Segundos <script>location.reload();</script></div>";
				} else {
					$UpdatePerfil=array(
						'NOMBRE' => ucwords($Perfil->Nombre),
						'DESCRIPCION' => ucwords($Perfil->Descripcion)
					);
					$this->perfil_model->updatePerfil($Perfil->Id,$UpdatePerfil);
					$response['response_msg']="<script>recargar();</script>";
				}
			}
		}
		echo json_encode($response);
	}

}