<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class solicitud_compra extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->model('usuario_model');
		$this->load->model("solicitud_compra_model");
		$this->breadcrumbs->unshift(1,"Solicitudes","compra/solicitud_compra");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Solicitudes de compra');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            $this->data['solicitudes']=$this->solicitud_compra_model->listarSolicitudes();
            

            $this->template->admin_render("admin/solicitud_compra/index",$this->data);
		}
	}
	public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nueva Solicitud');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Nueva Solicitud","compra/solicitud_compra/solicitud_crear");
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            
			$this->data['unidad']=$this->ion_auth->get_unidad_empleado();			
            $this->template->admin_render("admin/solicitud_compra/solicitud_crear",$this->data);			
		}
		
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Solicitud');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Solicitud","compra/solicitud/solicitud_editar");

			$id=base64_decode($id);
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            
			$this->data['unidad']=$this->ion_auth->get_unidad_empleado();
			$this->data['solicitud']=$this->solicitud_compra_model->listarSolicitudId($id);
			$this->data['detalles']=$this->solicitud_compra_model->listarDetalles($id);
            $this->template->admin_render("admin/solicitud_compra/solicitud_editar",$this->data);			
		}		
	}

	public function vistaProductos()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Lista de productos");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['productos']=$this->solicitud_compra_model->listarProductos();			
			$this->load->view("admin/solicitud_compra/modal_productos",$this->data);
					
		}		
	}
	public function obtenerCodigo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo = $this->solicitud_compra_model->ultimoCodigo();
			$json=json_encode($codigo);						
		}
		echo $json;
		
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$solicitud=json_decode($this->input->post("SolicitudPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);

			if($solicitud->id==''){
				$unidad=$this->ion_auth->get_unidad_empleado();
				if ($unidad==null) {
					$unidad=52;
				}
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$registrarRequisicion= array(
					'ID_UNIDAD_SOLICITA'=>$unidad,
					'FECHA_SOLICITUD'=>$solicitud->fecha,
					'CODIGO'=>$solicitud->correlativo,
					'USUARIO_CREACION'=>$usuario,
					'FECHA_CREACION'=>date('Y-m-d H:i:s')
				);

				$id=$this->solicitud_compra_model->saveSolicitud($registrarRequisicion);
				//var_dump($id);exit;
				$response['response_msg']='<input type="hidden" name="id" id="id_solicitud" value="'.$id.'">';
			} else {
				$unidad=$this->ion_auth->get_unidad_empleado();
				if ($unidad==null) {
					$unidad=52;
				}
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$updateRequisicion= array(					
					'FECHA_SOLICITUD'=>$solicitud->fecha,
					'ID_UNIDAD_SOLICITA'=>$unidad,
					'USUARIO_MODIFICACION'=>$usuario,
					'FECHA_MODIFICACION'=>date('Y-m-d H:i:s')
				);
				$this->solicitud_compra_model->updateSolicitud($solicitud->id,$updateRequisicion);
			}
		}
		echo json_encode($response);

	}

	public function saveDetalle()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$detalle=json_decode($this->input->post("DetallePost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>'',
				'detalle_id'=>''
			);

			if ($detalle->id_solicitud!='' && $detalle->id_detalle=='') {
				$registrardetalle=array(
					"CANTIDAD"=>$detalle->cantidad,
					"ID_PRODUCTO"=>$detalle->id_producto,
					"ID_SOLICITUD"=>$detalle->id_solicitud
				);
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());			
				$id=$this->solicitud_compra_model->saveDetalle($registrardetalle);
				$response['detalle_id']=$id;

				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente</div>";
			} else if($detalle->id_solicitud!='' && $detalle->id_detalle!=''){
				$updatedetalle=array(
					"CANTIDAD"=>$detalle->cantidad
				);
				$this->solicitud_compra_model->updateDetalle($detalle->id_detalle,$updatedetalle);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro actualizado adecuadamente</div>";
			} else {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>";
			}
		}
		echo json_encode($response);
		
	}	

}