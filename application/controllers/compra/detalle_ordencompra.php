<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
setlocale(LC_MONETARY, 'en_US');
/**
 * 
 */
class detalle_ordencompra extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->model('detalle_ordencompra_model');
        $this->load->model('ordencompra_model');
        $this->load->model("usuario_model");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Detalle de Ingreso de Productos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /*Listar Detalle de orden de compra*/
            $this->data['detalle']=$this->detalle_ordencompra_model->listarDetalle();


            $this->template->admin_render("admin/compra/detalle_ordencompra",$this->data);

			
		}
	}

	public function editar($id)
	{
        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login",'refresh');
        } else {
            $this->page_title->push('Editar detalle');
            $this->data['pagetitle']=$this->page_title->show();
            $this->breadcrumbs->unshift(2,"Editar detalle","compra/detalle_ordencompra/editar_ordencompra");

            $id=base64_decode($id);
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            // $this->data['unidad']=$this->ion_auth->get_unidad_empleado();
            $this->data['proveedores']=$this->detalle_ordencompra_model->listarProveedores();
            $this->data['ordencompra']=$this->detalle_ordencompra_model->listarOrdenCompraId($id);
            $this->data['productos']=$this->ordencompra_model->listarProductos();
            $this->data['detalles']=$this->detalle_ordencompra_model->listarDetalleProductos($id);
            $this->template->admin_render("admin/compra/editar_ordencompra_uaci",$this->data);           
        }       	
		
	}

	public function vistaPDF($id)
		{
			if (!$this->ion_auth->logged_in()) {
				redirect("auth/login",'refresh');
			} else {
				$this->page_title->push("Orden de Compra");
				$this->data['pagetitle']=$this->page_title->show();
				$this->data['Idorden']=base64_decode($id);
				$this->load->view('admin/compra/orden_compra_pdf',$this->data); 			
			}
			
		}	

	public function ordenPDF($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			if($id==null){
	           $html="Ha ocurrido un error al realizar la operación";
	           $this->mpdf->setDisplayMode('fullpage');
	           $this->mpdf->WriteHTML($html);
	           $this->mpdf->Output();
	           exit;
	        }else {
            $ordenCompra=$this->detalle_ordencompra_model->ordenCompra($id);
            $total = $this->detalle_ordencompra_model->totalOrden($id);
            $detalle = $this->detalle_ordencompra_model->detalleOrdenCompra($id);
            $totales = $this->detalle_ordencompra_model->valorEnLetras($total[0]->TOTALON);
            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            $fecha=date('d',strtotime($ordenCompra[0]->FECHA))."-".$meses[date('n',strtotime($ordenCompra[0]->FECHA))-1]."-".date('Y',strtotime($ordenCompra[0]->FECHA));
            $html='
            <!DOCTYPE HTML PUBLIC ">
            <html>
            <head>
            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

            <title>Orden de Compra</title>
            <style type="text/css">

            @page {
                margin: 1.5cm;
            }


        #header,
        #footer {
          position: fixed;
          left: 0;
          right: 0;
          font-size: 0.9em;
      }
      #header {
          top: 0;
          
      }
      #footer {

          bottom: 0;
          border-top: 0.1pt solid #aaa;
      }
    #logo_clinica{
    width:20%;
    display: inline-block;
    float:left;
    }

    #logo_sistema{
        width:20%;
        display: inline-block;
        float:right;
    }
    #encabezado
    {
        width:60%;
        display:inline-block;
        float:left;
    }
    body {
       font-family: Sans-Serif;
        margin: 0.5cm 0;
        text-align: justify;
    }
    #tet{
        border-radius:15px;
        width: 13%;
        padding: 3% 5%;
        display: inline-block;
        float:right;
        text-align: center;
        border:1px solid red;
        color: red;
    }
    #prueba{
        position: relative;
        top: 40%;
        text-align:center;
        left: 2%;
      }
    #prueba2{
        position: relative;
        top: 40%;
        text-align:center;
        left: 2%;
      }
     #foot{
        width: 90%;
        position:absolute;
        text-align: center;
        bottom:-1%;
        }
    table{
        clear:both;
        text-align: left;
        height:auto;
        border-collapse:collapse; 
        table-layout:fixed;
        width: 100%;
    }

    #cuerpo{
        position: abosolute;
        text-align: center;
        bottom=12%;
    }

    table td {
        word-wrap:break-word;
        padding:8px 0px 10px 0px;
    }

    .page-number {
      text-align: center;
    }

    .page-number:before {
      content: "Página " counter(page);
    }

    hr {
      page-break-after: always;
      border: 0;
    }

</style>
</head>';
foreach ($ordenCompra as $oc) {

$html.='
<body>
<div id="header">
<div id="logo_clinica" > 
<img src="img/escudo_mini.png" style="width:100; height=100;">
</div>
<div id="encabezado">
    <h3 style="text-align:center;height:0%;">Alcaldía Municipal de Antiguo Cuscatlan</h3>
    <h5 style="text-align:center;height:0%;">Departamento de la Libertad, El Salvador, C.A</h5>
    <h5 style="text-align:center;">PBX:2511-0100/2511-0121</h5>
    <h3 style="text-align:center;">Orden de Compra</h3>
</div>
<div id="tet">
    N° '.$oc->CODIGO_ORDEN_COMPRA.'
</div>

<div style="position:relative; padding-bottom:200px">
    <table>
        <tr>
            <td style="font-weight:bold;width:25%">Fecha: </td><td>'.$fecha.'</td>
        </tr>
        <tr>
            <td style="font-weight:bold;">Proveedor: </td><td style="text-align:left;">'.$oc->NOMBRE_PROVEEDOR.'</td>
        </tr>
        <tr>
            <td style="font-weight:bold;">Condiciones de Pago:</td><td style="text-align:left;">'.$oc->CONDICIONES_PAGO.' </td>
        </tr>
        <tr>
            <td style="font-weight:bold; ">Concepto: </td>
            <td style="text-align:left; overflow:hidden;">'.$oc->VIA.'</td>
        </tr>
        <tr>
            <td style="font-weight:bold;">Creado Por: </td>
            <td style="text-align:left;">'.$oc->NOMBRE.'</td>
        </tr>
    </table>
</div>    
</div>

';
}

$html.='<div id="footer" >

</div>
';

$html.='
<div id="prueba" style="position:relative; bottom:2%;">
<h4 style="text-align:center;">DETALLE</h4>
        <table style="border:1px solid;" >
        <thead>
        <tr>
        <th style="text-align:center;border:1px solid;width:15%;">CANTIDAD</th>
        <th style="text-align:center;border:1px solid;width:50%;">DESCRIPCION</th>
        <th style="text-align:center;border:1px solid;width:17%;">PRECIO UNITARIO</td>
        <th style="text-align:center;border:1px solid;">TOTAL</th>
        </tr>
        </thead>
        </table>
    </div>
    ';
    if ($detalle) {
        foreach ($detalle as $det) {
            if ($det->CODIGO_PRESUPUESTARIO == 1) {
                $html.='
        <div id="prueba">
            <table style="border:1px solid;">
            <tbody>
            <tr>
            <td style="text-align:center;border:1px solid;font-size:11px;width:15%;"></td>
            <td style="text-align:center;border:1px solid;font-size:11px;width:50%;">'.$det->DESCRIPCION.'</td>
            <td style="text-align:center;border:1px solid;font-size:11px;width:17%;"></td>
            <td style="text-align:center;border:1px solid;font-size:11px;">$'.$det->PRECIO_UNITARIO.'</td>
            </tr>
            </tbody>
            </table>
        </div>
            ';
            }else if ($det->CODIGO_PRESUPUESTARIO == NULL) {
                $html.='
        <div id="prueba">
            <table style="border:1px solid;">
            <tbody>
            <tr>
            <td style="text-align:center;border:1px solid;font-size:11px;width:15%;">'.$det->CANTIDAD_TOTAL.'</td>
            <td style="text-align:center;border:1px solid;font-size:11px;width:50%;">'.$det->DESCRIPCION.'</td>
            <td style="text-align:center;border:1px solid;font-size:11px;width:17%;">$'.$det->PRECIO_UNITARIO.'</td>
            <td style="text-align:center;border:1px solid;font-size:11px;">$'.$det->TOTAL.'</td>

            </tr>
            </tbody>
            </table>
        </div> ';
            }else{
                $html.='
            <div id="prueba">
                <table style="border:1px solid;">
                <tbody>
                <tr>
                <td style="text-align:center;border:1px solid;font-size:11px;width:15%">'.$det->CANTIDAD_TOTAL.'</td>
                <td style="text-align:center;border:1px solid;font-size:11px; width:50%;">'.$det->DESCRIPCION.'</td>
                <td style="text-align:center;border:1px solid;font-size:11px; width:17%;">$'.$det->PRECIO_UNITARIO.'</td>
                <td style="text-align:center;border:1px solid;font-size:11px;">$'.$det->TOTAL.'</td>

                </tr>
                </tbody>
                </table>
            </div> ';


            }
        }
    }
    if ($totales) {
        foreach ($totales as $tot) {
                 $html.='
         <div id="prueba" >
        <table style="border:1px solid; ">
        <tbody>
        <tr> 
            <td colspan="3" style="text-align:center;width:82%;">'.$tot->TOTLETRAS.'</td>';
        }
    }
    if ($total) {

        foreach ($total as $t) {

        $total=number_format(floatval(str_replace(",","",$t->TOTALON)),2);
   $html.='<td style="text-align:center;border:1px solid;font-size:14px;">$<b>'.$total.'</b></td>
    </tr>
    </tbody>
    </table>
    </div>';
    }
     
}

$html.='
<div id="foot" >
<table >
<tr>
<td style="text-align:center;width:10%;"></td>
<td style="text-align:center;">______________</td>
<td style="text-align:center;">______________</td>
<td style="text-align:center;">______________</td>
</tr>
<tr>
<td style="text-align:center;"></td>
<td style="text-align:center;">Firma</td>
<td style="text-align:center;">Sello</td>
<td style="text-align:center;">Secretaria</td>
</tr>
</table>
</div>
</body></html>
';          

            $this->load->library('pdf');
            $nombre_archivo = "ORDEN COMPRA N° ".$ordenCompra[0]->CODIGO_ORDEN_COMPRA." ".$ordenCompra[0]->FECHA."";
            $this->pdf->loadHtml($html);
            $this->pdf->setPaper('A4');
            $this->pdf->set_option('defaultMediaType', 'all');
            $this->pdf->set_option('isFontSubsettingEnabled', true);            
            $this->pdf->render();
            $fontMetrics = $this->pdf->getFontMetrics();
            $canvas = $this->pdf->get_canvas();
            $font = $fontMetrics->getFont('Arial','bold');
            $canvas->page_text(455, 120, "Página {PAGE_NUM} de {PAGE_COUNT}", $font, 10, array(0, 0, 0));
            $this->pdf->stream($nombre_archivo,array("Attachment" => 0));
        
        }
	}
		
  }

  public function actualizarOrden()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect("auth/login",'refresh');
        } else {
            $orden = json_decode($this->input->post("ordenUpdatePost"));
            $usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
            $response = array(
                'response_msg' => ''
                 );

            $updateOrden = array(
                'FECHA' =>$orden->fecha,
                'CONDICIONES_PAGO' =>$orden->condPago,
                'VIA' => $orden->concepto,
                'ID_PROVEEDOR' => $orden->proveedor,
                'USUARIO_MODIFICACION' =>$usuario,
                'FECHA_MODIFICACION' =>date('Y-m-d H:i:s') 
            );
            $this->detalle_ordencompra_model->updateOrdenCompra($orden->id_orden_compra, $updateOrden);
            $response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Guardado correctamente</div>";
    }
    echo json_encode($response);
  }

  public function updateDetalle()
  {
      if (!$this->ion_auth->logged_in()) {
            redirect("auth/login",'refresh');
        } else {
            $detalle = json_decode($this->input->post("detallePost"));

            $Id = $detalle->id;
            //$cantidad = $this->ordencompra_bodegas_model->obtenerCantidadParcial($detalle->id);
            $response = array(
                'response_msg' => ''
                 );

            $updateDetalle = array(
                'CANTIDAD_TOTAL'  => $detalle->cantidad,
                'DESCRIPCION'     => $detalle->descripcion,
                'PRECIO_UNITARIO' => $detalle->precio   
            );
            $this->detalle_ordencompra_model->updateDetalle($detalle->id, $updateDetalle);
            $response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Guardado correctamente</div>";
        }
        echo json_encode($response);
    }
}