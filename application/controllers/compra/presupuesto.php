<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class presupuesto extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->model('presupuesto_model');
		$this->load->model("usuario_model");
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Presupuesto de Insumos');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /*Listar Detalle de orden de compra*/
            $this->data['presupuesto']=$this->presupuesto_model->listarPresupuesto();
            $this->data['cuentas']=$this->presupuesto_model->listarCuentaPresupuestaria();
            $this->data['productos']=$this->presupuesto_model->listarProductos();
            $this->data['unidades']=$this->presupuesto_model->listarUnidades();

            $this->template->admin_render("admin/presupuesto/index",$this->data);

		}
	}

		public function nuevo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Nuevo Presupuesto');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Nuevo Presupuesto","compra/presupuesto/presupuesto_crear");
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            
			$this->data['unidad']=$this->ion_auth->get_unidad_empleado();			
            $this->template->admin_render("admin/presupuesto/presupuesto_crear",$this->data);			
		}
		
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$presupuesto=json_decode($this->input->post("PresupuestoPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);

			if($presupuesto->id==''){
				$unidad=$this->ion_auth->get_unidad_empleado();
				if ($unidad==null) {
					$unidad=52;
				}
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$registrarPresupuesto= array(
					'ID_UNIDAD'=>$unidad,
					'FECHA_REGISTRO'=>$solicitud->fecha,
					'CODIGO'=>$solicitud->correlativo,
					'USUARIO_CREACION'=>$usuario,
					'FECHA_CREACION'=>date('Y-m-d H:i:s')
				);

				$id=$this->solicitud_compra_model->savePresupuesto($registrarPresupuesto);
				//var_dump($id);exit;
				$response['response_msg']='<input type="hidden" name="id" id="id_requisicion" value="'.$id.'">';
			} else {
				$unidad=$this->ion_auth->get_unidad_empleado();
				if ($unidad==null) {
					$unidad=52;
				}
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$updatePresupuesto= array(					
					'FECHA_REGISTRO'=>$requisicion->fecha,
					'ID_UNIDAD'=>$unidad,
					'USUARIO_MODIFICACION'=>$usuario,
					'FECHA_MODIFICACION'=>date('Y-m-d H:i:s')
				);
				$this->solicitud_compra_model->updatePresupuesto($solicitud->id,$updateRequisicion);
			}
		}
		echo json_encode($response);

	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Solicitud');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Solicitud","compra/presupuesto/presupuesto_editar");

			$id=base64_decode($id);
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
            
			$this->data['unidad']=$this->ion_auth->get_unidad_empleado();
			$this->data['presupuesto']=$this->solicitud_compra_model->listarSolicitudId($id);
			$this->data['detalles']=$this->solicitud_compra_model->listarDetalles($id);
            $this->template->admin_render("admin/presupuesto/presupuesto_editar",$this->data);			
		}		
	}

	public function vistaProductos()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push("Lista de productos");
			$this->data['pagetitle']=$this->page_title->show();
			$this->data['productos']=$this->presupuesto_model->listarProductos();		
			$this->load->view("admin/presupuesto/modal_productos",$this->data);
					
		}		
	}

		public function obtenerCodigo()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$codigo = $this->presupuesto_model->ultimoCodigo();
			$json=json_encode($codigo);						
		}
		echo $json;
		
	}
	

	public function agregarPresupuesto()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$presupuesto=json_decode($this->input->post("presupuestoPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);

			if($presupuesto->id_presupuesto==''){
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$registrarOrden= array(
					'ID_UNIDAD'=>$presupuesto->unidad,
					'FECHA_REGISTRO'=>date('Y-m-d H:i:s'),
					'FECHA_CREACION'=>$presupuesto->fecha,
					'USUARIO_CREACION'=>$usuario,
					'FECHA_CREACION'=>date('Y-m-d H:i:s')
				);

				$id=$this->presupuesto_model->agregarPresupuesto($registrarOrden);
				//var_dump($id);
				$response['response_msg']='<input type="hidden" name="id" id="id_presupuesto" value="'.$id.'">';				
			} 
			// else {
			// 	$unidad=$this->ion_auth->get_unidad_empleado();
			// 	if ($unidad==null) {
			// 		$unidad=52;
			// 	}
			// 	$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
			// 	$updateOrdenCompra= array(					
			// 		'FECHA'=>$requisicion->,
			// 		'ID_UNIDAD_CREA'=>$unidad,
			// 		'ID_UNIDAD_SOLICITA'=>$requisicion->unidad_solicita,
			// 		'USUARIO_MODIFICACION'=>$usuario,
			// 		'FECHA_MODIFICACION'=>date('Y-m-d H:i:s')
			// 	);
			// 	$this->requisicion_model->updateRequisicion($updateRequisicion);
			// }
		}
		echo json_encode($response);
	}
	public function agregarProducto()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$detalle=json_decode($this->input->post("DetallePost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);

			if ($detalle->id_presupuesto!='' && $detalle->id_detalle=='') {
				$registrardetalle=array(
					"ID_PRESUPUESTO"=>$detalle->id_presupuesto,
					"ID_PRODUCTO"=>$detalle->id_producto,
					"CANTIDAD"=>$detalle->cantidad,
					"PRECIO_UNITARIO"=>$detalle->precio,
					"COSTO_APROXIMADO"=>(($detalle->precio)*($detalle->cantidad)),
					"ID_CUENTA_PRESUPUESTARIA"=>$detalle->cuenta
				);
				$this->presupuesto_model->agregarProducto($registrardetalle);			
				$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro guardado adecuadamente</div>";
			} else if($detalle->id_presupuesto!='' && $detalle->id_detalle!=''){
				$updatedetalle=array(
					"CANTIDAD"=>$detalle->cantidad,
					"PRECIO_UNITARIO"=>$detalle->precio
				);
				$this->ordencompra_model->updatePresupuesto($detalle->id_detalle,$updatedetalle);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro actualizado adecuadamente</div>";
			} else {
				$response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>";
			}
		}
		echo json_encode($response);
	}

}
