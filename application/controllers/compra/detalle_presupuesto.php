<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class detalle_presupuesto extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->model('detalle_presupuesto_model');
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Detalle de Ordenes de Compra');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /*Listar Detalle de orden de compra*/
            $this->data['detalle']=$this->detalle_presupuesto_model->listarDetalle();

            

            $this->template->admin_render("admin/presupuesto/detalle_presupuesto",$this->data);

			
		}
	}

	public function editar($id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Editar Presupuesto');
			$this->data['pagetitle']=$this->page_title->show();
			$this->breadcrumbs->unshift(2,"Editar Presupuesto","
				presupuesto/presupuesto_editar");

			$id=base64_decode($id);
			/* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());
			// $this->data['unidad']=$this->ion_auth->get_unidad_empleado();
			$this->data['productos']=$this->detalle_presupuesto_model->listarProductos();
			$this->data['presupuesto']=$this->detalle_presupuesto_model->listarPresupuestoId($id);
			$this->data['cuenta']=$this->detalle_presupuesto_model->obtenerCuentaPresupuestaria($id);
		    $this->data['total']=$this->detalle_presupuesto_model->obtenerTotal($id);
			$this->data['detalles']=$this->detalle_presupuesto_model->listarDetalleProductos($id);
            $this->template->admin_render("admin/presupuesto/presupuesto_editar",$this->data);			
		}		
		
	}
}