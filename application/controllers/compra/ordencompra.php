<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/El_Salvador');
/**
 * 
 */
class ordenCompra extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
		$this->load->model('ordencompra_model');
		$this->load->model("usuario_model");
	}

	public function index(){
		if(!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
			redirect('auth/login', 'refresh');
		} else {
			$this->page_title->push('Alcaldía Munincipal de Antiguo Cuscatlán');
			$this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Menu*/
            $this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
           
            $this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

            /* Obtener lista de proveedores*/
             $this->data['proveedores']=$this->ordencompra_model->listarProveedores();
             /* Obtener lista de productos*/
             $this->data['productos']=$this->ordencompra_model->listarProductos();
             /*Obtiene y muestra el siguiente correlativo*/
             $this->data['correlativo']=$this->ordencompra_model->obtenerCorrelativo();
                         $this->data['unidades']=$this->ordencompra_model->listarUnidades();
          
            $this->template->admin_render('admin/compra/index', $this->data);
            /* Obtener los perfiles para crear usuario */
		}
	}


	public function agregarPedido()
	{	
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$detalle=json_decode($this->input->post("ordenPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);
			
			for ($i=0; $i < sizeof($detalle->products); $i++) { 
				if ($detalle->products[$i]->destino=='E' && $detalle->id=='') {
					/*$registro = array(
					'ID_ORDEN_COMPRA' =>$detalle->products[$i]->id_orden_compra,
					'ID_PRODUCTO' =>$detalle->products[$i]->id_producto,
					'PRECIO' =>$detalle->products[$i]->precio,
					'CANTIDAD' =>$detalle->products[$i]->cantidad,
					'ESTADO'=>0,
					//'FECHA_INICIO_VIGENCIA'=>date('Y-m-d H:i:s'),
					'CANTIDAD_INVENTARIO'=>0
				);*/
					//$this->ordencompra_model->agregarHistorico($registro);
					$registrardetalle = array(
						'ID_ORDEN_COMPRA' =>$detalle->products[$i]->id_orden_compra,
						'ORDEN_COMPRA' =>$detalle->products[$i]->codOrden,
						'ID_PRODUCTO' =>$detalle->products[$i]->id_producto,
						'CANTIDAD_TOTAL' =>$detalle->products[$i]->cantidad,
						'DESCRIPCION'   =>strtoupper($detalle->products[$i]->descripcion),
						'CANTIDAD_PARCIAL'=>0,
						'DESTINO' =>$detalle->products[$i]->destino,
						//'CODIGO_PRESUPUESTARIO' =>$detalle->products[$i]->cuenta,
						// 'TIPO_ENTREGA' =>$detalle->products[$i]->tipoEntrega,
						'PRECIO_UNITARIO' =>$detalle->products[$i]->precio,
					);
					$this->ordencompra_model->agregarPedido($registrardetalle);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Guardado Correctamente</div>";
					
			} else if($detalle->id!=''){
				$updatedetalle=array(
					"CANTIDAD"=>$detalle->products[$i]->cantidad,
					"PRECIO_UNITARIO"=>$detalle->products[$i]->precio
				);
				$this->ordencompra_model->updateOrdenCompra($detalle->id,$updatedetalle);
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
					$product_exist = $this->ordencompra_model->buscarProducto($detalle->products[$i]->id_producto);
					$existencia    = $this->ordencompra_model->existenciaProducto($detalle->products[$i]->id_producto);
					$val1 = (int) $existencia;
					$val2 = (int) $detalle->products[$i]->cantidad;
					$suma = $val1+ $val2;
					if (empty($product_exist)) {
						$registrarInventario=array(
						'FECHA_INGRESO'=>date('Y-m-d H:i:s'),
						'ID_ORDEN_COMPRA'=>$detalle->products[$i]->id_orden_compra,
						'ID_PRODUCTO'=>$detalle->products[$i]->id_producto,
						'CANTIDAD'=>0,
						'PRECIO'=>$detalle->products[$i]->precio,
						'USUARIO_CREACION'=>$usuario,
						'FECHA_CREACION'=>date('Y-m-d H:i:s')
				);						
				$this->ordencompra_model->saveInventario($registrarInventario);
					}else if(!empty($product_exist)){
						$updateInventario = array(
						
						'CANTIDAD'=>$suma,
						'USUARIO_MODIFICACION'=>$usuario,
						'FECHA_MODIFICACION'=>date('Y-m-d H:i:s')
				);						
				 $this->ordencompra_model->updateInventario($detalle->products[$i]->id_producto, $updateInventario);

				}
				$registrarMovimiento=array(
					'FECHA'=>date('Y-m-d H:i:s'),
					'ID_ORDEN_COMPRA'=>$detalle->products[$i]->id_orden_compra,
					'ID_PRODUCTO'=>$detalle->products[$i]->id_producto,
					'CANTIDAD'=>$detalle->products[$i]->cantidad,
					'COSTO_UNITARIO'=>$detalle->products[$i]->precio,
					'USUARIO_CREACION'=>$usuario,
					'FECHA_CREACION'=>date('Y-m-d H:i:s'),
					'ID_TIPO_MOVIMIENTO'=>1									
				);
				$this->ordencompra_model->saveMovimiento($registrarMovimiento);
			
			// $this->template->admin_render("admin/compra/detalle_ordencompra",$this->data);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Guardado Correctamente</div>";
			}else if($detalle->id == '' && $detalle->products[$i]->destino == 'G'){
				$registrardetalle = array(
						'ID_ORDEN_COMPRA' =>$detalle->products[$i]->id_orden_compra,
						'ORDEN_COMPRA' =>$detalle->products[$i]->codOrden,
						'ID_PRODUCTO' =>$detalle->products[$i]->id_producto,
						'CANTIDAD_TOTAL' =>$detalle->products[$i]->cantidad,
						'DESCRIPCION'   =>strtoupper($detalle->products[$i]->descripcion),
						'CANTIDAD_PARCIAL'=>0,
						'DESTINO' =>$detalle->products[$i]->destino,
						'CODIGO_PRESUPUESTARIO' =>$detalle->products[$i]->cuenta,
						// 'TIPO_ENTREGA' =>$detalle->products[$i]->tipoEntrega,
						'PRECIO_UNITARIO' =>$detalle->products[$i]->precio,
					);
					$this->ordencompra_model->agregarPedido($registrardetalle);
					$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Guardado Correctamente</div>";

			} else {
				 $response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>";
				 }
			} // fin del for  || FIN del FOR
		  } // fin del if antes del for || FIN DEL ELSE
		  echo json_encode($response);
		} // fin else
		
	
	public function agregarOrdenCompra()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$ordencompra=json_decode($this->input->post("ordenCompraPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);

			if($ordencompra->id==''){
				$unidad=$this->ion_auth->get_unidad_empleado();
				if ($unidad==null) {
					$unidad=8;
				}
				if ($ordencompra->unidad=='') {
					$ordencompra->unidad=NULL;
				}
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$registrarOrden= array(
					'CODIGO_ORDEN_COMPRA'=>$ordencompra->codigoOrden,
					'FECHA'=>$ordencompra->fecha,
					'VIA' =>strtoupper($ordencompra->via),
					'SP'  =>$ordencompra->sp,
					'CONDICIONES_PAGO' =>$ordencompra->condicionPago,
					'UNIDAD_CREA'=>$ordencompra->unidad,
					//'NUMERO_FACTURA'=>$ordencompra->numFactura,
					'USUARIO_CREACION'=>$usuario,
					'FECHA_CREACION'=>date('Y-m-d H:i:s'),
					'ID_PROVEEDOR' => $ordencompra->id_proveedor,
					'ESTADO' => 1
				);

				$id=$this->ordencompra_model->agregarOrdenCompra($registrarOrden);

				$response['response_msg']='<input type="hidden" name="id" id="id_orden_compra" value="'.$id.'">';			
			} 
		}
		echo json_encode($response);
	}

	public function updateOrden()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$orden=json_decode($this->input->post("Ordenpost"));
			$response=array(
				'response_msg'=>''
			);
			if ($orden->id!='') {
				$updateOrden=array(
					'NUMERO_FACTURA'=>$orden->factura
				);
				$this->ordencompra_model->updateOrden($orden->id,$updateOrden);
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro Actualizado correctamente</div>";				
			} else {
				 $response['response_msg']="<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>";
			
			}
			
		}
		echo json_encode($response);
	}

	public function agregarOrden()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$ordencompra=json_decode($this->input->post("ordenCompraPost"));
			$response=array(
				'response_msg'=>'',
				'campo'=>''
			);

			if($ordencompra->id_orden_compra!=''){
				$unidad=$this->ion_auth->get_unidad_empleado();
				if ($unidad==null) {
					$unidad=52;
				}
				$usuario=$this->usuario_model->buscarUsuario($this->ion_auth->get_user_id());
				$updateDetalleOrden = array(
					'CODIGO_ORDEN_COMPRA' => $ordencompra->codigo_orden,
					'SP'  =>$ordencompra->sp,
					'NUMERO_FACTURA'=>$ordencompra->numFactura,
					'USUARIO_MODIFICACION'=>$usuario,
					'FECHA_MODIFICACION'=>date('Y-m-d H:i:s')
				);

				$id=$this->ordencompra_model->updateOrden($ordencompra->id_orden_compra, $updateDetalleOrden);

				// $response['response_msg']='<input type="hidden" name="id" id="id_requisicion" value="'.$id.'">';				
			} 
		}
		echo json_encode($response);
		
	}

	public function anularOrden()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$detalle = json_decode($this->input->post("AnularOrdenPost"));

			$Id=base64_decode($detalle->id);
			$response = array(
				'response_msg' => ''
				 );
			$anularOrden = array(
				'ESTADO' => 0,
				'MOTIVO_ANULACION'=>$detalle->motivo 
			);
			$this->ordencompra_model->anularOrden($Id, $anularOrden);
			$response['response_msg']="<div class='alert alert-success text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>Registro Anulado correctamente</div>";
		}
		echo json_encode($response);
	}
}

