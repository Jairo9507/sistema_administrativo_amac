<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_requisicion_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct(); 
	}

	public function listarRequisicionesPDF($unidad,$fechainicio,$fechafin)
	{
		$sql='SELECT r.ID_REQUISICION, r.CORRELATIVO,DATE_FORMAT(r.FECHA,"%Y-%m-%d") as FECHA,uac.DESCRIPCION as unidad_crea,uas.DESCRIPCION as unidad_solicita,uas.CODIGO_UNIDAD,r.USUARIO_CREACION,r.FECHA_CREACION ,r.CONCEPTO  
				FROM inv_requisicion_empleado re left join  inv_requisicion r
				on re.id_requisicion=r.id_requisicion left join seg_usuario u 
				on u.id_usuario=re.id_empleado_usuario left join seg_cat_unidad_administrativa uac
				on uac.id_unidad=r.id_unidad_crea left join seg_cat_unidad_administrativa uas
				on uas.id_unidad=r.id_unidad_solicita WHERE r.ID_UNIDAD_CREA='.$unidad.' and r.ESTADO=1 and DATE_FORMAT(r.FECHA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechainicio.'" ,"%Y-%m-01")  and DATE_FORMAT("
				'.$fechafin.'","%Y-%m-%d") ORDER BY r.FECHA DESC';
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function obtenerUnidad($unidad)
	{
		$sql="SELECT * FROM seg_cat_unidad_administrativa WHERE ESTADO=1 and ID_UNIDAD=".$unidad;
		$query=$this->db->query($sql);
		$unidad='';
		foreach ($query->result_array() as $q) {
			$unidad=$q['CODIGO_UNIDAD']." ".$q['DESCRIPCION'];
		}
		return $unidad;
	}

	public function listarRequisicionCodigo($correlativo)
	{
		$sql='SELECT r.ID_REQUISICION, r.CORRELATIVO,DATE_FORMAT(r.FECHA,"%Y-%m-%d") as FECHA,uac.DESCRIPCION as unidad_crea,uas.DESCRIPCION as unidad_solicita,uas.CODIGO_UNIDAD,r.USUARIO_CREACION,r.FECHA_CREACION ,r.CONCEPTO  
				FROM inv_requisicion_empleado re left join  inv_requisicion r
				on re.id_requisicion=r.id_requisicion left join seg_usuario u 
				on u.id_usuario=re.id_empleado_usuario left join seg_cat_unidad_administrativa uac
				on uac.id_unidad=r.id_unidad_crea left join seg_cat_unidad_administrativa uas
				on uas.id_unidad=r.id_unidad_solicita WHERE r.CORRELATIVO="'.$correlativo.'"';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarDetalleRequisicion($id)
	{
		$sql="SELECT cp.COD_PRODUCTO,cp.NOMBRE_PRODUCTO,ir.ID_PRODUCTO,ir.CANTIDAD,ir.ID_DETALLE_REQUISICION as ID_DETALLE FROM inv_detalle_requisicion ir  inner join inv_cat_producto cp on ir.ID_PRODUCTO=cp.ID_PRODUCTO where ir.id_requisicion=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}
}