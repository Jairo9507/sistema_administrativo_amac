<?php 
/**
 * 
 */
class detalle_ordencompra_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarDetalle(){
		$sql='SELECT a.ID_ORDEN_COMPRA, a.ID_PROVEEDOR, a.UNIDAD_CREA, a.CODIGO_ORDEN_COMPRA CODIGO_ORDEN,
			b.NOMBRE_PROVEEDOR, a.FECHA FECHA_ORDEN, a.USUARIO_CREACION USER, a.FECHA_CREACION, a.ESTADO 
			FROM inv_orden_compra a
			LEFT JOIN inv_cat_proveedor b 
			ON (a.ID_PROVEEDOR = b.ID_PROVEEDOR)
			WHERE  DATE_FORMAT(a.FECHA,"%Y-%m-%d") BETWEEN DATE_SUB(DATE_FORMAT(now(),"%Y-%m-%d"),INTERVAL 4 MONTH) and DATE_FORMAT(LAST_DAY(now()),"%Y-%m-%d")		
			ORDER BY a.FECHA_CREACION DESC;';

		$query = $this->db->query($sql);
		return $query->result();
	}


	public function listarDetalleProductos($id)
	{
		$sql='SELECT d.ID_ORDEN_COMPRA, d.ID_DETALLE_ORDEN_COMPRA ID_DETALLE,d.ID_PRODUCTO,
				 d.DESCRIPCION, CANTIDAD_TOTAL, PRECIO_UNITARIO PRECIO,(CANTIDAD_TOTAL*PRECIO_UNITARIO) TOTAL,
                 ORDEN_COMPRA CODIGO_ORDEN, CODIGO_PRESUPUESTARIO,p.NOMBRE_PRODUCTO,cp.cod_cuenta as CUENTA, CONCAT(cp.COD_CUENTA," ",p.NOMBRE_PRODUCTO) as NOMBRE_CATALOGO
				FROM inv_detalle_orden_compra d 
				LEFT JOIN inv_cat_producto p
				ON (d.id_producto=p.ID_PRODUCTO)
                LEFT JOIN inv_cat_cuentas cp 
                ON (cp.id_cat_cuenta=p.id_cat_cuenta)
                WHERE d.ID_ORDEN_COMPRA='.$id;
			$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarProductos(){
		$sql = 'select * from inv_cat_producto';
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function obtenerTotal($id)
	{
		$sql='SELECT truncate((sum(CANTIDAD_TOTAL*PRECIO_UNITARIO)),2) TOTAL 
				FROM inv_detalle_orden_compra where ID_ORDEN_COMPRA='.$id;
				$query=$this->db->query($sql);
				return $query->result();
	}

	public function valorEnLetras($valor)
	{
		$sql = 'SELECT numeroALetras(VALOR) TOTLETRAS from (SELECT REPLACE("'.$valor.'",",","") AS VALOR) TABLA';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function detalleOrdenCompra($id)
	{
		$sql = 'SELECT  CASE WHEN doc.CODIGO_PRESUPUESTARIO IS NULL THEN CONCAT(cp.COD_CUENTA, " ", doc.DESCRIPCION) ELSE CONCAT(doc.CODIGO_PRESUPUESTARIO, " ", doc.DESCRIPCION ) 
     		END AS DESCRIPCION, doc.CANTIDAD_TOTAL, pto.NOMBRE_PRODUCTO,
			FORMAT(doc.PRECIO_UNITARIO,2) PRECIO_UNITARIO,
			FORMAT((doc.CANTIDAD_TOTAL*doc.PRECIO_UNITARIO),2) TOTAL,
			doc.ID_DETALLE_ORDEN_COMPRA, oc.ID_ORDEN_COMPRA, pro.ID_PROVEEDOR, pto.ID_PRODUCTO,
            doc.CODIGO_PRESUPUESTARIO
			FROM inv_detalle_orden_compra doc
			LEFT JOIN inv_orden_compra oc 
			ON (doc.ID_ORDEN_COMPRA = oc.ID_ORDEN_COMPRA)
			LEFT JOIN inv_cat_proveedor pro
			ON (pro.ID_PROVEEDOR = oc.ID_PROVEEDOR)
			LEFT JOIN inv_cat_producto pto
			ON (pto.ID_PRODUCTO = doc.ID_PRODUCTO)
			LEFT JOIN inv_cat_cuentas cp
			ON (cp.id_cat_cuenta = pto.id_cat_cuenta)
			WHERE oc.ID_ORDEN_COMPRA ='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function totalOrden($id)
	{
		$sql = 'SELECT  case when cantidad_total = 0 then format(PRECIO_UNITARIO,2) 
				else format(SUM(CANTIDAD_TOTAL*PRECIO_UNITARIO),2)   
				end TOTALON
				FROM inv_detalle_orden_compra WHERE id_orden_compra ='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function ordenCompra($id)
	{
		$sql = 'SELECT oc.ID_ORDEN_COMPRA,oc.CODIGO_ORDEN_COMPRA, date_format(oc.FECHA, "%d-%m-%Y") FECHA, pro.NOMBRE_PROVEEDOR, oc.CONDICIONES_PAGO, CONCAT(UCASE(LEFT(oc.VIA, 1)), LCASE(SUBSTRING(oc.VIA, 2))) VIA,  oc.USUARIO_CREACION,u.ID_USUARIO, CONCAT(e.PRIMER_NOMBRE, "", e.PRIMER_APELLIDO) NOMBRE 
		    FROM inv_orden_compra oc
			LEFT JOIN inv_cat_proveedor pro
			ON (pro.ID_PROVEEDOR = oc.ID_PROVEEDOR)
			LEFT JOIN seg_usuario u
			ON (u.USUARIO = oc.USUARIO_CREACION)
			LEFT JOIN seg_empleado e
			ON (u.ID_EMPLEADO = e.ID_EMPLEADO )
			WHERE oc.id_orden_compra ='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

		public function listarOrdenCompraId($id)
	{
		$sql = 'SELECT oc.ID_ORDEN_COMPRA,oc.CODIGO_ORDEN_COMPRA, oc.FECHA, 
				pro.NOMBRE_PROVEEDOR, oc.CONDICIONES_PAGO, CONCAT(UCASE(LEFT(oc.VIA, 1)), LCASE(SUBSTRING(oc.VIA, 2))) VIA, oc.ID_PROVEEDOR,oc.NUMERO_FACTURA
				FROM inv_orden_compra oc
				LEFT JOIN inv_cat_proveedor pro
				ON (pro.ID_PROVEEDOR = oc.ID_PROVEEDOR)
		WHERE id_orden_compra ='.$id;
		 $query = $this->db->query($sql);
		 return $query->result();

	}

	public function listarProveedores()
	{
		$sql = 'SELECT * FROM  inv_cat_proveedor ORDER BY nombre_proveedor ASC';
		$query = $this->db->query($sql);
		 return $query->result();
	}

		public function updateDetalle($id, $updateDetalle)
	{
		$this->db->trans_start();
		$this->db->where("ID_DETALLE_ORDEN_COMPRA",$id);
		$this->db->update("inv_detalle_orden_compra",$updateDetalle);
		$this->db->trans_complete();
	}

	public function updateOrdenCompra($id, $updateOrden)
	{
		$this->db->trans_start();
		$this->db->where("ID_ORDEN_COMPRA",$id);
		$this->db->update("inv_orden_compra",$updateOrden);
		$this->db->trans_complete();
	}



}