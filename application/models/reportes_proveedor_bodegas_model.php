<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_proveedor_bodegas_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarProveedores()
	{
		$sql="SELECT ID_PROVEEDOR, NOMBRE_PROVEEDOR FROM inv_cat_proveedor WHERE ESTADO = 1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarUnidades()
	{
		$sql="SELECT ID_UNIDAD,CODIGO_UNIDAD,DESCRIPCION from seg_cat_unidad_administrativa WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarProveedoresUnidad($fechainicio,$fechafinal,$unidad,$proveedor)
	{
		$sql="SELECT  oc.CODIGO_ORDEN_COMPRA, FECHA,VIA,p.NOMBRE_PROVEEDOR,p.NIT,p.TELEFONO,p.DIRECCION from inv_orden_compra oc inner join inv_cat_proveedor p on oc.id_proveedor=p.id_proveedor 
			where DATE_FORMAT(oc.FECHA,'%Y-%m-%d') between DATE_FORMAT('".$fechainicio."','%Y-%m-%d') and DATE_FORMAT('".$fechafinal."','%Y-%m-%d') and UNIDAD_CREA=".$unidad." ".$proveedor;
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function obtenerBodega($id)
	{
		$sql = 'SELECT 	CONCAT(em.PRIMER_NOMBRE, "  ", em.PRIMER_APELLIDO) NOMBRE, ua.DESCRIPCION 	 FROM seg_empleado em
				LEFT JOIN seg_cat_unidad_administrativa ua 
				ON (em.ID_UNIDAD = ua.ID_UNIDAD)
				LEFT JOIN seg_usuario usu
				ON (usu.ID_EMPLEADO = em.ID_EMPLEADO)
				WHERE usu.ID_USUARIO='.$id;
				$query = $this->db->query($sql);
				return $query->result();
	}	
}