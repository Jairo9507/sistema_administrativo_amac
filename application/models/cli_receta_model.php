<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 
 */
class cli_receta_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}


	public function listarConsultas()
	{
		$sql='SELECT co.ID_CONSULTA,CONCAT(em.PRIMER_NOMBRE," ",em.SEGUNDO_NOMBRE," ",em.PRIMER_APELLIDO," ",em.SEGUNDO_APELLIDO) as nombre_medico, 
			CONCAT(pa.PRIMER_NOMBRE," ",pa.SEGUNDO_NOMBRE," ",pa.PRIMER_APELLIDO," ",pa.SEGUNDO_APELLIDO," ",pa.TERCER_APELLIDO) as nombre_paciente,
			c.ESTADO,c.ASUNTO,DATE_FORMAT(co.FECHA_CONSULTA,"%Y-%m-%d") as FECHA_CONSULTA, ex.CODIGO_EXPEDIENTE,co.DIAGNOSTICO,co.ESTADO
			from cli_cita c inner join cli_medico me on c.ID_MEDICO=me.ID_MEDICO inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO 
			inner join cli_paciente pa on pa.ID_PACIENTE=c.ID_PACIENTE inner join cli_consulta co on co.id_cita=c.id_cita inner join cli_expediente_clinico ex on ex.id_paciente=pa.id_paciente 
			WHERE c.ESTADO<>"En Sala De Espera" and c.ESTADO<>"Cancelada" and DATE_FORMAT(co.FECHA_CONSULTA,"%Y-%m-%d")=DATE_FORMAT(now(),"%Y-%m-%d")';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarCitasFecha($id,$fecha)
	{
		$sql='SELECT co.ID_CONSULTA,CONCAT(em.PRIMER_NOMBRE," ",em.SEGUNDO_NOMBRE," ",em.PRIMER_APELLIDO," ",em.SEGUNDO_APELLIDO) as nombre_medico, 
			CONCAT(pa.PRIMER_NOMBRE," ",pa.SEGUNDO_NOMBRE," ",pa.PRIMER_APELLIDO," ",pa.SEGUNDO_APELLIDO," ",pa.TERCER_APELLIDO) as nombre_paciente,
			c.ESTADO,c.ASUNTO,DATE_FORMAT(co.FECHA_CONSULTA,"%d-%m-%Y") as FECHA_CONSULTA, ex.CODIGO_EXPEDIENTE,IFNULL(co.DIAGNOSTICO,"") as DIAGNOSTICO,co.ESTADO
			from cli_cita c inner join cli_medico me on c.ID_MEDICO=me.ID_MEDICO inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO 
			inner join cli_paciente pa on pa.ID_PACIENTE=c.ID_PACIENTE inner join cli_consulta co on co.id_cita=c.id_cita inner join cli_expediente_clinico ex on ex.id_paciente=pa.id_paciente WHERE c.ESTADO<>"En Sala De Espera" and c.ESTADO<>"Cancelada" and c.ID_MEDICO='.$id.' and DATE_FORMAT(co.FECHA_CONSULTA,"%Y-%m-%d")=DATE_FORMAT("'.$fecha.'","%Y-%m-%d")';
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarConsultaId($id)
	{
		$sql='SELECT co.ID_CONSULTA,CONCAT(em.PRIMER_NOMBRE," ",em.SEGUNDO_NOMBRE," ",em.PRIMER_APELLIDO," ",em.SEGUNDO_APELLIDO) as nombre_medico, 
			CONCAT(pa.PRIMER_NOMBRE," ",pa.SEGUNDO_NOMBRE," ",pa.PRIMER_APELLIDO," ",pa.SEGUNDO_APELLIDO," ",pa.TERCER_APELLIDO) as nombre_paciente,
			c.ESTADO,c.ASUNTO,DATE_FORMAT(co.FECHA_CONSULTA,"%Y-%m-%d") as FECHA_CONSULTA, ex.CODIGO_EXPEDIENTE,co.DIAGNOSTICO
			from cli_cita c inner join cli_medico me on c.ID_MEDICO=me.ID_MEDICO inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO 
			inner join cli_paciente pa on pa.ID_PACIENTE=c.ID_PACIENTE inner join cli_consulta co on co.id_cita=c.id_cita inner join cli_expediente_clinico ex on ex.id_paciente=pa.id_paciente WHERE co.ID_CONSULTA='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarRecetaConsulta($id)
	{
		$sql='SELECT *,(select COUNT(*) from inv_movimientos m where m.id_receta=r.id_receta ) as existe from cli_receta r inner join cli_cat_medicamento me on me.id_medicamento=r.id_medicamento inner join inv_cat_producto p on p.id_producto=me.id_producto inner join cli_consulta co on co.id_consulta=r.id_consulta where co.ID_CONSULTA='.$id;		
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarRecetaPDF($id)
	{
		$sql="SELECT pa.ID_PACIENTE,CONCAT(IFNULL(pa.PRIMER_NOMBRE,''),' ',IFNULL(pa.SEGUNDO_NOMBRE,''),' ',IFNULL(pa.PRIMER_APELLIDO,''),' ',IFNULL(pa.SEGUNDO_APELLIDO,'')) as nombre_paciente, 
			CONCAT(IFNULL(em.PRIMER_NOMBRE,''),' ',IFNULL(em.SEGUNDO_NOMBRE,''),' ',IFNULL(em.PRIMER_APELLIDO,''),' ',IFNULL(em.SEGUNDO_APELLIDO,'')) as nombre_medico,
			p.NOMBRE_PRODUCTO, med.FORMA_FARMACEUTICA,med.PRESENTACION,med.CONCENTRACION,DATE_FORMAT(co.FECHA_CONSULTA,'%Y/%m/%d') as FECHA_CONSULTA,r.INDICACIONES ,r.CANTIDAD,
            co.INDICACIONES as OBSERVACIONES, 
			CASE WHEN (select cantidad from inv_inventario_fisico fi where fi.id_producto=p.id_producto)<r.CANTIDAD 
            THEN 'CANTIDAD INSUFICIENTE O NULA FAVOR COMPRAR EL MEDICAMENTO EN UNA FARMACIA' ELSE '' END MENSAJE,ci.ID_CITA,es.descripcion as ESPECIALIDAD, ex.CODIGO_EXPEDIENTE,p.COD_PRODUCTO
            from cli_receta r inner join cli_cat_medicamento med on med.id_medicamento=r.id_medicamento 
			inner join inv_cat_producto p on p.id_producto=med.id_producto
			inner join cli_consulta co on co.id_consulta=r.id_consulta 
			inner join cli_cita ci on ci.id_cita=co.id_cita
			inner join cli_paciente pa on pa.id_paciente=ci.id_paciente
			inner join cli_expediente_clinico ex on ex.id_paciente=pa.id_paciente
			inner join cli_medico me on me.id_medico=ci.id_medico
			inner join cli_cat_especialidad es on es.ID_CAT_ESPECIALIDAD=me.ID_CAT_ESPECIALIDAD
			inner join seg_empleado em on em.id_empleado=me.id_empleado where co.id_consulta= ".$id;
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function countConsulta($id)
	{
		$sql="SELECT COUNT(*) as contarCitas FROM cli_cita where ID_PACIENTE=".$id.' and EXISTS(SELECT * from cli_antecedente WHERE ID_PACIENTE='.$id.')';
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarExamenesConsulta($id)
	{
		$sql="SELECT DESCRIPCION from cli_detalle_estudios de 
			inner join cli_cat_estudio_clinico ec 
			on ec.ID_CAT_ESTUDIO_CLINICO=de.ID_CAT_ESTUDIO_CLINICO
			where  ec.ESTADO=1 AND (LECTURA<>'' or LECTURA is null) 
			and de.ID_CITA=".$id;

		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarInventario($id)
	{
		$sql="SELECT * from inv_inventario_fisico WHERE ID_PRODUCTO=".$id." and VIGENCIA=1 limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}	
	public function listarHistorico($id,$fecha)
	{
		$sql="SELECT * from cat_historico_inventario WHERE ESTADO=1 and ID_PRODUCTO=".$id." AND DATE_FORMAT(FECHA_INICIO_VIGENCIA,'%Y-%m-%d')<=DATE_FORMAT('".$fecha."','%Y-%m-%d') ORDER BY FECHA_INICIO_VIGENCIA ASC limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function ObtenerMovimientos($producto)
	{
		$sql="select mo.*,SUM(mo.CANTIDAD) as TOTAL from inv_inventario_fisico fi inner join inv_movimientos mo on mo.id_producto=fi.id_producto where mo.id_producto=".$producto." and mo.id_tipo_movimiento=2
			and DATE_FORMAT(mo.FECHA,'%Y-%m-%d') between DATE_FORMAT(fi.FECHA_CREACION,'%Y-%m-%d')
 			and DATE_FORMAT(IFNULL(fi.FECHA_FIN_VIGENCIA,now()),'%Y-%m-%d ') and VIGENCIA=1";
		$query=$this->db->query($sql);
		$cantidad;
		foreach ($query->result_array() as $c) {
			$cantidad=$c['TOTAL'];
		}
		return $cantidad;
	}

	public function ultimoCosto($id)
	{
		$sql='SELECT * from inv_inventario_fisico  where id_producto='.$id.' and VIGENCIA=1 limit 1';
		$query=$this->db->query($sql);
		$costo=array();
		foreach ($query->result_array() as $c) {
			$costo=$c['PRECIO'];
		}		
		if (empty($costo)) {
			$costo=0.0;
		}
		return $costo;
	}		

	public function listarMedicamentos()
	{
		$sql='SELECT me.ID_MEDICAMENTO,p.NOMBRE_PRODUCTO,me.PRESENTACION,p.COD_PRODUCTO from inv_cat_producto p  
		inner join cli_cat_medicamento me on me.ID_PRODUCTO=p.ID_PRODUCTO WHERE ACTIVO=1 ';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarProductoId($id)
	{
		$sql='SELECT * from inv_cat_producto p  
		inner join cli_cat_medicamento me on me.ID_PRODUCTO=p.ID_PRODUCTO WHERE ACTIVO=1 and me.ID_MEDICAMENTO='.$id;
		$query=$this->db->query($sql);
		$producto='';
		foreach ($query->result_array() as $q) {
			$producto=$q['ID_PRODUCTO'];
		}
		return $producto;
	}

	public function listarBrigadas()
	{
		$sql="SELECT ID_BRIGADA,DATE_FORMAT(FECHA_BRIGADA,'%Y-%m-%d') as FECHA,COMUNIDAD, USUARIO_CREACION,FECHA_CREACION,USUARIO_MODIFICACION,FECHA_MODIFICACION FROM cli_brigada WHERE ESTADO=1 and DATE_FORMAT(FECHA_BRIGADA,'%Y-%m-%d') BETWEEN CAST(DATE_FORMAT(NOW() ,'%Y-%m-01') as DATE) and DATE_FORMAT(LAST_DAY(now()),'%Y-%m-%d') order by FECHA DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarBrigadaId($id)
	{
		$sql="SELECT ID_BRIGADA,FECHA_BRIGADA as FECHA, COMUNIDAD,USUARIO_CREACION,FECHA_CREACION,USUARIO_MODIFICACION,FECHA_MODIFICACION FROM cli_brigada WHERE ESTADO=1 and ID_BRIGADA=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}	

	public function listarRecetasBrigada($id)
	{
		$sql="SELECT p.COD_PRODUCTO,CONCAT(p.COD_PRODUCTO,' ',p.NOMBRE_PRODUCTO) as NOMBRE, r.ID_RECETA,me.ID_MEDICAMENTO, r.CANTIDAD,p.ID_PRODUCTO from cli_receta r inner join cli_cat_medicamento me on me.id_medicamento=r.id_medicamento inner join inv_cat_producto p on p.id_producto=me.id_producto inner join cli_brigada b on r.id_brigada=b.id_brigada where r.ID_BRIGADA=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarRecetaId($id)
	{
		$sql="SELECT * from cli_receta where id_receta=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function saveMovimiento($registrarMovimiento)
	{
		$this->db->trans_start();
		$this->db->insert("inv_movimientos",$registrarMovimiento);
		$this->db->trans_complete();
	}

		public function updateInventario($id,$updateInventario)
	{
		$this->db->trans_start();
		$this->db->where("id_inventario_fisico",$id);
		$this->db->update("inv_inventario_fisico",$updateInventario);
		$this->db->trans_complete();
	}

	public function updateReceta($id,$updateReceta)
	{
		$this->db->trans_start();
		$this->db->where("id_receta",$id);
		$this->db->update("cli_receta",$updateReceta);
		$this->db->trans_complete();
	}

	public function saveBrigada($registrarBrigada)
	{
		$this->db->trans_start();
		$this->db->insert("cli_brigada",$registrarBrigada);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function updateBrigada($id,$updateBrigada)
	{
		$this->db->trans_start();
		$this->db->where("id_brigada",$id);
		$this->db->update("cli_brigada",$updateBrigada);
		$this->db->trans_complete();
	}

	public function deleteBrigada($id,$deleteBrigada)
	{
		$this->db->trans_start();
		$this->db->where("id_brigada",$id);
		$this->db->update("cli_brigada",$deleteBrigada);
		$this->db->trans_complete();		
	}

	public function saveReceta($registrarReceta)
	{
		$this->db->trans_start();
		$this->db->insert("cli_receta",$registrarReceta);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}


	public function deleteDetalle($id)
	{
		$this->db->trans_start();
		$this->db->where("ID_RECETA",$id);
		$this->db->delete("cli_receta");
		$this->db->trans_complete();		
	}

	public function deleteMovimiento($idproducto,$idrequisicion)
	{
		$this->db->trans_start();
		$this->db->where("ID_PRODUCTO",$idproducto);
		$this->db->where("ID_RECETA",$idrequisicion);
		$this->db->delete("inv_movimientos");
		$this->db->trans_complete();				
	}	

	public function updateConsulta($id,$updateConsulta)
	{
		$this->db->trans_start();
		$this->db->where("id_consulta",$id);
		$this->db->update("cli_consulta",$updateConsulta);
		$this->db->trans_complete();			
	}

	public function updateHistorico($id,$updateHistorico)
	{
		$this->db->trans_start();
		$this->db->where("ID_HISTORICO_INVENTARIO",$id);
		$this->db->update("cat_historico_inventario",$updateHistorico);
		$this->db->trans_complete();
	}	

}