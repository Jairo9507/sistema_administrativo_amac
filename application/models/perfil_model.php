<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Perfil_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarPerfiles()
	{
		$sql='SELECT * from seg_perfil WHERE ESTADO=1 ORDER BY ID_PERFIL_USUARIO';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function savePerfil($RegistrarPerfil){
		$this->db->trans_start();
		$this->db->insert("seg_perfil", $RegistrarPerfil);
		$this->db->trans_complete();
	}

	public function updatePerfil($id, $UpdatePerfil){
		$this->db->trans_start();
		$this->db->where("ID_PERFIL_USUARIO",$id);
		$this->db->update("seg_perfil",$UpdatePerfil);
		$this->db->trans_complete();
	}

	public function deletePerfil($id,$DeletePerfil){
		$this->db->trans_start();
		$this->db->where("ID_PERFIL_USUARIO",$id);
		$this->db->update("seg_perfil",$DeletePerfil);
		$this->db->trans_complete();
	}

	public function ListarPerfilId($id){
		$sql='SELECT * FROM seg_perfil WHERE ID_PERFIL_USUARIO='.$id.' and ESTADO=1';
		$query=$this->db->query($sql);
		return $query->result();
	}
}