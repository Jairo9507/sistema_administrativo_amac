<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_tipo_obra_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarTipoObra()
	{
		$sql='SELECT ID_CAT_TIPO_OBRA,DESCRIPCION,ESTADO FROM pro_cat_tipo_obra WHERE ESTADO=1';
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function saveTipoObra($registrarTipoObra)
	{
		$this->db->trans_start();
		$this->db->insert('pro_cat_tipo_obra',$registrarTipoObra);
		$this->db->trans_complete();
	}

	public function updateTipoObra($id,$updateTipoObra)
	{
		$this->db->trans_start();
		$this->db->where('ID_CAT_TIPO_OBRA',$id);
		$this->db->update('pro_cat_tipo_obra',$updateTipoObra);
		$this->db->trans_complete();
	}

	public function deleteTipoObra($id,$deleteTipoPrioridad)
	{
		$this->db->trans_start();
		$this->db->where('ID_CAT_TIPO_OBRA',$id);
		$this->db->update('pro_cat_tipo_obra',$deleteTipoPrioridad);
		$this->db->trans_complete();
	}

	public function listarTipoObraId($id)
	{
		$sql='SELECT ID_CAT_TIPO_OBRA,DESCRIPCION,ESTADO FROM pro_cat_tipo_obra WHERE ID_CAT_TIPO_OBRA='.$id.' AND ESTADO=1';
		$query= $this->db->query($sql);
		return $query->result();
	}

}