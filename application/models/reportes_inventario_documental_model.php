<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_inventario_documental_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarUnidades()
	{
		$sql="SELECT * from seg_cat_unidad_administrativa where ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarDocumentosAlmacenados()
	{
		$sql="SELECT sd.CODIGO as CODIGO_SERIE,ss.CODIGO AS CODIGO_SUBSERIE, DATE_FORMAT(d.FECHA_ENTRADA,'%d-%m-%Y') as FECHA_ENTRADA,DATE_FORMAT(d.FECHA_SALIDA,'%d-%m-%Y') as FECHA_SALIDA,d.NO_CAJAS,d.NO_CARPETAS,d.NO_TOMO,d.OTROS,d.NUMERO_FOLIOS, sd.NOMBRE as SERIE, ss.NOMBRE as SUBSERIE, an.CODIGO as ANAQUEL, es.CODIGO_ESTANTE as ESTANTE,p.CODIGO as PASILLO,g.DESCRIPCION as GERENCIA, ua.DESCRIPCION as UNIDAD,d.NOMBRE,d.VIGENCIA_CENTRAL,d.VIGENCIA_GESTION,d.CORRELATIVO_GESTION,d.CORRELATIVO_CENTRAL,d.OTROS,d.FECHA_DOCUMENTACION_INICIO,d.FECHA_DOCUMENTACION_FINAL,d.SOPORTE, d.CONTENIDO as OBSERVACIONES,d.FRECUENCIA_CONSULTA,d.NUMERO_FOLIOS
			from arc_documento d inner join arc_serie_documental sd on sd.ID_SERIE_DOCUMENTAL=d.ID_SERIE_DOCUMENTAL 
			left join arc_sub_serie_documental ss on ss.ID_SUB_SERIE_DOCUMENTAL=d.ID_SUB_SERIE_DOCUMENTAL 
			inner join arc_cat_anaqueles an on an.ID_ANAQUEL=d.ID_CAT_ANAQUEL 
			inner join arc_cat_estantes es on es.ID_ESTANTE=an.ID_ESTANTE 
			inner join arc_cat_pasillo p on p.ID_CAT_PASILLO=es.ID_CAT_PASILLO  
			left join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=d.ID_UNIDAD 
			inner join seg_cat_unidad_administrativa g on g.ID_UNIDAD=d.ID_GERENCIA where d.ESTADO=1 ORDER BY d.CORRELATIVO_CENTRAL +0 ASC ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarDocumentosPrestados($fechainicio,$fechafinal,$unidad)
	{

		$sql="SELECT sd.CODIGO as CODIGO_DOCUMENTO,DATE_FORMAT(d.FECHA_ENTRADA,'%d-%m-%Y') as FECHA_ENTRADA,DATE_FORMAT(d.FECHA_SALIDA,'%d-%m-%Y') as FECHA_SALIDA,d.NO_CAJAS,d.NO_CARPETAS,d.NO_TOMO,d.OTROS,d.NUMERO_FOLIOS, sd.NOMBRE as SERIE, ss.NOMBRE as SUBSERIE, an.CODIGO as ANAQUEL, es.CODIGO_ESTANTE as ESTANTE,p.CODIGO as PASILLO,g.DESCRIPCION as GERENCIA, ua.DESCRIPCION as UNIDAD,d.NOMBRE,d.VIGENCIA_CENTRAL,d.VIGENCIA_GESTION,d.CORRELATIVO_GESTION,d.CORRELATIVO_CENTRAL,d.OTROS,dt.FECHA_REGRESO_ARCHIVO_CENTRAL,dt.FECHA_SALIDA_ARCHIVO_CENTRAL,t.FECHA_TRANSFERENCIA,IFNULL(DATE_FORMAT(dt.FECHA_RETORNO_REAL,'%d-%m-%Y'),'No ha regresado') as FECHA_RETORNO_REAL,uas.DESCRIPCION as UNIDAD_SOLICITA,d.SOPORTE, d.CONTENIDO as OBSERVACIONES, dt.OBSERVACIONES as DETALLE,d.FRECUENCIA_CONSULTA
			from arc_documento d inner join arc_serie_documental sd on sd.ID_SERIE_DOCUMENTAL=d.ID_SERIE_DOCUMENTAL 
			left join arc_sub_serie_documental ss on ss.ID_SUB_SERIE_DOCUMENTAL=d.ID_SUB_SERIE_DOCUMENTAL 
			inner join arc_cat_anaqueles an on an.ID_ANAQUEL=d.ID_CAT_ANAQUEL 
			inner join arc_cat_estantes es on es.ID_ESTANTE=an.ID_ESTANTE 
			inner join arc_cat_pasillo p on p.ID_CAT_PASILLO=es.ID_CAT_PASILLO  
			left join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=d.ID_UNIDAD 
			inner join seg_cat_unidad_administrativa g on g.ID_UNIDAD=d.ID_GERENCIA 
			inner join arc_detalle_transferencia dt on dt.ID_DOCUMENTO=d.ID_DOCUMENTO
			inner join arc_transferencia t on t.ID_TRANSFERENCIA=dt.ID_TRANSFERENCIA
			inner join seg_cat_unidad_administrativa uas on t.ID_UNIDAD_SOLICITA=uas.ID_UNIDAD 
			where DATE_FORMAT(t.FECHA_TRANSFERENCIA,'%Y-%m-%d') BETWEEN DATE_FORMAT('".$fechainicio."','%Y-%m-%d') AND DATE_FORMAT('".$fechafinal."','%Y-%m-%d') ".$unidad." ORDER BY d.CORRELATIVO_CENTRAL +0 ASC";
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarDocumentosEliminados($fechainicio,$fechafinal,$unidad)
	{
		$sql="SELECT sd.CODIGO as CODIGO_DOCUMENTO,DATE_FORMAT(d.FECHA_ENTRADA,'%d-%m-%Y') as FECHA_ENTRADA,DATE_FORMAT(d.FECHA_SALIDA,'%d-%m-%Y') as FECHA_SALIDA,d.NO_CAJAS,d.NO_CARPETAS,d.NO_TOMO,d.OTROS,d.NUMERO_FOLIOS, sd.NOMBRE as SERIE, sd.CODIGO AS CODIGO_SERIE,ss.NOMBRE as SUBSERIE, ss.CODIGO as CODIGO_SUBSERIE ,an.CODIGO as ANAQUEL, es.CODIGO_ESTANTE as ESTANTE,p.CODIGO as PASILLO,g.DESCRIPCION as GERENCIA,g.CODIGO_UNIDAD as CODIGO_GERENCIA ,ua.DESCRIPCION as UNIDAD,ua.CODIGO_UNIDAD,d.NOMBRE,d.VIGENCIA_CENTRAL,d.VIGENCIA_GESTION,d.CORRELATIVO_GESTION,d.CORRELATIVO_CENTRAL,d.OTROS,d.CONTENIDO,d.FECHA_DOCUMENTACION_INICIO,d.FECHA_DOCUMENTACION_FINAL,d.SOPORTE, d.CONTENIDO as OBSERVACIONES,d.FRECUENCIA_CONSULTA
			from arc_documento d inner join arc_serie_documental sd on sd.ID_SERIE_DOCUMENTAL=d.ID_SERIE_DOCUMENTAL 
			left join arc_sub_serie_documental ss on ss.ID_SUB_SERIE_DOCUMENTAL=d.ID_SUB_SERIE_DOCUMENTAL 
			inner join arc_cat_anaqueles an on an.ID_ANAQUEL=d.ID_CAT_ANAQUEL 
			inner join arc_cat_estantes es on es.ID_ESTANTE=an.ID_ESTANTE 
			inner join arc_cat_pasillo p on p.ID_CAT_PASILLO=es.ID_CAT_PASILLO  
			left join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=d.ID_UNIDAD 
			inner join seg_cat_unidad_administrativa g on g.ID_UNIDAD=d.ID_GERENCIA where d.ESTADO=0 and DATE_FORMAT(d.FECHA_SALIDA,'%d-%m-%Y') between DATE_FORMAT('".$fechainicio."','%d-%m-%Y') and DATE_FORMAT('".$fechafinal."','%d-%m-%Y')".$unidad." ORDER BY d.CORRELATIVO_CENTRAL +0 ASC";
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarProximosavencer($fechainicio,$fechafinal,$unidad)
	{
		$sql="SELECT sd.CODIGO as CODIGO_DOCUMENTO,DATE_FORMAT(d.FECHA_ENTRADA,'%d-%m-%Y') as FECHA_ENTRADA,DATE_FORMAT(d.FECHA_SALIDA,'%d-%m-%Y') as FECHA_SALIDA,d.NO_CAJAS,d.NO_CARPETAS,d.NO_TOMO,d.OTROS,d.NUMERO_FOLIOS, sd.NOMBRE as SERIE, sd.CODIGO AS CODIGO_SERIE,ss.NOMBRE as SUBSERIE, ss.CODIGO as CODIGO_SUBSERIE ,an.CODIGO as ANAQUEL, es.CODIGO_ESTANTE as ESTANTE,p.CODIGO as PASILLO,g.DESCRIPCION as GERENCIA,g.CODIGO_UNIDAD as CODIGO_GERENCIA ,ua.DESCRIPCION as UNIDAD,ua.CODIGO_UNIDAD,d.NOMBRE,d.VIGENCIA_CENTRAL,d.VIGENCIA_GESTION,d.CORRELATIVO_GESTION,d.CORRELATIVO_CENTRAL,d.OTROS,d.CONTENIDO,d.FECHA_DOCUMENTACION_INICIO,d.FECHA_DOCUMENTACION_FINAL,d.SOPORTE, d.CONTENIDO as OBSERVACIONES,d.FRECUENCIA_CONSULTA
			from arc_documento d inner join arc_serie_documental sd on sd.ID_SERIE_DOCUMENTAL=d.ID_SERIE_DOCUMENTAL 
			left join arc_sub_serie_documental ss on ss.ID_SUB_SERIE_DOCUMENTAL=d.ID_SUB_SERIE_DOCUMENTAL 
			inner join arc_cat_anaqueles an on an.ID_ANAQUEL=d.ID_CAT_ANAQUEL 
			inner join arc_cat_estantes es on es.ID_ESTANTE=an.ID_ESTANTE 
			inner join arc_cat_pasillo p on p.ID_CAT_PASILLO=es.ID_CAT_PASILLO  
			left join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=d.ID_UNIDAD 
			inner join seg_cat_unidad_administrativa g on g.ID_UNIDAD=d.ID_GERENCIA where d.ESTADO=1  AND DATE_FORMAT(d.FECHA_SALIDA,'%Y-%m-%d' ) BETWEEN DATE_FORMAT('".$fechainicio."','%Y-%m-%d' ) AND DATE_FORMAT('".$fechafinal."','%Y-%m-%d') or (DATE_FORMAT(d.FECHA_SALIDA,'%Y-%m-%d' )< DATE_FORMAT('".$fechafinal."','%Y-%m-%d')) AND d.ESTADO=1 ".$unidad." ORDER BY d.CORRELATIVO_CENTRAL +0 ASC";
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarDocumentosTabla($fechainicio,$fechafinal,$unidad)
	{
		$sql="SELECT sd.CODIGO as CODIGO_DOCUMENTO,DATE_FORMAT(d.FECHA_ENTRADA,'%d-%m-%Y') as FECHA_ENTRADA,
				DATE_FORMAT(d.FECHA_SALIDA,'%d-%m-%Y') as FECHA_SALIDA, sd.NOMBRE as SERIE, sd.CODIGO AS CODIGO_SERIE,ss.NOMBRE as SUBSERIE, 
				ss.CODIGO as CODIGO_SUBSERIE ,an.CODIGO as ANAQUEL, es.CODIGO_ESTANTE as ESTANTE,p.CODIGO as PASILLO,g.DESCRIPCION as GERENCIA,g.CODIGO_UNIDAD as CODIGO_GERENCIA ,
				ua.DESCRIPCION as UNIDAD,ua.CODIGO_UNIDAD,d.NOMBRE,d.VIGENCIA_CENTRAL,d.VIGENCIA_GESTION,d.CORRELATIVO_GESTION,d.CORRELATIVO_CENTRAL,
				d.OTROS,d.CONTENIDO,d.DISPOSICION_FINAL,d.PROCEDIMIENTO,sd.TIPOLOGIA as TIPOLOGIA_SERIE,ss.TIPOLOGIA as TIPOLOGIA_SUBSERIE ,d.FRECUENCIA_CONSULTA
				from arc_documento d inner join arc_serie_documental sd on sd.ID_SERIE_DOCUMENTAL=d.ID_SERIE_DOCUMENTAL 
				left join arc_sub_serie_documental ss on ss.ID_SUB_SERIE_DOCUMENTAL=d.ID_SUB_SERIE_DOCUMENTAL 
				inner join arc_cat_anaqueles an on an.ID_ANAQUEL=d.ID_CAT_ANAQUEL 
				inner join arc_cat_estantes es on es.ID_ESTANTE=an.ID_ESTANTE 
				inner join arc_cat_pasillo p on p.ID_CAT_PASILLO=es.ID_CAT_PASILLO 
				left join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=d.ID_UNIDAD 
				inner join seg_cat_unidad_administrativa g on g.ID_UNIDAD=d.ID_GERENCIA 
				where d.ESTADO=1
			and d.ID_UNIDAD=".$unidad." AND DATE_FORMAT(d.FECHA_SALIDA,'%Y-%m-%d' ) BETWEEN DATE_FORMAT('".$fechainicio."','%Y-%m-%d' ) AND DATE_FORMAT('".$fechafinal."','%Y-%m-%d')  or (DATE_FORMAT(d.FECHA_SALIDA,'%Y-%m-%d' )< DATE_FORMAT('".$fechafinal."','%Y-%m-%d')) ORDER BY d.CORRELATIVO_CENTRAL +0 ASC" ;
		$query=$this->db->query($sql);
		return $query->result();		
	}	
}