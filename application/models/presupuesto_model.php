<?php  

/**
 * 
 */
class presupuesto_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarPresupuesto()
	{
		$sql = 'SELECT ID_PRESUPUESTO, pre.ID_UNIDAD,uni.DESCRIPCION UNIDAD, FECHA_REGISTRO, FECHA_INICIO, FECHA_FIN, USUARIO_CREACION
				from inv_presupuesto pre
				left join seg_cat_unidad_administrativa uni
				on (pre.id_unidad = uni.id_unidad)
				';
				$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarCuentaPresupuestaria()
	{
		$sql='SELECT ID_CAT_CUENTA ID, COD_CUENTA, NOMBRE_CUENTA FROM inv_cat_cuentas';
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function listarProductos()
	{
		$sql='select * FROM inv_cat_producto';
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function listarUnidades()
	{
		$sql='SELECT * FROM seg_cat_unidad_administrativa';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function savePresupuesto($registrarPresupuesto)
	{
		$this->db->trans_start();
		$this->db->insert("inv_presupuesto",$registrarPresupuesto);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}
	public function updatePresupuesto($id,$updatePresupuesto)
	{
		$this->db->trans_start();
		$this->db->where("ID_PRESUPUESTO",$id);
		$this->db->update("inv_presupuesto",$updatePresupuesto);
		$this->db->trans_complete();
	}
	public function saveDetalle($registrarDetalle)
	{
		$this->db->trans_start();
		$this->db->insert("inv_detalle_orden_compra",$registrarDetalle);
		$this->db->trans_complete();
	}
	public function agregarPresupuesto($registrarOrden)
	{
		$this->db->trans_start();
		$this->db->insert("inv_presupuesto",$registrarOrden);		
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();		
		return $last_id;  
	}
	public function agregarProducto($registrarDetalle)
	{
		$this->db->trans_start();
		$this->db->insert("inv_presupuesto_detalle",$registrarDetalle);
		$this->db->trans_complete();
	}

}