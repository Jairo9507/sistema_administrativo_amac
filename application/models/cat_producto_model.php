<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_producto_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarProductos($unidad){
		$sql='SELECT p.ID_PRODUCTO,p.NOMBRE_PRODUCTO,p.MINIMO,p.MAXIMO,tp.DESCRIPCION as TIPO,ua.NOMBRE_CUENTA,um.DESCRIPCION as UNIDAD,IFNULL(p.COD_PRODUCTO,"N/A") as COD_PRODUCTO  from inv_cat_producto p  inner join inv_cat_cuenta_contable tp on tp.id_cat_cuenta_contable=p.inv_cat_cuenta_contable inner join inv_cat_cuentas ua on ua.id_cat_cuenta=p.id_cat_cuenta left join inv_cat_unidad_medida um on um.id_cat_unidad_medida=p.id_cat_unidad_medida WHERE ACTIVO=1 and p.id_unidad_administrativa='.$unidad.' ORDER BY FECHA_CREACION DESC';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarPDF($unidad)
	{
		$sql='SELECT p.ID_PRODUCTO,p.NOMBRE_PRODUCTO,p.MINIMO,p.MAXIMO,tp.CODIGO as TIPO,ua.cod_cuenta as NOMBRE_CUENTA,um.DESCRIPCION as UNIDAD,IFNULL(p.COD_PRODUCTO,"N/A") as COD_PRODUCTO  from inv_cat_producto p  inner join inv_cat_cuenta_contable tp on tp.id_cat_cuenta_contable=p.inv_cat_cuenta_contable inner join inv_cat_cuentas ua on ua.id_cat_cuenta=p.id_cat_cuenta left join inv_cat_unidad_medida um on um.id_cat_unidad_medida=p.id_cat_unidad_medida WHERE ACTIVO=1 and p.id_unidad_administrativa='.$unidad.' ORDER BY ua.COD_CUENTA ASC, p.NOMBRE_PRODUCTO ASC';
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function levantamientoFisico($unidad)
	{
		$sql = 'SELECT * FROM (SELECT ua.COD_CUENTA, tp.CODIGO ,p.NOMBRE_PRODUCTO, 
				CASE WHEN inv.CANTIDAD IS NULL THEN "0"
				ELSE inv.CANTIDAD
				END EXISTENCIAS_SISTEMA
				from inv_cat_producto p 
				 inner join inv_cat_cuenta_contable tp on tp.id_cat_cuenta_contable=p.inv_cat_cuenta_contable 
				 inner join inv_cat_cuentas ua on ua.id_cat_cuenta=p.id_cat_cuenta 
				 left join inv_cat_unidad_medida um ON um.id_cat_unidad_medida=p.id_cat_unidad_medida 
				 LEFT JOIN inv_inventario_fisico inv ON (inv.ID_PRODUCTO = p.ID_PRODUCTO)
				WHERE ACTIVO=1 and p.id_unidad_administrativa='.$unidad.' ORDER BY ua.COD_CUENTA ASC, p.NOMBRE_PRODUCTO ASC)levantamiento
                where EXISTENCIAS_SISTEMA <>0 ;';
			$query=$this->db->query($sql);
			return $query->result();		
	}

	public function nombreUnidad($id)
	{
		$sql="SELECT * from seg_cat_unidad_administrativa where ESTADO=1 and ID_UNIDAD=".$id;
		$query=$this->db->query($sql);
		$nombre='';
		foreach ($query->result_array() as $q) {
			$nombre=$q['DESCRIPCION'];
		}
		return $nombre;
	}

	public function saveProducto($registrarProducto)
	{
		$this->db->trans_start();
		$this->db->insert("inv_cat_producto",$registrarProducto);
		$this->db->trans_complete();
	}


	public function updateProducto($id,$updateProducto)
	{
		$this->db->trans_start();
		$this->db->where("id_producto",$id);
		$this->db->update("inv_cat_producto",$updateProducto);
		$this->db->trans_complete();
	}

	public function updateMovimiento($id,$updateMovimiento)
	{
		$this->db->trans_start();
		$this->db->where("id_movimiento",$id);
		$this->db->update("inv_movimientos",$updateMovimiento);
		$this->db->trans_complete();
	}

	public function updateInventario($id,$updateInventario)
	{
		$this->db->trans_start();
		$this->db->where("id_inventario_fisico",$id);
		$this->db->update("inv_inventario_fisico",$updateInventario);
		$this->db->trans_complete();
	}

	public function deleteProducto($id,$deleteProducto)
	{
		$this->db->trans_start();
		$this->db->where("id_producto",$id);
		$this->db->update("inv_cat_producto",$deleteProducto);
		$this->db->trans_complete();
	}

	public function listarProveedor()
	{
		$sql='SELECT ID_PROVEEDOR,NOMBRE_PROVEEDOR from inv_cat_proveedor ';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarTipoproducto()
	{
		$sql='SELECT * FROM inv_cat_cuenta_contable';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarCuentaAdministrativa()
	{
		$sql='SELECT * from inv_cat_cuentas';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarUnidadMedida()
	{
		$sql='SELECT ID_CAT_UNIDAD_MEDIDA,DESCRIPCION from inv_cat_unidad_medida';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarProductoId($id)
	{
		$sql='SELECT p.ID_PRODUCTO,p.NOMBRE_PRODUCTO,p.MINIMO,p.MAXIMO, p.inv_cat_cuenta_contable as ID_TIPO,ua.id_cat_cuenta,um.id_cat_unidad_medida as ID_UNIDAD,p.COD_PRODUCTO from inv_cat_producto p  inner join inv_cat_cuenta_contable tp on tp.id_cat_cuenta_contable=p.inv_cat_cuenta_contable inner join inv_cat_cuentas ua on ua.id_cat_cuenta=p.id_cat_cuenta left join inv_cat_unidad_medida um on um.id_cat_unidad_medida=p.id_cat_unidad_medida WHERE p.ID_PRODUCTO='.$id;
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarMovimientosProductos($productoid)
	{
		$sql='SELECT mo.ID_MOVIMIENTO,mo.FECHA,mo.CANTIDAD,mo.COSTO_UNITARIO,cm.DESCRIPCION as TIPO_MOVIMIENTO,cm.DESCRIPCION,mo.USUARIO_CREACION,mo.FECHA_CREACION,mo.USUARIO_MODIFICACION,mo.FECHA_MODIFICACION,mo.ID_TIPO_MOVIMIENTO from inv_movimientos mo inner join inv_cat_producto p on mo.ID_PRODUCTO=p.ID_PRODUCTO inner join cat_movimientos cm on cm.ID_CAT_MOVIMIENTO=mo.ID_TIPO_MOVIMIENTO WHERE mo.ID_PRODUCTO='.$productoid.' and DATE_FORMAT(mo.FECHA,"%Y-%m-%d") BETWEEN CAST(DATE_FORMAT(NOW() ,"%Y-%m-01") as DATE) and DATE_FORMAT(LAST_DAY(now()),"%Y-%m-%d")';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarInventarioProductos($productoid)
	{
		$sql='SELECT fi.ID_INVENTARIO_FISICO,fi.FECHA_INGRESO,oc.CODIGO_ORDEN_COMPRA,fi.CANTIDAD,fi.PRECIO,fi.VIGENCIA,fi.USUARIO_CREACION,fi.FECHA_CREACION,fi.USUARIO_MODIFICACION,fi.FECHA_MODIFICACION,oc.CODIGO_ORDEN_COMPRA  FROM inv_inventario_fisico fi left join inv_orden_compra oc on fi.ID_ORDEN_COMPRA=oc.CODIGO_ORDEN_COMPRA inner join inv_cat_producto p on p.id_producto=fi.id_producto WHERE fi.ID_PRODUCTO='.$productoid;
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function obtenerCodigo()
	{
		$sql="SELECT nextval('codigo_producto') CORRELATIVO";
		$query=$this->db->query($sql);
		$cod='';
		foreach ($query->result_array() as $q) {
			$cod=$q['CORRELATIVO'];
		}
		return $cod;
	}

	public function obtenerProductoNombre($nombre,$cuentap,$cuentac)
	{
		$sql="SELECT * from inv_cat_producto WHERE NOMBRE_PRODUCTO='".$nombre."' AND id_cat_cuenta=".$cuentap." AND inv_cat_cuenta_contable=".$cuentac;
		$query=$this->db->query($sql);
		return $query->result();
	}
}