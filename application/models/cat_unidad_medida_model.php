<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_unidad_medida_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarUnidadesMedida()
	{
		$sql='SELECT * FROM inv_cat_unidad_medida WHERE ESTADO=1';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveUnidadMedida($registrarUnidadmedida)
	{
		$this->db->trans_start();
		$this->db->insert("inv_cat_unidad_medida",$registrarUnidadmedida);
		$this->db->trans_complete();
	}

	public function updateUnidadMedida($id,$updateUnidadMedida)
	{
		$this->db->trans_start();
		$this->db->where("id_cat_unidad_medida",$id);
		$this->db->update("inv_cat_unidad_medida",$updateUnidadMedida);
		$this->db->trans_complete();
	}

	public function deleteUnidadMedida($id,$deleteUnidadMedida)
	{
		$this->db->trans_start();
		$this->db->where("id_cat_unidad_medida",$id);
		$this->db->update("inv_cat_unidad_medida",$deleteUnidadMedida);
		$this->db->trans_complete();		
	}

	public function listarUnidadMedidaId($id)
	{
		$sql='SELECT id_cat_unidad_medida,descripcion FROM inv_cat_unidad_medida WHERE ID_CAT_UNIDAD_MEDIDA='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}
}