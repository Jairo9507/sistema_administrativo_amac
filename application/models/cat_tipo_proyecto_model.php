<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_tipo_proyecto_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarTipoProyecto()
	{
		$sql='SELECT ID_CAT_TIPO_PROYECTO,DESCRIPCION,ESTADO FROM pro_cat_tipo_proyecto WHERE ESTADO=1';
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function saveTipoProyecto($registrarTipoProyecto)
	{
		$this->db->trans_start();
		$this->db->insert('pro_cat_tipo_proyecto',$registrarTipoProyecto);
		$this->db->trans_complete();
	}

	public function updateTipoProyecto($id,$updateTipoProyecto)
	{
		$this->db->trans_start();
		$this->db->where('ID_CAT_TIPO_PROYECTO',$id);
		$this->db->update('pro_cat_tipo_proyecto',$updateTipoProyecto);
		$this->db->trans_complete();
	}

	public function deleteTipoProyecto($id,$deleteTipoProyecto)
	{
		$this->db->trans_start();
		$this->db->where('ID_CAT_TIPO_PROYECTO',$id);
		$this->db->update('pro_cat_tipo_proyecto',$deleteTipoProyecto);
		$this->db->trans_complete();
	}

	public function listarTipoProyectoId($id)
	{
		$sql='SELECT ID_CAT_TIPO_PROYECTO,DESCRIPCION,ESTADO FROM pro_cat_tipo_proyecto WHERE ID_CAT_TIPO_PROYECTO='.$id.' AND ESTADO=1';
		$query= $this->db->query($sql);
		return $query->result();
	}

}