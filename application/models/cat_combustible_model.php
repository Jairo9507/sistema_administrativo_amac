<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_combustible_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarCombustible()
	{
		$sql = 'SELECT * FROM vyc_combustible WHERE ESTADO = 1';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function guardarCombustible($registroCombustible)
	{
		$this->db->trans_start();
		$this->db->insert("vyc_combustible",$registroCombustible);
		$this->db->trans_complete();
	}

		public function listarCombustibleId($id)
	{
		$sql = 'SELECT * FROM vyc_combustible WHERE ID_COMBUSTIBLE ='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function updateCombustible($id, $updateCombustible)
	{
		$this->db->trans_start();
		$this->db->where("ID_COMBUSTIBLE",$id);
		$this->db->update("vyc_combustible",$updateCombustible);
		$this->db->trans_complete();
	}

	public function deleteCombustible($id, $deleteCombustible)
	{
		$this->db->trans_start();
		$this->db->where("ID_COMBUSTIBLE",$id);
		$this->db->update("vyc_combustible",$deleteCombustible);
		$this->db->trans_complete();
	}




}