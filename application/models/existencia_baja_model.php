<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 
 */
class existencia_baja_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarExistenciasBajas($unidad)
	{
		$sql='select p.NOMBRE_PRODUCTO,um.DESCRIPCION as MEDIDA, tp.DESCRIPCION as TIPO, p.MINIMO,
			fi.cantidad AS CANTIDAD_MOVIMIENTOS,
			 cp.nombre_cuenta as CUENTA
             from inv_cat_producto p
			left join inv_cat_unidad_medida um on um.id_cat_unidad_medida=p.id_cat_unidad_medida
			inner join inv_cat_cuenta_contable tp on tp.id_cat_cuenta_contable=p.inv_cat_cuenta_contable
 			inner join seg_cat_unidad_administrativa ua on ua.id_unidad=p.id_cat_cuenta
            inner join inv_cat_cuentas cp on cp.id_cat_cuenta=p.id_cat_cuenta
            inner join inv_inventario_fisico fi on fi.id_producto=p.id_producto
		    where fi.cantidad<=p.minimo and  p.minimo<>0 and p.ACTIVO=1
			and p.id_unidad_administrativa='.$unidad;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function contarExistenciasbajas($unidad)
	{
		$sql="select COUNT(*) as row_existencia
             from inv_cat_producto p
			left join inv_cat_unidad_medida um on um.id_cat_unidad_medida=p.id_cat_unidad_medida
			inner join inv_cat_cuenta_contable tp on tp.id_cat_cuenta_contable=p.inv_cat_cuenta_contable
 			inner join seg_cat_unidad_administrativa ua on ua.id_unidad=p.id_cat_cuenta
            inner join inv_cat_cuentas cp on cp.id_cat_cuenta=p.id_cat_cuenta
            inner join inv_inventario_fisico fi on fi.id_producto=p.id_producto
		    where fi.cantidad<=p.minimo and  p.minimo<>0 and p.ACTIVO=1
			and p.id_unidad_administrativa=".$unidad;
		$query=$this->db->query($sql);
		$numero=0;
		foreach ($query->result_array() as $q) {
			$numero=$q['row_existencia'];
		}
		return $numero;
	}
}