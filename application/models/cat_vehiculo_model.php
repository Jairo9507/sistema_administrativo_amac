<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class cat_vehiculo_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarVehiculos()
	{
		$sql = 'SELECT v.ID_VEHICULO, v.CODIGO_VEHICULO, v.PLACA, v.COLOR, v.ANIO, cl.DESCRIPCION CLASE, t.DESCRIPCION TIPO, m.DESCRIPCION MODELO, c.DESCRIPCION COMBUSTIBLE
			FROM vyc_vehiculo v
			LEFT JOIN  vyc_combustible c ON (v.ID_COMBUSTIBLE = c.ID_COMBUSTIBLE)
			LEFT JOIN vyc_tipo_vehiculo t ON (v.ID_TIPO_VEHICULO = t.ID_TIPO_VEHICULO)
			LEFT JOIN vyc_vehiculo_modelo m ON (v.ID_VEHICULO_MODELO = m.ID_VEHICULO_MODELO)
			LEFT JOIN vyc_clase_vehiculo cl ON (v.ID_CLASE_VEHICULO = cl.ID_CLASE_VEHICULO) 
			WHERE v.ESTADO = 1 
			ORDER BY v.FECHA_CREACION DESC';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarClase()
	{
		$sql = 'SELECT * FROM vyc_clase_vehiculo';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function obtenerModeloId($id)
	{
		$sql = 'SELECT * FROM vyc_vehiculo_modelo WHERE ID_VEHICULO_MARCA='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function obtenerTipoId($id)
	{
		$sql = 'SELECT * FROM vyc_tipo_vehiculo WHERE ESTADO=1 AND ID_CLASE_VEHICULO='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarCombustible()
	{
		$sql = 'SELECT * FROM vyc_combustible';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarMarca()
	{
		$sql = 'SELECT * FROM vyc_vehiculo_marca ORDER BY ID_VEHICULO_MARCA';
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function listarUnidad()
	{
		$sql = 'SELECT * FROM seg_cat_unidad_administrativa';
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function listarActividad()
	{
		$sql = 'SELECT * FROM vyc_cat_actividad';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarTipo()
	{
		$sql = 'SELECT * FROM vyc_tipo_vehiculo';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarModelo()
	{
		$sql = 'SELECT * FROM vyc_vehiculo_modelo';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarPlacas()
	{
		$sql = 'SELECT * FROM vyc_vehiculo';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function guardarVehiculo($registrarVehiculo)
	{
		$this->db->trans_start();
		$this->db->insert("vyc_vehiculo", $registrarVehiculo);
		$this->db->trans_complete();
	}

	public function listarVehiculoId($id)
	{
		$sql = 'SELECT v.ID_VEHICULO, v.CODIGO_VEHICULO, v.PLACA, v.COLOR, v.ANIO, v.CAPACIDAD_TANQUE, 
					v.ASIENTOS, v.CAPACIDAD_PESO , v.PESO_VEHICULO PESO, v.NUMERO_MOTOR NMOTOR,
					 v.CHASIS_GRABADO NCHASIS, v.CHASIS_VIN NVIN, cl.DESCRIPCION CLASE,t.DESCRIPCION TIPO, 
					 mo.DESCRIPCION MODELO, c.DESCRIPCION COMBUSTIBLE, c.DESCRIPCION COMBUSTIBLE,a.DESCRIPCION MISION,
					 CONCAT(TRIM(e.PRIMER_NOMBRE), " ", TRIM(e.PRIMER_APELLIDO)) EMPLEADO, v.ID_CLASE_VEHICULO, 
					 v.ID_TIPO_VEHICULO, v.UNIDAD_PERTENECE, v.ID_VEHICULO_MODELO, v.ID_VEHICULO_MARCA, v.ID_COMBUSTIBLE, a.ID_ACTIVIDAD, e.ID_EMPLEADO, cm.ID_CONTROL_MISION
						FROM vyc_vehiculo v
						LEFT JOIN  vyc_combustible c ON (v.ID_COMBUSTIBLE = c.ID_COMBUSTIBLE)
						LEFT JOIN vyc_tipo_vehiculo t ON (v.ID_TIPO_VEHICULO = t.ID_TIPO_VEHICULO)
						LEFT JOIN vyc_vehiculo_modelo mo ON (v.ID_VEHICULO_MODELO = mo.ID_VEHICULO_MODELO)
						LEFT JOIN vyc_clase_vehiculo cl ON (v.ID_CLASE_VEHICULO = cl.ID_CLASE_VEHICULO) 
						LEFT JOIN vyc_control_mision cm ON (v.ID_VEHICULO = cm.ID_VEHICULO)
			            LEFT JOIN vyc_cat_actividad a ON (cm.ID_ACTIVIDAD = a.ID_ACTIVIDAD)
			            LEFT JOIN seg_empleado e ON (cm.ID_EMPLEADO = e.ID_EMPLEADO)
			            WHERE v.ID_VEHICULO ='.$id;
			$query = $this->db->query($sql);
			return $query->result();
	}

	public function updateVehiculo($id, $updateVehiculo)
	{
		$this->db->trans_start();
		$this->db->where("ID_VEHICULO",$id);
		$this->db->update("vyc_vehiculo",$updateVehiculo);
		$this->db->trans_complete();
	}
	public function eliminarVehiculo($id, $eliminarVehiculo)
	{
		$this->db->trans_start();
		$this->db->where("ID_VEHICULO",$id);
		$this->db->update("vyc_vehiculo",$eliminarVehiculo);
		$this->db->trans_complete();
	}

	public function listarEmpleados()
	{
		$sql = 'SELECT ID_EMPLEADO, CONCAT(TRIM(PRIMER_NOMBRE), " " , TRIM(PRIMER_APELLIDO)) NOMBRE FROM seg_empleado';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarCodigos()
	{
		$sql = 'SELECT ID_VEHICULO, CODIGO_VEHICULO,PLACA FROM vyc_vehiculo';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarValesId($id)
	{
		$sql = 'SELECT ID_VALE_COMBUSTIBLE, ID_VEHICULO, NUMERO_EQUIPO, PLACA, FECHA,format(GALONES,2) GALONES, format(VALOR,2) VALOR, KILOMETRAJE, NUMERO_VALE
				FROM vyc_vale_combustible WHERE ID_VEHICULO ='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarMisionesId($id)
	{
		$sql = 'SELECT m.ID_CONTROL_MISION, m.ID_VEHICULO, v.PLACA, m.FECHA_SALIDA, m.FECHA_ENTRADA,
			m.KILOMETRAJE_ENTRADA, m.KILOMETRAJE_SALIDA, m.DESTINO,a.ID_ACTIVIDAD, a.DESCRIPCION ACTIVIDAD, e.ID_EMPLEADO,
			CONCAT(TRIM(e.PRIMER_NOMBRE), " ", TRIM(e.PRIMER_APELLIDO)) EMPLEADO
			FROM vyc_control_mision m
			LEFT JOIN vyc_vehiculo v ON (m.ID_VEHICULO = v.ID_VEHICULO)
			LEFT JOIN seg_empleado e ON (m.ID_EMPLEADO = e.ID_EMPLEADO)
			LEFT JOIN vyc_cat_actividad a ON (m.ID_ACTIVIDAD = a.ID_ACTIVIDAD) WHERE m.ID_VEHICULO ='.$id;
			$query = $this->db->query($sql);
			return $query->result();
	}

	public function listarVehiculosExcel()
	{
		$sql=" SELECT v.CODIGO_VEHICULO,v.PLACA,cv.DESCRIPCION AS CLASE, vt.DESCRIPCION as TIPO,vm.DESCRIPCION as MODELO,
			 mv.DESCRIPCION as MARCA,v.COLOR,v.ANIO,v.NUMERO_MOTOR,v.CHASIS_GRABADO,v.CHASIS_VIN,v.CLASIFICACION,ua.DESCRIPCION as UNIDAD
			 from vyc_vehiculo v 
			 left join vyc_clase_vehiculo cv on v.ID_CLASE_VEHICULO=cv.ID_CLASE_VEHICULO 
			 left join vyc_tipo_vehiculo vt on vt.ID_TIPO_VEHICULO=v.ID_TIPO_VEHICULO
			 left join vyc_vehiculo_modelo vm on vm.ID_VEHICULO_MODELO=v.ID_VEHICULO_MODELO
			 left join vyc_vehiculo_marca mv on mv.ID_VEHICULO_MARCA=v.ID_VEHICULO_MARCA
			 left join seg_cat_unidad_administrativa ua on v.UNIDAD_PERTENECE=ua.ID_UNIDAD WHERE v.ESTADO=1";
			 $query=$this->db->query($sql);
			 return $query->result();
	}



}