<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class archivo_transferencia_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarTransferencias()
	{
		$sql="SELECT tr.*, ua.DESCRIPCION AS UNIDAD, sd.CODIGO as CODIGO_SERIE FROM arc_transferencia tr 
		inner join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=tr.ID_UNIDAD_SOLICITA
		inner join seg_empleado em on em.ID_EMPLEADO=tr.ID_RESPONSABLE_ARCHIVO_GESTION
		inner join arc_detalle_transferencia dt on dt.ID_TRANSFERENCIA=tr.iD_TRANSFERENCIA
		inner join arc_documento d on dt.ID_DOCUMENTO=d.ID_DOCUMENTO
		inner join arc_serie_documental sd on sd.ID_SERIE_DOCUMENTAL=d.ID_SERIE_DOCUMENTAL
		WHERE tr.ESTADO=1 and DATE_FORMAT(tr.FECHA_TRANSFERENCIA,'%Y-%m-%d') 
        BETWEEN DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 6 MONTH), '%Y-%m-%d ') 
        and DATE_FORMAT(LAST_DAY(now()),'%Y-%m-%d') ORDER BY tr.FECHA_TRANSFERENCIA DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarUnidades()
	{
		$sql="SELECT ID_UNIDAD,DESCRIPCION,CODIGO_UNIDAD FROM seg_cat_unidad_administrativa WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEmpleados()
	{
		$sql="SELECT ID_EMPLEADO,IFNULL(CODIGO_EMPLEADO,ID_EMPLEADO) as CODIGO,CONCAT(IFNULL(PRIMER_NOMBRE,''),'&nbsp; ',IFNULL(SEGUNDO_NOMBRE,''),'&nbsp; ',IFNULL(PRIMER_APELLIDO,''),'&nbsp; ',IFNULL(SEGUNDO_APELLIDO,'')) as nombre_completo FROM seg_empleado WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarSeries()
	{
		$sql="SELECT ID_SERIE_DOCUMENTAL,NOMBRE,DESCRIPCION FROM arc_serie_documental WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarSubseriesId($id)
	{
		$sql="SELECT ID_SUB_SERIE_DOCUMENTAL,NOMBRE,DESCRIPCION FROM arc_sub_serie_documental WHERE ESTADO=1 and ID_SERIE_DOCUMENTAL=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarDocumentosSerie($id)
	{
		$sql="SELECT d.ID_DOCUMENTO,d.CODIGO_DOCUMENTO,d.NOMBRE as DOCUMENTO FROM arc_documento d  
			WHERE d.ESTADO in(1,2) and d.ID_SUB_SERIE_DOCUMENTAL=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}	

	public function ultimoCodigo()
	{
		$sql="SELECT nextval('codigo_transferencia') CODIGO";
		$query=$this->db->query($sql);
		$codigo='000';
		foreach ($query->result_array() as $q) {
			$codigo=$q['CODIGO'];
		}
		return $codigo;
	}

	public function listarDocumentoSeries($id)
	{
		$sql="SELECT d.ID_DOCUMENTO,d.CODIGO_DOCUMENTO,d.FECHA_ENTRADA,d.FECHA_SALIDA, sd.ID_SERIE_DOCUMENTAL AS SERIE,ss.ID_SUB_SERIE_DOCUMENTAL AS SUBSERIE, d.ID_UNIDAD ,an.ID_ANAQUEL,d.NOMBRE as DOCUMENTO,d.NO_CAJAS,d.NO_CARPETAS,d.NO_TOMO,d.NUMERO_FOLIOS,d.OTROS,d.SOPORTE,es.ID_ESTANTE,pa.ID_CAT_PASILLO,d.CORRELATIVO_CENTRAL FROM arc_documento d 
			left join arc_serie_documental sd on d.ID_SERIE_DOCUMENTAL=sd.ID_SERIE_DOCUMENTAL 
			left join arc_sub_serie_documental ss on ss.ID_SUB_SERIE_DOCUMENTAL=d.ID_SUB_SERIE_DOCUMENTAL
			inner join arc_cat_anaqueles an on an.ID_ANAQUEL=d.ID_CAT_ANAQUEL 
			inner join arc_cat_estantes es on an.ID_ESTANTE=es.ID_ESTANTE
			inner join arc_cat_pasillo pa on pa.ID_CAT_PASILLO=es.ID_CAT_PASILLO
			inner join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=d.ID_UNIDAD WHERE d.ESTADO in(1,2)    or ss.ID_SUB_SERIE_DOCUMENTAL is null and sd.ID_SERIE_DOCUMENTAL=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarDocumentoSubseries($id)
	{
		$sql="SELECT d.ID_DOCUMENTO,d.CODIGO_DOCUMENTO,d.NOMBRE AS DOCUMENTO,d.CORRELATIVO_CENTRAL FROM arc_documento d 
			left join arc_serie_documental sd on d.ID_SERIE_DOCUMENTAL=sd.ID_SERIE_DOCUMENTAL 
			left join arc_sub_serie_documental ss on ss.ID_SUB_SERIE_DOCUMENTAL=d.ID_SUB_SERIE_DOCUMENTAL
			inner join arc_cat_anaqueles an on an.ID_ANAQUEL=d.ID_CAT_ANAQUEL 
			inner join arc_cat_estantes es on an.ID_ESTANTE=es.ID_ESTANTE
			inner join arc_cat_pasillo pa on pa.ID_CAT_PASILLO=es.ID_CAT_PASILLO
			inner join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=d.ID_UNIDAD WHERE d.ESTADO in(1,2) 
			and ss.ID_SUB_SERIE_DOCUMENTAL=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}	

	public function listarDetallesId($id)
	{
		$sql="SELECT  d.ID_DOCUMENTO,d.NOMBRE AS DOCUMENTO,d.CODIGO_DOCUMENTO,dt.ID_DETALLE_TRANSFERENCIA,DATE_FORMAT(dt.FECHA_REGRESO_ARCHIVO_CENTRAL,'%d-%m-%Y') as FECHA_REGRESO_ARCHIVO_CENTRAL,DATE_FORMAT(dt.FECHA_SALIDA_ARCHIVO_CENTRAL,'%d-%m-%Y') as FECHA_SALIDA_ARCHIVO_CENTRAL,d.ESTADO,d.CORRELATIVO_CENTRAL,dt.OBSERVACIONES,IFNULL(DATE_FORMAT(dt.FECHA_RETORNO_REAL,'%d-%m-%Y'),'') as FECHA_RETORNO_REAL FROM arc_detalle_transferencia dt inner join arc_documento d on d.id_documento=dt.id_documento WHERE ID_TRANSFERENCIA=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarTransferenciaId($id)
	{
		$sql="SELECT * from arc_transferencia WHERE ID_TRANSFERENCIA=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveTransferencia($registrarTransferencia)
	{
		$this->db->trans_start();
		$this->db->insert("arc_transferencia",$registrarTransferencia);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function updateTransferencia($id,$updateTransferencia)
	{
		$this->db->trans_start();
		$this->db->where("id_transferencia",$id);
		$this->db->update("arc_transferencia",$updateTransferencia);
		$this->db->trans_complete();
	}

	public function updateDetalleTransferencia($id,$detalleTransferenica)
	{
		$this->db->trans_start();
		$this->db->where("id_detalle_transferencia",$id);
		$this->db->update("arc_detalle_transferencia",$detalleTransferenica);
		$this->db->trans_complete();
	}

	public function updateDocumento($id,$updateDocumento)
	{
		$this->db->trans_start();
		$this->db->where("id_documento",$id);
		$this->db->update("arc_documento",$updateDocumento);
		$this->db->trans_complete();		
	}

	public function saveDetalle($registrarDetalle)
	{
		$this->db->trans_start();
		$this->db->insert("arc_detalle_transferencia",$registrarDetalle);
		$this->db->trans_complete();
	}

	public function deleteTransferencia($id,$deleteTransferencia)
	{
		$this->db->trans_start();
		$this->db->where("id_transferencia",$id);
		$this->db->update("arc_transferencia",$deleteTransferencia);
		$this->db->trans_complete();		
	}

	public function deleteDetalle($id)
	{
		$this->db->trans_start();
		$this->db->where("ID_DETALLE_TRANSFERENCIA",$id);
		$this->db->delete("arc_detalle_transferencia");
		$this->db->trans_complete();		
	}
}