<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_anaqueles_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarAnaqueles()
	{
		$sql="SELECT an.ID_ANAQUEL,an.DESCRIPCION,an.ESTADO,an.CODIGO,an.NUMERO_ANAQUEL, es.DESCRIPCION as ESTANTE from arc_cat_anaqueles an inner join arc_cat_estantes es on es.ID_ESTANTE=an.ID_ESTANTE WHERE an.ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarAnaquelId($id)
	{
		$sql="SELECT an.ID_ANAQUEL,an.DESCRIPCION,an.ESTADO,an.CODIGO,an.NUMERO_ANAQUEL, es.ID_ESTANTE from arc_cat_anaqueles an inner join arc_cat_estantes es on es.ID_ESTANTE=an.ID_ESTANTE WHERE an.ESTADO=1 and an.ID_ANAQUEL=".$id;
		$query=$this->db->query($sql);
		return $query->result();

	}

	public function listarEstantes()
	{
		$sql="SELECT ID_ESTANTE,CODIGO_ESTANTE,DESCRIPCION from arc_cat_estantes WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function obtenerCodigo()
	{
		$sql="SELECT nextval('codigo_anaquel') CORRELATIVO";
		$query=$this->db->query($sql);
		$cod='';
		foreach ($query->result_array() as $q) {
			$cod=$q['CORRELATIVO'];
		}
		return $cod;
	}

	public function saveAnaquel($registrarEstante)
	{
		$this->db->trans_start();
		$this->db->insert("arc_cat_anaqueles",$registrarEstante);
		$this->db->trans_complete();
	}

	public function updateAnaquel($id,$updateEstante)
	{
		$this->db->trans_start();
		$this->db->where("id_anaquel",$id);
		$this->db->update("arc_cat_anaqueles",$updateEstante);
		$this->db->trans_complete();
	}

	public function deleteAnaquel($id,$deleteEstante)
	{
		$this->db->trans_start();
		$this->db->where("id_anaquel",$id);
		$this->db->update("arc_cat_anaqueles",$deleteEstante);
		$this->db->trans_complete();		
	}
}