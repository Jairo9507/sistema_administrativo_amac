<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 
 */
class documentos_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarDocumentos()
	{
		$sql="SELECT d.ID_DOCUMENTO,ss.CODIGO as CODIGO_DOCUMENTO,d.FECHA_SALIDA, sd.NOMBRE AS SERIE,ss.NOMBRE AS SUBSERIE,g.DESCRIPCION as GERENCIA,de.DESCRIPCION as DEPARTAMENTO,an.NUMERO_ANAQUEL AS ANAQUEL, es.CODIGO_ESTANTE as ESTANTE,p.CODIGO as PASILLO,d.CORRELATIVO_CENTRAL,d.tipo,d.ESTADO, DATE_FORMAT(d.FECHA_DOCUMENTACION_INICIO,'%Y-%m-%d') as FECHA_INICIO,DATE_FORMAT(d.FECHA_DOCUMENTACION_FINAL,'%Y-%m-%d') as FECHA_FINAL,d.NUMERO_FOLIOS,d.SOPORTE,d.CONTENIDO,d.FRECUENCIA_CONSULTA 
			FROM arc_documento d 
			left join arc_serie_documental sd on d.ID_SERIE_DOCUMENTAL=sd.ID_SERIE_DOCUMENTAL 
			left join arc_sub_serie_documental ss on ss.ID_SUB_SERIE_DOCUMENTAL=d.ID_SUB_SERIE_DOCUMENTAL 
			left join arc_cat_anaqueles an on an.ID_ANAQUEL=d.ID_CAT_ANAQUEL 
			left join arc_cat_estantes es on es.ID_ESTANTE=an.ID_ESTANTE
			left join arc_cat_pasillo p on p.ID_CAT_PASILLO=es.ID_CAT_PASILLO
			left join seg_cat_unidad_administrativa g on g.ID_UNIDAD=d.ID_GERENCIA 
			left join seg_cat_unidad_administrativa de on de.ID_UNIDAD=d.ID_UNIDAD 
			WHERE d.ESTADO<>0 ORDER BY d.CORRELATIVO_CENTRAL +0 ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function listarUnidadesExcel(){
		$sql="SELECT distinct(ua.ID_UNIDAD),ua.CODIGO_UNIDAD,ua.DESCRIPCION DEPARTAMENTO,
		IFNULL((SELECT g.DESCRIPCION from seg_cat_unidad_administrativa g where g.id_unidad=ua.unidad_depende),DESCRIPCION) as GERENCIA from arc_documento d inner join 
		 seg_cat_unidad_administrativa ua on d.ID_UNIDAD=ua.ID_UNIDAD WHERE ua.ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function listarDocumentoExcel($id)
	{
		$sql="SELECT d.ID_DOCUMENTO,d.CODIGO_DOCUMENTO,d.FECHA_ENTRADA,d.FECHA_SALIDA, sd.NOMBRE AS SERIE,ss.NOMBRE AS SUBSERIE, 
			d.ID_UNIDAD ,an.ID_ANAQUEL,d.NOMBRE as DOCUMENTO,d.NO_CAJAS,d.NO_CARPETAS,d.NO_TOMO,DATE_FORMAT(d.FECHA_DOCUMENTACION_INICIO,'%Y-%m-%d') as FECHA_INICIO,DATE_FORMAT(d.FECHA_DOCUMENTACION_FINAL,'%Y-%m-%d') as FECHA_FINAL,d.NUMERO_FOLIOS,d.OTROS,d.SOPORTE,es.ID_ESTANTE,pa.ID_CAT_PASILLO,
			d.ID_GERENCIA,d.TIPO,d.VIGENCIA_CENTRAL,d.VIGENCIA_DOCUMENTAL,d.CONTENIDO,
			d.CORRELATIVO_CENTRAL,d.CORRELATIVO_GESTION,d.FECHA_DOCUMENTACION_INICIO
			,d.FECHA_DOCUMENTACION_FINAL,d.VIGENCIA_GESTION,d.ALMACENADO,d.VALOR_PRIMARIO,d.VALOR_SECUNDARIO,
			d.DISPOSICION_FINAL,d.FRECUENCIA_CONSULTA,d.FECHA_SALIDA,d.ESTADO 
			FROM arc_documento d 
			left join arc_serie_documental sd on d.ID_SERIE_DOCUMENTAL=sd.ID_SERIE_DOCUMENTAL 
			left join arc_sub_serie_documental ss on ss.ID_SUB_SERIE_DOCUMENTAL=d.ID_SUB_SERIE_DOCUMENTAL
			inner join arc_cat_anaqueles an on an.ID_ANAQUEL=d.ID_CAT_ANAQUEL 
			inner join arc_cat_estantes es on an.ID_ESTANTE=es.ID_ESTANTE
			inner join arc_cat_pasillo pa on pa.ID_CAT_PASILLO=es.ID_CAT_PASILLO
			inner join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=d.ID_UNIDAD WHERE d.ESTADO<>0 AND d.ID_UNIDAD=".$id." ORDER BY d.CORRELATIVO_CENTRAL +0 ASC ";
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarDocumentoId($id)
	{
		$sql="SELECT d.ID_DOCUMENTO,d.CODIGO_DOCUMENTO,d.FECHA_ENTRADA,d.FECHA_SALIDA, sd.ID_SERIE_DOCUMENTAL AS SERIE,ss.ID_SUB_SERIE_DOCUMENTAL AS SUBSERIE, 
			d.ID_UNIDAD ,an.ID_ANAQUEL,d.NOMBRE as DOCUMENTO,d.NO_CAJAS,d.NO_CARPETAS,d.NO_TOMO,
			d.NUMERO_FOLIOS,d.OTROS,d.SOPORTE,es.ID_ESTANTE,pa.ID_CAT_PASILLO,
			d.ID_GERENCIA,d.TIPO,d.VIGENCIA_CENTRAL,d.VIGENCIA_DOCUMENTAL,d.CONTENIDO,
			d.CORRELATIVO_CENTRAL,d.CORRELATIVO_GESTION,d.FECHA_DOCUMENTACION_INICIO
			,d.FECHA_DOCUMENTACION_FINAL,d.VIGENCIA_GESTION,d.ALMACENADO,d.VALOR_PRIMARIO,d.VALOR_SECUNDARIO,
			d.DISPOSICION_FINAL,d.FRECUENCIA_CONSULTA,d.FECHA_SALIDA,d.ESTADO,d.PROCEDIMIENTO
			FROM arc_documento d 
			left join arc_serie_documental sd on d.ID_SERIE_DOCUMENTAL=sd.ID_SERIE_DOCUMENTAL 
			left join arc_sub_serie_documental ss on ss.ID_SUB_SERIE_DOCUMENTAL=d.ID_SUB_SERIE_DOCUMENTAL
			inner join arc_cat_anaqueles an on an.ID_ANAQUEL=d.ID_CAT_ANAQUEL 
			inner join arc_cat_estantes es on an.ID_ESTANTE=es.ID_ESTANTE
			inner join arc_cat_pasillo pa on pa.ID_CAT_PASILLO=es.ID_CAT_PASILLO
			inner join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=d.ID_UNIDAD WHERE d.ESTADO<>0
			and d.ID_DOCUMENTO=".$id;
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarUnidadComparten($id){
		$sql="SELECT CODIGO_UNIDAD,DESCRIPCION from arc_documentos_unidades_compartidos du 
		left join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=du.ID_UNIDAD WHERE ID_DOCUMENTO=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarSeries()
	{
		$sql="SELECT ID_SERIE_DOCUMENTAL,NOMBRE FROM arc_serie_documental WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarUnidades()
	{
		$sql="SELECT ID_UNIDAD,DESCRIPCION,CODIGO_UNIDAD FROM seg_cat_unidad_administrativa WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarUnidadEditar($id)
	{
		$sql="SELECT ua.ID_UNIDAD,DESCRIPCION,CODIGO_UNIDAD FROM seg_cat_unidad_administrativa ua WHERE ESTADO=1 and not
			exists(SELECT * from arc_documentos_unidades_compartidos where id_documento=".$id." and ID_UNIDAD IN(ua.ID_UNIDAD))";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarSubseries()
	{
		$sql="SELECT ID_SUB_SERIE_DOCUMENTAL,NOMBRE,DESCRIPCION FROM arc_sub_serie_documental WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarSubseriesId($id)
	{
		$sql="SELECT ID_SUB_SERIE_DOCUMENTAL,NOMBRE FROM arc_sub_serie_documental WHERE ESTADO=1 and ID_SERIE_DOCUMENTAL=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarTipos()
	{
		$sql="SELECT ID_CAT_TIPO_DOCUMENTO,DESCRIPCION FROM arc_cat_tipo_documento WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarPasillos()
	{
		$sql="SELECT p.ID_CAT_PASILLO,p.CODIGO,p.DESCRIPCION FROM arc_cat_pasillo p  WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEstantesId($id)
	{
		$sql="SELECT ID_ESTANTE,CODIGO_ESTANTE,DESCRIPCION FROM arc_cat_estantes WHERE ESTADO=1 and ID_CAT_PASILLO=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEstantes()
	{
		$sql="SELECT ID_ESTANTE,CODIGO_ESTANTE,DESCRIPCION FROM arc_cat_estantes WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}	

	public function listarAnquelesId($id)
	{
		$sql="SELECT ID_ANAQUEL,DESCRIPCION FROM arc_cat_anaqueles WHERE ESTADO=1 and ID_ESTANTE=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarAnaqueles()
	{
		$sql="SELECT ID_ANAQUEL,DESCRIPCION,CODIGO,NUMERO_ANAQUEL FROM arc_cat_anaqueles WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}	

	public function listarDepartamentos($id)
	{
		$sql="SELECT ID_UNIDAD, CODIGO_UNIDAD,DESCRIPCION from seg_cat_unidad_administrativa WHERE  UNIDAD_DEPENDE=".$id." union SELECT ID_UNIDAD, CODIGO_UNIDAD,DESCRIPCION from seg_cat_unidad_administrativa WHERE ID_UNIDAD=".$id."  ORDER BY ID_UNIDAD";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function obtenerCodigo()
	{
		$sql="SELECT nextval('codigo_documento') CODIGO_DOCUMENTO";
		$query=$this->db->query($sql);
		$cod='';
		foreach ($query->result_array() as $q) {
			$cod=$q['CODIGO_DOCUMENTO'];
		}
		return $cod;
	}

	public function obtenerCorrelativoCentral()
	{
		$sql="SELECT nextval('codigo_caja') CORRELATIVO";
		$query=$this->db->query($sql);
		$cod='';
		foreach ($query->result_array() as $q) {
			$cod=$q['CORRELATIVO'];
		}
		return $cod;
	}

	public function saveDocumento($registrarDocumento)
	{
		$this->db->trans_start();
		$this->db->insert("arc_documento",$registrarDocumento);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function saveUnidadComparte($registrarUnidadDocumento)
	{
		$this->db->trans_start();
		$this->db->insert("arc_documentos_unidades_compartidos",$registrarUnidadDocumento);
		$this->db->trans_complete();
	}

	public function updateDocumento($id,$updateDocumento)
	{
		$this->db->trans_start();
		$this->db->where("id_documento",$id);
		$this->db->update("arc_documento",$updateDocumento);
		$this->db->trans_complete();
	}

	public function deleteDocumento($id,$deleteDocumento)
	{
		$this->db->trans_start();
		$this->db->where("id_documento",$id);
		$this->db->update("arc_documento",$deleteDocumento);
		$this->db->trans_complete();		
	}
}