<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cli_consulta_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarCitaMedico($id)
	{
		$sql='SELECT c.ID_CITA,me.ID_MEDICO,CONCAT(em.PRIMER_NOMBRE," ",em.SEGUNDO_NOMBRE," ",em.PRIMER_APELLIDO," ",em.SEGUNDO_APELLIDO) as nombre_medico, 
			CONCAT(pa.PRIMER_NOMBRE," ",pa.SEGUNDO_NOMBRE," ",pa.PRIMER_APELLIDO," ",pa.SEGUNDO_APELLIDO," ",pa.TERCER_APELLIDO) as nombre_paciente,
			c.ESTADO,c.ASUNTO,DATE_FORMAT(co.FECHA_CONSULTA,"%Y-%m-%d") as FECHA_CONSULTA, ex.CODIGO_EXPEDIENTE,c.ASUNTO
			from cli_cita c inner join cli_medico me on c.ID_MEDICO=me.ID_MEDICO inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO 
			inner join cli_paciente pa on pa.ID_PACIENTE=c.ID_PACIENTE inner join cli_consulta co on co.id_cita=c.id_cita inner join cli_expediente_clinico ex on ex.id_paciente=pa.id_paciente WHERE c.ESTADO<>"En Sala De Espera" and c.ESTADO<>"Cancelada" and c.ID_MEDICO='.$id.' and DATE_FORMAT(c.FECHA_HORA,"%Y-%m-%d")=DATE_FORMAT(now(),"%Y-%m-%d")';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarCitasFecha($id,$fecha)
	{
		$sql='SELECT c.ID_CITA,me.ID_MEDICO,CONCAT(em.PRIMER_NOMBRE," ",em.SEGUNDO_NOMBRE," ",em.PRIMER_APELLIDO," ",em.SEGUNDO_APELLIDO) as nombre_medico, 
			CONCAT(pa.PRIMER_NOMBRE," ",pa.SEGUNDO_NOMBRE," ",pa.PRIMER_APELLIDO," ",pa.SEGUNDO_APELLIDO," ",pa.TERCER_APELLIDO) as nombre_paciente,
			c.ESTADO,c.ASUNTO,DATE_FORMAT(co.FECHA_CONSULTA,"%Y-%m-%d") as FECHA_CONSULTA, ex.CODIGO_EXPEDIENTE,c.ASUNTO
			from cli_cita c inner join cli_medico me on c.ID_MEDICO=me.ID_MEDICO inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO 
			inner join cli_paciente pa on pa.ID_PACIENTE=c.ID_PACIENTE inner join cli_consulta co on co.id_cita=c.id_cita inner join cli_expediente_clinico ex on ex.id_paciente=pa.id_paciente WHERE c.ESTADO<>"En Sala De Espera" and c.ESTADO<>"Cancelada" and c.ID_MEDICO='.$id.' and DATE_FORMAT(co.FECHA_CONSULTA,"%Y-%m-%d")=DATE_FORMAT("'.$fecha.'","%Y-%m-%d")';

		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarHistorial($id)
	{
		$sql='SELECT ce.DESCRIPCION as ENFERMEDAD_NOMBRE, co.FECHA_CONSULTA, IFNULL(co.DIAGNOSTICO,"N/A") AS DIAGNOSTICO,IFNULL(co.SINTOMA,"N/A") AS SINTOMA
			from cli_cita c inner join cli_paciente pa on pa.ID_PACIENTE=c.ID_PACIENTE inner join cli_consulta co on co.id_cita=c.id_cita left join cli_cat_enfermedad ce on ce.id_cat_enfermedad=co.id_cat_enfermedad  WHERE c.ESTADO="Atendida" and c.ID_PACIENTE='.$id." ORDER BY co.FECHA_CONSULTA DESC limit 10";
		$query=$this->db->query($sql);
		return $query->result();

	}



	public function updateCita($id,$updateCita)
	{
		$this->db->trans_start();
		$this->db->where("ID_CITA",$id);
		$this->db->update("cli_cita",$updateCita);
		$this->db->trans_complete();
	}
	public function saveConsulta($registrarCita)
	{
		$this->db->trans_start();
		$this->db->insert("cli_consulta",$registrarCita);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function saveMenstruacion($registrarMenstruacion)
	{
		$this->db->trans_start();
		$this->db->insert("cli_menstruacion",$registrarMenstruacion);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function saveEmbarazo($registrarEmbarazo)
	{
		$this->db->trans_start();
		$this->db->insert("cli_embarazo",$registrarEmbarazo);
		$last_id =$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function saveInformacionInteres($registrarInformacion)
	{
		$this->db->trans_start();
		$this->db->insert("cli_informacion_interes",$registrarInformacion);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function saveAntecedente($registrarAntecedente)
	{
		$this->db->trans_start();
		$this->db->insert("cli_antecedente",$registrarAntecedente);
		$this->db->trans_complete();
	}

	public function saveEstudio($registrarEstudio)
	{
		$this->db->trans_start();
		$this->db->insert("cli_detalle_estudios",$registrarEstudio);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
	}

	public function saveReceta($registrarReceta)
	{
		$this->db->trans_start();
		$this->db->insert("cli_receta",$registrarReceta);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function saveExploracion($registrarExploracion)
	{
		$this->db->trans_start();
		$this->db->insert("cli_exploracion",$registrarExploracion);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function saveAlergiaPaciente($registrarAlergia)
	{
		$this->db->trans_start();
		$this->db->insert("cli_alergia_paciente",$registrarAlergia);
		$this->db->trans_complete();
	}

	public function updateConsulta($id,$updateCita)
	{
		$this->db->trans_start();
		$this->db->where("ID_CONSULTA",$id);
		$this->db->update("cli_consulta",$updateCita);
		$this->db->trans_complete();
	}

	public function updateAntecedente($id,$updateAntecedente)
	{
		$this->db->trans_start();
		$this->db->where("ID_ANTECEDENTE",$id);
		$this->db->update("cli_antecedente",$updateAntecedente);
		$this->db->trans_complete();
	}

	public function saveEnfermedadConsulta($registrarEnfermedad)
	{
		$this->db->trans_start();
		$this->db->insert('cli_enfermedad_paciente',$registrarEnfermedad);
		$this->db->trans_complete();
	}

	public function updateMenstruacion($id,$updateMenstruacion)
	{
		$this->db->trans_start();
		$this->db->where("ID_MENSTRUACION",$id);
		$this->db->update("cli_menstruacion",$updateMenstruacion);
		$this->db->trans_complete();
	}

	public function updateEmbarazo($id,$updateEmbarazo)
	{
		$this->db->trans_start();
		$this->db->where("ID_EMBARAZO",$id);
		$this->db->update("cli_embarazo",$updateEmbarazo);
		$this->db->trans_complete();
	}

	public function updateInteres($id,$updateInteres)
	{
		$this->db->trans_start();
		$this->db->where("ID_INFORMACION_INTERES",$id);
		$this->db->update("cli_informacion_interes",$updateInteres);
		$this->db->trans_complete();
	}

	public function updateExploracion($id,$updateExploracion)
	{
		$this->db->trans_start();
		$this->db->where("ID_EXPLORACION",$id);
		$this->db->update("cli_exploracion",$updateExploracion);
		$this->db->trans_complete();
	}

	public function deleteReceta($id)
	{
		$this->db->trans_start();
		$this->db->where("ID_RECETA",$id);
		$this->db->delete("cli_receta");
		$this->db->trans_complete();
	}

	public function updateEstudio($id,$updateEstudio)
	{
		$this->db->trans_start();
		$this->db->where("ID_DETALLE_ESTUDIOS",$id);
		$this->db->update("cli_detalle_estudios",$updateEstudio);
		$this->db->trans_complete();
	}


	public function listarEnfermedades()
	{
		$sql="SELECT ID_CAT_ENFERMEDAD,DESCRIPCION,CODIGO FROM cli_cat_enfermedad WHERE ESTADO=1 ";	
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarAlergiaSustancias()
	{
		$sql="SELECT ID_CAT_ALERGIA_SUSTANCIA,DESCRIPCION FROM cli_cat_alergia_sustancia WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarOtrasAlergias()
	{
		$sql="SELECT ID_CAT_OTRA_ALERGIA,DESCRIPCION FROM cli_cat_otras_alergias WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarMedicamentos()
	{
		$sql="SELECT * FROM cli_cat_medicamento me inner join inv_cat_producto pr  
			on pr.id_producto=me.id_producto WHERE ACTIVO=1 ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEstudios()
	{
		$sql="SELECT * FROM cli_cat_estudio_clinico";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEstudiosCita($id)
	{
		$sql="SELECT ec.ID_CAT_ESTUDIO_CLINICO,ec.DESCRIPCION,ds.ESTADO,ds.ID_DETALLE_ESTUDIOS,ds.LECTURA FROM cli_detalle_estudios ds inner join cli_cat_estudio_clinico ec on ec.ID_CAT_ESTUDIO_CLINICO=ds.ID_CAT_ESTUDIO_CLINICO WHERE  ID_CITA=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}


	public function listarConsultaCita($id)
	{
		$sql="SELECT co.ID_CONSULTA,co.ID_CAT_ENFERMEDAD,an.ID_ANTECEDENTE,an.ID_PACIENTE,an.ID_MENSTRUACION,an.ID_EMBARAZO,an.ID_INFORMACION_INTERES,co.ID_CITA,ex.ID_EXPLORACION,
			 an.DATOS_GENERALES,an.HEREDITARIO_FAMILIAR,an.PERSONAL_NOPATOLOGICO,
			 me.PRIMERA_MENSTRUACION,me.CARACTERISTICAS,me.IVSA,me.MENOPAUSIA,me.OTROS as OTROS_MENSTRUACION,
			 emb.TOTAL_EMBARAZO,emb.NUMERO_PARTOS,emb.NUMERO_CESAREAS,emb.NUMERO_ABORTOS,emb.NACIDO_VIVOS,emb.VIVOS_ACTUALES,emb.OTROS as OTROS_EMBARAZO,
			 inf.ULTIMA_PAPANICOLAU,inf.ULTIMA_COLPOSCOPIA, inf.ULTIMA_MAMOGRAFIA,inf.PAREJAS_SEXUALES,inf.METODOS_ANTICONCEPTIVOS,
			 inf.FLUJOS_VAGINALES,inf.PROCEDIMIENTOS_GINECOLOGICOS,inf.HABITOS,inf.OTROS as OTROS_INTERES, inf.CIRUJIAS_PREVIAS,
			 co.SINTOMA,co.OBSERVACION,co.TRATAMIENTO,co.DIAGNOSTICO,co.ID_CAT_ENFERMEDAD,co.INDICACIONES, ex.PESO,ex.TALLA,ex.CADERA,ex.CINTURA,ex.TEMPERATURA,ex.FRECUENCIA_CARDIACA,ex.FRECUENCIA_RESPIRATORIA,
			 ex.PRESION_ARTERIAL,ex.GLUCOSA_LABORATORIO,ex.GLUCOSA_CAPILAR,ex.IMC,ex.COLESTEROL,ex.TRIGLICERIDOS,
			 ex.HEMOGLOBINA,ex.CABEZA,ex.CUELLO,ex.TORAX,ex.ABDOMEN,ex.PULSOS,ex.FACIES,ex.GENITALES,ex.OTROS AS OTROS_EXPLORACION,
			 ex.CINTURA_ABDOMINAL,ex.PLIEGUE_ABDOMINAL,ex.FUERZA_MANO,ex.HDL,ex.LDL,ex.CREATININA,ex.ACIDO_URICO, 
			 ex.TRANSAMINASA,ex.OBSERVACIONES,ci.ID_PACIENTE
			FROM cli_consulta co inner join cli_cita ci on ci.id_cita=co.id_cita 
			inner join cli_paciente pa on pa.id_paciente=ci.id_paciente 
			inner join cli_antecedente an on pa.id_paciente=an.id_paciente 
			inner join cli_exploracion ex on co.id_consulta=ex.id_consulta
            inner join cli_menstruacion me on me.id_menstruacion=an.id_menstruacion
            inner join cli_embarazo emb on emb.id_embarazo=an.id_embarazo
            inner join cli_informacion_interes inf on inf.id_informacion_interes=an.id_informacion_interes
			WHERE  co.ID_CITA=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function buscarPacienteCita($id)
	{
		$sql="SELECT * from cli_cita where ID_CITA=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEnfermedadPaciente($id)
	{
		$sql='SELECT ID_CAT_ENFERMEDAD,CODIGO,DESCRIPCION FROM cli_cat_enfermedad en inner join cli_enfermedad_paciente ep on ep.id_enfermedad=en.ID_CAT_ENFERMEDAD WHERE ep.ID_PACIENTE='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function ObtenerIdPacienteCita($id)
	{
		$sql="SELECT * FROM cli_cita WHERE ID_CITA=".$id;
		$query=$this->db->query($sql);
		$idp;
		foreach ($query->result_array() as $c) {
			$idp=$c['ID_PACIENTE'];
		}
		return $idp;
	}



	public function ExisteAntecedente($id)
	{
		$sql="SELECT * from cli_antecedente WHERE ID_PACIENTE=".$id;
		$query=$this->db->query($sql);
		$antecedente=$query->result();
		$existe;
		if (!empty($antecedente)) {
			$existe=true;
		} else {
			$existe=false;
		}
		return $existe;
	}

	public function listarAlergiasPaciente($id)
	{
		$sql="SELECT ID_ALERGIA_PACIENTE, IFNULL(OTRA_ALERGIA,IFNULL(ac.DESCRIPCION,oa.DESCRIPCION)) nombre_alergia  
			from cli_alergia_paciente ap 
			left join cli_cat_otras_alergias ac on ac.ID_CAT_OTRA_ALERGIA=ap.ID_ALERGIA
			left join cli_cat_alergia_sustancia oa on oa.ID_CAT_ALERGIA_SUSTANCIA=ap.ID_OTRA_ALERGIA 
			WHERE ID_PACIENTE=".$id;
			$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarMedicamentosCita($id)
	{
		$sql="SELECT re.ID_RECETA,p.NOMBRE_PRODUCTO,me.PRESENTACION,re.DOSIS,re.INDICACIONES,re.CANTIDAD from cli_receta re inner join cli_cat_medicamento me on me.id_medicamento=re.id_medicamento inner join inv_cat_producto p on p.id_producto=me.id_producto where re.ID_CONSULTA=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function obtenerIdConsulta($id)
	{
		$sql="SELECT * FROM cli_consulta co inner join cli_cita c on c.id_cita=co.id_cita WHERE co.ID_CITA=".$id;
		$idConsulta='';
		$query=$this->db->query($sql);
		foreach ($query->result_array() as $q) {
			$idConsulta=$q['ID_CONSULTA'];
		}
		return $idConsulta;
	}

}