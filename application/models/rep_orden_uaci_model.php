<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class rep_orden_uaci_model extends CI_Model 
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarProveedor()
	{
		$sql = 'SELECT ID_PROVEEDOR, NOMBRE_PROVEEDOR FROM inv_cat_proveedor WHERE ESTADO = 1';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarProductos()
	{
		$sql = 'SELECT ID_PRODUCTO, NOMBRE_PRODUCTO FROM inv_cat_producto WHERE ACTIVO = 1';
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function listarCuentas()
	{
		$sql = 'SELECT ID_CAT_CUENTA, COD_CUENTA FROM  inv_cat_cuentas';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function obtenerReporte($fechaInicio, $fechaFin, $proveedor, $producto, $descripcion, $concepto, $cuenta,$estado)
	{
		if ($fechaInicio!=0 and $fechaFin!=0 AND $proveedor==0 AND $producto==0 AND $descripcion=='' and $concepto=='' and $cuenta==0 AND $estado==8) {

			$sql = 'SELECT oc.ID_ORDEN_COMPRA, oc.CODIGO_ORDEN_COMPRA, DATE_FORMAT(oc.FECHA,"%Y-%m-%d") as FECHA,p.NOMBRE_PROVEEDOR PROVEEDOR,
			CASE WHEN doc.MTOTAL IS NULL THEN 0.00
			ELSE doc.MTOTAL 
			END MTOTAL, oc.CONDICIONES_PAGO,oc.ID_PROVEEDOR
			FROM (SELECT * FROM inv_orden_compra) oc
			LEFT JOIN (SELECT *, FORMAT(SUM(PRECIO_UNITARIO*CANTIDAD_TOTAL),2) MTOTAL
			FROM inv_detalle_orden_compra group by id_orden_compra) doc
			ON (doc.ID_ORDEN_COMPRA = oc.ID_ORDEN_COMPRA)
			LEFT JOIN inv_cat_proveedor p
			ON (p.ID_PROVEEDOR = oc.ID_PROVEEDOR)
			WHERE oc.FECHA BETWEEN "'.$fechaInicio.'" AND "'.$fechaFin.'" 
			ORDER BY oc.FECHA  ASC';


		}
		else if ($fechaInicio!=0 and $fechaFin!=0 and $proveedor!=0 and $descripcion=='' and $concepto=='' and $cuenta==0 and $estado==8) {
			$sql = 'SELECT oc.ID_ORDEN_COMPRA, oc.CODIGO_ORDEN_COMPRA, DATE_FORMAT(oc.FECHA,"%Y-%m-%d") as FECHA,p.NOMBRE_PROVEEDOR PROVEEDOR,
			CASE WHEN doc.MTOTAL IS NULL THEN 0.00
			ELSE doc.MTOTAL 
			END MTOTAL, oc.CONDICIONES_PAGO,oc.ID_PROVEEDOR
			FROM (SELECT * FROM inv_orden_compra) oc
			LEFT JOIN (SELECT *, FORMAT(SUM(PRECIO_UNITARIO*CANTIDAD_TOTAL),2) MTOTAL
			FROM inv_detalle_orden_compra group by id_orden_compra) doc
			ON (doc.ID_ORDEN_COMPRA = oc.ID_ORDEN_COMPRA)
			LEFT JOIN inv_cat_proveedor p
			ON (p.ID_PROVEEDOR = oc.ID_PROVEEDOR)
			WHERE oc.FECHA BETWEEN "'.$fechaInicio.'" AND "'.$fechaFin.'" AND oc.ID_PROVEEDOR='.$proveedor.'
			ORDER BY oc.FECHA  ASC';
		 }
		 else if ($fechaInicio!=0 and $fechaFin!=0 and $producto!=0 and $proveedor==0 and $descripcion=='' and $concepto=='' and $cuenta==0 and $estado==8) {
			
			$sql = 'SELECT co.ID_ORDEN_COMPRA, co.CODIGO_ORDEN_COMPRA, DATE_FORMAT(co.FECHA,"%Y-%m-%d") as FECHA, pro.NOMBRE_PROVEEDOR PROVEEDOR,
			det.MTOTAL, co.CONDICIONES_PAGO,co.ID_PROVEEDOR
			FROM (SELECT *,FORMAT(SUM(CANTIDAD_TOTAL*PRECIO_UNITARIO),2) MTOTAL
			FROM inv_detalle_orden_compra group by ID_ORDEN_COMPRA) det
			LEFT JOIN inv_detalle_orden_compra oc
			ON(det.ID_DETALLE_ORDEN_COMPRA = oc.ID_DETALLE_ORDEN_COMPRA)
			LEFT JOIN inv_orden_compra co
			ON (oc.ID_ORDEN_COMPRA = co.ID_ORDEN_COMPRA)
			LEFT JOIN inv_cat_proveedor pro
			ON (pro.ID_PROVEEDOR = co.ID_PROVEEDOR)
			WHERE  co.FECHA BETWEEN "'.$fechaInicio.'" AND "'.$fechaFin.'" AND oc.ID_PRODUCTO='.$producto;
		}else if ($fechaInicio!=0 and $fechaFin!=0 and $descripcion!='' and $producto==0 and $proveedor==0 and $concepto=='' and $cuenta==0 and $estado==8) {
			
			$sql =' SELECT co.ID_ORDEN_COMPRA, co.CODIGO_ORDEN_COMPRA, DATE_FORMAT(co.FECHA,"%Y-%m-%d") as FECHA, pro.NOMBRE_PROVEEDOR PROVEEDOR,
					 det.MTOTAL, co.CONDICIONES_PAGO,co.ID_PROVEEDOR
					FROM (SELECT *,FORMAT(SUM(CANTIDAD_TOTAL*PRECIO_UNITARIO),2) MTOTAL
					FROM inv_detalle_orden_compra group by ID_ORDEN_COMPRA) det
					LEFT JOIN inv_detalle_orden_compra oc
					ON(det.ID_DETALLE_ORDEN_COMPRA = oc.ID_DETALLE_ORDEN_COMPRA)
					LEFT JOIN inv_orden_compra co
					ON (oc.ID_ORDEN_COMPRA = co.ID_ORDEN_COMPRA)
					LEFT JOIN inv_cat_proveedor pro
					ON (pro.ID_PROVEEDOR = co.ID_PROVEEDOR)
					WHERE co.FECHA BETWEEN "'.$fechaInicio.'" AND "'.$fechaFin.'" AND oc.DESCRIPCION LIKE "%'.$descripcion.'%"
					ORDER BY co.FECHA  ASC ';
		}else if ($fechaInicio!='' and $fechaFin!='' and $concepto!='' and $producto==0 and $proveedor== 0 and $descripcion=='' and $cuenta==0 and $estado==8) {

	 		$sql = 'SELECT oc.ID_ORDEN_COMPRA, oc.CODIGO_ORDEN_COMPRA, DATE_FORMAT(oc.FECHA,"%Y-%m-%d") as FECHA,p.NOMBRE_PROVEEDOR PROVEEDOR,
					CASE WHEN doc.MTOTAL IS NULL THEN 0.00
					ELSE doc.MTOTAL 
					END MTOTAL, oc.CONDICIONES_PAGO,oc.ID_PROVEEDOR
					FROM (SELECT * FROM inv_orden_compra) oc
					LEFT JOIN (SELECT *, FORMAT(SUM(PRECIO_UNITARIO*CANTIDAD_TOTAL),2) MTOTAL
					FROM inv_detalle_orden_compra group by id_orden_compra) doc
					ON (doc.ID_ORDEN_COMPRA = oc.ID_ORDEN_COMPRA)
					LEFT JOIN inv_cat_proveedor p
					ON (p.ID_PROVEEDOR = oc.ID_PROVEEDOR)
					WHERE oc.FECHA BETWEEN "'.$fechaInicio.'" AND "'.$fechaFin.'" AND oc.VIA LIKE "%'.$concepto.'%" 
					ORDER BY oc.FECHA  ASC';
		}else if ($fechaInicio!='' and $fechaFin!='' and $concepto=='' and $producto==0 and $proveedor== 0 and $descripcion=='' and $cuenta!=0 and $estado==8) {

			$sql = 'SELECT oc.ID_ORDEN_COMPRA, oc.CODIGO_ORDEN_COMPRA, DATE_FORMAT(oc.FECHA,"%Y-%m-%d") as FECHA,p.NOMBRE_PROVEEDOR PROVEEDOR,
					CASE WHEN doc.MTOTAL IS NULL THEN 0.00
					ELSE doc.MTOTAL 
					END MTOTAL, oc.CONDICIONES_PAGO,oc.ID_PROVEEDOR, ctas.COD_CUENTA
					FROM (SELECT * FROM inv_orden_compra) oc
					LEFT JOIN (SELECT *, FORMAT(SUM(PRECIO_UNITARIO*CANTIDAD_TOTAL),2) MTOTAL
					FROM inv_detalle_orden_compra group by id_orden_compra) doc
					ON (doc.ID_ORDEN_COMPRA = oc.ID_ORDEN_COMPRA)
					LEFT JOIN inv_cat_proveedor p
					ON (p.ID_PROVEEDOR = oc.ID_PROVEEDOR)
					LEFT JOIN inv_cat_producto pro
					ON (pro.ID_PRODUCTO = doc.ID_PRODUCTO)
					LEFT JOIN inv_cat_cuentas ctas
					ON (ctas.ID_CAT_CUENTA = pro.ID_CAT_CUENTA)
					WHERE oc.FECHA BETWEEN "'.$fechaInicio.'" AND "'.$fechaFin.'" AND ctas.ID_CAT_CUENTA = "'.$cuenta.'"
					ORDER BY oc.FECHA  ASC';
			}else if ($fechaFin!='' and $fechaInicio!=''  and $producto==0 and $concepto==0 and $cuenta==0 and $proveedor==0 and $estado!=8) {
			
				$sql = ' SELECT oc.ID_ORDEN_COMPRA, oc.CODIGO_ORDEN_COMPRA, oc.FECHA,p.NOMBRE_PROVEEDOR PROVEEDOR,
				CASE WHEN doc.MTOTAL IS NULL THEN 0.00
				ELSE doc.MTOTAL 
				END MTOTAL, oc.CONDICIONES_PAGO,oc.ID_PROVEEDOR
				FROM (SELECT * FROM inv_orden_compra) oc
				LEFT JOIN (SELECT *, FORMAT(SUM(PRECIO_UNITARIO*CANTIDAD_TOTAL),2) MTOTAL
				FROM inv_detalle_orden_compra group by id_orden_compra) doc
				ON (doc.ID_ORDEN_COMPRA = oc.ID_ORDEN_COMPRA)
				LEFT JOIN inv_cat_proveedor p
				ON (p.ID_PROVEEDOR = oc.ID_PROVEEDOR)
				WHERE oc.FECHA BETWEEN "'.$fechaInicio.'" AND "'.$fechaFin.'" AND oc.ESTADO='.$estado.'
				ORDER BY oc.FECHA  ASC';

			}
		$query = $this->db->query($sql);
		return $query->result();
	}

}