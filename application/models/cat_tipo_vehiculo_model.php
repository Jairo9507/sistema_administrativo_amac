<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_tipo_vehiculo_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarTipoVehiculo()
	{
		$sql = 'SELECT t.ID_TIPO_VEHICULO, t.DESCRIPCION DESTIPO, c.DESCRIPCION DESCLASE, t.ESTADO
 			FROM vyc_tipo_vehiculo t
			LEFT JOIN vyc_clase_vehiculo c 
			ON (t.ID_CLASE_VEHICULO = c.ID_CLASE_VEHICULO) WHERE t.ESTADO=1';
			$query = $this->db->query($sql);
			return $query->result();
	}

	public function listarClases()
	{
		$sql = 'SELECT * FROM vyc_clase_vehiculo';
		$query = $this->db->query($sql);
		return $query->result();
	}


	public function guardarTipoVehiculo($registroTipo)
	{
		$this->db->trans_start();
		$this->db->insert("vyc_tipo_vehiculo",$registroTipo);
		$this->db->trans_complete();
	}

	public function listarTipoVehiculoId($id)
	{
		$sql = 'SELECT t.ID_TIPO_VEHICULO, t.DESCRIPCION DESTIPO, t.ID_CLASE_VEHICULO, c.DESCRIPCION DESCLASE
			FROM vyc_tipo_vehiculo t
			LEFT JOIN vyc_clase_vehiculo c 
			ON (t.ID_CLASE_VEHICULO = c.ID_CLASE_VEHICULO) WHERE t.ID_TIPO_VEHICULO='.$id;
			$query = $this->db->query($sql);
			return $query->result();
	}

	public function updateTipoVehiculo($id, $updateTipo)
	{
		$this->db->trans_start();
		$this->db->where("ID_TIPO_VEHICULO",$id);
		$this->db->update("vyc_tipo_vehiculo",$updateTipo);
		$this->db->trans_complete();
	}

	public function deleteTipoVehiculo($id, $deleteTipo)
	{
		$this->db->trans_start();
		$this->db->where("ID_TIPO_VEHICULO",$id);
		$this->db->update("vyc_tipo_vehiculo",$deleteTipo);
		$this->db->trans_complete();
	}


}