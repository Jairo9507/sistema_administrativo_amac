<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class mision_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

		public function guardarMision($registrarMision)
	{
		$this->db->trans_start();
		$this->db->insert("vyc_archivos_misiones", $registrarMision);
		$this->db->trans_complete();
	}

	public function updateMision($id, $updateMision)
	{
		$this->db->trans_start();
		$this->db->where("ID_CONTROL_MISION",$id);
		$this->db->update("vyc_control_mision",$updateMision);
		$this->db->trans_complete();
	}

	// public function guardarArchivoMision($registro)
	// {
		
	// }
}