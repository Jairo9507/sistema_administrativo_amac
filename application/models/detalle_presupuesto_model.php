<?php 
/**
 * 
 */
class detalle_presupuesto_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarDetalle(){
		$sql='select p.ID_PRESUPUESTO, u.DESCRIPCION UNIDAD, p.FECHA_REGISTRO,p.USUARIO_CREACION  from inv_presupuesto p
			left join seg_cat_unidad_administrativa u 
			on (p.ID_UNIDAD = u.ID_UNIDAD) ORDER BY p.ID_PRESUPUESTO DESC;';

		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarPresupuestoId($id)
	{
		$sql = 'SELECT p.ID_PRESUPUESTO, p.FECHA_REGISTRO, p.ID_UNIDAD, b.DESCRIPCION, p.USUARIO_CREACION
				from inv_presupuesto p
				LEFT JOIN seg_cat_unidad_administrativa b
				ON (p.ID_UNIDAD = b.ID_UNIDAD) WHERE p.ID_PRESUPUESTO ='.$id;

		$query = $this->db->query($sql);
		return $query->result();
	}
	public function listarDetalleProductos($id)
	{
		$sql='SELECT *, pro.NOMBRE_PRODUCTO FROM inv_presupuesto_detalle pre
				left join inv_cat_producto pro
				on (pre.ID_PRODUCTO = pro.ID_PRODUCTO)
				WHERE ID_PRESUPUESTO ='.$id;
			$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarProductos(){
		$sql = 'select * from inv_cat_producto';
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function obtenerTotal($id)
	{
		$sql='SELECT  TRUNCATE(SUM(COSTO_APROXIMADO),2) TOTAL
				FROM inv_presupuesto_detalle WHERE ID_PRESUPUESTO='.$id;
				$query=$this->db->query($sql);
				return $query->result();
	}
	public function obtenerCuentaPresupuestaria($id)
	{
		$sql='select distinct id_cuenta_presupuestaria ID_CUENTA, c.NOMBRE_CUENTA
				from inv_presupuesto_detalle p
				LEFT JOIN inv_cat_cuentas c
				ON(p.ID_CUENTA_PRESUPUESTARIA = c.ID_CAT_CUENTA ) 
				where ID_PRESUPUESTO ='.$id;
			$query=$this->db->query($sql);
			return $query->result();
	}
}