<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_proveedor_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarProveedores(){
		$sql='SELECT p.ID_PROVEEDOR,p.NOMBRE_PROVEEDOR, r.DESCRIPCION as RUBRO,SUBSTRING(p.DIRECCION,1,20) as DIRECCION,
CONCAT(SUBSTRING(p.TELEFONO,1,4),"-",SUBSTRING(p.TELEFONO,5,8)) as TELEFONO,
CONCAT(IFNULL(p.PRIMER_NOMBRE_PERSONA_CONTACTO,"")," ",IFNULL(p.SEGUNDO_NOMBRE_PERSONA_CONTACTO,"")," ",
IFNULL(p.PRIMER_APELLIDO_PERSONA_CONTACTO,"")," ",IFNULL(p.SEGUNDO_APELLIDO_PERSONA_CONTACTO,"")) as NOMBRE_CONTACTO,
CONCAT(SUBSTRING(p.NIT,1,4),"-",SUBSTRING(p.NIT,5,6),"-",SUBSTRING(p.NIT,11,3),"-",SUBSTRING(p.NIT,14,1)) as NIT
from inv_cat_proveedor p left join inv_cat_rubros r on r.id_cat_rubro=p.id_cat_rubro WHERE ESTADO=1 ORDER by p.ID_PROVEEDOR DESC 
';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarRubros() {
		$sql='SELECT ID_CAT_RUBRO,DESCRIPCION from inv_cat_rubros';
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function saveProveedor($RegistrarProveedor){
		$this->db->trans_start();
		$this->db->insert("inv_cat_proveedor",$RegistrarProveedor);
		$this->db->trans_complete();
	}

	public function updateProveedor($id,$updateProveedor)
	{
		$this->db->trans_start();
		$this->db->where("ID_PROVEEDOR",$id);
		$this->db->update("inv_cat_proveedor",$updateProveedor);
		$this->db->trans_complete();
	}

	public function deleteProveedor($id,$deleteProveedor){
		$this->db->trans_start();
		$this->db->where("ID_PROVEEDOR",$id);
		$this->db->update("inv_cat_proveedor",$deleteProveedor);
		$this->db->trans_complete();
	}

	public function listarProveedorId($id){
		$sql='SELECT p.ID_PROVEEDOR, r.DESCRIPCION,p.ID_CAT_RUBRO,p.NOMBRE_PROVEEDOR,p.DIRECCION,CONCAT(SUBSTRING(p.TELEFONO,1,4),"-",SUBSTRING(p.TELEFONO,5,8)) as TELEFONO,CONCAT(SUBSTRING(p.NIT,1,4),"-",SUBSTRING(p.NIT,5,6),"-",SUBSTRING(p.NIT,11,3),"-",SUBSTRING(p.NIT,14,1)) as NIT,p.PRIMER_NOMBRE_PERSONA_CONTACTO,p.SEGUNDO_NOMBRE_PERSONA_CONTACTO,p.PRIMER_APELLIDO_PERSONA_CONTACTO,p.SEGUNDO_APELLIDO_PERSONA_CONTACTO,CONCAT(SUBSTRING(p.TELEFONO_CONTACTO,1,4),"-",SUBSTRING(p.TELEFONO_CONTACTO,5,8)) as TELEFONO_CONTACTO,CONCAT(SUBSTRING(p.CELULAR_CONTACTO,1,4),"-",SUBSTRING(p.CELULAR_CONTACTO,5,8)) as CELULAR_CONTACTO  from inv_cat_proveedor p left join inv_cat_rubros r on r.id_cat_rubro=p.id_cat_rubro WHERE ESTADO=1 AND ID_PROVEEDOR='.$id;
		$query=$this->db->query($sql);
		return $query->result();	
	}

	public function buscarProveedor($nit)
	{
		$sql='SELECT p.ID_PROVEEDOR, r.DESCRIPCION,p.ID_CAT_RUBRO,p.NOMBRE_PROVEEDOR,p.DIRECCION,p.TELEFONO,p.NIT from inv_cat_proveedor p left join inv_cat_rubros r on r.id_cat_rubro=p.id_cat_rubro WHERE ESTADO=1 AND NIT="'.$nit.'"';
		$query=$this->db->query($sql);
		return $query->result();	

	}

}


