<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class proyecto_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarProyecto()
	{
		$sql='SELECT p.ID_PROYECTO, p.CODIGO_PROYECTO, p.NOMBRE_PROYECTO, p.FECHA_INICIO_PROYECTO, p.UBICACION, CONCAT(e.PRIMER_NOMBRE," &nbsp;",e.SEGUNDO_NOMBRE,"&nbsp; ",e.PRIMER_APELLIDO," &nbsp;",e.SEGUNDO_APELLIDO) as EMPLEADO, p.NOMBRE_REALIZADOR FROM pro_proyecto p 
		LEFT JOIN seg_empleado e ON e.ID_EMPLEADO=p.ID_SUPERVISOR
		WHERE  p.ESTADO=1';
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function saveProyecto($registrarProyecto)
	{
		$this->db->trans_start();
		$this->db->insert('pro_proyecto',$registrarProyecto);
		$this->db->trans_complete();
	}

	public function updateProyecto($id,$updateProyecto)
	{
		$this->db->trans_start();
		$this->db->where('ID_PROYECTO',$id);
		$this->db->update('pro_proyecto',$updateProyecto);
		$this->db->trans_complete();
	}


	public function deleteProyecto($id,$deleteProyecto)
	{
		$this->db->trans_start();
		$this->db->where('ID_PROYECTO',$id);
		$this->db->update('pro_proyecto',$deleteProyecto);
		$this->db->trans_complete();
	}

	public function listarProyectoId($id)
	{
		$sql='SELECT p.ID_PROYECTO, p.CODIGO_PROYECTO, p.NOMBRE_PROYECTO, p.FECHA_INICIO_PROYECTO, p.FECHA_FINAL_PROYECTO ,p.UBICACION,p.ALCANCE_PROYECTO,p.JUSTIFICANTE_PROYECTO,p.COSTO_PROYECTO ,CONCAT(e.PRIMER_NOMBRE," ",e.SEGUNDO_NOMBRE," ",e.PRIMER_APELLIDO," ",e.SEGUNDO_APELLIDO) as EMPLEADO, p.NOMBRE_REALIZADOR,p.ID_CAT_PRIORIDAD,p.ID_CAT_TIPO_PROYECTO,p.ID_SUPERVISOR FROM pro_proyecto p 
		LEFT JOIN seg_empleado e ON e.ID_EMPLEADO=p.ID_SUPERVISOR
		WHERE p.ID_PROYECTO='.$id.' AND p.ESTADO=1';
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function listarTiposProyectos()
	{
		$sql="SELECT ID_CAT_TIPO_PROYECTO, DESCRIPCION from pro_cat_tipo_proyecto WHERE ESTADO=1";
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function listarPrioridades()
	{
		$sql="SELECT ID_CAT_PRIORIDAD, DESCRIPCION FROM pro_cat_prioridad_proyecto WHERE ESTADO=1";
		$query = $this->db->query($sql);
		return $query->result();
	}

}