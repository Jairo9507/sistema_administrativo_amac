<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_tipo_documento_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarTiposDocumentos()
	{
		$sql="SELECT ID_CAT_TIPO_DOCUMENTO,DESCRIPCION,ESTADO FROM arc_cat_tipo_documento WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}


	public function saveTipoDocumento($registrarTipo)
	{
		$this->db->trans_start();
		$this->db->insert("arc_cat_tipo_documento",$registrarTipo);
		$this->db->trans_complete();
	}

	public function updateTipoDocumento($id,$updateTipoDocumento)
	{
		$this->db->trans_start();
		$this->db->where("id_cat_tipo_documento",$id);
		$this->db->update("arc_cat_tipo_documento",$updateTipoDocumento);
		$this->db->trans_complete();
	}

	public function deleteTipoDocumento($id,$deleteTipoDocumento)
	{
		$this->db->trans_start();
		$this->db->where("id_cat_tipo_documento",$id);
		$this->db->update("arc_cat_tipo_documento",$deleteTipoDocumento);
		$this->db->trans_complete();		
	}
}