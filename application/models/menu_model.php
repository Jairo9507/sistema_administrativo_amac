<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Menu_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function MenuTree($id)
	{
		$sql='SELECT g.ID_PERFIL_USUARIO, P.PRIMER_NOMBRE, P.PRIMER_APELLIDO, AC.ACTIVO,
		AC.ID_MENU, M.ID_MENU as IdMenu, M.LINEA as Linea, M.DESCRIPCION 
		as Descripcion, M.URL_MENU as URL,AC.ID_ACCESO_USUARIO,M.TREEVIEW,M.ID_DEPENDE,M.ICONO 
		from seg_usuario U inner join seg_usuarios_perfiles gu on U.ID_USUARIO=gu.ID_USUARIO
		inner join seg_perfil g on gu.ID_PERFIL_USUARIO=g.ID_PERFIL_USUARIO
		inner join seg_acceso_usuario AC on AC.id_perfil_usuario=g.id_perfil_usuario
		inner join seg_menu M on AC.ID_MENU=M.ID_MENU
		left join seg_empleado as P on U.ID_EMPLEADO=P.ID_EMPLEADO
		where U.ESTADO_USUARIO = 1 and U.ID_USUARIO='.$id.' and AC.ACTIVO=1 and LINEA <> 2 ORDER by ORDEN ASC';
		$query=$this->db->query($sql);
 		return $query->result();
	}	

	public function Submenu($idusuario){
		$sql=' SELECT  g.ID_PERFIL_USUARIO, P.PRIMER_NOMBRE, P.PRIMER_APELLIDO, AC.ACTIVO,
		AC.ID_MENU, M.ID_MENU as IdMenu, M.LINEA as Linea, M.DESCRIPCION 
		as Descripcion, M.URL_MENU as URL,AC.ID_ACCESO_USUARIO,M.TREEVIEW,M.ID_DEPENDE,M.ICONO
		from seg_usuario U inner join seg_usuarios_perfiles gu on U.ID_USUARIO=gu.ID_USUARIO
		inner join seg_perfil g on gu.ID_PERFIL_USUARIO=g.ID_PERFIL_USUARIO
		inner join seg_acceso_usuario AC on AC.id_perfil_usuario=g.id_perfil_usuario
		inner join seg_menu M on AC.ID_MENU=M.ID_MENU        
		left join seg_empleado as P on U.ID_EMPLEADO=P.ID_EMPLEADO        
	 	where U.ESTADO_USUARIO = 1 and U.ID_USUARIO='.$idusuario.' and AC.ACTIVO=1 and LINEA = 2 ORDER by ORDEN ASC';
	 	$query=$this->db->query($sql);
	 	return $query->result();
	}
}