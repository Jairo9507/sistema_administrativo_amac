<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_consumo_vales_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarVehiculos()
	{
		$sql="SELECT * from vyc_vehiculo where ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}


	public function listarUnidadVehiculo($unidad)
	{
		$sql="SELECT * from vyc_vehiculo where ESTADO=1 and UNIDAD_PERTENECE=".$unidad;
		$query=$this->db->query($sql);
		return $query->result();		
	}
	public function listarUnidades()
	{
		$sql="SELECT * from seg_cat_unidad_administrativa where ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarVales($fechainicio,$fechafinal,$vehiculo,$unidad)
	{

		$sql="SELECT v.CODIGO_VEHICULO, v.PLACA,DATE_FORMAT(vc.FECHA,'%Y-%m-%d') as FECHA,vc.GALONES, vc.KILOMETRAJE,vc.NUMERO_VALE,ua.DESCRIPCION as UNIDAD,ua.CODIGO_UNIDAD, vc.VALOR from vyc_vale_combustible vc inner join vyc_vehiculo v on v.ID_VEHICULO=vc.ID_VEHICULO inner join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=v.UNIDAD_PERTENECE where v.ESTADO=1 and DATE_FORMAT(vc.FECHA,'%Y-%m-%d') between DATE_FORMAT('".$fechainicio."','%Y-%m-%d') and DATE_FORMAT('".$fechafinal."','%Y-%m-%d') and v.UNIDAD_PERTENECE=".$unidad."".$vehiculo." ORDER BY vc.NUMERO_VALE,vc.FECHA DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function nombreJefatura($id)
	{
		$sql="SELECT * from seg_usuario us inner join  seg_empleado em on em.ID_EMPLEADO=us.ID_EMPLEADO inner join seg_usuarios_perfiles up on up.id_usuario=us.id_usuario inner join seg_perfil p on p.ID_PERFIL_USUARIO=up.ID_PERFIL_USUARIO  WHERE p.ID_PERFIL_USUARIO=14 and em.ID_UNIDAD=".$id;
		$query=$this->db->query($sql);
		$empleado='';
		foreach ($query->result_array() as $q) {
			$empleado=$q['PRIMER_NOMBRE']." ".$q['PRIMER_APELLIDO'];			
		}
		return $empleado;	
	}
}