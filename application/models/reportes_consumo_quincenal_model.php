<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_consumo_quincenal_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarVales($fechaInicio,$fechaFin)
	{
		$sql='SELECT vc.ID_VEHICULO, v.CODIGO_VEHICULO, vc.NUMERO_EQUIPO, v.PLACA, date_format(vc.FECHA,"%d/%m/%Y") FECHA,
				 vc.OBSERVACIONES, vc.NUMERO_VALE,vc.VALOR 
				FROM vyc_vale_combustible vc
				LEFT JOIN vyc_vehiculo v
				ON (vc.ID_VEHICULO = v.ID_VEHICULO)
				WHERE DATE_FORMAT(vc.FECHA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND DATE_FORMAT("'.$fechaFin.'","%Y-%m-%d") ';

		$query=$this->db->query($sql);
		return $query->result();
	}
	public function listarValesDetalle($fecha,$id)
	{
		$sql='SELECT vc.ID_VEHICULO, v.CODIGO_VEHICULO, vc.NUMERO_EQUIPO, v.PLACA, date_format(vc.FECHA,"%d-%m-%Y") FECHA,vc.GALONES,
				 vc.OBSERVACIONES, vc.NUMERO_VALE,vc.VALOR 
				FROM vyc_vale_combustible vc
				LEFT JOIN vyc_vehiculo v
				ON (vc.ID_VEHICULO = v.ID_VEHICULO)
				WHERE DATE_FORMAT(vc.FECHA,"%Y-%m-%d")= "'.$fecha.'" AND vc.ID_VEHICULO='.$id;

		$query=$this->db->query($sql);
		return $query->result();
	}
	public function listarDias($fechaInicio, $fechaFin)
	{
		$sql = 'SELECT selected_date, EXTRACT(DAY FROM selected_date) DIA_FECHA,
				CASE WHEN WEEKDAY(selected_date)= 0 THEN "Lunes"
					WHEN WEEKDAY(selected_date)= 1 THEN "Martes"
					WHEN WEEKDAY(selected_date)= 2 THEN "Miércoles"
					WHEN WEEKDAY(selected_date)= 3 THEN "Jueves"
					WHEN WEEKDAY(selected_date)= 4 THEN "Viernes"
					WHEN WEEKDAY(selected_date)= 5 THEN "Sábado"
					WHEN WEEKDAY(selected_date)= 6 THEN "Domingo"
				END DIA_SEMANA
				from
				(select adddate("1970-01-01",t4*10000 + t3*1000 + t2*100 + t1*10 + t0) 
				selected_date from
				  (select 0 t0 union select 1 union select 2 union select 3 union select 
				4 union select 5 union select 6 union select 7 union select 8 union 
				select 9) t0,
				  (select 0 t1 union select 1 union select 2 union select 3 union select 
				4 union select 5 union select 6 union select 7 union select 8 union 
				select 9) t1,
				  (select 0 t2 union select 1 union select 2 union select 3 union select 
				4 union select 5 union select 6 union select 7 union select 8 union 
				select 9) t2,
				  (select 0 t3 union select 1 union select 2 union select 3 union select 
				4 union select 5 union select 6 union select 7 union select 8 union 
				select 9) t3,
				  (select 0 t4 union select 1 union select 2 union select 3 union select 
				4 union select 5 union select 6 union select 7 union select 8 union 
				select 9) t4) v 
				where selected_date between "'.$fechaInicio.'" and "'.$fechaFin.'"';
				$query=$this->db->query($sql);
				return $query->result();
	}

	public function listarVehiculos()
	{ 
		$sql="SELECT CODIGO_VEHICULO,ID_VEHICULO,PLACA from vyc_vehiculo WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function obtenerTotales($fechaInicio,$fechaFin)
	{
		$sql=' SELECT SUM(vc.VALOR) as TOTAL_VALOR,SUM(vc.GALONES) as TOTAL_GALONES,vc.ID_VEHICULO,vc.FECHA
				FROM vyc_vale_combustible vc
				LEFT JOIN vyc_vehiculo v
				ON (vc.ID_VEHICULO = v.ID_VEHICULO)
				WHERE DATE_FORMAT(vc.FECHA,"%Y-%m-%d") 
                BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND DATE_FORMAT("'.$fechaFin.'","%Y-%m-%d") GROUP BY vc.FECHA ';
        $query=$this->db->query($sql);
        return $query->result();
	}

}