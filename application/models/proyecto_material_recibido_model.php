<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class proyecto_material_recibido_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function saveMaterialRecibido($registarMaterial)
	{
		$this->db->trans_start();
		$this->db->insert('pro_proyecto_material_recibido',$registarMaterial);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;  
	}

	public function saveMaterialRecibidoOrden($registarMaterialOrden)
	{
		$this->db->trans_start();
		$this->db->insert('pro_material_recibido_orden_compra',$registarMaterialOrden);
		$this->db->trans_complete();
	}

	public function saveMaterialOrdenDetalle($registrarMaterialDetalle)
	{
		$this->db->trans_start();
		$this->db->insert('pro_proyecto_material_recibido_detalle',$registrarMaterialDetalle);
		$this->db->trans_complete();		
	}

	public function updateMaterialRecibido($id,$updateMaterialRecibido)
	{
		$this->db->trans_start();
		$this->db->where('ID_PROYECTO_MATERIAL_RECIBIDO',$id);
		$this->db->update('pro_proyecto_material_recibido',$updateMaterialRecibido);
		$this->db->trans_complete();
	}

	public function updateMaterialOrdenDetalle($id,$updateMaterialDetalle)
	{
		$this->db->trans_start();
		$this->db->where('ID_MATERIAL_RECIBIDO_DETALLE',$id); 
		$this->db->update('pro_proyecto_material_recibido_detalle',$updateMaterialDetalle);
		$this->db->trans_complete();
	}


	public function deleteDetalleMaterialRecibido($id,$deleteProyecto)
	{
		$this->db->trans_start();
		$this->db->from("pro_proyecto_material_recibido_detalle");
		$this->db->where("ID_MATERIAL_RECIBIDO_DETALLE",$id);
		$this->db->delete("inv_requisicion");
		$this->db->trans_complete();
	}


	public function obtenerMaterialRecibidoId($id)
	{
		$sql="SELECT * FROM pro_proyecto_material_recibido WHERE ID_PROYECTO=".$id;
		$query = $this->db->query($sql);
		$materialId;
		if (!empty($query->result_array())) {
			foreach ($query->result_array() as $q) {
				$materialId=$q['ID_PROYECTO_MATERIAL_RECIBIDO'];
			}
		} else {
			$materialId=0;
		}
		return $materialId;				
	}

	public function listarOrdenesMateriales($id)
	{
		$sql="SELECT b.* FROM pro_material_recibido_orden_compra a INNER JOIN inv_orden_compra b ON a.ID_ORDEN_COMPRA=b.ID_ORDEN_COMPRA WHERE ID_MATERIAL_RECIBIDO=".$id;
		$query = $this->db->query($sql);
		return $query->result();		
	}

	public function listarDetallesMaterial($id)
	{
		$sql="SELECT b.DESCRIPCION, b.CANTIDAD_TOTAL, a.ID_MATERIAL_RECIBIDO_DETALLE ,a.CANTIDAD_RECIBIDA,a.FECHA_RECIBIDA,a.ID_PROYECTO_MATERIAL_RECIBIDO
		FROM pro_proyecto_material_recibido_detalle a
		LEFT JOIN inv_detalle_orden_compra b ON a.ID_DETALLE_ORDEN_COMPRA=b.ID_DETALLE_ORDEN_COMPRA
		WHERE a.ID_PROYECTO_MATERIAL_RECIBIDO=".$id.' ORDER BY a.FECHA_RECIBIDA ASC';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarDetallesCompra($id)
	{
		$sql="SELECT B.DESCRIPCION,B.CANTIDAD_TOTAL,ID_MATERIAL_RECIBIDO,B.ID_DETALLE_ORDEN_COMPRA,IFNULL((SELECT SUM(CANTIDAD_RECIBIDA) FROM pro_proyecto_material_recibido_detalle WHERE ID_PROYECTO_MATERIAL_RECIBIDO=C.ID_MATERIAL_RECIBIDO AND ID_DETALLE_ORDEN_COMPRA=B.ID_DETALLE_ORDEN_COMPRA),0) as CANTIDAD_RECIBIDA FROM inv_orden_compra A
		INNER JOIN inv_detalle_orden_compra B ON A.ID_ORDEN_COMPRA=B.ID_ORDEN_COMPRA
		INNER JOIN pro_material_recibido_orden_compra C ON C.ID_ORDEN_COMPRA=A.ID_ORDEN_COMPRA
		WHERE ID_MATERIAL_RECIBIDO=".$id;
		$query = $this->db->query($sql);
		return $query->result();

	}




}