<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_clase_vehiculo_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarClaseVehiculo()
	{
		$sql = 'SELECT * FROM vyc_clase_vehiculo where ESTADO = 1';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function guardarClaseVehiculo($registroClaseVehiculo)
	{
		$this->db->trans_start();
		$this->db->insert("vyc_clase_vehiculo",$registroClaseVehiculo);
		$this->db->trans_complete();
	}

	public function listarClaseVehiculoId($id)
	{
		$sql = 'SELECT * FROM vyc_clase_vehiculo where ID_CLASE_VEHICULO ='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function updateClaseVehiculo($id, $updateClaseVehiculo)
	{
		$this->db->trans_start();
		$this->db->where("ID_CLASE_VEHICULO",$id);
		$this->db->update("vyc_clase_vehiculo",$updateClaseVehiculo);
		$this->db->trans_complete();
	}

	public function deleteClaseVehiculo($id, $deleteClaseVehiculo)
	{
		$this->db->trans_start();
		$this->db->where("ID_CLASE_VEHICULO",$id);
		$this->db->update("vyc_clase_vehiculo",$deleteClaseVehiculo);
		$this->db->trans_complete();
	}
}
