<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class requisicion_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarRequisiciones($unidad)
	{
		$sql='SELECT r.ID_REQUISICION, r.CORRELATIVO,r.FECHA,uac.DESCRIPCION as unidad_crea,uas.DESCRIPCION as unidad_solicita,r.USUARIO_CREACION,r.FECHA_CREACION  
				FROM inv_requisicion_empleado re left join  inv_requisicion r
				on re.id_requisicion=r.id_requisicion left join seg_usuario u 
				on u.id_usuario=re.id_empleado_usuario left join seg_cat_unidad_administrativa uac
				on uac.id_unidad=r.id_unidad_crea left join seg_cat_unidad_administrativa uas
				on uas.id_unidad=r.id_unidad_solicita WHERE r.ID_UNIDAD_CREA='.$unidad.' and r.ESTADO=1 /*and DATE_FORMAT(r.FECHA,"%Y-%m-%d") BETWEEN DATE_SUB(DATE_FORMAT(now(),"%Y-%m-%d"),INTERVAL 2 MONTH) and DATE_FORMAT(LAST_DAY(now()),"%Y-%m-%d")*/ ORDER BY r.FECHA DESC';
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function saveRequisicion($registrarRequisicion)
	{
		$this->db->trans_start();
		$this->db->insert("inv_requisicion",$registrarRequisicion);		
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		
		return $last_id;  
	}

	public function saveRequisicionEmpleado($registrarRequisicion)
	{
		$this->db->trans_start();
		$this->db->insert("inv_requisicion_empleado",$registrarRequisicion);
		$this->db->trans_complete();
	}

	public function saveDetalleRequisicion($registrarDetalle)
	{
		$this->db->trans_start();
		$this->db->insert("inv_detalle_requisicion",$registrarDetalle);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function saveMovimiento($registrarSalida)
	{
		$this->db->trans_start();
		$this->db->insert("inv_movimientos",$registrarSalida);
		$this->db->trans_complete();
	}

	public function ultimoCosto($id)
	{
		$sql='SELECT * from inv_detalle_orden_compra where id_producto='.$id.' ORDER BY ID_DETALLE_ORDEN_COMPRA DESC limit 1';
		$query=$this->db->query($sql);
		$costo=array();
		foreach ($query->result_array() as $c) {
			$costo=$c['PRECIO_UNITARIO'];
		}		
		if (empty($costo)) {
			$costo=0.0;
		}
		return $costo;
	}




	public function updateRequisicion($id,$updateRequisicion)
	{
		$this->db->trans_start();
		$this->db->where("id_requisicion",$id);
		$this->db->update("inv_requisicion",$updateRequisicion);
		$this->db->trans_complete();
	}

	public function updateDetalleRequisicion($id,$updateDetalle)
	{
		$this->db->trans_start();
		$this->db->where("ID_DETALLE_REQUISICION",$id);
		$this->db->update("inv_detalle_requisicion",$updateDetalle);
		$this->db->trans_complete();
	}

		public function deleteDetalleRequisicion($id)
	{
		$this->db->trans_start();
		$this->db->from("inv_requisicion");
		$this->db->join("inv_detalle_requisicion","inv_detalle_requisicion.id_requisicion=inv_requisicion.id_requisicion");
		$this->db->where("inv_detalle_requisicion.id_requisicion",$id);
		$this->db->delete("inv_requisicion");
		$this->db->trans_complete();
	}


	public function listarUnidadAdministrativa()
	{
		$sql="SELECT ID_UNIDAD,CODIGO_UNIDAD,DESCRIPCION from seg_cat_unidad_administrativa";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarProductos($unidad)
	{
		$sql='SELECT p.ID_PRODUCTO,p.COD_PRODUCTO, p.NOMBRE_PRODUCTO from inv_cat_producto p  WHERE ACTIVO=1 and id_unidad_administrativa='.$unidad;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function ultimoCodigo()
	{
		$sql="SELECT nextval('codigo_requisicion') CORRELATIVO";
		$query=$this->db->query($sql);
		$corr='000';
		foreach ($query->result_array() as $c) {
			$corr=$c['CORRELATIVO'];
		}

		return $corr;
	}

	public function listarRequisicionId($id)
	{
		$sql='SELECT r.ID_REQUISICION, r.CORRELATIVO,r.FECHA,uac.DESCRIPCION as unidad_crea,uas.ID_UNIDAD as unidad_solicita,r.USUARIO_CREACION,r.FECHA_CREACION,r.CONCEPTO  
				FROM inv_requisicion_empleado re inner join  inv_requisicion r
				on re.id_requisicion=r.id_requisicion inner join seg_usuario u 
				on u.id_usuario=re.id_empleado_usuario inner join seg_cat_unidad_administrativa uac
				on uac.id_unidad=r.id_unidad_crea inner join seg_cat_unidad_administrativa uas
				on uas.id_unidad=r.id_unidad_solicita WHERE r.ID_REQUISICION='.$id;
		$query= $this->db->query($sql);
		return $query->result();		
	}

	public function listarDetalles($id)
	{
		$sql='SELECT cp.COD_PRODUCTO,cp.NOMBRE_PRODUCTO,ir.ID_PRODUCTO,ir.CANTIDAD,ir.ID_DETALLE_REQUISICION as ID_DETALLE FROM inv_detalle_requisicion ir  inner join inv_cat_producto cp on ir.ID_PRODUCTO=cp.ID_PRODUCTO where ir.id_requisicion='.$id;
		$query=$this->db->query($sql);
		return $query->result();

	}

	public function listarInventario($id)
	{
		$sql="SELECT * from inv_inventario_fisico WHERE ID_PRODUCTO=".$id."  limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarHistorico($id,$fecha)
	{
		$sql="SELECT * from cat_historico_inventario WHERE ESTADO=1 and ID_PRODUCTO=".$id." AND DATE_FORMAT(FECHA_INICIO_VIGENCIA,'%Y-%m-%d')<=DATE_FORMAT('".$fecha."','%Y-%m-%d') ORDER BY FECHA_INICIO_VIGENCIA ASC limit 1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function ObtenerMovimientos($producto)
	{
		$sql="select mo.*,SUM(mo.CANTIDAD) as TOTAL from inv_inventario_fisico fi inner join inv_movimientos mo on mo.id_producto=fi.id_producto where mo.id_producto=".$producto." and mo.id_tipo_movimiento=2
			and DATE_FORMAT(mo.FECHA,'%Y-%m-%d') between DATE_FORMAT(fi.FECHA_CREACION,'%Y-%m-%d')
 			and DATE_FORMAT(IFNULL(fi.FECHA_FIN_VIGENCIA,now()),'%Y-%m-%d ') and VIGENCIA=1";
		$query=$this->db->query($sql);
		$cantidad;
		foreach ($query->result_array() as $c) {
			$cantidad=$c['TOTAL'];
		}
		return $cantidad;
	}

	public function listarDetalleId($id)
	{
		$sql="SELECT * from inv_detalle_requisicion where id_detalle_requisicion=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarMovimiento($requisicon,$producto)
	{
		$sql="SELECT * from inv_movimientos where id_requisicion=".$requisicon." and id_producto=".$producto;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function updateInventario($id,$updateInventario)
	{
		$this->db->trans_start();
		$this->db->where("ID_INVENTARIO_FISICO",$id);
		$this->db->update("inv_inventario_fisico",$updateInventario);
		$this->db->trans_complete();
	}

	public function updateMovimiento($id,$updateMovimiento)
	{
		$this->db->trans_start();
		$this->db->where("ID_MOVIMIENTO",$id);
		$this->db->update("inv_movimientos",$updateMovimiento);
		$this->db->trans_complete();

	}

	public function deleteDetalle($id)
	{
		$this->db->trans_start();
		$this->db->where("ID_DETALLE_REQUISICION",$id);
		$this->db->delete("inv_detalle_requisicion");
		$this->db->trans_complete();		
	}

	public function deleteMovimiento($idproducto,$idrequisicion)
	{
		$this->db->trans_start();
		$this->db->where("ID_PRODUCTO",$idproducto);
		$this->db->where("ID_REQUISICION",$idrequisicion);
		$this->db->delete("inv_movimientos");
		$this->db->trans_complete();				
	}

	public function updateHistorico($id,$updateHistorico)
	{
		$this->db->trans_start();
		$this->db->where("ID_HISTORICO_INVENTARIO",$id);
		$this->db->update("cat_historico_inventario",$updateHistorico);
		$this->db->trans_complete();
	}

	public function listarProductoExistente($idproducto,$idrequisicion,$cantidad)
	{
		$sql="SELECT * FROM inv_requisicion re inner join inv_detalle_requisicion de on
			de.id_requisicion=re.id_requisicion where de.id_requisicion=".$idrequisicion." and id_producto=".$idproducto." and de.CANTIDAD=".$cantidad;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function cerrarRequisicion($fecha_inicio,$fecha_cierre,$updateRequisicion)
	{
		$this->db->trans_start();
		$this->db->where("FECHA BETWEEN '".date('Y-m-d',strtotime($fecha_inicio))."' and '".date('Y-m-d',strtotime($fecha_cierre))."'");
		$this->db->update("inv_requisicion",$updateRequisicion);
		$this->db->trans_complete();
	}

}