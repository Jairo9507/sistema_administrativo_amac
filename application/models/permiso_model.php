<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class permiso_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarPerfiles(){
		$sql='SELECT * FROM seg_perfil where ESTADO=1 and id_perfil_usuario>1';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarModulosLineaUno($id){
		$sql='SELECT m.ID_MENU,m.DESCRIPCION,m.ID_DEPENDE, ac.ACTIVO from seg_menu m inner join seg_acceso_usuario ac on 
		ac.id_menu=m.id_menu WHERE m.LINEA=1  AND m.ID_MENU='.$id;
		$query= $this->db->query($sql);
		return $query->result();
	}

/*	public function listarModulosLineaDos(){
		$sql='SELECT ID_MENU,DESCRIPCION,ID_DEPENDE from seg_menu WHERE LINEA=2 ';
		$query= $this->db->query($sql);
		return $query->result();
	}
*/
	public function PermisosDenegados($id)
	{
		$sql='select m.* from seg_acceso_usuario ac
			inner join seg_menu m on m.id_menu=ac.id_menu
			inner join seg_perfil p on p.id_perfil_usuario=ac.id_perfil_usuario
			WHERE p.id_perfil_usuario='.$id.' and ac.ACTIVO<>1';
		$query=$this->db->query($sql);
		return $query->result();
	}



	public function modulosPermitidos($id)
	{
		$sql="select m.ID_MENU,m.DESCRIPCION,m.ID_DEPENDE,m.LINEA from seg_menu m
			where m.ID_MENU<>1  ";//and not exists(SELECT 1 from seg_acceso_usuario where id_perfil_usuario=".$id." and id_menu=m.id_menu and LINEA<>1)";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function modulosAcceso($id)
	{
		$sql="select m.ID_MENU,m.DESCRIPCION,m.ID_DEPENDE,m.LINEA from seg_acceso_usuario ac
			inner join seg_menu m on m.id_menu=ac.id_menu
			inner join seg_perfil p on p.id_perfil_usuario=ac.id_perfil_usuario
			WHERE p.id_perfil_usuario in(".$id.") and m.id_menu<>1";
		$query=$this->db->query($sql);
		return $query->result();

	}

	public function savePermisos($registrarPermisos)
	{
		$this->db->trans_start();
		$this->db->insert("seg_acceso_usuario",$registrarPermisos);
		$this->db->trans_complete();
	}

	public function updatePermiso($id,$updatePermiso)
	{
		$this->db->trans_start();
		$this->db->where("id_acceso_usuario",$id);
		$this->db->update("seg_acceso_usuario",$updatePermiso);
		$this->db->trans_complete();
	}

	public function moduloExiste($idpermiso,$idperfil)
	{
		$sql="SELECT * FROM seg_acceso_usuario where ID_MENU=".$idpermiso." and id_perfil_usuario=".$idperfil." ";
		$query=$this->db->query($sql);
		$row=$query->row();
		if (isset($row)) {
			return true;
		} else {
			return false;
		}
		
	}

	public function moduloNombre($id)
	{
		$sql="SELECT * from seg_menu WHERE ID_MENU=".$id;
		$query=$this->db->query($sql);
		$nombre;
		foreach ($query->result_array() as $row) {
			$nombre=$row['DESCRIPCION'];
		}
		return $nombre;
	}

	public function moduloId($idpermiso,$idperfil)
	{
		$sql="SELECT * FROM seg_acceso_usuario where ID_MENU=".$idpermiso." and id_perfil_usuario=".$idperfil." ";
		$query=$this->db->query($sql);
		$id;
		foreach ($query->result_array() as $row) {
			$id=$row['ID_ACCESO_USUARIO'];
		}
		return $id;
	}

}