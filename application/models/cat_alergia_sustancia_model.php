<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_alergia_sustancia_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarAlergiaSustancia()
	{
		$sql='SELECT ID_CAT_ALERGIA_SUSTANCIA,DESCRIPCION,ESTADO FROM cli_cat_alergia_sustancia WHERE ESTADO=1';
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function saveAlergiaSustancia($registrarAlergiaSustancia)
	{
		$this->db->trans_start();
		$this->db->insert("cli_cat_alergia_sustancia",$registrarAlergiaSustancia);
		$this->db->trans_complete();
	}

	public function updateAlergiaSustancia($id,$updateAlergiaSustancia)
	{
		$this->db->trans_start();
		$this->db->where("ID_CAT_ALERGIA_SUSTANCIA",$id);
		$this->db->update("cli_cat_alergia_sustancia",$updateAlergiaSustancia);
		$this->db->trans_complete();
	}

	public function deleteAlergiaSustancia($id,$deleteAlergiaSustancia)
	{
		$this->db->trans_start();
		$this->db->where("ID_CAT_ALERGIA_SUSTANCIA",$id);
		$this->db->update("cli_cat_alergia_sustancia",$deleteAlergiaSustancia);
		$this->db->trans_complete();
	}

	public function listarAlergiaSustanciaId($id)
	{
		$sql='SELECT ID_CAT_ALERGIA_SUSTANCIA,DESCRIPCION,ESTADO FROM cli_cat_alergia_sustancia WHERE ESTADO=1 and ID_CAT_ALERGIA_SUSTANCIA='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}
}