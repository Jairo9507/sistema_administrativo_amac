<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cli_medico_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarMedicos()
	{
		$sql="SELECT me.ID_MEDICO,CONCAT(em.PRIMER_NOMBRE,'&nbsp;',em.SEGUNDO_NOMBRE,'&nbsp; ',em.PRIMER_APELLIDO,' &nbsp;',em.SEGUNDO_APELLIDO) as nombre_completo, IFNULL(em.CODIGO_EMPLEADO,'') as CODIGO_EMPLEADO, IFNULL(em.SEXO,'') as SEXO,es.DESCRIPCION,me.NO_JVPM,me.VER_AGENDA from cli_medico me inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO inner join cli_cat_especialidad es on es.ID_CAT_ESPECIALIDAD=me.ID_CAT_ESPECIALIDAD WHERE me.ESTADO=1 and em.ID_UNIDAD=44";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarMedicoExiste($id)
	{
		$sql="SELECT * from cli_medico me inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO where em.ESTADO=1 and em.ID_EMPLEADO=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveMedico($registrarMedico)
	{
		$this->db->trans_start();
		$this->db->insert("cli_medico",$registrarMedico);
		$this->db->trans_complete();
	}

	public function updateMedico($id,$updateMedico)
	{
		$this->db->trans_start();
		$this->db->where("ID_MEDICO",$id);
		$this->db->update("cli_medico",$updateMedico);
		$this->db->trans_complete();
	}

	public function deleteMedico($id,$deleteMedico)
	{
		$this->db->trans_start();
		$this->db->where("ID_MEDICO",$id);
		$this->db->update("cli_medico",$deleteMedico);
		$this->db->trans_complete();
	}

	public function listarMedicoId($id)
	{
		$sql="SELECT em.ID_EMPLEADO,CONCAT(em.PRIMER_NOMBRE,'&nbsp; ',em.SEGUNDO_NOMBRE,'&nbsp; ',em.PRIMER_APELLIDO,'&nbsp; ',em.SEGUNDO_APELLIDO) as nombre_completo, em.CODIGO_EMPLEADO,em.SEXO,es.ID_CAT_ESPECIALIDAD,me.NO_JVPM,me.VER_AGENDA from cli_medico me inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO inner join cli_cat_especialidad es on es.ID_CAT_ESPECIALIDAD=me.ID_CAT_ESPECIALIDAD WHERE em.ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEspecialidades()
	{
		$sql="SELECT ID_CAT_ESPECIALIDAD,DESCRIPCION,ESTADO from cli_cat_especialidad WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEmpleados()
	{
		$sql="SELECT CONCAT(em.PRIMER_NOMBRE,'&nbsp; ',em.SEGUNDO_NOMBRE,'&nbsp; ',em.PRIMER_APELLIDO,' &nbsp;',em.SEGUNDO_APELLIDO) as nombre_completo,ID_EMPLEADO,CODIGO_EMPLEADO from seg_empleado em WHERE em.ESTADO=1 ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function obtenerIdMedico($id)
	{
		$sql="SELECT me.ID_MEDICO from cli_medico me 
			inner join seg_empleado em on em.id_empleado=me.id_empleado
			left join seg_usuario us on us.id_empleado=em.id_empleado WHERE ID_USUARIO=".$id;
		$query=$this->db->query($sql);
		$medico=array();
		foreach ($query->result_array() as $m) {
			$medico=$m['ID_MEDICO'];
		}
		return $medico;
	}

}