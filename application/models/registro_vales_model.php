<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class registro_vales_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}


	public function guardarVale($registrarVale)
	{
		$this->db->trans_start();
		$this->db->insert("vyc_vale_combustible", $registrarVale);
		$this->db->trans_complete();
	}

		public function updateVale($id, $updateVale)
	{
		$this->db->trans_start();
		$this->db->where("ID_VALE_COMBUSTIBLE",$id);
		$this->db->update("vyc_vale_combustible",$updateVale);
		$this->db->trans_complete();	
	}	

	public function obtenerId($placa)
	{
		$sql = 'SELECT ID_VEHICULO FROM vyc_vehiculo WHERE PLACA ="'.$placa.'"';
		$query = $this->db->query($sql);
		return $query->result();
	}
}