<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_medicamento_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarMedicamentos($unidad)
	{
		$sql='SELECT p.ID_PRODUCTO,IFNULL(p.COD_PRODUCTO,"N/A") as COD_PRODUCTO,p.NOMBRE_PRODUCTO,me.PRESENTACION,me.CONCENTRACION,me.FORMA_FARMACEUTICA,p.MINIMO, tp.DESCRIPCION as TIPO,ua.nombre_cuenta as NOMBRE_CUENTA,um.DESCRIPCION as UNIDAD from inv_cat_producto p   inner join inv_cat_cuenta_contable tp on tp.id_cat_cuenta_contable=p.inv_cat_cuenta_contable inner join inv_cat_cuentas ua on ua.id_cat_cuenta=p.id_cat_cuenta left join inv_cat_unidad_medida um on um.id_cat_unidad_medida=p.id_cat_unidad_medida inner join cli_cat_medicamento me on me.ID_PRODUCTO=p.ID_PRODUCTO WHERE ACTIVO=1 and p.id_unidad_administrativa='.$unidad.' ORDER BY p.FECHA_CREACION DESC';

		$query=$this->db->query($sql);
		return $query->result();
	}


	public function listarPDF($unidad)
	{
		$sql='SELECT p.ID_PRODUCTO,IFNULL(p.COD_PRODUCTO,"N/A") as COD_PRODUCTO,p.NOMBRE_PRODUCTO,p.MINIMO,p.MAXIMO, tp.DESCRIPCION as TIPO,ua.NOMBRE_CUENTA,um.DESCRIPCION as UNIDAD,me.PRESENTACION from inv_cat_producto p   inner join inv_cat_cuenta_contable tp on tp.id_cat_cuenta_contable=p.inv_cat_cuenta_contable inner join inv_cat_cuentas ua on ua.id_cat_cuenta=p.id_cat_cuenta left join inv_cat_unidad_medida um on um.id_cat_unidad_medida=p.id_cat_unidad_medida inner join cli_cat_medicamento me on me.ID_PRODUCTO=p.ID_PRODUCTO WHERE ACTIVO=1 and p.id_unidad_administrativa='.$unidad.' ORDER BY ua.COD_CUENTA ASC,p.NOMBRE_PRODUCTO ASC';
		$query=$this->db->query($sql);
		return $query->result();

	}

	public function obtenerProductoNombre($nombre,$cuentap,$cuentac,$forma)
	{
		$sql="SELECT * from inv_cat_producto p left join cli_cat_medicamento me on p.id_producto=me.id_producto  WHERE p.NOMBRE_PRODUCTO='".$nombre."' AND p.id_cat_cuenta=".$cuentap." AND inv_cat_cuenta_contable=".$cuentac." AND me.presentacion='".$forma."'";
		$query=$this->db->query($sql);
		return $query->result();
	}	

	public function saveMedicamento($registrarMedicamento)
	{
		$this->db->trans_start();
		$this->db->insert("cli_cat_medicamento",$registrarMedicamento);
		$this->db->trans_complete();
	}

	public function updateMedicamento($id,$updateMedicamento)
	{
		$this->db->trans_start();
		$this->db->where("ID_MEDICAMENTO",$id);
		$this->db->update("cli_cat_medicamento",$updateMedicamento);
		$this->db->trans_complete();
	}


	public function saveProducto($registrarProducto)
	{
		$this->db->trans_start();
		$this->db->insert("inv_cat_producto",$registrarProducto);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();

		return $last_id;
	}

	public function updateProducto($id,$updateProducto)
	{
		$this->db->trans_start();
		$this->db->where("id_producto",$id);
		$this->db->update("inv_cat_producto",$updateProducto);
		$this->db->trans_complete();
	}

	public function deleteProducto($id,$deleteProducto)
	{
		$this->db->trans_start();
		$this->db->where("id_producto",$id);
		$this->db->update("inv_cat_producto",$deleteProducto);
		$this->db->trans_complete();
	}

	public function listarMedicamentoId($id)
	{
		$sql='SELECT me.ID_MEDICAMENTO,p.ID_PRODUCTO,IFNULL(p.COD_PRODUCTO,"N/A") as COD_PRODUCTO,p.NOMBRE_PRODUCTO,p.MINIMO,p.MAXIMO, tp.id_cat_cuenta_contable as ID_TIPO,ua.ID_CAT_CUENTA as ID_CUENTA,um.id_cat_unidad_medida as UNIDAD,me.PRESENTACION,me.FORMA_FARMACEUTICA,me.CONCENTRACION from inv_cat_producto p   inner join inv_cat_cuenta_contable tp on tp.id_cat_cuenta_contable=p.inv_cat_cuenta_contable inner join inv_cat_cuentas ua on ua.id_cat_cuenta=p.id_cat_cuenta left join inv_cat_unidad_medida um on um.id_cat_unidad_medida=p.id_cat_unidad_medida inner join cli_cat_medicamento me on me.ID_PRODUCTO=p.ID_PRODUCTO WHERE ACTIVO=1 and p.ID_PRODUCTO='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarMovimientosProductos($productoid)
	{
		$sql='SELECT mo.ID_MOVIMIENTO,mo.FECHA,mo.CANTIDAD,mo.COSTO_UNITARIO,cm.DESCRIPCION as TIPO_MOVIMIENTO,cm.DESCRIPCION,mo.USUARIO_CREACION,mo.FECHA_CREACION,mo.USUARIO_MODIFICACION,mo.FECHA_MODIFICACION,mo.ID_TIPO_MOVIMIENTO from inv_movimientos mo inner join inv_cat_producto p on mo.ID_PRODUCTO=p.ID_PRODUCTO inner join cat_movimientos cm on cm.ID_CAT_MOVIMIENTO=mo.ID_TIPO_MOVIMIENTO  WHERE mo.ID_PRODUCTO='.$productoid;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarInventarioProductos($productoid)
	{
		$sql='SELECT fi.ID_INVENTARIO_FISICO,fi.FECHA_INGRESO,oc.CODIGO_ORDEN_COMPRA,fi.CANTIDAD,fi.PRECIO,fi.VIGENCIA,fi.USUARIO_CREACION,fi.FECHA_CREACION,fi.USUARIO_MODIFICACION,fi.FECHA_MODIFICACION  FROM inv_inventario_fisico fi left join inv_orden_compra oc on fi.ID_ORDEN_COMPRA=oc.ID_ORDEN_COMPRA left join inv_cat_producto p on fi.id_producto=p.id_producto WHERE fi.ID_PRODUCTO='.$productoid;
		$query= $this->db->query($sql);
		return $query->result();
	}	


}