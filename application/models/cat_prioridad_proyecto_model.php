<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_prioridad_proyecto_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarPrioridad()
	{
		$sql='SELECT ID_CAT_PRIORIDAD,DESCRIPCION,ESTADO FROM pro_cat_prioridad_proyecto WHERE ESTADO=1';
		$query= $this->db->query($sql);
		return $query->result();
	}

	public function savePrioridad($registrarPrioridad)
	{
		$this->db->trans_start();
		$this->db->insert('pro_cat_prioridad_proyecto',$registrarPrioridad);
		$this->db->trans_complete();
	}

	public function updatePrioridad($id,$updatePrioridad)
	{
		$this->db->trans_start();
		$this->db->where('ID_CAT_PRIORIDAD',$id);
		$this->db->update('pro_cat_prioridad_proyecto',$updatePrioridad);
		$this->db->trans_complete();
	}

	public function deletePrioridad($id,$deletePrioridad)
	{
		$this->db->trans_start();
		$this->db->where('ID_CAT_PRIORIDAD',$id);
		$this->db->update('pro_cat_prioridad_proyecto',$deletePrioridad);
		$this->db->trans_complete();
	}

	public function listarPrioridadId($id)
	{
		$sql='SELECT ID_CAT_PRIORIDAD,DESCRIPCION,ESTADO FROM pro_cat_prioridad_proyecto WHERE ID_CAT_PRIORIDAD='.$id.' AND ESTADO=1';
		$query= $this->db->query($sql);
		return $query->result();
	}

}