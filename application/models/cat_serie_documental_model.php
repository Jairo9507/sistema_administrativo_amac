<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_serie_documental_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarSeries()
	{
		$sql="SELECT ID_SERIE_DOCUMENTAL,NOMBRE,ESTADO,DESCRIPCION,CODIGO,TIPOLOGIA FROM arc_serie_documental WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarSerieId($id)
	{
		$sql="SELECT ID_SERIE_DOCUMENTAL,NOMBRE,ESTADO,DESCRIPCION,CODIGO,TIPOLOGIA FROM arc_serie_documental WHERE ESTADO=1 and ID_SERIE_DOCUMENTAL=".$id;
		$query=$this->db->query($sql);
		return $query->result();		
	}


	public function saveSerie($registrarSerie)
	{
		$this->db->trans_start();
		$this->db->insert("arc_serie_documental",$registrarSerie);
		$this->db->trans_complete();
	}

	public function updateSerie($id,$updateSerie)
	{
		$this->db->trans_start();
		$this->db->where("id_serie_documental",$id);
		$this->db->update("arc_serie_documental",$updateSerie);
		$this->db->trans_complete();
	}

	public function deleteSerie($id,$deleteSerie)
	{
		$this->db->trans_start();
		$this->db->where("id_serie_documental",$id);
		$this->db->update("arc_serie_documental",$deleteSerie);
		$this->db->trans_complete();		
	}
}