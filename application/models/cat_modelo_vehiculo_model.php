<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_modelo_vehiculo_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarModelo()
	{
		$sql = 'SELECT mo.ID_VEHICULO_MODELO, mo.DESCRIPCION DESMODELO, mo.ESTADO,mo.ID_VEHICULO_MARCA, ma.DESCRIPCION DESMARCA FROM vyc_vehiculo_modelo mo
			LEFT JOIN vyc_vehiculo_marca ma
			ON (mo.ID_VEHICULO_MARCA = ma.ID_VEHICULO_MARCA) WHERE mo.ESTADO=1';
			$query = $this->db->query($sql);
			return $query->result();
	}

	public function listarMarca()
	{
		$sql = 'SELECT * FROM vyc_vehiculo_marca';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function guardarModelo($registroModelo)
	{
		$this->db->trans_start();
		$this->db->insert("vyc_vehiculo_modelo",$registroModelo);
		$this->db->trans_complete();
	}

	public function listarModeloVehiculoId($id)
	{
		$sql = 'SELECT mo.ID_VEHICULO_MODELO, mo.DESCRIPCION DESMODELO, mo.ESTADO, mo.ID_VEHICULO_MARCA, ma.DESCRIPCION FROM vyc_vehiculo_modelo mo
			LEFT JOIN vyc_vehiculo_marca ma
			ON (mo.ID_VEHICULO_MARCA = ma.ID_VEHICULO_MARCA) WHERE mo.ID_VEHICULO_MODELO ='.$id;
			$query = $this->db->query($sql);
			return $query->result();
	}

	public function updateModeloVehiculo($id, $updateModelo)
	{
		$this->db->trans_start();
		$this->db->where("ID_VEHICULO_MODELO",$id);
		$this->db->update("vyc_vehiculo_modelo",$updateModelo);
		$this->db->trans_complete();
	}

	public function deleteModeloVehiculo($id, $deleteModelo)
	{
		$this->db->trans_start();
		$this->db->where("ID_VEHICULO_MODELO",$id);
		$this->db->update("vyc_vehiculo_modelo",$deleteModelo);
		$this->db->trans_complete();
	}


}