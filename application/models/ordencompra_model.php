<?php  

/**
 * 
 */
class OrdenCompra_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarProveedores(){
		$sql = 'SELECT * FROM  inv_cat_proveedor ORDER BY nombre_proveedor ASC';
		$query = $this->db->query($sql);
		 return $query->result();
	}
	public function listarProductos(){

		$sql = 'SELECT p.ID_PRODUCTO,IFNULL(CONCAT(me.PRESENTACION," ",me.CONCENTRACION," ",me.FORMA_FARMACEUTICA)," ") as MEDICAMENTO, CONCAT(c.cod_cuenta," ", p.NOMBRE_PRODUCTO) NOMBRE_PRODUCTO, p.id_cat_cuenta
            FROM inv_cat_producto p LEFT JOIN inv_cat_cuentas c on (c.id_cat_cuenta = p.id_cat_cuenta) 
            LEFT JOIN cli_cat_medicamento me on (me.id_producto=p.id_producto)
            WHERE p.ACTIVO=1';
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function guardarOrdenCompra($idProveedor, $numFactura,      $codigoOrdenFactura, $usuario){
		$sql='INSERT INTO inv_orden_compra(CODIGO_ORDEN_COMPRA, FECHA, ID_PROVEEDOR, NUMERO_FACTURA, USUARIO_CREACION, FECHA_CREACION) values("'.$codigoOrdenFactura.'",sysdate(),"'.$idProveedor.'","'.$numFactura.'","'.$usuario.'",sysdate())';
		return $this->db->query($sql);
	}

	public function obtenerCorrelativo()
	{
		$sql = 'select nextval("ejemplo_prueba") CORRELATIVO';
 		$query = $this->db->query($sql);
 		return $query->result();
	}

	public function listarUnidades()
	{
		$sql="SELECT * FROM seg_cat_unidad_administrativa";
		$query=$this->db->query($sql);
		return $query->result();
	}
	

	public function agregarPedido($registrarDetalle)
	{
		$this->db->trans_start();
		$this->db->insert("inv_detalle_orden_compra",$registrarDetalle);
		$this->db->trans_complete();
	}

	public function updateOrdenCompra($id, $updateDetalleOrden)
	{	
		$this->db->trans_start();
		$this->db->where("id_detalle_orden_compra",$id);
		$this->db->update("inv_detalle_orden_compra",$updateDetalleOrden);
		$this->db->trans_complete();
		
	}
	public function agregarHistorico($registro)
	{
		$this->db->trans_start();
		$this->db->insert("cat_historico_inventario",$registro);
		$this->db->trans_complete();
	}

	public function agregarOrdenCompra($registrarOrden)
	{
		$this->db->trans_start();
		$this->db->insert("inv_orden_compra",$registrarOrden);		
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();		
		return $last_id;  
	}
	public function updateOrden($id, $updateDetalleOrden)
	{
		$this->db->trans_start();
		$this->db->where("ID_ORDEN_COMPRA",$id);
		$this->db->update("inv_orden_compra",$updateDetalleOrden);
		$this->db->trans_complete();
	}

	public function saveMovimiento($registrarMovimiento)
	{
		$this->db->trans_start();
		$this->db->insert("inv_movimientos",$registrarMovimiento);
		$this->db->trans_complete();
	}

	public function saveInventario($registrarInventario)
	{
		$this->db->trans_start();
		$this->db->insert("inv_inventario_fisico",$registrarInventario);
		$this->db->trans_complete();
	}

	public function anularOrden($id, $anularOrden)
	{
		$this->db->trans_start();
		$this->db->where("ID_ORDEN_COMPRA",$id);
		$this->db->update("inv_orden_compra", $anularOrden);
		$this->db->trans_complete();
	}

	public function buscarProducto($id)
	{
		$sql = 'SELECT ID_PRODUCTO FROM inv_inventario_fisico where ID_PRODUCTO ='.$id;
		$query = $this->db->query($sql);
 		return $query->result();
	}

	public function existenciaProducto($id)
	{
		$sql = 'SELECT CANTIDAD FROM inv_inventario_fisico WHERE id_producto='.$id;
		$query = $this->db->query($sql);
 		return $query->result();
	}

	public function updateInventario($idProducto, $updateInventario)
	{
		$this->db->trans_start();
		$this->db->where("ID_PRODUCTO",$idProducto);
		$this->db->update("inv_inventario_fisico", $updateInventario);
		$this->db->trans_complete();
	}
}