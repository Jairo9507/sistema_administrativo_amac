<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_marca_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarMarca()
	{
		$sql = 'SELECT * FROM vyc_vehiculo_marca WHERE ESTADO = 1';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function guardarMarca($registroMarca)
	{
		$this->db->trans_start();
		$this->db->insert("vyc_vehiculo_marca",$registroMarca);
		$this->db->trans_complete();
	}

	public function listarMarcaId($id)
	{
		$sql = 'SELECT * FROM vyc_vehiculo_marca WHERE ID_VEHICULO_MARCA ='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function updateMarca($id, $updateMarca)
	{
		$this->db->trans_start();
		$this->db->where("ID_VEHICULO_MARCA",$id);
		$this->db->update("vyc_vehiculo_marca",$updateMarca);
		$this->db->trans_complete();
	}

	public function deleteMarca($id, $deleteMarca)
	{
		$this->db->trans_start();
		$this->db->where("ID_VEHICULO_MARCA",$id);
		$this->db->update("vyc_vehiculo_marca",$deleteMarca);
		$this->db->trans_complete();
	}



}