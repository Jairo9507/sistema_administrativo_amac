<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class unidades_administrativa_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarUnidades()
	{
		$sql="SELECT g.ID_UNIDAD,g.CODIGO_UNIDAD,
			g.DESCRIPCION,(SELECT DESCRIPCION from seg_cat_unidad_administrativa where ID_UNIDAD=g.UNIDAD_DEPENDE limit 1) as DEPENDENCIA
			,g.ESTADO FROM seg_cat_unidad_administrativa g WHERE g.ESTADO=1 ORDER BY g.ID_UNIDAD DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarGerencias()
	{
		$sql="SELECT * from seg_cat_unidad_administrativa WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarUnidadId($id)
	{
		$sql="SELECT * from seg_cat_unidad_administrativa WHERE ESTADO=1 and ID_UNIDAD=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarUnidadCodigo($codigo)
	{
		$sql="SELECT * from seg_cat_unidad_administrativa WHERE CODIGO_UNIDAD='".$codigo."'";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveUnidad($registrarUnidad)
	{
		$this->db->trans_start();
		$this->db->insert("seg_cat_unidad_administrativa",$registrarUnidad);
		$this->db->trans_complete();
	}

	public function updateUnidad($id,$updateUnidad)
	{
		$this->db->trans_start();
		$this->db->where("id_unidad",$id);
		$this->db->update("seg_cat_unidad_administrativa",$updateUnidad);
		$this->db->trans_complete();
	}

	public function deleteUnidad($id,$deleteUnidad)
	{
		$this->db->trans_start();
		$this->db->where("id_unidad",$id);
		$this->db->update("seg_cat_unidad_administrativa",$deleteUnidad);
		$this->db->trans_complete();		
	}
}
