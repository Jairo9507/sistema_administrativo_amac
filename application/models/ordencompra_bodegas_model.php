<?php
/**
 * 
 */
class ordencompra_bodegas_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarDetalle($id)
	{
		$sql='SELECT a.ID_ORDEN_COMPRA, a.ID_PROVEEDOR, a.UNIDAD_CREA, a.CODIGO_ORDEN_COMPRA CODIGO_ORDEN,
		b.NOMBRE_PROVEEDOR, a.FECHA FECHA_ORDEN, a.USUARIO_CREACION USER, a.FECHA_CREACION, a.ESTADO,
		(SELECT COUNT(*) FROM inv_detalle_orden_compra o WHERE o.ID_ORDEN_COMPRA=a.ID_ORDEN_COMPRA AND o.CANTIDAD_PARCIAL=0) as countDetalle
		FROM inv_orden_compra a
		LEFT JOIN inv_cat_proveedor b 
		ON (a.ID_PROVEEDOR = b.ID_PROVEEDOR)
		WHERE a.UNIDAD_CREA='.$id.' and a.ESTADO=1
		and DATE_FORMAT(a.FECHA,"%Y-%m-%d") BETWEEN DATE_SUB(DATE_FORMAT(now(),"%Y-%m-%d"),INTERVAL 4 MONTH) and DATE_FORMAT(LAST_DAY(now()),"%Y-%m-%d")
		GROUP BY a.ID_ORDEN_COMPRA
		ORDER BY a.FECHA_CREACION DESC , countDetalle ASC;';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarDetalleProyectos($id)
	{
		$sql='SELECT a.ID_ORDEN_COMPRA, a.ID_PROVEEDOR, a.UNIDAD_CREA, a.CODIGO_ORDEN_COMPRA CODIGO_ORDEN,
		b.NOMBRE_PROVEEDOR, a.FECHA FECHA_ORDEN, a.USUARIO_CREACION USER, a.FECHA_CREACION, a.ESTADO,
		(SELECT COUNT(*) FROM inv_detalle_orden_compra o WHERE o.ID_ORDEN_COMPRA=a.ID_ORDEN_COMPRA AND o.CANTIDAD_PARCIAL=0) as countDetalle
		FROM inv_orden_compra a
		LEFT JOIN inv_cat_proveedor b 
		ON (a.ID_PROVEEDOR = b.ID_PROVEEDOR)
		WHERE a.UNIDAD_CREA='.$id.' and a.ESTADO=1
		AND DATE_FORMAT(a.FECHA,"%Y-%m-%d") BETWEEN DATE_SUB(DATE_FORMAT(now(),"%Y-%m-%d"),INTERVAL 4 MONTH) and DATE_FORMAT(LAST_DAY(now()),"%Y-%m-%d")
		AND NOT EXISTS (SELECT * FROM pro_material_recibido_orden_compra mc WHERE mc.ID_ORDEN_COMPRA=a.ID_ORDEN_COMPRA )
		GROUP BY a.ID_ORDEN_COMPRA
		ORDER BY a.FECHA_CREACION DESC , countDetalle ASC;';
		$query = $this->db->query($sql);
		return $query->result();		
	}

	public function listarOrdenCompraId($id)
	{
		$sql = 'SELECT oc.ID_ORDEN_COMPRA,oc.CODIGO_ORDEN_COMPRA, date_format(oc.FECHA, "%d-%b-%Y") FECHA, pro.NOMBRE_PROVEEDOR, oc.CONDICIONES_PAGO, oc.VIA,oc.NUMERO_FACTURA
				FROM inv_orden_compra oc
				LEFT JOIN inv_cat_proveedor pro
				ON (pro.ID_PROVEEDOR = oc.ID_PROVEEDOR)
		WHERE id_orden_compra ='.$id;
		 $query = $this->db->query($sql);
		 return $query->result();

	}

	public function listarProductos(){

		$sql = 'SELECT * FROM inv_cat_producto';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function obtenerCantidadParcial($id)
	{
		$sql = 'SELECT ID_DETALLE_ORDEN_COMPRA, ID_PRODUCTO, CANTIDAD_PARCIAL 
				FROM inv_detalle_orden_compra where id_detalle_orden_compra='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarDetalleProductos($id)
	{
		$sql='SELECT d.ID_ORDEN_COMPRA, d.ID_DETALLE_ORDEN_COMPRA ID_DETALLE,d.ID_PRODUCTO,  d.DESCRIPCION,IFNULL(CONCAT(me.PRESENTACION," ",me.CONCENTRACION," ",me.FORMA_FARMACEUTICA)," ") as MEDICAMENTO,
				CANTIDAD_TOTAL, CANTIDAD_PARCIAL, d.PRECIO_UNITARIO PRECIO, ORDEN_COMPRA CODIGO_ORDEN, TIPO_ENTREGA,
				truncate((CANTIDAD_TOTAL*PRECIO_UNITARIO),2) AS MTOTAL,  (CANTIDAD_TOTAL-CANTIDAD_PARCIAL) DIFERENCIA ,p.NOMBRE_PRODUCTO,cp.cod_cuenta as CUENTA,
				(SELECT DATE_FORMAT(ip.FECHA_INGRESO,"%Y-%m-%d") FROM inv_ingreso_producto ip WHERE ip.ID_DETALLE=d.ID_DETALLE_ORDEN_COMPRA LIMIT 1) as FECHA_INGRESO,
				(SELECT NUMERO_ENTREGA FROM inv_ingreso_producto ip WHERE ip.ID_DETALLE=d.ID_DETALLE_ORDEN_COMPRA LIMIT 1) as NUMERO_ENTREGA
				FROM inv_detalle_orden_compra d 
				LEFT JOIN inv_cat_producto p
				ON (d.id_producto=p.ID_PRODUCTO)
				LEFT JOIN inv_cat_cuentas cp 
                ON (cp.id_cat_cuenta=p.id_cat_cuenta)
                LEFT JOIN cli_cat_medicamento me 
                ON (me.id_producto=p.id_producto) 
                 WHERE d.ID_PRODUCTO !=0 AND d.ID_ORDEN_COMPRA='.$id;
			$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarDetalleMateriales($id)
	{
		$sql='SELECT d.ID_ORDEN_COMPRA, d.ID_DETALLE_ORDEN_COMPRA ID_DETALLE,d.ID_PRODUCTO,  d.DESCRIPCION,IFNULL(CONCAT(me.PRESENTACION," ",me.CONCENTRACION," ",me.FORMA_FARMACEUTICA)," ") as MEDICAMENTO,
				CANTIDAD_TOTAL, CANTIDAD_PARCIAL, d.PRECIO_UNITARIO PRECIO, ORDEN_COMPRA CODIGO_ORDEN, TIPO_ENTREGA,
				truncate((CANTIDAD_TOTAL*PRECIO_UNITARIO),2) AS MTOTAL,  (CANTIDAD_TOTAL-CANTIDAD_PARCIAL) DIFERENCIA ,p.NOMBRE_PRODUCTO,cp.cod_cuenta as CUENTA,
				(SELECT DATE_FORMAT(ip.FECHA_INGRESO,"%Y-%m-%d") FROM inv_ingreso_producto ip WHERE ip.ID_DETALLE=d.ID_DETALLE_ORDEN_COMPRA LIMIT 1) as FECHA_INGRESO,
				(SELECT NUMERO_ENTREGA FROM inv_ingreso_producto ip WHERE ip.ID_DETALLE=d.ID_DETALLE_ORDEN_COMPRA LIMIT 1) as NUMERO_ENTREGA
				FROM inv_detalle_orden_compra d 
				LEFT JOIN inv_cat_producto p
				ON (d.id_producto=p.ID_PRODUCTO)
				LEFT JOIN inv_cat_cuentas cp 
                ON (cp.id_cat_cuenta=p.id_cat_cuenta)
                LEFT JOIN cli_cat_medicamento me 
                ON (me.id_producto=p.id_producto) 
                WHERE  d.ID_ORDEN_COMPRA IN('.$id.')';
                 
                $query = $this->db->query($sql);
                return $query->result();		
	}

	public function updateEntrega($id, $updateCantidad)
	{
		$this->db->trans_start();
		$this->db->where("ID_DETALLE_ORDEN_COMPRA",$id);
		$this->db->update("inv_detalle_orden_compra",$updateCantidad);
		$this->db->trans_complete();
		
	}

	public function validarEntradaParcial($id)
	{
		$sql = 'SELECT  * FROM inv_detalle_orden_compra 
				where ID_DETALLE_ORDEN_COMPRA ='.$id;
		$query = $this->db->query($sql);
		$cantidad_parcial = array();
		foreach ($query->result_array() as $cp) {
			$cantidad_parcial =$cp['CANTIDAD_PARCIAL'];
		}
		return $cantidad_parcial;
	}

		public function saveInventario($registrarInventario)
	{
		$this->db->trans_start();
		$this->db->insert("inv_inventario_fisico",$registrarInventario);
		$this->db->trans_complete();
	}

	public function inventario($id)
	{
		$sql = 'select * from inv_inventario_fisico where id_producto ='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function buscarHistorico($idOrdenCompra, $idProducto)
	{
		$sql='SELECT * FROM cat_historico_inventario WHERE id_orden_compra='.$idOrdenCompra.' and id_producto='.$idProducto.' AND CANTIDAD<>CANTIDAD_INVENTARIO ORDER BY FECHA_INICIO_VIGENCIA ASC limit 1';
		$query = $this->db->query($sql);
		return $query->result();
	}

		public function buscarProducto($id)
	{
		$sql = 'SELECT ID_PRODUCTO FROM inv_inventario_fisico where ID_PRODUCTO ='.$id;
		$query = $this->db->query($sql);
 		return $query->result();
	}

	public function saveIngresoProducto($ingresoProducto)
	{
		$this->db->trans_start();
		$this->db->insert("inv_ingreso_producto",$ingresoProducto);
		$this->db->trans_complete();
	}
	public function saveInventarioHistorico($ingresoProducto, $idOrdenCompra, $idProducto)
	{
		$this->db->trans_start();
		$this->db->where("ID_HISTORICO_INVENTARIO = '".$idOrdenCompra."'AND ID_PRODUCTO=".$idProducto);
		$this->db->update("cat_historico_inventario",$ingresoProducto);
		$this->db->trans_complete();
	}

	public function updateOrdenCompra($id, $updateOrden)
	{
		$this->db->trans_start();
		$this->db->where("ID_HISTORICO_INVENTARIO",$id);		
		$this->db->update("inv_orden_compra",$updateOrden);
		$this->db->trans_complete();
	}
}