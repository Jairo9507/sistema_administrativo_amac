<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_estantes_model extends CI_Model //el model debe llamarse igual que el archivo
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarEstantes()
	{
		$sql = 'SELECT es.ID_ESTANTE,es.DESCRIPCION as DESTANTE,es.ESTADO,es.CODIGO_ESTANTE,pa.CODIGO,pa.DESCRIPCION,es.ID_CAT_PASILLO FROM arc_cat_estantes es inner join arc_cat_pasillo pa on pa.ID_CAT_PASILLO=es.ID_CAT_PASILLO and es.ESTADO=1';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarPasillos()
	{
		$sql="SELECT ID_CAT_PASILLO,CODIGO,DESCRIPCION FROM arc_cat_pasillo WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEstantesId($id)
	{
		$sql = 'SELECT es.ID_ESTANTE,es.DESCRIPCION as DESTANTE,es.ESTADO,es.CODIGO_ESTANTE,pa.CODIGO,pa.DESCRIPCION,es.ID_CAT_PASILLO FROM arc_cat_estantes es inner join arc_cat_pasillo pa on pa.ID_CAT_PASILLO=es.ID_CAT_PASILLO WHERE es.ID_ESTANTE='.$id;
		$query = $this->db->query($sql);
		return $query->result();		
	}	

	public function obtenerCorrelativo()
	{
		$sql = 'select nextval("codigo_estante") CORRELATIVO';
 		$query = $this->db->query($sql);
 		$codigo='';
 		foreach ($query->result_array() as $q) {
 			$codigo=$q['CORRELATIVO'];
 		}
 		return $codigo;
	}	

	public function guardarEstante($registroEstante)
	{
		$this->db->trans_start();
		$this->db->insert("arc_cat_estantes", $registroEstante);
		$this->db->trans_complete();
	}

	public function updateEstante($id,$updateEstante)
	{
		$this->db->trans_start();
		$this->db->where("id_estante",$id);
		$this->db->update("arc_cat_estantes",$updateEstante);
		$this->db->trans_complete();
	}

	public function deleteEstante($id,$deleteEstante)
	{
		$this->db->trans_start();
		$this->db->where("id_estante",$id);
		$this->db->update("arc_cat_estantes",$deleteEstante);
		$this->db->trans_complete();		
	}
}