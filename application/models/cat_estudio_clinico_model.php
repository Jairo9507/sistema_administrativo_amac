<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_estudio_clinico_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarEstudios()
	{
		$sql='SELECT ID_CAT_ESTUDIO_CLINICO,DESCRIPCION,ESTADO,TIPO FROM cli_cat_estudio_clinico WHERE ESTADO=1';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveEstudio($registrarEstudio)
	{
		$this->db->trans_start();
		$this->db->insert("cli_cat_estudio_clinico",$registrarEstudio);
		$this->db->trans_complete();
	}

	public function updateEstudio($id,$updateEstudio)
	{
		$this->db->trans_start();
		$this->db->where("ID_CAT_ESTUDIO_CLINICO",$id);
		$this->db->update("cli_cat_estudio_clinico",$updateEstudio);
		$this->db->trans_complete();
	}

	public function deleteEstudio($id,$deleteEstudio)
	{
		$this->db->trans_start();
		$this->db->where("ID_CAT_ESTUDIO_CLINICO",$id);
		$this->db->update("cli_cat_estudio_clinico",$deleteEstudio);
		$this->db->trans_complete();
	}

	public function listarEstudioId($id)
	{
		$sql='SELECT ID_CAT_ESTUDIO_CLINICO,DESCRIPCION,ESTADO,TIPO FROM cli_cat_estudio_clinico WHERE ESTADO=1 and ID_CAT_ESTUDIO_CLINICO='.$id;
		$query=$this->db->query($sql);
		return $query->result();		
	}
}