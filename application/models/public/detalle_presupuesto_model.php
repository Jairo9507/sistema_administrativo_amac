<?php 
/**
 * 
 */
class detalle_ordencompra_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarDetalle(){
		$sql='SELECT a.CODIGO_ORDEN_COMPRA CODIGO_ORDEN, a.FECHA, a.NUMERO_FACTURA, a.USUARIO_CREACION USER, b.DESCRIPCION FROM inv_orden_compra a
			LEFT JOIN seg_cat_unidad_administrativa b on (a.UNIDAD_CREA = b.ID_UNIDAD)
			ORDER BY fecha DESC';

		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarOrdenCompraId($id)
	{
		$sql = 'SELECT CODIGO_ORDEN_COMPRA CODIGO_ORDEN, FECHA, NUMERO_FACTURA, USUARIO_CREACION, b.DESCRIPCION 
			FROM inv_orden_compra a
			LEFT JOIN seg_cat_unidad_administrativa b
			ON (a.UNIDAD_CREA = b.CODIGO_UNIDAD) WHERE a.CODIGO_ORDEN_COMPRA ='.$id;

		$query = $this->db->query($sql);
		return $query->result();
	}
	public function listarDetalleProductos($id)
	{
		$sql='SELECT d.ID_DETALLE_ORDEN_COMPRA ID_DETALLE,d.ID_PRODUCTO, p.NOMBRE_PRODUCTO NOMBRE_PRODUCTO, CANTIDAD, PRECIO_UNITARIO PRECIO, ORDEN_COMPRA CODIGO_ORDEN, truncate((CANTIDAD*PRECIO_UNITARIO),2) AS MTOTAL 
FROM inv_detalle_orden_compra d LEFT JOIN inv_cat_producto p
				ON (d.id_producto=p.ID_PRODUCTO) WHERE ORDEN_COMPRA='.$id;
			$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarProductos(){
		$sql = 'select * from inv_cat_producto';
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function obtenerTotal($id)
	{
		$sql='SELECT truncate((sum(cantidad*PRECIO_UNITARIO)),2) TOTAL 
				FROM inv_detalle_orden_compra where orden_compra='.$id;
				$query=$this->db->query($sql);
				return $query->result();
	}
}