<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_enfermedad_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarEnfermedades()
	{
		$sql='SELECT ID_CAT_ENFERMEDAD,CODIGO,DESCRIPCION,ESTADO from cli_cat_enfermedad WHERE ESTADO=1';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveEnfermedad($registraEnfermedad)
	{
		$this->db->trans_start();
		$this->db->insert("cli_cat_enfermedad",$registraEnfermedad);
		$this->db->trans_complete();
	}

	public function updateEnfermedad($id,$updateEnfermedad)
	{
		$this->db->trans_start();
		$this->db->where("ID_CAT_ENFERMEDAD",$id);
		$this->db->update("cli_cat_enfermedad",$updateEnfermedad);
		$this->db->trans_complete();
	}

	public function deleteEnfermedad($id,$deleteEnfermedad)
	{
		$this->db->trans_start();
		$this->db->where("ID_CAT_ENFERMEDAD",$id);
		$this->db->update("cli_cat_enfermedad",$deleteEnfermedad);
		$this->db->trans_complete();
	}

	public function listarEnfermedadId($id)
	{
		$sql='SELECT ID_CAT_ENFERMEDAD,CODIGO,DESCRIPCION,ESTADO from cli_cat_enfermedad where ESTADO=1 and ID_CAT_ENFERMEDAD='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}
}