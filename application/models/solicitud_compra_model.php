<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class solicitud_compra_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarSolicitudes()
	{
		$sql="SELECT so.ID_SOLICITUD,so.CODIGO,ua.DESCRIPCION,so.FECHA_SOLICITUD,so.ESTADO,so.USUARIO_CREACION,so.FECHA_CREACION FROM inv_solicitud so inner join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=so.ID_UNIDAD_SOLICITA WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveSolicitud($registrarSolicitud)
	{
		$this->db->trans_start();
		$this->db->insert("inv_solicitud",$registrarSolicitud);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function saveDetalle($registrarDetalle)
	{
		$this->db->trans_start();
		$this->db->insert("inv_solicitud_detalle",$registrarDetalle);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();

		return $last_id;
	}

	public function updateSolicitud($id,$updateSolicitud)
	{
		$this->db->trans_start();
		$this->db->where("ID_SOLICITUD",$id);
		$this->db->update("inv_solicitud",$updateSolicitud);
		$this->db->trans_complete();
	}

	public function updateDetalle($id,$updateDetalle)
	{
		$this->db->trans_start();
		$this->db->where("ID_DETALLE_SOLICITUD",$id);
		$this->db->update("inv_solicitud_detalle",$updateDetalle);
		$this->db->trans_complete();
	}

		public function listarSolicitudId($id)
	{
		$sql="SELECT so.ID_SOLICITUD,so.CODIGO,ua.DESCRIPCION,so.FECHA_SOLICITUD,so.ESTADO,so.USUARIO_CREACION,so.FECHA_CREACION FROM inv_solicitud so inner join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=so.ID_UNIDAD_SOLICITA WHERE ESTADO=1 and ID_SOLICITUD=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarDetalles($id)
	{
		$sql="SELECT de.ID_DETALLE_SOLICITUD as ID_DETALLE,p.NOMBRE_PRODUCTO,de.CANTIDAD,de.ID_PRODUCTO,p.COD_PRODUCTO from inv_solicitud_detalle de inner join inv_cat_producto p on de.id_producto=p.id_producto where id_solicitud=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarProductos(){
		$sql='SELECT p.COD_PRODUCTO,p.ID_PRODUCTO,p.NOMBRE_PRODUCTO,p.MINIMO,p.MAXIMO,pr.NOMBRE_PROVEEDOR, tp.DESCRIPCION as TIPO,ua.NOMBRE_CUENTA,um.DESCRIPCION as UNIDAD from inv_cat_producto p inner join inv_cat_proveedor pr on pr.id_proveedor=p.id_proveedor inner join inv_cat_tipo_producto tp on tp.id_tipo_producto=p.inv_id_tipo_producto inner join inv_cat_cuentas ua on ua.id_cat_cuenta=p.id_cat_cuenta inner join inv_cat_unidad_medida um on um.id_cat_unidad_medida=p.id_cat_unidad_medida WHERE ACTIVO=1';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function ultimoCodigo()
	{
		$sql="SELECT * FROM inv_solicitud ORDER BY ID_SOLICITUD DESC LIMIT 1";
		$query=$this->db->query($sql);
		$corr;
		foreach ($query->result_array() as $c) {
			$corr=$c['CODIGO'];
		}
		return $corr;
	}

	


}