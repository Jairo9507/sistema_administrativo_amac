<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_tipo_producto_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarTipos()
	{
		$sql='SELECT * from inv_cat_tipo_producto WHERE ESTADO=1';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveTipoProducto($registrarTipoProducto)
	{
		$this->db->trans_start();
		$this->db->insert("inv_cat_tipo_producto",$registrarTipoProducto);
		$this->db->trans_complete();
	}

	public function updateTipoProducto($id,$updateTipoProducto)
	{
		$this->db->trans_start();
		$this->db->where("id_tipo_producto",$id);
		$this->db->update("inv_cat_tipo_producto",$updateTipoProducto);
		$this->db->trans_complete();
	}

	public function deleteTipoProducto($id,$deleteTipoProducto)
	{
		$this->db->trans_start();
		$this->db->where("id_tipo_producto",$id);
		$this->db->update("inv_cat_tipo_producto",$deleteTipoProducto);
		$this->db->trans_complete();		
	}

	public function listarTipoProductoId($id)
	{
		$sql='SELECT id_tipo_producto,descripcion from inv_cat_tipo_producto WHERE id_tipo_producto='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}
}