<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class kardex_model extends CI_Model //el model debe llamarse igual que el archivo
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarProductos($unidad)
	{
		$sql = 'SELECT p.*,cp.*,IFNULL(CONCAT(me.FORMA_FARMACEUTICA," ",me.PRESENTACION," ",me.CONCENTRACION)," ") as MEDICAMENTO FROM inv_cat_producto p 
					LEFT JOIN inv_cat_cuentas cp
					ON (cp.ID_CAT_CUENTA = p.ID_CAT_CUENTA)
					LEFT JOIN cli_cat_medicamento me 
					ON (me.id_producto=p.id_producto)
					where p.activo=1 and 
					exists(SELECT * FROM inv_inventario_fisico fi 
					where fi.id_producto=p.id_producto) and p.id_unidad_administrativa='.$unidad.'
					ORDER BY cp.COD_CUENTA ASC,p.NOMBRE_PRODUCTO ASC';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function obtenerExistencias($fechaInicio,$fechaFinal,$producto)
	{
		$sql = 'SELECT his.ID_PRODUCTO, pro.NOMBRE_PRODUCTO, cp.COD_CUENTA, pro.COD_PRODUCTO,  his.PRECIO,IFNULL(CONCAT(me.FORMA_FARMACEUTICA," ",me.PRESENTACION," ",me.CONCENTRACION)," ") as MEDICAMENTO, 
				IFNULL(SUM(his.CANTIDAD_INVENTARIO)+(SELECT SUM(mo.CANTIDAD) as SUMA from inv_movimientos mo 
				WHERE DATE_FORMAT(mo.FECHA, "%Y-%m-%d") 
				BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND 
				DATE_FORMAT(now(), "%Y-%m-%d") and his.ID_ORDEN_COMPRA=his.ID_ORDEN_COMPRA 
				and mo.ID_TIPO_MOVIMIENTO=2 and mo.id_producto='.$producto.' and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4) and his.ID_HISTORICO_INVENTARIO=mo.ID_HISTORICO_PERTENECE),his.CANTIDAD_INVENTARIO) 
				CANTIDAD, oc.CODIGO_ORDEN_COMPRA
				FROM cat_historico_inventario his
				LEFT JOIN inv_cat_producto pro 
				ON (his.ID_PRODUCTO = pro.ID_PRODUCTO)
				LEFT JOIN seg_cat_unidad_administrativa ua
				ON (ua.ID_UNIDAD = pro.ID_UNIDAD_ADMINISTRATIVA)
				LEFT JOIN inv_cat_cuentas cp
                ON (cp.ID_CAT_CUENTA = pro.ID_CAT_CUENTA)
                LEFT JOIN inv_orden_compra oc 
                ON (his.ID_ORDEN_COMPRA= oc.ID_ORDEN_COMPRA)
                LEFT JOIN cli_cat_medicamento me 
                ON (me.id_producto=pro.id_producto)
				WHERE  his.ID_PRODUCTO='.$producto.'
				and DATE_FORMAT(FECHA,"%Y-%m-%d")<=
				DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") 
				and exists(SELECT * from inv_movimientos WHERE 
				DATE_FORMAT(his.FECHA_INICIO_VIGENCIA,"%Y-%m-%d")<DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND ID_TIPO_MOVIMIENTO=1 AND ID_PRODUCTO='.$producto.')
				GROUP BY his.ID_ORDEN_COMPRA
				HAVING CANTIDAD>0
				ORDER BY FECHA_INICIO_VIGENCIA ASC';
				//var_dump($sql);exit;
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function ObtenerPrimeraExistencia($fechaInicio,$producto,
		$fechaFinal,$fechamov,$sumas)
	{
		$sql = 'SELECT his.PRECIO,his.ID_HISTORICO_INVENTARIO,oc.ID_ORDEN_COMPRA,
				IFNULL(his.CANTIDAD_INVENTARIO+(SELECT SUM(mo.CANTIDAD) from inv_movimientos mo 
						where id_producto='.$producto.' and DATE_FORMAT(mo.FECHA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND DATE_FORMAT(now(),"%Y-%m-%d") and id_tipo_movimiento=2 and mo.id_movimiento and ROUND(mo.COSTO_UNITARIO,4)=ROUND(his.PRECIO,4) and his.ID_HISTORICO_INVENTARIO=mo.ID_HISTORICO_PERTENECE),his.CANTIDAD) CANTIDAD, IFNULL((SELECT SUM(mo.CANTIDAD) from inv_movimientos mo where id_producto='.$producto.'  and id_tipo_movimiento=2 and DATE_FORMAT(mo.FECHA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") 
				AND DATE_FORMAT("'.$fechamov.'","%Y-%m-%d") and mo.id_movimiento in('.$sumas.')  and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4) and mo.ID_HISTORICO_PERTENECE=his.ID_HISTORICO_INVENTARIO),0) SALIDAS,his.FECHA_FIN_VIGENCIA,his.FECHA_INICIO_VIGENCIA 
				FROM cat_historico_inventario his LEFT JOIN inv_orden_compra oc on oc.id_orden_compra=his.id_orden_compra
				WHERE his.id_producto='.$producto.'
				AND DATE_FORMAT(his.FECHA_FIN_VIGENCIA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechamov.'","%Y-%m-%d ")
				AND DATE_FORMAT("'.$fechaFinal.'","%Y-%m-%d")/* AND his.CANTIDAD_INVENTARIO>0*/
				HAVING (CANTIDAD-SALIDAS)>0
				union 
				SELECT his.PRECIO,his.ID_HISTORICO_INVENTARIO,oc.ID_ORDEN_COMPRA,
				IFNULL(his.CANTIDAD_INVENTARIO+(SELECT SUM(mo.CANTIDAD) 
				from inv_movimientos mo where id_producto='.$producto.' and DATE_FORMAT(mo.FECHA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") 
				AND DATE_FORMAT(now(),"%Y-%m-%d")  and id_tipo_movimiento=2 and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4)  and his.ID_HISTORICO_INVENTARIO=mo.ID_HISTORICO_PERTENECE),his.CANTIDAD) CANTIDAD, 
				IFNULL((SELECT SUM(mo.CANTIDAD) from inv_movimientos mo where id_producto='.$producto.' and mo.id_movimiento in('.$sumas.') and id_tipo_movimiento=2 and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4) and mo.ID_HISTORICO_PERTENECE=his.ID_HISTORICO_INVENTARIO),0) 		 SALIDAS,his.FECHA_FIN_VIGENCIA,his.FECHA_INICIO_VIGENCIA 
				FROM cat_historico_inventario his LEFT JOIN inv_orden_compra oc on oc.id_orden_compra=his.id_orden_compra
				WHERE his.id_producto='.$producto.' and FECHA_FIN_VIGENCIA is null 
				and DATE_FORMAT(oc.FECHA,"%Y-%m-%d")<=DATE_FORMAT("'.$fechamov.'","%Y-%m-%d")
				AND DATE_FORMAT(his.FECHA_INICIO_VIGENCIA,"%Y-%m-%d %H:%i")<=DATE_FORMAT("'.$fechamov.'","%Y-%m-%d %H:%i") 
				AND his.CANTIDAD_INVENTARIO>0
				ORDER BY FECHA_INICIO_VIGENCIA ASC';
		$query = $this->db->query($sql);
		return $query->result();		
	}

	public function entradasAnteriores($producto,$fechaInicio,$fechaFinal,
		$fechamov,$idsalidas,$compra)
	{

		$sql='SELECT his.PRECIO,his.ID_HISTORICO_INVENTARIO,(SELECT SUM(mo.CANTIDAD) from inv_movimientos mo  WHERE mo.id_producto=his.id_producto and mo.ID_ORDEN_COMPRA=oc.ID_ORDEN_COMPRA and id_movimiento in('.$idsalidas.')) as TOTAL_ENTRADAS,oc.ID_ORDEN_COMPRA,
				IFNULL(his.CANTIDAD_INVENTARIO+(SELECT SUM(mo.CANTIDAD) 
				from inv_movimientos mo where id_producto='.$producto.' and DATE_FORMAT(mo.FECHA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") 
				AND DATE_FORMAT("'.$fechaFinal.'","%Y-%m-%d")  and id_tipo_movimiento=2 and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4) and his.ID_HISTORICO_INVENTARIO=mo.ID_HISTORICO_PERTENECE),his.CANTIDAD)-IFNULL((SELECT SUM(mo.CANTIDAD) from inv_movimientos mo where id_producto='.$producto.' and DATE_FORMAT(mo.FECHA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") 
				AND DATE_FORMAT("'.$fechamov.'","%Y-%m-%d") and mo.id_movimiento in('.$idsalidas.') and id_tipo_movimiento=2 and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4) and his.ID_HISTORICO_INVENTARIO=mo.ID_HISTORICO_PERTENECE ),0) CANTIDAD, 
				IFNULL((SELECT SUM(mo.CANTIDAD)from inv_movimientos mo where id_producto='.$producto.' and mo.id_movimiento in('.$idsalidas.') and id_tipo_movimiento=2),0) SALIDAS,his.FECHA_FIN_VIGENCIA,his.FECHA_INICIO_VIGENCIA 
				FROM cat_historico_inventario his LEFT JOIN inv_orden_compra oc on oc.id_orden_compra=his.id_orden_compra
				WHERE his.id_producto='.$producto.'
				AND DATE_FORMAT(his.FECHA_FIN_VIGENCIA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d ")
				AND DATE_FORMAT("'.$fechaFinal.'","%Y-%m-%d")
				HAVING CANTIDAD>0
				union 
				SELECT his.PRECIO,his.ID_HISTORICO_INVENTARIO,(SELECT SUM(mo.CANTIDAD) from inv_movimientos mo WHERE mo.id_producto=his.id_producto and mo.ID_ORDEN_COMPRA=oc.ID_ORDEN_COMPRA and id_movimiento in('.$idsalidas.')) as TOTAL_ENTRADAS,oc.ID_ORDEN_COMPRA,
				IFNULL(his.CANTIDAD_INVENTARIO+(SELECT SUM(mo.CANTIDAD) 
				from inv_movimientos mo where id_producto='.$producto.' and DATE_FORMAT(mo.FECHA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") 
				AND DATE_FORMAT(now(),"%Y-%m-%d")  and id_tipo_movimiento=2 and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4) and his.ID_HISTORICO_INVENTARIO=mo.ID_HISTORICO_PERTENECE),his.CANTIDAD)-IFNULL((SELECT SUM(mo.CANTIDAD) from inv_movimientos mo where id_producto='.$producto.' and DATE_FORMAT(mo.FECHA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") 
				AND DATE_FORMAT("'.$fechamov.'","%Y-%m-%d") and mo.id_movimiento in('.$idsalidas.') and id_tipo_movimiento=2 and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4) and his.ID_HISTORICO_INVENTARIO=mo.ID_HISTORICO_PERTENECE ),0) CANTIDAD, 
				IFNULL((SELECT SUM(mo.CANTIDAD)from inv_movimientos mo where id_producto='.$producto.' and mo.id_movimiento  and id_tipo_movimiento=2 
				and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4)),0) SALIDAS,his.FECHA_FIN_VIGENCIA,his.FECHA_INICIO_VIGENCIA 
				FROM cat_historico_inventario his LEFT JOIN inv_orden_compra oc on oc.id_orden_compra=his.id_orden_compra
				WHERE his.id_producto='.$producto.'	 	
				 AND (his.FECHA_INICIO_VIGENCIA is not null AND FECHA_FIN_VIGENCIA is null)
				-- AND (oc.ID_ORDEN_COMPRA in('.$compra.') AND his.ID_ORDEN_COMPRA=oc.ID_ORDEN_COMPRA)
				AND DATE_FORMAT(his.FECHA_INICIO_VIGENCIA,"%Y-%m-%d %H:%i")<=DATE_FORMAT("'.$fechamov.'","%Y-%m-%d %H:%i") 	
				HAVING CANTIDAD>0
				ORDER BY FECHA_INICIO_VIGENCIA ASC';
				//var_dump($sql);exit;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saldosActuales($fechaFinal,$producto){
		$sql = 'SELECT his.ID_PRODUCTO,  pro.NOMBRE_PRODUCTO, cp.COD_CUENTA, pro.COD_PRODUCTO,  his.PRECIO,SUM(his.CANTIDAD_INVENTARIO) 
				CANTIDAD, oc.CODIGO_ORDEN_COMPRA,COUNT(*) CONTAR
				FROM cat_historico_inventario his
				LEFT JOIN inv_cat_producto pro 
				ON (his.ID_PRODUCTO = pro.ID_PRODUCTO)
				LEFT JOIN seg_cat_unidad_administrativa ua
				ON (ua.ID_UNIDAD = pro.ID_UNIDAD_ADMINISTRATIVA)
				LEFT JOIN inv_cat_cuentas cp
                ON (cp.ID_CAT_CUENTA = pro.ID_CAT_CUENTA)
                LEFT JOIN inv_orden_compra oc 
                ON (his.ID_ORDEN_COMPRA= oc.ID_ORDEN_COMPRA)
				WHERE  his.ID_PRODUCTO='.$producto.'	
				AND DATE_FORMAT(FECHA_INICIO_VIGENCIA,"%Y-%m-%d")<=	"'.$fechaFinal.'" 
				AND FECHA_FIN_VIGENCIA is null and his.CANTIDAD_INVENTARIO<>0
				GROUP BY his.PRECIO
				ORDER BY his.ID_HISTORICO_INVENTARIO ASC';
				
		$query = $this->db->query($sql);
		return $query->result();		
	}



	public function obtenerProductoId($id)
	{
		$sql='SELECT p.NOMBRE_PRODUCTO,p.COD_PRODUCTO,cp.cod_cuenta as COD_CUENTA,IFNULL(CONCAT(me.FORMA_FARMACEUTICA," ",me.PRESENTACION," ",me.CONCENTRACION)," ") as MEDICAMENTO from inv_cat_producto p inner join inv_cat_cuentas cp on cp.id_cat_cuenta=p.id_cat_cuenta LEFT JOIN cli_cat_medicamento me on me.id_producto=p.id_producto WHERE p.ID_PRODUCTO='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function salidasProducto($fechaInicio, $fechaFinal, $producto)
	{
		$sql="SELECT SUM(mo.CANTIDAD) as SUMA from inv_movimientos mo WHERE DATE_FORMAT(mo.FECHA, '%Y-%m-%d') BETWEEN DATE_FORMAT('".$fechaInicio."','%Y-%m-%d') AND DATE_FORMAT('".$fechaFinal."', '%Y-%m-%d') and mo.ID_TIPO_MOVIMIENTO=2 and mo.id_producto=".$producto;
		$query=$this->db->query($sql);
		$total=0;
		foreach ($query->result_array() as $q) {
			$total=$q['SUMA'];
		}
		return $total;
	}

	public function obtenerKardex($fechaInicio, $fechaFinal, $producto)
	{
		$sql = 'SELECT mo.ID_MOVIMIENTO,mo.FECHA,IFNULL(oc.ID_ORDEN_COMPRA ,"0") as ID_ORDEN_COMPRA,pr.NOMBRE_PROVEEDOR, oc.CODIGO_ORDEN_COMPRA,mo.FECHA_CREACION,CONCAT(SUBSTRING(req.CORRELATIVO,1,2),SUBSTRING(req.CORRELATIVO,11,char_length(req.CORRELATIVO))) as CORRELATIVO,
			CASE WHEN mo.ID_TIPO_MOVIMIENTO = 1 THEN DATE_FORMAT(mo.FECHA, "%Y-%m-%d %H:%i") ELSE IFNULL(DATE_FORMAT(req.FECHA, "%Y-%m-%d %H:%i"),DATE_FORMAT(re.FECHA_RECETA,"%Y-%m-%d %H:%i"))   END FECHA_MOVIMIENTO, CASE WHEN mo.ID_TIPO_MOVIMIENTO = 1 THEN CONCAT("ORDEN N° ", oc.CODIGO_ORDEN_COMPRA)
			ELSE CONCAT("EGRESO " ) END MOVIMIENTO,
			CASE WHEN mo.ID_TIPO_MOVIMIENTO = 1 THEN mo.CANTIDAD
			ELSE " " END CANTIDAD_ENTRADA, 
			CASE WHEN  mo.ID_TIPO_MOVIMIENTO = 1 THEN mo.COSTO_UNITARIO 
			ELSE " " END COSTO_ENTRADA, 
			CASE WHEN mo.ID_TIPO_MOVIMIENTO = 2 THEN mo.CANTIDAD
			ELSE " " END CANTIDAD_SALIDA,
			CASE WHEN mo.ID_TIPO_MOVIMIENTO = 2 THEN mo.COSTO_UNITARIO
			ELSE " " END COSTO_SALIDA,
            CASE WHEN ur.DESCRIPCION is not null THEN ur.DESCRIPCION
            ELSE IFNULL(CONCAT("PACIENTE No.",(select CODIGO_EXPEDIENTE from cli_expediente_clinico e inner join cli_paciente p on p.id_paciente=e.id_paciente inner join cli_cita ci on
            ci.id_paciente=p.id_paciente inner join cli_consulta co on ci.id_cita=co.id_cita inner join cli_receta r on co.id_consulta=r.id_consulta where r.id_receta=mo.id_receta)),
            CONCAT("BRIGADA EN ",b.COMUNIDAD))
            END UNIDAD_SOLICITA	, mo.FECHA_CREACION,mo.ID_HISTORICO_PERTENECE
			FROM inv_movimientos mo
			LEFT JOIN inv_cat_producto pro
			ON (pro.ID_PRODUCTO = mo.ID_PRODUCTO)
			LEFT JOIN inv_orden_compra oc
			ON (oc.ID_ORDEN_COMPRA =mo.ID_ORDEN_COMPRA)
			LEFT JOIN inv_requisicion req
			ON (req.ID_REQUISICION = mo.ID_REQUISICION)
			LEFT JOIN seg_cat_unidad_administrativa ua
			ON (ua.ID_UNIDAD = pro.ID_UNIDAD_ADMINISTRATIVA)
			LEFT JOIN inv_cat_proveedor pr 
			ON (pr.ID_PROVEEDOR=oc.ID_PROVEEDOR)
			LEFT JOIN seg_cat_unidad_administrativa ur 
			ON (ur.ID_UNIDAD=req.ID_UNIDAD_SOLICITA)
            LEFT JOIN cli_cat_medicamento me 
            ON(me.ID_PRODUCTO=pro.ID_PRODUCTO)
            LEFT JOIN cli_receta re
            ON(re.ID_MEDICAMENTO=me.ID_MEDICAMENTO)
            LEFT JOIN cli_brigada b 
            ON(mo.ID_BRIGADA=b.ID_BRIGADA)            
			WHERE mo.ID_PRODUCTO='.$producto.' 
			AND DATE_FORMAT(mo.FECHA ,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND DATE_FORMAT("'.$fechaFinal.'","%Y-%m-%d") 			
			GROUP BY mo.ID_PRODUCTO, mo.COSTO_UNITARIO, mo.ID_TIPO_MOVIMIENTO,mo.FECHA,mo.ID_MOVIMIENTO
			ORDER BY FECHA_CREACION ASC,mo.FECHA ASC,mo.ID_HISTORICO_PERTENECE';
			//var_dump($sql);exit;
			$query = $this->db->query($sql);
			return $query->result();
	}

	public function obtenerBodega($id)
	{
		$sql = 'SELECT 	CONCAT(em.PRIMER_NOMBRE, "  ", em.PRIMER_APELLIDO) NOMBRE, ua.DESCRIPCION 	 FROM seg_empleado em
				LEFT JOIN seg_cat_unidad_administrativa ua 
				ON (em.ID_UNIDAD = ua.ID_UNIDAD)
				LEFT JOIN seg_usuario usu
				ON (usu.ID_EMPLEADO = em.ID_EMPLEADO)
				WHERE usu.ID_USUARIO='.$id;
				$query = $this->db->query($sql);
				return $query->result();
	}

	public function nombreJefatura($id)
	{
		$sql="SELECT * from seg_usuario us inner join  seg_empleado em on em.ID_EMPLEADO=us.ID_EMPLEADO inner join seg_usuarios_perfiles up on up.id_usuario=us.id_usuario inner join seg_perfil p on p.ID_PERFIL_USUARIO=up.ID_PERFIL_USUARIO  WHERE (p.ID_PERFIL_USUARIO=2 or p.ID_PERFIL_USUARIO=3) AND us.ESTADO_USUARIO=1 and em.ID_UNIDAD=".$id;

		$query=$this->db->query($sql);
		$empleado='';
		foreach ($query->result_array() as $q) {
			$empleado=$q['PRIMER_NOMBRE']." ".$q['PRIMER_APELLIDO']." ";
		}
		return $empleado;	
	}
}