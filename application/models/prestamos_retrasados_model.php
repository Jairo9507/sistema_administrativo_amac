<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class prestamos_retrasados_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarRetrasos()
	{
		$sql="SELECT dt.FECHA_REGRESO_ARCHIVO_CENTRAL,dt.FECHA_SALIDA_ARCHIVO_CENTRAL,t.FECHA_TRANSFERENCIA,t.CODIGO,d.CORRELATIVO_CENTRAL,
			ua.DESCRIPCION as UNIDAD,CONCAT(IFNULL(em.PRIMER_NOMBRE,''),' ',IFNULL(em.SEGUNDO_NOMBRE,''),' ',IFNULL(em.PRIMER_APELLIDO,''),' ',IFNULL(em.SEGUNDO_APELLIDO,'')) as responsable,
			d.CORRELATIVO_CENTRAL as CODIGO_DOCUMENTO,d.CONTENIDO as DOCUMENTO 
			from arc_detalle_transferencia dt inner join arc_transferencia t on dt.ID_TRANSFERENCIA=t.ID_TRANSFERENCIA 
			inner join arc_documento d on d.ID_DOCUMENTO=dt.ID_DOCUMENTO inner join seg_cat_unidad_administrativa ua 
			on ua.ID_UNIDAD=t.ID_UNIDAD_SOLICITA inner join seg_empleado em on em.ID_EMPLEADO=t.ID_RESPONSABLE_ARCHIVO_GESTION 
			where DATE_FORMAT(now(),'%Y-%m-%d')>DATE_FORMAT(dt.FECHA_REGRESO_ARCHIVO_CENTRAL,'%Y-%m-%d') and d.ESTADO=2";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function contarRetrasos()
	{
		$sql="SELECT COUNT(*) as row_retrasos 
			from arc_detalle_transferencia dt inner join arc_transferencia t on dt.ID_TRANSFERENCIA=t.ID_TRANSFERENCIA 
			inner join arc_documento d on d.ID_DOCUMENTO=dt.ID_DOCUMENTO inner join seg_cat_unidad_administrativa ua 
			on ua.ID_UNIDAD=t.ID_UNIDAD_SOLICITA inner join seg_empleado em on em.ID_EMPLEADO=t.ID_RESPONSABLE_ARCHIVO_GESTION 
			where DATE_FORMAT(now(),'%Y-%m-%d')<DATE_FORMAT(dt.FECHA_REGRESO_ARCHIVO_CENTRAL,'%Y-%m-%d')and d.ESTADO=2";
		$query=$this->db->query($sql);
		$numero=0;
		foreach ($query->result_array() as $q) {
			$numero=$q['row_retrasos'];
		}
		return $numero;
	}

}