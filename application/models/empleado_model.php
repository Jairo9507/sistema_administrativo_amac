<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class empleado_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarEmpleados()
	{
		$sql="SELECT em.ID_EMPLEADO,em.CODIGO_EMPLEADO,CONCAT(em.PRIMER_NOMBRE,' ',em.SEGUNDO_NOMBRE,' ',em.PRIMER_APELLIDO,' ',em.SEGUNDO_APELLIDO) as nombre_completo,ua.DESCRIPCION as UNIDAD,mu.DESCRIPCION as MUNICIPIO 
			from seg_empleado em left join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=em.ID_UNIDAD left join seg_cat_municipio mu on mu.ID_CAT_MUNICIPIO=em.ID_CAT_MUNICIPIO WHERE em.ESTADO=1 ORDER BY em.ID_EMPLEADO DESC";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEmpleadoId($id)
	{
		$sql="SELECT em.ID_EMPLEADO,em.CODIGO_EMPLEADO,em.PRIMER_NOMBRE,em.SEGUNDO_NOMBRE,em.PRIMER_APELLIDO,em.SEGUNDO_APELLIDO,em.APELLIDO_CASADA,em.SEXO,em.ESTADO_CIVIL,ua.ID_UNIDAD as UNIDAD,mu.ID_CAT_MUNICIPIO as MUNICIPIO 
			from seg_empleado em inner join seg_cat_unidad_administrativa ua on ua.ID_UNIDAD=em.ID_UNIDAD left join seg_cat_municipio mu on mu.ID_CAT_MUNICIPIO=em.ID_CAT_MUNICIPIO WHERE em.ESTADO=1 and em.ID_EMPLEADO=".$id;
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarUnidades()
	{
		$sql="SELECT ID_UNIDAD,DESCRIPCION,CODIGO_UNIDAD from seg_cat_unidad_administrativa where ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarMunicipios()
	{
		$sql="SELECT ID_CAT_MUNICIPIO,DESCRIPCION from seg_cat_municipio";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveEmpleado($registrarEmpleado)
	{
		$this->db->trans_start();
		$this->db->insert("seg_empleado",$registrarEmpleado);
		$this->db->trans_complete();
	}

	public function updateEmpleado($id,$updateEmpleado)
	{
		$this->db->trans_start();
		$this->db->where("id_empleado",$id);
		$this->db->update("seg_empleado",$updateEmpleado);
		$this->db->trans_complete();
	}

	public function deleteEmpleado($id,$deleteEmpleado)
	{
		$this->db->trans_start();
		$this->db->where("id_empleado",$id);
		$this->db->update("seg_empleado",$deleteEmpleado);
		$this->db->trans_complete();		
	}
}