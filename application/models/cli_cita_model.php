<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cli_cita_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function listarCitas($idMedico)
	{
		$sql='SELECT c.ID_CITA,me.ID_MEDICO,CONCAT(em.PRIMER_NOMBRE," ",em.SEGUNDO_NOMBRE," ",em.PRIMER_APELLIDO," ",em.SEGUNDO_APELLIDO) as nombre_medico, 
			CONCAT(pa.PRIMER_NOMBRE," ",pa.SEGUNDO_NOMBRE," ",pa.TERCER_NOMBRE," ",pa.PRIMER_APELLIDO," ",pa.SEGUNDO_APELLIDO," ",pa.TERCER_APELLIDO) as nombre_paciente,
			c.ESTADO,c.ASUNTO,DATE_FORMAT(c.FECHA_HORA,"%Y-%m-%d") as FECHA_INICIO,(DATE_FORMAT(c.FECHA_HORA,"%Y-%m-%d") + INTERVAL 1 DAY)as FECHA_FINAL
			from cli_cita c inner join cli_medico me on c.ID_MEDICO=me.ID_MEDICO inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO 
			inner join cli_paciente pa on pa.ID_PACIENTE=c.ID_PACIENTE WHERE c.ESTADO="En Sala De Espera" and me.ID_MEDICO='.$idMedico.' ORDER BY FECHA_HORA ASC ';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveCita($registrarCita)
	{
		$this->db->trans_start();
		$this->db->insert("cli_cita",$registrarCita);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();

		return $last_id;
	}


	public function updateCita($id,$updateCita)
	{
		$this->db->trans_start();
		$this->db->where("ID_CITA",$id);
		$this->db->update("cli_cita",$updateCita);
		$this->db->trans_complete();
	}

	public function deleteCita($id,$deleteCita)
	{
		$this->db->trans_start();
		$this->db->where("ID_CITA",$id);
		$this->db->update("cli_cita",$deleteCita);
		$this->db->trans_complete();
	}

	public function deleteCitaF($id)
	{
		$this->db->trans_start();
		$this->db->where("ID_CITA",$id);
		$this->db->delete("cli_cita");
		$this->db->trans_complete();
	}

	public function saveEstudioCita($registrarCita)
	{
		$this->db->trans_start();
		$this->db->insert("cli_detalle_estudios",$registrarCita);
		$this->db->trans_complete();
	}

	public function deleteEstudioCita($id)
	{
		$this->db->trans_start();
		$this->db->where("ID_DETALLE_ESTUDIOS",$id);
		$this->db->delete("cli_detalle_estudios");
		$this->db->trans_complete();
	}

	public function listarMedico()
	{
		$sql="SELECT me.ID_MEDICO,CONCAT(em.PRIMER_NOMBRE,' ',em.SEGUNDO_NOMBRE,' ',em.PRIMER_APELLIDO,' ',em.SEGUNDO_APELLIDO) as nombre_completo from cli_medico me inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO WHERE me.ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarPacientes()
	{
		$sql='SELECT ID_PACIENTE,CONCAT(PRIMER_NOMBRE," ",SEGUNDO_NOMBRE," ",PRIMER_APELLIDO," ",SEGUNDO_APELLIDO," ",TERCER_APELLIDO) as nombre_completo FROM cli_paciente WHERE ESTADO=1';
		$query=$this->db->query($sql);
		return $query->result();
	}	

	public function listarCitaId($id)
	{
		$sql='SELECT c.ID_CITA,me.ID_MEDICO,pa.ID_PACIENTE,c.ESTADO,c.ASUNTO,c.FECHA_HORA from cli_cita c inner join cli_medico me on c.ID_MEDICO=me.ID_MEDICO inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO 
			inner join cli_paciente pa on pa.ID_PACIENTE=c.ID_PACIENTE WHERE c.ID_CITA='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEstudiosCita($id)
	{
		$sql="SELECT ec.ID_CAT_ESTUDIO_CLINICO,ec.DESCRIPCION,ds.ESTADO,ds.ID_DETALLE_ESTUDIOS FROM cli_detalle_estudios ds inner join cli_cat_estudio_clinico ec on ec.ID_CAT_ESTUDIO_CLINICO=ds.ID_CAT_ESTUDIO_CLINICO WHERE  ID_CITA=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEstudios()
	{
		$sql="SELECT ID_CAT_ESTUDIO_CLINICO,DESCRIPCION,ESTADO FROM cli_cat_estudio_clinico WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEstudioId($id)
	{
		$sql="SELECT ID_CAT_ESTUDIO_CLINICO,DESCRIPCION,ESTADO FROM cli_cat_estudio_clinico WHERE ESTADO=1 and ID_CAT_ESTUDIO_CLINICO=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function EstudioExiste($idestudio,$idcita)
	{
		$sql="SELECT * FROM cli_detalle_estudios WHERE ESTADO=1 and ID_CAT_ESTUDIO_CLINICO=".$idestudio." and ID_CITA=".$idcita;
		$query=$this->db->query($sql);
		$row=$query->row();
		if (isset($row)) {
			return true;
		} else {
			return false;
		}
	}
}