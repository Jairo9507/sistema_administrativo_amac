<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_actividad_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarActividad()
	{
		$sql = 'SELECT * FROM vyc_cat_actividad WHERE ESTADO=1';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function guardarActividad($registrarActividad)
	{
		$this->db->trans_start();
		$this->db->insert("vyc_cat_actividad",$registrarActividad);
		$this->db->trans_complete();
	}

	public function listarActividadId($id)
	{
		$sql = 'SELECT * FROM vyc_cat_actividad WHERE ID_ACTIVIDAD ='.$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function updateActividad($id, $updateActividad)
	{
		$this->db->trans_start();
		$this->db->where("ID_ACTIVIDAD",$id);
		$this->db->update("vyc_cat_actividad",$updateActividad);
		$this->db->trans_complete();
	}

	public function deleteActividad($id, $deleteActividad)
	{
		$this->db->trans_start();
		$this->db->where("ID_ACTIVIDAD",$id);
		$this->db->update("vyc_cat_actividad",$deleteActividad);
		$this->db->trans_complete();
	}
}