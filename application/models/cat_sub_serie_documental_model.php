<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_sub_serie_documental_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarSubseries()
	{
		$sql="SELECT ss.ID_SUB_SERIE_DOCUMENTAL,ss.NOMBRE,ss.DESCRIPCION,ss.ESTADO, sd.NOMBRE as SERIE,ss.CODIGO,ss.TIPOLOGIA FROM arc_serie_documental sd inner join arc_sub_serie_documental ss on sd.ID_SERIE_DOCUMENTAL=ss.ID_SERIE_DOCUMENTAL WHERE ss.ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarSubserieId($id)
	{
		$sql="SELECT ss.ID_SUB_SERIE_DOCUMENTAL,ss.NOMBRE,ss.DESCRIPCION,ss.ESTADO, ss.ID_SERIE_DOCUMENTAL,CODIGO,ss.TIPOLOGIA FROM arc_sub_serie_documental ss  WHERE ss.ESTADO=1 and ss.ID_SUB_SERIE_DOCUMENTAL=".$id;
		$query=$this->db->query($sql);
		return $query->result();		
	}
	public function listarSeries()
	{
		$sql="SELECT ID_SERIE_DOCUMENTAL,NOMBRE,CODIGO FROM arc_serie_documental sd WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveSubserie($registrarSubserie)
	{
		$this->db->trans_start();
		$this->db->insert("arc_sub_serie_documental",$registrarSubserie);
		$this->db->trans_complete();
	}

	public function updateSubserie($id,$updateSubserie)
	{
		$this->db->trans_start();
		$this->db->where("ID_SUB_SERIE_DOCUMENTAL",$id);
		$this->db->update("arc_sub_serie_documental",$updateSubserie);
		$this->db->trans_complete();
	}

	public function deleteSubserie($id,$deleteSubserie)
	{
		$this->db->trans_start();
		$this->db->where("ID_SUB_SERIE_DOCUMENTAL",$id);
		$this->db->update("arc_sub_serie_documental",$deleteSubserie);
		$this->db->trans_complete();		
	}
}