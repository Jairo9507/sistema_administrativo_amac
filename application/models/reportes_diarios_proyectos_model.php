<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_diarios_proyectos_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarProyectosSelect()
	{
		$sql="SELECT ID_PROYECTO, CODIGO_PROYECTO, NOMBRE_PROYECTO FROM pro_proyecto WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarEntradaMaterial($id,$fechaInicio,$fechaFinal)
	{
		$sql='SELECT A.CODIGO_PROYECTO,A.NOMBRE_PROYECTO, A.NOMBRE_REALIZADOR,
		CONCAT(e.PRIMER_NOMBRE," &nbsp;",e.SEGUNDO_NOMBRE,"&nbsp; ",e.PRIMER_APELLIDO," &nbsp;",e.SEGUNDO_APELLIDO) as EMPLEADO, D.DESCRIPCION,C.CANTIDAD_RECIBIDA,C.FECHA_RECIBIDA
		FROM pro_proyecto A 
		LEFT JOIN pro_proyecto_material_recibido B ON A.ID_PROYECTO=B.ID_PROYECTO
		LEFT JOIN pro_proyecto_material_recibido_detalle C ON C.ID_PROYECTO_MATERIAL_RECIBIDO=B.ID_PROYECTO_MATERIAL_RECIBIDO
		LEFT JOIN inv_detalle_orden_compra D ON C.ID_DETALLE_ORDEN_COMPRA=D.ID_DETALLE_ORDEN_COMPRA
		LEFT JOIN seg_empleado E ON E.ID_EMPLEADO=A.ID_SUPERVISOR
		WHERE A.ID_PROYECTO='.$id.' AND DATE_FORMAT(C.FECHA_RECIBIDA,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND DATE_FORMAT("'.$fechaFinal.'","%Y-%m-%d")';
		$query = $this->db->query($sql);
		return $query->result();		
	}

	public function listarSalidaMaterial($id,$fechaInicio,$fechaFinal)
	{
		$sql='SELECT A.CODIGO_PROYECTO,A.NOMBRE_PROYECTO, A.NOMBRE_REALIZADOR,
		B.NOMBRE_SUPERVISOR, E.DESCRIPCION,C.CANTIDAD_CONSUMIDA,C.FECHA_CONSUMO
		FROM pro_proyecto A 
		LEFT JOIN pro_proyecto_consumo B ON A.ID_PROYECTO=B.ID_PROYECTO
		LEFT JOIN pro_proyecto_consumo_detalle C ON C.ID_PROYECTO_CONSUMO=B.ID_PROYECTO_CONSUMO
		LEFT JOIN pro_proyecto_material_recibido_detalle D ON D.ID_MATERIAL_RECIBIDO_DETALLE=C.ID_MATERIAL_RECIBIDO_DETALLE
		LEFT JOIN inv_detalle_orden_compra E ON D.ID_DETALLE_ORDEN_COMPRA=E.ID_DETALLE_ORDEN_COMPRA
		WHERE A.ID_PROYECTO='.$id.' AND DATE_FORMAT(C.FECHA_CONSUMO,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND DATE_FORMAT("'.$fechaFinal.'","%Y-%m-%d")';

		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarBitacorasReporte($id,$fechaInicio,$fechaFinal)
	{
		$sql='SELECT A.CODIGO_PROYECTO,A.NOMBRE_PROYECTO, A.NOMBRE_REALIZADOR,
		B.NOMBRE_SUPERVISOR as EMPLEADO, C.DESCRIPCION as TIPO_OBRA, B.MANTENIMIENTO,A.UBICACION,
		B.FECHA_INICIO, B.CLIMA_DIA,B.HORA_CREACION, B.DESCRIPCION,B.COMPROBANTE_FOTOGRAFICO,B.CLIMA_DIA
		FROM pro_proyecto A 
		LEFT JOIN pro_proyecto_bitacora_diaria B ON A.ID_PROYECTO=B.ID_PROYECTO
		LEFT JOIN pro_cat_tipo_obra C ON C.ID_CAT_TIPO_OBRA=B.ID_CAT_TIPO_OBRA
		LEFT JOIN seg_empleado E  ON E.ID_EMPLEADO=A.ID_SUPERVISOR WHERE A.ID_PROYECTO='.$id.' AND DATE_FORMAT(B.FECHA_INICIO,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d")';
		$query = $this->db->query($sql);
		return $query->result();		
	}

}