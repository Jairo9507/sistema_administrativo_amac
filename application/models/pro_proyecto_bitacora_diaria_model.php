<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class pro_proyecto_bitacora_diaria_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	public function listarBitacorasDiaria($id){
		$sql='SELECT 
			bitacora.ID_PROYECTO_BITACORA_DIARIA,
			tipoObra.DESCRIPCION as NOMBRE_OBRA,
			bitacora.NOMBRE_SUPERVISOR,
			bitacora.FECHA_INICIO,
			bitacora.CLIMA_DIA,
			bitacora.DESCRIPCION,
			bitacora.COMPROBANTE_FOTOGRAFICO,
			bitacora.HORA_CREACION,
			proyecto.NOMBRE_PROYECTO
			from pro_proyecto_bitacora_diaria bitacora
			left join pro_cat_tipo_obra tipoObra on bitacora.ID_CAT_TIPO_OBRA=tipoObra.ID_CAT_TIPO_OBRA
			left join pro_proyecto proyecto on bitacora.ID_PROYECTO = proyecto.ID_PROYECTO 
			WHERE proyecto.ID_PROYECTO='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarBitacorasDiariaId($id){
		$sql='SELECT 
			bitacora.ID_PROYECTO_BITACORA_DIARIA,
			tipoObra.ID_CAT_TIPO_OBRA,
			bitacora.NOMBRE_SUPERVISOR,
			bitacora.FECHA_INICIO,
			bitacora.CLIMA_DIA,
			bitacora.DESCRIPCION,
			bitacora.COMPROBANTE_FOTOGRAFICO,
			bitacora.HORA_CREACION,
			proyecto.ID_PROYECTO,
			proyecto.NOMBRE_PROYECTO
			from pro_proyecto_bitacora_diaria bitacora
			inner join pro_cat_tipo_obra tipoObra on bitacora.ID_CAT_TIPO_OBRA=tipoObra.ID_CAT_TIPO_OBRA
			inner join pro_proyecto proyecto on bitacora.ID_PROYECTO = proyecto.ID_PROYECTO
			where bitacora.ID_PROYECTO_BITACORA_DIARIA ='.$id.';
			';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveBitacora($registrarBitacora)
	{
		$this->db->trans_start();
		$this->db->insert('pro_proyecto_bitacora_diaria',$registrarBitacora);
		$this->db->trans_complete();
	}

	public function updateBitacora($id,$updateBitacora)
	{
		$this->db->trans_start();
		$this->db->where('ID_PROYECTO_BITACORA_DIARIA',$id);
		$this->db->update('pro_proyecto_bitacora_diaria',$updateBitacora);
		$this->db->trans_complete();
	}


	public function deleteBitacora($id)
	{
		$this->db->trans_start();
		$this->db->where('ID_PROYECTO_BITACORA_DIARIA',$id);
		$this->db->delete('pro_proyecto_bitacora_diaria');
		$this->db->trans_complete();
	}

	public function listarProyectos(){
		$sql='SELECT
			proyectos.ID_PROYECTO, 
			proyectos.CODIGO_PROYECTO,
			proyectos.NOMBRE_PROYECTO
			FROM pro_proyecto proyectos
			WHERE proyectos.ESTADO = 1';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function obtenerNombreProyecto($id) {
			$sql='SELECT
			proyectos.ID_PROYECTO, 
			proyectos.CODIGO_PROYECTO,
			proyectos.NOMBRE_PROYECTO
			FROM pro_proyecto proyectos
			WHERE proyectos.ID_PROYECTO = '.$id;
		$query=$this->db->query($sql);
		return $query->result();	
	}

	public function listarTiposObras(){
		$sql='SELECT 
			tipoObra.ID_CAT_TIPO_OBRA,
			tipoObra.DESCRIPCION
			FROM pro_cat_tipo_obra tipoObra
			WHERE tipoObra.ESTADO = 1';
		$query=$this->db->query($sql);
		return $query->result();
	}
}
?>