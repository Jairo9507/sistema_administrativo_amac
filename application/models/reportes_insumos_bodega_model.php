<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_insumos_bodega_model extends CI_Model //el model debe llamarse igual que el archivo
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarProductos($unidad)
	{
		$sql = 'SELECT p.ID_PRODUCTO, p.NOMBRE_PRODUCTO,CONCAT("-",me.FORMA_FARMACEUTICA,"-",me.PRESENTACION,"-",me.CONCENTRACION) as MEDICAMENTO FROM inv_cat_producto p LEFT JOIN cli_cat_medicamento me on me.id_producto=p.id_producto WHERE ACTIVO = 1 AND p.id_unidad_administrativa='.$unidad;
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function listarCuentas()
	{
		$sql = 'SELECT ID_CAT_CUENTA, COD_CUENTA FROM  inv_cat_cuentas ORDER BY COD_CUENTA ASC';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarUnidades()
	{
		$sql = 'SELECT * FROM seg_cat_unidad_administrativa where ESTADO = 1';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function obtenerUnidadAdministrativa()
	{
		$sql='SELECT * FROM seg_cat_unidad_administrativa WHERE ESTADO=1';
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function obtenerCuentas()
	{
		$sql='SELECT * FROM inv_cat_cuentas order by cod_cuenta ASC';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function obtenerReporteDetalle($fechaInicio, $fechaFinal, $producto, $cuenta,$unidad)
	{
		if ($fechaInicio!=0 and $fechaFinal!=0 and $unidad==0 and $cuenta==0 and $producto==0) {
			
			$sql = 'SELECT date_format(mo.FECHA, "%d/%m/%Y") FECHA, cu.COD_CUENTA, cc.CODIGO,
					pro.NOMBRE_PRODUCTO, mo.COSTO_UNITARIO, SUM(mo.CANTIDAD) CANTIDAD,
					(mo.COSTO_UNITARIO*SUM(mo.CANTIDAD)) COSTO_TOTAL,CONCAT("-",me.FORMA_FARMACEUTICA,"-",me.PRESENTACION,"-",me.CONCENTRACION) as MEDICAMENTO
					FROM inv_movimientos mo
					LEFT JOIN inv_cat_producto pro
					ON (mo.ID_PRODUCTO = pro.ID_PRODUCTO)
					LEFT JOIN inv_cat_cuentas cu
					ON (pro.ID_CAT_CUENTA = cu.ID_CAT_CUENTA)
					LEFT JOIN inv_orden_compra oc
					ON (oc.ID_ORDEN_COMPRA = mo.ID_ORDEN_COMPRA)
					LEFT JOIN seg_cat_unidad_administrativa ua
					ON (ua.ID_UNIDAD = oc.UNIDAD_CREA)
					LEFT JOIN inv_cat_cuenta_contable cc
					ON (cc.ID_CAT_CUENTA_CONTABLE = pro.INV_CAT_CUENTA_CONTABLE)
					LEFT JOIN cli_cat_medicamento me 
					ON (me.ID_PRODUCTO = pro.ID_PRODUCTO)
					WHERE CAST(mo.FECHA AS DATE)  BETWEEN "'.$fechaInicio.'" AND "'.$fechaFinal.'" AND ID_TIPO_MOVIMIENTO =1 
					GROUP BY mo.ID_PRODUCTO, mo.COSTO_UNITARIO
					ORDER BY pro.NOMBRE_PRODUCTO ASC,mo.FECHA ASC
			';
		}
		else if ($fechaInicio!=0 and $fechaFinal!=0 and $producto!=0 and   $unidad==0 and $cuenta==0) {
				
			$sql = 'SELECT date_format(mo.FECHA, "%d/%m/%Y") FECHA, cu.COD_CUENTA, cc.CODIGO,
			pro.NOMBRE_PRODUCTO, mo.COSTO_UNITARIO, SUM(mo.CANTIDAD) CANTIDAD,
			(mo.COSTO_UNITARIO*SUM(mo.CANTIDAD)) COSTO_TOTAL,CONCAT("-",me.FORMA_FARMACEUTICA,"-",me.PRESENTACION,"-",me.CONCENTRACION) as MEDICAMENTO
			FROM inv_movimientos mo
			LEFT JOIN inv_cat_producto pro
			ON (mo.ID_PRODUCTO = pro.ID_PRODUCTO)
			LEFT JOIN inv_cat_cuentas cu
			ON (pro.ID_CAT_CUENTA = cu.ID_CAT_CUENTA)
			LEFT JOIN inv_orden_compra oc
			ON (oc.ID_ORDEN_COMPRA = mo.ID_ORDEN_COMPRA)
			LEFT JOIN seg_cat_unidad_administrativa ua
			ON (ua.ID_UNIDAD = oc.UNIDAD_CREA)
			LEFT JOIN inv_cat_cuenta_contable cc
			ON (cc.ID_CAT_CUENTA_CONTABLE = pro.INV_CAT_CUENTA_CONTABLE)
			LEFT JOIN cli_cat_medicamento me 
			ON (me.ID_PRODUCTO= pro.ID_PRODUCTO)
			WHERE CAST(mo.FECHA AS DATE)  BETWEEN "'.$fechaInicio.'" AND "'.$fechaFinal.'" AND ID_TIPO_MOVIMIENTO =1 AND mo.ID_PRODUCTO="'.$producto.'"
			GROUP BY mo.ID_ORDEN_COMPRA
			ORDER BY cu.COD_CUENTA ASC,pro.NOMBRE_PRODUCTO ASC,mo.FECHA ASC';
		 }
		else if ($fechaInicio!=0 and $fechaFinal!=0 and $unidad!=0 and $cuenta==0 and $producto==0) {
			
			$sql='SELECT date_format(mo.FECHA, "%d/%m/%Y") FECHA, cu.COD_CUENTA, cc.CODIGO,
			pro.NOMBRE_PRODUCTO, mo.COSTO_UNITARIO, SUM(mo.CANTIDAD) CANTIDAD,
			(mo.COSTO_UNITARIO*SUM(mo.CANTIDAD)) COSTO_TOTAL, ua.DESCRIPCION, ua.CODIGO_UNIDAD,CONCAT("-",me.FORMA_FARMACEUTICA,"-",me.PRESENTACION,"-",me.CONCENTRACION) as MEDICAMENTO
			FROM inv_movimientos mo
			LEFT JOIN inv_cat_producto pro
			ON (mo.ID_PRODUCTO = pro.ID_PRODUCTO)
			LEFT JOIN inv_cat_cuentas cu
			ON (pro.ID_CAT_CUENTA = cu.ID_CAT_CUENTA)
			LEFT JOIN inv_orden_compra oc
			ON (oc.ID_ORDEN_COMPRA = mo.ID_ORDEN_COMPRA)
			LEFT JOIN seg_cat_unidad_administrativa ua
			ON (ua.ID_UNIDAD = oc.UNIDAD_CREA)
			LEFT JOIN inv_cat_cuenta_contable cc
			ON (cc.ID_CAT_CUENTA_CONTABLE = pro.INV_CAT_CUENTA_CONTABLE)
			LEFT JOIN cli_cat_medicamento me 
			ON (me.ID_PRODUCTO=pro.ID_PRODUCTO)
			WHERE CAST(mo.FECHA AS DATE)  BETWEEN "'.$fechaInicio.'" AND "'.$fechaFinal.'" AND ID_TIPO_MOVIMIENTO =1  AND ua.ID_UNIDAD='.$unidad.'
			GROUP BY mo.ID_PRODUCTO, mo.COSTO_UNITARIO
			ORDER BY pro.NOMBRE_PRODUCTO ASC';
		}
		 else if ($fechaInicio!=0 and $fechaFinal!=0 and $unidad!=0 and $cuenta!=0 and $producto==0) {
			$sql = 'SELECT date_format(mo.FECHA, "%d/%m/%Y") FECHA, cu.COD_CUENTA, cc.CODIGO,
			pro.NOMBRE_PRODUCTO, mo.COSTO_UNITARIO, SUM(mo.CANTIDAD) CANTIDAD,
			(mo.COSTO_UNITARIO*SUM(mo.CANTIDAD)) COSTO_TOTAL, cu.NOMBRE_CUENTA,CONCAT("-",me.FORMA_FARMACEUTICA,"-",me.PRESENTACION,"-",me.CONCENTRACION) as MEDICAMENTO
			FROM inv_movimientos mo
			LEFT JOIN inv_cat_producto pro
			ON (mo.ID_PRODUCTO = pro.ID_PRODUCTO)
			LEFT JOIN inv_cat_cuentas cu
			ON (pro.ID_CAT_CUENTA = cu.ID_CAT_CUENTA)
			LEFT JOIN inv_orden_compra oc
			ON (oc.ID_ORDEN_COMPRA = mo.ID_ORDEN_COMPRA)
			LEFT JOIN seg_cat_unidad_administrativa ua
			ON (ua.ID_UNIDAD = oc.UNIDAD_CREA)
			LEFT JOIN inv_cat_cuenta_contable cc
			ON (cc.ID_CAT_CUENTA_CONTABLE = pro.INV_CAT_CUENTA_CONTABLE)
			LEFT JOIN cli_cat_medicamento me 
			ON (me.ID_PRODUCTO=pro.ID_PRODUCTO)
			WHERE CAST(mo.FECHA AS DATE)  BETWEEN "'.$fechaInicio.'" AND "'.$fechaFinal.'" AND ID_TIPO_MOVIMIENTO =1  AND cu.ID_CAT_CUENTA='.$cuenta.'
			and pro.id_unidad_administrativa='.$unidad.'
			GROUP BY mo.ID_PRODUCTO, mo.COSTO_UNITARIO
			ORDER BY pro.NOMBRE_PRODUCTO ASC, mo.FECHA ASC';
			//var_dump($sql);exit;
		}

		$query = $this->db->query($sql);
		return $query->result();
	}

	public function obtenerReporteConsolidado($fechaInicio, $fechaFinal, $producto, $cuenta, $unidad)
	{
		if ($fechaInicio!=0 and $fechaFinal!=0 and $producto==0 and $cuenta==0 and $unidad==0) {
			$sql='SELECT date_format(mo.FECHA, "%d/%m/%Y") FECHA, cu.COD_CUENTA, cc.CODIGO,
					pro.NOMBRE_PRODUCTO, mo.COSTO_UNITARIO, SUM(mo.CANTIDAD) CANTIDAD,
					(mo.COSTO_UNITARIO*SUM(mo.CANTIDAD)) COSTO_TOTAL
					FROM inv_movimientos mo
					LEFT JOIN inv_cat_producto pro
					ON (mo.ID_PRODUCTO = pro.ID_PRODUCTO)
					LEFT JOIN inv_cat_cuentas cu
					ON (pro.ID_CAT_CUENTA = cu.ID_CAT_CUENTA)
					LEFT JOIN inv_orden_compra oc
					ON (oc.ID_ORDEN_COMPRA = mo.ID_ORDEN_COMPRA)
					LEFT JOIN seg_cat_unidad_administrativa ua
					ON (ua.ID_UNIDAD = oc.UNIDAD_CREA)
					LEFT JOIN inv_cat_cuenta_contable cc
					ON (cc.ID_CAT_CUENTA_CONTABLE = pro.INV_CAT_CUENTA_CONTABLE)
					WHERE CAST(mo.FECHA AS DATE)  BETWEEN "'.$fechaInicio.'" AND "'.$fechaFinal.'" AND ID_TIPO_MOVIMIENTO =1 
					GROUP BY mo.ID_PRODUCTO, mo.COSTO_UNITARIO
					ORDER BY pro.NOMBRE_PRODUCTO ASC,mo.FECHA ASC';
			}
			else if($fechaInicio!=0 and $fechaFinal!=0 and $producto!=0 and $cuenta==0 and $unidad==0){

			$sql='SELECT SUM(COSTO_TOTAL) TOTAL FROM (SELECT date_format(mo.FECHA, "%d/%m/%Y") FECHA, cu.COD_CUENTA, cc.CODIGO,
			pro.NOMBRE_PRODUCTO, mo.COSTO_UNITARIO, SUM(mo.CANTIDAD) CANTIDAD,
			(mo.COSTO_UNITARIO*SUM(mo.CANTIDAD)) COSTO_TOTAL
			FROM inv_movimientos mo
			LEFT JOIN inv_cat_producto pro
			ON (mo.ID_PRODUCTO = pro.ID_PRODUCTO)
			LEFT JOIN inv_cat_cuentas cu
			ON (pro.ID_CAT_CUENTA = cu.ID_CAT_CUENTA)
			LEFT JOIN inv_orden_compra oc
			ON (oc.ID_ORDEN_COMPRA = mo.ID_ORDEN_COMPRA)
			LEFT JOIN seg_cat_unidad_administrativa ua
			ON (ua.ID_UNIDAD = oc.UNIDAD_CREA)
			LEFT JOIN inv_cat_cuenta_contable cc
			ON (cc.ID_CAT_CUENTA_CONTABLE = pro.INV_CAT_CUENTA_CONTABLE)
			WHERE CAST(mo.FECHA AS DATE)  BETWEEN "'.$fechaInicio.'" AND "'.$fechaFinal.'" AND ID_TIPO_MOVIMIENTO =1 
			GROUP BY mo.ID_PRODUCTO, mo.COSTO_UNITARIO
			ORDER BY pro.NOMBRE_PRODUCTO ASC, mo.FECHA ASC)consolidado';
		}
		elseif ($fechaInicio!=0 and $fechaFinal!=0 and $producto==0 and $cuenta!=0 and $unidad==0) {
			$sql='SELECT COD_CUENTA,NOMBRE_CUENTA, SUM(COSTO_TOTAL) VALOR,CODIGO FROM (SELECT date_format(mo.FECHA, "%d/%m/%Y") FECHA, cu.COD_CUENTA, cc.CODIGO, pro.NOMBRE_PRODUCTO, mo.COSTO_UNITARIO, SUM(mo.CANTIDAD) CANTIDAD,
			(mo.COSTO_UNITARIO*SUM(mo.CANTIDAD)) COSTO_TOTAL, cu.NOMBRE_CUENTA
			FROM inv_movimientos mo
			LEFT JOIN inv_cat_producto pro
			ON (mo.ID_PRODUCTO = pro.ID_PRODUCTO)
			LEFT JOIN inv_cat_cuentas cu
			ON (pro.ID_CAT_CUENTA = cu.ID_CAT_CUENTA)
			LEFT JOIN inv_orden_compra oc
			ON (oc.ID_ORDEN_COMPRA = mo.ID_ORDEN_COMPRA)
			LEFT JOIN seg_cat_unidad_administrativa ua
			ON (ua.ID_UNIDAD = oc.UNIDAD_CREA)
			LEFT JOIN inv_cat_cuenta_contable cc
			ON (cc.ID_CAT_CUENTA_CONTABLE = pro.INV_CAT_CUENTA_CONTABLE)
			WHERE CAST(mo.FECHA AS DATE)  BETWEEN "'.$fechaInicio.'" AND "'.$fechaFinal.'" AND ID_TIPO_MOVIMIENTO =1 AND pro.ID_UNIDAD_ADMINISTRATIVA='.$this->ion_auth->get_unidad_empleado().'
			GROUP BY  mo.ID_PRODUCTO, mo.COSTO_UNITARIO) consolidado
			GROUP BY COD_CUENTA';
		}
		elseif ($fechaInicio!=0 and $fechaFinal!=0 and $producto==0 and $cuenta==0 and $unidad!=0) {
			$sql='SELECT CODIGO_UNIDAD, DESCRIPCION, SUM(COSTO_TOTAL) VALOR,CODIGO FROM (SELECT date_format(mo.FECHA, "%d/%m/%Y") FECHA, cu.COD_CUENTA, cc.CODIGO,
			pro.NOMBRE_PRODUCTO, mo.COSTO_UNITARIO, SUM(mo.CANTIDAD) CANTIDAD, ua.CODIGO_UNIDAD,
			(mo.COSTO_UNITARIO*SUM(mo.CANTIDAD)) COSTO_TOTAL, ua.DESCRIPCION, ua.ID_UNIDAD
			FROM inv_movimientos mo
			LEFT JOIN inv_cat_producto pro
			ON (mo.ID_PRODUCTO = pro.ID_PRODUCTO)
			LEFT JOIN inv_cat_cuentas cu
			ON (pro.ID_CAT_CUENTA = cu.ID_CAT_CUENTA)
			LEFT JOIN inv_orden_compra oc
			ON (oc.ID_ORDEN_COMPRA = mo.ID_ORDEN_COMPRA)
			LEFT JOIN seg_cat_unidad_administrativa ua
			ON (ua.ID_UNIDAD = oc.UNIDAD_CREA)
			LEFT JOIN inv_cat_cuenta_contable cc
			ON (cc.ID_CAT_CUENTA_CONTABLE = pro.INV_CAT_CUENTA_CONTABLE)
			WHERE CAST(mo.FECHA AS DATE)  BETWEEN "'.$fechaInicio.'" AND "'.$fechaFinal.'" AND ID_TIPO_MOVIMIENTO =1 
			GROUP BY mo.ID_PRODUCTO, mo.COSTO_UNITARIO
			ORDER BY pro.NOMBRE_PRODUCTO ASC,mo.FECHA ASC) consolidado
			GROUP BY ID_UNIDAD ';
		}
		$query = $this->db->query($sql);
		return $query->result();
	}

		public function nombreJefatura($id)
	{
		$sql="SELECT * from seg_usuario us inner join  seg_empleado em on em.ID_EMPLEADO=us.ID_EMPLEADO inner join seg_usuarios_perfiles up on up.id_usuario=us.id_usuario inner join seg_perfil p on p.ID_PERFIL_USUARIO=up.ID_PERFIL_USUARIO  WHERE (p.ID_PERFIL_USUARIO=2 OR p.ID_PERFIL_USUARIO=3) and em.ID_UNIDAD=".$id;
		$query=$this->db->query($sql);
		$empleado='';
		foreach ($query->result_array() as $q) {
			$empleado=$q['PRIMER_NOMBRE']." ".$q['PRIMER_APELLIDO']." ";
		}
		return $empleado;	
	}

	public function obtenerBodega($id)
	{
		$sql = 'SELECT 	CONCAT(em.PRIMER_NOMBRE, "  ", em.PRIMER_APELLIDO) NOMBRE, ua.DESCRIPCION 	 FROM seg_empleado em
				LEFT JOIN seg_cat_unidad_administrativa ua 
				ON (em.ID_UNIDAD = ua.ID_UNIDAD)
				LEFT JOIN seg_usuario usu
				ON (usu.ID_EMPLEADO = em.ID_EMPLEADO)
				WHERE usu.ID_USUARIO='.$id;
				$query = $this->db->query($sql);
				return $query->result();
	}
}