<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Usuario_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarUsuarios(){

		$sql = 'SELECT u.ID_USUARIO, u.ID_EMPLEADO CODIGO, concat(e.PRIMER_NOMBRE, " ", e.PRIMER_APELLIDO) NOMBRE, u.USUARIO,
					u.CORREO_ELECTRONICO CORREO, p.NOMBRE PERFIL, u.FECHA_CREACION
					 FROM seg_usuario u
					LEFT JOIN seg_usuarios_perfiles up on (u.ID_USUARIO = up.ID_USUARIO)
					LEFT JOIN seg_perfil p ON (up.ID_PERFIL_USUARIO = p.ID_PERFIL_USUARIO)
					LEFT JOIN seg_empleado e ON (u.ID_EMPLEADO = e.ID_EMPLEADO) WHERE u.ESTADO_USUARIO = 1';

		$query = $this->db->query($sql);
		
		return $query->result();
	}

	public function listarEmpleados()
	{
		$sql = 'select ID_EMPLEADO, CODIGO_EMPLEADO, concat(TRIM( PRIMER_NOMBRE), " ", TRIM(PRIMER_APELLIDO)) NOMBRE, ESTADO from seg_empleado WHERE ESTADO = 1 ORDER BY PRIMER_NOMBRE ASC';
				$query = $this->db->query($sql);
				return $query->result();
	}
	public function obtenerEmpleadoId($id)
	{
		$sql = 'select ID_EMPLEADO, concat(TRIM( PRIMER_NOMBRE), " ", TRIM(PRIMER_APELLIDO)) NOMBRE
			from seg_empleado WHERE ID_EMPLEADO ='.$id;
	}

	public function guardar($registrarUsuario){
		$this->db->trans_start();
		$this->db->insert("seg_usuario",$registrarUsuario);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;
	}

	public function guardarPerfil($registrarPerfil)
	{
		$this->db->trans_start();
		$this->db->insert("seg_usuarios_perfiles",$registrarPerfil);
		$this->db->trans_complete();
	}

	public function updatePerfil($value='')
	{
		# code...
	}

	public function listarPerfil(){

		$sql='SELECT ID_PERFIL_USUARIO as ID_PERFIL, DESCRIPCION,ESTADO, NOMBRE FROM seg_perfil';
		$query = $this->db->query($sql);
		return $query->result();
	}


	public function crearUsuario($idEmpleado, $usuario, $contrasena, $email){
		
		$sql = 'INSERT INTO seg_usuario (IP_ADDRESS, USUARIO, CONTRASENA, CORREO_ELECTRONICO, FECHA_CREACION, ESTADO_USUARIO,
		ID_EMPLEADO) select "127.0.0.0", "'.$usuario.'", MD5("'.$contrasena.'"), "'.$email.'", sysdate(), 1, ID_EMPLEADO
		from 10319.seg_empleado WHERE ID_EMPLEADO = '.$idEmpleado.'';

		$query = $this->db->query($sql);
		return $query->result();

	}

	public function buscarUsuario($id)
	{
		$sql='SELECT * FROM seg_usuario where ID_USUARIO='.$id;
		$query=$this->db->query($sql);
		$usuario=array();
		foreach ($query->result_array() as $u) {
			$usuario=$u['USUARIO'];
		}
		return $usuario;
	}

	public function listarNombreEmpleado($id)	
	{
		$sql="SELECT * from seg_usuario us inner join  seg_empleado em on em.ID_EMPLEADO=us.ID_EMPLEADO WHERE us.ID_USUARIO=".$id;
		$query=$this->db->query($sql);
		$empleado='';
		foreach ($query->result_array() as $q) {
			$empleado=$q['PRIMER_NOMBRE'].' '.$q['PRIMER_APELLIDO'];
		}
		return $empleado;
	}

	public function listarUsuarioId($id)
	{
		$sql = 'SELECT u.ID_USUARIO, e.ID_EMPLEADO CODIGO, CASE WHEN CONCAT(RTRIM(e.PRIMER_NOMBRE)," ", RTRIM(e.PRIMER_APELLIDO)) IS NULL THEN "Administrador"
				ELSE CONCAT(e.PRIMER_NOMBRE," ", e.PRIMER_APELLIDO)
				end  NOMBRE, u.USUARIO, u.CONTRASENA, u.CORREO_ELECTRONICO CORREO, u.FECHA_CREACION, 
				p.DESCRIPCION PERFIL, p.ID_PERFIL_USUARIO, up.ID_USUARIO_PEFIL 
				FROM seg_usuario u
				LEFT JOIN seg_empleado e on(u.ID_EMPLEADO = e.ID_EMPLEADO)
				LEFT JOIN seg_usuarios_perfiles up on (u.ID_USUARIO = up.ID_USUARIO)
				LEFT JOIN seg_perfil p ON (up.ID_PERFIL_USUARIO = p.ID_PERFIL_USUARIO)
				 WHERE u.ID_USUARIO ='.$id;
			$query = $this->db->query($sql);
			return $query->result();
	}

	public function updateUsuario($id, $updateUsuario)
	{
		$this->db->trans_start();
		$this->db->where("ID_USUARIO",$id);
		$this->db->update("seg_usuario",$updateUsuario);
		$this->db->trans_complete();
	}

	public function updateUsuarioPerfil($id, $updateUsuarioPerfil)
	{
		$this->db->trans_start();
		$this->db->where("ID_USUARIO_PEFIL",$id);
		$this->db->update("seg_usuarios_perfiles",$updateUsuarioPerfil);
		$this->db->trans_complete();
	}

	public function deleteUsuario($id, $deleteUsuario)
	{
		$this->db->trans_start();
		$this->db->where("ID_USUARIO",$id);
		$this->db->update("seg_usuario",$deleteUsuario);
		$this->db->trans_complete();
	}



}