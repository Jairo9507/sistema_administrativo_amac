<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 
 */
class cli_referencia_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarEspecialidad()
	{
		$sql='SELECT * from cli_cat_especialidad';
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function listarEspecialidadId($id)
	{
		$sql='SELECT * from cli_cat_especialidad WHERE ID_CAT_ESPECIALIDAD='.$id;
		$query=$this->db->query($sql);
		$nombre='';
		foreach ($query->result_array() as $q) {
			$nombre=$q['DESCRIPCION'];
		}
		return $nombre;
	}
	public function listarMedicoEspecialidad($especialidad)
	{
		$sql="SELECT me.ID_MEDICO,CONCAT(em.PRIMER_NOMBRE,' ',em.SEGUNDO_NOMBRE,' ',em.PRIMER_APELLIDO,' ',em.SEGUNDO_APELLIDO) as nombre_completo from cli_medico me inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO WHERE em.ESTADO=1 and ID_CAT_ESPECIALIDAD=".$especialidad;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarMedico()
	{
		$sql="SELECT me.ID_MEDICO,CONCAT(em.PRIMER_NOMBRE,' ',em.SEGUNDO_NOMBRE,' ',em.PRIMER_APELLIDO,' ',em.SEGUNDO_APELLIDO) as nombre_completo from cli_medico me inner join seg_empleado em on em.ID_EMPLEADO=me.ID_EMPLEADO WHERE em.ESTADO=1 ";
		$query=$this->db->query($sql);
		return $query->result();
	}	

	public function listarPacientes()
	{
		$sql='SELECT ID_PACIENTE,CONCAT(PRIMER_NOMBRE," ",SEGUNDO_NOMBRE," ",PRIMER_APELLIDO," ",SEGUNDO_APELLIDO," ",TERCER_APELLIDO) as nombre_completo FROM cli_paciente WHERE ESTADO=1';
		$query=$this->db->query($sql);
		return $query->result();
	}	

	public function listarReferencias()
	{
		$sql="SELECT rf.ID_REFERENCIA, CONCAT(pa.PRIMER_NOMBRE,' ',pa.SEGUNDO_NOMBRE,' ',pa.PRIMER_APELLIDO,' ',pa.SEGUNDO_APELLIDO) as nombre_paciente,
			CONCAT(emt.PRIMER_NOMBRE,' ',emt.SEGUNDO_NOMBRE,' ',emt.PRIMER_APELLIDO,' ',emt.SEGUNDO_APELLIDO) as nombre_doctor_refiere,
			IFNULL(CONCAT(emr.PRIMER_NOMBRE,' ',emr.SEGUNDO_NOMBRE,' ',emr.PRIMER_APELLIDO,' ',emr.SEGUNDO_APELLIDO),rf.nombre_medico_recibe) as nombre_doctor_recibe,
			rf.fecha_referencia as fecha,rf.MOTIVO_REFERENCIA as referencia
			from cli_referencia rf 
			inner join cli_paciente pa on pa.id_paciente=rf.id_paciente
			inner join cli_medico met on met.id_medico=rf.id_medico_refiere
			inner join seg_empleado emt on emt.id_empleado=met.id_empleado
			left join cli_medico mer on mer.id_medico=rf.id_medico_recibe
			left join seg_empleado emr on emr.id_empleado=mer.id_empleado 
			/*WHERE rf.ESTADO=1 and DATE_FORMAT(rf.FECHA_REFERENCIA,'%Y-%m-%d') BETWEEN CAST(DATE_FORMAT(NOW() ,'%Y-%m-01') as DATE) and DATE_FORMAT(LAST_DAY(now()),'%Y-%m-%d') ORDER BY rf.fecha_referencia DESC */";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarReferenciaId($idreferencia){
		$sql=" SELECT rf.ID_REFERENCIA, rf.ID_PACIENTE,
			rf.ID_MEDICO_RECIBE,rf.nombre_medico_recibe as nombre_doctor_recibe,
			rf.fecha_referencia as fecha,rf.MOTIVO_REFERENCIA as referencia, mer.ID_CAT_ESPECIALIDAD,rf.CORREO_MEDICO_RECIBE,rf.ESPECIALIDAD,rf.NOMBRE_CLINICA
			from cli_referencia rf 
			inner join cli_paciente pa on pa.id_paciente=rf.id_paciente
			inner join cli_medico met on met.id_medico=rf.id_medico_refiere
			inner join seg_empleado emt on emt.id_empleado=met.id_empleado
			left join cli_medico mer on mer.id_medico=rf.id_medico_recibe
			left join seg_empleado emr on emr.id_empleado=mer.id_empleado WHERE rf.ESTADO=1 and rf.ID_REFERENCIA=".$idreferencia;
					$query = $this->db->query($sql);
					return $query->result();	
		}

	public function saveReferencia($registrarReferencia)
	{
		$this->db->trans_start();
		$this->db->insert("cli_referencia",$registrarReferencia);
		$this->db->trans_complete();
	}

	public function updateReferencia($updateReferencia,$id)
	{
		$this->db->trans_start();
		$this->db->where("ID_REFERENCIA",$id);
		$this->db->update("cli_referencia",$updateReferencia);
		$this->db->trans_complete();
	}

	public function deleteReferencia($id,$deleteReferencia)
	{
		$this->db->trans_start();
		$this->db->where("ID_REFERENCIA",$id);
		$this->db->update("cli_referencia",$deleteReferencia);
		$this->db->trans_complete();		
	}
	public function referenciaDocumento($id)
	{
		$sql="SELECT co.ID_CONSULTA,rf.FECHA_REFERENCIA,ci.ID_CITA,
			CONCAT(pa.PRIMER_NOMBRE,' ',pa.SEGUNDO_APELLIDO,' ',pa.PRIMER_APELLIDO,' ',pa.SEGUNDO_APELLIDO) as NOMBREPACIENTE,
			pa.SEXO,ec.CODIGO_EXPEDIENTE as EXPEDIENTE,ROUND(DATEDIFF(now(),pa.FECHA_NACIMIENTO)/365,0) as EDAD,
			(esr.DESCRIPCION) as EMEDICOREFIERE,
			CONCAT(em.PRIMER_NOMBRE,' ',em.SEGUNDO_NOMBRE,' ',em.PRIMER_APELLIDO,' ',em.SEGUNDO_APELLIDO) as NOMBREDOCTOR,
			IFNULL(CONCAT(emb.PRIMER_NOMBRE,' ',emb.SEGUNDO_NOMBRE,' ',emb.PRIMER_APELLIDO,' ',emb.SEGUNDO_APELLIDO),rf.NOMBRE_MEDICO_RECIBE) as MEDICOREFERIDO,
			IFNULL(co.OBSERVACION,'') as IMPRESION, IFNULL((SELECT DESCRIPCION from cli_cat_especialidad es inner join cli_medico me on me.ID_CAT_ESPECIALIDAD=es.ID_CAT_ESPECIALIDAD 
			WHERE me.ID_MEDICO=rf.ID_MEDICO_RECIBE),rf.ESPECIALIDAD) as EMREFERIDO,
			rf.MOTIVO_REFERENCIA, ex.CABEZA,ex.CUELLO,ex.TORAX,ex.ABDOMEN,ex.PULSOS,ex.FACIES,ex.PIE,
			ex.GENITALES,ex.OTROS,ex.TEMPERATURA,ex.FRECUENCIA_CARDIACA,ex.FRECUENCIA_RESPIRATORIA,
			ex.PRESION_ARTERIAL,ex.GLUCOSA_CAPILAR,ex.IMC,ex.MIEMBROS_INFERIORES,IFNULL(rf.NOMBRE_CLINICA,'Clinica de Antiguo Cuscatlan') as NOMBRE_CLINICA
			FROM cli_referencia rf 
			inner join cli_medico mr on mr.ID_MEDICO=rf.ID_MEDICO_REFIERE
			inner join seg_empleado em on em.ID_EMPLEADO=mr.ID_EMPLEADO
			inner join cli_cat_especialidad esr on esr.ID_CAT_ESPECIALIDAD=mr.ID_CAT_ESPECIALIDAD
			inner join cli_paciente pa on pa.ID_PACIENTE=rf.ID_PACIENTE
			inner join cli_expediente_clinico ec on ec.ID_PACIENTE=pa.ID_PACIENTE
			inner join cli_cita ci on ci.ID_PACIENTE=pa.ID_PACIENTE 
			inner join cli_consulta co on co.ID_CITA=ci.ID_CITA
			inner join cli_exploracion ex on ex.ID_CONSULTA=co.ID_CONSULTA
			left join cli_medico mrb on mrb.ID_MEDICO=rf.ID_MEDICO_RECIBE
			left join seg_empleado emb on emb.ID_EMPLEADO=mrb.ID_EMPLEADO
			WHERE rf.ID_REFERENCIA=".$id." ORDER BY co.FECHA_CONSULTA DESC LIMIT 1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function listaExamenesCita($id)
	{
		$sql="SELECT DESCRIPCION FROM cli_cat_estudio_clinico ec inner join cli_detalle_estudios ds on ds.ID_CAT_ESTUDIO_CLINICO=ec.ID_CAT_ESTUDIO_CLINICO WHERE ds.ID_CITA=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}
}