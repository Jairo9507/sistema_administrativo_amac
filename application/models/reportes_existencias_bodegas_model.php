<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class reportes_existencias_bodegas_model extends CI_Model //el model debe llamarse igual que el archivo
{
	
	function __construct()
	{
		parent::__construct();
	}


	public function listarProductos($unidad)
	{
		$sql = 'SELECT p.ID_PRODUCTO, p.NOMBRE_PRODUCTO,CONCAT("-",me.FORMA_FARMACEUTICA,"-",me.PRESENTACION,"-",me.CONCENTRACION) as MEDICAMENTO FROM inv_cat_producto p LEFT JOIN cli_cat_medicamento me ON p.ID_PRODUCTO=me.ID_PRODUCTO  WHERE ACTIVO = 1 and id_unidad_administrativa='.$unidad;
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function listarCuentas()
	{
		$sql = 'SELECT ID_CAT_CUENTA, COD_CUENTA FROM  inv_cat_cuentas ORDER BY COD_CUENTA ASC';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function listarUnidades()
	{
		$sql = 'SELECT * FROM seg_cat_unidad_administrativa where ESTADO = 1';
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function obtenerBodega($id)
	{
		$sql = 'SELECT 	CONCAT(em.PRIMER_NOMBRE, "  ", em.PRIMER_APELLIDO) NOMBRE, ua.DESCRIPCION, 
		        ua.ID_UNIDAD 	
		        FROM seg_empleado em
				LEFT JOIN seg_cat_unidad_administrativa ua 
				ON (em.ID_UNIDAD = ua.ID_UNIDAD)
				LEFT JOIN seg_usuario usu
				ON (usu.ID_EMPLEADO = em.ID_EMPLEADO)
				WHERE usu.ID_USUARIO='.$id;
				$query = $this->db->query($sql);
				return $query->result();
	}

	public function nombreJefatura($id)
	{
		$sql="SELECT * from seg_usuario us inner join  seg_empleado em on em.ID_EMPLEADO=us.ID_EMPLEADO inner join seg_usuarios_perfiles up on up.id_usuario=us.id_usuario inner join seg_perfil p on p.ID_PERFIL_USUARIO=up.ID_PERFIL_USUARIO  WHERE (p.ID_PERFIL_USUARIO=2 OR p.ID_PERFIL_USUARIO=3) and em.ID_UNIDAD=".$id;
		$query=$this->db->query($sql);
		$empleado='';
		foreach ($query->result_array() as $q) {
			$empleado=$q['PRIMER_NOMBRE']." ".$q['PRIMER_APELLIDO']." ";
		}
		return $empleado;	
	}

	public function obtenerReporteDetalle($producto, $unidad, $cuenta,$fechaInicio,$fechaFinal)
	{
		if ($producto!=0) {
			$historico=(!empty($fechaInicio) && $fechaInicio != "1" ) && (!empty($fechaFinal) && $fechaFinal !="1" ) ? 'AND (DATE_FORMAT(his.FECHA_INICIO_VIGENCIA,"%Y-%m-%d")<= DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") OR DATE_FORMAT(his.FECHA_INICIO_VIGENCIA,"%Y-%m-%d")> DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") )
			AND ( DATE_FORMAT(his.FECHA_FIN_VIGENCIA,"%Y-%m-%d")<= DATE_FORMAT("'.$fechaFinal.'","%Y-%m-%d") OR his.FECHA_FIN_VIGENCIA IS NULL)':'';
			$existencias= (!empty($fechaInicio) && $fechaInicio != "1" ) && (!empty($fechaFinal) && $fechaFinal !="1" ) ? 'IFNULL(SUM(his.CANTIDAD_INVENTARIO)+(SELECT SUM(mo.CANTIDAD) as SUMA from inv_movimientos mo 
				WHERE DATE_FORMAT(mo.FECHA, "%Y-%m-%d") 
				BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND 
				DATE_FORMAT(now(), "%Y-%m-%d") 
				and mo.ID_TIPO_MOVIMIENTO=2 and mo.id_producto=pro.id_producto and mo.ID_HISTORICO_PERTENECE=his.ID_HISTORICO_INVENTARIO and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4) )-IFNULL((SELECT SUM(mo.CANTIDAD) as SUMA 
					from inv_movimientos mo WHERE DATE_FORMAT(mo.FECHA, "%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") 
					AND DATE_FORMAT("'.$fechaFinal.'", "%Y-%m-%d")  and mo.ID_TIPO_MOVIMIENTO=2 
					and mo.id_producto='.$producto.'  and mo.ID_HISTORICO_PERTENECE=his.ID_HISTORICO_INVENTARIO and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4)),0),SUM(his.CANTIDAD_INVENTARIO))': 'his.CANTIDAD_INVENTARIO';		
			$sql = 'SELECT cp.COD_CUENTA, cc.CODIGO, pro.NOMBRE_PRODUCTO, '.$existencias.' CANTIDAD, his.PRECIO, (his.CANTIDAD_INVENTARIO*his.PRECIO) COSTO_TOTAL, his.ID_PRODUCTO, cp.COD_CUENTA, cp.nombre_cuenta,CONCAT("-",me.FORMA_FARMACEUTICA,"-",me.PRESENTACION,"-",me.CONCENTRACION) as MEDICAMENTO
			FROM cat_historico_inventario his
			LEFT JOIN inv_cat_producto pro 
			ON (his.ID_PRODUCTO = pro.ID_PRODUCTO)
			LEFT JOIN inv_cat_cuenta_contable cc 
			ON (cc.ID_CAT_CUENTA_CONTABLE = pro.INV_CAT_CUENTA_CONTABLE)
			LEFT JOIN inv_cat_cuentas cp
			ON (cp.ID_CAT_CUENTA = pro.ID_CAT_CUENTA)
			LEFT JOIN seg_cat_unidad_administrativa ua
			ON (ua.ID_UNIDAD = pro.ID_UNIDAD_ADMINISTRATIVA)
			LEFT JOIN cli_cat_medicamento me 
			ON (me.ID_PRODUCTO=pro.ID_PRODUCTO) 
			WHERE his.ID_PRODUCTO='.$producto.'  AND ua.ID_UNIDAD = '.$unidad.' 
			'.$historico .'
			GROUP BY his.ID_HISTORICO_INVENTARIO,cp.COD_CUENTA
			HAVING CANTIDAD<>0
			ORDER BY pro.NOMBRE_PRODUCTO ASC,FECHA_INICIO_VIGENCIA ASC';
		}elseif ($producto==0) {
			$historico=(!empty($fechaInicio) && $fechaInicio != "1" ) && (!empty($fechaFinal) && $fechaFinal !="1" ) ?  'AND (DATE_FORMAT(his.FECHA_INICIO_VIGENCIA,"%Y-%m-%d")<= DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") OR DATE_FORMAT(his.FECHA_INICIO_VIGENCIA,"%Y-%m-%d")> DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") )
			AND ( DATE_FORMAT(his.FECHA_FIN_VIGENCIA,"%Y-%m-%d")<= DATE_FORMAT("'.$fechaFinal.'","%Y-%m-%d") OR his.FECHA_FIN_VIGENCIA IS NULL)':'';
			$existencias= (!empty($fechaInicio) && $fechaInicio != "1" ) && (!empty($fechaFinal) && $fechaFinal !="1" ) ? 'IFNULL(SUM(his.CANTIDAD_INVENTARIO)+(SELECT SUM(mo.CANTIDAD) as SUMA from inv_movimientos mo 
				WHERE DATE_FORMAT(mo.FECHA, "%Y-%m-%d") 
				BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") AND 
				DATE_FORMAT(now(), "%Y-%m-%d") 
				and mo.ID_TIPO_MOVIMIENTO=2 and mo.id_producto=pro.id_producto and mo.ID_HISTORICO_PERTENECE=his.ID_HISTORICO_INVENTARIO and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4) )-IFNULL((SELECT SUM(mo.CANTIDAD) as SUMA 
					from inv_movimientos mo WHERE DATE_FORMAT(mo.FECHA, "%Y-%m-%d") BETWEEN DATE_FORMAT("'.$fechaInicio.'","%Y-%m-%d") 
					AND DATE_FORMAT("'.$fechaFinal.'", "%Y-%m-%d")  and mo.ID_TIPO_MOVIMIENTO=2
					and mo.id_producto=pro.id_producto and mo.ID_HISTORICO_PERTENECE=his.ID_HISTORICO_INVENTARIO and mo.COSTO_UNITARIO=ROUND(his.PRECIO,4)),0),SUM(his.CANTIDAD_INVENTARIO))': 'SUM(his.CANTIDAD_INVENTARIO)';			
			$sql = 'SELECT cp.COD_CUENTA, cc.CODIGO, pro.NOMBRE_PRODUCTO, '.$existencias.' CANTIDAD, his.PRECIO, (his.CANTIDAD_INVENTARIO*his.PRECIO) COSTO_TOTAL, his.ID_PRODUCTO, cp.COD_CUENTA, cp.NOMBRE_CUENTA,ua.ID_UNIDAD,CONCAT("-",me.FORMA_FARMACEUTICA,"-",me.PRESENTACION,"-",me.CONCENTRACION) as MEDICAMENTO
				FROM cat_historico_inventario his
				LEFT JOIN inv_cat_producto pro 
				ON (his.ID_PRODUCTO = pro.ID_PRODUCTO)
				LEFT JOIN inv_cat_cuenta_contable cc 
				ON (cc.ID_CAT_CUENTA_CONTABLE = pro.INV_CAT_CUENTA_CONTABLE)
				LEFT JOIN inv_cat_cuentas cp
				ON (cp.ID_CAT_CUENTA = pro.ID_CAT_CUENTA)
				LEFT JOIN seg_cat_unidad_administrativa ua
				ON (ua.ID_UNIDAD = pro.ID_UNIDAD_ADMINISTRATIVA) 
				LEFT JOIN cli_cat_medicamento me 
				ON (me.ID_PRODUCTO=pro.ID_PRODUCTO)
				WHERE cp.ID_CAT_CUENTA ='.$cuenta.' AND ua.ID_UNIDAD = '.$unidad.' 
				'.$historico.'				
				GROUP BY his.ID_HISTORICO_INVENTARIO,cp.COD_CUENTA
				HAVING CANTIDAD<>0
				ORDER BY   pro.NOMBRE_PRODUCTO ASC,cp.COD_CUENTA ASC,FECHA_INICIO_VIGENCIA ASC';

		}

		$query = $this->db->query($sql);
		return $query->result();
		
	}

}