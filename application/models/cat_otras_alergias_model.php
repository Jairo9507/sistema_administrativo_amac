<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_otras_alergias_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarAlergias()
	{
		$sql='SELECT ID_CAT_OTRA_ALERGIA,DESCRIPCION,ESTADO FROM cli_cat_otras_alergias WHERE ESTADO=1';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function saveAlergia($registrarAlergia)
	{
		$this->db->trans_start();
		$this->db->insert("cli_cat_otras_alergias",$registrarAlergia);
		$this->db->trans_complete();
	}

	public function updateAlergia($id,$updateAlergia)
	{
		$this->db->trans_start();
		$this->db->where("ID_CAT_OTRA_ALERGIA",$id);
		$this->db->update("cli_cat_otras_alergias",$updateAlergia);
		$this->db->trans_complete();
	}

	public function deleteAlergia($id,$deleteAlergia)
	{
		$this->db->trans_start();
		$this->db->where("ID_CAT_OTRA_ALERGIA",$id);
		$this->db->update("cli_cat_otras_alergias",$deleteAlergia);
		$this->db->trans_complete();

	}

		public function listarAlergiaID($id)
	{
		$sql='SELECT ID_CAT_OTRA_ALERGIA,DESCRIPCION,ESTADO FROM cli_cat_otras_alergias WHERE ESTADO=1 and ID_CAT_OTRA_ALERGIA='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}
}