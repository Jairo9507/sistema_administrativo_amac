<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 
 */
class cat_pasillos_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarPasillos()
	{
		$sql="SELECT p.ID_CAT_PASILLO,p.DESCRIPCION,p.ESTADO,p.CODIGO,p.NUMERO_ESTANTES from arc_cat_pasillo p WHERE p.ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarPasilloId($id)
	{
		$sql="SELECT ID_CAT_PASILLO,DESCRIPCION,ESTADO,CODIGO,NUMERO_ESTANTES FROM arc_cat_pasillo WHERE ESTADO=1 and ID_CAT_PASILLO=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarUnidades()
	{
		$sql="SELECT ID_UNIDAD, DESCRIPCION FROM seg_cat_unidad_administrativa ";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function obtenerCodigo()
	{
		$sql="SELECT nextval('codigo_pasillo') CORRELATIVO";
		$query=$this->db->query($sql);
		$cod='';
		foreach ($query->result_array() as $q) {
			$cod=$q['CORRELATIVO'];
		}
		return $cod;
	}

	public function savePasillo($registrarPasillo)
	{
		$this->db->trans_start();
		$this->db->insert("arc_cat_pasillo",$registrarPasillo);
		$this->db->trans_complete();
	}

	public function updatePasillo($id,$updatePasillo)
	{
		$this->db->trans_start();
		$this->db->where("id_cat_pasillo",$id);
		$this->db->update("arc_cat_pasillo",$updatePasillo);
		$this->db->trans_complete();
	}

	public function deletePasillo($id,$deletePasillo)
	{
		$this->db->trans_start();
		$this->db->where("id_cat_pasillo",$id);
		$this->db->update("arc_cat_pasillo",$deletePasillo);
		$this->db->trans_complete();		
	}
}