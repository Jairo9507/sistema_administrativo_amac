<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 
 */
class cli_paciente_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listarPacientes()
	{
		$sql='SELECT p.ID_PACIENTE,CONCAT(PRIMER_NOMBRE," ",SEGUNDO_NOMBRE," ",TERCER_NOMBRE) as NOMBRES,CONCAT(PRIMER_APELLIDO," ",SEGUNDO_APELLIDO," ",TERCER_APELLIDO) as APELLIDOS,
			FECHA_NACIMIENTO,NOMBRE_MADRE,NOMBRE_PADRE,DIRECCION,
			CONCAT(SUBSTRING(DUI,1,7),"-",SUBSTRING(DUI,7,1)) as DUI,SEXO,ESTADO,ex.CODIGO_EXPEDIENTE 
			FROM cli_paciente p inner join cli_expediente_clinico ex on ex.id_paciente=p.id_paciente WHERE ESTADO=1 ';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function savePaciente($registarPaciente)
	{
		$this->db->trans_start();
		$this->db->insert("cli_paciente",$registarPaciente);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();

		return $last_id;
	}

	public function saveExpediente($registrarExpediente)
	{
		$this->db->trans_start();
		$this->db->insert("cli_expediente_clinico",$registrarExpediente);
		$this->db->trans_complete();
	}

	public function updatePaciente($id,$updatePaciente)
	{
		$this->db->trans_start();
		$this->db->where("ID_PACIENTE",$id);
		$this->db->update("cli_paciente",$updatePaciente);
		$this->db->trans_complete();
	}

	public function deletePaciente($id,$deletePaciente)
	{
		$this->db->trans_start();
		$this->db->where("ID_PACIENTE",$id);
		$this->db->update("cli_paciente",$deletePaciente);
		$this->db->trans_complete();
	}

	public function listarPacienteId($id)
	{
		$sql='SELECT p.ID_PACIENTE,PRIMER_NOMBRE,SEGUNDO_NOMBRE,TERCER_NOMBRE,PRIMER_APELLIDO,SEGUNDO_APELLIDO,TERCER_APELLIDO,FECHA_NACIMIENTO,NOMBRE_MADRE,NOMBRE_PADRE,RESPONSABLE,DIRECCION,DUI,SEXO,ESTADO,ex.CODIGO_EXPEDIENTE FROM cli_paciente p inner join cli_expediente_clinico ex on ex.id_paciente=p.id_paciente WHERE ESTADO=1 and p.ID_PACIENTE='.$id;
		$query=$this->db->query($sql);
		return $query->result();		
	}
	public function listarHistorial($id)
	{
		$sql='SELECT ce.DESCRIPCION as ENFERMEDAD_NOMBRE, co.FECHA_CONSULTA, IFNULL(co.DIAGNOSTICO,"N/A") AS DIAGNOSTICO,IFNULL(co.SINTOMA,"N/A") AS SINTOMA,co.ID_CITA
			from cli_cita c inner join cli_paciente pa on pa.ID_PACIENTE=c.ID_PACIENTE inner join cli_consulta co on co.id_cita=c.id_cita left join cli_cat_enfermedad ce on ce.id_cat_enfermedad=co.id_cat_enfermedad  WHERE c.ESTADO="Atendida" and c.ID_PACIENTE='.$id;
		$query=$this->db->query($sql);
		return $query->result();

	}

	public function listarConsultaCita($id)
	{
		$sql="SELECT an.ID_ANTECEDENTE,an.ID_PACIENTE,an.ID_MENSTRUACION,an.ID_EMBARAZO,an.ID_INFORMACION_INTERES,
			 an.DATOS_GENERALES,an.HEREDITARIO_FAMILIAR,an.PERSONAL_NOPATOLOGICO,
			 me.PRIMERA_MENSTRUACION,me.CARACTERISTICAS,me.IVSA,me.MENOPAUSIA,me.OTROS as OTROS_MENSTRUACION,
			 emb.TOTAL_EMBARAZO,emb.NUMERO_PARTOS,emb.NUMERO_CESAREAS,emb.NUMERO_ABORTOS,emb.NACIDO_VIVOS,emb.VIVOS_ACTUALES,emb.OTROS as OTROS_EMBARAZO,
			 inf.ULTIMA_PAPANICOLAU,inf.ULTIMA_COLPOSCOPIA, inf.ULTIMA_MAMOGRAFIA,inf.PAREJAS_SEXUALES,inf.METODOS_ANTICONCEPTIVOS,
			 inf.FLUJOS_VAGINALES,inf.PROCEDIMIENTOS_GINECOLOGICOS,inf.HABITOS,inf.OTROS as OTROS_INTERES, inf.CIRUJIAS_PREVIAS
			FROM  cli_paciente pa   
			inner join cli_antecedente an on pa.id_paciente=an.id_paciente 
            inner join cli_menstruacion me on me.id_menstruacion=an.id_menstruacion
            inner join cli_embarazo emb on emb.id_embarazo=an.id_embarazo
            inner join cli_informacion_interes inf on inf.id_informacion_interes=an.id_informacion_interes
			WHERE  pa.ID_PACIENTE=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarAlergiaSustancias()
	{
		$sql="SELECT ID_CAT_ALERGIA_SUSTANCIA,DESCRIPCION FROM cli_cat_alergia_sustancia WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarOtrasAlergias()
	{
		$sql="SELECT ID_CAT_OTRA_ALERGIA,DESCRIPCION FROM cli_cat_otras_alergias WHERE ESTADO=1";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarAlergiasPaciente($id)
	{
		$sql="SELECT ID_ALERGIA_PACIENTE, IFNULL(OTRA_ALERGIA,IFNULL(ac.DESCRIPCION,oa.DESCRIPCION)) nombre_alergia  
			from cli_alergia_paciente ap 
			left join cli_cat_otras_alergias ac on ac.ID_CAT_OTRA_ALERGIA=ap.ID_ALERGIA
			left join cli_cat_alergia_sustancia oa on oa.ID_CAT_ALERGIA_SUSTANCIA=ap.ID_OTRA_ALERGIA 
			WHERE ID_PACIENTE=".$id;
			$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarEnfermedadPaciente($id)
	{
		$sql='SELECT ID_CAT_ENFERMEDAD,CODIGO,DESCRIPCION FROM cli_cat_enfermedad en inner join cli_enfermedad_paciente ep on ep.id_enfermedad=en.ID_CAT_ENFERMEDAD WHERE ep.ID_PACIENTE='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}
	

	public function ultimoExpediente()
	{
		$sql="SELECT * FROM cli_expediente_clinico ORDER BY ID_EXPEDIENTE_CLINICO DESC LIMIT 1";
		$query=$this->db->query($sql);
		$cod='';
		foreach ($query->result_array() as $q) {
			$cod=$q['CODIGO_EXPEDIENTE'];
		}
		return $cod;
	}
}