<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class pro_proyecto_consumo_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	public function listarConsumos($id){
		$sql='SELECT p.ID_PROYECTO, p.CODIGO_PROYECTO, p.NOMBRE_PROYECTO, c.NOMBRE_REALIZADOR, c.NOMBRE_SUPERVISOR , c.ID_PROYECTO_CONSUMO,c.FECHA_CREACION
		FROM pro_proyecto p 
		INNER JOIN pro_proyecto_consumo c ON c.ID_PROYECTO=p.ID_PROYECTO 
		WHERE p.ID_PROYECTO='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function listarConsumoId($id){
		$sql='SELECT p.ID_PROYECTO, p.CODIGO_PROYECTO, p.NOMBRE_PROYECTO, c.NOMBRE_REALIZADOR, c.NOMBRE_SUPERVISOR , c.ID_PROYECTO_CONSUMO,c.FECHA_CREACION
		FROM pro_proyecto p 
		INNER JOIN pro_proyecto_consumo c ON c.ID_PROYECTO=p.ID_PROYECTO 
		WHERE c.ID_PROYECTO_CONSUMO='.$id;
		$query=$this->db->query($sql);
		return $query->result();
	}	

	public function listarDetalles($id)
	{
		$sql='SELECT C.DESCRIPCION,A.CANTIDAD_CONSUMIDA,DATE_FORMAT(A.FECHA_CONSUMO,"%d-%m-%Y") as FECHA_CONSUMO FROM pro_proyecto_consumo_detalle A
			LEFT JOIN pro_proyecto_material_recibido_detalle B ON  A.ID_MATERIAL_RECIBIDO_DETALLE=B.ID_MATERIAL_RECIBIDO_DETALLE
			LEFT JOIN inv_detalle_orden_compra C ON B.ID_DETALLE_ORDEN_COMPRA=C.ID_DETALLE_ORDEN_COMPRA
			LEFT JOIN inv_orden_compra D ON D.ID_ORDEN_COMPRA=C.ID_ORDEN_COMPRA
			WHERE A.ID_PROYECTO_CONSUMO='.$id.' ORDER BY A.FECHA_CONSUMO ASC';
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarProyectos(){
		$sql='SELECT ID_PROYECTO,CODIGO_PROYECTO,NOMBRE_PROYECTO FROM pro_proyecto WHERE ESTADO=1';
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function listarProyectoId($id)
	{
		$sql='SELECT p.ID_PROYECTO,p.CODIGO_PROYECTO,p.NOMBRE_PROYECTO,
		CONCAT(e.PRIMER_NOMBRE,"-",e.PRIMER_APELLIDO) as NOMBRE_SUPERVISOR ,p.NOMBRE_REALIZADOR
		FROM pro_proyecto p 
		LEFT JOIN seg_empleado e ON p.ID_SUPERVISOR=e.ID_EMPLEADO
		WHERE p.ESTADO=1 AND ID_PROYECTO='.$id;
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function listarIngresos($id)
	{
		$sql="SELECT (SELECT DESCRIPCION FROM inv_detalle_orden_compra WHERE ID_DETALLE_ORDEN_COMPRA=C.ID_DETALLE_ORDEN_COMPRA) as DESCRIPCION, 
			C.CANTIDAD_RECIBIDA , C.ID_MATERIAL_RECIBIDO_DETALLE,IFNULL((SELECT SUM(CANTIDAD_CONSUMIDA) FROM pro_proyecto_consumo_detalle A
		WHERE ID_MATERIAL_RECIBIDO_DETALLE=C.ID_MATERIAL_RECIBIDO_DETALLE),0)  as TOTAL
		FROM pro_proyecto A
		LEFT JOIN pro_proyecto_material_recibido B ON A.ID_PROYECTO=B.ID_PROYECTO
        LEFT JOIN pro_proyecto_material_recibido_detalle C ON C.ID_PROYECTO_MATERIAL_RECIBIDO=B.ID_PROYECTO_MATERIAL_RECIBIDO
		WHERE A.ESTADO=1 AND  A.ID_PROYECTO=".$id;
		$query=$this->db->query($sql);
		return $query->result();				
	}

	public function saveConsumo($registrarConsumo)
	{
		$this->db->trans_start();
		$this->db->insert("pro_proyecto_consumo",$registrarConsumo);
		$last_id=$this->db->insert_id();
		$this->db->trans_complete();
		return $last_id;  

	}

	public function updateConsumo($id,$updateConsumo)
	{
		$this->db->trans_start();
		$this->db->where('ID_PROYECTO_CONSUMO',$id);
		$this->db->update('pro_proyecto_consumo',$registrarConsumo);
		$this->db->trans_complete();
	}

	public function saveConsumoDetalle($registrarConsumoDetalle)
	{
		$this->db->trans_start();
		$this->db->insert("pro_proyecto_consumo_detalle",$registrarConsumoDetalle);
		$this->db->trans_complete();
	}


}


?>