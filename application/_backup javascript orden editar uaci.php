<script type="text/javascript">
  var base_url = "<?php echo base_url();?>";

  $(document).ready(function() {
     var totalCompra=$("#totalCompra").val();      

    $("textarea#concepto").keypress(function(e){
        var value=$(this).val();
        console.log(value.length);
        var maximo=2500;
        if (value.length===maximo) {
          alertify.warning("Ha llegado al maximo de caracteres disponibles");
          return   false;
        }

    });
    $("#tipoOrden").change(function() {
        var option = $("select#tipoOrden").val();
        var input1 = document.getElementById('formularioProductosNuevos');
        var input2 = document.getElementById('formularioProductosOtros'); 
        var input3 = document.getElementById('formularioDetalleOrden');

        if (option == 1) {
          input1.style.display  = 'block';
          input2.style.display = 'none';
          input.style.display  = 'none';
        }else if (option == 5) {
          input1.style.display  = 'none';
          input2.style.display = 'block';
          input3.style.display = 'none';
        }else if(option == 0) {
          input1.style.display  = 'none';
          input2.style.display = 'none';
          input3.style.display = 'none';
        }else {
          input1.style.display  = 'none';
          input2.style.display = 'none';
          input3.style.display = 'block';
        }
    });

    $("#tipoEntrega").change(function() {
      var val = $("select#tipoEntrega").val();
      var f = document.getElementById('fechaEntrega');
      f.style.display = 'block';
      
    });
    var products=[];
    $("#productos").select2();
    $('#products').DataTable({
      pageLength:3,
      "order" : [],
      language: {
       search:'Buscar:',
       order: 'Mostrar Entradas',
       paginate: {
        first:"Primero",
        previous:"Anterior",
        next:"Siguiente",
        last:"Ultimo"
      },
      emptyTable: "No hay informacion disponible",
      infoEmpty: "Mostrando 0 de 0 de 0 entradas",
      lengthMenu:"Mostrar _MENU_ Entradas",
      info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
    }            
  });

    $('#example1').DataTable(
    {
      pageLength:5,
      "order" : [],
      language: {
        search:'Buscar:',
        order: 'Mostrar Entradas',
        paginate: {
          first:"Primero",
          previous:"Anterior",
          next:"Siguiente",
          last:"Ultimo"
        },
        emptyTable: "No hay informacion disponible",
        infoEmpty: "Mostrando 0 de 0 de 0 entradas",
        lengthMenu:"Mostrar _MENU_ Entradas",
        info:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
      }
    });

    $("#ActualizarOrden").click(function(e) {

      // var formulario = document.getElementById('productosNuevos');
      // formulario.style.display = 'block';
      var ordenCompra = new Object();
      ordenCompra.id_orden_compra = $("input#id_orden_compra").val();
      ordenCompra.fecha = $("input#fecha").val();
      ordenCompra.proveedor = $("select#proveedor").val();
      ordenCompra.concepto = $("textarea#concepto").val();
      ordenCompra.condPago = $("textarea#condPago").val();
      var DatosJson = JSON.stringify(ordenCompra);
      console.log(DatosJson);
      $.post(base_url+"compra/detalle_ordencompra/actualizarOrden",{
        ordenUpdatePost : DatosJson
      }, function(data, textStatus) {
        $("#mensaje").append(data.response_msg);
      }, "json").fail(function (response) {
       $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
       console.log('Error: ' + response.responseText);
     });
    });


    $("#btnAddProduct").click(function() {
      var product = new Object();
      var html = '';
      var p    = document.getElementById("productos");

      if (document.getElementById('d1').checked) {
        product.destino = document.getElementById('d1').value;
      }else if (document.getElementById('d2').checked) {
        product.destino = document.getElementById('d2').value;
      }

      // var cantidad = parseInt();
      product.nombre = p.options[p.selectedIndex].text;
      product.id_orden_compra = $("input#id_orden_compra").val();
      product.id_producto     = $("select#productos").val();
      product.precio          = $("input#precio").val();
      product.cantidad        = $("input#cantidad").val();
      product.cuenta          = '';
      product.codOrden        = $("input#numOrdenCompra").val();
      product.descripcion     = $("textarea#descripcionOrden").val(); 
      product.concepto      = $("textarea#concepto").val();

      products.push(product);
      console.log(products);

      html+='<tr>';
      html+='<td>'+product.cantidad+'</td>';
      html+='<td class="Idproducto" style="display:none;">'+product.id_producto+'</td>';
      // html+='<td>'+product.nombre+'</td>';
      html+='<td>'+product.descripcion+'</td>';
      html+='<td>'+"$"+product.precio+'</td>';
      html+='<td>'+"$"+parseFloat(product.precio*product.cantidad)+'</td>';
      html+='<td><button type="button"  title="Eliminar Estudio"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>';
      html+='</tr>';
      totalCompra=parseFloat(totalCompra)+parseFloat(product.precio*product.cantidad);
      $("#totalCompra").val(totalCompra);
      console.log(html);
      $("#tableProducts").append(html);
      $("input#precio").val("");
      $("input#cantidad").val("");
      $("textarea#descripcionOrden").val("");
      $("input#codPresupuestario").val("");

       
    });
      $("#btnAddProduct2").click(function() {
      var product = new Object();
      var html = '';
      var p = document.getElementById("productos");
      product.nombre = p.options[p.selectedIndex].text;
      product.id_orden_compra = $("input#id_orden_compra").val();
      product.codOrden        = $("input#numOrdenCompra").val();
      var   precio2           = '';
      product.precio          = $("input#precio3").val();
      var   cantidad          = '';
      product.descripcion     = $("textarea#descripcionOrden3").val(); 
      product.destino         ='G';
      product.cuenta          =1;
      product.id_producto     ='';
      product.cantidad        ='';
      products.push(product);
      console.log(products);
      html+='<tr>';
      html+='<td>'+cantidad+'</td>';
      html+='<td class="Idproducto" style="display:none;">'+product.id_producto+'</td>';
      // html+='<td>'+product.nombre+'</td>';
      html+='<td>'+product.descripcion+'</td>';
      html+='<td>'+precio2+'</td>';
      html+='<td>'+"$"+product.precio+'</td>';
      html+='<td><button type="button"  title="Eliminar Estudio"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>';
      html+='</tr>';
      totalCompra=parseFloat(totalCompra)+parseFloat(product.precio*product.cantidad);
      $("#totalCompra").val(totalCompra);      
      console.log(html);
      $("#tableProducts").append(html);
      $("input#precio").val("");
      $("input#cantidad").val("");
       
    });
     $("#btnAddProduct3").click(function() {
      var product = new Object();
      var html = '';
      var p    = document.getElementById("productos");




        product.destino = 'G';
      // var cantidad = parseInt();
     // product.nombre = p.options[p.selectedIndex].text;
      product.id_orden_compra = $("input#id_orden_compra").val();
      product.id_producto     = 0;
      product.precio          = $("input#precio1").val();
      product.cantidad        = $("input#cantidad1").val();
      product.cuenta          = $("input#codPresupuestario").val();
     //product.destino         = $("input#destino").val();
      // product.tipoEntrega     = $("select#tipoEntrega").val();
      product.codOrden        = $("input#numOrdenCompra").val();
      product.descripcion     = $("textarea#descripcionOrden1").val(); 

      products.push(product);
      console.log(products);
      html+='<tr>';
      html+='<td>'+product.cantidad+'</td>';
      html+='<td class="Idproducto" style="display:none;">'+product.id_producto+'</td>';
      // html+='<td>'+product.nombre+'</td>';
      html+='<td>'+product.descripcion+'</td>';
      html+='<td>'+"$"+product.precio+'</td>';
      html+='<td>'+"$"+parseFloat(product.precio*product.cantidad)+'</td>';
      html+='<td><button type="button"  title="Eliminar Estudio"  class="btn btn-danger btn-xs btnEliminar"><span class="glyphicon glyphicon-remove"></span></button></td>';
      html+='</tr>';
      console.log(html);
      $("#tableProducts").append(html);
      $("input#precio1").val("");
      $("input#cantidad1").val("");
      $("textarea#descripcionOrden1").val("");
       
    });

      $("#btnGuardar").click(function() {
      alertify.confirm("¿Es la fecha correcta?"+"  "+$("input#fecha").val(),"Si da click en ACEPTAR se guardará el registro",function(){
          document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ base_url +"/img/gif-load.gif'> Cargando...</center></div></div>";

        var Orden = new Object();
        Orden.id = '';
        Orden.tipoOrden = $("select#tipoOrden").val();
        Orden.products = products;
        //Orden.codOrden = $("input#numOrdenCompra").val();
        console.log(products);
        $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ base_url +"/img/gif-load.gif'> Cargando...</center></div></div>");
        var DatosJson = JSON.stringify(Orden);
        $.post(base_url+"compra/ordencompra/agregarPedido",{
            ordenPost: DatosJson
        }, function (data, textStatus) {
            console.log(data);
            $("#mensaje").html(data.response_msg);
           //window.location.href = base_url+"compra/detalle_ordencompra";
           location.reload();
        }, "json").fail(function (response) {
            $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
            console.log('Error: '+response.responseText);
        });;
         
      },function(){

      }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
    });


  });

function modificar(id_producto, iddetalle) {

  var product = new Object();
  product.precio   = $("#precio_"+id_producto).val();
  product.cantidad = $("#cantidad_"+id_producto).val();
  product.id= iddetalle;
  product.id_producto = id_producto;
  product.descripcion = $('textarea#descripcion_'+id_producto).val();
  //Orden.codOrden = $("input#codigo_orden").val();
  var DatosJson=JSON.stringify(product);
  $("#mensaje").append("<div class='modal1'><div class='center1'> <center> <img src='"+ base_url +"/img/gif-load.gif'> Cargando...</center></div></div>");
  console.log(DatosJson);
  $.post(base_url+'compra/detalle_ordencompra/updateDetalle',{
    detallePost:DatosJson
  },function(data,textStatus){
    console.log(data);
    $("#mensaje").append(data.response_msg);
    location.reload(); 
  },"json").fail(function(response){
    $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
    console.log('Error: ' + response.responseText);
  });;

}
    $(document).on("click",".btnEliminar",function(){
      var id=$(this).closest('tr').find(".Idproducto").text();
      console.log(id);
      console.log(products);
      for (var i = 0; i < products.length; i++) {
          if(products[i].id_producto=id){            
           var descuento=parseFloat(products[i].precio*products[i].cantidad);
           //console.log('descuento '+descuento);
           var total=parseFloat($("#totalCompra").val())-descuento;
           console.log('total '+total);
           totalCompra=total;
            $("#totalCompra").val(total);        
            products.splice(i,1);            
          }
      }
      console.log(products);
      $(this).closest('tr').remove();
    });

function salir()
{
  alertify.confirm("¿Esta seguro de guardar el registro?","Click en ACEPTAR para guardar",function(){
    window.location.href = base_url+"compra/detalle_ordencompra"

  },function(){

  }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});

}
function regresar()
{
  alertify.confirm("¿Desea Regresar?","Si da clic en aceptar regresara a la lista",function(){
    window.location.href = base_url+"compra/detalle_ordencompra"

  },function(){

  }).set({labels:{ok:'Aceptar',cancel:'Cancelar'}});
} 

function eliminarfila(products)
{
 // document.getElementById("tableProducts").deleteRow(0);
} 
function eliminarDato(nombre, iddetalle, id_producto) {
  alertify.confirm("Eliminar Estudio","¿Seguro de eliminar el estudio "+nombre+"?",function(){
   document.getElementById('mensaje').innerHTML = "<div class='modal1'><div class='center1'> <center> <img src='"+ base_url +"/img/gif-load.gif'> Eliminando...</center></div></div>";
   var Producto = new Object();
   Producto.id = iddetalle;
   var DatosJson = JSON.stringify(Producto);
   $.post(base_url+'compra/ordencompra/eliminar',{
    detalleProducto:DatosJson
  }, function (data, textStatus) {
    $("#mensaje").html(data.response_msg);
    document.getElementById("tableProducts").deleteRow(0);
  }, "json").fail(function(response) {
   $("#mensaje").html("<div class='alert alert-danger text-center' alert-dismissable> <button type='button' class='close' data-dismiss='alert'>&times;</button>No se pudo realizar la operacion</div>");
   console.log('Error: ' + response.responseText);
 });;
},function() {

});;
}

function justNumbers(e)
{
  var keynum = window.event ? window.event.keyCode : e.which;
  if ((keynum == 8) || (keynum == 46))
    return true;

  return /\d/.test(String.fromCharCode(keynum));
}
</script>

