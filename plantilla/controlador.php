<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class cat_anaqueles extends Admin_Controller //el controlador debe llmarse igual que el archivo
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->model("cat_anaqueles_model");
		$this->load->model("usuario_model");
		$this->breadcrumbs->unshift(1,"Anaqueles","catalogos/cat_anaqueles");	//La ruta es la carpeta del controlador, la cual esta en controllers->catalogos/cat_anaqueles
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect("auth/login",'refresh');
		} else {
			$this->page_title->push('Actividades');
			$this->data['pagetitle']=$this->page_title->show();
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/*Menu*/
			$this->data['menu']=$this->menu_model->MenuTree($this->ion_auth->get_user_id());
			
			$this->data['submenu']=$this->menu_model->Submenu($this->ion_auth->get_user_id());

			/*Obtener lista de actividades */

            // $this->data['actividades'] = $this->cat_actividad_model->listarActividad();

			$this->template->admin_render("admin/cat_anaquel/index",$this->data); //la ruta depende de la carpeta donde se este la vista OJO-> en este caso la vista esta en la carpeta View -> admin -> cat_anaque->index
		}
	}
}